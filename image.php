<?php

include 'include/config.php';
include 'include/functions.php';


if(!empty($_GET['info']))
{
	if(isset($_GET['team']))
	{
		$info = explode('-', $_GET['info']);
		if(empty($info[1]))
		{
			$info[1] = 'small';
		}
		if(empty($info[3]))
		{
			$info[3] = '/';
		}
		
		$file = __DIR__ . '/' . TEAM_DATADIR . $global_team_images[$info[1]]['name'];

		if(count($info) != 4 || $info[2] != getTeamImageHash($info[0], $info[1], $info[3]))
		{
			
		}
		else
		{
			$im = __DIR__ . '/' . TEAM_DATADIR . intval($info[0]) . $info[3] . $global_team_images[$info[1]]['name'];
        
	        if(file_exists($im))
	        {
	            $file = $im;
	        }
		}
	}
	elseif(isset($_GET['user']))
	{

	}

	Header('Content-Type: image/jpg');
	header('Content-Length: ' . filesize($file));
	readfile($file);
}
