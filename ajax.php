<?php
/* *****************************************************************************
  SOUBOR, KTERY BUDE ZPRACOVAVAT VESKERE AJAX POZADAVKY
  ****************************************************************** */
  Header("Content-Type:application/json; charset=utf-8");

require_once 'include/config.php';
require_once 'include/functions.php';
require_once 'include/init.php';
require_once 'include/preload.php';
require_once 'action/'.$page.'.action.php';
require_once 'ajax.functions.php';

/**
 * Vrátí kompletní seznam měst pro našeptávač
 * Toto může volat i nepřihlášený uživatel na úvodní stránce
 **/
if(isset($_REQUEST['city']) && !empty($_REQUEST['tag']))
{
    // načteme seznam měst
    $tag = trim($_REQUEST['tag']);

    $cities = SearchDAO::lookupCities($tag);

    if(count($cities) > 0)
    {
        // musíme vrátit javascriptové pole objektů
        $items = array();
        foreach($cities as $a)
        {
            $data[] = '{"key": "'.$a['attrs']['id'].'", "value": "'.$a['attrs']['name'].', '.$a['attrs']['region'] .', '.$a['attrs']['country'].'"}';
        }
        $return = '[' . implode(",", $data) . ']';
    }
    else
    {
        $return = '[]';
    }

    echo $return;
    die;
}


/**
 * Vrátí kompletní seznam měst pro našeptávač
 * Toto může volat i nepřihlášený uživatel na úvodní stránce
 **/
if(isset($_REQUEST['quick_search']) && !empty($_REQUEST['tag']))
{
    // načteme seznam měst
    $tag = trim($_REQUEST['tag']);

    if(empty($tag))
    {
        die;
    }

    $view = false;
    $start = 0;

    $searched_teams = SearchDAO::lookupTeams($tag, $view, $start, 0, 30);
    $searched_users = SearchDAO::lookupUsers($tag, $view, $start, 0, 30);

    $data = array();

    if(count($searched_teams) > 0)
    {
        // musíme vrátit javascriptové pole objektů
        $items = array();
        foreach($searched_teams as $found_team)
        {
            $key = $found_team->getProfileLink();
            $value = $found_team->getName();
            $data[] = '{"key": "'.$key.'", "value": "'.$value.'"}';
        }
    }

    if(count($searched_users) > 0)
    {
        // musíme vrátit javascriptové pole objektů
        $items = array();
        foreach($searched_users as $found_user)
        {
            $key = $found_user->getProfileLink();
            $value = $found_user->getName();
            $data[] = '{"key": "'.$key.'", "value": "'.$value.'"}';
        }
    }

    $return = '[' . implode(",", $data) . ']';

    echo $return;
    die;
}


/**
 * defaultně je pro všechny dotazy status chybný, pokud to proběhne v pořádku, tak přepíšeme na OK
 **/
$return = array(
    'status'  => 'error',
    'message' => ''
);


/**
 * všechny následující akce smí dělat jen přihlášený uživatel, jinak provádění skriptu ukončíme
 **/
if(!($logged_user instanceof User) || empty($logged_user->id))
{
    $return['message'] = 'not-logged';

    echo json_encode($return);
    die;
}


/**
 * Najde uživatele do našeptávače kterým můžeme napsat zprávu
 **/
if(isset($_REQUEST['messageRecipients']) && !empty($_REQUEST['tag']))
{
    $db = new Database();

    $teams = $logged_user->getTeamList(true);

    $teams = implode(',', array_keys($teams));

    $sql = 'SELECT *, CONCAT(`user_fname`, " ", `user_sname`) as fullname
            FROM `user`
            LEFT JOIN `user_in_team` ON `user_in_team`.`user_id` = `user`.`user_id`
            LEFT JOIN `team` ON `team`.`team_id` = `user`.`user_team`
            WHERE (`user_fname` LIKE "%'.mysqli_real_escape_string($db->mysqli(), $_REQUEST['tag']).'%" OR
                  `user_sname` LIKE "%'.mysqli_real_escape_string($db->mysqli(), $_REQUEST['tag']).'%")
                  AND `user`.`user_id` != ' . $logged_user->id . ' AND `user_in_team`.`team_id` IN ('.$teams.') AND `user_in_team`.`player` = 1
            GROUP BY `user`.`user_id`';

    $db->query($sql);

    while($a = $db->readRow('aarr'))
    {
        $teamname = '';
        if($a['user_team'] > 0)
        {
            $team = new Team($a);
            $teamname  = ' [' . $team->getName() . ']';
        }
        $data[] = '{"key": "'.$a['user_id'].'-'.getRecipientHash($a['user_id']).'", "value": "'.$a['fullname'].$teamname.'"}';
    }

    $return = '['.implode(', ', $data).']';

    echo $return;
    die;
}


/**
 * Nahrání profilové fotky uživatele
 **/
if(isset($_REQUEST['file']))
{
    // default hláška
    $return['message'] = $tr->tr('Nastala chyba při editaci profilového obrázku.');

    // pouze pokud je odeslán nějaký soubor
    if(!empty($_FILES['image']))
    {
        // musime preulozit info o verzi
        $logged_user->version = uniqid();
        $logged_user->updateVersion();

        // přeuložíme session
        updateLoggedUser($_project, $logged_user);

        // samotný upload obrázku
        // true znaci ze chceme vrátit cesty ke všem obrázkům
        $img = $logged_user->uploadImage(true);

        // pokud se upload povedl
        if(is_array($img))
        {
            // vše proběhlo v pořádku
            $return['status'] = 'ok';
            $return['message'] = $tr->tr('Změna profilového obrázku proběhla úspěšně.');

            // ke statusu musíme vrátit i obrázky,
            // abychom je mohli aktualizovat na všech místech stránky
            $return = array_merge($return, $img);
        }
    }

    echo json_encode($return);
    die;
}


/**
 * Smazání profilové fotky uživatele
 **/
if(isset($_REQUEST['delete_profile_photo']))
{
    // musime preulozit info o verzi
    $logged_user->version = uniqid();
    $logged_user->updateVersion();

    // přeuložíme session
    updateLoggedUser($_project, $logged_user);

    @unlink(USER_DATADIR . $logged_user->id . '/' . ORIGINAL_IMAGE_NAME);
    @unlink(USER_DATADIR . $logged_user->id . '/' . ORIGINAL_IMAGE_NAME_CROPPED);

    // máme několik typů obrázků, pokusíme se je všechny smazat
    foreach($default_users as $key => $image)
    {
        $return['img_'.$key] = '<img src="'.$logged_user->getUserImage($key).'" alt="'.$logged_user->fname.' '.$logged_user->sname.'" width="'.intval($sizes_user[$key][0]).'" height="'.intval($sizes_user[$key][1]).'">';
    }


    // vše proběhlo v pořádku
    $return['status'] = 'ok';
    $return['message'] = $tr->tr('Smazání profilového obrázku proběhlo úspěšně.');
    echo json_encode($return);
    die;
}


/**
 * Změna výřezu profilového obrázku uživatele
 **/
if(isset($_REQUEST['crop']))
{
    // default hláška
    $return['message'] = $tr->tr('Nastala chyba při editaci profilového obrázku.');

    // musime preulozit info o verzi
    $logged_user->version = uniqid();
    $logged_user->updateVersion();

    // přeuložíme session
    updateLoggedUser($_project, $logged_user);

    // ořízneme obrázek
    $img = $logged_user->cropImage($_REQUEST['width'], $_REQUEST['x'], $_REQUEST['y']);

    // v $img by měl být seznam všech velikostí profilového obrázku
    if(is_array($img))
    {
        // vše proběhlo v pořádku
        $return['status'] = 'ok';
        $return['message'] = $tr->tr('Změna profilového obrázku proběhla úspěšně.');
        $return = array_merge($return, $img);
    }

    echo json_encode($return);
    die;
}


/**
 * TZ = tisková zpráva
 * Editace tiskové zprávy
 * Načte data tiskové zprávy, které se pak vypíši do edit formuláře
 **/
if(isset($_REQUEST['edit_press']) && !empty($_REQUEST['edit_press']))
{
    // načteme si object z DB podle zadaného ID - hledáme pouze mezi Tiskovými zprávami
    $origin = WallDAO::getObject($_REQUEST['edit_press'], array('press'));

    // TZ existuje
    if(is_array($origin) && count($origin) > 0)
    {
        // nás zajímá jen první prvek pole, víc nám jich v tom poli stejně nevrátí
        $origin = current($origin);

        // musíme si "rozbalit" data
        $origin['value'] = unserialize($origin['value']);

        // potřebujeme objekt týmu, abychom zkontrolovali zda má uživatel práva editovat TZ
        $team = TeamDAO::get($origin['team_id']);

        // kontrola zda uživatel opravdu může tuto TZ editovat
        if($team instanceof Team && $team->userCanChangePress($logged_user))
        {
            $return = array(
                'id'        => $_REQUEST['edit_press'],
                'heading'   => $origin['value']->heading,
                'perex'     => $origin['value']->perex,
                'text'      => $origin['value']->text,
                'date'      => date('Y-m-d', strtotime($origin['value']->date_begin)),
                'time'      => date('H:i', strtotime($origin['value']->date_begin)),
                'status'    => 'ok',
                'message'   => $tr->tr('Tisková zpráva byla nalezena.')
            );
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při editaci tiskové zprávy.') . ' error code: #001002';
        }
    }
    // TZ neexistuje
    else
    {
        $return['message'] = $tr->tr('Hledaná tisková zpráva neexistuje.');
    }

    echo json_encode($return);
    die;
}


/**
 * TZ = tisková zpráva
 * Samotná editace TZ
 **/
if(isset($_REQUEST['edit_press_id']))
{
    // povinné položky jsou nadpis, perex a text
    if(empty($_REQUEST['press']['heading']) || empty($_REQUEST['press']['perex']) || empty($_REQUEST['press']['text']))
    {
        $return['message'] = $tr->tr('Nebyly vyplněny všechny povinné údaje.') . ' error code: #001001';
    }
    else
    {
        // potrebujeme ID tymu, kteremu TZ patri
        $req = explode('_', $_REQUEST['edit_press_id']);

        // potrebujeme objekt tymu, abychom zjistili, jestli vubec exitsuje a jestli ma uzivatel práva to přidat
        $team = TeamDAO::get($req[0]);

        // existuje tym a uzivatel muze menit TZ
        if($team instanceof Team && $team->userCanChangePress($logged_user))
        {
            // uprava času, aby byl ve správném formátu
            $time = substr($_REQUEST['press']['start_time'], 0, 5);
            $time .= ':00';
            $time = explode(':', $time);
            foreach($time as $i => $val)
            {
                // pokud ma jen jedno cislo, tak tam prihodime nulu
                $time[$i] = (strlen($val) == 2 ? $val : '0'.intval($val));
            }
            $time = implode(':', $time);
            $now = date('Y-m-d H:i:s');

            $data['id']         = $req[1];
            $data['parent_id']  = 0;
            $data['user_id']    = $logged_user->id;
            $data['team_id']    = $req[0];
            $data['type']       = 'press';
            $data['created']    = $now;
            $data['date_begin'] = $_REQUEST['press']['start'] . ' ' . $time;
            //$data['date_end']   = '0000-00-00 00:00:00';
            $data['heading']    = htmlspecialchars($_REQUEST['press']['heading']);
            $data['perex']      = htmlspecialchars($_REQUEST['press']['perex']);
            $data['text']       = htmlspecialchars($_REQUEST['press']['text']);
            $data['value']      = serialize((object)$data);

            // nemuze dat datum zobrazeni v minulosti
            if($data['date_begin'] < $now)
            {
                $data['date_begin'] = $now;
            }

            $object = new PressRelease($data);

            // samotná editace v DB
            if(WallDAO::updateItem($object, $object->team_id))
            {
                // zprava je nepublikovana
                if($data['date_begin'] > $now)
                {
                    $return = array(
                        'status'    => 'edit-unpublished-ok',
                        'message'   => '<h3 class="title">' . $tr->tr('Tisková zpráva byla editována!') . '</h3><p class="text">' . $tr->tr('Publikována bude dne') . ' ' . date('d.m.Y', strtotime($data['date_begin'])) . ' ' . $tr->tr('v') . ' ' . date('H:i', strtotime($data['date_begin'])) . ' hod.</p>',
                        'response'  => printUnpublishedRow($data),
                        'messageWarning' => $tr->tr('Nebyly nalezeny žádné publikované tiskové zprávy.')
                    );
                }
                else
                // zprava je publikovana
                {
                    $return = array(
                        'status'    => 'edit-ok',
                        'message'   => '<h3 class="title">' . $tr->tr('Hotovo!') . '</h3><p class="text">' . $tr->tr('Tisková zpráva byla v pořádku editována a publikována.') . '</p>',
                        'response'  => getWall(array($data)),
                        'messageWarning' => $tr->tr('Nebyly nalezeny žádné publikované tiskové zprávy.')
                    );
                }
            }
        }
    }


    echo json_encode($return);
    die;
}

/**
 * Přidání nové TZ
 **/
if(isset($_REQUEST['add_press']))
{
    $return = array(
        'status'    => 'error'
    );

    // FIXME kontrola podvrzenych dat
    // povinne udaje jsou nadpis, perex a text
    if(empty($_REQUEST['press']['heading']) || empty($_REQUEST['press']['perex']) || empty($_REQUEST['press']['text']))
    {
        $return = array(
            'status'    => 'error',
            'message'   => $tr->tr('Nastala chyba při přidávání tiskové zprávy. Zkuste to prosím později.') . ' error code: #001004'
        );
    }
    else
    {
        // potrebujeme objekt tymu, abychom zjistili, jestli vubec exitsuje a jestli ma uzivatel práva to přidat
        if(!empty($logged_user->getActiveTeam()->id))
        {
            $team = TeamDAO::get($logged_user->getActiveTeam()->id);
        }
        else
        {
            $team = false;
        }

        // existuje tym a uzivatel muze menit TZ
        if($team instanceof Team && $team->userCanChangePress($logged_user))
        {
            // uprava času, aby byl ve správném formátu
            $time = substr($_REQUEST['press']['start_time'], 0, 5);
            $time .= ':00';
            $time = explode(':', $time);
            foreach($time as $i => $val)
            {
                $time[$i] = (strlen($val) == 2 ? $val : '0'.intval($val));
            }
            $time = implode(':', $time);

            $now = date('Y-m-d H:i:s');

            $data['parent_id']  = 0;
            $data['user_id']    = $logged_user->id;
            $data['team_id']    = $logged_user->getActiveTeam()->id;
            $data['type']       = 'press';
            $data['created']    = $now;
            $data['date_begin'] = $_REQUEST['press']['start'] . ' ' . $time;
            //$data['date_end']   = '0000-00-00 00:00:00';
            $data['heading']    = htmlspecialchars($_REQUEST['press']['heading']);
            $data['perex']      = htmlspecialchars($_REQUEST['press']['perex']);
            $data['text']       = htmlspecialchars($_REQUEST['press']['text']);
            $data['value']      = serialize((object)$data);

            if($data['date_begin'] < $now)
            {
                $data['date_begin'] = $now;
            }

            $object = new PressRelease($data);

            // vlozeni
            $inserted_id = WallDAO::insertItem($object, $object->team_id, 'press');
            if($inserted_id)
            {
                $notification_data = array();
                $time = time();
                $users = $team->getPlayers();

                $object_id = $team->id . '_' . $inserted_id . '_press';
                $array = array(
                    'team'          => $team->id,
                    'logged_user'   => $logged_user->id
                );

                foreach($users as $user_id)
                {
                    if($user_id == $logged_user->id)
                    {
                        continue;
                    }

                    $notification_data[] = array(
                        'user_id'    => $user_id,
                        'object_id'  => $object_id,
                        'type_id'    => 'NEW-PRESS-RELEASE',
                        'info'       => serialize($array),
                        'object_info'=> '',
                        'timestamp'  => $time,
                        'read'       => '0'
                    );
                }

                @Notifications::insertNotifications($notification_data);


                $data['id'] = $inserted_id;

                if(!empty($data['id']))
                {
                    // zprava je nepublikovana
                    if($data['date_begin'] > $now)
                    {
                        $return = array(
                            'status'    => 'unpublished-ok',
                            'message'   => $tr->tr('<h3 class="title">Tisková zpráva byla přidána!</h3><p class="text">Publikována bude dne') . ' ' . date('d.m.Y', strtotime($data['date_begin'])) . ' ' . $tr->tr('v') . ' ' . date('H:i', strtotime($data['date_begin'])) . ' hod.</p>',
                            'response'  => printUnpublishedRow($data),
                            'messageWarning' => $tr->tr('Nebyly nalezeny žádné publikované tiskové zprávy.') // tohle si zkontroluje v JS zda ma vypsat
                        );
                    }
                    else
                    // zprava je publikovana
                    {
                        $return = array(
                            'status'    => 'ok',
                            'response'  => getWall(array($data)),
                            'message'   => $tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Tisková zpráva byla v pořádku přidána a publikována.'),
                            'messageWarning' => $tr->tr('Nebyly nalezeny žádné publikované tiskové zprávy.') // tohle si zkontroluje v JS zda ma vypsat
                        );
                    }
                }
            }
            else
            {
                $return['message'] = $tr->tr('Nastala chyba při přidávání tiskové zprávy. Zkuste to prosím později znovu.') . ' error code: #001007';
            }
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při přidávání tiskové zprávy. Zkuste to prosím později znovu.') . ' error code: #001008';
        }
    }

    echo json_encode($return);
    die;
}


/**
 * Smazání TZ
 **/
if(isset($_REQUEST['delete_press']))
{
    $object = explode('_', $_REQUEST['delete_press']);
    if(count($object) == 3)
    {
        // potrbeujeme tym, kvuli pravum
        $team = TeamDAO::get($object[0]);

        // může smazat?
        if($team instanceof Team && $team->userCanChangePress($logged_user))
        {
            //
            if(WallDAO::deleteItem($object[0], $object[1], $object[2]))
            {
                $return['status'] = 'ok';
                $return['message'] = $tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Tisková zpráva byla úspěšně smazána.</p>');
                $return['messageWarning'] = $tr->tr('Nebyly nalezeny žádné publikované tiskové zprávy.'); // tohle si zkontroluje v JS zda ma vypsat
            }
            else
            {
                $return['message'] = $tr->tr('Nastala chyba při mazání zprávy.') . ' error code: #001012';
            }
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při mazání zprávy.' . ' error code: #001013');
        }
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při mazání zprávy.' . ' error code: #001014');
    }

    echo  json_encode($return);
    die;
}


/**
 * Přidání komentáře
 **/
if(isset($_REQUEST['add_comment']))
{
    // parent_id ve tvaru "teamid_objectid"
    $parent = explode('_', $_REQUEST['parent_id']);
    if(!empty($parent[0]))
    {
        $team_id = $parent[0];
    }
    else
    {
        // to bude vetsinou kdyz pridavas komentar k novince z RSS
        $team_id = 0;
    }

    $date = date('Y-m-d H:i:s');
    $data['parent_id']  = $_REQUEST['parent_id'];
    $data['user_id']    = $logged_user->id;
    $data['team_id']    = $team_id;
    $data['type']       = 'reply';
    $data['created']    = $date;
    $data['date_begin'] = $date;
    $data['text']       = strip_tags($_REQUEST['text']);
    $data['value']      = serialize($data);

    $object = new Reply($data);
    $inserted_id = WallDAO::insertItem($object, $object->team_id, 'reply');

    // pokud se povedlo vložit komentář
    if($inserted_id)
    {
        $data['id'] = $inserted_id;
        $data['value'] = serialize($object);

        $count = count(WallDAO::getWallList(null, array('reply'), $data['parent_id']));

        $return['status'] = 'ok';

        // object_id puvodniho clanku, je vlastne ten parent_id
        $object_id = $data['parent_id'];

        // vytáhneme si objekt ke kterému přidáváme příspěvek
        $origin = WallDAO::getObject($object_id);

        if(!count($origin))
        {
            $return = array(
                'status'  => 'error-deleted',
                'message' => $tr->tr('K tomuto objektu nemůžete přidat komentář, protože již byl smazán.')
            );

            echo json_encode($return);
            die;
        }

        $origin = current($origin);

        if(!$origin)
        {
            $return = array(
                'status'  => 'error-deleted',
                'message' => $tr->tr('K tomuto objektu nemůžete přidat komentář, protože již byl smazán.')
            );

            echo json_encode($return);
            die;
        }

        $origin['value'] = unserialize($origin['value']);
        // rozlišení zda zobrazit odkaz na "všechny komentáře", nebo je komentaru malo a neni potreba

        if($count > 0)
        {
            switch($origin['type'])
            {
                case 'teampost':
                    $type = 'teampost-detail';
                    $team = TeamDAO::get($data['team_id']);
                    $href = $team->getTeampostDetailLink($tr->tr('tymovy-vzkaz'), $data['parent_id']);
                    //$href = getNewsLink($object_id, $origin['value']->title, $type);
                break;

                case 'press':
                    $type = 'press-release-detail';
                    $team = TeamDAO::get($data['team_id']);
                    $href = $team->getPressDetailLink($origin['value']->heading, $object_id);
                break;

                case 'roster-insert':
                case 'roster-delete':
                    $type = 'roster-edit';
                    $team = TeamDAO::get($data['team_id']);
                    $href = $team->getPressDetailLink('Změny na soupisce', $object_id);
                break;

                case 'gallery-insert':
                    $type = 'gallery-edit';
                    $team = TeamDAO::get($data['team_id']);
                    $href = $team->getPressDetailLink('Nové fotky v galerii', $object_id);
                    // $gallery = GalleryDAO::getGallery($origin['team_id'], $origin['parent_id']);
                    // printr($origin);die;
                break;

                case 'event-result':
                    $type = 'event-result';
                    $event_info = explode('_', $origin['parent_id']);

                    if(count($event_info) != 3)
                    {
                        continue;
                    }

                    $object_team = TeamDAO::get($origin['team_id']);
                    if(!$object_team instanceof Team || empty($object_team->id))
                    {
                        continue;
                    }

                    $event_id = $event_info[1];
                    $event_team_id = $event_info[0];

                    $object_id = $event_team_id . '-' . $event_id . '_'.$type;

                    $href = $object_team->getProfileLink('event-detail') . $event_id . '-' . $event_team_id . '/';
                break;

                default:
                case 'news':
                    $type = 'news-detail';
                    $href = getNewsLink($object_id, $origin['value']->title, $type);
                break;
            }
            $return['count_bubble'] = '<a class="bubble-comment" href="'.$href.'" title="Zobrazit všechny komentáře" id="bubble-'.$object_id.'"><span>' . $count . '</span> '.$tr->tr('reakcí') . '</a>';
        }
        else
        {
            $return['count_bubble'] = '<div class="bubble-comment first" id="bubble-'.$object_id.'"><span>0</span> '.$tr->tr('reakcí') . '</div>';
        }

        if($count > LIST_COMMENT_COUNT_MAX)
        {
            $return['count_all_comments'] = '<span class="allComments"><a href="'.$href.'"><span title="'.$tr->tr('Zobrazit všechny komentáře').'"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.($count ? $count : '0').')</span></a></span>&nbsp;<span class="separator">|</span>&nbsp;</span>';
        }
        else
        {
            $return['count_all_comments'] = '<span class="allComments"><!-- --></span>';
        }
        $return['response'] = printComment($data, 'reply');

        // nacteme si vsechny uzivatele, kteri uz tento objekt okomentovali
        $users = Notifications::getCommentUsers($object_id);

        // vsechny notifikace, ktere jiz jsou k tomuto obejktu
        $notifications = Notifications::getNotificationsByObject($object_id);

        $data = array();
        $time = time();

        // aktualne prihlaseny uzivatel
        $pom = $logged_user->getAllData();

        if($origin['type'] == 'press')
        {
            $admins = $team->getAdmins();
            
            // a pro každého uděláme notifikace
            foreach($admins as $user_id)
            {
                $user = UserDAO::get($user_id);
                $user->getSettings();

                if(empty($user->id))
                {
                    continue;
                }

                $users[] = $user;
            }
        }

        $sent = array();

        // a pro každého uděláme notifikace
        foreach($users as $user)
        {
            // jestlize uzivatel nema nastaveno posilani notifikaci, tak skocime na dalsiho
            if(!$user->settings[UserSettings::KEY_PUBLIC_COMMENT] == UserSettings::VAL_NOTICE_YES || in_array($user->id, $sent))
            {
                continue;
            }

            $sent[] = $user->id;

            // klic notifikace
            $key = $user->id . '-' . $object_id;

            // pro prihlaseneho uzivatele prece nebudu davat notifikaci
            if($user->id != $logged_user->id)
            {
                // notifikace již existuje, takze ji musime updatovat
                if(in_array($key, array_keys($notifications)))
                {
                    // nacteme si existujici notifikaci
                    $notification = $notifications[$key];

                    // vrati nam seznam vsech uzivatelu, kterych se to tyka
                    $uns = unserialize($notification->info);

                    // odstranime ho z uzivatelu,abychom ho pridali zase na konec
                    unset($uns['users'][$pom['id']]);

                    // pridame prihlaseneho uzivatele
                    $uns['users'][$pom['id']] = $pom['id'];

                    // opet ulozime
                    $notification->info = serialize($uns);

                    // jestlize uz byla prectena tak ji zobrazime znovu
                    if($notification->read == 1)
                    {
                        // zmenime cas
                        $notification->timestamp = $time;

                        // a nastavime jako neprectenou
                        $notification->read = 0;
                    }

                    if($origin['type'] == 'press' && $team->isAdmin($user))
                    {
                        $notification->type_id = 'NEW-PRESS-COMMENT';
                    }
                    else
                    {
                        $notification->type_id = 'ARTICLE-COMMENT';
                    }

                    // a notifikaci ulozime i v DB
                    Notifications::updateNotification($notification);
                }
                // vložíme novou notifikaci
                else
                {
                    $data[] = array(
                        'user_id'    => $user->id,
                        'object_id'  => $object_id,
                        'type_id'    => ($origin['type'] == 'press' && $team->isAdmin($user) ? 'NEW-PRESS-COMMENT' : 'ARTICLE-COMMENT'),
                        'info'       => serialize(
                                            array(
                                                'users' => array(
                                                    $pom['id'] => $pom['id']
                                                ))),
                        'object_info'=> '',
                        'timestamp'  => $time,
                        'read'       => '0'
                    );
                }
            }
        }

        // uložíme do DB nové notifikace
        @Notifications::insertNotifications($data);

    }

    echo json_encode($return);
    die;
}


/**
 * Smazání komentáře
 **/
if(isset($_REQUEST['delete_comment']))
{
    if(isset($_REQUEST['team']) && isset($_REQUEST['id']))
    {
        // objekt ktery chceme smazat
        $object_id = $_REQUEST['team'].'_'.$_REQUEST['id'].'_reply';
        $item = WallDAO::getObject($object_id);
        $item = current($item);

        if(count($item) > 0)
        {
            // jen kdyz prispevek patri prihlasenemu uzivateli
            if($logged_user->id == $item['user']->id)
            {
                // smazeme prispevek
                if(WallDAO::deleteItem($_REQUEST['team'], $_REQUEST['id'], 'reply'))
                {
                    $team = TeamDAO::get($_REQUEST['team']);
                    // budeme potřebovat parent objekt, ke kterém komentář patřil, abychom zjistili počet příspěvků pro daný objekt a mohli ho aktualizovat
                    $origin = WallDAO::getObject($item['parent_id']);
                    $origin = current($origin);
                    $origin['value'] = unserialize($origin['value']);

                    $return['response'] = true;
                    $count = count(WallDAO::getWallList(null, array('reply'), $item['parent_id']));

                    if($count > 0)
                    {
                        switch($origin['type'])
                        {
                            case 'teampost':
                                $type = 'teampost-detail';
                                $team = TeamDAO::get($data['team_id']);
                                $href = $team->getTeampostDetailLink($tr->tr('tymovy-vzkaz'), $data['parent_id']);
                                //$href = getNewsLink($object_id, $origin['value']->title, $type);
                            break;

                            case 'press':
                                $type = 'press-release-detail';
                                $href = $team->getPressDetailLink($origin['value']->heading, $object_id);
                            break;

                            case 'roster-insert':
                            case 'roster-delete':
                                $type = 'roster-edit';
                                $href = $team->getPressDetailLink('Změny na soupisce', $object_id);
                            break;

                            case 'gallery-insert':
                                $type = 'gallery-edit';
                                $href = $team->getPressDetailLink('Nové fotky v galerii', $object_id);
                                // $gallery = GalleryDAO::getGallery($origin['team_id'], $origin['parent_id']);
                                // printr($origin);die;
                            break;

                            case 'news':
                            default:
                                $type = 'news-detail';
                                $href = getNewsLink($object_id, $origin['value']->title, $type);
                            break;
                        }
                        $return['count_bubble'] = '<a class="bubble-comment" href="'.$href.'" id="bubble-'.$object_id.'"><span>' . $count . '</span> '.$tr->tr('reakcí') . '</a>';

                        if($count >LIST_COMMENT_COUNT_MAX)
                        {
                            $return['count_all_comments'] = '<a href="'.getNewsLink($object_id, $origin['value']->title, $type).'"><span title="'.$tr->tr('Zobrazit všechny komentáře').'"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.$count.')</span></a>&nbsp;<span class="separator">|</span>&nbsp;';
                        }
                        else
                        {
                            $return['count_all_comments'] = '<span class="allComments"><!-- --></span>';
                        }
                    }
                    else
                    {
                        $return['count_bubble'] = '<div class="bubble-comment first" id="bubble-'.$object_id.'"><span>0</span> '.$tr->tr('reakcí') . '</div>';
                        $return['count_all_comments'] = '<a class="firstComment" title="'.$tr->tr('Přidat první komentář k článku').'" href="#">'.$tr->tr('přidat první komentář').'</a>&nbsp;<span class="separator">|</span>&nbsp;';
                    }

                    $return['status'] = 'ok';
                }
                else
                {
                    $return['message'] = $tr->tr('Nastala chyba při mazání komentáře.') . ' error code: #002010';
                    $return['response'] = false;
                }
            }
            else
            {
                $return['message'] = $tr->tr('Nastala chyba při mazání komentáře.') . ' error code: #002011';
                $return['response'] = false;
            }
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při mazání komentáře.') . ' error code: #002012';
            $return['response'] = false;
        }
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při mazání komentáře.') . ' error code: #002013';
        $return['response'] = false;
    }

    echo json_encode($return);
    die;
}


/**
 * Vrátí seznam lidí, kterí "olajkovali" daný object
 **/
if(isset($_REQUEST['getLikes']))
{
    if(isset($_REQUEST['object_id']))
    {
        $likes = WallDAO::getLikeList($_REQUEST['object_id']);
        if(count($likes) > 0)
        {
            echo '<div class="fancybox-popup">';
            echo '<h3 class="title">Uživatelé, kterým se to líbí...</h3>';

            echo printLikeList($likes, $_REQUEST['object_id'], true);

            echo '</div>';
        }
        else
        {
            echo $tr->tr('Žádné záznamy k zobrazení.');
        }
    }

    die;
}


/**
 * Vrátí seznam lidí, kteří jsou v daném příspěvku označeni jako změny na soupisce
 **/
if(isset($_REQUEST['getRosterChangeUsers']))
{
    if(isset($_REQUEST['object_id']))
    {
        $object = WallDAO::getRosterChangeUsers($_REQUEST['object_id']);
        if($object instanceof RosterEditObject)
        {
            $users = unserialize($object->value);
            foreach($users['users'] as $user_id)
            {
                $all[$user_id] = UserDAO::get($user_id);
            }

            echo '<div class="fancybox-popup">';
            echo '<h3 class="title">Změny na soupisce</h3>';

            echo printLikeList($all, $_REQUEST['object_id'], true);

            echo '</div>';
        }
        else
        {
            echo $tr->tr('Žádné záznamy k zobrazení.');
        }
    }

    die;
}

/**
 * LIKE příspěvku
 **/
if(isset($_REQUEST['like']))
{
    $object = WallDAO::getObject($_REQUEST['id']);

    if($object)
    {
        $ret = WallDAO::likeUnlike($_REQUEST['id'], $logged_user->id, $_REQUEST['type']);

        if($ret !== false)
        {
            if($ret == 'like')
            {
                $return['text'] = $tr->tr('už se mi nelíbí');
            }
            elseif($ret == 'unlike')
            {
                $return['text'] = $tr->tr('líbí se mi');
            }
            $list = WallDAO::getLikeList($_REQUEST['id']);

            $i = 0;
            if(count($list) > 0)
            {
                $response = printLikeList($list, $_REQUEST['id']);
            }
            else
            {
                $response = '';
            }
            $return['status'] = 'ok';
            $return['count'] = count($list);
            $return['response'] = $response;
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při lajkování příspěvku.') . ' error code: #001042';
        }
    }
    else
    {
        $return = array(
            'status' => 'error-deleted',
            'message' => $tr->tr('Tento příspěvek byl již smazán.')
        );
    }
    echo json_encode($return);
    die;
}

/**
 * Endless scroll na nastence, nacte dalsi zpravy
 **/
if(isset($_REQUEST['viewMore']) && !empty($_REQUEST['start']))
{
    // musime si nacist filter ze session
    $filter = $_project['session']->get('filter');

    // podle filtru rovnou nacteme nove prispevky
    $list = $logged_user->getWall($_REQUEST['start'], true, $filter, false);
    if(count($list) > 0)
    {
        $return['status'] = 'ok';
    }
    else
    {
        $return['status'] = 'ok-no-more';
    }

    // vrátíme HTML s vypsanými příspěvky
    $return['response'] = getWall($list, false, $logged_user);
    $return['count'] = count($list);

    echo json_encode($return);
    die;
}


/**
 * Nastavení notifikace, že je přečtená
 **/
if(isset($_REQUEST['readNotification']))
{
    // nastavi zadane notifikaci, ze je prectena
    Notifications::setNotificationsRead($logged_user->id, $_REQUEST['readNotification']);

    // nacteme novy pocet notifikaci
    $logged_user->getNoticesCount(true);

    updateLoggedUser($_project, $logged_user);
}


if(!empty($_POST['wallSetFilter']) && isset($_POST['filter']))
{
    // ulozime do session, kdyby nahodou vlezl na jinou stranku, at to nemusi znovu vyplnovat
    if(isset($_POST['filter']))
    {
        $_project['session']->set('filter', $_POST['filter']);
    }

    $filter = $_project['session']->get('filter');

    if(!is_array($filter))
    {
        $filter = array();
    }

    $wall = $logged_user->getWall(0, true, $filter);

    $return = array(
        'status' => 'ok',
        'response' => getWall($wall, false, true)
    );
    echo json_encode($return);
    die;
}


/**
 * Vrátí nové notifikace
 **/
if(isset($_REQUEST['getNotifications']))
{
    // všechny nepřečtené notifikace
    $all = Notifications::getAllUnreadNotificationsForUser($logged_user->id);

    $gnot = new Notifications();
    $types = $gnot->getTypes();
    $alert = '';
    $time = time() - (NOTIFICATIONS_TIME_LIMIT/1000);
    $messagesCount = 0;

    foreach($all as $key => $notification)
    {
        if($notification->type_id == 'NEW-PERSONAL-MESSAGE')
        {
            $messagesCount++;
        }

        // pouze nové chceme zobrazovat
        if($notification->timestamp > $time)
        {
            if(!isset($types[$notification->type_id]))
            {
                continue;
            }
            $notification->text = $types[$notification->type_id];
            $notification->replace_patterns(true);
            $alert .= '<p class="notificationAlert" data-id="'.$notification->object_id.'">'.$notification->text.'</p>';
        }
    }

    //nacetme filter na nastence
    $filter = $_project['session']->get('filter');

    $list = array();
    $show_last = false;
    if(!empty($_REQUEST['last_news']))
    {
        // nove prispevky na zdi
        $last = str_replace('T', ' ', substr($_REQUEST['last_news'],0,19));
        $list = $logged_user->getWall(0, true, $filter, false, $last);
        $show_last = true;
    }

    $return = array(
        'response'  => count($list) && $show_last ? getWall($list, false, $logged_user) : '',
        'status' => 'ok',
        'count'  => count($all) - $messagesCount,
        'messageCount' => $logged_user->getMessagesCount(true),
        'alert'  => $alert
    );

    // nacteme novy pocet notifikaci
    $logged_user->setNoticesCount($return['count']);
    $logged_user->setMessagesCount($return['messageCount']);

    updateLoggedUser($_project, $logged_user);

    echo json_encode($return);
    die;
}


/**
 * Úprava soupisky
 **/
if(isset($_REQUEST['edit_roster']) && $_REQUEST['edit_roster'] == 1 && isset($_REQUEST['player']) && is_array($_REQUEST['player']) && intval($_REQUEST['team_id']) > 0)
{
    $return = array(
        'status' => 'error',
        'message' => $tr->tr('Nastala chyba při úpravě soupisky. Zkuste to prosím znovu později.')
    );

    $team = TeamDAO::get(intval($_REQUEST['team_id']));

    // kontrola zda muze uzivatel menit tento tym
    if($team instanceof Team && $team->userCanChangeRoster($logged_user))
    {
        // uložení do DB
        if(Rosters::updateRoster($_REQUEST['player'], $team))
        {
            $deleted = array();
            $added   = array();
            // projdeme vsechny smazane hrace a hodime o tom info na nastenku
            foreach($_REQUEST['player'] as $key => $player)
            {
                // potrebujeme jenom id
                $key = explode('-', $key);

                // pokud byl hrac smazan, tak si ho ulozime pro pozdejsi zpracovani
                if(!empty($player['removed']))
                {
                    $deleted[$key[0]] = $key[0];
                }

                // pokud byl hrac pridan, tak si ho ulozime pro pozdejsi zpracovani
                if(!empty($player['added']))
                {
                    $added[$key[0]] = $key[0];
                }
            }

            // mame nejake smazane?
            if(count($deleted))
            {
                $data = WallDAO::getLastTypeObject('delete', $team->id);

                // pokud uz tento zaznam existuje, tak ho updatujeme
                if($data)
                {
                    $data['object_value'] = unserialize($data['object_value']);

                    foreach($deleted as $del)
                    {
                        // ulozime pouze pokud uz to tam neni
                        if(!isset($data['object_value']['users'][$del]))
                        {
                            $data['object_value']['users'][$del] = $del;
                        }
                    }
                    $data['object_value'] = serialize($data['object_value']);
                    $data['object_date_begin'] = date('Y-m-d H:i:s');

                    WallDAO::updateTypeItem($data);
                }
                else
                {
                    $data = array(
                        'object_parent_id'     => 0,
                        'object_source_id'     => 0,
                        'object_user_id'       => $logged_user->id,
                        'object_team_id'       => $team->id,
                        'object_oponent_id'    => 0,
                        'object_type'          => 'roster-delete',
                        'object_date_created'  => date('Y-m-d H:i:s'),
                        'object_date_begin'    => date('Y-m-d H:i:s'),
                        'object_value'         => serialize(array('users' => $deleted))
                    );

                    WallDAO::insertTypeItem($data);
                }
            }

            // mame nejake pridane?
            if(count($added))
            {
                $data = WallDAO::getLastTypeObject('insert', $team->id);

                if($data)
                {
                    $data['object_value'] = unserialize($data['object_value']);

                    foreach($added as $del)
                    {
                        // ulozime pouze pokud uz to tam neni
                        if(!isset($data['object_value']['users'][$del]))
                        {
                            $data['object_value']['users'][$del] = $del;
                        }
                    }
                    $data['object_value'] = serialize($data['object_value']);
                    $data['object_date_begin'] = date('Y-m-d H:i:s');

                    WallDAO::updateTypeItem($data);
                }
                else
                {
                    $data = array(
                        'object_parent_id'     => 0,
                        'object_source_id'     => 0,
                        'object_user_id'       => $logged_user->id,
                        'object_team_id'       => $team->id,
                        'object_oponent_id'    => 0,
                        'object_type'          => 'roster-insert',
                        'object_date_created'  => date('Y-m-d H:i:s'),
                        'object_date_begin'    => date('Y-m-d H:i:s'),
                        'object_value'         => serialize(array('users' => $added))
                    );

                    WallDAO::insertTypeItem($data);
                }
            }

            $return = array(
                'status' => 'ok',
                'message' => $tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Soupiska vašeho týmu byla úspěšně upravena.</p>')
            );

            $return['response'] = '<div class="contentAbox">';

            // vrátíme HTML pro soupisku
            $positions = SportDAO::getPositions($team->sport_id);
            $roster = Rosters::getTeamRoster($team->id);
            $position_select = '';
            foreach($positions as $position)
            {
                $position_select .= '<option value="'.$position->id.'"'.(!empty($position->staff) ? '  data-staff="1"' : '') .'>'.$position->name.'</option>';
                if(isset($roster[$position->id]))
                {
                   $players = $roster[$position->id];
                }
                else
                {
                    $players = array();
                }

                $showAll = false;

                if($position->staff != 1)
                {
                    $return['response'] .= printPositionRoster($position, $players, $showAll);
                }
                else
                {
                    $return['response'] .= printStaffRoster($position, $players, $showAll);
                }
            }

            $return['response'] .= '</div>';

            $return['editRoster'] = printEditRoster($positions, $roster, $position_select, $team);

        }
    }

    echo json_encode($return);
    die;
}


/**
 * rychle pozvani do tymu
 * !!! MUSI ZUSTAT PRIMO PRED PRIDANIM HRACE NA SOUPISKU !!!
*/
if(!empty($_REQUEST['add_player_quick']) && !empty($_REQUEST['team_id']))
{
    $user = explode('-', $_REQUEST['add_player_quick']);
    if($user[1] != getRecipientHash($user[0]))
    {
        $return['message'] = $tr->tr('Nastala chyba při posílání pozvánky.');

        echo json_encode($return);
        die;
    }

    $player = UserDAO::get($user[0]);

    if($player instanceof User)
    {
        $_REQUEST = array(
            'add_player' => 1,
            'add_player_quick' => 1,
            'player'     => array(
                'email'   => $player->email,
                'team_id' => $_REQUEST['team_id']
            ),
            'search'    => intval(@$_REQUEST['search'])
        );
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při posílání pozvánky.');

        echo json_encode($return);
        die;
    }

    unset($user);
    unset($player);
}


/**
 * pridani hostujiciho hrace do tymu
 * MUSI ZUSTAT PRED PRIDANIM HRACE NA SOUPISKU
 */
if(isset($_REQUEST['add_host_player']) && $_REQUEST['add_host_player'] == 1)
{
    $return = array(
        'status'  => 'error',
        'message' => $tr->tr('Nastala chyba při přidávání hráče.')
    );

    // musime mit i ID tymu
    if(!empty($_REQUEST['host_player']['team_id']) && $_REQUEST['host_player']['team_id'] == $logged_user->getActiveTeam()->id)
    {
        // pokud máme email
        if(!empty($_REQUEST['host_player']['email']) && empty($_REQUEST['host_player']['name']) && empty($_REQUEST['host_player']['surname']))
        {
            $_REQUEST = array(
                'host_player' => 1,
                'add_player' => 1,
                'player'     => array(
                    'email'   => $_REQUEST['host_player']['email'],
                    'team_id' => $_REQUEST['host_player']['team_id'],
                    'host'    => 1
                )
            );
        }
        // jinak musime mit jmeno a prijmeni
        elseif(!empty($_REQUEST['host_player']['name']) && !empty($_REQUEST['host_player']['surname']) && empty($_REQUEST['host_player']['email']))
        {
            // info o tymu, do ktereho pridavame hrace
            $team = TeamDAO::get($_REQUEST['host_player']['team_id']);

            if($team->userCanChangeRoster($logged_user))
            {
                $data = array(
                    'fname'   => $_REQUEST['host_player']['name'],
                    'sname'   => $_REQUEST['host_player']['surname'],
                    'key'     => friendly_url($_REQUEST['host_player']['name'] . '-' . $_REQUEST['host_player']['surname']),
                    'team'    => $team->id,
                    'sport'   => $team->sport_id
                );
                $user = new User($data);
                $user->save();

                // pridame ho do tymu (id, admin, hrac, aktivni, host)
                if($team->addUser($user->id, false, true, true, true))
                {
                    $return = array(
                        'status'  => 'host-add-ok',
                        'message' => $tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Uživatel byl v pořádku přidán na soupisku vašeho týmu do skupiny "Nezařazení".</p>'),
                    );
                    $return['response'] = '<div class="contentAbox">';

                    // vrátíme HTML pro soupisku
                    $positions = SportDAO::getPositions($team->sport_id);
                    $roster = Rosters::getTeamRoster($team->id);
                    $position_select = '';
                    foreach($positions as $position)
                    {
                        $position_select .= '<option value="'.$position->id.'">'.$position->name.'</option>';
                        if(isset($roster[$position->id]))
                        {
                           $players = $roster[$position->id];
                        }
                        else
                        {
                            $players = array();
                        }

                        if($position->staff != 1)
                        {
                            $return['response'] .= printPositionRoster($position, $players);
                        }
                        else
                        {
                            $return['response'] .= printStaffRoster($position, $players);
                        }
                    }

                    $return['response'] .= '</div>';
                    $return['editRoster'] = printEditRoster($positions, $roster, $position_select, $team);
                }

                echo json_encode($return);
                die;
            }
            else
            {
                echo json_encode($return);
                die;
            }
        }
        else
        {
            echo json_encode($return);
            die;
        }
    }
    else
    {
        echo json_encode($return);
        die;
    }
}


/**
 * Pozvani hrace do tymu
 **/
if(isset($_REQUEST['add_player']) && $_REQUEST['add_player'] == 1 && !empty($_REQUEST['player']['email']) && !empty($_REQUEST['player']['team_id']))
{
    $return = array(
        'status'    => 'error',
        'message'   => 'Nastala chyba při posílání pozvánky.' . ' #1037'
    );
    // FIXME jestli opravdu email je email
    // FIXME - jestli uz uzivatel neni do týmu pozván, kdyztak update notifikace

    $time = time();
    $team = TeamDAO::get($_REQUEST['player']['team_id']);

    if($team instanceof Team && $team->id > 0 && $team->userCanChangeRoster($logged_user))
    {
        // uživatel ktereho chceme pozvat do týmu
        $user = UserDAO::getByEmail($_REQUEST['player']['email']);

        // jeslize uz je uzivatel v týmu, tak ho přece nebudeme zvát znovu
        if($user instanceof User && $user->id > 0 && $team->isPlayer($user))
        {
            $return = array(
                'status'  => 'error',
                'message' => $tr->tr('Uživatel je již členem týmu. Pozvánka nebyla odeslána.')
            );
        }
        // uživatel v týmu ještě není
        else
        {
            // abychom si mohli vytáhnout starou pozvánku
            $data = array(
                'user_id'   => $user instanceof User && $user->id > 0 ? $user->id : 0,
                'team_id'   => $team->id,
                'email'     => $_REQUEST['player']['email']
            );

            // vytvoreni kontrolniho hashe, pro prijeti nebo odmitnuti pozvanky
            $hash = getInvitationHash($team->id, $logged_user->id, $data['user_id']);

            // kouknem jestli uz nahodou neni pozvanka tam
            $oldinvite = RosterInvites::getInviteByTeam($data);

            // pozvanka uz existuje ale uzivatel ji odmitnul, nebo vubec neexistuje jeste, tak ho muzeme pozvat znovu
            if((count($oldinvite) > 0 && $oldinvite['status'] == 'declined') || count($oldinvite) == 0)
            {
                // pozvánka data
                $invite = array(
                    'user_id'   => $user instanceof User && $user->id > 0 ? $user->id : 0,
                    'is_host'   => isset($_REQUEST['host_player']) ? '1' : '0',
                    'logged_user_id'   => $logged_user->id,
                    'team_id'   => $team->id,
                    'email'     => $_REQUEST['player']['email'],
                    'invited'   => $time,
                    'hash'      => $hash
                );

                // ulozime samotnou pozvánku do DB
                $invite_id = RosterInvites::insert($invite);
                if($invite_id)
                {
                    // odesleme email o pozvance
                    if(RosterInvites::sendInvitation($_REQUEST['player']['email'], $team, $logged_user, $invite['user_id']))
                    {
                    	if(empty($_REQUEST['add_player_quick']))
                    	{
                            $return = array(
                                'status'  => 'ok',
                                'message' => $tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Na e-mailovou adresu') . ' ' . strip_tags($_REQUEST['player']['email']) . ' ' . $tr->tr('byla odeslána pozvánka pro vstup do vašeho týmu.</p>')
                            );
                        }
                        else
                        {
                            // mame pozvanku ve vyhledavani
                            if(!empty($_REQUEST['search']))
                            {
                                $return = array(
                                    'status'  => 'ok',
                                    'response' => print_invite_leave_search($team, $user)
                                );
                            }
                            else
                            {
	                        	$return = array(
	                                'status'  => 'ok',
	                                'response' => print_invite_leave($team, $user)
	                            );
                            }
                        }

                        // pokud je to existujici uzivatel tak mu posleme notifikaci
                        if($user instanceof User && $user->id > 0)
                        {
                            $data = array();

                            // info, které si uložíme
                            // přihlášený uzivatel kvuli notifikaci
                            // team kvuli odkazu na tym
                            $array = array(
                                'team'          => $team->id,
                                'logged_user'   => $logged_user->id,
                                'invited_user'  => $user->id,
                                'invite_id'     => $invite_id
                            );

                            // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                            $data[] = array(
                                'user_id'    => $user->id,
                                'object_id'  => $team->id,
                                'type_id'    => 'TEAM-INVITATION',
                                'info'       => serialize($array),
                                'object_info'=> '',
                                'timestamp'  => $time,
                                'read'       => '0'
                            );


                            // pokud existuje uzivatel, tak vložíme ještě notifikaci
                            if(!Notifications::insertNotifications($data))
                            {
                                // musime odebrati tu pozvanku, aby ho mohl zkusit pozvat znovu
                                $invite = array(
                                    'id'        => $invite_id,
                                    'user_id'   => $array['invited_user']
                                );
                                RosterInvites::deleteInvite($invite);

                                $return['status'] = 'error';
                                $return['message'] = $tr->tr('Nastala chyba při posílání pozvánky.') . ' error code: #100001';
                            }
                        }
                    }
                }
            }
            // uživatel již byl pozván a čekáme až odpoví
            else
            {
                $return = array(
                    'status'  => 'error',
                    'message' => $tr->tr('Uživatel již byl do týmu pozván.')
                );
            }
        }
    }

    echo json_encode($return);
    die;
}


/**
 * zruseni pozvanky do tymu
*/
if(!empty($_REQUEST['remove_player_quick']) && !empty($_REQUEST['team_id']))
{
    $user = explode('-', $_REQUEST['remove_player_quick']);
    if($user[1] != getRecipientHash($user[0]))
    {
        $return['message'] = $tr->tr('Nastala chyba při rušení pozvánky do týmu.');

        echo json_encode($return);
        die;
    }

    $player = UserDAO::get($user[0]);

    if($player instanceof User)
    {
        $team = TeamDAO::get($_REQUEST['team_id']);

        // zjistime zda tym existuje a jestli ma prihlaseny uzivatel prava k editaci
        if($team instanceof Team && !empty($team->id) && $team->isAdmin($logged_user))
        {
            // abychom si mohli vytáhnout starou pozvánku
            $data = array(
                'user_id'   => $player->id,
                'team_id'   => $team->id
            );

            // kouknem jestli uz nahodou neni pozvanka tam
            $oldinvite = RosterInvites::getInviteByTeam($data);

            // pokud pozvanka existuje, tak ji muzeme zrusit
            if(is_array($oldinvite) && count($oldinvite) > 0)
            {
                //smazeme
                if(RosterInvites::deleteInvite($oldinvite))
                {
                    // smazeme starou notifikaci
                    Notifications::deleteNotifications($player->id, $team->id, array('TEAM-INVITATION', 'CANCEL-TEAM-INVITATION'));

                    // posleme mu notifikaci o zruseni
                    $notification_data = array();
                    $array = array(
                        'user_id'    => $logged_user->id,
                        'logged_user'=> $logged_user->id,
                        'team_id'    => $team->id
                    );
                    $time = time();
                    $notification_data[] = array(
                        'user_id'    => $player->id,
                        'object_id'  => $team->id,
                        'type_id'    => 'CANCEL-TEAM-INVITATION',
                        'info'       => serialize($array),
                        'object_info'=> '',
                        'timestamp'  => $time,
                        'read'       => '0'
                    );

                    // nepotřebujeme ošetřovat, protože přinejhorším jen neodejde notifikace
                    Notifications::insertNotifications($notification_data);

                    if(!empty($_REQUEST['search']))
                    {
                        // vratime data
                        $return = array(
                            'status'   => 'ok',
                            'response' => print_invite_user_search($team, $player)
                        );
                    }
                    else
                    {
                        $return = array(
                            'status'  => 'ok',
                            'response' => print_invite_user($team, $player)
                        );
                    }

                    echo json_encode($return);
                    die;
                }
                else
                {
                    $return['message'] = $tr->tr('Nastala chyba při rušení pozvánky do týmu.');

                    echo json_encode($return);
                    die;
                }
            }
            else
            {
                $return['message'] = $tr->tr('Nastala chyba při rušení pozvánky do týmu.');

                echo json_encode($return);
                die;
            }
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při rušení pozvánky do týmu.');

            echo json_encode($return);
            die;
        }
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při rušení pozvánky do týmu.');

        echo json_encode($return);
        die;
    }

    unset($user);
    unset($player);
}


/**
 * Přijmutí a odmítnutí pozvánky do týmu
 **/
if(isset($_REQUEST['confirmInvitation']) || isset($_REQUEST['declineInvitation']))
{
    $return = array(
        'status'  => 'error',
        'message' => 'Nastala chyba při práci s pozvánkou. Zkuste to prosím znovu později.'
    );

    //$info['team']->id, $info['logged_user']->id, $info['invited_user']->id
    $hash = getInvitationHash($_REQUEST['team_id'], $_REQUEST['logged_user_id'], $_REQUEST['invited_user_id']);

    // nejsou podvrzena data
    if($hash == $_REQUEST['invitation_hash'])
    {
        $team = TeamDAO::get($_REQUEST['team_id']);

        // abychom ho mohli pridat, musi existovat team
        // zaroven pozvani muze prijmout pouze prihlaseny uzivatel - neboli id nyni prihlaseneho je stejne jako id pozvaneho do tymu
        if($team instanceof Team && $team->id > 0 && $logged_user->id == $_REQUEST['invited_user_id'])
        {
            // podle toho budem urcovat spravnou pozvanku
            $data = array(
                'user_id' => $_REQUEST['invited_user_id'],
                'id' => $_REQUEST['invite_id']
            );

            // nacteme si pozvanku
            $oldinvite = RosterInvites::getInvite($data);

            // info, které si uložíme
            // přihlášený uzivatel kvuli notifikaci
            // team kvuli odkazu na tym
            $array = array(
                'team'          => $team->id,
                'logged_user'   => $logged_user->id,
                'invited_user'  => $data['user_id'],
                'invite_id'     => intval($_REQUEST['invite_id'])
            );

            // nacteme vsechny adminy a posleme jim pak info o prijmuti/odmitnuti pozvanky
            $admins = $team->getAdmins(true);

            // aktualni cas
            $time = time();

            // přijmutí pozvánky
            if(isset($_REQUEST['confirmInvitation']))
            {
                // vytahneme uzivatele
                $user = UserDAO::get($logged_user->id);

                // jestli uz uzivatel nahodou neni v tymu pridan
                if(!$team->isPlayer($user) && !$team->isAdmin($user))
                {
                    // pridame do tymu addUser($user_id, $is_admin, $is_player, $is_active)
                    // v metode addUSer se i osetri, jestli uz ten uzivatel neni treba fanousek a tak
                    if($team->addUser($_REQUEST['invited_user_id'], 0, 1, 0, intval(@$oldinvite['is_host'])))
                    {

                        // pokud je to prvni tym, do ktereho jsem vstoupil, tak ho nastavim jako hlavni tym
                        if(empty($logged_user->team))
                        {
                            $logged_user->team = $team->id;
                        }

                        $logged_user->setActiveTeam($team);
                        $logged_user->getTeamList(true);
                        $logged_user->save();

                        updateLoggedUser($_project, $logged_user);

                        if(RosterInvites::updateInvite($data, 'confirmed'))
                        {
                            $data = array();

                            // musime jim vsem poslat notifikace
                            foreach($admins as $user_id)
                            {
                                // logged_user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                                $data[] = array(
                                    'user_id'    => $user_id,
                                    'object_id'  => $team->id,
                                    'type_id'    => 'CONFIRM-TEAM-INVITATION',
                                    'info'       => serialize($array),
                                    'object_info'=> '',
                                    'timestamp'  => $time,
                                    'read'       => '0'
                                );
                            }

                            // nepotřebujeme ošetřovat, protože přinejhorším jen neodejde notifikace
                            Notifications::insertNotifications($data);

                            // navratova hodnota
                            $return = array(
                                'status'  => 'confirm',
                                'messageConfirm' => '<span class="confirm">'.$tr->tr('Přijato!').'</span>',
                                'messageConfirmBox' => '<div class="msg noClose done"><h3 class="title">Hotovo!</h3><p class="text">'.$tr->tr('Úspěšně jste vstoupili do týmu').' <strong>'.$team->getName().'.</strong></div>'
                            );
                        }
                    }
                    else
                    {
                        $return['message'] = $tr->tr('Nastala chyba při potvrzení pozvánky. Zkuste to prosím znovu později.');
                    }
                }
                else
                {
                    $return['message'] = $tr->tr('Již jste členem tohoto týmu.');
                }
            }
            // odmítnutí pozvánky
            elseif(isset($_REQUEST['declineInvitation']))
            {
                // jestli uz uzivatel nahodou neni v tymu pridan
                if(!$team->isPlayer($logged_user) && !$team->isAdmin($logged_user))
                {
                    RosterInvites::updateInvite($data, 'declined');

                    $data = array();

                    // musime jim vsem poslat notifikace
                    foreach($admins as $user_id)
                    {
                        // logged_user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                        $data[] = array(
                            'user_id'    => $user_id,
                            'object_id'  => $team->id,
                            'type_id'    => 'DECLINE-TEAM-INVITATION',
                            'info'       => serialize($array),
                            'object_info'=> '',
                            'timestamp'  => $time,
                            'read'       => '0'
                        );
                    }

                    // nepotřebujeme ošetřovat, protože přinejhorším jen neodejde notifikace
                    Notifications::insertNotifications($data);

                    // navraotva hodnota
                    $return = array(
                        'status'  => 'decline',
                        'messageDecline' => '<span class="decline">'.$tr->tr('Odmítnuto!').'</span>',
                        'messageDeclineBox' => '<div class="msg noClose done"><h3 class="title">Hotovo!</h3><p class="text">'.$tr->tr('Odmítli jste vstoupit do týmu').' <strong>'.$team->getName().'.</strong></p></div>'
                    );
                }
                else
                {
                    // navratova hodnota
                    $return = array(
                        'status'  => 'error',
                        'message' => $tr->tr('Již jste členem tohoto týmu.'),
                        'messageConfirm' => '<span class="confirm">'.$tr->tr('Přijato!').'</span>',
                        'messageConfirmBox' => '<div class="msg noClose done"><h3 class="title">Hotovo!</h3><p class="text">'.$tr->tr('Úspěšně jste vstoupili do týmu').' <strong>'.$team->getName().'.</strong></div>'
                    );
                }
            }

            // musime nacist aktulni
            $logged_user->getTeamList(true);
            updateLoggedUser($_project, $logged_user);
        }
        else
        {
            $return['message'] = $tr->tr('Tato pozvánka již není platná.');
        }
    }
    else
    {
        $return['message'] = $tr->tr('Kontrola dat neproběhla v pořádku.');
    }

    echo json_encode($return);
    die;
}



/**
 * Přijmutí a odmítnutí pozvánky do týmu
 **/
if(isset($_REQUEST['confirmRequest']) || isset($_REQUEST['declineRequest']))
{
    $return = array(
        'status'  => 'error',
        'message' => 'Nastala chyba při práci s žádostí. Zkuste to prosím znovu později.'
    );

    //$info['team']->id, $info['logged_user']->id, $info['invited_user']->id
    $hash = getRequestHash($_REQUEST['team_id'], $_REQUEST['logged_user_id']);

    // nejsou podvrzena data
    if($hash == $_REQUEST['request_hash'])
    {
        $team = TeamDAO::get($_REQUEST['team_id']);
        $user = UserDAO::get($_REQUEST['logged_user_id']);

        // abychom ho mohli pridat, musi existovat team
        // zaroven pozvani muze prijmout pouze admin tymu
        if($team instanceof Team && $team->id > 0 && $team->isAdmin($logged_user))
        {
            // podle toho budem urcovat spravnou pozvanku
            $data = array(
                'user_id' => $_REQUEST['logged_user_id'],
                'id' => $_REQUEST['request_id']
            );

            // nacteme si pozvanku
            $oldinvite = RosterInvites::getRequest($data);

            // info, které si uložíme
            // přihlášený uzivatel kvuli notifikaci
            // team kvuli odkazu na tym
            $array = array(
                'team'           => $team->id,
                'logged_user'    => $logged_user->id
            );

            // aktualni cas
            $time = time();

            // přijmutí pozvánky
            if(isset($_REQUEST['confirmRequest']))
            {
                // pridame do tymu addUser($user_id, $is_admin, $is_player, $is_active)
                // v metode addUSer se i osetri, jestli uz ten uzivatel neni treba fanousek a tak
                if($team->addUser($_REQUEST['logged_user_id'], 0, 1, 0, 0))
                {
                    SendMail::sendConfirmRequest($user->email, $team, $logged_user);

                    if($user instanceof User)
                    {
                        if(empty($user->team))
                        {
                            $user->team = $team->id;
                        }
                        $user->setActiveTeam($team);
                        $user->save();
                    }

                    if(RosterInvites::updateRequest($data, 'confirmed'))
                    {
                        $data = array();

                        // logged_user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                        $data[] = array(
                            'user_id'    => $_REQUEST['logged_user_id'],
                            'object_id'  => $team->id,
                            'type_id'    => 'CONFIRM-TEAM-ENTRY-REQUEST',
                            'info'       => serialize($array),
                            'object_info'=> '',
                            'timestamp'  => $time,
                            'read'       => '0'
                        );

                        // nepotřebujeme ošetřovat, protože přinejhorším jen neodejde notifikace
                        Notifications::insertNotifications($data);

                        // navratova hodnota
                        $return = array(
                            'status'  => 'confirm',
                            'messageConfirm' => '<span class="confirm">'.$tr->tr('Přijato!').'</span>',
                            'messageConfirmBox' => '<div class="msg noClose done"><h3 class="title">Hotovo!</h3><p class="text">'.$tr->tr('Úspěšně jste přijali uživatele <strong>'.$user->getName().'</strong> do vašeho týmu ').' <strong>'.$team->getName().'.</strong></div>'
                        );
                    }
                }
                else
                {
                    $return['message'] = $tr->tr('Nastala chyba při potvrzení žádosti. Zkuste to prosím znovu později.');
                }
            }
            // odmítnutí pozvánky
            elseif(isset($_REQUEST['declineRequest']))
            {
                RosterInvites::updateRequest($data, 'declined');

                $data = array();
                $data[] = array(
                    'user_id'    => $_REQUEST['logged_user_id'],
                    'object_id'  => $team->id,
                    'type_id'    => 'DECLINE-TEAM-ENTRY-REQUEST',
                    'info'       => serialize($array),
                    'object_info'=> '',
                    'timestamp'  => $time,
                    'read'       => '0'
                );

                // nepotřebujeme ošetřovat, protože přinejhorším jen neodejde notifikace
                Notifications::insertNotifications($data);

                // navraotva hodnota
                $return = array(
                    'status'  => 'decline',
                    'messageDecline' => '<span class="decline">'.$tr->tr('Zamítnuto!').'</span>',
                    'messageDeclineBox' => '<div class="msg noClose done"><h3 class="title">Hotovo!</h3><p class="text">'.$tr->tr('Odmítli jste žádost uživatele <strong>'.$user->getName().'</strong> o vstup do týmu').' <strong>'.$team->getName().'.</strong></p></div>'
                );
            }
        }
        else
        {
            $return['message'] = $tr->tr('Tato žádost již není platná.');
        }
    }
    else
    {
        $return['message'] = $tr->tr('Kontrola dat neproběhla v pořádku.');
    }

    echo json_encode($return);
    die;
}



/**
 * Odešle soukromou zprávu
 **/
if(isset($_REQUEST['pm']))
{
    $return['message'] = $tr->tr('Nastala chyba při posílání zprávy. Zkuste to prosím později znovu.') . ' #error 1197';
    $ok = true;

    // ošetření vstupu
    $_REQUEST['pm']['text'] = strip_tags($_REQUEST['pm']['text']);

    // pokud nemáme poslaný ID konverzace, tak je to nová zpráva
    if(!isset($_REQUEST['pm']['conversation_id']))
    {
        // nejprve teda zkontrolujeme zda jsou příjemci správně poslaní
        if(!is_array($_REQUEST['pm']['to']))
        {
            $return['message'] = $tr->tr('Vyberte příjemce svojí zprávy.') . ' error #1222';

            $ok = false;
        }

        // projdeme všechny uživatele a zkontrolujeme zda sedí hashe
        foreach($_REQUEST['pm']['to'] as $val)
        {
            $split = explode('-', $val);

            // odelsna hodnota je spatne - neni v ni pomlcka, nebo je tam pomlcek moc
            // hodnota se posila ve tvaru "userid-hash"
            if(count($split) != 2)
            {
                $ok = false;
                break;
            }

            // nesedí hash, posleme ho do haje a zpravy neposleme
            if($split[1] != getRecipientHash($split[0]))
            {
                $ok = false;
                break;
            }

            // nacteme uzivatele a zkontrolujeme zda se mu muze posilat zprava
            $user = UserDAO::get($split[0]);
            if(!$user instanceof User || empty($user->id) || $logged_user->id == $user->id || !$user->canReceivePm($logged_user))
            {
                $ok = false;
                break;
            }

            // pole příjemců
            $users[] = $split[0];
        }

        if($ok)
        {
            $users[] = $logged_user->id;
            sort($users);
            $implodeUsers = '-'.implode('-', $users).'-';

            // podle příjemců se pokusíme najít v databázi existující konverzaci
            // pokud ji to nenajde, tak metoda rovnou vrací nové ID
            $conversation = PrivateMessages::getConversationId($implodeUsers, $logged_user->id);

            $_REQUEST['conversation_id'] = $conversation['id'];

            // pokud konverzace jeste neexistuje, tak se ji pokusime vlozit
            if($conversation['type'] == 'new' && empty($conversation['subtype']))
            {
                // uložíme do DB
                if(!PrivateMessages::insertConversation($conversation['id'], $implodeUsers))
                {
                    $return['message'] = $tr->tr('Nastala chyba při posílání zprávy. Zkuste to prosím později znovu.') . ' #error 1255';

                    $ok = false;
                }
            }
        }
    }
    // máme poslaný conversation_id -> měla by to tdy být reakce
    else
    {
        // nacteme si uzivatele
        // metoda zaroven kontroluje, zda je uzivatel v konverzaci a muze do ni teda odpovidat
        $implodeUsers = PrivateMessages::getConversationUsers($_REQUEST['pm']['conversation_id'], $logged_user->id);

        if($implodeUsers === false)
        {
            $return['message'] = $tr->tr('Nastala chyba při posílání zprávy. Zkuste to prosím později znovu.') . ' #error 1270';

            $ok = false;
        }
        else
        {
            // abyhcom meli uzivaele i v poli
            $users = explode('-', substr($implodeUsers, 1, -1));

            // podle příjemců se pokusíme najít v databázi existující konverzaci
            $conversation = PrivateMessages::getConversationId($implodeUsers, $logged_user->id);
        }
    }

    // probehlo predtim vse v pořádku?
    if($ok)
    {
        $data = array();
        foreach($users as $user_id)
        {
            $user = UserDAO::get($user_id);
            if(!$user->canReceivePm($logged_user))
            {
                continue;
            }
            // FIXME - kontrola zda uzivatel ma povoleno prijimani zprav

            if($user_id == $logged_user->id)
            {
                $read = 1;
            }
            else
            {
                $read = 0;
            }

            $data[] = array(
                'id'                => PrivateMessages::getMax($conversation['id'], $user_id),
                'user_id'           => $user_id, // komu je urcena
                'sent_user_id'      => $logged_user->id, // kdo zpravu napsal
                'conversation_id'   => $conversation['id'],
                'users'             => $implodeUsers,
                'text'              => $_REQUEST['pm']['text'],
                'read'              => $read
            );
        }

        // uložíme zprávy
        PrivateMessages::insertMessages($data);

        // vsechny notifikace, ktere jiz jsou k tomuto obejktu
        $notifications = Notifications::getNotificationsByObject($implodeUsers);

        // aktualne prihlaseny uzivatel
        $pom = $logged_user->getAllData();

        // pro ukladani notifikaci
        $data1 = array();

        // aktualni cas
        $time = time();

        // a pro všechny ostatní uděláme notifikace
        foreach($users as $user_id)
        {
            // pro prihlaseneho uzivatele prece nebudu davat notifikaci
            if($user_id != $logged_user->id)
            {
                /*
                // U ZPRAVY ZATIM NEJDOU VYPNOUT NOTIFIKACE
                // jestlize uzivatel nema nastaveno posilani notifikaci, tak skocime na dalsiho
                if($user->settings[UserSettings::KEY_PUBLIC_COMMENT] == UserSettings::VAL_NOTICE_NO)
                {
                    continue;
                }
                */

                // nacteme si uzivatele
                $user = UserDAO::get($user_id);

                // virtualni objekt id
                $object_id = $implodeUsers;

                // klic notifikace
                $key = $user->id . '-' . $object_id;

                // notifikace již existuje, takze ji musime updatovat
                if(in_array($key, array_keys($notifications)))
                {
                    // nacteme si existujici notifikaci
                    $notification = $notifications[$key];

                    // vrati nam seznam vsech uzivatelu, kterych se to tyka
                    $uns = unserialize($notification->info);

                    // odstranime ho z uzivatelu,abychom ho pridali zase na konec
                    unset($uns[$pom['id']]);

                    // pridame prihlaseneho uzivatele
                    $uns[$pom['id']] = $pom;

                    // opet ulozime
                    $notification->info = serialize($uns);

                    // jestlize uz byla prectena tak ji zobrazime znovu
                    if($notification->read == 1)
                    {
                        // zmenime cas
                        $notification->timestamp = $time;

                        // a nastavime jako neprectenou
                        $notification->read = 0;
                    }

                    // a notifikaci ulozime i v DB
                    Notifications::updateNotification($notification);
                }
                // vložíme novou notifikaci
                else
                {


                    // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                    $data1[] = array(
                        'user_id'    => $user->id,
                        'object_id'  => $implodeUsers,
                        'type_id'    => 'NEW-PERSONAL-MESSAGE',
                        'info'       => serialize(
                                            array(
                                                'users' => array(
                                                    $pom['id'] => $pom['id']
                                                ))),
                        'object_info'=> '',
                        'timestamp'  => $time,
                        'read'       => '0'
                    );
                }
            }
        }

        // uložíme do DB nové notifikace
        Notifications::insertNotifications($data1);

        // pokud je to nová konverzace, tak vrátíme celý div se zprávami
        if($conversation['type'] == 'new')
        {
            $detail = PrivateMessages::getConversationDetail($conversation['id'], $logged_user->id);

            // nacteme si vsechny prijemce v konverzaci
            foreach($users as $user_id)
            {
                if($user_id == $logged_user->id)
                {
                    continue;
                }
                $mainUser = UserDAO::get($user_id);
                if(!empty($mainUser->team))
                {
                    $userTeam = TeamDAO::get($mainUser->team);
                }
                else
                {
                    $userTeam = false;
                }

                $userArray[] = array(
                    'user' => $mainUser,
                    'team' => $userTeam
                );
            }
            $message = printMessageBox($detail, $userArray);
        }
        else
        {
            $pm = new PrivateMessage();
            $pm->time = date('Y-m-d H:i:s', $time);
            $pm->text = $_REQUEST['pm']['text'];
            $message = printMessage(' sent last', $pm, $logged_user);
        }

        $return = array(
            'status'    => 'ok',
            'id'        => 'conversation-'.str_replace('.','',$conversation['id']),
            'type'      => $conversation['type'],
            'response'  => $message,
            'message'   => '<div class="msg noClose done">'.$tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Zpráva byla v pořádku odeslána.</p>').'</div>'
        );
    }

    echo json_encode($return);
    die;
}


/**
 * Nastaví konverzaci jako přečtenou
 **/
if(isset($_REQUEST['readConversation']) && isset($_REQUEST['id']))
{
    $id = str_replace('conversation-', '', $_REQUEST['id']);

    if(PrivateMessages::readConversation($id, $logged_user->id))
    {
        $return = array(
            'status'       => 'ok',
            'messageCount' => $logged_user->getMessagesCount(true),
        );

        updateLoggedUser($_project, $logged_user);
    }
    else
    {
        $return['message'] = '';
    }

    echo json_encode($return);
    die;
}


/**
 * Smaže konverzaci pro přihlášeného uživatele, ostatním ve výpise zůstane
 **/
if(isset($_REQUEST['deleteConversation']) && isset($_REQUEST['id']))
{
    $id = str_replace('conversation-', '', $_REQUEST['id']);

    if(PrivateMessages::deleteConversation($id, $logged_user->id))
    {
        $conv = PrivateMessages::getConversations($logged_user->id);

        $return = array(
            'status'        => 'ok',
            'message'       => (count($conv) == 0 ? '<p class="msg noClose">' . $tr->tr('Nemáte žádné otevřené konverzace.') . '</p>' : ''),
            'messageCount'  => $logged_user->getMessagesCount(true),
        );

        updateLoggedUser($_project, $logged_user);
    }
    else
    {
        $return['message'] = '';
    }

    echo json_encode($return);
    die;
}



/**
 * Odebrání z konverzace
 **/
if(isset($_REQUEST['leaveConversation']) && isset($_REQUEST['id']))
{
    // vsichni projemci konverzace
    $id = str_replace('conversation-', '', $_REQUEST['id']);

    // nacteme si uzivatele
    // metoda zaroven kontroluje, zda je uzivatel v konverzaci a muze do ni teda odpovidat
    $oldusers = PrivateMessages::getConversationUsers($id, $logged_user->id);

    if($oldusers !== false)
    {
        // odebereme
        $newusers = str_replace('-'.$logged_user->id.'-', '-', $oldusers);

        // update v db
        if(PrivateMessages::updateConversation($id, $newusers))
        {
            $return['status'] = 'ok';
            $data = array();
            $users = explode('-', substr($newusers, 1, -1));
            foreach($users as $user_id)
            {
                $read = 0;

                $data[] = array(
                    'id'                => PrivateMessages::getMax($id, $user_id),
                    'user_id'           => $user_id, // komu je urcena
                    'sent_user_id'      => '0', // kdo zpravu napsal
                    'conversation_id'   => $id,
                    'users'             => $newusers,
                    'text'              => $tr->tr('Uživatel') . ' [user:'.$logged_user->id.'] ' . $tr->tr('opustil konverzaci.'),
                    'read'              => $read
                );
            }

            // uložíme zprávy
            PrivateMessages::insertMessages($data);
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při opouštění konverzace.') . ' error #1493';
        }
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při opouštění konverzace.') . ' error #1488';
    }

    echo json_encode($return);
    die;
}



/**
 * vytvoření události, načtení formuláře podle zadaného typu události
 */
if(isset($_REQUEST['manageEvent']))
{
    $return['status'] = 'ok';
    $return['response'] = '';
    include(BLOCK_PATH . 'event-manage-data.php');

    switch($_REQUEST['manageEvent'])
    {
        case Event::COMPETITION:
        case Event::FRIENDLY:
            $return['response'] .= $event_data['team'] . $event_data['button1'] ;
            $_project['session']->set('eventManageType', $_REQUEST['manageEvent']);
        break;

        case Event::TRAINING:
            $return['response'] .= $event_data['name'] . $event_data['place'] . $event_data['date'] . $event_data['attend'] . $event_data['save'];
            $_project['session']->set('eventManageType', $_REQUEST['manageEvent']);
        break;

        case 'eventUploadLogo':
            $return['response'] .= $event_data['logo'] . $event_data['invite'] . $event_data['button2'];
        break;

        case 'teamSelected':
            $return['response'] .= $event_data['name'] . $event_data['place'] . $event_data['date'] . $event_data['attend'] . $event_data['save'];
        break;

        case Event::ACTION:
            $return['response'] .= $event_data['name'] . $event_data['place'] . $event_data['date_team_action'] . $event_data['attend'] . $event_data['save'];
            $_project['session']->set('eventManageType', $_REQUEST['manageEvent']);
        break;

        default:
            $return['status'] = 'error';
            $return['message'] = $tr->tr('Nastala chyba, zkuste to prosím později.') . ' error #1001589';
        break;
    }

    echo json_encode($return);
}



/**
 * Našeptávač do editace události, vrátí seznam týmů ze stejného sportu jako je aktivní tým
 **/
if(isset($_REQUEST['eventOpponent']) && !empty($_REQUEST['tag']))
{
    $db = new Database();

    $team = $logged_user->getActiveTeam(true);

    $sql = 'SELECT *
            FROM `team`
            WHERE `team_sport_id` = '.intval($team->sport_id).' AND `team_id` != "'.intval($team->id).'" AND (
                `team_name` LIKE "%'.mysqli_real_escape_string($db->mysqli(), $_REQUEST['tag']).'%" OR
                CONCAT(`team_prefix`, " ", `team_name`) LIKE "%'.mysqli_real_escape_string($db->mysqli(), $_REQUEST['tag']).'%")
            ORDER BY CONCAT(`team_prefix`, " ", `team_name`)';

    $db->query($sql);

    $data = array();

    while($a = $db->readRow('aarr'))
    {
        $team = new Team($a);
        $data[] = '{"key": "'.$team->id.'-'.getTeamHash($team->id).'", "value": "'.$team->getName().'"}';
    }

    $return = '['.implode(', ', $data).']';

    echo $return;
    die;
}



/**
 * Smazani udalosti
 */
if(isset($_REQUEST['deleteEvent']) && !empty($_REQUEST['deleteEvent']))
{
    // id udalosti
    $id = explode('-', $_REQUEST['deleteEvent']);

    // musime mit presne dve ID
    if(count($id) != 2)
    {
        $return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // nacteni udalosti
    $event = EventDAO::get($id[0], $id[1], false);

    // kontrola zda se povedlo nacist
    if(!($event instanceof Event && !empty($event->id)))
    {
        $return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // kontrola zda ji muze editovat prihlaseny uzivatel
    if(!$event->userCanEdit($logged_user))
    {
        $return['message'] = $tr->tr('Nemáte oprávnění smazat tuto událost.');

        echo json_encode($return);
        die;
    }

    // pokud je potvrzena
    if($event->isResultConfirmed($logged_user->getActiveTeam()))
    {
        $return['message'] = $tr->tr('Výsledek události již byl potvrzen, nemůžete ji tedy smazat.');

        echo json_encode($return);
        die;
    }

    $return = array(
        'status'    => 'error',
        'message'   => $tr->tr('Nepovedlo se smazat událost.')
    );

    // nacteme si info o udalosti
    $event->info = unserialize($event->info);

    if(EventDAO::deleteEvent($event, $logged_user))
    {
        $return = array(
            'status'    => 'ok'
        );

        $notification_data = array();

        // nacteme si info o tymu, ktery udalost vytvoril
        $team = TeamDAO::get($event->info[0]['team_id']);

        // info do notifikaci
        $info = serialize(
            array(
                'event_id'  => $event->id,
                'user_team' => $logged_user->getActiveTeam()->id
            )
        );

        // jestlize je pozvany cely tym
        if(!empty($event->info[0]['attend_team']))
        {
            // tak projdeme vsechny jeho hrace a tem posleme notifikaci o zruseni
            $players = $team->getPlayers(true, true);

            // aktualni cas
            $now = time();

            foreach($players as $player)
            {
                // pro prihlaseneho uzivatele to prece nebudeme pridavat
                if($player['user_id'] == $logged_user->id)
                {
                    continue;
                }

                $notification_data[] = array(
                    'user_id'    => $player['user_id'],
                    'object_id'  => $team->id . '_' . $event->id . '_' . 'event',
                    'type_id'    => 'CANCEL-EVENT',
                    'info'       => $info,
                    'object_info'=> '',
                    'timestamp'  => $now,
                    'read'       => '0'
                );
            }
        }
        else
        {
            if(!empty($event->info[0]['attend']))
            {
                foreach($event->info[0]['attend'] as $id => $user)
                {
                    // pro prihlaseneho uzivatele to prece nebudeme pridavat
                    if($id == $logged_user->id)
                    {
                        continue;
                    }

                    $player = UserDAO::get($id);

                    $notification_data[] = array(
                        'user_id'    => $player->id,
                        'object_id'  => $team->id . '_' . $original->id . '_' . 'event',
                        'type_id'    => 'CANCEL-EVENT',
                        'info'       => $info,
                        'object_info'=> '',
                        'timestamp'  => $now,
                        'read'       => '0'
                    );
                }
            }
        }

        // ulozime do DB nove notifikace
        // TODO
        // Notifications::insertNotifications($notification_data);

    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při mazání události.');
    }


    echo json_encode($return);
    die;
}



/**
 * Scrollovani v udalostech doleva a doprava, vrati cele html s nove nactenyma udalostma
 */
if(isset($_REQUEST['viewMoreEvents']) && !empty($_REQUEST['viewMoreEvents']) && !empty($_REQUEST['type']) && !empty($_REQUEST['start']))
{
    // FIXME kontrola práv uživatele
    $start = explode('-', $_REQUEST['start']);
    $id = $start[0];
    $team_id = $start[1];

    $lastEvent = EventDAO::get($id, $team_id);

    $filter = array(
        'year'  => '',
        'type'  => ''
    );

    if(isset($_REQUEST['fyear']) && $_REQUEST['fyear'] != 'all')
    {
        $filter['year'] = $_REQUEST['fyear'];
    }

    if(isset($_REQUEST['ftype']) && $_REQUEST['ftype'] != 'all')
    {
        $filter['type'] = $_REQUEST['ftype'];
    }

    if($_REQUEST['ftype'] == 'all')
    {
        $filter['type'] = '';
    }

    // zkontrolujeme jestli máme tým
    if(isset($_REQUEST['fteam']))
    {
        // musíme zkontrolovat jestli sedí hash
        $t = explode('-', $_REQUEST['fteam']);

        if($t[1] != getTeamHash($t[0]))
        {
            $return = array(
                'status'   => 'error',
                'message'  => $tr->tr('Nastala chyba při načítání událostí.')
            );

            echo json_encode($return);
            die;
        }

        $send_team = TeamDAO::get($t[0]);
    }
    else
    {
        $send_team = $logged_user->getActiveTeam();
    }

    $types = array();
    
    // pokud uzivatel neni hrac tohoto tymu, tak muze videt jen zapasy
    if(!$send_team->isPlayer($logged_user))
    {
        $types = array(Event::FRIENDLY, Event::COMPETITION);
    }

    $array['events'] = EventDAO::getTeamEvents($send_team->id, $types, $_REQUEST['type'], $lastEvent->start, $lastEvent->id, $filter);

    if(count($array['events']) > 0)
    {
        $return = array();
        //$return['count'] = count($array['events']);

        if(count($array['events']) < (EVENT_LIMIT + 1))
        {
            $return['end'] = '1';
        }
        else
        {
            if($_REQUEST['type'] == 'past')
            {
                array_pop($array['events']);
            }
        }

        if($_REQUEST['type'] == 'future')
        {
            if(count($array['events']) == (EVENT_LIMIT + 1))
            {
                array_pop($array['events']);
            }
            $array['events'] = array_reverse($array['events']);
        }

        $return['status'] = 'ok-' . $_REQUEST['type'];
        $return['response'] = printEvents($array, $send_team, $logged_user);
    }
    else
    {
        $return = array(
            'status'   => 'ok-' . $_REQUEST['type'],
            'response' => '<tr class="infoText"><td colspan="6">' . $tr->tr('Žádné další události k zobrazení.') . '</td></tr>',
            'end'      => '1'
        );
    }

    echo json_encode($return);
    die;
}



/**
 * Filtr událostí na stránce events
 */
if(isset($_REQUEST['eventsFilter']) && !empty($_REQUEST['fyear']) && !empty($_REQUEST['ftype']))
{
    $filter = array(
        'year'  => $_REQUEST['fyear'],
        'type'  => $_REQUEST['ftype']
    );

    if($_REQUEST['ftype'] == 'all')
    {
        $filter['type'] = '';
    }

    if($_REQUEST['fyear'] == 'all')
    {
        $filter['year'] = '';
    }

    // zkontrolujeme jestli máme tým
    if(isset($_REQUEST['fteam']))
    {
        // musíme zkontrolovat jestli sedí hash
        $t = explode('-', $_REQUEST['fteam']);

        if($t[1] != getTeamHash($t[0]))
        {
            $return = array(
                'status'   => 'error',
                'message'  => $tr->tr('Nastala chyba při načítání událostí.')
            );

            echo json_encode($return);
            die;
        }

        $send_team = TeamDAO::get($t[0]);
    }
    else
    {
        $send_team = $logged_user->getActiveTeam();
    }
    if($send_team->isPlayer($logged_user))
    {
        $types = array(
            Event::COMPETITION,
            Event::FRIENDLY,
            Event::ACTION,
            Event::TRAINING
        );
    }
    else
    {
        $types = array(
            Event::COMPETITION,
            Event::FRIENDLY
        );
    }
    $events = $send_team->getEvents($filter, $types);

    $return = array(
        'status'    => 'ok',
        'response'  => printEvents($events, $send_team, $logged_user, true)
    );

    echo json_encode($return);
    die;
}



/**
 * Vyplnění statusu docházky u nějaké události
 */
if(!empty($_REQUEST['attendance']['status']) && !empty($_REQUEST['attendance']['event-id']))
{
    $statuses = array(
        'yes',
        'no',
        'na',
        'blackDot'
    );

    // nepovolený status
    if(!in_array($_REQUEST['attendance']['status'], $statuses))
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#1';
        echo json_encode($return);
        die;
    }

    // roparsování udaju k udalosti
    $args = explode('-', $_REQUEST['attendance']['event-id']);

    if(!empty($args[2]) && !empty($args[1]) && !empty($args[0]))
    {
        $usersplit  = $args[2];
        $event_id = $args[1];
        $team_id  = $args[0];
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#2';
        echo json_encode($return);
        die;
    }

    // musime jeste zkontrolovat uživatele, zda sedí hash
    $usersplit = explode('@', $usersplit);
    if(!empty($usersplit[0]) && !empty($usersplit[1]))
    {
        $user_id = $usersplit[0];

        // nesedí hash u uživatele je to podvržené
        if(getRecipientHash($user_id) != $usersplit[1])
        {
            $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#3';
            echo json_encode($return);
            die;
        }
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#4';
        echo json_encode($return);
        die;
    }

    // uživatel
    $user = UserDAO::get($user_id);

    // detail udalosti
    $event = EventDAO::get($event_id, $team_id, false);

    if(!($event instanceof Event))
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#11';
        echo json_encode($return);
        die;
    }

    if($event->team_id == $logged_user->getActiveTeam()->id)
    {
        $user_team_id = $event->team_id;
        $team_type = '';
    }
    elseif($event->oponent_id == $logged_user->getActiveTeam()->id)
    {
        $user_team_id = $event->oponent_id;
        $team_type = '_away';
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#25';
        echo json_encode($return);
        die;
    }

    // team
    $team = TeamDAO::get($user_team_id);

    // zkontrolujeme zda je vubec uzivatel hracem tohoto tymu
    if(!$team->isPlayer($user))
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#5';
        echo json_encode($return);
        die;
    }

    //  a taky jeste kontrola, zda se prihlasuje samostny uzivatel, nebo ho prihlasuje admin
    if($user->id != $logged_user->id)
    {
        // uzivatel neni admin, nemuze ho proto prihlasit
        if(!$team->isAdmin($logged_user))
        {
            $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#6';
            echo json_encode($return);
            die;
        }
    }
    // uzivatel edituje sám sebe, tak muze delat jen budouci udalosti
    elseif(date('Y-m-d') > date('Y-m-d', strtotime($event->start)) && !$team->isAdmin($logged_user))
    {
        $return['message'] = $tr->tr('Docházku k této události již nemůžete editovat.');
        echo json_encode($return);
        die;
    }

    // načtení události podle ID
    // $event = EventDAO::get($event_id, $team_id, false);

    // pokud uz udalost probehla, tak muzu dochazku vyplnit jenom kdyz jsem admin
    if(date('Y-m-d') > date('Y-m-d', strtotime($event->start)) && !$team->isAdmin($logged_user))
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#10';
        echo json_encode($return);
        die;
    }

    // info o udalosti
    $event->info = unserialize($event->info);

    // kontrola zda je uživatel na tuto událost pozván vůbec
    if(!@in_array($user_id, @array_keys(@$event->info[0]['attend'.$team_type])) && @$event->info[0]['attend_team'.$team_type] != 1)
    {
        $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#7';
        echo json_encode($return);
        die;
    }

    // kontrola zda už se uživatel vyjádřil
    $data = array(
        'team_id'       => $team_id,
        'event_id'      => $event_id,
        'user_id'       => $user_id,
        'attend'        => $_REQUEST['attendance']['status'],
        'note'          => strip_tags(@$_REQUEST['attendance']['text']),
        'inserted_by'   => $logged_user->id
    );

    // zjistime zda uz se k tomu vyjadril
    $attend = AttendDAO::get($data);

    if(count($attend) > 0)
    {
        if(AttendDAO::update($data))
        {
            $return = array(
                'status'  => 'ok'
            );
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#8';
            echo json_encode($return);
            die;
        }
    }
    else
    {
        if(AttendDAO::insert($data))
        {
            $return = array(
                'status'  => 'ok'
            );
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při vyplňování docházky.') . '#9';
            echo json_encode($return);
            die;
        }
    }

    if(isset($_REQUEST['quickAttendance']) && $_REQUEST['quickAttendance'] == 1)
    {
        $return['response'] = ($_REQUEST['attendance']['status'] == 'yes' ? 'zúčastním se' : 'nezúčastním se');
    }

    echo json_encode($return);
    die;
}



/**
 * nacte dalsi udalosti do dochazky
 */
if(!empty($_REQUEST['viewMoreEventsAttendances']) && !empty($_REQUEST['type']) && !empty($_REQUEST['start']))
{
    // FIXME kontrola práv uživatele
    $start = explode('-', $_REQUEST['start']);
    if(!empty($start[0]) && !empty($start[1]))
    {
        $team_id    = $start[0];
        $id         = $start[1];
    }
    else
    {
        die;
    }

    $lastEvent = EventDAO::get($id, $team_id, false);

    if(!$lastEvent instanceof Event)
    {
        die;
    }

    switch($_REQUEST['type'])
    {
        case 'next':
            $type = 'future';
            break;
        case 'prev':
            $type = 'past';
            break;
        default:
            die;
    }

    $return = array(
        'status'  => 'ok',
        'response' => ''
    );
    // nacteme si udalosti
    //$events = EventDAO::getTeamEvents($logged_user->getActiveTeam()->id, $types, , $lastEvent->start, $lastEvent->id, $filter);

    $positions = SportDAO::getPositions($logged_user->getActiveTeam()->sport_id);
    $roster = Rosters::getTeamRoster($logged_user->getActiveTeam()->id);
    $limit = 4;
    $events = EventDAO::getTeamEvents($logged_user->getActiveTeam()->id, array(), $type, $lastEvent->start, $lastEvent->id, array(), $limit);
    $stopLeft  = false;
    $stopRight = false;
    if($type == 'future')
    {
        if(count($events) == ($limit + 1))
        {
            array_pop($events);
        }
        else
        {
            $stopRight = true;
        }
        //$events = array_reverse($events, true);
    }
    elseif($type == 'past')
    {
        if(count($events) == ($limit + 1))
        {
            array_pop($events);
        }
        else
        {
            $stopLeft = true;
        }
        $events = array_reverse($events, true);
    }

    if(count($events))
    {
        // doplnime si prazdne sloupce
        if(count($events) < $limit)
        {
            $repeat = ($limit - count($events));
            for($a = $repeat; $a > 0; $a--)
            {
                if($type == 'past')
                {
                    $events = array($a => 'nic') + $events;
                }
                else
                {
                    $events = $events + array($a => 'nic');
                }
            }
        }

        $attend = AttendDAO::getAttendForEvents($events);
        foreach($positions as $position)
        {
            if(isset($roster[$position->id]))
            {
                $players = $roster[$position->id];
            }
            else
            {
                $players = array();
            }

            if($position->staff != 1)
            {
                $return['response'] .= printPositionAttend($position, $players, $attend, $events, $stopLeft, $stopRight, $team, $type);
            }
            else
            {
                //echo printStaffAttend($position, $players, $attend, $events);
            }
        }

        // zkusime vypsat i nezarazene hrace
        if(isset($roster[0]))
        {
            $data = array(
                'id'        => '0',
                'sport_id'  => $position->sport_id,
                'name'      => 'Nezařazení hráči',
                'name2'     => 'Nezařazení hráči',
                'short'     => '',
                'order'     => '',
                'staff'     => '0',
                'stats'     => ''
            );
            $position = new Position($data);
            
            $players = $roster[0];
            $return['response'] .= printPositionAttend($position, $players, $attend, $events, $stopLeft, $stopRight, $team, $type);
        }
    }
    else
    {
        $return['response'] = '';
    }

    echo json_encode($return);
    die;
}



/**
 * nacteni starsich zprav do konverzace
 */
if(!empty($_REQUEST['viewMoreMessages']) && !empty($_REQUEST['start']) && !empty($_REQUEST['id']))
{
    $id = str_replace('conversation-', '', $_REQUEST['id']);
    $start = intval($_REQUEST['start']);
    $limit = (VIEW_MESSAGE_LIMIT + 1);
    $messages = PrivateMessages::getConversationDetail($id, $logged_user->id, $start, $limit);

    $messagesHTML = '';
    if(count($messages) === 0)
    {
        $return['status'] = 'ok-no-more';
        $return['message'] = '<span>' . $tr->tr('Žádné další zprávy k zobrazení') . '</span>';

        echo json_encode($return);
        die;
    }
    // zjistime zda jeste nejaky zpravy mame, nebo uz je konec
    elseif(count($messages) < $limit)
    {
        $return['status'] = 'ok-no-more';
        //$messagesHTML = '<span>' . $tr->tr('Žádné další zprávy k zobrazení') . '</span>';
    }
    else
    {
        // odebereme posledni zpravu, ktera slouzila jen pro zjisteni zda budeme chtit jeste nacitat
        array_pop($messages);

        // a vypíšeme je od nejstarší po nejnovější
        $messages = array_reverse($messages, true);

        $return['status'] = 'ok';
    }

    // vypiseme vsechny zpravy
    foreach($messages as $message)
    {
        // pomocne pole pro uzivatele, protoze nesedi uplne presne nazvy sloupcu
        $userData = $message;
        $userData['user_id'] = $userData['sent_user_id'];
        unset($userData['sent_user_id']);

        $user = new User($userData);
        $pm = new PrivateMessage($message);

        if($user->id == $logged_user->id)
        {
            $class = ' sent';
        }
        else
        {
            $class = ' received';
        }

        $messagesHTML .= printMessage($class, $pm, $user);
    }

    $return['response'] = $messagesHTML;
    echo json_encode($return);
    die;
}



/**
 * Přidání teampostu - vzkaz na nástěnku pro členy týmu
 */
if(!empty($_REQUEST['teampost']['text']) && !empty($_REQUEST['teampost']['team']))
{
    // potrebujeme aktivni tym, protoze tomu bude posilat zpravu
    $active_team = $logged_user->getActiveTeam();

    $team = explode('-', $_REQUEST['teampost']['team']);

    // zkontrolujeme hash
    if($team[1] != getTeamHash($team[0]))
    {
        $return['message'] = $tr->tr('Nastala chyba při posílání zprávy.') . ' #15346';

        echo json_encode($return);
        die;
    }

    if($team[0] != $logged_user->getActiveTeam()->id)
    {
        $return['message'] = $tr->tr('Můžete psát vzkazy pouze aktivnímu týmu.');

        echo json_encode($return);
        die;
    }

    // nacteme si info o tymu
    $team = TeamDAO::get($team[0]);

    if(!$team instanceof Team || empty($team->id))
    {
        $return['message'] = $tr->tr('Nastala chyba při posílání zprávy.') . ' #15423';

        echo json_encode($return);
        die;
    }

    // uzivatel musi byt hracem tymu, jinak jim nesmi psat!
    if(!$team->isPlayer($logged_user))
    {
        $return['message'] = $tr->tr('Můžete psát vzkazy pouze týmu, kterého jste členem.');

        echo json_encode($return);
        die;
    }

    $date = date('Y-m-d H:i:s');

    $data = array(
        'parent_id'  => '0',
        'user_id'    => $logged_user->id,
        'team_id'    => $logged_user->getActiveTeam()->id,
        'type'       => 'teampost',
        'created'    => $date,
        'date_begin' => $date,
        'text'       => strip_tags($_REQUEST['teampost']['text']),
    );
    $data['value']      = serialize($data);

    $object = new TeamPost($data);

    $inserted_id = WallDAO::insertItem($object, $object->team_id, 'teampost');

    // povedlo se vlozit?
    if($inserted_id)
    {
        $data['id'] = $inserted_id;

        if(!empty($data['id']))
        {
            // nacteme si vsechny hrace z tymu a posleme jim notifikace
            $players = $team->getPlayers();

            if(is_array($players) && !empty($players))
            {
                // ulozime si informace kvuli notifikacim
                //$data['user'] = $logged_user;
                $data['value'] = serialize($object);

                $object = (object) $data;

                $object_id = $data['team_id'] . '_' . $inserted_id . '_teampost';

                $time = time();
                $notification_data = array();

                // kvuli notifikacim
                $pom = $logged_user->getAllData();

                foreach($players as $player_id)
                {
                    if($player_id == $logged_user->id)
                    {
                        continue;
                    }

                    // data pro notifikaci
                    $notification_data[] = array(
                        'user_id'    => $player_id,
                        'object_id'  => $object_id,
                        'type_id'    => 'NEW-TEAMPOST',
                        'info'       => serialize(
                                            array(
                                                'users' => array(
                                                    $pom['id'] => $pom['id']
                                                ))),
                        'object_info'=> serialize($object),
                        'timestamp'  => $time,
                        'read'       => '0'
                    );
                }

                // uložíme do DB nové notifikace
                Notifications::insertNotifications($notification_data);
            }

            $return = array(
                'status'    => 'ok',
                'response'  => getWall(array($data)),
                'message'   => $tr->tr('Zpráva byla v pořádku přidána.')
            );
        }
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při přidávání zprávy. Zkuste to prosím později znovu.') . ' error code: #005007';
    }


    echo json_encode($return);
    die;
}



/**
 * Smazání teampostu
 **/
if(isset($_REQUEST['delete_teampost']))
{
    $object = explode('_', $_REQUEST['delete_teampost']);
    if(count($object) == 3)
    {
        // potrbeujeme tym, kvuli pravum
        $team = TeamDAO::get($object[0]);

        // nacteme si info o mazanem objektu, abychom zjistili zda ma na to vubec uzivatel pravo
        $item = WallDAO::getObject($_REQUEST['delete_teampost'], array('teampost'));
        $item = current($item);

        // může smazat?
        if($team instanceof Team && ($team->userCanChangePress($logged_user) || $item['user_id'] == $logged_user->id))
        {
            if(WallDAO::deleteItem($object[0], $object[1], 'teampost'))
            {
            	@Notifications::deleteNotifications(0, $object[0] . '_' . $object[1], array('NEW-TEAMPOST'));

                $return['status'] = 'ok';
                $return['message'] = '<div class="msg done"><h3 class="title">' . $tr->tr('Hotovo!') . '</h3><p class="text">' . $tr->tr('Vzkaz byl úspěšně smazán.') . '</p></div>';
            }
            else
            {
                $return['message'] = $tr->tr('Nastala chyba při mazání vzkazu.') . ' error code: #004012';
            }
        }
        else
        {
            $return['message'] = $tr->tr('Nastala chyba při mazání vzkazu.' . ' error code: #004013');
        }
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při mazání vzkazu.' . ' error code: #004014');
    }

    echo  json_encode($return);
    die;
}



/**
 * editace a pridani sezony v editaci tymoveho profilu
 */
if(isset($_REQUEST['manage_season']) && !empty($_REQUEST['season']))
{
    // uživatel musí byt admin, aby mohl editovat sezony
    if($logged_user->getActiveTeam() instanceof Team && $logged_user->getActiveTeam()->isAdmin($logged_user))
    {
        // v getu nam prijde jmeno a jestli je sezona aktivní
        $data = $_REQUEST['season'];

        // zkontrolujeme data
        if(empty($data['name']))
        {
            $return['message'] = $tr->tr('Musíte zadat název sezóny.');

            echo json_encode($return);
            die;
        }

        // maximalni delka sezony 25 znaků
        $data['name'] = mb_substr($data['name'], 0, 25);

        // aktivni muze byt vzdy jen jedna
        /*if(!empty($data['active']))
        {
            // takze je vsechny zneaktivnime
            Seasons::setInactiveTeamSeasons($data['team_id']);
        }*/

        // potřebujeme ID týmu
        $data['team_id'] = $logged_user->getActiveTeam()->id;

        // kvuli bezpecnosti
        $data['id'] = intval(@$data['id']);

        // pokud nemáme ID, tak vložíme novou sezonu
        if(empty($data['id']))
        {
            if($inserted_id = Seasons::insertSeason($data))
            {
                $team_seasons = $logged_user->getActiveTeam(true)->getSeasons(true);

                $return = array(
                    'status'    => 'ok-new',
                    'response'  => print_edit_seasons($team_seasons),
                    'select'    => print_season_select($team_seasons)
                );

                // pokud jsme vlozili prvni sezonu, tak ji nastavime jako aktivni
                if(count($team_seasons) == 1)
                {
                    Seasons::setActiveTeamSeason($data['team_id'], $inserted_id, false);
                }
            }
            else
            {
                $return['message'] = $tr->tr('Nastala chyba při přidávání sezóny.');
            }
        }
        // jinak musíme editovat
        else
        {
            if(Seasons::updateSeason($data))
            {
                $team_seasons = $logged_user->getActiveTeam(true)->getSeasons(true);

                $return = array(
                    'status'    => 'ok-edit',
                    'select'    => print_season_select($team_seasons)
                );
            }
            else
            {
                $return['message'] = $tr->tr('Nastala chyba při editaci sezóny.');
            }
        }

        if(empty($return['select']) && $return['status'] == 'ok')
        {
            $return['button'] = $tr->tr('přidat sezónu');
        }
        else
        {
            $return['button'] = $tr->tr('editovat sezóny');
        }

    }

    echo json_encode($return);
    die;
}



/**
 * smazani sezony v editaci tymoveho profilu
 */
if(!empty($_REQUEST['delete_season']) && !empty($_REQUEST['season']['id']))
{
    $return['message'] = $tr->tr('Nastala chyba při mazání sezony, zkuste to prosím znovu později.');

    // uživatel musí byt admin, aby mohl editovat sezony
    if($logged_user->getActiveTeam() instanceof Team && $logged_user->getActiveTeam()->isAdmin($logged_user))
    {
        $db = new Database();
        $sql = 'SELECT count(`id`) as pocet
                FROM `event`
                WHERE (`season_id` = ' . intval($_REQUEST['season']['id']) . ' AND `team_id` = ' . $logged_user->getActiveTeam()->id .')
                 OR (`oponent_season_id` = ' . intval($_REQUEST['season']['id']) . ' AND `oponent_id` = ' . $logged_user->getActiveTeam()->id . ')';
        $db->query($sql);
        $row = $db->readrow('aarr');

        if($row['pocet'] > 0)
        {
            $return = array(
                'status'    => 'error',
                'message'   => $tr->tr('Nemůžete smazat sezónu, ve které máte zařazené události.')
            );

            echo json_encode($return);
            die;
        }

        if(Seasons::deleteSeason($logged_user->getActiveTeam()->id, $_REQUEST['season']['id']))
        {

            $team_seasons = $logged_user->getActiveTeam(true)->getSeasons(true);

            $return = array(
                'status'  => 'ok',
                'select'  => print_season_select($team_seasons)
            );

            if(empty($return['select']))
            {
                $return['button'] = $tr->tr('přidat sezónu');
            }
            else
            {
                $return['button'] = $tr->tr('editovat sezóny');
            }
        }
    }

    echo json_encode($return);
    die;
}



/**
 * nastaveni vysledku udalosti - nacte kompletni formular pro editaci vysledku
 */
if(isset($_REQUEST['result']['settings']) && $_REQUEST['result']['settings'] == 'sent')
{
    $players_count = 1;
    if(empty($_REQUEST['result']['players']) || intval($_REQUEST['result']['players']) < 1)
    {
        $return['message'] = $tr->tr('Zadejte počet hráčů.');

        echo json_encode($return);
        die;
    }
    else
    {
        $players_count = intval($_REQUEST['result']['players']);
    }

    // nacteme si info o udalosti
    $event = EventDAO::get($_REQUEST['result']['event_id'], $_REQUEST['result']['team_id']);

    // uzivatel musi mit moznost ji vubec editovat
    if(!$event instanceof Event || empty($event->id))
    {
        $return['message'] = $tr->tr('Událost nebyla nalezena.');

        echo json_encode($return);
        die;
    }

    // uzivatel musi mit moznost ji vubec editovat
    if(!$event->userCanEdit($logged_user))
    {
        $return['message'] = $tr->tr('Nastala chyba při nastavování události.') . ' #2689';

        echo json_encode($return);
        die;
    }

    // udalost neni soutezni ani pratelsky zapas, takze ji nemuzeme pridavat vysledky
    if($event->type != Event::COMPETITION && $event->type != Event::FRIENDLY)
    {
        $return['message'] = $tr->tr('Nastala chyba při nastavování události.') . ' #2698';

        echo json_encode($return);
        die;
    }

    // odeslana data o udalosti
    $event->info = unserialize($event->info);

    // musime mit info o udalosti
    if(!is_array($event->info))
    {
        $return['message'] = $tr->tr('Nastala chyba při nastavování události.') . ' #2710';

        echo json_encode($return);
        die;
    }

    // nacteme si domaci tym
    $homeTeam = TeamDAO::get($event->team_id);

    if(!empty($event->oponent_id))
    {
        // hostujici tym
        $awayTeam = TeamDAO::get($event->oponent_id);
    }
    else
    {
        // FIXME tym neni na mtw
        $data = array(
            'id'        => 0,
            'sport_id'  => $homeTeam->sport_id,
            'name'      => $event->info[0]['team_name'],
        );
        $awayTeam = new Team($data);
    }


    // info o sportu - sport_id musi byt stejne u obou tymu, takze neresime rozdil
    $sport = SportDAO::get($homeTeam->sport_id);

    // mozne pozice u sportu
    $positions = $sport->getPositions();

    // moznosti na pocty hracu
    $sport->players = unserialize(@$sport->players);
    $sport->team_stats = unserialize(@$sport->team_stats);
    $sport->select_stats = unserialize(@$sport->select_stats);

    // vysledek udalosti
    $event->result = unserialize(@$event->result);
    $event->rosters = unserialize(@$event->rosters);
    $event->team_stats = unserialize(@$event->team_stats);

    // musime zkontrolovat zda nepodvrhnul data
    if(empty($_REQUEST['result']['players']) || !in_array($_REQUEST['result']['players'], array_keys($sport->players)))
    {
        $return['message'] = $tr->tr('Nastala chyba při nastavování události.');

        echo json_encode($return);
        die;
    }

    // zkontrolujeme pocet casti
    $periods = intval(@$_REQUEST['result']['periods']);
    if(empty($periods) || $periods > 4)
    {
        $return['message'] = $tr->tr('Nastala chyba při nastavování události.');

        echo json_encode($return);
        die;
    }
    // $inputs = '';
    $results = array();

    $res = '<p id="stepError" class="msg error noClose" style="display: none;">Než přejdete na další krok, je nutné vyplnit správně následující položky.</p>
            <form id="resultsContainer" method="post" class="cleaned" action="">
            <input type="hidden" name="result[result][periods]" value="'.$periods.'" />
            <input type="hidden" name="result[result][players_count]" value="'.$players_count.'" />
            <ul id="stepsNavigation">
                <li class="active"><a class="resultBox" href="#">výsledek</a></li>
                <li class="disabled"><a class="rosterBox" href="#">sestavy</a></li>
                <li class="disabled"><a class="statsBox" href="#">statistiky</a></li>
                <li class="disabled"><a class="playersBox" href="#">hráči</a></li>
                <!--<li class="disabled"><a class="summaryBox" href="#">přehled</a></li>-->
            </ul><!-- #stepsNavigation -->

            <div id="resultBox" class="tab active cleaned">
                <div class="left">
                    <div class="resultEdit">
                        <fieldset>

                            <p class="entries cleaned">
                                <label>konečný výsledek</label>
                                <input class="input text required" data-name="finalResultHome" autocomplete="off" type="text" name="result[result][home][final]" value="'.@$event->result['home']['final'].'" />
                            </p>';
            if($periods > 1)
            {
                for($i = 1; $i <= $periods; $i++)
                {
                    $res .=     '<p class="entries cleaned">
                                    <label>'.$i.'. část</label>
                                    <input class="input text required" data-name="resultHome-'.$i.'" autocomplete="off" type="text" name="result[result][home][period'.$i.']" value="'.@$event->result['home']['period'.$i].'" />
                                </p>';
                    //$inputs .= '<input class="resultHome-'.$i.'" type="text" />';

                    $results[] = '<span class="resultHome-'.$i.'">'.@$event->result['home']['period'.$i].'</span>:<span class="resultAway-'.$i.'">'.@$event->result['away']['period'.$i].'</span>';
                }
            }

            $showOvertime = $showPenalties = $showShootout = false;
            // pokud muze mit sport prodlouzeni
            if(!empty($sport->overtime))
            {
                $showOvertime = true;
            }

            if(!empty($sport->penalties))
            {
                $showPenalties = true;
            }

            // nebo samostatne najezdy?
            if(!empty($sport->shootout))
            {
                $showShootout = true;
            }

            $checkPenalties = $checkShootout = $checkOvertime = false;
            if(!empty($event->result['isPenalties']) && $showPenalties)
            {
                $checkPenalties = true;
            }

            if(!empty($event->result['isShootout']) && $showShootout)
            {
                $checkShootout = true;
            }

            if(!empty($event->result['isOvertime']) && $showOvertime)
            {
                $checkOvertime = true;
            }


            if($showOvertime)
            {
                $res .= '
                            <p class="entries typeCheckbox cleaned">
                                <label>prodloužení</label>
                                <input class="checkbox overtime" type="checkbox" data-name="overtime" value="1" name="result[result][isOvertime]"'.($checkOvertime ? ' checked="checked"' : '').' />
                            </p>';
            }

            if($showShootout)
            {
                $res .= '
                            <p class="entries typeCheckbox cleaned">
                                <label>sam. nájezdy</label>
                                <input class="checkbox shootout" type="checkbox" data-name="shootout" value="1" name="result[result][isShootout]"'.($checkShootout ? ' checked="checked"' : '').' />
                            </p>';
            }

            if($showPenalties)
            {
                $res .= '
                            <p class="entries typeCheckbox cleaned">
                                <label>penalty</label>
                                <input class="checkbox penalty" type="checkbox" data-name="penalty" value="1" name="result[result][isPenalties]"'.($checkShootout ? ' checked="checked"' : '').' />
                            </p>';
            }

            if($showOvertime)
            {
                $res .= '
                            <p class="entries overtime'.(empty($event->result['isOvertime']) ? ' forBlind' : '').' cleaned">
                                <label>prodloužení</label>
                                <input class="input text" data-name="overtimeHome" autocomplete="off" type="text" name="result[result][home][overtime]"  value="'.@$event->result['home']['overtime'].'"/>
                            </p>';
            }

            $res .= '
                        </fieldset>
                    </div>
                </div>

                <div class="right">
                    <div class="resultEdit">
                        <fieldset>

                            <p class="entries cleaned">
                                <label>konečný výsledek</label>
                                <input class="input text required" data-name="finalResultAway" autocomplete="off" type="text" name="result[result][away][final]" value="'.@$event->result['away']['final'].'" />
                            </p>';

            if($periods > 1)
            {
                for($i = 1; $i <= $periods; $i++)
                {
                    $res .=     '<p class="entries cleaned">
                                    <label>'.$i.'. část</label>
                                    <input class="input text required" data-name="resultAway-'.$i.'" autocomplete="off" type="text" name="result[result][away][period'.$i.']" value="'.@$event->result['away']['period'.$i].'" />
                                </p>';

                    // $inputs .= '<input class="resultAway-'.$i.'" type="text" />';
                }
            }

            $numbers = array_combine(range(1,99),range(1,99));

            //$roster = Rosters::getTeamRoster($team->id);

            if($showOvertime)
            {
                $res .= '
                            <p class="entries overtime'.(empty($event->result['isOvertime']) ? ' forBlind' : '').' cleaned">
                                <label>prodloužení</label>
                                <input class="input text" data-name="overtimeAway" autocomplete="off" type="text" name="result[result][away][overtime]" value="'.@$event->result['away']['overtime'].'" />
                            </p>';
            }

            $res .= '
                        </fieldset>
                    </div>
                </div>

                <p class="nextLink">
                    <a class="button buttonC">uložit a přejít na další krok</a>
                </p>
            </div><!-- #resultBox -->

            <!-- /* ROSTER */ -->
            <div id="rosterBox" class="tab cleaned">
                <div class="left">
                    <div class="rosterEdit">
                        <fieldset>';

            $ours = 'away';
            // zjistime si jestli jsme domaci nebo hoste
            if($event->team_id == $logged_user->getActiveTeam()->id)
            {
                $ours = 'home';
            }

            $home_players = $homeTeam->getPlayers(false, true);
            $i = 0;
            foreach($home_players as $player)
            {
                if(!empty($player['staff']))
                {
                    continue;
                }

                $res .= print_player_roster($positions, $home_players, $numbers, $i, 'home', true, $event->rosters, $players_count, false);
                $i++;
            }
            /*<p class="more addPlayer">
                                <a href="#" class="button buttonB icon add">přidat dalšího hráče</a>
                            </p>*/
            $res .= '
                        </fieldset>
                    </div>
                </div>

                <div class="right">
                    <div class="rosterEdit">
                        <fieldset>';

            $away_players = array();
            $i = 0;
            if($awayTeam->id > 0)
            {
                $away_players = $awayTeam->getPlayers(false, true);

                foreach($away_players as $player)
                {
                    if(!empty($player['staff']))
                    {
                        continue;
                    }
                    $res .= print_player_roster($positions, $away_players, $numbers, $i, 'away', true, $event->rosters, $players_count, false);
                    $i++;
                }
            }
            else
            {
                if(!isset($event->columns['rosters']['away']))
                {
                    $event->columns['rosters']['away'] = array();
                }

                if(count($event->rosters['away']))
                {
                    $away_players = $event->rosters['away'];

                    foreach($away_players as $key => $player)
                    {
                        //$new_key = 'away-' . $i . '-1';
                        $res .= print_player_roster($positions, array($player), $numbers, $key, 'away', false, $event->rosters, $players_count, true);
                        $i++;
                    }
                }
                else
                {
                	$res .= print_player_roster($positions, array(), $numbers, $i, 'away', false, $event->rosters, $players_count, true);
                    $i++;
                }

                $res .=     '
                            <p class="more addPlayer">
                                <a href="#" class="button buttonB icon add">přidat dalšího hráče</a>
                            </p>';
            }

            $res .= '
                            <input type="hidden" name="result[event_id]" value="'.$event->id.'" />
                            <input type="hidden" name="result[team_id]" value="'.$event->team_id.'" />
                        </fieldset>
                    </div>
                </div>

                <p class="nextLink sendAjax">
                    <a class="button buttonC">uložit a přejít na další krok</a>
                </p>
            </div><!-- #rosterBox -->


            <!-- /* STATISTIKY */ -->
            <div id="statsBox" class="tab cleaned">
                <div class="left">
                    <div class="statsEdit">
                        <fieldset>';
                            $i = 0;
                            if(!is_array($event->team_stats))
                            {
                                $event->team_stats = unserialize($event->team_stats);
                            }

                            $team_stats = $event->team_stats;

                            if(!is_array($team_stats))
                            {
                                $team_stats = array();
                            }

                            foreach($sport->team_stats as $key => $val)
                            {
                                $res .= '<p class="entries cleaned">
                                            <label for="homeTeamStats'.$key.'">'.$tr->tr($val).'</label>
                                            <input id="homeTeamStats'.$key.'" class="input text" maxlength="3" name="result[team_stats]['.$key.'][home]" type="text" value="'.(isset($team_stats[$key]['home']) ? $team_stats[$key]['home'] : '').'" />
                                        </p>';
                                $i++;
                            }

                        $res .= '</fieldset>
                    </div>
                </div>

                <div class="right">
                    <div class="statsEdit">
                        <fieldset>';
                            $i = 0;
                            foreach($sport->team_stats as $key => $val)
                            {
                                $res .= '<p class="entries cleaned">
                                            <label for="awayTeamStats'.$key.'">'.$tr->tr($val).'</label>
                                            <input id="awayTeamStats'.$key.'" class="input text" maxlength="3" name="result[team_stats]['.$key.'][away]" type="text" value="'.(isset($team_stats[$key]['away']) ? $team_stats[$key]['away'] : '').'" />
                                        </p>';
                                $i++;
                            }

                        $res .= '</fieldset>
                    </div>
                </div>

                <p class="helpLink">
                    <span class="help textWithIcon">jak vše správně a rychle vyplnit</span>
                    <span class="tooltip big">Pokud pole necháte prázdné, vyplní se do něj po uložení automaticky 0. Pokud některé statistiky nechcete používat, vypňte do polí " - ".</span>
                </p>
                <p class="nextLink">
                    <a class="button buttonC">uložit a přejít na další krok</a>
                </p>
            </div><!-- #statsBox -->';

            // musime spocitat kolik statistik bude zadavat
            $scount = 0;

            foreach($positions as $position)
            {
                if(!is_array($position->stats))
                {
                    $position->stats = unserialize($position->stats);
                }

                $i = 0;
                if(is_array($position->stats))
                {
                    foreach($position->stats as $stat)
                    {
                        if(empty($stat['auto']))
                        {
                            $i++;
                        }
                    }

                    if($i > $scount)
                    {
                        $scount = $i;
                    }
                }
            }

            $res .= '<!-- /* HRACI */ -->
            <div id="playersBox" class="tab cleaned">
                <div class="left stats-'.$scount.'">
                    <div class="playersEdit">
                        <fieldset>';

                            //$res .= print_players();
                            $players = $home_players;
                            //$stats = StatsDAO::getStatsForEvent($event->id . '-' . $event->team_id, $sport->stat_table);

                            if(!isset($stats['home']) || !is_array($stats['home']))
                            {
                                $stats['home'] = array();
                            }

                            if(!isset($stats['away']) || !is_array($stats['away']))
                            {
                                $stats['away'] = array();
                            }

                            //$res .= print_players_stats_new($positions, $home_players, 'home', $stats['home']);
                            $res .= '
                        </fieldset>
                    </div>
                </div>

                <div class="right stats-'.$scount.'">
                    <div class="playersEdit">
                        <fieldset>';
                            //$res .= print_players_stats_new($positions, $away_players, 'away', $stats['away']);
                            $res .= '
                        </fieldset>
                    </div>
                </div>

                <p class="saveLink">
                    <button class="button buttonC" type="submit">uložit a dokončit</button>
                </p>
            </div><!-- #playersBox -->
        </form><!-- .resultContainer -->';
/* <p class="helpLink">
                    <span class="help textWithIcon">jak vše správně a rychle vyplnit</span>
                    <span class="tooltip big">Pokud pole necháte prázdné, vyplní se do něj po uložení automaticky 0. Pokud některé statistiky nechcete používat, vypňte do polí " - ".</span>
                </p>*/

    $return = array(
        'status'    => 'ok',
        'response'  => $res,
        'title'     => $tr->tr('Nastavit výsledek události'),
        'results'   => $periods > 1 ? '<span class="results">(&nbsp;' . implode(',&nbsp;', $results) . '<span class="overtimeBox">&nbsp;-&nbsp;<span class="overtimeHome">'.@$event->result['home']['overtime'].'</span>:<span class="overtimeAway">'.@$event->result['away']['overtime'].'</span></span>&nbsp;)</span>' : ''
    );

    echo json_encode($return);
    die;
}


/**
 * nacteni zalozky v editaci vysledku, kde se vyplnuji statistiky hracu
 */
if(!empty($_REQUEST['getResultPlayers']) && !empty($_REQUEST['result']['event_id']) && !empty($_REQUEST['result']['team_id']))
{
    // nacteme si info o udalosti
    $event = EventDAO::get($_REQUEST['result']['event_id'], $_REQUEST['result']['team_id']);

    // musi byt admin
    if(!$event->userCanEdit($logged_user))
    {
        $return = array(
            'status'   => 'error',
            'message'  => $tr->tr('Nemůžete upravovat tuto událost.')
        );

        echo json_encode($return);
        die;
    }

    // nacteme si domaci tym
    $homeTeam = TeamDAO::get($event->team_id);

    // info o sportu - sport_id musi byt stejne u obou tymu, takze neresime rozdil
    $sport = SportDAO::get($homeTeam->sport_id);

    // mozne pozice u sportu
    $positions = $sport->getPositions();

    // statistiky zapasu
    $stats = StatsDAO::getStatsForEvent($event->id . '-' . $event->team_id, $sport->stat_table);

    $res_home = $res_away = '<fieldset>';
    $res_home .= print_players_stats_new(@$_REQUEST['result']['players']['home'], 'home', @$stats['home'], $sport);
    $res_home .= '</fieldset>';

    $res_away .= print_players_stats_new(@$_REQUEST['result']['players']['away'], 'away', @$stats['away'], $sport);
    $res_away .= '</fieldset>';

    $return = array(
        'status'        => 'ok',
        'responseLeft'  => $res_home,
        'responseRight' => $res_away
    );

    echo json_encode($return);
    die;
}



/**
 * nacteni vice poslednich uzivatelovych aktivit na profilu
 */
if(!empty($_REQUEST['viewMoreUserActivities']) && !empty($_REQUEST['user']) && !empty($_REQUEST['last']))
{
    // musime nejdriv zkontrovat hash
    $user = explode('-', $_REQUEST['user']);

    if(count($user) != 2 || ($user[1] != getRecipientHash($user[0])))
    {
        $return['message'] = $tr->tr('Chyba při načítání starších aktivit.') . '#3233';

        echo json_encode($return);
        die;
    }

    // nacteme o jedno vic, abychom vedeli jestli mame jeste zobrazit tlacitko na nacteni dalsich
    $user = UserDAO::get($user[0]);
    if($user instanceof User && $user->id > 0)
    {
        $limit = LAST_ACTIVITIES_LIMIT + 1;
        $last = UserDAO::getLastActivities($user->id, html_entity_decode($_REQUEST['last']), $limit);

        if(is_array($last))
        {
            // pokud
            if(count($last) < $limit)
            {
                $return['status'] = 'ok';
                $print_more = false;
            }
            else
            {
                $return['status'] = 'ok';
                $print_more = true;
                array_pop($last);
            }

            $return['response'] = print_last_activities($user, $logged_user, $last, $print_more);
        }
        else
        {
            $return['message'] = $tr->tr('Chyba při načítání starších aktivit.') . '#3264';

            echo json_encode($return);
            die;
        }
    }
    else
    {
        $return['message'] = $tr->tr('Chyba při načítání starších aktivit.') . '#3272';

        echo json_encode($return);
        die;
    }

    echo json_encode($return);
    die;
}



/**
 * nacteni dalsich vysledku ve vyhledavani
 */
if(!empty($_REQUEST['more_search']) && !empty($_REQUEST['type']) && !empty($_REQUEST['start']))
{
    $s = str_replace(array('%','"',"'"), '', strip_tags($_REQUEST['more_search']));
    if(!empty($s))
    {
        $search = search_string_prepare($s);
        $start = intval($_REQUEST['start']);

        $return['status'] = 'ok';

        if($_REQUEST['type'] == "users")
        {
            $searched_users = SearchDAO::lookupUsers($s, $view_more_users, $user_count, $start);
            $return['response'] = print_searched_users($searched_users, $logged_user, $view_more_users);
        }
        elseif($_REQUEST['type'] == "teams")
        {
            $searched_teams = SearchDAO::lookupTeams($s, $view_more_teams, $team_count, $start);
            $return['response'] = print_searched_teams($searched_teams, $logged_user, $view_more_teams);
        }

        echo json_encode($return);
        die;
    }
    else
    {
        $return['message'] = $tr->tr('Zadejte prosím výraz k vyhledávání.');

        echo json_encode($return);
        die;
    }
}



/**
 * potvrzení události
 */
if(isset($_REQUEST['confirmEvent']) && !empty($_REQUEST['confirmEvent']))
{
    $now = date('Y-m-d H:i:s');

	// id udalosti
    $id = explode('-', $_REQUEST['confirmEvent']);

    // musime mit presne dve ID
    if(count($id) != 2)
    {
        $return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // nacteni udalosti
    $event = EventDAO::get($id[0], $id[1]);

    // kontrola zda se povedlo nacist
    if(!($event instanceof Event && !empty($event->id)))
    {
    	$return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // kontrola zda ji muze editovat prihlaseny uzivatel
    if(!$event->userCanEdit($logged_user))
    {
        $return['message'] = $tr->tr('Nemáte oprávnění schválit tuto událost.');

        echo json_encode($return);
        die;
    }

    // pokud tym nema zadnou sezonu, tak nemuze potvrdit udalost
    $season = $logged_user->getActiveTeam()->getActiveSeason();
    if(empty($season))
    {
        $return['message'] = $tr->tr('Nemůžete potvrdit tuto událost, protože nemáte nastavenou hlavní sezónu.');

        echo json_encode($return);
        die;
    }

	// ID protitymu
	$oponent_team_id = 0;

    // info o udalosti
    $einfo = unserialize($event->info);

	// zjisteni jestli potvrzujeme jako domaci nebo hostujici tym
	// domaci
	if($event->team_id == $logged_user->getActiveTeam()->id)
	{
		// ID oponenta
		$oponent_team_id = $event->oponent_id;

		// kontrola zda už není potvrzena
		if(!empty($event->confirmed_home))
		{
			$return['message'] = $tr->tr('Událost již byla Vaším týmem potvrzena.');

	        echo json_encode($return);
	        die;
		}

        // pokud domaci nemaji ulozenou sezonu
        if(empty($event->season_id))
        {
            $event->season_id = $season->id;
        }

        // pokud nemame vubec vyplnenou dochazku, tak pozveme cely tym
        if(empty($einfo[0]['attend_team']) && empty($einfo[0]['attend']))
        {
            $einfo[0]['attend_team'] = 1;
            $einfo[0]['attend'] = array();
        }

		// nastavit jako potvrzene
		$event->confirmed_home = $now;
	}
	// hostujici
	elseif($event->oponent_id == $logged_user->getActiveTeam()->id)
	{
		// ID oponenta
		$oponent_team_id = $event->team_id;

		// kontrola zda už není potvrzena
		if(!empty($event->confirmed_away))
		{
			$return['message'] = $tr->tr('Událost již byla Vaším týmem potvrzena.');

	        echo json_encode($return);
	        die;
		}

        // pokud hoste nemaji ulozenou sezonu
        if(empty($event->oponent_season_id))
        {
            $event->oponent_season_id = $season->id;
        }

        // pokud nemame vubec vyplnenou dochazku, tak pozveme cely tym
        if(empty($einfo[0]['attend_team_away']) && empty($einfo[0]['attend_away']))
        {
            $einfo[0]['attend_team_away'] = 1;
            $einfo[0]['attend_away'] = array();
        }

		// nastavit jako potvrzene
		$event->confirmed_away = $now;
	}
	else
	{
		$return['message'] = $tr->tr('Tuto událost nemůžete potvrdit.');

        echo json_encode($return);
        die;
	}

    // musime si to zase serializovat kvuli ulozeni do DB
    $event->info = serialize($einfo);

	// ulozeni udalost
	if($event->save())
	{
		// odeslat notifikaci druhymu tymu pokud existuje
		if(!empty($oponent_team_id))
		{
			// nacteme soupere
			$oponent = TeamDAO::get($oponent_team_id);

			// pokud existuje
			if($oponent instanceof Team && $oponent->id > 0)
			{
				// nacteme si adminy a jim posleme notifikace
				$admins = $oponent->getAdmins();

				$data = array();
				$time = time();

				foreach($admins as $admin_id)
				{
                    if($admin_id == $logged_user->id)
                    {
                        continue;
                    }

					// loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
	                $data[] = array(
	                    'user_id'    => $admin_id,
	                    'object_id'  => $event->team_id . '_' . $event->id . '_event',
	                    'type_id'    => 'EVENT-CONFIRMED',
	                    'info'       => serialize(
	                    					array(
		                    					'user_team'      => $oponent->id,
                                                'oponent_team'   => $logged_user->getActiveTeam()->id,
		                    					'event_id'       => $event->id
	                    					)
	                    				),
	                    'object_info'=> '',
	                    'timestamp'  => $time,
	                    'read'       => '0'
	                );
	            }

		        // uložíme do DB nové notifikace
		        @Notifications::insertNotifications($data);

			}
			else
			{
				// FIXME nejaky log, jinak je nam to jedno
			}
		}

		// data k vytisteni
		$events = array(
			'events' => array($_REQUEST['confirmEvent'] => $event),
			'future' => '0',
			'past'	 => '0'
		);

		// a nakonec vratime radek s vypisem
		$return = array(
			'status'   => 'ok',
			'response' => printEvents($events, $logged_user->getActiveTeam(), $logged_user, false, false)
		);

		echo json_encode($return);
		die;
	}
	else
	{
		$return['message'] = $tr->tr('Nepovedlo se potvrdit událost. Zkuste to prosím znovu později.');

        echo json_encode($return);
        die;
	}
}



/**
 * odmitnuti události
 */
if(isset($_REQUEST['declineEvent']) && !empty($_REQUEST['declineEvent']))
{
	// id udalosti
    $id = explode('-', $_REQUEST['declineEvent']);

    // musime mit presne dve ID
    if(count($id) != 2)
    {
        $return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // nacteni udalosti
    $event = EventDAO::get($id[0], $id[1], false);

    // kontrola zda se povedlo nacist
    if(!($event instanceof Event && !empty($event->id)))
    {
    	$return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // kontrola zda ji muze editovat prihlaseny uzivatel
    if(!$event->userCanEdit($logged_user))
    {
        $return['message'] = $tr->tr('Nemáte oprávnění upravovat tuto událost.');

        echo json_encode($return);
        die;
    }

    // pokud je udalost jiz potvrzena, tak ji nemuzeme odmitnout
    if($event->isConfirmed() || $event->isConfirmed($logged_user->getActiveTeam()))
    {
        $return['message'] = $tr->tr('Nemáte oprávnění upravovat tuto událost.');

        echo json_encode($return);
        die;
    }

	// ID protitymu
	$oponent_team_id = 0;

	// zjisteni jestli odmitame jako domaci nebo hostujici tym
	// domaci
	if($event->team_id == $logged_user->getActiveTeam()->id)
	{
		// ID oponenta
		$oponent_team_id = $event->oponent_id;

		// kontrola zda už není potvrzena
		if(!empty($event->deleted_home))
		{
			$return['message'] = $tr->tr('Událost již byla smazána.');

	        echo json_encode($return);
	        die;
		}

		// nastavit jako potvrzene
        $event->result_confirmed_away = 1;
        $event->confirmed_away = 1;   //
        $event->result_confirmed_home = 1;
		$event->confirmed_home = 1;
        $event->deleted_home = 1;
	}
	// hostujici
	elseif($event->oponent_id == $logged_user->getActiveTeam()->id)
	{
		// ID oponenta
		$oponent_team_id = $event->oponent_id;

		// kontrola zda už není potvrzena
		if(!empty($event->deleted_away))
		{
			$return['message'] = $tr->tr('Událost již byla smazána');

	        echo json_encode($return);
	        die;
		}

		// nastavit jako potvrzene
		$event->result_confirmed_away = 1;
        $event->confirmed_away = 1;
        $event->result_confirmed_home = 1;
        $event->confirmed_home = 1;
        $event->deleted_away = 1;
	}
	else
	{
		$return['message'] = $tr->tr('Tuto událost nemůžete upravovat.');

        echo json_encode($return);
        die;
	}

	// ulozeni udalost
	if($event->save())
	{
		// odeslat notifikaci druhymu tymu pokud existuje
		if(!empty($oponent_team_id))
		{
			// nacteme soupere
			$oponent = TeamDAO::get($oponent_team_id);

			// pokud existuje
			if($oponent instanceof Team && $oponent->id > 0)
			{
				// nacteme si adminy a jim posleme notifikace
				$admins = $oponent->getAdmins();

				$data = array();
				$time = time();

				foreach($admins as $admin_id)
				{
                    if($admin_id == $logged_user->id)
                    {
                        continue;
                    }

					// loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
	                $data[] = array(
	                    'user_id'    => $admin_id,
	                    'object_id'  => $event->team_id . '_' . $event->id . '_event',
	                    'type_id'    => 'EVENT-DECLINED',
	                    'info'       => serialize(
	                    					array(
                                                'user_team'     => $oponent->id,
		                    					'oponent_team'  => $logged_user->getActiveTeam()->id,
		                    					'event_id'      => $event->id
	                    					)
	                    				),
	                    'object_info'=> '',
	                    'timestamp'  => $time,
	                    'read'       => '0'
	                );
	            }

		        // uložíme do DB nové notifikace
		        @Notifications::insertNotifications($data);

			}
			else
			{
				// FIXME nejaky log, jinak je nam to jedno
			}
		}

		// a nakonec vratime radek s vypisem, ktery je prazdny protoze udalost mazeme
		$return = array(
			'status'   => 'ok',
			'response' => ''
		);

		echo json_encode($return);
		die;
	}
	else
	{
		$return['message'] = $tr->tr('Nepovedlo se odmítnout událost. Zkuste to prosím znovu později.');

        echo json_encode($return);
        die;
	}
}



/**
 * potvrzení události
 */
if(isset($_REQUEST['confirmResult']) && !empty($_REQUEST['confirmResult']))
{
    $now = date('Y-m-d H:i:s');

    // id udalosti
    $id = explode('-', $_REQUEST['confirmResult']);

    // musime mit presne dve ID
    if(count($id) != 2)
    {
        $return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // nacteni udalosti
    $event = EventDAO::get($id[0], $id[1]);

    // kontrola zda se povedlo nacist
    if(!($event instanceof Event && !empty($event->id)))
    {
        $return['message'] = $tr->tr('Zadaná událost neexistuje.');

        echo json_encode($return);
        die;
    }

    // kontrola zda ji muze editovat prihlaseny uzivatel
    if(!$event->userCanEdit($logged_user))
    {
        $return['message'] = $tr->tr('Nemáte oprávnění schválit tuto událost.');

        echo json_encode($return);
        die;
    }

    // ID protitymu
    $oponent_team_id = 0;

    // zjisteni jestli potvrzujeme jako domaci nebo hostujici tym
    // domaci
    if($event->team_id == $logged_user->getActiveTeam()->id)
    {
        // ID oponenta
        $oponent_team_id = $event->oponent_id;

        // kontrola zda už není potvrzena
        if(!empty($event->result_confirmed_home))
        {
            $return['message'] = $tr->tr('Výsledek události již byl Vaším týmem potvrzen.');

            echo json_encode($return);
            die;
        }

        // nastavit jako potvrzene
        $event->result_confirmed_home = $now;
    }
    // hostujici
    elseif($event->oponent_id == $logged_user->getActiveTeam()->id)
    {
        // ID oponenta
        $oponent_team_id = $event->oponent_id;

        // kontrola zda už není potvrzena
        if(!empty($event->result_confirmed_away))
        {
            $return['message'] = $tr->tr('Výsledek události již byl Vaším týmem potvrzen.');

            echo json_encode($return);
            die;
        }

        // nastavit jako potvrzene
        $event->result_confirmed_away = $now;
    }
    else
    {
        $return['message'] = $tr->tr('Tento výsledek události nemůžete potvrdit.');

        echo json_encode($return);
        die;
    }

    // ulozeni udalost
    if($event->save())
    {
        // odeslat notifikaci druhymu tymu pokud existuje
        if(!empty($oponent_team_id))
        {
            // nacteme soupere
            $oponent = TeamDAO::get($oponent_team_id);

            // pokud existuje
            if($oponent instanceof Team && $oponent->id > 0)
            {
                // nacteme si adminy a jim posleme notifikace
                $admins = $oponent->getAdmins();

                $data = array();
                $time = time();

                foreach($admins as $admin_id)
                {
                    if($admin_id == $logged_user->id)
                    {
                        continue;
                    }

                    // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                    $data[] = array(
                        'user_id'    => $admin_id,
                        'object_id'  => $event->team_id . '_' . $event->id . '_event',
                        'type_id'    => 'RESULT-CONFIRMED',
                        'info'       => serialize(
                                            array(
                                                'user_team'     => $oponent->id,
                                                'oponent_team'  => $logged_user->getActiveTeam()->id,
                                                'event_id'      => $event->id
                                            )
                                        ),
                        'object_info'=> '',
                        'timestamp'  => $time,
                        'read'       => '0'
                    );
                }

                // uložíme do DB nové notifikace
                @Notifications::insertNotifications($data);

                // TODO insert object to Wall
                // pokud je udalost potvrzena z obou stran, tak se zobrazi na nastence
                if($event->isConfirmed())
                {
                    $val = array();
                    $obj = array(
                        'object_parent_id'     => $event->team_id . '_' . $event->id . '_event',
                        'object_source_id'     => 0,
                        'object_user_id'       => $logged_user->id,
                        'object_team_id'       => $event->team_id,
                        'object_oponent_id'    => 0,
                        'object_type'          => 'event-result',
                        'object_date_created'  => date('Y-m-d H:i:s'),
                        'object_date_begin'    => date('Y-m-d H:i:s'),
                        'object_value'         => serialize($val)
                    );

                    WallDAO::insertTypeItem($obj);

                    if(!empty($event->oponent_id))
                    {
                        $obj = array(
                            'object_parent_id'     => $event->team_id . '_' . $event->id . '_event',
                            'object_source_id'     => 0,
                            'object_user_id'       => $logged_user->id,
                            'object_team_id'       => $event->oponent_id,
                            'object_oponent_id'    => 0,
                            'object_type'          => 'event-result',
                            'object_date_created'  => date('Y-m-d H:i:s'),
                            'object_date_begin'    => date('Y-m-d H:i:s'),
                            'object_value'         => serialize($val)
                        );

                        WallDAO::insertTypeItem($obj);
                    }
                }

            }
            else
            {
                // FIXME nejaky log, jinak je nam to jedno
            }
        }

        // pokud je udalost potvrzena vcetne vysledku, tak musime pridat objekt na nastenku
        if($event->isConfirmed() && $event->isResultConfirmed())
        {

        }

        // data k vytisteni
        $events = array(
            'events' => array($_REQUEST['confirmResult'] => $event),
            'future' => '0',
            'past'   => '0'
        );

        // a nakonec vratime radek s vypisem
        $return = array(
            'status'   => 'ok',
            'response' => printEvents($events, $logged_user->getActiveTeam(), $logged_user, false, false)
        );

        echo json_encode($return);
        die;
    }
    else
    {
        $return['message'] = $tr->tr('Nepovedlo se potvrdit událost. Zkuste to prosím znovu později.');

        echo json_encode($return);
        die;
    }
}



/**
 * Pripojeni statistiky od fake hrace k realnemu hraci
 */
if(isset($_REQUEST['mergeStats']))
{
    // uzivatel musi byt
    if(!$logged_user->getActiveTeam()->isAdmin($logged_user))
    {
        $return['message'] = $tr->tr('Nemůžete měnit soupisku tohoto týmu.');

        echo json_encode($retun);
        die;
    }

    // musime mit zadany od ktereho hrace ty statistiky davame pryc a komu je budeme pridavat
    if(!empty($_REQUEST['fromPlayer']) && !empty($_REQUEST['toPlayer']))
    {
        $from = explode('-', $_REQUEST['fromPlayer']);

        $return['message'] = $tr->tr('Nastala chyba při slučování statistik.');

        if(count($from) != 2)
        {
            echo json_encode($return);
            die;
        }

        // kontrola zda sedi hash
        if($from[1] != getRecipientHash($from[0]))
        {
            echo json_encode($return);
            die;
        }

        // kontrola zda je hracem naseho tymu, zda opravdu existuje a hlavne ze je FAKE - od jinyho nemuzeme brat statistiky pryc
        $fromPlayer = UserDAO::get($from[0]);
        if(!$fromPlayer instanceOf User || empty($fromPlayer->id) || !$logged_user->getActiveTeam()->isPlayer($fromPlayer) || !$fromPlayer->isFakeUser())
        {
            echo json_encode($return);
            die;
        }




        $to = explode('-', $_REQUEST['toPlayer']);
        if(count($to) != 2)
        {
            echo json_encode($return);
            die;
        }

        // kontrola zda sedi hash
        if($to[1] != getRecipientHash($to[0]))
        {
            echo json_encode($return);
            die;
        }

        // kontrola zda je hracem naseho tymu
        $toPlayer = UserDAO::get($to[0]);
        if(!$toPlayer instanceof User || empty($toPlayer->id) || !$logged_user->getActiveTeam()->isPlayer($toPlayer))
        {
            echo json_encode($return);
            die;
        }

        // potřebujeme sport, ve kterém tým hraje
        $sport_id = $logged_user->getActiveTeam()->sport_id;
        $sport = SportDAO::get($sport_id);

        // presuneme vsechny statistiky od puvodniho k tomu novemu
        StatsDAO::moveStatsToPlayer($fromPlayer->id, $toPlayer->id, $sport);

        // a jeste musime smazat hrace
        UserDAO::delete($fromPlayer->id);

        // return
        $return = array(
            'status'   => 'ok',
            'response' => '<div class="msg noClose done"><h3 class="title">'.$tr->tr('Spárování účtů bylo dokončeno!').'</h3><p class="text">'.$tr->tr('Statistiky z účtu').' "'.$fromPlayer->getName().'" '.$tr->tr('byly převedeny na účet').' "'.$toPlayer->getName().'".</p></div>'
        );

        echo json_encode($return);
        die;
    }
}


if(isset($_REQUEST['statsFilter']) && $_REQUEST['statsFilter'] == 1 && !empty($_REQUEST['fteam']))
{
	$team = explode('-', $_REQUEST['fteam']);

	if(count($team) != 2)
	{
		$return['message'] = $tr->tr('Nastala chyba při nastavování filtru.');

		echo json_encode($return);
		die;
	}

	if($team[1] != getTeamHash($team[0]))
	{
		$return['message'] = $tr->tr('Nastala chyba při nastavování filtru.');

		echo json_encode($return);
		die;
	}

	$team = TeamDAO::get($team[0]);

	if(!$team instanceof Team || empty($team->id) || !$team->userCanViewModule('stats', $logged_user))
	{
		$return['message'] = $tr->tr('Nastala chyba při nastavování filtru.');

		echo json_encode($return);
		die;
	}

	$team_seasons = $team->getSeasons(true);
	$season_id = intval($_REQUEST['fyear']);

	if(!in_array($season_id, array_keys($team_seasons)) && !empty($season_id))
	{
		$return['message'] = $tr->tr('Nastala chyba při nastavování filtru.');

		echo json_encode($return);
		die;
	}

    $positions 	= SportDAO::getPositions($team->sport_id);
	$roster 	= Rosters::getTeamRoster($team->id);
	$sport 		= SportDAO::get($team->sport_id);
	$team_stats = StatsDAO::getAllTeamStats($team, $sport, $season_id);

    $return = array(
        'status'   => 'ok',
        'response' => print_stats($positions, $roster, $sport, $team_stats)
    );

    echo json_encode($return);
    die;
}


/**
 * přidání admina k týmu
 */
if(isset($_REQUEST['add_admin']) && !empty($_REQUEST['admin']['id']))
{
	$return['message'] = $tr->tr('Nastala chyba při přidávání admina');

	// prihlaseny uzivatel musi byt admin
	if(!$logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		echo json_encode($return);
        die;
	}

	$user = explode('-', $_REQUEST['admin']['id']);

	// kontrola uzivatele
    if(count($user) != 2)
    {
        echo json_encode($return);
        die;
    }

    // kontrola zda sedi hash
    if($user[1] != getRecipientHash($user[0]))
    {
        echo json_encode($return);
        die;
    }


    // kontrola zda je hracem naseho tymu
    // kontrola zda hrac existuje
    // zda jeste neni admin
    // a zda neni fake user
    $user = UserDAO::get($user[0]);
    if(!$user instanceof User || empty($user->id) || !$logged_user->getActiveTeam()->isPlayer($user))
    {
        echo json_encode($return);
        die;
    }

    // nesmí být fake user
    if($user->isFakeUser())
    {
    	$return['message'] = $tr->tr('Tento uživatel se nemůže stát adminem týmu.');

    	echo json_encode($return);
        die;
    }

    // nesmi byt admin
    if($logged_user->getActiveTeam()->isAdmin($user))
    {
    	$return['message'] = $tr->tr('Uživatel již je adminem tohoto týmu.');

    	echo json_encode($return);
        die;
    }

    // pridani admina
    if(TeamDAO::addAdmin($user->id, $logged_user->getActiveTeam()->id))
    {
    	$select = '';
    	$response = '';
    	foreach($logged_user->getActiveTeam(true)->getAdmins(true) as $admin_id)
    	{
    		$admin = UserDAO::get($admin_id);

    		if(!$admin->isFakeUSer())
    		{
	    		$select .= '<option value="'.$admin_id.'-'.getRecipientHash($admin_id).'">'.$admin->getName().'</option>';
	    	}
    		$response .= '
    			<p data-id="'.$admin_id.'-'.getRecipientHash($admin_id).'" class="entries cleaned">
                    <label class="adminName">'.$admin->getName().'</label>
                    <input type="text" name="" class="text forBlind">
                    <span class="deleteBox controls">
                        <a title="Smazat admina" class="delete icon delete" href="#"></a>
                    </span>
                </p>
    		';
    	}

    	$return = array(
    		'status' 	=> 'ok',
    		'response' 	=> $response,
    		'select' 	=> $select
		);
    }

    echo json_encode($return);
    die;
}



/**
 * odebrání admina z týmu
 */
if(isset($_REQUEST['delete_admin']) && !empty($_REQUEST['admin']['id']))
{
	$return['message'] = $tr->tr('Nastala chyba při odebírání admina');

	// prihlaseny uzivatel musi byt admin
	if(!$logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		echo json_encode($return);
        die;
	}

	$user = explode('-', $_REQUEST['admin']['id']);

	// kontrola uzivatele
    if(count($user) != 2)
    {
        echo json_encode($return);
        die;
    }

    // kontrola zda sedi hash
    if($user[1] != getRecipientHash($user[0]))
    {
        echo json_encode($return);
        die;
    }


    // kontrola zda je hracem naseho tymu
    // kontrola zda hrac existuje
    // zda jeste neni admin
    // a zda neni fake user
    $user = UserDAO::get($user[0]);
    if(!$user instanceof User || empty($user->id) || !$logged_user->getActiveTeam()->isPlayer($user) || $user->isFakeUser())
    {
        echo json_encode($return);
        die;
    }

    // nemuze odebrat sam sebe
    if($logged_user->id == $user->id)
    {
    	$return['message'] = $tr->tr('Nemůžete odebrat sám sebe.');

		echo json_encode($return);
        die;
    }

    // musí byt admin
    if(!$logged_user->getActiveTeam()->isAdmin($user))
    {
    	$return['message'] = $tr->tr('Uživatel není adminem tohoto týmu.');

    	echo json_encode($return);
        die;
    }

    // nesmí být poslední admin
    if(count($logged_user->getActiveTeam()->getAdmins()) == 1)
    {
    	$return['message'] = $tr->tr('U týmu musí zůstat alespoň jeden admin.');

    	echo json_encode($return);
        die;
    }

    // nemuzeme smazat majitele tymu
    if($logged_user->getActiveTeam()->user_id == $user->id)
    {
    	$return['message'] = $tr->tr('Nemůžete odebrat majitele týmu.');

    	echo json_encode($return);
        die;
    }

    if(TeamDAO::deleteAdmin($user->id, $logged_user->getActiveTeam()->id))
    {
    	$select = '';
    	foreach($logged_user->getActiveTeam(true)->getAdmins(true) as $admin_id)
    	{
    		$admin = UserDAO::get($admin_id);

    		$select .= '<option value="'.$admin_id.'">'.$admin->getName().'</option>';
    	}

    	$return = array(
    		'status' => 'ok',
    		'select' => $select
		);
    }

    echo json_encode($return);
    die;
}


if(isset($_REQUEST['recommend-email']))
{
    $return['message'] = $tr->tr('Nastala chyba při odesílání pozvánky.');

    if(!filter_var($_REQUEST['recommend-email'], FILTER_VALIDATE_EMAIL))
    {
        echo json_encode($return);
        die;
    }

    $user = UserDAO::getByEmail($_REQUEST['recommend-email']);

    if($user instanceof User && $user->id > 0)
    {
        $return = array(
            'status'   => 'ok',
            'response' => '<p class="msg new error noClose">'.$tr->tr('E-mail je již zaregistrován!').'</p>'
        );

        echo json_encode($return);
        die;
    }


    if(SendMail::userRecommend($_REQUEST['recommend-email'], $logged_user))
    {
        $return = array(
            'response'  => '<p class="msg done noClose">' . $tr->tr('Pozvánka byla v pořádku odeslána, děkujeme!') . '</p>',
            'status'    =>'ok'
        );
    }

    echo json_encode($return);
    die;
}



if(!empty($_REQUEST['getHelp']) && !empty($_REQUEST['action']))
{
    $help = array();
    require(BLOCK_PATH . 'help-overlay.php');

    $return = array(
        'status' => 'ok'
    );

    if(count($logged_user->getTeamList()) == 0)
    {
        $return['response'] = strval(@$help[$_REQUEST['action']]['without-team']);
    }
    else
    {
        $return['response'] = strval(@$help[$_REQUEST['action']]['with-team']);
    }

    echo json_encode($return);
    die;
}


// prepinani tymu
if(!empty($_GET['getSwitchTeams']))
{
    echo '<div class="fancybox-popup">';
    echo '<h3 class="title">Zvolte váš aktivní tým</h3>';
    echo '<ul class="list">';

    $i = 0;
    foreach($logged_user->getTeamList() as $team)
    {
        $class = '';
        if($i == 0)
        {
            $class = 'first';
        }
        $sport = SportDAO::get($team->sport_id);
        echo '<li class="item '.$class.' cleaned">
            <p class="picture"><a href="#" id="team_'.$team->id.'" class="switchTeam cleaned" title="'.$tr->tr('Nastavit tým').' '.$team->getName().' '.$tr->tr('jako aktivní').'"><img src="'.$team->getTeamLogo('medium').'" alt="'.$team->getName().'" width="46" height="46" /></a></p>
                <h5 class="name"><a href="#" id="team_'.$team->id.'" class="switchTeam cleaned" title="'.$tr->tr('Nastavit tým').' '.$team->getName().' '.$tr->tr('jako aktivní').'">'.$team->getName().'</a></h5>
                <h5 class="team">'.$sport->name.'</h5>
        </li>';

        $i++;
    }
    echo '</ul>';
    //echo printLikeList($likes, $_REQUEST['object_id'], true);

    echo '</div>';
    echo '<script type="text/javascript">switchTeam();</script>';

    die;
}



if(!empty($_REQUEST['feedback']) && !empty($_REQUEST['feedback_form']['text']))
{

    if(SendMail::sendFeedback($_REQUEST['feedback_form']['text'], $logged_user))
    {
        $return = array(
            'status' =>'ok',
            'response' => '<div class="msg noClose done"><h3 class="title">Hotovo!</h3><p class="text">Děkujeme, že se nám snažíte pomoci vylepšit myteamworld.com</p></div>'
        );
    }
    else
    {
        $return['message'] = $tr->tr('Nastala chyba při odesílání feedbacku. Zkuste to prosím znovu později.');
    }
    echo json_encode($return);
    die;
}


if(!empty($_REQUEST['change_charges_season']) && $logged_user->getActiveTeam()->userCanViewModule('cashbox', $logged_user) && $logged_user->getActiveTeam()->isAdmin($logged_user))
{
	$return = array(
		'status' => 'ok',
		'response' => print_pricelist_popup($_REQUEST['season'], false)
	);

	echo json_encode($return);
	die;
}

if(!empty($_REQUEST['delete_charge']) && $logged_user->getActiveTeam()->userCanViewModule('cashbox', $logged_user) && $logged_user->getActiveTeam()->isAdmin($logged_user))
{
	if(CashboxDAO::delete($_REQUEST['charge']['id']))
	{
		$return = array(
			'status' => 'ok'
		);
	}
	else
	{
		$return = array(
			'status' => 'error',
			'message' => $tr->tr('Nastala chyba při mazání poplatku.')
		);
	}

	echo json_encode($return);
	die;
}

if(!empty($_REQUEST['manage_charge']) && $logged_user->getActiveTeam()->userCanViewModule('cashbox', $logged_user) && $logged_user->getActiveTeam()->isAdmin($logged_user))
{
	// editace
	if(!empty($_REQUEST['charge']['id']))
	{
		// FIXME season check
		// 
		$data = array(
			'id'		=> $_REQUEST['charge']['id'],
			'team_id'	=> $logged_user->getActiveTeam()->id,
			'season_id' => $_REQUEST['season'],
			'name'		=> $_REQUEST['charge']['name'],
			'price'		=> $_REQUEST['charge']['price']
		);

		if(CashboxDAO::update($data))
		{
			$return = array(
                'status'    => 'ok-edit',
                'response'	=> ''
            );
		}
	}
	// vlozeni
	else
	{
		// FIXME season check
		// 
		$data = array(
			'team_id'	=> $logged_user->getActiveTeam()->id,
			'season_id' => $_REQUEST['season'],
			'name'		=> $_REQUEST['charge']['name'],
			'price'		=> $_REQUEST['charge']['price']
		);

		if(CashboxDAO::insert($data))
		{
			$return = array(
                'status'    => 'ok-new',
                'response'	=> print_pricelist_popup($_REQUEST['season'], false),
                'select'	=> ''
                //'select'    => print_season_select($team_seasons)
            );
		}
	}

	echo json_encode($return);
	die;
}


if(!empty($_REQUEST['feedbackForm']))
{
    echo '
        <div id="feddbackPopup" class="fancybox-popup">
            <h3 class="title">Feedback</h3>

            <h4 class="headline">Nové funkce, na kterých pracujeme</h4>
            <ul class="list functions">';

        echo '<li class="">Týmová pokladna - tabulka s přehledem financí vašeho klubu</li>';
        //echo '<li class="">Katalog týmů - vyhledávání týmů podle kraje, města, sportu apod.</li>'; // <span class="progress">10%</span>
        //echo '<li class="">Kalendář událostí uživatele - přehled všech událostí na které je uživatel pozvaný</li>';
        echo '</ul>

            <h4 class="headline">Chyby, o kterých víme a na kterých pracujeme</h4>
            <ul class="list error">
                <li class="">Není možnost smazat celý tým';//<span class="progress">10%</span></li>
    echo '
            </ul>

            <h3 class="bigTitle">Nahlásit novou chybu nebo navrhnout novou funkci</h3>

            <form  class="popUpForm">
                <fieldset>
                    <legend class="forBlind">Feedback</legend>

                    <p class="entries">
                        <label>Popište, prosím, co nejpodrobněji chybu na stránce nebo co byste chtěli změnit</label><br />
                        <textarea class="text" rows="3" cols="5" name="feedback_form[text]"></textarea>
                        <span class="errorMsg">Vyplňte, prosím, toto pole.</span>
                    </p>';
                    /*
                    <p class="entries">
                        <label>Přiložte, prosím, screenshot chyby na stránce</label><br />
                        <input id="uploadScreenshot" class="screenshot" type="file" name="feedback[screenshot]" />
                    </p>
                    */

    echo '
                    <p class="entries submit">
                        <button type="submit" name="send_feedback">odeslat feedback</button>
                    </p>
                </fieldset>
            </form>

        </div>
    ';

    die;
}

/* <!--<?php /* PREHLED * / ?>
            <div id="summaryBox" class="tab cleaned">
                <div class="left">
                    <div class="summaryEdit">
                        <fieldset>

                            <p class="entries cleaned">
                                <select class="time" data-name="xxx">
                                    <option selected="selected" value="1">1´</option>
                                    <option value="2">2´</option>
                                    <option value="3">33´</option>
                                    <option value="45+2">45+2´</option>
                                </select>
                                <select class="type" data-name="zzz">
                                    <option selected="selected" value="">Gól ze hry</option>
                                    <option value="">Gól z přímého kopu</option>
                                    <option value="">Gól z penalty</option>
                                    <option value="">Neproměněná penalta</option>
                                    <option value="">Žlutá karta</option>
                                    <option value="">Červená karta po 2 žluté</option>
                                    <option value="">Červená karta</option>
                                    <option value="">Střídání</option>
                                </select>
                                <select class="first player" data-name="yyy">
                                    <option selected="selected" value="0">Autor gólu</option>
                                    <option value="">Bíza M.</option>
                                    <option value="">Nowak J.</option>
                                    <option value="">Maxmiliánský M.</option>
                                </select>
                                <select class="second player" data-name="yxz">
                                    <option selected="selected" value="0">Asistence</option>
                                    <option value="">Bíza M.</option>
                                    <option value="">Nowak J.</option>
                                    <option value="">Maxmiliánský M.</option>
                                </select>
                            </p>

                            <p class="entries cleaned">
                                <select class="time" data-name="xxx">
                                    <option selected="selected" value="1">1´</option>
                                    <option value="2">2´</option>
                                    <option value="3">33´</option>
                                    <option value="45+2">45+2´</option>
                                </select>
                                <select class="type" data-name="zzz">
                                    <option selected="selected" value="">Gól ze hry</option>
                                    <option value="">Gól z přímého kopu</option>
                                    <option value="">Gól z penalty</option>
                                    <option value="">Neproměněná penalta</option>
                                    <option value="">Žlutá karta</option>
                                    <option value="">Červená karta po 2 žluté</option>
                                    <option value="">Červená karta</option>
                                    <option value="">Střídání</option>
                                </select>
                                <select class="first player small" data-name="yyy">
                                    <option selected="selected" value="0">Autor gólu</option>
                                    <option value="">Bíza M.</option>
                                    <option value="">Nowak J.</option>
                                    <option value="">Maxmiliánský M.</option>
                                </select>
                                <select class="second player small" data-name="yxz">
                                    <option selected="selected" value="0">Asistence</option>
                                    <option value="">Bíza M.</option>
                                    <option value="">Nowak J.</option>
                                    <option value="">Maxmiliánský M.</option>
                                </select>
                                <select class="third player small" data-name="yxz">
                                    <option selected="selected" value="0">Asistence</option>
                                    <option value="">Bíza M.</option>
                                    <option value="">Nowak J.</option>
                                    <option value="">Maxmiliánský M.</option>
                                </select>
                            </p>

                            <p class="more addSummary">
                                <a href="#" class="button buttonB icon add">přidat další událost</a>
                            </p>

                        </fieldset>
                    </div>
                </div>

                <div class="right">
                    <div class="summaryEdit">
                        <fieldset>

                            <p class="entries cleaned">
                                <select class="time" data-name="xxx">
                                    <option selected="selected" value="1">1´</option>
                                    <option value="2">2´</option>
                                    <option value="3">33´</option>
                                    <option value="45+2">45+2´</option>
                                </select>
                                <select class="type" data-name="zzz">
                                    <option selected="selected" value="">Gól ze hry</option>
                                    <option value="">Gól z přímého kopu</option>
                                    <option value="">Gól z penalty</option>
                                    <option value="">Neproměněná penalta</option>
                                    <option value="">Žlutá karta</option>
                                    <option value="">Červená karta po 2 žluté</option>
                                    <option value="">Červená karta</option>
                                    <option value="">Střídání</option>
                                </select>
                                <select class="first player" data-name="yyy">
                                    <option selected="selected" value="0">Autor gólu</option>
                                    <option value="">Bíza M.</option>
                                    <option value="">Nowak J.</option>
                                    <option value="">Maxmiliánský M.</option>
                                </select>
                                <select class="second player" data-name="yxz">
                                    <option selected="selected" value="0">Asistence</option>
                                    <option value="">Bíza M.</option>
                                    <option value="">Nowak J.</option>
                                    <option value="">Maxmiliánský M.</option>
                                </select>
                            </p>

                            <p class="more addSummary">
                                <a href="#" class="button buttonB icon add">přidat další událost</a>
                            </p>

                        </fieldset>
                    </div>
                </div>

                <p class="nextLink finish">
                    <a class="button buttonC">uložit a dokončit</a>
                </p>
            </div><!-- #summaryBox -->
 */