<?php

Header("Content-Type:application/json; charset=utf-8");

require_once 'include/config.php';
require_once 'include/functions.php';
require_once 'include/init.php';
require_once 'include/preload.php';
require_once 'action/'.$page.'.action.php';
require_once 'ajax.functions.php';

// pokud posle nazev nove galerie
if(!empty($_POST['add_gallery']))
{
	$team_id = $logged_user->getActiveTeam()->id;
	if(!empty($team_id) && $logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		$data = array(
			'name' 		=> $_POST['add_gallery'],
			'team_id'	=> $team_id
		);
		$return = GalleryDAO::insertGallery($data);
		
		if(is_array($return) && !empty($return))
		{
			$return['status']	= 'ok';
			$return['hash'] 	= getGalleryHash($team_id, $return['id']);
		}
		else
		{
			$return = array(
				'status'  => 'error',
				'message' => $tr->tr('Nastala chyba při ukládání galerie. Zkuste to prosím znovu později.')
			);
		}
	}
	else
	{
		$return = array(
			'status'  => 'error',
			'message' => $tr->tr('Nemůžete vytvořit novou galerie pokud nejde administrátorem týmu.')
		);
	}
	
	echo json_encode($return);
	die;
}


// zmena nazvu
if(!empty($_POST['name_gallery']) && !empty($_POST['gallery']) && !empty($_POST['name']))
{
	$team_id = $logged_user->getActiveTeam()->id;

	$gallery = GalleryDAO::getGallery($team_id, $_POST['gallery']);
	
	if(!empty($team_id) && $logged_user->getActiveTeam()->isAdmin($logged_user) && $gallery instanceof Gallery)
	{
		$data = array(
			'team_id'	=> $logged_user->getActiveTeam()->id, 
			'name'		=> $_POST['name'],
			'id'		=> $_POST['gallery']
		);
		if(GalleryDAO::updateGallery($data))
		{
			$return = array(
				'status'	=> 'ok',
				'response'  => $tr->tr('Zobrazit galerii') . ' ' . $_POST['name']
			);
		}
		else
		{
			$return = array(
				'status'	=> 'error',
				'message'	=> $tr->tr('Nastala chyba při editaci galerie. Zkuste to později znovu.')
			);
		}
	}
	else
	{
		$return = array(
			'status'	=> 'error',
			'message'	=> $tr->tr('Nemůžete editovat tuto galerii.')
		);
	}

	echo json_encode($return);
	die;
}



// zmena nazvufotky
if(!empty($_POST['name_photo']) && !empty($_POST['gallery']) && !empty($_POST['photo']) && !empty($_POST['name']))
{
	$team_id = $logged_user->getActiveTeam()->id;
	
	$gallery = GalleryDAO::getGallery($team_id, $_POST['gallery']);

	if(!empty($team_id) && $logged_user->getActiveTeam()->isAdmin($logged_user) && $gallery instanceof Gallery)
	{
		$data = array(
			'team_id'	=> $logged_user->getActiveTeam()->id, 
			'id'		=> $_POST['photo'],
			'name'		=> $_POST['name'],
			'gallery_id' => $_POST['gallery']
		);
		if(GalleryDAO::updateGalleryItem($data))
		{
			$return = array(
				'status'	=> 'ok',
				'response'  => $_POST['name']
			);
		}
		else
		{
			$return = array(
				'status'	=> 'error',
				'message'	=> $tr->tr('Nastala chyba při editaci fotografie. Zkuste to později znovu.')
			);
		}
	}
	else
	{
		$return = array(
			'status'	=> 'error',
			'message'	=> $tr->tr('Nemůžete editovat tuto fotografii.')
		);
	}

	echo json_encode($return);
	die;
}

// poslali nam nejakou fotku
if(count($_FILES) > 0 && !empty($_POST['gallery_id']) && !empty($_POST['gallery_hash']))
{
	$return = array(
		'status'	=> 'error'
	);
	$team_id = $logged_user->getActiveTeam()->id;
	
	if(!empty($team_id) && $logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		if($_POST['gallery_hash'] != getGalleryHash($team_id, $_POST['gallery_id']))
		{
			$return['message'] = $tr->tr('Nemůžete přidat fotografie.');
		}
		// muzeme nahrat fotky
		else
		{
			// pokusime se uploadovat fotky na server
			$result = GalleryDAO::uploadPhotos($_FILES, $team_id, $_POST['gallery_id']);

			if($result)
			{
				$photolist 	= GalleryDAO::getPhotoList($team_id, $_POST['gallery_id']);

				// data pro vlozeni fotky
				$data = array(
					'team_id'		=> $team_id,
					'gallery_id'	=> $_POST['gallery_id'],
					'filename'		=> $result['filename'],
					'extension'		=> $result['extension'],
					'description'	=> $result['description'],
					'main'			=> (count($photolist) ? '0' : '1'),
					'date'			=> date('Y-m-d H:i:s')
				);

				$res = GalleryDAO::insertItem($data);
				$data['id'] = $res['id'];

				$gallery = GalleryDAO::getGallery($team_id, $_POST['gallery_id']);
				$return = array(
					'status' 	=> 'ok',
					'response'  => (count($photolist) ? '' : print_gallery($gallery, $logged_user->getActiveTeam())),
					'photo_response' => print_gallery_item($gallery, $data, $logged_user->getActiveTeam())
				);



				/** pridame prispevek na nastenku */
				$obj = WallDAO::getLastTypeObject('gallery-insert', $team_id, $_POST['gallery_id']);

				$photo_id = $data['id'];
                if($obj)
                {
                    $obj['object_value'] = unserialize($obj['object_value']);

                    // ulozime pouze pokud uz to tam neni
                    if(!isset($obj['object_value']['photos'][$photo_id]))
                    {
                        $obj['object_value']['photos'][$photo_id] = array(
                        	'id'			=> $data['id'],
                        	'description'	=> $data['description'],
                        	'filename'		=> $data['filename'],
                        	'extension' 	=> $data['extension']
                    	);
                    }
                    $obj['object_value'] = serialize($obj['object_value']);
                    $obj['object_date_begin'] = date('Y-m-d H:i:s');

                    WallDAO::updateTypeItem($obj);
                }
                else
                {
                	$val = array(
                		'photos'	=> array(
                			$photo_id => array(
                				'id'			=> $data['id'],
                        		'description'	=> $data['description'],
                				'filename'		=> $data['filename'],
                        		'extension'		=> $data['extension']
            				)
            			)
            		);
                    $obj = array(
                        'object_parent_id'     => $_POST['gallery_id'],
                        'object_source_id'     => 0,
                        'object_user_id'       => $logged_user->id,
                        'object_team_id'       => $team_id,
                        'object_oponent_id'    => 0,
                        'object_type'          => 'gallery-insert',
                        'object_date_created'  => date('Y-m-d H:i:s'),
                        'object_date_begin'    => date('Y-m-d H:i:s'),
                        'object_value'         => serialize($val)
                    );

                    WallDAO::insertTypeItem($obj);
                }


			}
		}
	}
	else
	{
		$return['message'] = $tr->tr('Nemůžete přidat fotografie, pokud nejste administrátorem týmu.');
	}

	echo json_encode($return);
	die;
}



// zmena poradi fotek v galerii
if(!empty($_POST['change_items_order']) && !empty($_POST['gallery_id']))
{
	$return = array(
		'status' => 'error'
	);

	$items = explode(',', $_POST['change_items_order']);

	if(!is_array($items))
	{
		$return['message'] = $tr->tr('Nemůžete změnit pořadí fotek v galerii.');

		echo json_encode($return);
		die;
	}

	if(!empty($logged_user->getActiveTeam()->id) && $logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		$gallery = GalleryDAO::getGallery($logged_user->getActiveTeam()->id, $_POST['gallery_id']);

		if(!$gallery || !$gallery instanceof Gallery)
		{
			$return['message'] = $tr->tr('Nemůžete změnit pořadí fotek v galerii.');

			echo json_encode($return);
			die;		
		}

		$db = new Database();
		//$items = array_reverse($items);
		foreach($items as $key => $id)
		{
			echo $sql = 'UPDATE `gallery_items`
					SET `order` = '.intval($key * 10).'
					WHERE `id` = '.intval($id).' AND `gallery_id` = ' . intval($_POST['gallery_id']) . ' AND `team_id` = ' . $logged_user->getActiveTeam()->id;

			$db->query($sql);
		}

		$return = array(
			'status' => 'ok'
		);
	}
	else
	{
		$return['message'] = $tr->tr('Nemůžete měnit pořadí fotek.');
	}

	echo json_encode($return);
	die;
}


// zmena poradi fotek v galerii
if(!empty($_POST['change_gallery_order']))
{
	$return = array(
		'status' => 'error'
	);

	$items = explode(',', $_POST['change_gallery_order']);

	if(!is_array($items))
	{
		$return['message'] = $tr->tr('Nemůžete změnit pořadí fotek v galerii.');

		echo json_encode($return);
		die;
	}

	if(!empty($logged_user->getActiveTeam()->id) && $logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		$db = new Database();
		$items = array_reverse($items);
		foreach($items as $key => $id)
		{
			$sql = 'UPDATE `gallery`
					SET `order` = ' . intval($key * 10) . '
					WHERE `id` = '.intval($id).' AND `team_id` = ' . $logged_user->getActiveTeam()->id;

			$db->query($sql);
		}

		$return = array(
			'status' => 'ok'
		);
	}
	else
	{
		$return['message'] = $tr->tr('Nemůžete měnit pořadí fotek.');
	}
}



// zmena poradi fotek v galerii
if(!empty($_POST['delete_gallery']))
{
	$return = array(
		'status' => 'error'
	);

	if(!empty($logged_user->getActiveTeam()->id) && $logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		GalleryDAO::deleteGallery($logged_user->getActiveTeam()->id, $_POST['delete_gallery']);

		$return = array(
			'status'	=> 'ok'
		);
	}
	else
	{
		$return['message'] = $tr->tr('Nemůžete smazat tuto galerii.');
	}

	echo json_encode($return);
	die;
}


// zmena poradi fotek v galerii
if(!empty($_POST['delete_photo']) && !empty($_POST['gallery_id']))
{
	$return = array(
		'status' => 'error'
	);

	$gallery = GalleryDAO::getGallery($logged_user->getActiveTeam()->id, $_POST['gallery_id']);

	if(!empty($logged_user->getActiveTeam()->id) && $logged_user->getActiveTeam()->isAdmin($logged_user) && $gallery instanceof Gallery)
	{
		if(GalleryDAO::deleteItem($logged_user->getActiveTeam()->id, $_POST['gallery_id'], $_POST['delete_photo']))
		{
			$return = array(
				'status'	=> 'ok'
			);
		}
		else
		{
			$return = array(
				'status'	=> 'error',
				'message'	=> $tr->tr('Nastala chyba při mazání fotky, zkuste to prosím později znovu.')
			);
		}
	}
	else
	{
		$return['message'] = $tr->tr('Nemůžete smazat tuto fotku.');
	}

	echo json_encode($return);
	die;
}



// zmena poradi fotek v galerii
if(!empty($_POST['main_photo']) && !empty($_POST['gallery_id']) && !empty($_POST['photo_id']))
{
	$return = array(
		'status' => 'error'
	);

	if(!empty($logged_user->getActiveTeam()->id) && $logged_user->getActiveTeam()->isAdmin($logged_user))
	{
		GalleryDAO::setMainPhoto($logged_user->getActiveTeam()->id, $_POST['gallery_id'], $_POST['photo_id']);

		$return = array(
			'status'	=> 'ok'
		);
	}
	else
	{
		$return['message'] = $tr->tr('Nemůžete měnit nastavení této galerie.');
	}

	echo json_encode($return);
	die;
}