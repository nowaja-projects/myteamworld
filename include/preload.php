<?php /* ********** translated ************ */ ?>

<?php
setlocale(LC_ALL, 'cs_CZ.UTF-8');

// globalni objekt, ktery zajistuje praci s notifikacemi
$gnot = new Notifications();

//Zjisteni akce
$action = CHtml::checkAndSetGet('action', 'main');

// Subakce se používá jen v týmovém menu
$subaction = CHtml::checkAndSetGet('subaction', '');

// překlady
$lang = 'cz';
$tr = new Translation($action, $lang);

// seznam modulu, vcetne nastaveni jejich chovani
$global_modules = array(
    'press-releases'   => array(
        'name'   => $tr->tr('Tiskové zprávy'),
        'values' => array(
            'all'       => 'Aktivní, všichni uživatelé'
        ),
        'default'   => 'all'
    ),
    'events'    => array(
        'name'   => $tr->tr('Události'),
        'values' => array(
            'all'       => $tr->tr('Aktivní, všichni uživatelé'),
            'no'        => $tr->tr('Neaktivní')
        ),
        'default'   => 'all'
    ),
    'roster'   => array(
        'name'   => $tr->tr('Soupiska'),
        'values' => array(
            'all'       => $tr->tr('Aktivní, všichni uživatelé')
        ),
        'default'   => 'all'
    ),
    'stats'   => array(
        'name'   => $tr->tr('Statistiky'),
        'help'   => $tr->tr('Vyžaduje aktivní modul události'),
        'values' => array(
            'all'       => $tr->tr('Aktivní, všichni uživatelé'),
            'members'   => $tr->tr('Aktivní, pouze členové týmu'),
            'no'        => $tr->tr('Neaktivní')
        ),
        'default'   => 'all'
    ),
    'attendance'   => array(
        'name'   => $tr->tr('Docházka'),
        'help'   => $tr->tr('Vyžaduje aktivní modul události'),
        'values' => array(
            'members'   => $tr->tr('Aktivní, pouze členové týmu'),
            'no'        => $tr->tr('Neaktivní')
        ),
        'default'   => 'members'
    ),
    /*'cashbox'   => array(
        'name'   => $tr->tr('Pokladna'),
        'values' => array(
            'all'   => $tr->tr('Aktivní, všichni uživatelé'),
            'members'   => $tr->tr('Aktivní, pouze členové týmu'),
            'no'        => $tr->tr('Neaktivní')
        ),
        'default'   => 'all'
    ),*/
    /*'tasks'   => array(
        'name'   => $tr->tr('Úkoly'),
        'values' => array(
            'members'   => $tr->tr('Aktivní, pouze členové týmu'),
            'no'        => $tr->tr('Neaktivní')
        )
    ),*/
    'gallery'   => array(
        'name'   => $tr->tr('Galerie'),
        'values' => array(
            'all'       => $tr->tr('Aktivní, všichni uživatelé'),
            'members'   => $tr->tr('Aktivní, pouze členové týmu'),
            'no'        => $tr->tr('Neaktivní')
        ),
        'default'   => 'all'
    )
);

// nacteme seznam sportu - globalni promenna
$gsports = SportDAO::getSelectList();
$gadjectives = SportDAO::getAdjectiveList();

// ODHLÁŠENÍ
$logout = CHtml::checkAndSetGet('logout', false);
if($logout !== false)
{
    // odstranime session
    $_S->sunset('logged');
    $_S->sunset('filter');
    $_project['session']->sunset('logged');
    $_project['session']->sunset('filter');

    // odstranime globalni promennou
    unset($logged_user);

    // zneplatnime COOKIE
    setcookie(COOKIENAME, '', time() - 3600, '/'); // unset cookie
    unset($_COOKIE[COOKIENAME]);

    // info
    $_project['message']->addDone($tr->tr('Byl(a) jste úspěšně odhlášen(a).'));
    $_project['message']->saveMessages();

    // presmerování na uvod
    header('Location: ' . PATH_WEB_ROOT);
    die;
}

$logged_user = new User();

/**
 * Musi to byt tady a ne v registration.action.php, protoze se v ajaxu vola main.action.php a uzivatel neni v ajaxu potom prhlasen
 * je to jen kvuli overeni uzivatele kdyz preda hash
 */
 // action musi byt main a musime mit hash

if(isset($_GET['hash']))
{
    // http://beta.myteamworld.com/?action=registration&email=lukin145%40gmail.com&hash=9816fc1a1d49badeae193e6545aed557
    $control_hash = md5($_GET['email'] . HASH_SALT);
    if($control_hash != $_GET['hash']) // nesedi nam kontrolni hashe
    {
        Header('Location: ' . PATH_WEB_ROOT);
        die();
    }
    else // hash je v pohode
    {
        $db = new Database();

        $sql = 'SELECT *
                FROM `user`
                WHERE `user_email` = "'.mysqli_real_escape_string($db->mysqli(), $_GET['email']).'"
                    AND `user_hash` = "'.mysqli_real_escape_string($db->mysqli(), $_GET['hash']).'"
                    AND `user_verified` = 0';

        $db->query($sql);

        // jeslitze exisuje uzivatel s timhle mailem a hashem, tak ho prihlasime
        if($db->getRows() > 0)
        {
            $user_data = $db->readrow('aarr');
            //Vytvorim objekt User
			$logged_user = new User($user_data);
            // ulozime do cookie
            setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
            //Pridam objekt do session
			$_project['session']->set('logged', $logged_user);
        }
        else
        {
            Header('Location: ' . PATH_WEB_ROOT);
            die;
        }
    }
}

// pokud jsme se neprihlasili overenim hashe, tak se pokusime o prihlaseni ted
if(empty($logged_user->id))
{
    //printr($_project['session']);
    //Inicializace prihlaseneho uzivatele
    if($_project['session']->isSetSession('logged'))
    {
        // vytahneme ze session
    	$logged_user = $_project['session']->get('logged');

        // nacteme si aktualni info o uzivateli
        if(!$logged_user instanceof User)
        {
            $logged_user = UserDAO::get($logged_user);
        }
        else
        {
            $logged_user = UserDAO::get($logged_user->id);
        }
        //printr($logged_user);
        // a ulozime do cookie aktualizovany stav
        //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/', '.myteamworld.com');

        // jestlize uzivatel neni overen, tak ho odhlasime zase
        if(!($logged_user->isVerified()) && $action != 'registration')
        {
            unset($logged_user);
            $logged_user = new User();

            $_S->sunset('logged');
            $_project['session']->sunset('logged');
            setcookie(COOKIENAME, '', time() - 3600, '/'); // unset cookie
            unset($_COOKIE[COOKIENAME]);
        }
        /*elseif(!$logged_user->isVerified() && $action != 'registration')
        {
            header('Location: '.PATH_WEB_ROOT .'?action=registration&email='.$logged_user->email.'&hash='.$logged_user->hash);
        }*/
    }
    // zkusime prihlasit uzivatele z cookie
    elseif(isset($_COOKIE[COOKIENAME]))
    {
        $logged_user = unserialize(stripslashes($_COOKIE[COOKIENAME]));
        
        if($logged_user instanceof User && !empty($logged_user->id))
        {
            $_project['session']->set('logged', $logged_user);
        }
        else
        {
            $logged_user = new User();
        }
        //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
    }
    // jinak neexistuje
    else
    {
    	$logged_user = new User();
    }
}

if(!empty($logged_user->id))
{
    // nacteme seznam tymu
    $logged_user->getTeamList();
    $logged_user->getActiveTeam();
    $logged_user->getSettings();
}

// presmerovat na homepage musime jen kdyz nejsme v ajaxu
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']))
{
    if($action == 'main' && isset($logged_user) && $logged_user instanceof User && !empty($logged_user->id) && $_SERVER['REQUEST_URI'] != '/admin.php')
    {
        Header('Location: ' . PATH_WEB_ROOT . 'wall/');
        die;
    }

    if(empty($logged_user->id) && !in_array($action, $not_logged_access) && $_SERVER['REQUEST_URI'] != '/admin.php')
    {
        $_S->set('last_page', $_SERVER['REQUEST_URI']);

        Header('Location:' . PATH_WEB_ROOT);
        die;
    }


    // jestlize neni uzivatel jeste overeny, tak musi mit v getu hash a email
    // a zaroven musi mit akci regitration (otvrzeni registrace) nebo main (ajax na potvrzeni registrace)
    if(!$logged_user->isVerified() && (!isset($_GET['hash']) || !isset($_GET['email'])) && !in_array($action, $not_logged_access) && $_SERVER['REQUEST_URI'] != '/admin.php')
    {
        Header('Location:' . PATH_WEB_ROOT);
        die;
    }
}

// SUPERADMIN
if($logged_user->id == 1 || $logged_user->id == 3)
{
    define('SUPERADMIN', true);
}
else
{
    define('SUPERADMIN', false);
}

// Takovy maly admin
// nacitani modulu, titulku
switch($action)
{
    case 'what-is-myteamworld':
        $template   = 'text';
        $page       = 'what-is-myteamworld';
        $title      = $tr->tr('Co je myteamworld');
        $desc       = $tr->tr('Co je myteamworld');
        $keywords   = $tr->tr('Co je myteamworld');
    break;

  	case 'registration':
        $template   = 'subpage';
        $page       = 'registration';
        $title      = $tr->tr('Registrace nového uživatele');
        $desc       = $tr->tr('Registrace nového uživatele');
        $keywords   = $tr->tr('Registrace nového uživatele');
    break;

    case 'forgotten-password':
        $template   = 'text';
        $page       = 'forgotten-password';
        $title      = $tr->tr('Obnova zapomenutého hesla');
        $desc       = $tr->tr('Obnova zapomenutého hesla');
        $keywords   = $tr->tr('Obnova zapomenutého hesla');
    break;

    case 'edit-user-profile':
        $template   = 'subpage';
  		$page       = 'registration';
        $title      = $tr->tr('Úprava profilu');
        $desc       = $tr->tr('Úprava profilu');
        $keywords   = $tr->tr('Úprava profilu');
  	break;

    case 'edit-team-profile':
        $template   = 'subpage';
  		$page       = 'team-registration';
        $title      = $tr->tr('Úprava týmu');
        $desc       = $tr->tr('Úprava týmu');
        $keywords   = $tr->tr('Úprava týmu');
  	break;

  	case 'team-registration':
        $template   = 'subpage';
  		$page       = 'team-registration';
        $title      = $tr->tr('Registrace nového týmu');
        $desc       = $tr->tr('Registrace nového týmu');
        $keywords   = $tr->tr('Registrace nového týmu');
  	break;

    case 'wall':
        $template   = 'subpage';
        $page       = 'wall';
        $title      = $tr->tr('Nástěnka');
        $desc       = $tr->tr('Nástěnka');
        $keywords   = $tr->tr('Nástěnka');
    break;

    case 'news-detail':
        $template   = 'subpage';
        $page       = 'news-detail';
        $title      = $tr->tr('Detail novinky');
        $desc       = $tr->tr('Detail novinky');
        $keywords   = $tr->tr('Detail novinky');
    break;

    case 'user-profile':
        $template   = 'subpage';
        $page       = 'user-profile';
        //$title    = actions; TODO nejde mi to...
        //$desc       = $tr->tr('Profil uživatele');
        //$keywords   = $tr->tr('Profil uživatele');
    break;

    case 'team-profile':
        $template   = 'subpage';
        $page       = 'team-profile';
        //$title    = actions; TODO nejde mi to...
        //$desc       = $tr->tr('Profil týmu');
        //$keywords   = $tr->tr('Profil týmu');
    break;

    case 'press-releases':
        $template   = 'subpage';
        $page       = 'press-releases';
        $title      = $tr->tr('Tiskové zprávy');
        $desc       = $tr->tr('Tiskové zprávy');
        $keywords   = $tr->tr('Tiskové zprávy');
    break;

    case 'roster':
        $template   = 'subpage';
        $page       = 'roster';
        $title      = $tr->tr('Soupiska');
        $desc       = $tr->tr('Soupiska');
        $keywords   = $tr->tr('Soupiska');
    break;

    case 'stats':
        $template   = 'subpage';
        $page       = 'stats';
        $title      = $tr->tr('Statistiky');
        $desc       = $tr->tr('Statistiky');
        $keywords   = $tr->tr('Statistiky');
    break;

    case 'events':
        $template   = 'subpage';
        $page       = 'events';
        $title      = $tr->tr('Události');
        $desc       = $tr->tr('Události');
        $keywords   = $tr->tr('Události');
    break;

    case 'event-manage':
        $template   = 'subpage';
        $page       = 'event-manage';
        $title      = $tr->tr('Vytvořit novou událost');
        $desc       = $tr->tr('Vytvořit novou událost');
        $keywords   = $tr->tr('Vytvořit novou událost');
    break;

    case 'event-edit-result':
        $template   = 'subpage';
        $page       = 'event-edit-result';
        $title      = $tr->tr('Nastavit výsledek události');
        $desc       = $tr->tr('Nastavit výsledek události');
        $keywords   = $tr->tr('Nastavit výsledek události');
    break;

    case 'event-detail':
        $template   = 'subpage';
        $page       = 'event-detail';
        $title      = $tr->tr('Detail zápasu');
        $desc       = $tr->tr('Detail zápasu');
        $keywords   = $tr->tr('Detail zápasu');
    break;

    case 'attendance':
        $template   = 'subpage';
        $page       = 'attendance';
        $title      = $tr->tr('Docházka');
        $desc       = $tr->tr('Docházka');
        $keywords   = $tr->tr('Docházka');
    break;

    case 'gallery':
        $template   = 'subpage';
        $page       = 'gallery';
        $title      = $tr->tr('Galerie');
        $desc       = $tr->tr('Galerie');
        $keywords   = $tr->tr('Galerie');
    break;

    case 'gallery-detail':
        $template   = 'subpage';
        $page       = 'gallery-detail';
        $title      = $tr->tr('Detail galerie');
        $desc       = $tr->tr('Detail galerie');
        $keywords   = $tr->tr('Detail galerie');
    break;

    case 'cashbox':
        $template   = 'subpage';
        $page       = 'cashbox';
        $title      = $tr->tr('Týmová kasa');
        $desc       = $tr->tr('Týmová kasa');
        $keywords   = $tr->tr('Týmová kasa');
    break;

    case 'search':
        $template   = 'subpage';
        $page       = 'search';
        $title      = $tr->tr('Vyhledávání');
        $desc       = $tr->tr('Vyhledávání');
        $keywords   = $tr->tr('Vyhledávání');
    break;

    case 'messages':
        $template   = 'subpage';
        $page       = 'messages';
        $title      = $tr->tr('Zprávy');
        $desc       = $tr->tr('Zprávy');
        $keywords   = $tr->tr('Zprávy');
    break;

    case 'notices':
        $template   = 'subpage';
        $page       = 'notices';
        $title      = $tr->tr('Upozornění');
        $desc       = $tr->tr('Upozornění');
        $keywords   = $tr->tr('Upozornění');
    break;

    case '404':
    case 'page-not-found-404':
        $template   = 'subpage';
        $page       = 'page-not-found-404';
        $title      = $tr->tr('Stránka nebyla nenalezena');
        $desc       = $tr->tr('Stránka nebyla nenalezena');
        $keywords   = $tr->tr('Stránka nebyla nenalezena');
    break;

    case 'user-not-found':
        $template   = 'subpage';
        $page       = 'user-not-found';
        $title      = $tr->tr('Uživatel nenalezen');
        $desc       = $tr->tr('Uživatel nenalezen');
        $keywords   = $tr->tr('Uživatel nenalezen');
    break;

    case 'team-not-found':
        $template   = 'subpage';
        $page       = 'team-not-found';
        $title      = $tr->tr('Tým nenalezen');
        $desc       = $tr->tr('Tým nenalezen');
        $keywords   = $tr->tr('Tým nenalezen');
    break;

    case 'terms':
        $template   = 'text';
        $page       = 'terms';
        $title      = $tr->tr('Podmínky použití');
        $desc       = $tr->tr('Podmínky použití');
        $keywords   = $tr->tr('Podmínky použití');
    break;

    case 'help-without-team':
        $template   = 'text';
        $page       = 'help-without-team';
        $title      = $tr->tr('Nápověda');
        $desc       = $tr->tr('Nápověda');
        $keywords   = $tr->tr('Nápověda');
    break;

    case 'help':
        $template   = 'text';
        $page       = 'help';
        $title      = $tr->tr('Nápověda');
        $desc       = $tr->tr('Nápověda');
        $keywords   = $tr->tr('Nápověda');
    break;

    case 'contact':
        $template   = 'subpage';
        $page       = 'contact';
        $title      = $tr->tr('Kontakty');
        $desc       = $tr->tr('Kontakty');
        $keywords   = $tr->tr('Kontakty');
    break;

    case 'admin.php':
    break;

    case 'main':
    case '':
        if(!empty($logged_user->id) && (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') && $_SERVER['REQUEST_URI'] != '/admin.php')
        {
            Header('Location: ' . PATH_WEB_ROOT . 'wall/');
            die;
        }
        else
        {
            $template   = 'homepage';
            $page       = 'main';
            $title      = $tr->tr('Úvodní stránka');
            $desc       = $tr->tr('Díky myteamworld.com můžete získávat aktuální novinky z mnoha sportovních odvětví, sledovat svoje oblíbené týmy a zároveň vylepšit organizaci svého vlastního sportovního týmu!');
            $keywords   = $tr->tr('organizace sportovních týmů, vedení sportovního týmu, sportovní novinky, statistiky týmů');
        }
    break;

    default :
        $page       = 'page-not-found-404';
        $template   = 'subpage';
        $title      = $tr->tr('Stránka nebyla nalezena');
        $desc       = $tr->tr('Stránka nebyla nalezena');
        $keywords   = $tr->tr('Stránka nebyla nalezena');
    break;
}

// nacteni informaci o tymu
if(isset($_GET['team_id']) && intval($_GET['team_id']) > 0 && isset($_GET['key']) && $action == 'team-profile')
{
    // vstupni data se osetruji az v samotne tride
    $team = TeamDAO::getByKey($_GET['team_id'], $_GET['key']);

    if($team->id == $logged_user->getActiveTeam()->id && empty($_GET['subaction']))
    {
        Header('Location: ' . PATH_WEB_ROOT . $_GET['subaction'] . '/');
        die;
    }
}
elseif($logged_user->hasTeam())
{
    $team = $logged_user->getActiveTeam(true);
}
else
{
    $team = new Team();
}

// jestlize je uzivatel jinde nez na nastence, tak musi mit nejaky tym
if(!in_array($action, $global_allowed_pages_without_team) && $action != 'wall' && empty($team->id) && !isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !in_array($action, $not_logged_access))
{
    Header('Location: ' . PATH_WEB_ROOT);
    die;
}

// pokud se jedna o modul
if(isset($global_modules[$action]))
{
    // tak zkontrolujeme jestli ma tym navstiveny modul vubec povoleny
    if(!$team->userCanViewModule($action, $logged_user))
    {
        header('Location: ' . PATH_WEB_ROOT);
        die;
    }
}

// pokud se jedna o modul
if(isset($global_modules[$subaction]))
{
    // tak zkontrolujeme jestli ma tym navstiveny modul vubec povoleny
    if(!$team->userCanViewModule($subaction, $logged_user))
    {
        header('Location: ' . PATH_WEB_ROOT);
        die;
    }
}
