<?php

// nastaveni IP adres, kde uvidi errory z ladenky
$developers = array(
    '83.240.25.245',
    '46.227.13.51'
);
if(in_array(@$_SERVER['REMOTE_ADDR'], $developers))
{
    define('TESTIP', 1);
    ini_set('display_errors', 1);
    error_reporting(E_ALL ^ E_DEPRECATED);
    $config['db']['show_errors'] = true;
}
else
{
    define('TESTIP', 0);
    ini_set('display_errors', 0);
    error_reporting(0);
    $config['db']['show_errors'] = false;
}

// session se odhlasi az se zavrenim okna
ini_set('session.use_only_cookies', '1');

if(defined('PATH'))
{
    require_once(PATH . 'classes/ImageWorkshop/ImageWorkshop.php'); // Be sure of the path to the class
    require_once(PATH . 'classes/PHPMailerClasses/class.phpmailer.php'); // Be sure of the path to the class
}
else
{
    require_once(__DIR__ . '/../classes/ImageWorkshop/ImageWorkshop.php'); // Be sure of the path to the class
    require_once(__DIR__ . '/../classes/PHPMailerClasses/class.phpmailer.php'); // Be sure of the path to the class
}
use PHPImageWorkshop\ImageWorkshop;

define('VERSION',               '2.0');

include('local.php');
define('CACHE_RELATIVE_PATH',   'cache/');
define('DEFAULT_CACHE_PATH',    PATH_ROOT . CACHE_RELATIVE_PATH);
define('BLOCK_PATH',            'web/blocks/');
define('PAGE_PATH',             'web/pages/');

if(!empty($_SERVER['HTTP_HOST']))
{
    define('PATH_WEB_ROOT', 'http'.(@$_SERVER['HTTPS'] == 'on' ? 's' : '').'://'. $_SERVER['HTTP_HOST'].'/');
}
else
{
    define('PATH_WEB_ROOT', 'http'.(@$_SERVER['HTTPS'] == 'on' ? 's' : '').'://'. DOMAIN .'.myteamworld.com/');
}

define('IMAGES_PATH',           PATH_WEB_ROOT.'web/images/');
define('USER_PATH',             PATH_WEB_ROOT.'userdata/');
define('TEAM_PATH',             PATH_WEB_ROOT.'teamdata/');
define('SPORT_IMAGES_PATH',     PATH_WEB_ROOT.'web/images/sports/');
define('DEFAULT_SPORT_IMAGE',   SPORT_IMAGES_PATH.'default_small.png');
define('HASH_SALT',             'nejaka ta nase sul');
define('WALL_LIMIT',            15);
define('LIKE_LIMIT',            5);
define('COMMENT_LIMIT',         2);
define('COMMENT_LIMIT_DETAIL',  100);
define('TITLE_LENGTH',          125);
define('PEREX_LENGTH',          260);
define('PEREX_LENGTH_SHORT',    155);
define('EXPIRE_DAYS',           1/24);
define('USER_DATADIR',          'userdata/');
define('TEAM_DATADIR',          'teamdata/');
define('TEAM_SALT',             'NEJAKA SulPro TYM 4567891 s');
define('TEAM_SALT_OBS',         'NeJAkA SulPro TYM 456791 s');
define('TEAM_SALT_ENTRY',       'NeJAkdddedšěs#@A SulPr#TYM 456791 s');
define('SALT_INVITATION',       'PjoskTREfgs478AAwww');
define('SALT_RECIPIENT',        'Pdckzrw7548vde45cc43cd');
define('SALT_TEAM',             'Pdckzrw7ssweSbv fde548vde45cc43cd');
define('SALT_GALLERY',          'H@FDSacewsxcfr#D482AAawqdA/*233saqq');
define('NOTICE_USER_COUNT',     2); // pocet jmen ktera se zobrazi u notifikaci - PRI ZMENE JE POTREBA ZMENIT I TRIDU NOTIFICATION KDE SE TO POUZIVA
define('NOTIFICATIONS_TIME_LIMIT', 55000); // pocet milisekund mezi dotazem na nove notifikace
define('EVENT_LIMIT',           10);
define('VIEW_MESSAGE_LIMIT',    15);
define('CHANGE_DATE_LIMIT',     2);
define('LAST_ACTIVITIES_LIMIT', 10);
define('MAX_SEARCH_LIMIT',      100);
define('SEARCH_LIMIT',          10);
define('CACHE_ENABLED',         false);
define('ROSTER_GROUP_LIMIT',    8);
define('IMAGE_TEAM_HASH_SALT',  'lksZHGBVCDSWsxcds546543#&A#44as#.-SskqkTRwexc485zt.aaaaarrdxHGdsE.-');

// GALLERY - velikosti jednotlivych obrazku
define('GALLERY_BIG_W',         2048);
define('GALLERY_BIG_H',         1536);
define('GALLERY_MED_W',         800);
define('GALLERY_MED_H',         600);
define('GALLERY_SMA_W',         170);
define('GALLERY_SMA_H',         128);

// typ formátovaného času pro zobrazení u komentářů
define('COMMENT_DATE_TYPE',     2);
define('LIST_COMMENT_COUNT_MAX', 2);

// pod jakym nazvem se bude uchovavat originalni nahrany obrazek, ale zmenseny na sirku MAX_ORIGINAL_WIDTH
define('ORIGINAL_IMAGE_NAME',           'original_uploaded.jpg');
define('ORIGINAL_TEAM_IMAGE_NAME',      'original_team_uploaded.jpg');
define('MAX_ORIGINAL_TEAM_WIDTH',       900);
define('MAX_ORIGINAL_WIDTH',            300);

// pod jakym nazvem se bude ukladat cropped original, ze ktereho se pak resizem delaji ostatni obrazky
define('ORIGINAL_IMAGE_NAME_CROPPED',       'original_cropped.jpg');
define('ORIGINAL_TEAM_IMAGE_NAME_CROPPED',  'original_team_cropped.jpg');

$global_allowed_pages_without_team = array(
    'wall',
    'main',
    'messages',
    'search',
    'notices',
    'registration',
    'team-registration',
    'user-profile',
    'team-profile',
    'edit-user-profile',
    'forgotten-password',
    'news-detail',
    'event-detail',
    'help',
    'help-without-team',
    'contact'
);

$global_user_images = array(
    /*'small'             => array(
        'name'  => 'image-small.jpg',
        'size'  => array(28, 28),
        'show'  => true
    ),
    'medium'            => array(
        'name'  => 'image-medium.jpg',
        'size'  => array(46, 46),
        'show'  => true
    ),
    'big'               => array(
        'name'  => 'image-big.jpg',
        'size'  => array(200, 200),
        'show'  => true
    ),*/
    'original'          => array(
        'name'  => ORIGINAL_IMAGE_NAME,
        'size'  => array(300, 300),
        'show'  => false
    ),
    'original_cropped'  => array(
        'name'  => ORIGINAL_IMAGE_NAME_CROPPED,
        'size'  => array(300, 300),
        'show'  => false
    ),
);


/***********************
**********************
**********************
 * nastaveni resizu pro LOGA tymu *
***********************
**********************
***********************/
$default_logos = array(
    'small'     => 'image-small.jpg',
    'medium'    => 'image-medium.jpg',
    'big'       => 'image-big.jpg',
    'original'  => 'original_uploaded.jpg',
    'original_cropped'  => 'original_cropped.jpg'
);

// velikosti resizu LOGA tymu
$resizes_logo = array(
    'small'     => '-quality 90 -resize 28x28',
    'medium'    => '-quality 90 -resize 46x46',
    'big'       => '-quality 90 -resize 200x200',
    'original'  => '-quality 100',
    'original_cropped'  => '-quality 100'
);

$sizes_logo = array(
    'small'     => array(28, 28),
    'medium'    => array(46, 46),
    'big'       => array(200, 200),
    'original'  => array(300, 300),
    'original_cropped'  => array(300, 300),
);


/***********************
**********************
**********************
 * nastaveni resizu pro profilove obrazky uzivatele *
***********************
**********************
***********************/
$default_users = array(
    'small'     => 'image-small.jpg',
    'medium'    => 'image-medium.jpg',
    'big'       => 'image-big.jpg',
    'original'  => 'original_uploaded.jpg',
    'original_cropped'  => 'original_cropped.jpg'
);

// velikosti resizu profiloveho obrazku uzivatele
$resizes_user = array(
    'small'     => '-quality 90 -resize 28x28',
    'medium'    => '-quality 90 -resize 46x46',
    'big'       => '-quality 90 -resize 200x200',
    'original'  => '-quality 100',
    'original_cropped'  => '-quality 100'
);

$sizes_user = array(
    'small'     => array(28, 28),
    'medium'    => array(46, 46),
    'big'       => array(200, 200),
    'original'  => array(300, 300),
    'original_cropped'  => array(300, 300),
);



/***********************
**********************
**********************
 * nastaveni resizu pro profilove obrazky uzivatele *
***********************
**********************
***********************/
$default_teams = array(
    'small'     => 'team-small.jpg',
    'medium'    => 'team-medium.jpg',
    'big'       => 'team-big.jpg',
    'original'  => 'original_team_uploaded.jpg',
    'original_cropped'  => 'original_team_cropped.jpg'
);

// velikosti resizu profiloveho obrazku uzivatele
$resizes_team = array(
    'small'     => '-quality 90 -resize 182x',
    'medium'    => '-quality 90 -resize 590x',
    'big'       => '-quality 90 -resize 800x',
    'original'  => '-quality 100',
    'original_cropped'  => '-quality 100'
);

$global_team_images = array(
    /*'small'             => array(
        'name'  => 'team-small.jpg',
        'size'  => array(182, 99),
        'show'  => true
    ),
    'medium'            => array(
        'name'  => 'team-medium.jpg',
        'size'  => array(550, 300),
        'show'  => true
    ),
    'big'            => array(
        'name'  => 'team-big.jpg',
        'size'  => array(800, 600),
        'show'  => true
    ),*/
    'original'          => array(
        'name'  => ORIGINAL_TEAM_IMAGE_NAME,
        'size'  => array(800, 800),
        'show'  => false
    ),
    'original_cropped'  => array(
        'name'  => ORIGINAL_TEAM_IMAGE_NAME_CROPPED,
        'size'  => array(800, 800),
        'show'  => false
    ),
);

$sizes_team = array(
    'small'     => array(182, 99),
    'medium'    => array(590, 300),
    'big'       => array(800, 600),
    'original'  => array(800, 600),
    'original_cropped'  => array(800, 600),
);



// velikosti fotek v galerii
$gallery_sizes = array(
    'small'     => array(180, 136),
    'medium'    => array(800, 600),
    'big'       => array(2048, 1536)
);

$resizes_gallery = array(
    'small'     => '-quality 90 -thumbnail 180x136^ -extent 180x136 -gravity center',
    'medium'    => '-quality 90 -resize 900x'
);

define('GALLERY_CACHE_RELATIVE_PATH',   'cache-gallery/');
define('GALLERY_CACHE_PATH',    PATH_ROOT . GALLERY_CACHE_RELATIVE_PATH);


$_regions = array(
    '0'                  => '-- Vyberte --',
    'Jihomoravský'       => 'Jihomoravský kraj',
    'Středočeský'        => 'Středočeský kraj',
    'Jihočeský'          => 'Jihočeský kraj',
    'Plzeňský'           => 'Plzeňský kraj',
    'Jihomoravský'       => 'Jihomoravský kraj',
    'Kraj Vysočina'      => 'Kraj Vysočina',
    'Moravskoslezský'    => 'Moravskoslezský kraj',
    'Ústecký'            => 'Ústecký kraj',
    'Olomoucký'          => 'Olomoucký kraj',
    'Královéhradecký'    => 'Královéhradecký kraj',
    'Pardubický'         => 'Pardubický kraj',
    'Zlínský'            => 'Zlínský kraj',
    'Karlovarský'        => 'Karlovarský kraj',
    'Liberecký'          => 'Liberecký kraj',
    'Hlavní město Praha' => 'Hlavní město Praha'
);


$_days_of_week = array(
    'Monday'    => 'po',
    'Tuesday'   => 'út',
    'Wednesday' => 'st',
    'Thursday'  => 'čt',
    'Friday'    => 'pá',
    'Saturday'  => 'so',
    'Sunday'    => 'ne'
);


// nastaveni modulu je schvalne v preloadu, kvuli objektu $tr
// kontrola pristupu na stranky
// jesltlize neni uzivatel prihlasen, tak muze jen na tyto stranky
$not_logged_access = array(
    'main',
    'registration',
    'what-is-myteamworld',
    'terms',
    'forgotten-password',
    'team-profile',
    'user-profile',
    'event-detail',
    'contact'
);
