<?php
function __autoload($class)
{
    if(file_exists('classes/'.$class.'.class.php'))
    {
        include_once 'classes/'.$class.'.class.php';
    }
    elseif(defined('PATH'))
    {
        if(file_exists(PATH . 'classes/'.$class.'.class.php'))
        {
            require_once PATH . 'classes/'.$class.'.class.php';
        }
    }
}

function printr($value)
{
	if($value)
	{
		echo '<pre>';
		print_r($value);
		echo '</pre>';
        echo "\n\n";
	}
	else
	{
		var_dump($value);
	}
}

function getDateDiff($date1, $date2)
{
    $dStart = new DateTime($date1);
    $dEnd  = new DateTime($date2);
    $dDiff = $dStart->diff($dEnd);
    //echo $dDiff->format('%R'); // use for point out relation: smaller/greater
    return $dDiff->days;
}

/**
 * Vrati zpracované datum ve formátu
 * "před x minutami , sekundami"
 * asi před hodinou
 * před dvěma dny
 * 15.6.2012
 * TODO
 */
function getFormatedDate($date, $type = 1)
{
    if(empty($date))
    {
        return 'žádné datum';
    }

    // komentar
    if($type == COMMENT_DATE_TYPE)
    {
        $periods         = array('právě teď', 'asi před minutou', 'před hodinou', 'včera', 'minulý týden', 'minulý měsíc', 'před rokem', 'dekádou');
        $periods_second  = array('sekundy', 'minuty', 'hodiny', 'dny', 'týdny', 'měsíce', 'roky', 'dekády');
        $periods_plurals = array('sekundami', 'minutami', 'hodinami', 'dny', 'týdny', 'měsíci', 'lety', 'dekádami');
        $lengths         = array('59','59','23','6','4.35','12','100');
    }
    // bubliny u příspěvků
    else
    {
        $periods         = array('právě teď', 'asi minuta', '<span>1</span>hodina', 'včera', 'minulý týden', 'minulý měsíc', 'rok', 'dekádou');
        $periods_second  = array('sekundy', 'minuty', 'hodiny', 'dny', 'týdny', 'měsíce', 'roky', 'dekády');
        $periods_plurals = array('sekund', 'minut', 'hodin', 'dní', 'týdnů', 'měsíců', 'let', 'dekádami');
        $lengths         = array('59','59','23','6','4.35','12','100');
    }


    $now             = time();
    $unix_date       = strtotime($date);

    // check validity of date
    if(empty($unix_date))
    {
        return 'špatné datum';
    }

    // is it future date or past date
    if($now > $unix_date)
    {
        $difference     = $now - $unix_date;
        //$tense          = '';
    }
    else
    {
        $difference     = $unix_date - $now;
        //$tense          = '';
    }

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++)
    {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);
    $prefix = 'před ';
    if($type == 1)
    {
        if($difference > 1)
        {
            $periods = $periods_second;
        }

        if($difference > 4)
        {
            $periods = $periods_plurals;
        }
    }
    else
    {
        if($difference > 1)
        {
            $periods = $periods_plurals;
        }
    }

    if($difference == 0)
    {
        return 'právě teď';
    }
    //return $tense . ($difference > 1 ? ' '.$difference.' ' : ' ').$periods[$j];
    return ($difference > 1 ? ($type == 2 ? $prefix : ''). '<span>'.$difference.'</span>' : '').' ' .$periods[$j];
}

function redirect($url)
{
    $string = '<script type="text/javascript">';
    $string .= 'window.location = "' . $url . '"';
    $string .= '</script>';

    echo $string;
}

function getWall($wall, $detail = false, $isWall = false)
{
    global $logged_user, $tr;

    $return = '';

    function printComments($object_id, $val, $link = '')
    {
    	if(!isset($val['comments']) || !is_array($val['comments']))
    	{
    		$val['comments'] = array();
    	}

    	$return = '<div id="comments-'.$object_id.'" class="commentsBox">';

        foreach($val['comments'] as $key => $val1)
        {
	        $return .= printComment($val1, $val['type']);
	    }

	    $timeago = ' timeago';
	    if(getDateDiff(date('Y-m-d',strtotime($val['date_begin'])), date('Y-m-d')) > CHANGE_DATE_LIMIT)
	    {
	        $timeago = '';
	    }

        // getFormatedDate($val['value']->date)
        // nastavi nam kde se ma zobrazit ta pitoma bublina
        if(isset($val['comments_count']) && $val['comments_count'] > 0)
        {
            $css = '';
            $text = '<a href="'.$link.'" class="bubble-comment'.$css.'" id="bubble-'.$object_id.'"><span>'.intval($val['comments_count']).'</span> reakcí</a>';
        }
        else
        {
            $css = ' first';
            $text = '<div class="bubble-comment'.$css.'" id="bubble-'.$object_id.'"><span>0</span> reakcí</div>';
        }

        $return .= $text;
        $time = date('Y-m-d\TH:i:sP',strtotime(($val['date_begin'] != '0000-00-00 00:00:00' ? $val['date_begin'] : $val['created'])));
	    $return .= '</div>

	    <form id="object-'.$object_id.'" action="" class="'.(count($val['comments']) > 0 ? 'myComment' : 'hidden').' cleaned">
	        <fieldset>
	            <input type="hidden" name="parent_id" value="'.(empty($val['team_id']) ? '0' : $val['team_id']).'_'.$val['id'] . '_' . $val['type'].'" />
	            <textarea class="resizable noActive" name="text" cols="46" rows="1">Začněte psát komentář...</textarea>
	            <button class="button buttonA small" type="submit">odeslat</button>
	        </fieldset>
	    </form>

	    <div class="bubble-date'.$timeago.'" data-time="'.$time.'" title="'.$time.'">'.date('j.n. H:i', strtotime($val['date_begin'])).'</div>';

        if(!isset($val['like']))
        {
            $val['like'] = array();
        }

	    $return .= '<div class="popup-like-container">
	        <div class="bubble-like likeIt" data-id="'.$object_id.'"><span id="like-'.$object_id.'">'.count($val['like']).'</span> líbí se</div>
	        <div class="popup-like" id="popup-like-'.$object_id.'">';
	            if(isset($val['like']) && count($val['like']) > 0)
	            {
	                $return .= printLikeList($val['like'], $object_id);
	            }
	            $return .= '</div>
	    </div>';

	    return $return;
    }

    foreach($wall as $key => $val)
    {

        if(!isset($val['comments']) || !is_array($val['comments']))
        {
            $val['comments'] = array();
        }

        if(!isset($val['like']) || !is_array($val['like']))
        {
            $val['like'] = array();
        }

        // novinky RSS
        if(in_array($val['type'], array('news')))
        {
            $val['value'] = unserialize($val['value']);
            foreach($val['value'] as $k => $v)
            {
                $val['value']->$k = stripslashes(strip_tags($v));
            }
            $object_id = (empty($val['team_id']) ? '0' : $val['team_id']) . '_'. $val['id'] . '_' . $val['type'];
            /*<span>[<a href="#" title="">FBC Royal Gigolos</a>]</span>*/
            $url = $val['value']->link;
            $source = parse_url($url, PHP_URL_HOST);
            //$source = preg_replace("/^www\./", "", $source);

            // odkaz na detail objektu
            $link = getNewsLink($object_id, $val['value']->title);

            $return .= '
            <div class="box rss news">
                <div class="content" data-id="'.$object_id.'">
                    <h2 class="boxTitle"><a href="'.$val['value']->link.'" target="_blank" rel="nofolow" title="Zobrazit celý článek na webu www.'.$val['sport_source'].'">'.(mb_strlen($val['value']->title) > TITLE_LENGTH ? mb_substr($val['value']->title, 0, TITLE_LENGTH, 'utf-8') . '&hellip;' : $val['value']->title).'</a></h2>
                    <div class="post cleaned object-'.$object_id.'">';
                        $return .= '<p class="text">'.(mb_strlen($val['value']->text) > PEREX_LENGTH ? mb_substr($val['value']->text, 0, PEREX_LENGTH, 'utf-8') . '&hellip;' : $val['value']->text).'</p>';
                        $return .= '<p class="links">
                            <a href="http://'.$source.'" target="_blank" rel="nofolow" title="Zobrazit web '.$source.'" class="ico"><span class="'.$val['sport_image'].'">zdroj: '.$source.'</span></a>';

                            if(!empty($logged_user->id))
                            {
                                $cur = current($val['comments']);
                                if(count($val['comments']) == 0)
                                {
                                    $return .= '<span class="allComments"><a class="firstComment" title="'.$tr->tr('Přidat první komentář k článku').'" href="#">'.$tr->tr('přidat první komentář').'</a>&nbsp;<span class="separator">|</span>&nbsp;</span>';
                                }
                                elseif($val['comments_count'])
                                {
                                    $return .= '<span class="allComments"><!-- -->';
                                    // zobrazujeme jen kdyz nejsme na detailu
                                    if(!$detail && $val['comments_count'] > LIST_COMMENT_COUNT_MAX)
                                    {
                                        $return .= '<a href="'.$link.'"><span title="Zobrazit všechny komentáře"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.$val['comments_count'].')</span></a>&nbsp;|&nbsp;';
                                    }
                                    $return .= '</span>';
                                }
                                else
                                {
                                    $return .= '&hellip;';
                                }
                                if(!in_array($logged_user->id, array_keys($val['like'])))
                                {
                                    $text = $tr->tr('líbí se mi');
                                }
                                else
                                {
                                    $text = $tr->tr('už se mi nelíbí');
                                }
                                //$return .= '<a href="'.$val['value']->link.'" target="_blank" rel="nofolow" title="Zobrazit celý článek na webu '.$val['sport_source'].'">zobrazit celý článek</a>
                                $return .= '<a class="likeIt" href="#" title="'.$text.'" data-id="'.$object_id.'">'.$text.'</a>';
                            }
                        $return .= '</p>
                    </div><!-- .post -->';

                    $return .= printComments($object_id, $val, $link);

                $return .= '</div><!-- .content -->
            </div><!-- .box .rss -->';
        }



        // tiskové zprávy
        if(in_array($val['type'], array('press')))
        {
            $val['value'] = unserialize($val['value']);
            foreach($val['value'] as $k => $v)
            {
                $val['value']->$k = stripslashes(strip_tags($v));
            }
            $object_id = $val['team_id'] . '_'. $val['id'] . '_' . $val['type'];
            $team = new Team($val);
            // kvůli ajaxu
            if(empty($team->name))
            {
                $team = TeamDAO::get($val['team_id']);
            }
            $pr = new PressRelease($val);

            $link = $team->getPressDetailLink($val['value']->heading, $object_id);

            $return .= '
            <div class="box news'.($detail ? ' detail' : '').'">
                <div class="content" data-id="'.$object_id.'-'.intval($val['team_id']).'">
                    <h2 class="boxTitle">'.$team->getTeamLink().' | ';
                    if($detail)
                    {
                        $return .= $val['value']->heading . '</h2>';
                    }
                    else
                    {
                        $return .= '<a href="'.$link.'" class="title" title="Zobrazit celou tiskovou zprávu">'.(mb_strlen($val['value']->heading) > TITLE_LENGTH ? mb_substr($val['value']->heading, 0, TITLE_LENGTH, 'utf-8') . '&hellip;' : $val['value']->heading).'</a></h2>';
                    }
                    $return .= '<div class="post cleaned object-'.$object_id.'">
                        <p class="picture">
                            <a href="'.$team->getProfileLink().'" title="Zobrazit profil týmu '.$team->getName().'"><img src="'.$team->getTeamLogo('medium').'" alt="'.$team->getName().'" width="46" height="46" /></a>
                        </p>';
                        if($detail)
                        {
                            $return .= '<p class="longText"><span class="perex">'.$val['value']->perex.'</span> '.nl2br($val['value']->text).'</p>';
                        }
                        else
                        {
                            $return .= '<p class="text">'.(mb_strlen($val['value']->perex) > PEREX_LENGTH_SHORT ? mb_substr($val['value']->perex, 0, PEREX_LENGTH_SHORT, 'utf-8') . '&hellip;' : $val['value']->perex).'</p>';
                        }

                        if($detail || $isWall)
                        {
                            $return .= '<p class="links">
                                <a class="ico pressRelease" href="'.$team->getProfileLink('press-releases').'" title="zobrazit všechny tiskové zprávy týmu '.$team->getName().'">tiskové zprávy týmu</a>
                            ';
                        }
                        else
                        {
                            $return .= '<p class="links">
                                <span class="ico pressRelease">tisková zpráva</span>
                            ';
                        }

                        if(!empty($logged_user->id))
                        {
                            if(count($val['comments']) == 0)
                            {
                                $return .= '<span class="allComments"><a class="firstComment" title="'.$tr->tr('Přidat první komentář k tiskové zprávě').'" href="#">'.$tr->tr('přidat první komentář').'</a>&nbsp;<span class="separator">|</span>&nbsp;</span>';
                            }
                            elseif($val['comments_count'])
                            {
                                $return .= '<span class="allComments"><!-- -->';
                                // zobrazujeme jen kdyz nejsme na detailu
                                if(!$detail && $val['comments_count'] > LIST_COMMENT_COUNT_MAX)
                                {
                                    $return .= '<a class="allComments" href="'.$link.'"><span title="Zobrazit všechny komentáře"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.$val['comments_count'].')</span></a>&nbsp;|&nbsp;';
                                }
                                $return .= '</span>';
                            }

                            if(!in_array($logged_user->id, array_keys($val['like'])))
                            {
                                $text = $tr->tr('líbí se mi');
                            }
                            else
                            {
                                $text = $tr->tr('už se mi nelíbí');
                            }
                                //$return .= '<a href="'.$pr->getLink().'" title="zobrazit celou tiskovou zprávu">zobrazit celou tiskovou zprávu</a>
                                $return .= '<a class="likeIt" href="#" title="'.$text.'" data-id="'.$object_id.'">'.$text.'</a>';
                        }
                        $return .= '</p>
                    </div><!-- .post -->';

                    $return .= printComments($object_id, $val, $link);

                    if($team->isAdmin($logged_user) && !$isWall && !$detail)
                    {
                        $return .= '<div class="controls">
                            <a href="#" class="editBig" data-id="'.$object_id.'" title="Upravit tiskovou zprávu"><!-- --></a>
                            <a href="#" class="deleteBig" data-id="'.$object_id.'" title="Smazat tiskovou zprávu"><!-- --></a>
                        </div>';
                    }

                $return .= '</div><!-- .content -->
            </div><!-- .box -->';
        }



        // editace soupisek
        if(in_array($val['type'], array('roster-insert', 'roster-delete')))
        {
            $val['value'] = unserialize($val['value']);

            $object_id = $val['team_id'] . '_'. $val['id'] . '_' . $val['type'];
            $team = new Team($val);
            // kvůli ajaxu
            if(empty($team->name))
            {
                $team = TeamDAO::get($val['team_id']);
            }

            $link = $team->getPressDetailLink('Změny na soupisce', $object_id);

            $return .= '
            <div class="box news'.($detail ? ' pressReleaseDetail' : '').'">
                <div class="content" data-id="'.$object_id.'-'.intval($val['team_id']).'">
                    <h2 class="boxTitle">'.$team->getTeamLink().'</h2>';

                    $return .= '<div class="post cleaned object-'.$object_id.'">
                        <p class="picture">
                            <a href="'.$team->getProfileLink().'" title="Zobrazit profil týmu '.$team->getName().'"><img src="'.$team->getTeamLogo('medium').'" alt="'.$team->getName().'" width="46" height="46" /></a>
                        </p>';

                        $text = $tr->tr('Tým')  . ' ' . $team->getTeamLink() . ' ';
                        switch($val['type'])
                        {
                        	case 'roster-insert':
                        		$text .= $tr->tr('přidal na soupisku');
                        	break;

                        	case 'roster-delete':
                        		$text .= $tr->tr('odebral ze soupisky');
                        	break;
                        }

                        $tmp = array();
                        $other_items = array();

                        if(!is_array($val['value']['users']))
                        {
                            $val['value']['users'] = array();
                        }

                        $val['value']['users'] = array_reverse($val['value']['users']);

                        foreach($val['value']['users'] as $user_id)
			            {
			                $user = UserDAO::get($user_id);
			                if(!$user instanceof User || empty($user->id))
			                {
			                    continue;
			                }

			                $tmp[] = '' . $user->getAnchor() . '';
                            $other_items[] = '<a href="'.$user->getProfileLink().'" title="Zobrazit profil uživatele '.$user->getShortName().'">'.$user->getShortName().'</a>';
			            }

			            if(count($tmp) == 0)
			            {
			                $text = '';
			            }
			            elseif(count($tmp) == 1)
			            {
			                $text .= ' ' . $tr->tr('uživatele') . ' ' . current($tmp) . '.';
			            }
			            elseif(count($tmp) == NOTICE_USER_COUNT)
			            {
			            	$text .= ' ' . $tr->tr('uživatele') . ' ' . implode($tmp, ' a ') . '.';
			            }
			            elseif(count($tmp) > NOTICE_USER_COUNT)
			            {
			                $other = count($tmp) - NOTICE_USER_COUNT;

			                $text .= ' ' . $tr->tr('uživatele') . ' ' . implode(array_slice($tmp, 0, NOTICE_USER_COUNT, true), ', ') . ' a <span class="help noIcon">' . $other .'&nbsp;' .decline($other, 'dalšího', 'další', 'dalších') . ' ' . decline($other, 'uživatele', 'uživatele', 'uživatelů') . '</span>.';

                            $other_items = array_slice($other_items, NOTICE_USER_COUNT, LIKE_LIMIT, true);
			                $text .= '<span class="tooltip users">'.implode($other_items, '<br />').'<br /><a class="more light popUpAjax" data-type="object-likes" data-id="'.$object_id.'" href="'.PATH_WEB_ROOT.'ajax.php?getRosterChangeUsers=true&amp;object_id='.$object_id.'" title="Zobrazit všechny komu se líbí">Zobrazit vše...</a></span>';
			            }

                        $return .= '<p class="text">' . $text . '</p>';


                        if($detail || $isWall)
                        {
                            $return .= '<p class="links">
                                <a class="ico roster" href="'.$team->getProfileLink('roster').'" title="zobrazit soupisku týmu '.$team->getName().'">soupiska</a>
                            ';
                        }
                        else
                        {
                            $return .= '<p class="links">
                                <span class="ico pressRelease">soupiska</span>
                            ';
                        }

                        if(count($val['comments']) == 0)
                        {
                            $return .= '<span class="allComments"><a class="firstComment" title="'.$tr->tr('Přidat první komentář k tiskové zprávě').'" href="#">'.$tr->tr('přidat první komentář').'</a>&nbsp;<span class="separator">|</span>&nbsp;</span>';
                        }
                        elseif($val['comments_count'])
                        {
                            $return .= '<span class="allComments"><!-- -->';
                            // zobrazujeme jen kdyz nejsme na detailu
                            if(!$detail && $val['comments_count'] > LIST_COMMENT_COUNT_MAX)
                            {
                                $return .= '<a class="allComments" href="'.$link.'"><span title="Zobrazit všechny komentáře"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.$val['comments_count'].')</span></a>&nbsp;|&nbsp;';
                            }
                            $return .= '</span>';
                        }
                        if(!in_array($logged_user->id, array_keys($val['like'])))
                        {
                            $text = $tr->tr('líbí se mi');
                        }
                        else
                        {
                            $text = $tr->tr('už se mi nelíbí');
                        }

                            $return .= '<a class="likeIt" href="#" title="'.$text.'" data-id="'.$object_id.'">'.$text.'</a>
                        </p>
                    </div><!-- .post -->';

					$return .= printComments($object_id, $val, $link);

                    if($team->isAdmin($logged_user) && !$isWall)
                    {
                        $return .= '<div class="controls">
                            <a href="#" class="editBig" data-id="'.$object_id.'" title="Upravit tiskovou zprávu"><!-- --></a>
                            <a href="#" class="deleteBig" data-id="'.$object_id.'" title="Smazat tiskovou zprávu"><!-- --></a>
                        </div>';
                    }

                $return .= '</div><!-- .content -->
            </div><!-- .box -->';
        }



        // editace soupisek
        if(in_array($val['type'], array('gallery-insert')))
        {
            $val['value'] = unserialize($val['value']);

            $object_id = $val['team_id'] . '_'. $val['id'] . '_' . $val['type'];
            $team = new Team($val);
            // kvůli ajaxu
            if(empty($team->name))
            {
                $team = TeamDAO::get($val['team_id']);
            }

            $gallery = GalleryDAO::getGallery($val['team_id'], $val['parent_id']);
            if(!$gallery instanceof Gallery)
            {
                continue;
            }
            if(!is_array($val['value']['photos']))
            {
                $val['value']['photos'] = array();
            }
            $val['value']['photos'] = array_reverse($val['value']['photos']);

            $link = $team->getGalleryChangeLink('Nové fotky v galerii', $object_id);

            $return .= '
            <div class="box news gallery">
                <div class="content" data-id="'.$object_id.'-'.intval($val['team_id']).'">
                    <h2 class="boxTitle">'.$team->getTeamLink().'</h2>';

                    $return .= '<div class="post cleaned object-'.$object_id.'">
                        <p class="picture">
                            <a href="'.$team->getProfileLink().'" title="Zobrazit profil týmu '.$team->getName().'"><img src="'.$team->getTeamLogo('medium').'" alt="'.$team->getName().'" width="46" height="46" /></a>
                        </p>';

                        $text = $tr->tr('Tým')  . ' ' . $team->getTeamLink() . ' ';
                        $text .= $tr->tr('přidal nové fotografie do galerie');
                        $text .= ' <a href="'.$gallery->getLink().'">'.$gallery->getName().'</a>.<span class="photos">';

                        $tmp = array();
                        $other_items = array();
                        $photo_count = 0;

                        foreach($val['value']['photos'] as $photo)
                        {
                            $file = $gallery->getPhotoPath() . $photo['filename'].'s' . $photo['extension'];
                            if(!file_exists($file))
                            {
                                continue;
                            }
                            $text .= '<span class="item"><a href="'.$gallery->getLink().'#'.@$photo['id'].'" title="'.@$photo['description'].'"><img src="/'.$file.'" /></a></span>';

                            if(++$photo_count == 4)
                            {
                                break;
                            }
                        }

                        $return .= '<p class="text">' . $text . '</span></p>';

                        $return .= '<p class="links">
                            <span class="ico pressRelease"><a href="'.$gallery->getLink().'">fotogalerie</a></span>
                        ';

                        if(count($val['comments']) == 0)
                        {
                            $return .= '<span class="allComments"><a class="firstComment" title="'.$tr->tr('Přidat první komentář k tiskové zprávě').'" href="#">'.$tr->tr('přidat první komentář').'</a>&nbsp;<span class="separator">|</span>&nbsp;</span>';
                        }
                        elseif($val['comments_count'])
                        {
                            $return .= '<span class="allComments"><!-- -->';
                            // zobrazujeme jen kdyz nejsme na detailu
                            if(!$detail && $val['comments_count'] > LIST_COMMENT_COUNT_MAX)
                            {
                                $return .= '<a class="allComments" href="'.$link.'"><span title="Zobrazit všechny komentáře"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.$val['comments_count'].')</span></a>&nbsp;|&nbsp;';
                            }
                            $return .= '</span>';
                        }
                        if(!in_array($logged_user->id, array_keys($val['like'])))
                        {
                            $text = $tr->tr('líbí se mi');
                        }
                        else
                        {
                            $text = $tr->tr('už se mi nelíbí');
                        }

                            $return .= '<a class="likeIt" href="#" title="'.$text.'" data-id="'.$object_id.'">'.$text.'</a>
                        </p>
                    </div><!-- .post -->';

                    $return .= printComments($object_id, $val);

                $return .= '</div><!-- .content -->
            </div><!-- .box -->';
        }



        // zprava na nastenku tymu
        if(in_array($val['type'], array('teampost')))
        {
            $val['value'] = unserialize($val['value']);
            foreach($val['value'] as $k => $v)
            {
                $val['value']->$k = stripslashes(strip_tags($v));
            }
            $object_id = $val['team_id'] . '_'. $val['id'] . '_' . $val['type'];
            $team = new Team($val);

            // kvůli ajaxu
            if(empty($team->name))
            {
                $team = TeamDAO::get($val['team_id']);
            }
            $pr = new TeamPost($val);

            // potrebujeme info o uzivateli
            $user = UserDAO::get($val['user_id']);

            $return .= '
            <div class="box news teampost'.($detail ? ' detail' : '').'">
                <div class="content" data-id="'.$object_id.'-'.intval($val['team_id']).'">
                    <h2 class="boxTitle">'.$user->getAnchor().' | ';
                    if($detail)
                    {
                        $return .= $tr->tr('Vzkaz pro členy týmu') . ' ' . $team->getName() . '</h2>';
                    }
                    else
                    {
                        $return .= '<a href="'.$team->getTeampostDetailLink($tr->tr('tymovy-vzkaz'), $object_id).'" class="title" title="Zobrazit celý vzkaz">'.$tr->tr('Vzkaz pro členy týmu'). ' ' . $team->getName() . '</a></h2>';
                    }
                    $return .= '<div class="post cleaned object-'.$object_id.'">
                        <p class="picture">
                            <a href="'.$user->getProfileLink().'" title="Zobrazit profil uživatele '.$user->getName().'"><img src="'.$user->getUserImage('medium').'" alt="'.$user->getName().'" width="46" height="46" /></a>
                        </p>';
                        if($detail)
                        {
                            $return .= '<p class="longText">'.nl2br($val['value']->text).'</p>';
                        }
                        else
                        {
                            $return .= '<p class="text">'.(mb_strlen($val['value']->text) > PEREX_LENGTH_SHORT ? mb_substr($val['value']->text, 0, PEREX_LENGTH_SHORT, 'utf-8') . '&hellip;' : $val['value']->text).'</p>';
                        }

                        /*
                        if($detail || $isWall)
                        {
                            $return .= '<p class="links">
                                <a class="ico pressRelease" href="'.$team->getProfileLink('press-releases').'" title="zobrazit všechny tiskové zprávy týmu '.$team->getName().'">tiskové zprávy týmu</a>
                            ';
                        }
                        else
                        {*/
                            $return .= '<p class="links">
                                <span class="ico pressRelease">týmový vzkaz</span>
                            ';
                        //}

                        if(count($val['comments']) == 0)
                        {
                            $return .= '<span class="allComments"><a class="firstComment" title="'.$tr->tr('Přidat první komentář ke vzkazu').'" href="#">'.$tr->tr('přidat první komentář').'</a>&nbsp;<span class="separator">|</span>&nbsp;</span>';
                        }
                        elseif($val['comments_count'])
                        {
                            $return .= '<span class="allComments"><!-- -->';
                            // zobrazujeme jen kdyz nejsme na detailu
                            if(!$detail && $val['comments_count'] > LIST_COMMENT_COUNT_MAX)
                            {
                                $return .= '<a class="allComments" href="'.getNewsLink($object_id, $val['value']->title).'"><span title="Zobrazit všechny komentáře"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.$val['comments_count'].')</span></a>&nbsp;|&nbsp;';
                            }
                            $return .= '</span>';
                        }
                        if(!in_array($logged_user->id, array_keys($val['like'])))
                        {
                            $text = $tr->tr('líbí se mi');
                        }
                        else
                        {
                            $text = $tr->tr('už se mi nelíbí');
                        }
                            //$return .= '<a href="'.$pr->getLink().'" title="zobrazit celou tiskovou zprávu">zobrazit celou tiskovou zprávu</a>
                            $return .= '<a class="likeIt" href="#" title="'.$text.'" data-id="'.$object_id.'">'.$text.'</a>
                        </p>
                    </div><!-- .post -->';

                    $return .= printComments($object_id, $val);

                    if($team->isAdmin($logged_user) && !$isWall)
                    {
                        $return .= '<div class="controls">
                            <a href="#" class="deleteBig" data-id="'.$object_id.'" title="Smazat vzkaz"><!-- --></a>
                        </div>';
                    }

                $return .= '</div><!-- .content -->
            </div>';
        }



        // vysledek zapasu
        if(in_array($val['type'], array('event-result')))
        {
            $event_info = explode('_', $val['parent_id']);

            if(count($event_info) != 3)
            {
                continue;
            }

            $object_team = TeamDAO::get($val['team_id']);
            if(!$object_team instanceof Team || empty($object_team->id))
            {
                continue;
            }

            $event_id = $event_info[1];
            $event_team_id = $event_info[0];

            $event = EventDAO::get($event_id, $event_team_id);
            if(!$event instanceof Event)
            {
                continue;
            }

            $result = unserialize($event->result);
            if(!count($result))
            {
                continue;
            }

            $info = unserialize($event->info);

            // zajima nas hostujici tym
            if($event->team_id == $val['team_id'])
            {
                if(empty($event->oponent_id))
                {
                    $data = array(
                        'name' => $info[0]['team_name']
                    );
                    $oponent_team = new Team($data);
                }
                else
                {
                    $oponent_team = TeamDAO::get($event->oponent_id);
                    if(!$oponent_team instanceof Team || empty($oponent_team->id))
                    {
                        continue;
                    }
                }

                $home_final = $result['home']['final'];
                $away_final = $result['away']['final'];

            }
            else
            {
                $oponent_team = TeamDAO::get($event_team_id);
                if(!$oponent_team instanceof Team || empty($oponent_team->id))
                {
                    continue;
                }

                $home_final = $result['away']['final'];
                $away_final = $result['home']['final'];
            }

            $object_id = $val['team_id'] . '_'. $val['id'] . '_' . $val['type'];
            //$object_id = $val['parent_id'];

            $link = $object_team->getProfileLink('event-detail') . $event_id . '-' . $event_team_id . '/';

            $return .= '
            <div class="box news">
                <div class="content" data-id="'.$object_id.'-'.intval($val['team_id']).'">
                    <h2 class="boxTitle">'.$object_team->getTeamLink() . '</h2>';

                    $return .= '<div class="post cleaned object-'.$object_id.'">
                        <p class="picture">
                            <a href="'.$object_team->getProfileLink().'" title="Zobrazit profil týmu '.$object_team->getName().'"><img src="'.$object_team->getTeamLogo('medium').'" alt="'.$object_team->getName().'" width="46" height="46" /></a>
                        </p>';

                        $text = $tr->tr('Tým')  . ' ' . $object_team->getTeamLink() . ' ';
                        $text .= $tr->tr('odehrál').' '.date('j. n. Y', strtotime($event->start)) . ' "' . $info[0]['name'] . '" ' . $tr->tr('proti týmu') . ' ';
                        $text .= (empty($oponent_team->id) ? $oponent_team->getName() : $oponent_team->getTeamLink()) . ' s výsledkem  <a href="'.$link.'">' . $home_final . ':' . $away_final . '</a>.';

                        $return .= '<p class="text">' . $text . '</span></p>';

                        $return .= '<p class="links">
                            <span class="ico event"><a href="'.$object_team->getProfileLink('events').'">události</a></span>
                        ';

                        if(count($val['comments']) == 0)
                        {
                            $return .= '<span class="allComments"><a class="firstComment" title="'.$tr->tr('Přidat první komentář k událoti').'" href="#">'.$tr->tr('přidat první komentář').'</a>&nbsp;<span class="separator">|</span>&nbsp;</span>';
                        }
                        elseif($val['comments_count'])
                        {
                            $return .= '<span class="allComments"><!-- -->';
                            // zobrazujeme jen kdyz nejsme na detailu
                            if(!$detail && $val['comments_count'] > LIST_COMMENT_COUNT_MAX)
                            {
                                $return .= '<a class="allComments" href="'.$link.'"><span title="Zobrazit všechny komentáře"><span class="hidden-md hidden-sm hidden-xs">'.$tr->tr('zobrazit').'</span> '.$tr->tr('všechny komentáře').' ('.$val['comments_count'].')</span></a>&nbsp;|&nbsp;';
                            }
                            $return .= '</span>';
                        }
                        if(!in_array($logged_user->id, array_keys($val['like'])))
                        {
                            $text = $tr->tr('líbí se mi');
                        }
                        else
                        {
                            $text = $tr->tr('už se mi nelíbí');
                        }

                            $return .= '<a class="likeIt" href="#" title="'.$text.'" data-id="'.$object_id.'">'.$text.'</a>
                        </p>
                    </div><!-- .post -->';

                    $return .= printComments($object_id, $val);

                $return .= '</div><!-- .content -->
            </div><!-- .box -->';
        }
    }

    if(count($wall) == 0)
    {
        $return .= '<div class="msg warning icon noClose" id="noMoreMessages">Žádné další příspěvky k zobrazení.</div>';
    }
    return $return;
}

function printLikeList($likes = array(), $object_id = '', $showAll = false)
{
    global $tr;

    $return = '<ul class="list">';
    $i = 0;
    foreach($likes as $like)
    {
        if($like instanceof User)
        {
            $user = $like;
        }
        else
        {
            $user = new User();
            $user->parseAndSet($like);
        }

        if($showAll)
        {
            $class = '';
            if($i == 0)
            {
                $class = 'first';
            }


            if(!empty($user->team))
            {
                $team = TeamDAO::get($user->team);
            }
            elseif(count($user->getTeamList()) > 0)
            {
                reset($user->teams);
                $team = current($user->teams);
            }
            else
            {
                $team = false;
            }

            $return .= '
            <li class="item ' . $class . ' cleaned">
                <p class="picture"><a href="'.$user->getProfileLink().'" title="'.$tr->tr('Zobrazit profil uživatele').' '.$user->getName().'"><img src="'.$user->getUserImage('medium').'" alt="'.$user->getName().'" width="46" height="46" /></a></p>
                <h5 class="name"><a href="'.$user->getProfileLink().'" title="'.$tr->tr('Zobrazit profil uživatele').' '.$user->getName().'">'.$user->getName().'</a></h5>';
            if($team !== false)
            {
                $return .= '<h5 class="team"><a href="'.$team->getProfileLink().'" title="'.$tr->tr('Zobrazit profil týmu').' '.$team->getName().'">['.$team->getName().']</a></h5>';
            }
            else
            {
                $return .= '<h5 class="team">['.$tr->tr('Volný hráč').']</h5>';
            }
            $return .= '</li>';
        }
        else
        {
            if($i == LIKE_LIMIT && count($likes) > LIKE_LIMIT)
            {
                break;
            }

            $return .= '<li class="item"><a href="'.$user->getProfileLink().'" title="Zobrazit profil uživatele '.$user->getShortName().'">'.$user->getShortName().'</a></li>';
        }
        ++$i;
    }
    if(count($likes) > 0 && !$showAll)
    {
        $return .= '<li><a class="light popUpAjax" data-type="object-likes" data-id="'.$object_id.'" href="'.PATH_WEB_ROOT.'ajax.php?getLikes=true&amp;object_id='.$object_id.'" title="Zobrazit všechny komu se líbí">Zobrazit vše...</a></li>';
    }
    $return .= '</ul>';

    return $return;
}

function getNewsLink($object_id, $title, $type = 'news-detail')
{
    return (PATH_WEB_ROOT . $type . '/' . friendly_url($title) .'-' . $object_id . '/');
}

/**
 *
 * Funkce vypíše kód pro komentář
 *
 **/
function printComment($info, $type)
{
    global $logged_user;

    ob_start();
    // informace o komentari
    $info['value'] = unserialize($info['value']);
    if(!is_object($info['value']))
    {
        $info['value'] = new Object($info['value']);
    }

    // mame uzivatele? mel by byt!
    if(!empty($info['user_id']))
    {
        $user = UserDAO::get($info['user_id']);
    }

    // jestliže už je uživatel smazán, tak dáme nějakého default
    if(!$user instanceof User)
    {
        $data['id'] = 0;
        $data['fname'] = 'Profil uživatele';
        $data['sname'] = 'již neexistuje';
        $team_id = 0;
        $user = new User($data);
    }
    // pokud se jedna o tympost, tak tym by mel byt ten kterem post patri a nezajima nas aktivni tym
    elseif(substr_count($info['value']->parent_id, 'teampost'))
    {
        $team_id = $info['value']->team_id;
        $team = TeamDAO::get($team_id);
    }
    else
    {
        $team_id = $user->team;
        $team = TeamDAO::get($team_id);
    }
?>
    <div class="comment-container cleaned" id="comment-<?=$team_id;?>_<?=$info['id']?>">
        <div class="comment cleaned">
            <h3 class="name"><?=$user->getAnchor();?>
            <?php
            if($team_id > 0)
            {
            ?>
                <span class="teamName">[<a href="<?=$team->getProfileLink()?>" title="Zobrazit tým <?=$team->getName();?>"><?=$team->getName();?></a>]</span>
            <?php
            }
            ?>
            </h3>

            <? // <?=getFormatedDate($info['created'], COMMENT_DATE_TYPE);
            $timeago = ' timeago';
            if(getDateDiff(date('Y-m-d',strtotime($info['created'])), date('Y-m-d')) > CHANGE_DATE_LIMIT)
            {
                $timeago = '';
            }
            ?>
            <span class="date<?=$timeago?>" title="<?=date('Y-m-d\TH:i:sP',strtotime($info['created']))?>"><?=date('j.n. G:i',strtotime($info['created']))?></span>
            <p class="text clear"><?=$info['value']->text;?></p>

            <?php
            if($info['user_id'] == $logged_user->id)
            {
            ?>
                <a class="deleteComment" href="#" data-id="<?=$info['id']?>" data-team-id="<?=empty($info['team_id']) ? '0' : $info['team_id']?>" title="Odebrat"><!-- --></a>
            <?php
            }
            ?>
        </div>
        <p class="picture">
            <?php if($user->id > 0) { ?>
            <a href="<?=$user->getProfileLink();?>" title="Zobrazit uživatele <?=$user->getName();?>"><img src="<?=$user->getUserImage('medium')?>" alt="<?=$user->getName();?>" width="46" height="46" /></a>
            <?php } else { ?>
            <img src="<?=$user->getUserImage('medium')?>" alt="<?=$user->getName();?>" width="46" height="46" />
            <?php } ?>
        </p>
    </div><?php
    return ob_get_clean();
}

function cropOriginal($im_filename, $th_filename, $width, $x, $y)
{
    $th_path = dirname($th_filename);
    $th_filename = basename($th_filename);

    // vytvorime si objekt
    $layer = PHPImageWorkshop\ImageWorkshop::initFromPath($im_filename);

    // cropneme
    $layer->cropInPixel($width, $width, $x, $y, 'LT');

    // a ulozime
    $layer->save($th_path, $th_filename, true, null, 80);

}


function GenerateGalleryThumbnail($im_filename, $th_filename, $width = 70, $height = 70, $quality = 100)
{
    // vytvorime si objekt
    $layer = PHPImageWorkshop\ImageWorkshop::initFromPath($im_filename);

    $expectedWidth = $width;
    $expectedHeight = $height;

    // Determine the largest expected side automatically
    $largestSide = ($expectedWidth > $expectedHeight ? $expectedWidth : $expectedHeight);

    // Get a squared layer
    // $layer->cropMaximumInPixel(0, 0, "MM");

     // pokud je na vysku
    if($layer->getWidth() < $layer->getHeight())
    {
        if($layer->getWidth() > $expectedWidth)
        {
            $largestSide = $height;
            // Resize the squared layer with the largest side of the expected thumb
            $layer->resizeInPixel($largestSide, null, true);
        }
    }
    elseif($layer->getHeight() > $expectedHeight)
    {
        // Resize the squared layer with the largest side of the expected thumb
        $layer->resizeInPixel($largestSide, null, true);
    }

    // Crop the layer to get the expected dimensions
    // $layer->cropInPixel($expectedWidth, $expectedHeight, 0, 0, 'MM');

    $th_path = dirname($th_filename);
    $th_filename = basename($th_filename);

    // a ulozime
    $layer->save($th_path, $th_filename, true, null, $quality);

    return 99;
}

function GenerateThumbnail($im_filename, $th_filename, $width = 70, $height = 70, $quality = 100, $square = true)
{
    $th_path = dirname($th_filename);
    $th_filename = basename($th_filename);

    // vytvorime si objekt
    $layer = PHPImageWorkshop\ImageWorkshop::initFromPath($im_filename);

    if($square)
    {
        // cropneme
        $layer->resizeInPixel($width, $width, true);
    }
    else
    {
        $expectedWidth  = $width;
        $expectedHeight = $height;

        // Determine the largest expected side automatically
        ($expectedWidth > $expectedHeight) ? $largestSide = $expectedWidth : $largestSide = $expectedHeight;

        // Get a squared layer
        $layer->cropMaximumInPixel(0, 0, "MM");

        // Resize the squared layer with the largest side of the expected thumb
        $layer->resizeInPixel($largestSide, $largestSide);

        // Crop the layer to get the expected dimensions
        $layer->cropInPixel($expectedWidth, $expectedHeight, 0, 0, 'MM');
    }

    // a ulozime
    $layer->save($th_path, $th_filename, true, null, $quality);

    return 99;
}


function cropTeamPhoto($im_filename, $crop_filename, $expectedWidth, $expectedHeight)
{
    // The original image must exist
    if(is_file($im_filename))
    {
        // Let's create the directory if needed
        $th_path = dirname($crop_filename);
        $crop_filename = basename($crop_filename);

        if(!is_dir($th_path))
        {
            @mkdir($th_path, 0777, true);
        }

        // vytvorime si objekt
        $layer = PHPImageWorkshop\ImageWorkshop::initFromPath($im_filename);

        // Determine the largest expected side automatically
        $largestSide =  ($expectedWidth > $expectedHeight ? $expectedWidth : $expectedHeight);

        // Resize to get the thumbnail
        //$layer->resizeInPixel($expectedWidth, 120, true, 0, 0, 'MM');

        // Resize the squared layer with the largest side of the expected thumb
        $layer->resizeInPixel($largestSide, null, true);

        // Get a squared layer
        //$layer->cropMaximumInPixel(0, 0, "MM");

        // Crop the layer to get the expected dimensions
        //$layer->cropInPixel($expectedWidth, $expectedHeight, 0, 0, 'MM');

        // ulozime
        $layer->save($th_path, $crop_filename, true, null, 100);

        return 99;
    }

    return 1;
}

/**
 *
 * vygeneruje nahledy fotek
 *
 */
function generateMainPictures($im_filename, $th_filename, $crop_filename, $width)
{
    // The original image must exist
    if(is_file($im_filename))
    {
        // Let's create the directory if needed
        $th_path = dirname($th_filename);
        $th_filename = basename($th_filename);

        $crop_path = dirname($crop_filename);
        $crop_filename = basename($crop_filename);
        if(!is_dir($th_path))
        {
            @mkdir($th_path, 0777, true);
        }

        // vytvorime si objekt
        $layer = PHPImageWorkshop\ImageWorkshop::initFromPath($im_filename);

        // resizneme na spravno uvelikost a ulozime original
        $layer->resizeInPixel($width, null, true);

        // ulozime
        $layer->save($th_path, $th_filename, true, null, 100);


        if(is_file($th_path.'/'.$th_filename))
        {
            // vytvorime si objekt pro cropnuty obrazek
            $layer = PHPImageWorkshop\ImageWorkshop::initFromPath($th_path.'/'.$th_filename);

            // cropneme na ctverec
            $layer->cropMaximumInPixel(0, 0, 'MM');

            // ulozime
            $layer->save($crop_path, $crop_filename, true, null, 100);
        }

        return 99;

    }
    // soubor neexistuje
    return 1;
}



/** Vytvoření přátelského URL
* @param string řetězec v kódování UTF-8, ze kterého se má vytvořit URL
* @return string řetězec obsahující pouze čísla, znaky bez diakritiky, podtržítko a pomlčku
* @copyright Jakub Vrána, http://php.vrana.cz/
*/
function friendly_url($nadpis)
{
    $url = $nadpis;
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
    $url = trim($url, "-");
    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
    $url = strtolower($url);
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
    return $url;
}


function fix_link($link, $http = false, $onlyFirstLevel = false)
{
    if($http === true)
    {
        if(substr($link, 0, 7) == 'http://' || substr($link, 0, 8) == 'https://')
        {
            if($onlyFirstLevel)
            {
                preg_match('#^(http[s]?:\/\/[^\/$]+)\/?.*$#', $link, $match);

//                return $link;
                return $match[1];
            }
            else
            {
                return $link;
            }
        }
        else
        {
            if($onlyFirstLevel)
            {
                preg_match('#^(http[s]?:\/\/[^\/$]+)\/?.*$#', $link, $match);

                return 'http://'.$match[1];
            }
            else
            {
                return 'http://'.$link;
            }
        }
    }
    else
    {
        if(substr($link, 0, 7) == 'http://' || substr($link, 0, 8) == 'https://')
        {
            if($onlyFirstLevel)
            {
                preg_match('#^http[s]?:\/\/([^\/$]+)\/?.*$#', $link, $match);

                return $match[1];
            }
            else
            {
                return str_replace(array('http://', 'https://'), array('',''), $link);
            }
        }
        else
        {
            if($onlyFirstLevel)
            {
                preg_match('#^([^\/$]+)\/?.*$#', $link, $match);

                return $match[1];
            }
            else
            {
                return $link;
            }
        }
    }
}


function decline($count, $first, $second, $third = '')
{
    // když je to jednička, tak vrátíme první tvar
    // např. "je fanouškem 1 týmu"
    if($count == 1)
    {
        return $first;
    }

    // máme jenom dva tvary
    if(empty($third))
    {
        // je fanouškem "více týmů"
        return $second;
    }

    if($count > 1 && $count < 5)
    {
        return $second;
    }

    return $third;
}

function print_searched_team($foundTeam)
{
    // v preloadu nebo configu FIXME
    global $gadjectives;

    $return = '
    <h3 class="name">
        <span class="small">' . $gadjectives[$foundTeam->sport_id] . ' tým</span><br />
        <a href="' . $foundTeam->getProfileLink() . '" title="Zobrazit profil týmu ' . $foundTeam->getName() . '">' . $foundTeam->getName() . '</a>
    </h3>
    <p class="picture">
        <a href="' . $foundTeam->getProfileLink() . '" title="Zobrazit profil týmu ' . $foundTeam->getName() . '">
            <img src="' . $foundTeam->getTeamLogo('medium') . '" alt="' . $foundTeam->getName() . '" />
        </a>
    </p>
    <p class="info">
        ' . $foundTeam->players . ' ' . decline($foundTeam->players, 'člen', 'členové', 'členů') . '&nbsp;&nbsp;/&nbsp;&nbsp;
        <span class="fans">' . $foundTeam->fans . ' ' . decline($foundTeam->fans, 'fanoušek', 'fanoušci', 'fanoušků') . '</span>&nbsp;&nbsp;/&nbsp;&nbsp;
        <span class="observers">' . $foundTeam->observers . ' ' . decline($foundTeam->observers, 'odběratel', 'odběratelé', 'odběratelů') . '</span>
    </p>';

    return $return;
}


function print_searched_teams($searched_teams = array(), User $logged_user, $view_more = true)
{
    global $tr;

    $return = '';

    if(count($searched_teams) > 0)
    {
        foreach($searched_teams as $foundTeam)
        {
            if(!empty($foundTeam->user_id))
            {

                $return .= '<div class="result cleaned">
                <div class="left cleaned">';
                    $return .= print_searched_team($foundTeam);
                $return .= '</div><!-- .left -->

                <div class="right">';
                    if(!$foundTeam->isAdmin($logged_user) && !$foundTeam->isPlayer($logged_user))
                    {
                        if(!$foundTeam->isFan($logged_user->id) && !$foundTeam->isObserver($logged_user->id))
                        {
                            require(BLOCK_PATH.'team-links-visitor.php');
                        }
                        elseif($foundTeam->isFan($logged_user->id))
                        {
                            require(BLOCK_PATH.'team-links-fan.php');
                        }
                        else
                        {
                            require(BLOCK_PATH.'team-links-observer.php');
                        }

                        // nacita se v tech jednotlivych blocich
                        $return .= $linksHtml;

                         /*
                        <a href="#" class="button buttonC icon sendTeamMessage" title="Napsat zprávu týmu <?=$foundTeam->getName()?>">
                            napsat zprávu týmu
                        </a>
                        */
                    }

                $return .= '</div><!-- .right -->';
                $return .= '</div><!-- .result -->';
            }
        }

        if($view_more)
        {
            $return .= '<p class="viewMore"><a href="#">zobrazit další týmy</a></p>';
        }
    }

    return $return;
}


function print_searched_users($searched_users, $logged_user, $view_more)
{
    global $tr;

    $return = '';

    if(count($searched_users) > 0)
    {
        foreach($searched_users as $searched_user)
        {
            $return .= '<div class="result cleaned">
                <div class="left cleaned">
                    <h3 class="name">
                        <span>' . $searched_user->fname . '</span><br />
                        <a href="' . $searched_user->getProfileLink() . '" title="Zobrazit uživatele ' . $searched_user->getName() . '">' . $searched_user->sname . '</a>
                    </h3>
                    <p class="picture">
                        <a href="' . $searched_user->getProfileLink() . '" title="Zobrazit profil uživatele ' . $searched_user->getName() . '">
                            <img src="' . $searched_user->getUserImage('medium') . '" alt="' . $searched_user->getName() . '" />
                        </a>
                    </p>
                    <p class="info">';
                        $count = count($searched_user->teams);

                        if($count > 0)
                        {
                            $i = 0;
                            $uteams = array();
                            $return .= 'člen ' . decline($count, 'týmu ', 'týmů: ');

                            foreach($searched_user->teams as $user_team)
                            {
                                $uteams[] = '<a href="'.$user_team->getProfileLink().'">'.$user_team->getName().'</a>';
                                if(++$i == 2)
                                {
                                    $count = $count - 2;
                                    if($count > 0)
                                    {
                                        $uteams[] = '+ '.$count . ' další ' . decline($count, 'tým', 'týmy', 'týmů');
                                    }
                                    break;
                                }
                            }
                            $return .= implode(', ', $uteams);
                        }
                        else
                        {
                            $return .= 'uživatel není členem žádného týmu';
                        }
                        //FBC Royal Gigolos, Golf Club Austerlitz, + 2 další týmy

                    $return .= '</p>
                </div><!-- .left -->

                <div class="right">';

                    if($logged_user->id != $searched_user->id && $searched_user->canReceivePm($logged_user))
                    {
                        $return .= '<a href="#sendNewMessage" data-id="'. $searched_user->id . '-' . getRecipientHash($searched_user->id) . '" class="button buttonC icon sendMessage popUpMedium" title="Pozvat uživateli ' . $searched_user->getName() . ' zprávu">
                            napsat uživateli zprávu
                        </a>';
                    }

                    if($logged_user->getActiveTeam()->isAdmin($logged_user))
                    {
                        // pokud je uz pozvan, tak mu vypiseme moznost zruseni pozvanky
                        if($logged_user->getActiveTeam()->isInvited($searched_user))
                        {
                            $return .= print_invite_leave_search($logged_user->getActiveTeam(), $searched_user);
                        }
                        // pokud neni hrac jiz v nasem tymu, tak mu vypiseme pozvanku
                        elseif(!$logged_user->getActiveTeam()->isPlayer($searched_user))
                        {
                            $return .= print_invite_user_search($logged_user->getActiveTeam(), $searched_user);
                        }
                    }


                $return .= '</div><!-- .right -->';
            $return .= '</div><!-- .result -->';
        }

        if($view_more)
        {
            $return .= '<p class="viewMore"><a href="#">zobrazit další uživatele</a></p> ';
        }
    }

    return $return;
}


function print_invite_leave_search($team, $user)
{
    return '<a href="#" data-team="' . $team->id . '" data-id="' . $user->id . '-' . getRecipientHash($user->id) . '" class="button buttonC icon leave" title="Zrušit pozvání do týmu ' . $team->getName() . '">zrušit pozvání do týmu</a>';
}

function print_invite_user_search($team, $user)
{
    return '<a href="#" data-team="' . $team->id . '" data-id="' . $user->id . '-' . getRecipientHash($user->id) . '" class="button buttonC icon invite" title="Pozvat uživatele do týmu ' . $team->getName() . '">pozvat do týmu</a>';
}

function print_invite_leave($team, $user)
{
    return '<li class="item leave"><a href="#" data-team="'.$team->id.'" data-id="'.$user->id.'-'.getRecipientHash($user->id).'" title="Zrušit pozvání uživatele '.$user->getName().' do týmu ">zrušit pozvání do týmu</a></li>';
}

function print_request_cancel($team, $user)
{
    return '<li class="item leave"><a href="#" data-team="'.$team->id.'" data-id="'.$user->id.'-'.getRecipientHash($user->id).'" title="Zrušit pozvání uživatele '.$user->getName().' do týmu ">zrušit pozvání do týmu</a></li>';
}

function print_invite_user($team, $user)
{
    return '<li class="item invite"><a href="#" data-team="' . $team->id . '" data-id="' . $user->id . '-' . getRecipientHash($user->id) . '" title="Pozvat uživatele '.$user->getName().' do týmu ">pozvat do týmu</a></li>';
}

function printUnpublishedRow($val)
{
    if(!is_object($val['value']))
    {
        $val['value'] = unserialize($val['value']);
    }

    return '<li class="item cleaned">
        <span class="date">'.date('j.n. G:i', strtotime($val['date_begin'])).'</span>
        <h4 class="headLine">'.$val['value']->heading.'</h4>
        <p class="control">
            <a href="#" class="edit" data-id="'.$val['value']->team_id . '_' . $val['id'] . '_' . $val['type'] . '" title="Upravit tiskovou zprávu a její nastavení"><!-- --></a>
            <a href="#" class="delete" data-id="'.$val['value']->team_id . '_' . $val['id'] . '_' . $val['type'] . '" title="Smazat tiskovou zprávu"><!-- --></a>
        </p>
    </li>';
}

function printPositionRoster($position, $players, $showAll = true)
{
    global $tr;

    // pokud nechceme zobrazovat vsechny hrace (na detailu ciziho tymu), tak musime vyfiltrovat ty ktere se nemaji zobrazovat
    if(!$showAll)
    {
        $show = array();

        if(is_array($players) && count($players) > 0)
        {
            foreach($players as $key => $player)
            {
                if(!empty($player['info']->show))
                {
                    $show[$key] = $player;
                }
            }
        }

        $players = $show;
    }

    // FIXME - překlad
    $return = '<h2 class="headline">'.$tr->tr($position->name2).'</h2>' . "\n";
    if(count($players) == 0 || !is_array($players))
    {
        $return .= '<div class="msg noClose">V této kategorii nejsou žádní hráči.</div>' . "\n";
    }
    else
    {
        $return .=  "\n" . '<table class="table roster" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th class="number" title="'.$tr->tr('číslo dresu').'">#</th>
                        <th class="player">'.$tr->tr('jméno').'<span class="hidden-xs"> '.$tr->tr('a příjmení').'</span></th>
                        <th class="position">'.$tr->tr('pozice').'</th>
                        <th class="age">'.$tr->tr('věk').'</th>
                        <th class="height">'.$tr->tr('výška').'</th>
                        <th class="weight">'.$tr->tr('váha').'</th>
                        <th class="function hidden-xxs">'.$tr->tr('funkce').'</th>
                    </tr>
                </thead>
                <tbody>';
                $i = 0;
                foreach($players as $info)
                {
                    $player = $info['user'];

                    if(!$player instanceof User)
                    {
                        continue;
                    }

                    $class = '';
                    $playerType = '';
                    if($i++ == 0)
                    {
                        $class = 'first';
                    }

                    if($info['info']->isHost())
                    {
                        $class .= ' host';
                        $playerType = '<span class="playerType" title="Hostující hráč">H</span>';
                    }

                    $return .= '<tr class="'.$class.'">
                        <td class="number">'.(!empty($info['info']->number) ? $info['info']->number : '').'</td>
                        <td class="player">
                            <a href="'.$player->getProfileLink().'" title="Zobrazit uživatele '.$player->getName().'" class="img"><img src="'.$player->getUserImage('small').'" alt="'.$player->getName().'" />'.$playerType.'</a>
                            <a href="'.$player->getProfileLink().'" title="Zobrazit uživatele '.$player->getName().'" class="name"><span class="firstName">'.$player->fname.'</span><br /><span class="lastName">'.$player->sname.'</span></a>
                        </td>
                        <td>'.$position->name.'</td>
                        <td>'.($player->birth != '0000-00-00' ? $player->getAge().' '.decline($player->getAge(), 'rok', 'roky', 'let') : '&ndash;').'</td>
                        <td>'.(!empty($player->height) ? $player->height . ' cm' : '&ndash;').'</td>
                        <td>'.(!empty($player->weight) ? $player->weight . ' kg' : '&ndash;').'</td>
                        <td class="function hidden-xxs">'.$info['info']->function.'</td>
                    </tr>';
                }

                $return .= '</tbody>
            </table>';
    }

    return $return;
}


function eventDetailPossitionAttend($position, $players, $attend, $event, $class = ' first')
{
    global $logged_user;

    $return = '';

    $return .= '<h3 class="attendanceTitle'.$class.'">'.$position->name2.'</h3>
        <table cellspacing="0" cellpadding="0" class="table attendance">
            <thead>
                <tr>
                    <th class="number">#</th>
                    <th class="player">jméno a příjmení</th>
                    <th data-id="' . $event->team_id . '-' . $event->id . '" class="attendanceDate">docházka</th>
                </tr>
            </thead>
            <tbody>';

    $invitedCount = 0;
    $i = 0;
    foreach($players as $info)
    {
        $player = $info['user'];
        $player->getSettings();

        $class = '';
        if($i++ == 0)
        {
            $class = 'first';
        }

        $playerType = '';
        if($info['info']->isHost())
        {
            $class .= ' host';
            $playerType = '<span class="playerType" title="Hostující hráč">H</span>';
        }

        if(is_array($event->info))
        {
            $einfo = $event->info;
        }
        else
        {
            $einfo = unserialize($event->info);
        }

        if($event->team_id == $logged_user->getActiveTeam()->id)
        {
            $team = TeamDAO::get($event->team_id);
            $attend_key = 'attend';
            $attend_team_key = 'attend_team';
        }
        else
        {
            $team = TeamDAO::get($event->oponent_id);
            $attend_key = 'attend_away';
            $attend_team_key = 'attend_team_away';
        }

        $comment = '';
        // nejdriv se mrknem zda je uzivatel vubec pozvanej
        if(@!in_array($player->id, @array_keys($einfo[0][$attend_key])) && @empty($einfo[0][$attend_team_key]))
        {
            continue;
        }
        else
        {
            $invitedCount++;
            // už se k ní vyjádřil
            if(isset($attend[$player->id]))
            {
                $cls = 'edited ' . $attend[$player->id]['attend'];

                switch($attend[$player->id]['attend'])
                {
                    case 'yes':
                        $title = 'Zúčastním se';
                        break;

                    case 'no':
                        $title = 'Nezúčastním se';
                        break;

                    case 'na':
                        $title = 'Zatím nevím';
                        break;

                    case 'blackDot':
                        $title = 'Neomluvená neúčast';
                        break;
                }

                if(!empty($attend[$player->id]['note']))
                {
                    $cls .= 'Comment';
                    $comment = '<span class="help"><!-- --></span><span class="tooltip">'.$attend[$player->id]['note'].'</span>';
                }
            }
            else
            {
                $cls = 'noInfo';
                $title = 'Nevyplněná docházka';
            }

            if($team->isAdmin($logged_user))
            {
                $cls .= ' editable';
            }
            elseif($player->id == $logged_user->id && date('Y-m-d') <= date('Y-m-d', strtotime($event->start)))
            {
                $cls .= ' editable';
            }
            else
            {
                $cls .= '';
            }

            $id = $event->team_id . '-' . $event->id . '-' . $player->id . '@' . getRecipientHash($player->id);

            $td = '<td class="attStatus '.$cls.'" data-id="'.$id.'" title="'.$title.'">'.$comment.'</td>';

            $return .= '<tr class="'.($class).'">
                            <td class="number">'.(!empty($info['info']->number) ? $info['info']->number : '').'</td>
                            <td class="player">
                                <a class="img" title="Zobrazit uživatele '.$player->getName().'" href="'.$player->getProfileLink().'"><img alt="'.$player->getName().'" src="'.$player->getUserImage('small').'"></a>
                                <a class="name" title="Zobrazit uživatele '.$player->getName().'" href="'.$player->getProfileLink().'"><span class="firstName">'.$player->fname.'</span><br><span class="lastName">'.$player->sname.'</span></a>
                            </td>' . $td . '
                        </tr>';
        }
    }

    $return .= '
            </tbody>
        </table>';

    if(empty($invitedCount))
    {
        return '';
    }

    return $return;
}


function printPositionAttend($position, $players, $attend, $events, $stopLeft, $stopRight, $team, $type = 'future')
{
    global $tr, $_days_of_week, $logged_user;

    // FIXME - překlad
    $return = '<h2 class="headline">'.$tr->tr($position->name2).'</h2>' . "\n";
    if(count($players) == 0 || !is_array($players))
    {
        $return .= '<div class="msg noClose">V této kategorii nejsou žádní hráči.</div>' . "\n";
    }
    else
    {
        $return .=  "\n" . '<table class="table attendance" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th class="number">#</th>
                        <th class="player">'.$tr->tr('jméno').'<span class="hidden-xs"> '.$tr->tr('a příjmení').'</span></th>';
        // FIXME předchozí
        if(!$stopLeft)
        {
            $return .= '    <th class="prev"><a href="#" class="prevAttendances" title="Předchozí události">&lt;</a></th>';
        }
        else
        {
            $return .= '    <th class="prev"></th>';
        }


        foreach($events as $event)
        {
            if(!is_object($event) && $event == 'nic')
            {
                $return .= '<th class="attendanceDate">&nbsp;</th>';
                continue;
            }

            // info o události
            $einfo = unserialize($event->info);

            switch($einfo[0]['type'])
            {
                case Event::COMPETITION:
                    $type = 'Soutěžní zápas';
                break;

                case Event::ACTION:
                    $type = 'Týmová akce';
                break;

                case Event::FRIENDLY:
                    $type = 'Přátelský zápas';
                break;

                case Event::TRAINING:
                    $type = 'Trénink';
                break;
            }
            $day = date('l', strtotime($event->start));
            $id = $event->team_id . '-' . $event->id;


            $oponent_name = '';
            if($einfo[0]['type'] == Event::COMPETITION || $einfo[0]['type'] == Event::FRIENDLY)
            {
                // musime zjistit jestli jsme domaci nebo hoste
                if($event->team_id != $team->id && $event->oponent_id == $team->id && !empty($event->oponent_id))
                {
                    $event_team = TeamDAO::get($event->team_id);
                    $oponent_name = $event_team->name;
                }
                elseif($event->oponent_id != $team->id && $event->team_id == $team->id && !empty($event->oponent_id))
                {
                    $event_team = TeamDAO::get($event->oponent_id);
                    $oponent_name = $event_team->name;
                }
                // nemáme tým, tak je to trénink nebo týmová akce
                elseif(empty($event->oponent_id))
                {
                    $oponent_name = $einfo[0]['team_name'];
                }

                if(!empty($oponent_name))
                {
                    $oponent_name = $oponent_name . '<br />';
                }
            }

            $return .= '<th class="attendanceDate" data-id="'.$id.'"><span class="help"><span class="visible-lg">'.$_days_of_week[$day].', </span>'.date('d.m.', strtotime($event->start)).'</span><span class="tooltip">'.$type.'<br />'.$oponent_name.strip_tags($einfo[0]['place']).'<br />'.date('H:i', strtotime($event->start)). ' - ' . date('H:i', strtotime($event->end)) . '</span></th>';
        }

        if(!$stopRight)
        {
            $return .= '    <th class="next"><a href="#" class="nextAttendances" title="Další události">&gt;</a></th>';
        }
        else
        {
            $return .= '    <th class="next"></th>';
        }
        $return .= '    <th class="contact hidden-xxs">kontakt</th>
                    </tr>
                </thead>
                <tbody>';
                $i = 0;
                foreach($players as $info)
                {
                    $player = $info['user'];
                    $player->getSettings();

                    $class = '';
                    if($i++ == 0)
                    {
                        $class = 'first';
                    }

                    $playerType = '';
                    if($info['info']->isHost())
                    {
                        $class .= ' host';
                        $playerType = '<span class="playerType" title="Hostující hráč">H</span>';
                    }

                    $return .= '<tr class="'.$class.'">
                        <td class="number">'.(!empty($info['info']->number) ? $info['info']->number : '').'</td>
                        <td class="player">
                         	<a href="'.$player->getProfileLink().'" title="Zobrazit uživatele '.$player->getName().'" class="img"><img src="'.$player->getUserImage('small').'" alt="'.$player->getName().'" />'.$playerType.'</a>
                            <a href="'.$player->getProfileLink().'" title="Zobrazit uživatele '.$player->getName().'" class="name"><span class="firstName">'.$player->fname.'</span><br /><span class="lastName">'.$player->sname.'</span></a>
                        </td>
                        <td class="prev" >&nbsp;</td>';

                    foreach($events as $event)
                    {
                        if(!is_object($event) && $event == 'nic')
                        {
                            $return .= '<td>&nbsp;</td>';
                            continue;
                        }

                        // info o události
                        $einfo = unserialize($event->info);

                        $key = $event->team_id . '-' . $event->id;

                        $cls = '';
                        $comment = '';

                        if($event->team_id == $logged_user->getActiveTeam()->id)
                        {
                            $attend_key = 'attend';
                            $attend_team_key = 'attend_team';
                        }
                        else
                        {
                            $attend_key = 'attend_away';
                            $attend_team_key = 'attend_team_away';
                        }

                        $title = '';

                        // nejdriv se mrknem zda je uzivatel vubec pozvanej
                        if(@!in_array($player->id, @array_keys($einfo[0][$attend_key])) && @empty($einfo[0][$attend_team_key]))
                        {
                            $cls = 'notEditable';

                            $title = '';
                        }
                        else
                        {
                            // už se k ní vyjádřil
                            if(isset($attend[$key][$player->id]))
                            {
                                $cls = 'edited ' . $attend[$key][$player->id]['attend'];

                                switch($attend[$key][$player->id]['attend'])
                                {
                                    case 'yes':
                                        $title = 'Zúčastním se';
                                        break;

                                    case 'no':
                                        $title = 'Nezúčastním se';
                                        break;

                                    case 'na':
                                        $title = 'Zatím nevím';
                                        break;

                                    case 'blackDot':
                                        $title = 'Neomluvená neúčast';
                                        break;
                                }

                                if(!empty($attend[$key][$player->id]['note']))
                                {
                                    $cls .= 'Comment';
                                    $comment = '<span class="help"><!-- --></span><span class="tooltip">'.$attend[$key][$player->id]['note'].'</span>';
                                }
                            }
                            else
                            {
                                $cls = 'noInfo';
                                $title = 'Nevyplněná docházka';
                            }

                            if($team->isAdmin($logged_user))
                            {
                                $cls .= ' editable';
                            }
                            elseif($player->id == $logged_user->id && date('Y-m-d') <= date('Y-m-d', strtotime($event->start)))
                            {
                                $cls .= ' editable';
                            }
                            else
                            {
                                $cls .= '';
                            }
                        }

                        $id = $event->team_id . '-' . $event->id . '-' . $player->id . '@' . getRecipientHash($player->id);

                        $return .= '<td class="attStatus '.$cls.'" data-id="'.$id.'" title="'.$title.'">'.$comment.'</td>';
                        //$return .= '<td class="attStatus yes"><span class="tooltip">Mám v háji kotník, snad příště bude ok!</span>&nbsp;</td>';
                    }
                        /*<td class="attStatus yes">&nbsp;</td>
                        <td class="attStatus yes">&nbsp;</td>
                        <td class="attStatus na">&nbsp;</td>
                        <td class="attStatus no">&nbsp;</td>*/

                    $phone 	 = '';
                    $message = '';
                    // musi mit vyplneny telefon
                    // taky musi mit povoleny telefon pro vsechny, popr jen pro spoluhrace a ten kdo se diva musi byt spoluhrac
                    if(!empty($player->phone) && (@$player->settings[UserSettings::KEY_PHONE] == UserSettings::VAL_SHOW_ALL || (@$player->settings[UserSettings::KEY_PHONE] == UserSettings::VAL_SHOW_TEAMMATES && $player->isTeammate($logged_user->id))))
                    {
                        $phone = '<span class="phone help"><!-- --></span><span class="tooltip">'.$player->phone.'</span>';
                    }

                    if((@$player->settings[UserSettings::KEY_MESSAGES] == UserSettings::VAL_MESSAGE_YES || (@$player->settings[UserSettings::KEY_MESSAGES] == UserSettings::VAL_MESSAGE_TEAM && $player->isTeammate($logged_user->id))) && $player->id != $logged_user->id)
                    {
                    	$message = '<a class="message sendMessage popUpMedium" href="#sendNewMessage" data-id="'. $player->id . '-' . getRecipientHash($player->id) . '" title="Poslat uživateli '.$player->getName().' zprávu"><!-- --></a>';
                    }


                    $return .= '<td class="next">&nbsp;</td>
                        <td class="contact hidden-xxs">'.$message.$phone.'</td>';
                    $return .= '</tr>';
                }

                $return .= '</tbody>
            </table>';
    }

    return $return;
}


function printStaffRoster($position, $players, $showAll = true)
{
    global $tr, $logged_user;

    if(!$showAll)
    {
        $show = array();

        if(is_array($players) && count($players) > 0)
        {
            foreach($players as $key => $player)
            {
                if(!empty($player['info']->show))
                {
                    $show[$key] = $player;
                }
            }
        }

        $players = $show;
    }

    $return = '';

    if(is_array($players) && count($players) > 0)
    {
        $return = '<h2 class="headline">'.$tr->tr($position->name2).'</h2>' . "\n";
        $return .= '<table cellspacing="0" cellpadding="0" class="table roster">
        <thead>
            <tr>
                <th style="width: 5%;">&nbsp;</th>
                <th class="player" style="width: 30%;">jméno a příjmení</th>
                <th style="width: 13%;">&nbsp;</th>
                <th style="width: 13%;">věk</th>
                <th class="function" style="width: 39%;">funkce</th>
            </tr>
        </thead>
        <tbody>';
        $i = 0;
        foreach($players as $info)
        {
            $player = $info['user'];

            $class = '';
            if($i++ == 0)
            {
                $class = 'first';
            }

            $playerType = '';
            if($info['info']->isHost())
            {
                $class .= ' host';
                $playerType = '<span class="playerType" title="Hostující hráč">H</span>';
            }

            $return .= '<tr class="'.$class.'">
                <td class="number">&nbsp;</td>
                <td class="player">
                    <a href="'.$player->getProfileLink().'" title="Zobrazit uživatele '.$player->getName().'" class="img"><img src="'.$player->getUserImage('small').'" alt="'.$player->getName().'" />'.$playerType.'</a>
                    <a href="'.$player->getProfileLink().'" title="Zobrazit uživatele '.$player->getName().'" class="name"><span class="firstName">'.$player->fname.'</span><br /><span class="lastName">'.$player->sname.'</span></a>
                </td>
                <td>&nbsp;</td>
                <td>'.($player->birth != '0000-00-00' ? $player->getAge().' '.decline($player->getAge(), 'rok', 'roky', 'let') : '&ndash;').'</td>
                <td class="function">'.$info['info']->function.'</td>
            </tr>';

        }
            $return .= '</tbody>
        </table>';
    }
    return $return;
}

function printEditRoster($positions, $roster, $position_select, Team $team)
{
    global $tr, $logged_user;

    $return = '
            <h3 class="title">Upravit soupisku týmu</h3>

            <form id="popUpFormEditRoster" class="popUpForm" action="" method="post">
                <input type="hidden" value="'.$team->id.'" name="team_id" />
                <div id="inclusions">';

                $j = 0;
                foreach($positions as $position)
                {
                    $class = '';
                    if($j++ == 0)
                    {
                        $class = ' first';
                    }
                        $return .= '<fieldset class="group'.$class.'">
                            <legend>'.$position->name2.'</legend>';

                        $i = 0;
                        if(isset($roster[$position->id]) && count($roster[$position->id]))
                        {
                            foreach($roster[$position->id] as $info)
                            {
                                $player = $info['user'];
                                if(!$player instanceof User)
                                {
                                    continue;
                                }
                                $class = '';
                                if($i++ == 0)
                                {
                                    $class = 'first ';
                                }

                                $id = $player->id . '-' . getRecipientHash($player->id);
                                $return .= '<p class="entries '.$class.'cleaned">
                                    <input class="text number" type="text" name="player['.$id.'][number]" placeholder="#" maxlength="2" value="'.(!empty($info['info']->number) ? $info['info']->number : '').'" />
                                    <label class="name">'.$player->getName().'</label>
                                    <select class="position joined" name="player['.$id.'][position_id]">
                                        <option value="0">Nezařazený</option>
                                        '.str_replace('value="'.$position->id.'"', 'value="'.$position->id.'" selected="selected"', $position_select).'
                                    </select>
                                    <input class="text function" type="text" name="player['.$id.'][function]" value="'.$info['info']->function.'" placeholder="Název funkce" maxlength="10" />
                                    <span class="hostControls">
                                        <label class="host'.($info['info']->host == 1 ? ' active' : '').($player->isFakeUser() ? '' : '').'" for="host'.$player->id.'" title="Hostující hráč">H</label><input class="hostInput forBlind'.($player->isFakeUser() ? '' : '').'"'.($info['info']->host == 1 ? ' checked="checked"' : '').' type="checkbox" value="1" name="player['.$id.'][host]" id="host'.$player->id.'" />
                                        <label class="hostShow'.($info['info']->show == 1 ? ' active' : '').'" for="show'.$player->id.'" title="Zobrazit na soupisce">Z</label><input class="hostShowInput forBlind"'.($info['info']->show == 1 ? ' checked="checked"' : '').' type="checkbox" value="1" name="player['.$id.'][show]" id="show'.$player->id.'" />
                                    </span>';
                                if($logged_user->id != $player->id)
                                {
                                    $return .= '<span class="delete" title="Ostranit hráče z týmu"><!-- --></span>';
                                }
                                $return .= '<input class="playerStatus" name="player['.$id.'][deleted]" type="hidden" value="" />
                                <input class="playerStatusAdded" name="player['.$id.'][added]" type="hidden" value="" />
                                <input class="playerStatusRemoved" name="player['.$id.'][removed]" type="hidden" value="" />
                                </p>';
                            }

                        }
                        else
                        {
                            $return .= '<div class="msg noClose">V této kategorii nejsou žádní hráči.</div>' . "\n";
                        }
                        $return .= '<span class="errorMsg">Vyplňte, prosím, povinné položky.</span>
                    </fieldset>';
                }
                $return .= '</div><!-- #inclusions -->';

                // nezarazeni hraci
                if(!empty($roster[0]))
                {
                    $return .= '<div id="notInclusions">
                        <fieldset class="group first">
                            <legend>Nezařazení</legend>';

                        $i = 0;

                        foreach($roster[0] as $info)
                        {
                            $player = $info['user'];
                            $class = '';
                            if($i++ == 0)
                            {
                                $class = 'first ';
                            }

                            $id = $player->id . '-' . getRecipientHash($player->id);
                            $return .= '<p class="entries '.$class.'cleaned">
                                <input class="text number" type="text" name="player['.$id.'][number]" placeholder="#" maxlength="2" value="'.(!empty($info['info']->number) ? $info['info']->number : '').'" />
                                <label class="name">'.$player->getName().'</label>
                                <select class="position notJoined" name="player['.$id.'][position_id]">
                                    <option value="0">Nezařazený</option>
                                    '.str_replace('value="0"', 'value="0" selected="selected"', $position_select).'
                                </select>
                                <input class="text function" type="text" name="player['.$id.'][function]" value="'.$info['info']->function.'" placeholder="Název funkce" maxlength="10" />
                                <span class="hostControls">
                                    <label class="host'.($info['info']->host == 1 ? ' active' : '').'" for="host'.$player->id.'" title="Hostující hráč">H</label><input class="hostInput forBlind"'.($info['info']->host == 1 ? ' checked="checked"' : '').' type="checkbox" value="1" name="player['.$id.'][host]" id="host'.$player->id.'" />
                                    <label class="hostShow'.($info['info']->show == 1 ? ' active' : '').'" for="show'.$player->id.'" title="Zobrazit na soupisce">Z</label><input class="hostShowInput forBlind"'.($info['info']->show == 1 ? ' checked="checked"' : '').' type="checkbox" value="1" name="player['.$id.'][show]" id="show'.$player->id.'" />
                                </span>
                                <span class="delete" title="Ostranit hráče z týmu"><!-- --></span>
                                <input class="playerStatus" name="player['.$id.'][deleted]" type="hidden" value="" />
                                <input class="playerStatusAdded" name="player['.$id.'][added]" type="hidden" value="" />
                                <input class="playerStatusRemoved" name="player['.$id.'][removed]" type="hidden" value="" />
                            </p>';
                        }

                        $return .= '<span class="errorMsg">Vyplňte, prosím, povinné položky.</span>
                    </fieldset>
                </div><!-- #notInclusions -->';
                }
                $return .= '<fieldset>
                    <p class="entries submit">
                        <button type="submit">uložit a zavřít</button>
                    </p>
                </fieldset>
            </form>';

    return $return;
}

function getTeamImageHash($team_id, $type, $dir)
{
    return hash('sha512', $team_id . '-' . IMAGE_TEAM_HASH_SALT . '_' . $type . '---' . $dir);
}

function getRecipientHash($user_id)
{
    return hash('sha256', $user_id . SALT_RECIPIENT);
}

function getTeamHash($team_id)
{
    return hash('sha256', $team_id . SALT_TEAM);
}

function getInvitationHash($team_id, $logged_user_id, $invited_user_id)
{
    return hash('sha256', $team_id . $logged_user_id . $invited_user_id  . SALT_INVITATION);
}

function getRequestHash($team_id, $logged_user_id)
{
    return hash('sha256', $team_id . $logged_user_id . SALT_INVITATION);
}

function getGalleryHash($team_id, $gallery_id)
{
    return hash('sha256', $team_id . '->' . $gallery_id . '<->' . SALT_GALLERY);
}

/**
 * Funkce, která uloží změněné hodnoty přihlášeného uživatele do sessiony a do cookie
 **/
function updateLoggedUser($_project, User $logged_user)
{
    $_project['session']->set('logged', $logged_user);
    // FIXME
    //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
}


function printMessage($class, $pm, $user)
{
    global $_project, $logged_user;

    $timeago = ' class="timeago"';
    if(getDateDiff(date('Y-m-d',strtotime($pm->time)), date('Y-m-d')) > CHANGE_DATE_LIMIT)
    {
        $timeago = '';
    }

    if(!empty($user->id))
    {
        $text = nl2br(strip_tags($pm->text));
    }
    else
    {
        preg_match('/\[user:(\d+)\]/', $pm->text, $matches);

        if(empty($matches))
        {
            return '';
        }

        $left_user = UserDAO::get($matches[1]);

        if(!$left_user instanceof User)
        {
            return '';
        }

        $text = str_replace($matches[0], $left_user->getAnchor(), nl2br(strip_tags($pm->text)));
    }

    return '<div class="containerMessage'.$class.'" data-id="'.$pm->id.'">
        <div class="date">'.(!empty($user->id) ? $user->getName().',' : '') . ' <span'.$timeago.' title="'.date('Y-m-d\TH:i:sP', strtotime($pm->time)).'">'.date('j.n. G:i', strtotime($pm->time)).'</span></div>
        <div class="text"><p>'.$text.'</p></div>
    </div>';
}


function printMessageBox($detail, $userArray)
{
    global $logged_user, $_project;

    $return = '<div class="message-container cleaned">
                    <div class="message cleaned">
                        <div class="containerName cleaned">';
    // projdeme to už tady a vypíšeme později, abychom mohli zjistit datum poslední zprávy
    $i = 0;
    $detail = array_reverse($detail);
    $messagesHTML = '';
    foreach($detail as $message)
    {
        $class = ' forBlind';
        if(++$i == count($detail))
        {
            $class = ' last';
        }

        // pomocne pole pro uzivatele, protoze nesedi uplne presne nazvy sloupcu
        $userData = $message;
        $userData['user_id'] = $userData['sent_user_id'];
        unset($userData['sent_user_id']);

        $user = new User($userData);
        $pm = new PrivateMessage($message);

        if(empty($user->id))
        {
            $class .= ' info';
        }
        elseif($user->id == $logged_user->id)
        {
            $class .= ' sent';
        }
        else
        {
            $class .= ' received';
        }

        $messagesHTML .= printMessage($class, $pm, $user);
    }

    $return .= '<h3 class="name">';
    $anchors = array();
    $txt = '';
    foreach($userArray as $mainUser)
    {
         $txt = $mainUser['user']->getAnchor();
         if($mainUser['team'] !== false)
         {
            $txt .= '<a class="team" title="Zobrazit tým '.$mainUser['team']->getName().'" href="'.$mainUser['team']->getProfileLink().'">['.$mainUser['team']->getName().']</a>';
         }
         $anchors[] = $txt;
    }

    $return .= implode(', ', $anchors);


    $timeago = ' timeago';
    if(getDateDiff(date('Y-m-d',strtotime($pm->time)), date('Y-m-d')) > CHANGE_DATE_LIMIT)
    {
        $timeago = '';
    }

    $return .= '</h3>
        <!--<span class="dateLast'.$timeago.'" title="'.date('Y-m-d\TH:i:sP', strtotime($pm->time)).'">'.date('j.n. G:i', strtotime($pm->time)).'</span>-->'.
        ($pm->read == 0 ? '<span class="unread">nová zpráva</span>' : '')
    .'</div><!-- .containerName -->

    <div class="containerMessages" id="conversation-'.$pm->conversation_id.'">
        <div class="inner cleaned">
            '.$messagesHTML.'
        </div>
    </div><!-- .containerMessages -->

    <div class="controlPanel forBlind">
        <form class="cleaned myComment myMessage" action="" method="get" id="conversation-'.uniqid().'">
            <fieldset>
                <input type="hidden" value="'.$pm->conversation_id.'" name="pm[conversation_id]" />
                <textarea rows="2" cols="46" name="pm[text]" class="resizable noActive">Začněte psát zprávu...</textarea>
                <button type="submit" class="small button buttonA">odeslat</button>
            </fieldset>
        </form>
    </div><!-- .controlPanel -->
</div>

        <p class="picture">';

            if(count($userArray) > 1)
            {
                $mainUser = new User();

                $return .= '<img width="46" height="46" alt="Více příjemců" src="'.$mainUser->getUserImage('medium').'" />';
            }
            else
            {
                $return .= '<a title="Zobrazit uživatele '.$mainUser['user']->getName().'" href="'.$mainUser['user']->getProfileLink().'">
                    <img width="46" height="46" alt="'.$mainUser['user']->getName().'" src="'.$mainUser['user']->getUserImage('medium').'" />
                </a>';
            }
        $return .= '</p><!-- .picture -->
            <p class="controls cleaned">
                <a href="#" class="delete" title="Smazat konverzaci"><span class="forBlind">Smazat konverzaci</span></a>';
        if(count($userArray) > 1)
        {
                $return .= '<a href="#" class="leave" title="Opustit a smazat konverzaci"><span class="forBlind">Opustit a smazat konverzaci</span></a>';
        }
        $return .= '</p>
    </div><!-- .message-container -->';

    return $return;
}


function printEventRow($data = array())
{
	global $logged_user;

    $result = unserialize(@$data['event']->result);

    switch($data['type'])
    {
        case Event::COMPETITION:
            $title = 'Soutěžní zápas';
        break;

        case Event::FRIENDLY:
            $title = 'Přátelský zápas';
        break;

        case Event::TRAINING:
            $title = 'Trénink';
        break;

        case Event::ACTION:
            $title = 'Týmová akce';
        break;
    }

    //<span class="time">'.$data['startTime'].'</span><br />
    $return = '<tr class="item'.(!$data['event']->isConfirmed() || (!$data['event']->isResultConfirmed() && $data['event']->hasResult()) ? ' unofficial' : '').$data['class'].'" data-id="'.$data['data-id'] . '">
                    <td class="flag '.$data['type'].'" title="'.$title.'">';

    $not = array(
        Event::TRAINING,
        Event::ACTION
    );

    if($data['event']->userCanEdit($logged_user))
    {
        $return .= '<div class="controlsBox">
                            <div class="controls left">';

        if(!$data['event']->isConfirmed() && !$data['event']->isConfirmed($data['active_team']) && !in_array($data['type'], $not))
        {
            $return .= '<a title="Odmítnout událost" class="deleteBig declineEvent" href="#"><!-- --></a>';
        }
        else
        {
            $return .= '<a href="#" class="deleteBig" title="Smazat událost"><!-- --></a>';
        }

        $return .= '</div>
                </div>';
    }

    $link = $data['active_team']->getProfileLink('event-detail') .$data['data-id'].'/';
    $return .=     '</td>
                    <td class="team hidden-xxs">'.$data['teamTd'].'</td>
                    <td class="team left">'.$data['nameTd'].'</td>';

    /*if($data['type'] == Event::COMPETITION || $data['type'] == Event::FRIENDLY)
    {*/
        $return .= '<td class="left title hidden-xxs"><a href="'.$link.'" class="name" title="'.$data['title'].'">'.$data['title'].'</a></td>';
    /*}
    else
    {
        $return .= '<td class="left title hidden-xxs"><span class="name" title="'.$data['title'].'">'.$data['title'].'</span></td>';
    }*/
                    $return .= '
                    <td class="left date"><span class="help">'.$data['start'].'</span><span class="tooltip">'.($data['timeTooltip']).'</span></td>
                    <td class="result">
                        <div class="controlsBox">';

                            if($data['event']->userCanEdit($logged_user))
                            {
                                $return .= '<div class="controls%controls_class%">';
                                $cnt = 0;

                                // nepotvrzena udalost
                                if(!$data['event']->isConfirmed() || empty($data['oponent_id']))
                                {
                                    $cnt++;
                                    $return .= '<a href="'.PATH_WEB_ROOT.'event-manage/'.$data['data-id'].'/" class="editBig" title="Editovat událost"><!-- --></a>';
                                }

                                if(!$data['event']->isConfirmed() && !$data['event']->isConfirmed($data['active_team']))
                                {
                                    $link = '#';
                                    $class = '';
                                    $season_text = '';
                                    $season = $data['active_team']->getActiveSeason();

                                    // musime zkontrolovat, zda mame sezonu, jinak mu nepvolime potvrdit udalost
                                    if(empty($season))
                                    {
                                        $link = PATH_WEB_ROOT.'event-manage/'.$data['data-id'].'/';
                                        $class = ' noAjax';
                                    }
                                    else
                                    {
                                        // jsem domaci
                                        if($data['event']->team_id == $logged_user->getActiveTeam()->id)
                                        {
                                            // pokud je zadana Sezona, tak ji vypiseme
                                            if(!empty($data['event']->season_id))
                                            {
                                                $season = Seasons::get($data['event']->team_id, $data['event']->season_id);
                                            }

                                            // pokud nemame vubec vyplnenou dochazku, tak pozveme cely tym
                                            if(empty($einfo[0]['attend_team']) && empty($einfo[0]['attend']))
                                            {

                                            }
                                        }
                                        // jsem hoste
                                        elseif($data['event']->oponent_id == $logged_user->getActiveTeam()->id)
                                        {
                                            // pokud je zadana Sezona, tak ji vypiseme
                                            if(!empty($data['event']->oponent_season_id))
                                            {
                                                $season = Seasons::get($data['event']->oponent_id, $data['event']->oponent_season_id);
                                            }

                                            // pokud nemame vubec vyplnenou dochazku, tak pozveme cely tym
                                            if(empty($einfo[0]['attend_team_away']) && empty($einfo[0]['attend_away']))
                                            {

                                            }
                                        }
                                        $season_text = ' data-season="'.$season->name.'"';
                                    }

                                    // jeste zkontrolujeme koho budeme zvat
                                    $einfo = $data['event']->info;
                                    if(!is_array($einfo))
                                    {
                                        $einfo = unserialize($einfo);
                                    }
                                    $team_text = '';
                                    // jsem domaci
                                    if($data['event']->team_id == $logged_user->getActiveTeam()->id)
                                    {
                                        // pokud nemame vubec vyplnenou dochazku, tak pozveme cely tym
                                        if(empty($einfo[0]['attend_team']) && empty($einfo[0]['attend']))
                                        {
                                            $team_text = ' data-team="true"';
                                        }
                                        else
                                        {
                                            $team_text = ' data-team="false"';
                                        }
                                    }
                                    // jsem hoste
                                    elseif($data['event']->oponent_id == $logged_user->getActiveTeam()->id)
                                    {
                                        // pokud nemame vubec vyplnenou dochazku, tak pozveme cely tym
                                        if(empty($einfo[0]['attend_team_away']) && empty($einfo[0]['attend_away']))
                                        {
                                            $team_text = ' data-team="true"';
                                        }
                                        else
                                        {
                                            $team_text = ' data-team="false"';
                                        }
                                    }
                                    $cnt++;
                                    $return .= '<a title="Přijmout událost" class="checkBig'.$class.'"'.$season_text.$team_text.' href="'.$link.'"><!-- --></a>';
                                }

                                // udalost je potvzrnea z obou stran
                                if($data['event']->isConfirmed())
                                {
                                    if(!in_array($data['event']->type, $not))
                                    {
                                        // vysledek neni potvrzeny z obou stran, tak ho jeste muzeme editovat
                                        if(!$data['event']->isResultConfirmed() || empty($data['oponent_id']) && !in_array($data['type'], $not))
                                        {
                                            $cnt++;
                                            $return .= '<a href="'.PATH_WEB_ROOT.'event-edit-result/'.$data['data-id'].'/" class="editResult" title="Editovat výsledek a statistiky zápasu"><!-- --></a>';
                                        }

                                        // neni potvrzenej vysledek od nas, tak ho musime prijmout
                                        if($data['event']->hasResult() && !$data['event']->isResultConfirmed() && !$data['event']->isResultConfirmed($data['active_team']))
                                        {
                                            $cnt++;
                                            $return .= '<a title="Přijmout výsledek a statistiky zápasu" class="checkBig result" href="#"><!-- --></a>';
                                        }
                                    }
                                }

                                $cls = '';
                                if($cnt > 1)
                                {
                                    $cls = ' two cleaned';
                                }
                                $return = str_replace('%controls_class%', $cls, $return);

                                $return .= '</div>';
                            }

                            if(!in_array($data['type'], $not))
                            {
                                $return .= (!empty($result) ?
                                '<a href="'.$link.'" title="Zobrazit detail zápasu">'.$result['home']['final'].' : '.$result['away']['final'].$data['event']->getExtraTime().'</a>' :
                                '<a href="'.$link.'" title="Zobrazit detail zápasu">- : -</a>');
                            }
                            else
                            {
                                $return .= '<span>&nbsp;</span>';
                            }
                            //'<span>- : -</span>').
                        $return .= '</div>
                    </td>
                </tr>';

    return $return;
}


/**
 * vytiskne jednotlive radky s udalostmi
 *
 * @param  array   $events       [description]
 * @param  Team    $team         [description]
 * @param  User    $logged_user  [description]
 * @param  boolean $show_buttons [description]
 * @return [type]                [description]
 */
function printEvents($events = array(), Team $team, User $logged_user, $show_buttons = false, $add_first_class = true)
{
    $return = '';

    $future = 1;
    $past = 1;

    if(isset($events['events']))
    {
        $future = @$events['future'];
        $past = @$events['past'];
        $events = $events['events'];
    }
    else
    {
        return '';
    }

    if(!is_array($events) || count($events) == 0)
    {
        return '<tr class="infoText"><td colspan="6">Žádné události k zobrazení.</td></tr>';
    }

    reset($events);
    $first = current($events);
    end($events);
    $last = current($events);

    if($show_buttons && $future)
    {
        $return = '<tr class="more future">
                       <td colspan="6"><a href="#" id="future" class="button buttonC icon add">více budoucích událostí</a></td>
                   </tr>';
    }

    $i = 0;
    foreach($events as $event)
    {
        $event->info = unserialize($event->info);

        // musime zjistit jestli jsme domaci nebo hoste
        if($event->team_id != $team->id && $event->oponent_id == $team->id && !empty($event->oponent_id))
        {
            $event_team = TeamDAO::get($event->team_id);
        }
        elseif($event->oponent_id != $team->id && $event->team_id == $team->id && !empty($event->oponent_id))
        {
            $event_team = TeamDAO::get($event->oponent_id);
        }
        // nemáme tým, tak je to trénink nebo týmová akce
        elseif(empty($event->oponent_id))
        {
            $event_team = false;
        }
        else
        {
            continue;
        }

        $class = '';
        if(!$i++ && $add_first_class)
        {
            $class = ' first';
        }

        $span = '';
        $title = '';

        global $tr;

	    // pokud není událost potvrzená týmem, který se na ní zrovna diva
	    if(!$event->isConfirmed($team) && $team->isPlayer($logged_user))
	    {
    		$title = $tr->tr('Událost zatím nebyla vaším týmem potvrzena.');

	        $span = '<span title="'.$title.'" class="eventStatus exclamation-mark">!</span>';
	    }
	    //
	    elseif(!$event->isConfirmed())
	    {
	    	if(!$event->isConfirmed($team))
	    	{
		    	$title = $tr->tr('Událost zatím nebyla potvrzena týmem ') . $team->getName();
		    }
		    else
		    {
		    	$title = $tr->tr('Událost zatím nebyla potvrzena týmem ') . $event_team->getName();
		    }

	    	$span = '<span title="'.$title.'" class="eventStatus question-mark">?</span>';
	    }
        // neni potvrzen výsledek
        elseif(!empty($event->result) && !$event->isResultConfirmed($team) && $team->isPlayer($logged_user) && !in_array($event->type, array('training', 'action')))
        {
            $title = $tr->tr('Výsledek události zatím nebyl vaším týmem potvrzen.');

            $span = '<span title="'.$title.'" class="eventStatus exclamation-mark">!</span>';
        }
        elseif(!empty($event->result) && !$event->isResultConfirmed() && !in_array($event->type, array('training', 'action')))
        {
            if(!$event->isResultConfirmed($team))
            {
                $title = $tr->tr('Výsledek události zatím nebyl potvrzen týmem ') . $team->getName();
            }
            else
            {
                $title = $tr->tr('Výsledek události zatím nebyl potvrzen týmem ') . $event_team->getName();
            }

            $span = '<span title="'.$title.'" class="eventStatus question-mark">?</span>';
        }

        // soutěžní nebo přátelský zápas
        if($event->type == Event::COMPETITION || $event->type == Event::FRIENDLY)
        {
            // máme vybraný tým který je na MTW
            if($event_team !== false)
            {
                $print = array(
                    'event'         => $event,
                    'oponent_id'    => $event->oponent_id,
                    'active_team'   => $team,
                    'class'         => $class,
                    'data-id'       => $event->id . '-' . $event->team_id,
                    'type'          => $event->type,
                    'admin'         => ($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id),
                    'profileLink'   => $event_team->getProfileLink(),
                    'teamLogo'      => $event_team->getTeamLogo('small'),
                    'teamName'      => $event_team->getName(),
                    'teamPrefix'    => (!empty($event_team->prefix) ? '<span class="firstName">'.$event_team->prefix.'</span><br />' : ''),
                    'teamNameShort' => $event_team->name,
                    'title'         => $event->info[0]['name'],
                    'startTime'     => '', //date('H:i', strtotime($event->start)) . ' &ndash; ' . date('H:i', strtotime($event->end)),
                    'timeTooltip'   => 'od '.date('H:i, d. m. Y', strtotime($event->start)).' <br /> do ' . date('H:i, d. m. Y', strtotime($event->end)) . '<br />' . $event->info[0]['place'],
                    'start'         => date('d. m. Y', strtotime($event->start)),
                    'eventId'       => $event->id,
                    'teamTd'        => '<a href="'.$event_team->getProfileLink().'" class="img"><img src="'.$event_team->getTeamLogo('small').'" alt="'.$event_team->getName().'" title="'.$event_team->getName().'" />'.$span.'</a>',
                    'nameTd'        => '<a href="'.$event_team->getProfileLink().'" title="Detail týmu '.$event_team->getName().'" class="name">'.(!empty($event_team->prefix) ? '<span class="firstName">'.$event_team->prefix.'</span><br />' : '').'<span class="lastName">'.$event_team->name.'</span></a>'
                );

                $return .= printEventRow($print);
            }
            // tym na MTW neexistuje
            else
            {
                $print = array(
                    'event'         => $event,
                    'oponent_id'    => $event->oponent_id,
                    'active_team'   => $team,
                    'class'         => $class,
                    'data-id'       => $event->id . '-' . $event->team_id,
                    'type'          => $event->type,
                    'admin'         => ($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id),
                    'title'         => $event->info[0]['name'],
                    'startTime'     => '', //date('H:i', strtotime($event->start)) . ' &ndash; ' . date('H:i', strtotime($event->end)),
                    'timeTooltip'   => 'od '.date('H:i, d. m. Y', strtotime($event->start)).' <br /> do ' . date('H:i, d. m. Y', strtotime($event->end)) . '<br />' . $event->info[0]['place'],
                    'start'         => date('d. m. Y', strtotime($event->start)),
                    'eventId'       => $event->id,
                    'teamTd'        => '<span class="img"><img src="'.$team->getTeamLogo('small', '/event'.$event->id.'/').'" alt="'.$event->info[0]['team_name'].'" title="'.$event->info[0]['team_name'].'" />'.$span.'</span>',
                    'nameTd'        => '<span class="name" title="'.$event->info[0]['team_name'].'"><span class="lastName">'.$event->info[0]['team_name'].'</span></span>'
                );

                $return .= printEventRow($print);
            }
        }
        // trénink nebo týmová akce
        else
        {
            $print = array(
                'event'         => $event,
                'oponent_id'    => $event->oponent_id,
                'active_team'   => $team,
                'class'         => $class,
                'data-id'       => $event->id . '-' . $event->team_id,
                'type'          => $event->type,
                'admin'         => ($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id),
                'profileLink'   => $team->getProfileLink(),
                'teamLogo'      => $team->getTeamLogo('small'),
                'teamName'      => $team->getName(),
                'teamPrefix'    => (!empty($team->prefix) ? '<span class="firstName">'.$team->prefix.'</span><br />' : ''),
                'teamNameShort' => $team->name,
                'title'         => $event->info[0]['name'],
                'startTime'     => '', //date('H:i', strtotime($event->start)) . ' &ndash; ' . date('H:i', strtotime($event->end)),
                'timeTooltip'   => 'od '.date('H:i, d. m. Y', strtotime($event->start)).' <br /> do ' . date('H:i, d. m. Y', strtotime($event->end)) . '<br />' . $event->info[0]['place'],
                'start'         => date('d. m. Y', strtotime($event->start)),
                'eventId'       => $event->id,
                'teamTd'        => '<a href="'.$team->getProfileLink().'" class="img"><img src="'.$team->getTeamLogo('small').'" alt="'.$team->getName().'" title="'.$team->getName().'" />'.$span.'</a>',
                'nameTd'        => '<a href="'.$team->getProfileLink().'" title="Detail týmu '.$team->getName().'" class="name">'.(!empty($team->prefix) ? '<span class="firstName">'.$team->prefix.'</span><br />' : '').'<span class="lastName">'.$team->name.'</span></a>'
            );

            $return .= printEventRow($print);
        }
    }

    if($show_buttons && $past)
    {
        $return .= '<tr class="more past">
                        <td colspan="6"><a href="#" id="past" class="button buttonC icon add">více starších události</a></td>
                    </tr>';
    }

    return $return;
}



/**
 * Funkce nam z retezce pro vyhledavani vytvori REGEXP, ktery nam osetri i hledani s preklepama atd
 **/
function search_string_prepare($search)
{
    $search = mb_strtolower(preg_replace('/[^\w\d-\s]/ui', ' ', $search), 'UTF-8');

    // preklepy - zdvojena pismena
    $search = preg_replace('/(.)(\\1)+/', '$1', $search);

	//pole s informacemi o náhradách znaků za kousky regulárních výrazů
	$reps["a"] = $reps["á"] = "(a|á)";
	$reps["c"] = $reps["č"] = $reps["ć"] = "(c|č|ć)";
	$reps["d"] = $reps["ď"] = "(d|ď)";
	$reps["e"] = $reps["é"] = $reps["ě"] = "(e|é|ě)";
	$reps["i"] = $reps["í"] = $reps["y"] = $reps["ý"] = "(i|í|y|ý)";
	$reps["l"] = $reps["ľ"] = $reps["ĺ"] = "(l|ľ|ĺ)";
	$reps["n"] = $reps["ň"] = $reps["ń"] = "(n|ň|ń)";
	$reps["o"] = $reps["ó"] = $reps["ô"] = "(o|ó|ô)";
	$reps["r"] = $reps["ř"] = $reps["ŕ"] = "(r|ř|ŕ)";
	$reps["s"] = $reps["š"] = $reps["ś"] = "(s|š|ś)";
	$reps["t"] = $reps["ť"] = "(t|ť)";
	$reps["u"] = $reps["ú"] = $reps["ů"] = "(u|ú|ů)";
	$reps["z"] = $reps["ž"] = $reps["ź"] = "(z|ž|ź)";

	//pokud je řetězec víceslovný, rozdělíme jej do pole po slovech, abychom mohli
	//kontrolovat výskyty jednotlivých slov
	$words = explode(' ', $search);

	//projdeme postupně všechny slova a uděláme v nich potřebné úpravy
	foreach($words as $key => $word)
	{
		//projdeme postupně po znacích celé slovo
		for($c1 = 0; $c1 <= mb_strlen($words[$key], "UTF-8"); $c1++)
		{
			//jestli v poli náhrad pro aktuální znak existuje náhrada
			if(isset($reps[mb_substr($words[$key], $c1, 1, "UTF-8")]))
			{
				//zjistíme potřebný posun needle ($c1) po dosazení, abychom přeskočili
				//celý dosazený text a nenahrazovali části z něj (pokud chcete vidět co
				//se stane když needle neposuneme, následující řádek vykomentujte a na
				//konci každého cyklu si dejde vypsat $words[$key])
				$shift = mb_strlen($reps[mb_substr($words[$key], $c1, 1, "UTF-8")], "UTF-8");
				//teď uděláme náhradu
				$words[$key] =
    				mb_substr($words[$key], 0, $c1, "UTF-8") .
    				$reps[mb_substr($words[$key], $c1, 1, "UTF-8")] .
    				mb_substr($words[$key], $c1 + 1, 90000, "UTF-8");

                if(empty($words[$key]))
                {
                    unset($words[$key]);
                }

				//a posuneme needle
				$c1 = $c1 + $shift - 1;
			}
		}
	}

	//vrátíme pole s upravenými slovy
	return $words;
}

function print_edit_seasons($team_seasons)
{
    global $tr;
    $return = '';

    if(is_array($team_seasons))
    {
        foreach($team_seasons as $season)
        {
            $return .= '<p class="entries cleaned" data-id="'.$season->id.'">
                <label class="seasonName">'.$season->name.'</label>
                <input class="text forBlind" type="text" name="" maxlength="25" />
                <span class="editDelete controls">
                    <a href="#" class="edit icon edit" title="'.$tr->tr('Editovat sezónu').'"></a>
                    <a href="#" class="delete icon delete" title="'.$tr->tr('Smazat sezónu').'"></a>
                </span>
                <span class="saveStorno controls forBlind">
                    <a href="#" class="save" title="'.$tr->tr('Uložit změnu').'"></a>
                    <a href="#" class="storno" title="'.$tr->tr('Zrušit změnu').'"></a>
                </span>
            </p>';
        }
    }

    $return .= '';

    return $return;
}


function print_season_select($team_seasons)
{
    global $tr;
    $return = '';

    if(is_array($team_seasons) && count($team_seasons) > 0)
    {
        foreach($team_seasons as $season)
        {
            $return .= '<option value="'.$season->id.'"'.($season->active ? ' selected="selected"' : '').'>'.$season->name.'</option>';
        }
    }
    else
    {
        // $return .= $tr->tr('Nemáte vytvořeny žádné sezóny.');
    }

    return $return;
}


/**
 *
 */
function print_last_activities(User $user, User $logged_user, $all = array(), $print_more = true)
{
    global $tr;
    $return = '';

    foreach($all as $date => $act)
    {
        $text = '';
        switch($act['type'])
        {
            case 'reply':
                $class = ' icon comment';

                $object = WallDAO::getObject($act['info']['object_parent_id']);
                $object = current($object);

                // pokud uz objekt neexistuje
                if(!$object)
                {
                    $text = '';
                    continue;
                }

                $text = 'Uživatel <strong>'. $user->fname.'</strong> okomentoval ';

                // pokud jde o tiskovou zpravu
                if(substr_count($act['info']['object_parent_id'], 'press'))
                {
                    // nacteme si info o tymu a o rodicovskem oebjktu
                    $parent = explode('_', $act['info']['object_parent_id']);
                    $user_team = TeamDAO::get($parent[0]);

                    $press = unserialize($object['value']);

                    $text .= '<a href="'.$user_team->getPressDetailLink($press->heading, $act['info']['object_parent_id']).'" title="Zobrazit tiskovou zprávu">tiskovou zprávu</a> týmu ' . $user_team->getTeamLink() . '.';
                }
                // pokud jde o tymovy vzkaz
                elseif(substr_count($act['info']['object_parent_id'], 'teampost'))
                {
                    // nacteme si info o tymu a o rodicovskem oebjktu
                    $parent = explode('_', $act['info']['object_parent_id']);
                    $user_team = TeamDAO::get($parent[0]);

                    // pokud komentuje tymovy vzkaz, tak to musi byt hrac daneho tymu, aby to mohl videt
                    if($user_team->isPlayer($logged_user))
                    {
                        $tp = unserialize($object['value']);

                        $text .= '<a href="'.getNewsLink($act['info']['object_parent_id'], 'Týmový vzkaz').'" title="Zobrazit týmový vzkaz">týmový vzkaz</a> týmu ' . $user_team->getTeamLink() . '.';
                    }
                    else
                    {
                        $text = '';
                        continue;
                    }
                }
                // novinky
                elseif(substr_count($act['info']['object_parent_id'], 'news'))
                {
                    $news = unserialize($object['value']);

                    if(!is_object($news))
                    {
                        $text = '';
                        continue;
                    }
                    else
                    {
                        $text .= 'novinku <a href="'.getNewsLink($act['info']['object_parent_id'], $news->title).'" title="Zobrazit novinku">"' . $news->title . '"</a>.';
                    }
                }
                elseif(substr_count($act['info']['object_parent_id'], 'roster'))
                {
                    $team = TeamDAO::get($object['team_id']);
                    $link = $team->getProfileLink('press-releases') . $tr->tr('změny na soupisce') . '-' . $act['info']['object_parent_id'] . '/';
                    $text .= '<a href="'.$link.'">změny na soupisce</a> týmu ' . $team->getTeamLink() . '.';
                }
                else
                {
                    $text = '';
                }
            break;


            case 'user_in_team':
                $class = ' icon becameFan';
                // nacteme si info o tymu
                $user_team = TeamDAO::get($act['info']['team_id']);

                // pokud uz objekt neexistuje
                if(!$user_team)
                {
                    $text = '';
                    continue;
                }

                $text = 'Uživatel <strong>'. $user->fname .'</strong> se stal fanouškem týmu '.$user_team->getTeamLink().'.';
            break;


            case 'like':
                $class = ' icon confirmSmall';
                //$text = '';

                $object = WallDAO::getObject($act['info']['object_id']);
                $object = current($object);


                // pokud uz objekt neexistuje
                if(!$object)
                {
                    $text = '';
                    continue;
                }

                $text = 'Uživateli <strong>' . $user->fname . '</strong> se líbí ';

                // pokud jde o tiskovou zpravu
                if(substr_count($act['info']['object_id'], 'press'))
                {
                    $user_team = TeamDAO::get(intval($object['team_id']));

                    $press = unserialize($object['value']);
                    $text .= '<a href="'.$user_team->getPressDetailLink($press->heading, $act['info']['object_id']).'" title="Zobrazit tiskovou zprávu">tisková zpráva</a> týmu ' . $user_team->getTeamLink() . '.';
                }
                // pokud jde o tymovy vzkaz
                elseif(substr_count($act['info']['object_id'], 'teampost'))
                {
                    // nacteme si info o tymu
                    $user_team = TeamDAO::get(intval($object['team_id']));

                    // pokud komentuje tymovy vzkaz, tak to musi byt hrac daneho tymu, aby to mohl videt
                    if($user_team->isPlayer($logged_user))
                    {
                        $tp = unserialize($object['value']);

                        $text .= '<a href="'.getNewsLink($act['info']['object_id'], 'Týmový vzkaz').'" title="Zobrazit týmový vzkaz">týmový vzkaz</a> týmu ' . $user_team->getTeamLink() . '.';
                    }
                    else
                    {
                        $text = '';
                        continue;
                    }
                }
                // novinky
                elseif(substr_count($act['info']['object_id'], 'news'))
                {
                    $news = unserialize($object['value']);

                    if(!is_object($news))
                    {
                        $text = '';
                        continue;
                    }
                    else
                    {
                        $text .= 'novinka <a href="'.getNewsLink($act['info']['object_id'], $news->title).'" title="Zobrazit novinku">"' . $news->title . '"</a>.';
                    }
                }
                elseif(substr_count($act['info']['object_id'], 'roster-insert') || substr_count($act['info']['object_id'], 'roster-delete'))
                {
                    // nacteme si info o tymu
                    $user_team = TeamDAO::get(intval($object['team_id']));

                    $link = $user_team->getPressDetailLink('Změny na soupisce', $act['info']['object_id']);

                    $text .= '<a href="'.$link.'">změny na soupisce</a> týmu ' . $user_team->getTeamLink() . '.';
                }
                elseif(substr_count($act['info']['object_id'], 'event-result'))
                {
                    $user_team = TeamDAO::get(intval($object['team_id']));

                    $event = explode('_', $object['parent_id']);
                    $event_id = $event[1] . '-' . $event[0];

                    $text .= '<a href="'.$user_team->getProfileLink('event-detail').$event_id.'/">výsledek události</a>.';
                }
                else
                {
                    $text = '';
                }
            break;
        }

        if($text)
        {
            $return .= '<p class="lastActivite'.$class.'">' . $text . '</p>';
        }
        else
        {

        }
    }
    //printr($all);
    /*
    ?>
    <p class="lastActivite iconType"><strong><?php echo $user->fname;?></strong> okomentoval <a href="#" title="Zobrazit tiskovou zprávu">tiskovou zprávu</a> teamu <a href="#" title="Zobrazit profil teamu FBC Royal Gigolos">FBC Royal Gigolos</a>.</p>
    <p class="lastActivite iconFans"><strong><?php echo $user->fname;?></strong> se stal fanouškem teamu <a href="#" title="Zobrazit profil teamu HC Kometa Brno">HC Kometa Brno</a>.</p>
    <p class="lastActivite iconPhoto"><strong><?php echo $user->fname;?></strong> byl označen na <strong>6</strong> fotografiích teamu <a href="#" title="Zobrazit profil teamu FBC Royal Gigolos">FBC Royal Gigolos</a>.</p>
    <p class="imagesLast">
        <a href="#" class="first"><img width="109" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
        <a href="#"><img width="109" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
        <a href="#"><img width="109" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
        <a href="#"><img width="109" class="last" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
    </p>
    <p class="lastActivite iconMatch"><strong><?php echo $user->fname;?></strong> okomentoval <a href="#" title="Zobrazit zápas">zápas</a> teamu <a href="#" title="Zobrazit profil teamu FBC Royal Gigolos">FBC Royal Gigolos</a>.</p>
    <p class="lastActivite iconList">Uživateli <strong><?php echo $user->fname;?></strong> se líbí akce na <a href="#" title="Zobrazit soupisku">soupisce</a> teamu <a href="#" title="Zobrazit profil teamu HC Kometa Brno">HC Kometa Brno</a>.</p>
    <?php */

    if($print_more)
    {
        $last = explode(';', $date);
        $return .= '<p class="viewMore"><a href="#" data-last="'.$last[0].'" data-user="'.$user->id.'-'.getRecipientHash($user->id).'">zobrazit starší aktivity</a></p>';
    }

    return $return;
}


function inRange($int, $min, $max)
{
    return ($int >= $min && $int <= $max);
}


/**
 * pouziva se na detailu udalosti pro vypis statistik, nepouziva se v ajaxu pro editaci
 *
 * @param  [type] $positions   [description]
 * @param  [type] $all_players [description]
 * @param  [type] $team        [description]
 * @param  array  $stats       [description]
 * @return [type]              [description]
 */
function print_players_stats_static($positions, $all_players, $team, $stats = array())
{
    $return = '<div class="'.($team == 'home' ? 'left' : 'right').' stats-%max_stat_count%">';
    $players = $all_players;
    $last_stats = array();
    $max_stat_count = 0;
    $was_found_some_players = false;

    $i = 0;
    // projdeme vsechny pozice
    foreach($positions as $position_id => $position)
    {
        if(!is_array($position->stats))
        {
            $player_stats = unserialize($position->stats);
        }
        else
        {
            $player_stats = $position->stats;
        }

        if(!empty($player_stats) && is_array($player_stats))
        {
            if($team == 'away')
            {
                $player_stats = array_reverse($player_stats);
            }

            // ulozime si nejvetsi pocet statistik, abychom mohli vygenerovat tridu pro css
            if(count($player_stats) > $max_stat_count)
            {
                $max_stat_count = 0;

                foreach($player_stats as $data)
                {
                    if(empty($data['auto']))
                    {
                        $max_stat_count++;
                    }
                }
            }

            $is_players = false;
            $ret_players = '';

            foreach($all_players as $key => $player_data)
            {
                // pokud uzivatel neexistuje, tak ho muzeme otrimovat
                $player_data['player'] = trim($player_data['player']);

                if($player_data['player'] == '000' || empty($player_data['player']))
                {
                    continue;
                }

                $exists = !isset($player_data['exists']) || $player_data['exists'] != 0;

                if($player_data['position'] == $position_id)
                {
                    $was_found_some_players = true;
                    $is_players = true;

                    if($exists)
                    {
                        $player = explode('-', $player_data['player']);
                        $player = UserDAO::get($player[0]);

                        if($player->isDeleted())
                        {
                            continue;
                        }

                        $ret_players .= '
                            <p class="cleaned">
                                <a href="'.$player->getProfileLink().'" class="name" title="'.$player->getName().'">'.$player->getShortName().'</a>';

                        foreach($player_stats as $type => $data)
                        {
                            if(!empty($data['auto']))
                            {
                                continue;
                            }

                            $id = $player_data['player'];
                            $ret_players .= '<span class="stat">'.(isset($stats[$player->id][$type]) ? $stats[$player->id][$type] : '0').'</span>';
                        }
                    }
                    else
                    {
                        $ret_players .= '
                            <p class="cleaned">
                                <span class="name" title="'.$player_data['player'].'">'.$player_data['player'].'</span>';

                        foreach($player_stats as $type => $data)
                        {
                            if(!empty($data['auto']))
                            {
                                continue;
                            }
                            $ret_players .= '<span class="stat">'.(isset($stats[$key][$type]) ? $stats[$key][$type] : '0').'</span>';
                        }
                    }

                    $ret_players .= '</p>';

                    // jakoze uz je zpracovan
                    unset($players[$key]);
                }
            }

            if($is_players && $last_stats != $player_stats)
            {
                $last_stats = $player_stats;

                $class = '';
                if($i++ == 0)
                {
                    $class = ' first';
                }
                $return .= '<p class="legend cleaned'.$class.'">';
                foreach($player_stats as $data)
                {
                    if(!empty($data['auto']))
                    {
                        continue;
                    }

                    $return .= '<span class="stat" title="'.$data['long'].'">' . $data['short'] . '</span>';
                }
                $return .= '</p>';
            }

            if($is_players)
            {
                $return .= $ret_players;
            }
        }
    }

    $return .= '</div>';

    $return = str_replace('%max_stat_count%', $max_stat_count, $return);

    if(!$was_found_some_players)
    {
      $return = '<div class="'.($team == 'home' ? 'left' : 'right').'"><p class="cleaned">Žádní hráči na soupisce.</p></div>';
      //$return .= '<p class="cleaned">Žádní hráči na soupisce.</p>';
    }

    return $return;
}



function getEventOponentTeam(Event $event)
{
    global $team;

    // $team je prave aktivní tým z preloadu
    // musime zjistit jestli jsme domaci nebo hoste
    // team_id je nas tym
    if($event->team_id != $team->id && $event->oponent_id == $team->id && !empty($event->oponent_id))
    {
        $event_team = TeamDAO::get($event->team_id);
    }
    // oponent_id je nas tym
    elseif($event->oponent_id != $team->id && $event->team_id == $team->id && !empty($event->oponent_id))
    {
        $event_team = TeamDAO::get($event->oponent_id);
    }
    // nemáme tým, tak je to trénink nebo týmová akce nebo tým není na MTW
    elseif(empty($event->oponent_id))
    {
        $event_team = false;
    }
    // nesmí nastat - nas tym neni ani jeden, pritom je zadany oponent, takze neco smrdi
    else
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }

    return $event_team;
}

/** vytiskne tabulku se statistikami tymu */
function print_stats($positions, $roster, $sport, $team_stats)
{
	global $tr;

	$return = '';

	// kontrola statistik pro vypis
	if(!is_array($sport->select_stats))
	{
		$sport->select_stats = unserialize($sport->select_stats);
	}


	foreach($positions as $position)
	{
		// realizacni tym nevypisujeme
		if($position->staff == 1)
		{
			continue;
		}

		$return .= '<h2 class="headline">'.$position->name2.'</h2>';
		// pokud mame nejake hrace na teto pozici
		if(isset($roster[$position->id]))
		{
            $count = 0;
            foreach($roster[$position->id] as $player)
            {
                $player = $player['user'];

                if(!empty($team_stats[$player->id]['gp']))
                {
                    $count++;
                }
            }

            if(empty($count))
            {
                $return .= '<div class="msg noClose">' . $tr->tr('V této kategorii nejsou žádní hráči') . '.</div>';
                continue;
            }

			// kontrola statistik pro vypis
			if(!is_array($position->select_stats))
			{
				$position->stats = unserialize($position->stats);
			}

			// spocitame si sirku jedne bunky
			$width = number_format(65 / count($position->stats), 1, '.', '');

			$return .= '<table class="table stats" cellspacing="0" cellpadding="0">
                        <thead>
						<tr class="nohover">
							<th style="width: 5%;" title="'.$tr->tr('číslo dresu').'">#</th>
							<th style="width: 30%;" class="player">'.$tr->tr('jméno').'<span class="hidden-xs">'.$tr->tr(' a příjmení').'</span></th>';
			foreach($position->stats as $key => $stat)
			{
				$return .= '<th style="width: '.$width.'%;" title="'.$stat['long'].'">'.$stat['short'].'</th>';
			}

			$return .= '</tr>
					</thead>
					<tbody>';

			// vypiseme vsechny uzivatele dane pozice
			foreach($roster[$position->id] as $player)
			{
				$info = $player['info'];
				$player = $player['user'];

                if(empty($team_stats[$player->id]['gp']))
                {
                    continue;
                }

                $class = '';
                $playerType = '';

                if($info->isHost())
                {
                    $class .= 'host';
                    $playerType = '<span class="playerType" title="Hostující hráč">H</span>';
                }

				$return .= '<tr class="'.$class.'">
							<td class="number">'.$info->number.'</td>
							<td class="player">
                                <a href="'.$player->getProfileLink().'" class="img"><img src="'.$player->getUserImage('small').'" alt="'.$player->getName().'" />'.$playerType.'</a>
                                <a href="'.$player->getProfileLink().'" title="'.$tr->tr('Zobrazit uživatele') .' '.$player->getName().'" class="name">
                                    <span class="firstName">'.$player->fname.'</span><br /><span class="lastName">'.$player->sname.'</span></a></td>';

				// vypiseme statistiky
				foreach($position->stats as $key => $stat)
				{
                    $val = '0';
                    if(isset($team_stats[$player->id][$key]))
                    {
                        if(!empty($stat['percent']))
                        {
                            $team_stats[$player->id][$key] = number_format($team_stats[$player->id][$key], 2, ',', '') . ' %';
                        }
                        $val = $team_stats[$player->id][$key];
                    }
					$return .= '<td>'.$val.'</td>';
				}

				$return .= '</tr>';
			}


            $return .= '</tbody>
                </table>';
        }
        else
        {
        	$return .= '<div class="msg noClose">' . $tr->tr('V této kategorii nejsou žádní hráči') . '.</div>';
        }
	}

	return $return;
}


function print_user_stats(array $teams, User $user)
{
    $return = '';
    foreach($teams as $team)
    {
        // nacteme si sport, ve kterem je dany tym
        $sport = SportDAO::get($team->sport_id);

        // nacteme si vsechny pozice ve kterych uzivatel hral v danem tymu
        $team_positions = StatsDAO::getAllPositionsForUserAndTeam($sport, $user->id, $team->id);

        $user_stats = array();
        // nacteme si vsechny statistiky, ktere jsou ve vsech pozici
            /*$stats = unserialize($position->stats);
            $user_stats += $stats;
        }*/
/*
        printr($team->id);
        printr($user_stats);
        die;

        //$positions
        //printr($sport->getPositions());
        //die;
        $position = Rosters::getUserInTeamPosition($team->id, $user->id);

        if(!$position)
        {
            continue;
        }

        if(!is_array($position->stats))
        {
            $position->stats = unserialize($position->stats);
        }

        if(!is_array($position->stats))
        {
            continue;
        }*/

        $return .= '<div class="teamBox cleaned">
                        <h3 class="name">
                            <span class="small">'.$sport->adjective.' tým</span><br>
                            '.$team->getTeamLink().'
                        </h3>
                        <p class="picture">
                            <a href="'.$team->getProfileLink().'" title="Zobrazit profil týmu '.$team->getName().'">
                                <img src="'.$team->getTeamLogo('small').'" height="26" width="26" alt="'.$team->getName().'" />
                            </a>
                        </p>
                    </div><!-- .teamBox -->';

        $team_seasons = $team->getSeasons();

        if(!empty($team_positions))
        {
            foreach($team_positions as $position)
            {
                if(!is_array($position->stats))
                {
                    $position->stats = unserialize($position->stats);
                }

                if(!is_array($position->stats))
                {
                    continue;
                }


                $return .= '<h3>'.$position->name.'</h3>
                            <table cellspacing="0" cellpadding="0" class="table stats">
                                <thead>
                                    <tr class="nohover">
                                        <th class="season" style="width: 28%;">sezóna</th>';

                // sirka jedne bunky
                $width = floor(72 / count($position->stats));

                foreach($position->stats as $key => $stat)
                {
                    $return .= '<th title="'.$stat['long'].'" style="width: '.$width.'%;">'.$stat['short'].'</th>';
                }

                $return .= '        </tr>
                                </thead>
                                <tbody>';

                $total = array();
                $season_count = 0;
                foreach($team_seasons as $season)
                {
                    $stats = StatsDAO::getAllTeamStats($team, $sport, $season->id, $user->id, $position->id);

                    $stats = current($stats);

                    /// pokud v sezone nema na teto pozici zadny zapas, tak nasrat
                    if(empty($stats['gp']))
                    {
                        continue;
                    }
                    $season_count++;

                    $return .= '<tr>
                                    <td class="season">
                                        '.$season->name.'
                                    </td>';
                    foreach($position->stats as $key => $stat)
                    {
                        if(!empty($stat['percent']))
                        {
                            $stats[$key] = number_format($stats[$key], 2, ',', '') . ' %';
                        }
                        $value = $stats[$key];
                        if(!isset($total[$key]))
                        {
                            $total[$key] = 0;
                        }
                        $total[$key] += $value;
                        $return .= '<td>'.$value.'</td>';
                    }

                    $return .= '</tr>';
                }

                $return .= '<tr class="summary">
                                <td class="season">
                                    celkem
                                </td>';
                    foreach($position->stats as $key => $stat)
                    {
                        if(!isset($total[$key]))
                        {
                            $total[$key] = 0;
                        }

                        if($key == 'avg')
                        {
                            if(empty($total['sa']))
                            {
                                $total[$key] = number_format(0, 2, ',', '') . '&nbsp;%';
                            }
                            else
                            {
                                $total[$key] = number_format((100 / $total['sa'] * $total['sv']), 2, ',', '') . '&nbsp;%';
                            }
                        }
                        elseif(!empty($stat['percent']))
                        {
                            $total[$key] = number_format($total[$key], 2, ',', '') . '&nbsp;%';
                        }
                        $value = $total[$key];

                        $return .= '<td>'.$value.'</td>';
                    }
                $return .= '</tr>
                        </tbody>
                    </table>';
            }
        }
        else
        {
            $return .= '<table cellspacing="0" cellpadding="0" class="table stats"><tr class="season"><td>Uživatel zatím neodehrál žádný zápas v tomto týmu.</td></tr></table>';
        }

    }

    return $return;
}

function print_gallery($gallery, $team)
{
    global $logged_user, $resizes_gallery;

    $small_name = Image::GALLERY_CACHE_PATH . intval($gallery->main_photo_id) . '-smallphoto-' . friendly_url($gallery->getPhotoPath()) .'-v-' . Image::hash($gallery->getPhotoPath() . '-' . $gallery->id . '-s', $resizes_gallery['small']) . '.jpg';
    $small = Image::get($gallery->getPhotoPath(), $gallery->main_photo . 'b' . $gallery->main_extension, $small_name, $resizes_gallery['small'], true, 'jpg', Image::GALLERY_CACHE_PATH);

    $small = pathinfo($small, PATHINFO_BASENAME);
    $small = '/' . GALLERY_CACHE_RELATIVE_PATH . $small;

    $return = '
    <li class="item" data-id="' . $gallery->id . '">
        <a class="picture" href="' . $gallery->getLink() . '" title="Zobrazit galerii ' . $gallery->getName() . '">
            <img src="' . $small . '" alt="'.$gallery->getName().'" />
        </a>';

    if($team->isAdmin($logged_user))
    {
        $return .= '<div class="overlay cleaned">
            <div class="adminButtons">
                <span class="move icon" title="Stisknutím a přetažením seřadit fotografie"><span class="forBlind">Seřadit</span></span>
                <a href="#renameItem" class="edit icon popUp" title="Editovat název fotografie"><span class="forBlind">Editovat</span></a>
                <a href="#" class="delete icon" title="Smazat fotografii"><span class="forBlind">Smazat</span></a>
            </div>
        </div>';
    }
        $return .= '<h3 class="title"><a href="' . $gallery->getLink() . '" title="Zobrazit galerii ' . $gallery->getName() . '">' . $gallery->getName() . '</a></h3>
        <p class="info cleaned">Naposledy upraveno:<span class="date">' . $gallery->getLastUpdate() . '</span></p>
    </li>';

    return $return;
}

function print_gallery_item($gallery, $photo, $team)
{
    global $logged_user, $gallery_sizes, $resizes_gallery;

    $small_name = Image::GALLERY_CACHE_PATH . intval($photo['id']) . '-smallphoto-' . friendly_url($gallery->getPhotoPath()) .'-' . (empty($team->version) ? time() : $team->version) . '-' . Image::hash($gallery->getPhotoPath() . '-' . $gallery->id . '-s', $resizes_gallery['small']) . '.jpg';
    $small = Image::get($gallery->getPhotoPath(), $photo['filename'] . 'b' . $photo['extension'], $small_name, $resizes_gallery['small'], true, 'jpg', Image::GALLERY_CACHE_PATH);
    $small = pathinfo($small, PATHINFO_BASENAME);
    $small = '/' . GALLERY_CACHE_RELATIVE_PATH . $small;


    $medium_name = Image::GALLERY_CACHE_PATH . intval($photo['id']) . '-mediumphoto-' . friendly_url($gallery->getPhotoPath()) . '-' . (empty($team->version) ? time() : $team->version) . '-' . Image::hash($gallery->getPhotoPath() . '-' . $gallery->id . '-m', $resizes_gallery['medium']) . '.jpg';
    $medium = Image::get($gallery->getPhotoPath(), $photo['filename'] . 'b' . $photo['extension'], $medium_name, $resizes_gallery['medium'], true, 'jpg', Image::GALLERY_CACHE_PATH);
    $medium = pathinfo($medium, PATHINFO_BASENAME);
    $medium;
    $medium = '/' . GALLERY_CACHE_RELATIVE_PATH . $medium;

    $return = '<li class="item" data-id="'.$photo['id'].'" data-name="'.$photo['description'].'">
                    <a class="picture fancybox" rel="gallery" href="'.$medium.'" title="'.$photo['description'].'">
                        <img src="'.$small.'" alt="'.$photo['description'].'" />
                    </a>';
    if($team->isAdmin($logged_user))
    {
        $return .= '<div class="overlay cleaned">
                            <div class="adminButtons">
                                <span class="move icon" title="Stisknutím a přetažením seřadit fotografie"><span class="forBlind">Seřadit</span></span>
                                <a href="#renameItem" class="edit icon popUp" title="Editovat popis fotografie"><span class="forBlind">Editovat</span></a>
                                <a href="#" class="star icon'.($photo['main'] == 1 ? ' active' : '').'" title="Zvolit jako hlavní fotografii galerie"><span class="forBlind">Hlavní fotka</span></a>
                                <a href="#" class="delete icon" title="Smazat fotografii"><span class="forBlind">Smazat</span></a>
                            </div>
                        </div>';
    }
    $return .= '</li>';

    return $return;
}


function print_pricelist_popup($season_id, $show_add_button = true)
{
    global $logged_user;

    $pricelist = CashboxDAO::getPricelist($logged_user->getActiveTeam()->id, $season_id);

    $return = '';

    if($show_add_button)
    {
        $return = '<div class="popUpForm" id="popUpEditCharge">';
    }

    foreach($pricelist as $item)
    {
        $return .=
        '<p data-id="'.$item['id'].'" class="entries cleaned">
            <label class="chargeName">'.$item['name'].'</label>
            <input type="text" maxlength="22" name="" class="text name forBlind" placeholder="název poplatku" />
            <label class="chargePrice">'.$item['price'].'</label>
            <input type="text" maxlength="5" name="" class="text price forBlind" placeholder="částka" />
            <span class="editDelete controls">
                <a title="Editovat poplatek" class="edit icon edit" href="#"><!-- --></a>
                <a title="Smazat poplatek" class="delete icon delete" href="#"><!-- --></a>
            </span>
            <span class="saveStorno controls forBlind">
                <a title="Uložit změnu" class="save" href="#"><!-- --></a>
                <a title="Zrušit změnu" class="storno" href="#"><!-- --></a>
            </span>
        </p>';
    }

    if($show_add_button)
    {
        $return .= '
            <p class="link addCharge">
                <a class="button buttonB small icon add">přidat další</a>
            </p>
        </div>';
    }

    return $return;
}