<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    'css' => array(
    	'//web/css/common.css',
    	'//web/css/layout.css',
    	'//web/css/style.css',
    	'//web/css/modules.css',
    	'//web/css/jquery.fancybox.css',
    	'//web/css/jquery.Jcrop.css',
    	'//web/css/jquery.fcbkcomplete.css'
    ),
    'js'  => array(
    	'//web/javascript/jquery-1.8.3.min.js',
    	'//web/javascript/jquery.tools.min.js',
    	'//web/javascript/jquery.fancybox.min.js',
    	'//web/javascript/jquery.timeago.js',
    	'//web/javascript/jquery.autoresize.js',
    	'//web/javascript/jquery.Jcrop.min.js',
    	'//web/javascript/jquery-ui-1.10.4.custom.min.js',
    	'//web/javascript/jquery.fcbkcomplete.min.js',
    	'//web/javascript/jquery.dropzone.js',
    	'//web/javascript/functions.js',
        '//web/javascript/init.js'
    ),
);