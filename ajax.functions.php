<?php

function print_players_stats_new($players, $team, $stats = array(), $sport = 0)
{
    /*
    data[home][0][exists]
    data[home][0][position]
    data[home][0][number]
    data[home][0][player]
     */
    if(!$sport instanceof Sport)
    {
        return '';
    }

    $sport_id = $sport->id;

    if(!is_array($sport->select_stats))
    {
        $sport->select_stats = unserialize($sport->select_stats);
    }

    $return = '';
    $last_stats = array();
    $i = 0;

    $positions = $all_players = array();
    foreach($players as $key => $player_data)
    {
        $player_data['player'] = trim(@$player_data['player']);

        // nevyplnene veci nas nezajimaji
        if($player_data['player'] == '000' || empty($player_data['player']))
        {
            continue;
        }

        // nacteme si info o pozici
        $position = Positions::get($player_data['position'], $sport_id);

        // vymyslena pozice, podvrzena data
        if(!($position instanceof Position))
        {
            return $return;
        }

        $positions[$position->order . '-' . $position->id] = $position;


        // mame existujiciho uzivatele
        if(!empty($player_data['exists']))
        {
            $data = explode('-', $player_data['player']);

            if(count($data) != 2 || $data[1] != getRecipientHash($data[0]))
            {
                continue;
            }

            // FIXME - kontrola zda je uzivatel opravdu clenem tymu

            $user_id = $data[0];

            $all_players[$position->id][$user_id] = array(
                'user' => UserDAO::get($user_id),
                'data' => $player_data
            );
        }
        else
        {
            $all_players[$position->id][$key] = array(
                'user' => '',
                'data' => $player_data
            );
        }
    }

    // seradime podle pozic
    ksort($positions);

    $awayCount = 0;
    // projdeme vsechny pozice
    foreach($positions as $position)
    {
        if(!is_array($position->stats))
        {
            $player_stats = unserialize($position->stats);
        }
        else
        {
            $player_stats = $position->stats;
        }

        if(!empty($player_stats))
        {
            if($team == 'away')
            {
                $player_stats = array_reverse($player_stats, true);
            }
            $is_players = false;
            $ret_players = '';

            if(isset($all_players[$position->id]) && is_array($all_players[$position->id]))
            {
                $is_players = true;

                foreach($all_players[$position->id] as $key => $player_data)
                {
                    $player         = $player_data['user'];
                    $player_data    = $player_data['data'];
                    $exists         = !empty($player_data['exists']);

                    // jedna se o existujiciho uzivatele
                    if($exists)
                    {
                        if($player->isDeleted())
                        {
                            continue;
                        }

                        $ret_players .= '
                        <p class="entries playerBox cleaned" data-name="'.$player->getName().'">
                            <label class="cleaned"><span class="number">#'.intval($player_data['number']).'</span><span class="name" title="'.$player->getName().'">'.$player->getShortName().'</span></label>';

                        foreach($player_stats as $type => $data)
                        {
                            // statistika se pocita automaticky, takze ji preskocime
                            if(!empty($data['auto']))
                            {
                                continue;
                            }
                            $id = $team . '-' . $player->id . '-' . getRecipientHash($player->id) . '-' . $position->id;

                            $ret_players .= '<input class="input text" value="'.(isset($stats[$player->id][$type]) ? $stats[$player->id][$type] : '').'" name="result[player_stats]['.$id.']['.$type.']" type="text" maxlength="2" />';
                        }
                        $ret_players .= '<input type="hidden" name="result[player_stats]['.$id.'][exists]" value="1" />';
                    }
                    else
                    {
                        $ret_players .= '
                        <p class="entries playerBox cleaned" data-name="'.$player_data['player'].'">
                            <label class="cleaned"><span class="number">#'.intval($player_data['number']).'</span><span class="name" title="'.$player_data['player'].'">'.$player_data['player'].'</span></label>';

                        foreach($player_stats as $type => $data)
                        {
                            // statistika se pocita automaticky, takze ji preskocime
                            if(!empty($data['auto']))
                            {
                                continue;
                            }
                            
                            //$id = $team . '-' . str_replace('-', '', friendly_url($key)) . '-' . $position->id;
                            //$new_key = $team . '-' . str_replace('-', '', friendly_url($player_data['player'])) . '-' . $position->id;
                            $new_key = $team . '-' . $awayCount . '-' . $position->id;
                            $ret_players .= '<input class="input text" value="'.(isset($stats[$awayCount][$type]) ? $stats[$awayCount][$type] : '').'" name="result[player_stats]['.$new_key.']['.$type.']" type="text" maxlength="2" />';
                        }
                        $ret_players .= '<input type="hidden" name="result[player_stats]['.$new_key.'][exists]" value="0" />';                        
                        $awayCount++;
                    }

                    $ret_players .= '</p>';

                    // jakoze uz je zpracovan
                    unset($all_players[$position->id][$key]);
                }
            }
            

            if($is_players && $last_stats != $player_stats)
            {
                $last_stats = $player_stats;

                $class = '';
                if($i++ == 0)
                {
                    $class = ' first';
                }

                $return .= '<p class="entries legend cleaned'.$class.'">';
                foreach($player_stats as $data)
                {
                    // statistika se pocita automaticky, takze ji preskocime
                    if(!empty($data['auto']))
                    {
                        continue;
                    }

                    $return .= '<span title="'.$data['long'].'">' . $data['short'] . '</span>';
                }
                $return .= '</p>';
            }

            if($is_players)
            {
                $return .= $ret_players;
            }
        }
    }

    return $return;
}


function print_players_stats($positions, $all_players, $team, $stats = array())
{
    $return = '';
    $players = $all_players;
    $last_stats = array();
    $i = 0;

    // projdeme vsechny pozice
    foreach($positions as $position_id => $position)
    {
        //$position = Positions::get($position_id);
        if(!is_array($position->stats))
        {
            $player_stats = unserialize($position->stats);
        }
        else
        {
            $player_stats = $position->stats;
        }

        if(!empty($player_stats))
        {
            $is_players = false;
            $ret_players = '';
            foreach($all_players as $key => $player_data)
            {
                $exists = !isset($player_data['exists']) || $player_data['exists'] != 0;

                if((($exists && $player_data['position_id'] == $position_id) || (!$exists && $player_data['position'] == $position_id)) && !empty($player_data['player']))
                {
                    $is_players = true;

                    if($exists)
                    {
                        unset($player_data['id']);
                        $player = new User($player_data);

                        if($player->isDeleted())
                        {
                            continue;
                        }

                        $ret_players .= '
                        <p class="entries playerBox cleaned" data-name="'.$player->getName().'">
                            <label class="cleaned"><span class="number">#'.$player_data['number'].'</span><span class="name" title="'.$player->getName().'">'.$player->getShortName().'</span></label>';

                        foreach($player_stats as $type => $data)
                        {
                            // statistika se pocita automaticky, takze ji preskocime
                            if(!empty($data['auto']))
                            {
                                continue;
                            }
                            $id = $team . '-' . $player->id . '-' . getRecipientHash($player->id) . '-' . $position_id;

                            $ret_players .= '<input class="input text" value="'.(isset($stats[$player->id][$type]) ? $stats[$player->id][$type] : '').'" name="result[player_stats]['.$id.']['.$type.']" type="text" maxlength="2" />';
                        }
                        $ret_players .= '<input type="hidden" name="result[player_stats]['.$id.'][exists]" value="1" />';
                    }
                    else
                    {
                        $ret_players .= '
                        <p class="entries playerBox cleaned" data-name="'.$player_data['player'].'">
                            <label class="cleaned"><span class="number">#'.$player_data['number'].'</span><span class="name" title="'.$player_data['player'].'">'.$player_data['player'].'</span></label>';

                        foreach($player_stats as $type => $data)
                        {
                            // statistika se pocita automaticky, takze ji preskocime
                            if(!empty($data['auto']))
                            {
                                continue;
                            }

                            //$id = $team . '-' . str_replace('-', '', friendly_url($data['player'])) . '-' . $position_id;
                            $new_key = str_replace('-', '', friendly_url($data['player']));

                            $ret_players .= '<input class="input text" value="'.(isset($stats[$new_key][$type]) ? $stats[$new_key][$type] : '').'" name="result[player_stats]['.$new_key.']['.$type.']" type="text" maxlength="2" />';
                        }
                        $ret_players .= '<input type="hidden" name="result[player_stats]['.$new_key.'][exists]" value="0" />';
                    }                

                    $ret_players .= '</p>';

                    // jakoze uz je zpracovan
                    unset($players[$key]);
                }
            }

            if($is_players && $last_stats != $player_stats)
            {
                $last_stats = $player_stats;

                $class = '';
                if($i++ == 0)
                {
                    $class = ' first';
                }
                $return .= '<p class="entries legend cleaned'.$class.'">';
                foreach($player_stats as $data)
                {
                    // statistika se pocita automaticky, takze ji preskocime
                    if(!empty($data['auto']))
                    {
                        continue;
                    }
                    
                    $return .= '<span title="'.$data['long'].'">' . $data['short'] . '</span>';
                }
                $return .= '</p>';
            }

            if($is_players)
            {
                $return .= $ret_players;
            }
        }
    }

    return $return;
}



function print_player_roster($positions, $players, $numbers, $i, $team, $exists = true, $rosters = array(), $players_count = 0, $show_dash = false)
{
    // fce se vola v cyklu pro kazdeho hrace, nastavime toho ktery se zrovna vypisuje
    if(is_array($rosters) && isset($rosters[$team][$i]))
    {
        $selected = $rosters[$team][$i];
        $plid = explode('-', $selected['player']);
        $selected['player_id'] = $plid[0];
    }
    else
    {
        $selected = array(
            'exists'    => '',
            'position'  => '0',
            'number'    => '',
            'player'    => '',
            'player_id' => ''
        );
    }

    $return = '<p class="entries cleaned'.($i == $players_count ? ' substitutes' : '').'">
                    <input type="hidden" name="result[players]['.$team.']['.$i.'][exists]" value="'.($exists ? '1' : '0').'" />
                    <select class="position" name="result[players]['.$team.']['.$i.'][position]">';
                    if($show_dash)
                    {
                        //$return .= '<option value="0"'.(!empty($selected['position']) ? ' selected="selected"' : '').'>---</option>';
                    }
                    $j = 0;
                    foreach($positions as $position)
                    {
                        if(!empty($position->staff))
                        {
                            continue;
                        }
                        $return .= '<option '.($selected['position'] == $position->id ? 'selected="selected" ' : '').'value="'.$position->id.'">'.$position->name.'</option>';
                    }
    $return .=     '</select>';

                    
    $return .=      '<select class="number" name="result[players]['.$team.']['.$i.'][number]">
                        <option'.(empty($selected['number']) ? ' selected="selected"' : '').' value="000">0</option>';
                    foreach($numbers as $key => $number)
                    {
                        $return .= '<option'.($selected['number'] == $number ? ' selected="selected"' : '').' value="'.$key.'">'.intval($number).'</option>';
                    }
    $return .=     '</select>';


    if($exists)
    {
        $return .= '
                    <select class="player" name="result[players]['.$team.']['.$i.'][player]">
                        <option'.(empty($selected['player']) ? ' selected="selected"' : '').' value="000">Hráč</option>';

                    if(!in_array($selected['player_id'], array_keys($players)))
                    {
                        foreach($players as $key => $data)
                        {
                            if(!empty($data['staff']))
                            {
                                continue;
                            }
                            $player = new User($data);
                            $val = $player->id . '-' . getRecipientHash($player->id);
                            $return .= '<option'.($selected['player'] == $val ? ' selected="selected"' : '').' data-number="'.$data['number'].'" data-position="'.$data['position_id'].'" value="' . $val.'">'.$player->getName().'</option>';
                        }
                    }
                    else
                    {
                        foreach($players as $key => $data)
                        {
                            /*if($key != $selected['player_id'])
                            {
                                continue;
                            }*/
                            if(!empty($data['staff']))
                            {
                                continue;
                            }

                            $player = new User($data);
                            $val = $player->id . '-' . getRecipientHash($player->id);
                            $return .= '<option'.($selected['player'] == $val ? ' selected="selected"' : '').' data-number="'.$data['number'].'" data-position="'.$data['position_id'].'" value="' . $val.'">'.$player->getName().'</option>';
                        }
                    }
        $return .= '</select>';
    }
    // neexistujici hraci
    else
    {
        // mame vyplneny uz sestavy
        if(count($players) > 0)
        {
            foreach($players as $key => $data)
            {
                if(!empty($data['staff']))
                {
                    continue;
                }

                $new_key = $team . '-' . $i . '-' . $data['position'];

                $return .= '<input class="input text" type="text" name="result[players]['.$team.']['.$i.'][player]" value="'.$data['player'].'" maxlength="100" />';
            }
        }
        else
        {
            $return .= '<input class="input text" type="text" name="result[players]['.$team.']['.$i.'][player]" value="'.$selected['player'].'" maxlength="100" />';
        }
    }

    $return .= '</p>'; 
    
    return $return;
}

?>