<?php /* ********** translated ************ */ ?>


<?php
if(!isset($not_logged_access) || !is_array($not_logged_access))
{
    $not_logged_access = array();
}

if((!empty($logged_user->id) || (in_array($action, $not_logged_access))) && $action != 'registration') // uzivatel je prihlasen a neni to aktivace uctu
{
?>

	<?php 
    if(!empty($logged_user->id))
    {
    /*require_once(BLOCK_PATH . 'header.php'); */
    }
    ?>

    <div id="main">
    	<div class="container">
            <div class="row">

                <section class="sidebar left colSmall">

                    <?php
                    if($team instanceof Team && $team->id > 0)
                    {
                        require_once(BLOCK_PATH . 'team-info.php');

                        if(!empty($logged_user->id))
                        {
                            require_once(BLOCK_PATH . 'team-options.php');
                        }

                        require_once(BLOCK_PATH . 'last-match.php');
                 
                        if(!empty($logged_user->id))
                        {
                            require_once(BLOCK_PATH . 'recommend.php');
                        }
                        //require_once(BLOCK_PATH . 'poll.php');
                    }
                    elseif(!empty($logged_user->id))
                    {
                        require_once(BLOCK_PATH . 'team-registration.php');

                        require_once(BLOCK_PATH . 'recommend.php');
                    }
                    
                    ?>

                </section><!-- .side-column .left -->

                <section id="content" class="colBig">

        		  <?php require_once 'web/pages/'.$page.'.php'; ?>

                </section><!-- #content -->

        		<section class="sidebar right colSmall">

                    <?php
                    if($team instanceof Team && $team->id > 0 && $action != 'attendance' && $team->id == $logged_user->getActiveTeam()->id)
                    {
                        require_once(BLOCK_PATH . 'next-events.php');
                    }
                    ?>

                    <div class="fixedBlock"> <? /* musi byt zacilen v js a nasteven v css */ ?>
        			     <?php require_once(BLOCK_PATH . 'advertising.php'); ?>
                    </div><!-- .fixedBlock -->

        		</section><!-- .side-column .right -->

            </div><!-- .row -->
        </div><!-- .container -->

        <?php
        if( $action == 'wall' )
        {
        ?>
        <div id="scrollTop">
            <a href="#" title="<?=$tr->tr('nahoru')?>"><span class="forBlind"><?=$tr->tr('nahoru')?></span></a>
        </div>
        <?php
        }
        ?>
    </div><!-- #main -->

    <?php
    if( !empty($logged_user->id) )
    {
    ?>
    <div id="feedback">
        <a href="<?=PATH_WEB_ROOT?>ajax.php?feedbackForm=true" class="popUpMediumAjax" title="<?=$tr->tr('Zpětná vazba')?>"><!-- --></a>
    </div>
    <?php
    }
    ?>

<?php
}
else
{
?>
    <div id="main">
        <div class="container">
            <div class="row">

                <section class="sidebar left colSmall">

                </section><!-- .side-column .left -->

                <section id="content" class="colBig">

                  <?php require_once 'web/pages/'.$page.'.php'; ?>

                </section><!-- #content -->

                <section class="sidebar right colSmall">

                </section><!-- .side-column .right -->

            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- #main -->
<?php
}
?>