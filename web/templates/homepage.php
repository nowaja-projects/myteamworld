<div id="mainHp">
    <div class="container">
        <div class="row">

            <section id="content" class="colFullWidth">

              <?php require_once 'web/pages/'.$page.'.php'; ?>

            </section><!-- #content -->

        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- #main -->

<div id="bottom">
    <div class="container">
        <div class="row">

            <div class="column first">
                <h3 class="name">Zaregistrujte se</h3>
                <p class="text">Po vytvoření uživatelského účtu získáte možnost zdarma, rychle a&nbsp;jednoduše založit stránky pro váš sportovní tým nebo do vašeho týmu vstoupit.</p>
                <a class="button main" href="what-is-myteamworld#registration" title="Zobrazit více informací">co všechno registrací získáte?</a>
            </div>
            <div class="column second">
                <h3 class="name">Stránky na míru</h3>
                <p class="text">Zobrazujte na vašich stránkách pouze funkce a&nbsp;moduly, které opravdu chcete využívat. Ty nepotřebné veřejnosti skryjte nebo jednoduše vypněte.</p>
                <a class="button main" href="what-is-myteamworld#pages" title="Zobrazit více informací">jak to funguje?</a>
            </div>
            <div class="column third">
                <h3 class="name">Profesionální funkce</h3>
                <p class="text">Veďte zápasové i hráčské statistiky, nahrávejte fotky z turnajů, zapisujte docházku na týmové akce. Stejně jako profesionální týmy!</p>
                <a class="button main" href="what-is-myteamworld#functions" title="Zobrazit více informací">jaké rozšíření máte k dispozici?</a>
            </div>
            <div class="column fourth">
                <h3 class="name">Analyzujte a zlepšujte se</h3>
                <p class="text">V průběhu sezóny můžete jednoduše analyzovat hráčské i týmové statistiky a&nbsp;v&nbsp;trénincích a taktice se zaměřit na to, v&nbsp;čem máte největší rezervy.</p>
                <a class="button main" href="what-is-myteamworld#analysis" title="Zobrazit více informací">co mohu analyzovat?</a>
            </div>
            <div class="column fifth">
                <h3 class="name">Sdílejte své úspěchy</h3>
                <p class="text">Vaše fanoušky a&nbsp;kamarády můžete jednoduše informovat o&nbsp;dění v&nbsp;týmu zveřejněním odkazu na vaše stránky nebo sdílením na sociálních sítích.</p>
                <a class="button main" href="what-is-myteamworld#share" title="Zobrazit více informací">jak a co jde sdílet?</a>
            </div>

        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- #bottom -->