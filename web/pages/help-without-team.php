<div class="sidebar left colSmall">

    <div class="fixedBlock"> <? /* musi byt zacilen v js a nasteven v css */ ?>
        <div id="navigation">

            <h2 class="blockTitle">Navigace</h2>

            <div class="content">

                <ul>
                    <li><a class="anchor" href="#wall">Nástěnka</a></li>
                    <li><a class="anchor" href="#teams">Týmy</a></li>
                    <li><a class="anchor" href="#user-profile">Profil uživatele</a></li>
                    <li><a class="anchor" href="#notices">Upozornění</a></li>
                    <li><a class="anchor" href="#messages">Osobní zprávy</a></li>
                    <li><a class="anchor" href="#team-registration">Registrace týmu</a></li>
                    <li><a class="anchor" href="#team-members">Vstup do týmu</a></li>
                </ul>

            </div><!-- .content -->

        </div><!-- #navigation -->
    </div><!-- .fixedBlock -->

</div><!-- .sidebar.left.colSmall -->

<section id="content" class="colBig">

    <div id="contentA" class="text">

    	<div class="contentAbox">

         	<h2 class="headline">Nápověda</h2>
         	<p class="perex">Nevíte si rady s něčím na myteamworld.com? Potřebujete trochu nasměrovat? Doufáme, že na této stránce najdete to, co vám není jasné.</p>

         	<h3 id="wall" class="title">Nástěnka</h3>
         	<p>Nástěnka slouží k zobrazení zpráv a&nbsp;informací, které vás mohou zajímat. Budou se zde zobrazovat všechny novinky ze světa sportů, které jste si vybrali v <a class="anchor" href="#user-profile" title="Zobrazit více informací o nastavení profilu">nastavení vašeho profilu</a>, novinky týmů, jichž jste <a class="anchor" href="#teams" title="Zobrazit více informací o týmech">fanouškem</a> nebo které <a class="anchor" href="#teams" title="Zobrazit více informací o týmech">sledujete</a>.</p>
            <p>Pro lepší orientaci ve zprávách, které si necháváte zobrazovat na nástěnce, můžete využít "filtr zpráv". Ten zobrazíte pomocí tlačítka v pravém horním rohu nástěnky. Zatrhněte kategorie, které si přejete zobrazit, a&nbsp;klikněte na tlačítko "použít filtr". Na nástěnce se vám zobrazí pouze zprávy z&nbsp;kategorií, které jste vybrali. Ve "filtru zpráv" můžete vybírat pouze z&nbsp;kategorií, které máte v&nbsp;<a class="anchor" href="#user-profile" title="Zobrazit více informací o nastavení profilu">nastavení vašeho profilu</a> jako aktivní.</p>
            <p>Ke každé zprávě můžete přidat komentář nebo ji označit jako "líbí se mi". Je-li u novinky více komentářů, můžete si všechny zobrazit kliknutím na "zobrazit všechny komentáře" v&nbsp;pravém spodním rohu dané zprávy. Pokud se novinka líbí více lidem, můžete najetím myší na bublinu s&nbsp;počtem "líbí se" zobrazit, kterým uživatelům se to líbí. Kliknutím na nadpis sportovní zprávy se dostanete na webové stránky serveru, který zprávu vydal, kde si můžete přečíst celou zprávu.</p>

            <h3 id="teams" class="title">Týmy</h3>
            <p>Zobrazíte-li si detail jakéhokoliv týmu na myteamworld.com, zobrazí se vám v&nbsp;levém sloupci základní informace k týmu. Tento blok "tým info" zároveň ukazuje, na který tým na myteamworld.com se díváte a&nbsp;ke kterému týmu se váže obsah, který momentálně prohlížíte.</p>
            <p>Každý tým na myteamworld.com můžete sledovat nebo se stát jeho fanouškem. Pokud budete sledovat dění v týmu, stanete se "odběratelem" novinek tohoto týmu. Všechny novinky nebo změny, které tým provede, se zobrazí na vaší <a class="anchor" href="#wall" title="Zobrazit více informací o nástěnce">nástěnce</a>. Pokud se stanete fanouškem týmu, budete stejně jako "odběratel" dostávat všechny novinky z&nbsp;týmu a&nbsp;navíc rozšíříte fanklub týmu. Záleží pouze na vás, chcete-li tým podpořit a&nbsp;stát se jejich fanouškem, nebo týmu nefandíte, ale chcete sledovat dění v týmu. Fanouškem nebo odběratelem se můžete stát na detailu týmu pod blokem "tým info".</p>

            <h3 id="user-profile" class="title">Profil uživatele</h3>
            <p>V pravém horním rohu každé stránky na myteamworld.com máte základní informace ke svému účtu. Vidíte zde vaše jméno, profilovou fotografii a&nbsp;taky informaci, že jste volný hráč (nejste členem žádného týmu na myteamworld.com). Dále zde vidíte počet <a class="anchor" href="#messages" title="Zobrazit více informací o soukromých zprávách">soukromých zpráv</a> a&nbsp;<a class="anchor" href="#notices" title="Zobrazit více informací o upozorněních">upozornění</a>, možnost "upravit profil" a&nbsp;"odhlásit se" v rozbalovacím menu, které se zobrazí po najetí myší na šipku vpravo. Kliknutím na "upravit profil" se dostanete na stránku, kde můžete upravovat své základní osobní informace, změnit si profilovou fotografii nebo si nastavit svou nástěnku, soukromí a&nbsp;účet.</p>

            <h3 id="notices" class="title">Upozornění</h3>
            <p>V základních informacích o&nbsp;svém účtu v pravém horním rohu stránky je ikonka zvonečku, v které se zobrazuje počet vašich upozornění. Kliknutím na tento odkaz se zobrazí stránka s&nbsp;výpisem všech vašich upozornění na myteamworld.com. Tato upozornění jsou řazena od nejnovějších po nejstarší a&nbsp;dozvíte se v nich důležité informace.</p>

            <h3 id="messages" class="title">Osobní zprávy</h3>
            <p>V základních informacích o&nbsp;svém účtu v pravém horním rohu stránky je ikonka dvou bublin, v které se zobrazuje počet vašich nepřečtených osobních zpráv. Kliknutím na tento odkaz se zobrazí stránka s&nbsp;výpisem všech vašich přijatých a&nbsp;odeslaných zpráv na myteamworld.com.</p>
            <p>Pokud chcete napsat zprávu uživateli na myteamworld.com, se kterým ještě nemáte vytvořenou konverzaci, můžete tak učinit v profilu tohoto uživatele. Vpravo od jeho jména je tlačítko "napsat zprávu". Po odeslání první zprávy se konverzace přidá na stránku s&nbsp;osobními zprávami.</p>
            <p>Po rozkliknutí konverzace s&nbsp;uživatelem se vám zobrazí poslední přijaté a&nbsp;odeslané zprávy s&nbsp;tímto uživatelem. Také mu zde můžete odeslat další zprávu. Pod profilovou fotografií uživatele je tlačítko na smazání celé konverzace. Tímto tlačítkem se nenávratně smažou všechny odeslané i&nbsp;přijaté zprávy mezi vámi a&nbsp;tímto uživatelem. Pokud si poté mezi sebou pošlete další zprávu, vytvoří se úplně nová konverzace.</p>

            <h3 id="team-registration" class="title">Registrace týmu</h3>
            <p>V hlavním horním menu každé stránky v sekci "Ostatní" je odkaz na registraci nového týmu. Po dokončení registrace se vytvoří na myteamworld.com nový sportovní tým, který bude mít jednoho člena, a&nbsp;to vás. Zároveň jste i&nbsp;majitelem a administrátorem týmu.</p>
            <p>Přidat další uživatele do týmu je možné až po dokončení registrace, a&nbsp;to v&nbsp;profilu uživatele kliknutím na tlačítko vedle jména uživatele nebo v&nbsp;modulu týmu "soupiska" tlačítkem "přidat člena".</p>
            <p>Další nastavení týmu, modulů, změny administrátorů a&nbsp;mnohem více dalších věcí je možné provést po dokončení registrace na stránce "nastavení týmu", na kterou se dostanete kliknutím na tlačítko "nastavení týmu" v&nbsp;bloku "<a class="anchor" href="#teams" title="Zobrazit více informací o bloku tým info">tým info</a>" vašeho aktivního týmu.</p>

            <h3 id="team-members" class="title">Vstup do týmu</h3>
            <p>Pokud tým, kterého jste členem, už na myteamworld.com existuje, můžete do něj vstoupit pomocí pozvánky, kterou vám může zaslat admin daného týmu nebo můžete o vstup do týmu zažádat kliknutím na tlačítko "zažádat o vstup do týmu" pod blokem "<a class="anchor" href="#teams" title="Zobrazit více informací o bloku tým info">tým info</a>", pokud má daný tým tuto možnost povolenou.</p>

         </div><!-- .contentAbox -->

    </div><!-- #contentA -->
</section><!-- #content -->