<div class="sidebar left colSmall">
    
    <div class="fixedBlock"> <? /* musi byt zacilen v js a nasteven v css */ ?>
        <div id="navigation">

            <h2 class="blockTitle">Navigace</h2>

            <div class="content">

                <ul>
                    <li><a class="anchor" href="#registration">Co všechno registrací získáte?</a></li>
                    <li><a class="anchor" href="#pages">Stránky na míru</a></li>
                    <li><a class="anchor" href="#functions">Profesionální funkce</a></li>
                    <li><a class="anchor" href="#analysis">Analyzujte a zlepšujte se</a></li>
                    <li><a class="anchor" href="#share">Sdílejte své úspěchy</a></li>
                </ul>

            </div><!-- .content -->

        </div><!-- #navigation -->
    </div><!-- .fixedBlock -->

</div><!-- .sidebar.left.colSmall -->

<section id="content" class="colBig">
    <div id="contentA" class="text">

        <div class="contentAbox">

        	<h2 class="headline">Co je myteamworld?</h2>
        	<p class="perex">Myteamworld.com je webová stránka, díky které můžete pro svůj sportovní tým zdarma založit webové stránky s funkcemi, které používají profesionální sportovní týmy.
            Odkaz na vaše týmové stránky můžete pro vaše fanoušky a kamarády zveřejnit nebo je sdílet na sociálních sítích. Navíc můžete získávat aktuální novinky z mnoha sportovních odvětví a sledovat
            svoje oblíbené týmy na&nbsp;myteamworld.com!</p>

        	<h3 id="registration" class="title">Co všechno registrací získáte?</h3>
        	<p>Vytvořením účtu na myteamworld.com okamžitě získáte možnost odebírat aktuální sportovní novinky z&nbsp;domova i ze světa!
            Můžete si vybrat z několika sportovních serverů, mnoha sportů a mít aktuální přehled o&nbsp;sportovních novinkách.</p>
        	<p>S vaším uživatelským účtem na myteamworld.com můžete snadno, rychle a zdarma vytvořit stránky pro váš sportovní tým, nastavit funkce které chcete využívat a ihned stránky začít používat.</p>

        	<h3 id="pages" class="title">Stránky na míru</h3>
        	<p>U všech funkcí a rozšíření na myteamworld.com máte na výběr, jestli je chcete zobrazovat všem uživatelům kteří na vaše stránky přijdou nebo jen členům vašeho týmu.
            Samozřejmě můžete kteroukoliv funkci úplně vypnout a nepoužívat.</p>

        	<h3 id="functions" class="title">Profesionální funkce</h3>
        	<p>Momentálně máte na výběr z těchto funkcí a rozšíření: tiskové zprávy, události, soupiska, statistiky, docházka a galerie<?php /* galerie a týmová kasa */ ?>.</p>
            <p>V "tiskových zprávách" můžete psát články o dění v týmu, v "událostech" zveřejňovat zápasy a tréninky, které váš tým odehraje a zadávat k nim výsledky a statistiky.</p>
            <p>Funkce "soupiska" a "statistiky" slouží k přehledu členů v týmu a k analýze jejich hry. Jednoduše a přehledně zjistíte, kdo je nejlepší střelec, kdo nejlépe
            nahrává a kdo je nejtrestanější hráč týmu!</p>
            <p>Díky "docházce" můžete rychle a přehledně zjistit, kolik lidí dorazí na dnešní trénink nebo zápas.</p>
            <p>Rozšíření "galerie" vám umožňuje podělit se s fanoušky nebo spoluhráči vašeho týmu o fotografie například z posledního turnaje nebo nedávného soustředění.</p>
            <?php /*<p>"Týmová kasa" vám zaručí, že všichni členové vašeho týmu budou vědět, kolik přesně zaplatili nebo dluží do týmové kasy a kolik peněz přesně váš tým v týmové kase má!</p>*/ ?>

            <h3 id="analysis" class="title">Analyzujte a zlepšujte se</h3>
            <p>Během nebo na konci sezóny můžete díky vedení týmových statistik jednoduše a přehledně analyzovat, co vašemu týmu jde nejlépe a kde má naopak rezervy pro zlepšení.</p>

            <h3 id="share" class="title">Sdílejte své úspěchy</h3>
            <p>Vaše fanoušky, známé i kamarády můžete jednoduše informovat o dění v týmu zveřejněním odkazu na vaše stránky na myteamworld.com nebo sdílením na sociálních sítích.
            Uživatelé myteamworld.com se navíc mohou stát fanoušky nebo odběrately vašeho týmu a každá akce či změna ve vašem týmu se jim ihned zobrazí na jejich nástěnce.</p>

        </div><!-- .contentAbox -->
        
    </div><!-- #contentA .module -->
</section><!-- #content -->

<div class="sidebar right colSmall">
    
    <div class="fixedBlock"> <? /* musi byt zacilen v js a nasteven v css */ ?>
        <div id="screenshots">

            <h2 class="blockTitle">Náhledy stránek</h2>

            <div class="content">

                <div class="item">
                    <a class="img fancybox" href="<?=IMAGES_PATH?>help/screenshot-events.jpg" rel="screenshots" title="Ukázka rozšíření události">
                        <img src="<?=IMAGES_PATH?>help/screenshot-events-small.jpg" alt="" />
                    </a>
                </div>

                <div class="item">
                    <a class="img fancybox" href="<?=IMAGES_PATH?>help/screenshot-attendance.jpg" rel="screenshots">
                        <img src="<?=IMAGES_PATH?>help/screenshot-attendance-small.jpg" alt="" />
                    </a>
                </div>

                <div class="item">
                    <a class="img fancybox" href="<?=IMAGES_PATH?>help/screenshot-match-statistics.jpg" rel="screenshots">
                        <img src="<?=IMAGES_PATH?>help/screenshot-match-statistics-small.jpg" alt="" />
                    </a>
                </div>

            </div><!-- .content -->

        </div><!-- #screenshots -->
    </div><!-- .fixedBlock -->

</div><!-- .sidebar.right.colSmall -->