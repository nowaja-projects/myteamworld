<div id="contentA" class="module">

<?php
if($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id)
{
?>
    <div id="admin">
        <a href="#addPhotos" id="addPhotosButton" class="button buttonB icon add popUpMedium" title="Nahrát nové fotografie">vytvořit novou fotogalerii</a>
    </div>
<?php
}

$list = GalleryDAO::getGalleryList($team->id);
?>

    <div class="contentAbox clear">
    	<h2 class="headline">Galerie</h2>

        <?php
        if(count($list))
        {
        ?>
    	<ul id="galleryList" class="cleaned">
            <?php
            foreach($list as $key => $gallery)
            {
        		echo print_gallery($gallery, $team);
            }
            ?>
        </ul>
        <?php
        }
        else
        {
        ?>
            <div class="msg warning noClose">Tým momentálně nemá žádné galerie k zobrazení.</div>
        <?php
        }
        ?>
    </div>

    <?php
    if($logged_user->getActiveTeam()->id == $team->id && $team->isAdmin($logged_user))
    {
    ?>

    <div class="forBlind">

        <div id="addPhotos" class="fancybox-popup" data-id="">
            <h3 class="title">Vytvořit novou galerii</h3>

            <form id="createGallery" class="popUpForm" action="">
                <fieldset class="cleaned">
                    <legend class="forBlind">Název galerie</legend>

                    <p class="entries first">
                        <label>Název galerie</label><br />
                        <input class="text galleryName" type="text" name="gallery[name]" />
                        <span class="errorMsg">Vyplňte, prosím, toto pole.</span>
                    </p>
                </fieldset>

                <fieldset>
                    <p class="entries submit">
                        <button type="submit">uložit a pokračovat</button>
                    </p>
                </fieldset>
            </form>

            <form id="uploadPhotos" class="popUpForm galleryListPopup dropzone" action="<?=PATH_WEB_ROOT?>gallery_ajax.php">
                <fieldset class="cleaned">
                    <legend class="forBlind">Nahrát nové fotografie</legend>

                    <input class="galleryID" type="hidden" name="gallery_id" value="0" />
                    <input class="galleryHash" type="hidden" name="gallery_hash" value="0" />
                </fieldset>
            </form>

            <p class="link">
                <a href="#" title="hotovo, zavřít">hotovo</a>
            </p>

        </div><!-- #addPhotos -->

        <div id="renameItem" class="fancybox-popup">
            <h3 class="title">Editovat název galerie</h3>

            <form class="popUpForm" action="">
                <fieldset>
                    <legend class="forBlind">Editovat název galerie</legend>

                    <p class="entries first">
                        <label>Název galerie</label><br />
                        <input class="renameID" type="text" name="photo_name" value="" data-id="0" />
                    </p>

                    <p class="entries submit">
                        <button type="submit">uložit a zavřít</button>
                    </p>
                </fieldset>
            </form>
        </div><!-- #renameItem -->

    </div><!-- .forBlind -->
    
    <?php
    }
    ?>

</div><!-- #contentAbox .module -->