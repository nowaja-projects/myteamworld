
<div id="contentA" class="registration team">
<?php
$team_modules = $team->getModules();

if($action == 'edit-team-profile')
{
    $tmp = $team;

    if($team_modules)
    {
        $tmp->setModules($team_modules);
    }
}

$dir = ($action == 'team-registration' ? '/'.session_id().'/' : '/');
include(BLOCK_PATH.'messages.php');
?>
    <div class="contentAbox">

        <form method="post" action="" enctype="multipart/form-data">

                <?php if($action != 'team-registration') { ?>
                <h2 class="headline">Úprava týmu</h2>
                <input type="hidden" name="team[id]" value="<?=$tmp->id?>" />
                <?php } else { ?>
                <h2 class="headline">Registrace nového týmu</h2>
                <input type="hidden" name="tmpImages" id="tmpImages" value="<?=$dir?>" />
                <?php } ?>
                <div class="inner cleaned">

                    <div class="left">
                        <p id="team_picture_big" class="picture">
                            <img src="<?=$tmp->getTeamLogo('big',$dir)?>" alt="<?=$tmp->getName()?>" />
                        </p>
                        <ul class="list">
                            <li class="item upload">
                                <a href="#" title="Nahrát nové týmové logo">
                                    <span class="hidden-sm hidden-xs hidden-xxs">Nahrát týmové logo</span>
                                    <span class="visible-sm-inline visible-xs-inline visible-xxs-inline">Nahrát logo</span>
                                </a>
                            </li>
                            <?php
                            $show = $tmp->hasImage();
                            if($show)
                            {
                            ?>
                            <li class="item edit hidden-xs hidden-xxs">
                                <a href="#editProfilePhoto" class="popUpBig" title="Upravit výřez týmového loga">
                                    <span class="hidden-md hidden-sm hidden-xs hidden-xxs">Upravit výřez týmového loga</span>
                                    <span class="visible-md-inline">Upravit výřez loga</span>
                                    <span class="visible-sm-inline visible-xs-inline visible-xxs-inline">Upravit výřez</span>
                                </a>
                            </li>
                            <li class="item delete">
                                <a href="#" id="deleteProfilePhoto" title="Smazat týmové logo">
                                    <span class="hidden-sm hidden-xs hidden-xxs">Smazat týmové logo</span>
                                    <span class="visible-sm-inline visible-xs-inline visible-xxs-inline">Smazat logo</span>
                                </a>
                            </li>
                            <?php
                            }
                            ?>
                        </ul>

                        <p class="forBlind">
                            <input id="uploadPhoto" type="file" name="image" />

                            <input id="x" type="text" name="x" />
                            <input id="y" type="text" name="y" />
                            <input id="w" type="text" name="w" />
                            <input id="h" type="text" name="h" />

                        </p>

                        <div class="forBlind">
                            <div id="editProfilePhoto" class="fancybox-popup">

                                <h3 class="title">Upravit výřez fotografie</h3>

                                <div class="containerBox cleaned">
                                    <p class="mainPhoto">
                                        <span class="infoTitle">Vyberte výřez vaší fotografie:</span>
                                        <span class="infoText">Kliknutím a tažením myši na vaší profilové fotografii vyberte výřez, který chcete používat na celém webu.</span>
                                        <span class="crop team_picture_main">
                                            <img id="mainPhoto" src="<?=$tmp->getTeamLogo('original',$dir)?>" alt="Hlavní fotografie" width="<?=MAX_ORIGINAL_WIDTH?>" />
                                        </span>
                                        <span class="infoHelp">Náhledy miniatur, které se používají například v profilu, komentářích nebo soupisce vidíte v pravo.</span>
                                    </p>

                                    <p class="profilePhoto">
                                        <span class="crop team_picture_main">
                                            <img id="profilePhoto" src="<?=$tmp->getTeamLogo('original', $dir)?>" alt="" />
                                        </span>
                                    </p>

                                    <p class="commentPhoto">
                                        <span class="crop team_picture_main">
                                            <img id="commentPhoto" src="<?=$tmp->getTeamLogo('original', $dir)?>" alt="" />
                                        </span>
                                    </p>

                                    <p class="teamPhoto">
                                        <span class="crop team_picture_main">
                                            <img id="teamPhoto" src="<?=$tmp->getTeamLogo('original', $dir)?>" alt="" />
                                        </span>
                                    </p>

                                    <p class="controls">
                                        <a id="saveCropFoto" class="close" href="#" title="Uložit výřez a zavřít okno">uložit výřez fotografie</a>
                                    </p>

                                </div><!-- .container -->

                            </div><!-- .fancybox-popup -->
                        </div>
                    </div><!-- .left -->

                    <div class="right">
                        <div class="group">
                            <p class="entries cleaned">
                                <label class="label" for="fc">Typ klubu:</label>
                                <input class="input text small" type="text" id="fc" name="team[prefix]" value="<?=(isset($_POST['team']['prefix']) ? $_POST['team']['prefix'] : $tmp->prefix); ?>" />
                                <span class="help"><!-- --></span><span class="tooltip">Např. HC, FC, FBC...</span>
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="surname">Název týmu:<span class="star">*</span></label>
                                <input class="input text" type="text" id="surname" name="team[name]" value="<?=(isset($_POST['team']['name']) ? $_POST['team']['name'] : $tmp->name); ?>" />
                                <span class="help"><!-- --></span><span class="tooltip">Např. Kometa Brno</span>
                            </p>
                        </div><!-- .group -->

                        <div class="group">
                            <p class="entries cleaned">
                                <label class="label">Sport:<span class="star">*</span></label>
                                <?php
                                echo CHtml::getSelect('team[sport_id]', SportDAO::getSelectList(), (isset($_POST['team']['sport_id']) ? $_POST['team']['sport_id'] : ($tmp->sport_id ? $tmp->sport_id : $logged_user->sport)), '');
                                ?>
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="category">Kategorie:</label>
                                <input class="input text" type="text" id="category" name="team[category]" value="<?=(isset($_POST['team']['category']) ? $_POST['team']['category'] : $tmp->category); ?>" />
                                <span class="help"><!-- --></span><span class="tooltip">Kategorie týmu.<br />Např. Muži, Ženy...</span>
                            </p>
                        </div><!-- .group -->

                        <div class="group">
                            <p class="entries cleaned">
                                <label class="label" for="competition">Soutěž:</label>
                                <input class="input text" type="text" id="competition" name="team[league]" value="<?=(isset($_POST['team']['league']) ? $_POST['team']['league'] : $tmp->league); ?>" />
                                <span class="help"><!-- --></span><span class="tooltip">Hlavní soutěž, které se váš tým účastní</span>
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="competitionweb">Web soutěže:</label>
                                <input class="input text" type="text" id="competitionweb" name="team[league_web]" value="<?=(isset($_POST['team']['league_web']) ? $_POST['team']['league_web'] : $tmp->league_web); ?>" />
                                <span class="help"><!-- --></span><span class="tooltip">Webové stránky soutěže</span>
                            </p>
                        </div><!-- .group -->

                        <div class="group">
                            <p class="entries place selectOne cleaned">
                                <label class="label">Město:<span class="star">*</span></label>
                                <? /*
                                <input class="input text" type="text" id="location" name="team[city]" value="<?=(isset($_POST['team']['city']) ? $_POST['team']['city'] : $tmp->city); ?>" />
                                <input type="hidden" id="locationID" name="team[cityID]" value="" />
                                */ ?>
                                <select id="locationID" name="team[locationID]"></select>
                                <input type="hidden" id="location" name="team[city]" value="<?=(!empty($_POST['team']['city']) ? $_POST['team']['city'] : $tmp->city)?>" data-id="<?=(!empty($_POST['team']['locationID']) ? $_POST['team']['locationID'] : $tmp->locationID)?>" />
                                <span class="help"><!-- --></span><span class="tooltip">Domovské město vašeho týmu</span>
                            </p>
                        </div><!-- .group -->

                        <div class="group">
                            <p class="entries cleaned">
                                <label class="label" for="established">Rok založení týmu:</label>
                                <input class="input text" type="text" id="established" name="team[founded]" value="<?=(isset($_POST['team']['founded']) ? $_POST['team']['founded'] : $tmp->founded); ?>" />
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="clubcolors">Týmové barvy:</label>
                                <input class="input text" type="text" id="clubcolors" name="team[colors]" value="<?=(isset($_POST['team']['colors']) ? $_POST['team']['colors'] : $tmp->colors); ?>" />
                            </p>
                        </div><!-- .group -->

                        <div class="group">
                            <p class="entries cleaned">
                                <label class="label" for="contactperson">Kontaktní osoba:<span class="star">*</span></label>
                                <input class="input text" type="text" id="contactperson" name="team[contact_name]" value="<?=(isset($_POST['team']['contact_name']) ? $_POST['team']['contact_name'] : $tmp->contact_name); ?>" />
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="email">E-mail:<span class="star">*</span></label>
                                <input class="input text" type="text" id="email" name="team[contact_email]" value="<?=(isset($_POST['team']['contact_email']) ? $_POST['team']['contact_email'] : $tmp->contact_email); ?>" />
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="phone-number">Tel:</label>
                                <input class="input text" type="text" id="phone-number" name="team[contact_phone]" value="<?=(isset($_POST['team']['contact_phone']) ? $_POST['team']['contact_phone'] : $tmp->contact_phone); ?>" />
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="clubweb">Týmové stránky:</label>
                                <input class="input text" type="text" id="clubweb" name="team[web]" value="<?=(isset($_POST['team']['web']) ? $_POST['team']['web'] : $tmp->web); ?>" />
                            </p>
                        </div><!-- .group -->
                    </div><!-- .right -->

                </div><!-- .inner -->

                <div class="navigation">
                    <ul>
                        <li class="first current"><a href="#teamphoto">Týmová fotografie</a></li>
                        <li><a href="#modules">Moduly</a></li>
                        <?php
                        if($action != 'team-registration' && ($action == 'team-registration' || $tmp->isAdmin($logged_user)))
                        {
                        ?>
                        <li><a href="#teamsettings">Další nastavení</a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div><!-- .navigation -->



                <div class="tabs">

                <!-- BOX 1 -->

                <div id="teamphoto" class="innerBox">

                <p class="title-noborder">
                    <strong>Týmová fotografie</strong><small> - nahrejte týmovou fotografii, minimální šířka musí být 550px.</small>
                </p>

                <p class="inputTeamPhoto">
                    <input name="teamPhoto" type="file" class="file" />
                </p>

                <p class="teamPhoto">
                    <img src="<?=$tmp->getTeamPhoto('big')?>" alt="<?=$tmp->getName()?>" />
                </p>

                <?php if($action == 'registration') { ?>

                <p class="buttonBox">
                    <button class="button buttonA" type="submit">dokončit registraci</button>
                </p>

                <?php } else { ?>

                <p class="buttonBox">
                    <button class="button buttonA" type="submit">uložit nastavení</button>
                </p>

                <?php } ?>

                </div>

                <!-- BOX 2 -->

                <div id="modules" class="innerBox">

                <p class="title-noborder">
                    <strong>Moduly</strong><small> - vyberte si, jaké moduly bude váš tým používat, které uvidí jen členové týmu a které všichni uživatelé.</small>
                </p>
                <?php
                $team_modules = $tmp->getModules();

                foreach($global_modules as $module_key => $module)
                {
                    $disabled = '';
                    // pokud mame statistiky nebo dochazku
                    if($module_key == 'stats' || $module_key == 'attendance')
                    {
                        // pokud ma vypnute udalosti, tak musime disablovat
                        if($team_modules['events'] == 'no')
                        {
                            $disabled = ' disabled="disabled"';
                        }
                    }
                ?>
                <p class="entries cleaned">
                    <label class="label"><?=$module['name']?></label>
                    <select data-name="<?=$module_key?>" name="module[<?=$module_key?>]"<?=$disabled?>>
                        <?php
                        foreach($module['values'] as $key => $val)
                        {
                            echo '<option value="'.$key.'"'.($key == @$team_modules[$module_key] ? ' selected="selected"' : '').'>'.$val.'</option>';
                        }
                        ?>
                    </select>
                    <?php
                    if(!empty($module['help']))
                    {
                        echo '<span class="help"><!-- --></span><span class="tooltip">'.$module['help'].'</span>';
                    }
                    ?>
                </p>
                <?php
                }
                ?>


                <?php if($action == 'registration') { ?>

                <p class="buttonBox">
                    <button class="button buttonA" type="submit">dokončit registraci</button>
                </p>

                <?php } else { ?>

                <p class="buttonBox">
                    <button class="button buttonA" type="submit">uložit nastavení</button>
                </p>

                <?php } ?>

                </div>

        <?php
        if($team->isAdmin($logged_user))
        {
        ?>
                <!-- BOX 3 -->

                <div id="teamsettings" class="innerBox">

                <p class="title-noborder">
                    <strong>Vlastnosti týmu</strong><small> - vyberte, jaké volitelné vlastnosti vašeho týmu se budou zobrazovat ostatním uživatelům.</small>
                </p>

                <p class="entries admins cleaned">
                    <label class="label">Žádost o vstup</label>
                    <select name="team[show_request]">
                        <option value="1">Zobrazit všem uživatelům</option>
                        <option value="0"<?=(empty($team->show_request) ? ' selected="selected"' : '')?>>Nezobrazovat nikomu</option>
                    </select>
                    <span class="help"><!-- --></span><span class="tooltip">Zobrazuje / skrývá ostatním uživatelům tlačítko zažádat o vstup do vašeho týmu.</span>
                </p>

                <p class="title-border">
                    <strong>Nastavení práv</strong><small> - nastavte, kdo z týmu bude kromě vás moci nastavovat moduly a upravovat tým.</small>
                </p>

                <p class="entries admins cleaned">
                    <label class="label">Majitel týmu</label>
                    <select id="teamOwner" name="teamOwner" <?=($team->user_id != $logged_user->id ? ' disabled="disabled"' : '')?>>
                        <?php
                        foreach ($team->getAdmins(true) as $admin_id)
                        {
                            $admin = UserDAO::get($admin_id);
                            echo '<option'.($team->user_id == $admin_id ? ' selected="selected"' : '').' value="'.$admin_id.'-'.getRecipientHash($admin_id).'">'.$admin->getName().'</option>';
                        }
                        ?>
                    </select>
                    <a href="#adminsEdit" class="button buttonC small icon edit popUp" id="editAdmins">editovat adminy</a>
                    <span class="help"><!-- --></span><span class="tooltip">Nastavte adminy, kteří se mohou starat o váš tým. Majitele týmu může změnit pouze aktuální majitel.</span>
                </p>

                <p class="title-border">
                    <strong>Nastavení sezón</strong><small> - vytvořte a uprave sezóny, kterých se váš tým účastní.</small>
                </p>

                <p class="entries season cleaned">
                    <label class="label">Hlavní sezóna</label>
                    <?php
                    $team_seasons = $tmp->getSeasons();
                    if(count($team_seasons))
                    {
                    ?>
                        <select id="activeSeason" name="activeSeason">
                            <?php
                            echo $options = print_season_select($team_seasons, $tmp->getActiveSeason());
                            ?>
                        </select>
                        <a href="#seasonEdit" class="button buttonC small icon edit popUp" id="addSeasonButton">editovat sezóny</a>
                    <?php
                    }
                    else
                    {
                    ?>
                        <a href="#seasonEdit" class="button buttonC small icon edit popUp" id="addSeasonButton">přidat sezónu</a>
                    <?php
                    }
                    ?>
                    <span class="help"><!-- --></span><span class="tooltip">Hlavní sezóna bude jako výchozí volba při vytváření nebo přijímání událostí.</span>
                </p>


                <?php
                $fakes = $reals = array();
                $players = $team->getPlayers();
                foreach($players as $key => $player_id)
                {
                    $player = UserDAO::get($player_id);
                    if($player->isFakeUser())
                    {
                        $fakes[] = $player;
                    }
                    else
                    {
                        $reals[] = $player;
                    }
                }

                if(count($fakes) > 0)
                {
                ?>

                <p class="title-border">
                    <strong>Spárování účtů</strong><small> - převeďte statistiky z fiktivního uživatelského účtu hostujícího hráče na jeho pravý účet.</small>
                </p>

                <p class="entries mergeStats cleaned">
                    <label class="label">Převést statistiky z</label>
                    <select class="from">
                    <?php
                    foreach($fakes as $player)
                    {
                        echo '<option value="'.$player->id.'-'.getRecipientHash($player->id).'">'.$player->getName().'</option>';
                    }
                    ?>
                    </select>
                    <span class="separator">na</span>
                    <select class="to">
                    <?php
                    foreach($reals as $player)
                    {
                        echo '<option value="'.$player->id.'-'.getRecipientHash($player->id).'">'.$player->getName().'</option>';
                    }
                    ?>
                    </select>
                    <a href="#" class="button buttonC small" id="mergeStats">převést</a>
                </p>

                <?php
                }


                 /*
                <p class="title-noborder">
                    <strong>Další nastavení týmu</strong>
                </p>

                <p class="entries cleaned">
                    <label class="label">Přátelská utkání</label>
                    <select>
                        <option>Aktivní</option>
                        <option>Neaktivní</option>
                    </select>
                    <span class="help"><!-- --></span><span class="tooltip">Aktivní volba umožněje jiným týmům zaslat nabídku na přátelské utkání</span>
                </p> */ ?>

                <?php if($action == 'registration') { ?>

                <p class="buttonBox">
                    <button class="button buttonA" type="submit">dokončit registraci</button>
                </p>

                <?php } else { ?>

                <p class="buttonBox">
                    <button class="button buttonA" type="submit">uložit nastavení</button>
                </p>

                <?php } ?>

                </div>
        <?php
        }
        ?>
            </div>
        </form>

        <div class="forBlind">
            <div id="adminsEdit" class="fancybox-popup">
                <h3 class="title">Editovat adminy týmu</h3>

                <div id="popUpEditAdmins" class="popUpForm">
                    <?php
                    foreach ($team->getAdmins() as $admin_id)
                    {
                        $admin = UserDAO::get($admin_id);

                    ?>
                        <p data-id="<?=$admin_id?>-<?=getRecipientHash($admin_id)?>" class="entries cleaned">
                            <label class="adminName"><?=$admin->getName()?></label>
                            <input type="text" name="" class="text forBlind">
                            <span class="deleteBox controls">
                                <a title="Smazat admina" class="delete icon delete" href="#"></a>
                            </span>
                        </p>
                    <?php
                    }
                    ?>
                    <p class="link addAdmin">
                        <a class="button buttonB small icon add"><?=$tr->tr('přidat dalšího admina')?></a>
                    </p>
                </div>

                <div class="playersOptions hide">
                    <select>
                        <?php
                        foreach ($team->getPlayers() as $admin_id)
                        {
                            $admin = UserDAO::get($admin_id);

                            if($admin->isFakeUser())
                            {
                                continue;
                            }

                            echo '<option value="'.$admin_id.'-'.getRecipientHash($admin_id).'">'.$admin->getName().'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div><!-- #adminsEdit -->

            <div id="seasonEdit" class="fancybox-popup">
                <h3 class="title">Editovat sezóny</h3>

                <div id="popUpEditSeason" class="popUpForm">
                <?php
                    $team_seasons = $tmp->getSeasons();
                    echo print_edit_seasons($team_seasons);
                ?>
                    <p class="link addSeason">
                        <a class="button buttonB small icon add"><?=$tr->tr('přidat další')?></a>
                    </p>
                </div>
            </div><!-- #seasonEdit -->
        </div>

    </div><!-- .contentAbox -->
</div><!-- #contentA .registration -->


<?php /*
<script type="text/javascript">
/************************************************************
    Jcrop - orez obrazku - zavolat pro orez obrazku
************************************************************ /
function ajaxCrop(){

    $('#team_picture_big').html('');
    $('<span id="loading"><!-- --></span>').appendTo('#team_picture_big');

    $.ajax({
        url: "/team_ajax.php",
        type: "get",
        data: "crop=1&x="+$("#x").val()+"&y="+$("#y").val()+"&width="+$("#w").val()+'&page=' + window.location.pathname.replace(/\//g,''),
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (res) {

            if(res.status == "ok")
            {
                $("#team_picture_big").html(res.img_big); // tim se prepise loading
                var re1 = /width="\d+" height="\d+"/;
                var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                $(".team_picture_medium").html(res.img_medium);
                if(!res.serialized)
                {
                    $(".team_picture_small").html(res.img_small);
                }
                else
                {
                    $("#tmpImages").val(res.serialized);
                }
                $(".team_picture_main").html(res.img_original.replace(re1,replace));
                $('#mainPhoto').Jcrop({
                    onChange:       showPreview,
                    onSelect:       showPreview,
                    setSelect:      [ 50, 50, 250, 250 ],
                    minSize:        [ 200, 200 ],
                    aspectRatio:    1
                });
            }
            else
            {
                alert('Nastala chyba při resizu obrázku.');
            }

        },
        complete: function(){

            if ( $('#team-registration').length )
            {
                var messagePlace = $('#contentA');

                messagePlace.find('.msg:not(.noRemove)').remove();
                messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Logo týmu bylo úspěšně změněno.</p></div>');
            }
            
        }
    });
}


$('#deleteProfilePhoto').live('click', function() {

    // confirm na smazani zpravy
    $.confirm({
        'title'     : 'Potvrdit smazání loga',
        'message'   : 'Opravdu si přejete smazat vaše týmové logo?',
        'buttons'   : {
            'potvrdit'  : {
                'class' : 'buttonA icon confirm',
                'action': function(){

                    $('#team_picture_big').html('');
                    $('<span id="loading"><!-- --></span>').appendTo('#picture_big');

                    $.ajax({
                        url: '/team_ajax.php',
                        type: 'GET',
                        data: 'delete_profile_photo=1&page=' + window.location.pathname.replace(/\//g,''),
                        dataType: 'json',
                        success: function (res) {
                            if(res.status == 'ok')
                            {
                                $("#team_picture_big").html(res.img_big);
                                var re1 = /width="\d+" height="\d+"/;
                                var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                                $(".team_picture_medium").html(res.img_medium);
                                if(!res.serialized)
                                {
                                    $(".team_picture_small").html(res.img_small);
                                }
                                else
                                {
                                    $("#tmpImages").val(res.serialized);
                                }
                                $(".team_picture_main").html(res.img_original.replace(re1,replace));
                            }
                            else
                            {
                                alert('Nastala chyba při mazání obrázku.');
                            }
                        },
                        complete: function(){

                            var messagePlace    = $('#contentA'),
                                controlsPlace   = $('#contentA .inner .left .list');

                            controlsPlace.find('.edit').remove();
                            controlsPlace.find('.delete').remove();
                            messagePlace.find('.msg:not(.noRemove)').remove();
                            messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně smazána.</p></div>');
                        }
                    });

                }
            },
            'zrušit'    : {
                'class' : 'buttonA',
                'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
            }
        }
    });

    return false;
});


var input = document.getElementById("uploadPhoto");
var formdata = false;

input.addEventListener("change", function (evt) {
	var i = 0, len = this.files.length, img, reader, file;

	//for ( ; i < len; i++ ) {
		file = this.files[i];

		if (!!file.type.match(/image.* /)) {
            if (window.FormData) {
            	formdata = new FormData();
            	//document.getElementById("btn").style.display = "none";
            }
			if ( window.FileReader ) {
				reader = new FileReader();
				reader.onloadend = function (e) {
					//showUploadedItem(e.target.result, file.fileName);
				};
				reader.readAsDataURL(file);
			}
			if (formdata) {
				formdata.append("image", file);
			}
		}
	//}

	if (formdata) {

        $('#team_picture_big').html('');
        $('<span id="loading"><!-- --></span>').appendTo('#team_picture_big');

		$.ajax({
            url: "/team_ajax.php?file=" + window.location.pathname.replace(/\//g,''),
			type: "POST",
			data: formdata,
            dataType: 'json',
			processData: false,
			contentType: false,
			success: function (res) {

                if(res.status == "ok")
                {
                    $("#team_picture_big").html(res.img_big);
                    var re1 = /width="\d+" height="\d+"/;
                    var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                    //$(".picture_medium").html(res.img_medium);
                    if(!res.serialized)
                    {
                        $(".team_picture_small").html(res.img_small);
                    }
                    else
                    {
                        $("#tmpImages").val(res.serialized);
                    }
                    $(".team_picture_main").html(res.img_original.replace(re1,replace));
                    $('#mainPhoto').Jcrop({
                        onChange:       showPreview,
                        onSelect:       showPreview,
                        setSelect:      [ 50, 50, 250, 250 ],
                        minSize:        [ 200, 200 ],
                        aspectRatio:    1
                    });
                    //location.reload();

                    // nepouziva se - moznost menit pouze SRC vsech obrazku a ne generovat cely tag img
                    /*$("#picture_big img").attr('src',res.img_big);
                    $(".picture_medium img").attr('src',res.img_medium);
                    $(".picture_small img").attr('src',res.img_small);
                    $(".picture_big img").attr('src',res.img_big);
                    var re1 = /height="\d+"/;
                    var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                    $(".picture_original img").attr('src',res.img_original.replace(re1,''));

                    $(".picture_main img").attr('src',res.img_original.replace(re1,replace));* /
                }
                else
                {
                    alert('Nastala chyba při ukládání obrázku.');
                }
			},
            complete: function(){

                var messagePlace    = $('#contentA'),
                    controlsPlace   = $('#contentA .inner .left .list');

                controlsPlace.find('.edit').remove();
                controlsPlace.find('.delete').remove();
                messagePlace.find('.msg:not(.noRemove)').remove();
                messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně změněna.</p></div>');
                controlsPlace.append('<li class="item edit">' +
                                            '<a title="Upravit vřez profilové fotografie" class="popUpBig" href="#editProfilePhoto">Upravit výřez fotografie</a>' +
                                        '</li>' +
                                        '<li class="item delete">' +
                                            '<a title="Smazat profilovou fotografie" id="deleteProfilePhoto" href="#">Smazat profilovou fotografii</a>' +
                                        '</li>');
            }
		});
	}
}, false);


</script>
*/ ?>