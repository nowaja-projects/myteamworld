<div id="contentA" class="module">

    <div class="contentAbox" >    

            <h2 class="headline">Výsledky vyhledávání</h2>
            <?php
            if(empty($_GET['search']))
            {
                echo '<div class="cleaned">'.$tr->tr('Zadejte prosím výraz k vyhledávání').'</div>';
            }
            elseif(($team_count + $user_count) == 0)
            {
                echo '<div class="cleaned">'.$tr->tr('Vašemu dotazu neodpovídá žádný výsledek.').'</div>';
            }
            else
            {
            ?>
                <div class="results teams">
                    <div class="title cleaned">
                        <strong>
                            <span class="hidden-xxs">Nalezené týmy</span>
                            <span class="visible-xxs-inline">Týmy</span>
                        </strong>
                        <span class="info">celkem <?=decline($team_count, 'byl', 'byly', 'bylo')?> <?=decline($team_count, 'nalezen', 'nalezeny', 'nalezeno')?> <strong><?=$team_count?> <?=decline($team_count, 'tým', 'týmy', 'týmů')?></strong></span>
                    </div><!-- .title -->
                
                <?php
                echo print_searched_teams($searched_teams, $logged_user, $view_more_teams);
                ?>
                </div>
                
                <div class="results users">
                    <div class="title users cleaned">
                        <strong>
                            <span class="hidden-xxs">Nalezení uživatelé</span>
                            <span class="visible-xxs-inline">Uživatelé</span>
                        </strong>
                        <span class="info">celkem <?=decline($user_count, 'byl', 'byli', 'bylo')?> <?=decline($user_count, 'nalezen', 'nalezeni', 'nalezeno')?> <strong><?=$user_count?> <?=decline($user_count, 'uživatel', 'uživatelé', 'uživatelů')?></strong></span>
                    </div><!-- .title -->
                <?php
                echo print_searched_users($searched_users, $logged_user, $view_more_users);
                ?>
                </div>
            <?php
            }
            ?>

    </div><!-- .contentAbox -->


    <?php /* TODO - toto nemuze byt na strance pokud nejsou zadni uzivatele kterym muzu napsat nebo jsem tam jen ja, oifovat */ ?>

    <div class="forBlind">
        <div id="sendNewMessage" class="fancybox-popup">
            <h3 class="title">Poslat novou zprávu</h3>

            <form id="popUpFormSendNewMessage" class="popUpForm" action="" method="post">
                <fieldset>
                    <legend class="forBlind">Poslat novou zprávu</legend>

                    <p class="entries first">
                        <label for="messageRecipients">Příjemci zprávy</label><br /> 
                        <input id="messageRecipients" type="text" disabled="disabled" value="" />
                        <input id="messageRecipientsID" type="hidden" name="pm[to][]" value="" />
                    </p>

                    <p class="entries">
                        <label for="messageText">Text zprávy</label><br />
                        <textarea id="messageText" rows="10" cols="5" name="pm[text]"></textarea>
                    </p>

                    <p class="entries submit">
                        <button type="submit">poslat zprávu</button>
                    </p>
                </fieldset>
            </form>
        </div><!-- #sendNewMessage -->
    </div>

</div><!-- #contentAbox .module -->