                <div id="contentA" class="registration">

                    <?php if($action == 'registration') { ?>
                    <div class="msg warning noRemove noClose">
                        <h3 class="title">
                            Váš e-mail byl úspěšně potvrzen!
                        </h3>
                        <p class="text">
                            Vyplňte prosím všechny povinné položky, nastavte si účet a dokončete vaši registraci.
                        </p>
                    </div>
                    <?php }

                    include(BLOCK_PATH.'messages.php');
                    ?>

                    <div class="contentAbox">
                        <form method="post" action="" enctype="multipart/form-data">

                            <?php if($action == 'registration') { ?>

                            <h2 class="headline">Dokončení registrace uživatele</h2>

                            <?php } else { ?>

                            <h2 class="headline">Upravit profil uživatele</h2>

                            <?php } ?>

                            <div class="inner cleaned">

                                <div class="left">
                                    <p id="picture_big" class="picture">
                                        <img src="<?=$logged_user->getUserImage('big')?>" alt="<?php echo (isset($_POST['profile']['fname']) ? $_POST['profile']['fname'] : $logged_user->fname) .' ' . (isset($_POST['profile']['sname']) ? $_POST['profile']['sname'] : $logged_user->sname);?>" width="200" height="200" />
                                    </p>
                                    <ul class="list">
                                        <li class="item upload">
                                            <a href="#" title="Nahrát novou profilovou fotografii">
                                                <span class="hidden-sm hidden-xs hidden-xxs">Nahrát profilovou fotografii</span>
                                                <span class="visible-sm-inline visible-xs-inline visible-xxs-inline">Nahrát fotografii</span>
                                            </a>
                                        </li>
                                        <?php
                                        $show = $logged_user->hasImage();
                                        if($show)
                                        {
                                        ?>
                                        <li class="item edit hidden-xs hidden-xxs">
                                            <a href="#editProfilePhoto" class="popUpBig" title="Upravit výřez profilové fotografie">
                                                <span class="hidden-sm hidden-xs hidden-xxs">Upravit výřez fotografie</span>
                                                <span class="visible-sm-inline visible-xs-inline visible-xxs-inline">Upravit výřez</span>
                                            </a>
                                        </li>
                                        <li class="item delete">
                                            <a href="#" id="deleteProfilePhoto" title="Smazat profilovou fotografie">
                                                <span class="hidden-sm hidden-xs hidden-xxs">Smazat profilovou fotografii</span>
                                                <span class="visible-sm-inline visible-xs-inline visible-xxs-inline">Smazat fotografii</span>
                                            </a>
                                        </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>

                                    <p class="forBlind">
                                        <input id="uploadPhoto" type="file" name="image" />

                                        <input id="x" type="text" name="x" />
                                        <input id="y" type="text" name="y" />
                                        <input id="w" type="text" name="w" />
                                        <input id="h" type="text" name="h" />

                                    </p>

                                    <div class="forBlind">
                                        <div id="editProfilePhoto" class="fancybox-popup">

                                            <h3 class="title">Upravit výřez fotografie</h3>

                                            <div class="containerBox cleaned">
                                                <p class="mainPhoto">
                                                    <span class="infoTitle">Vyberte výřez vaší fotografie:</span>
                                                    <span class="infoText">Kliknutím a tažením myši na vaší profilové fotografii vyberte výřez, který chcete používat na celém webu.</span>
                                                    <span class="crop picture_main">
                                                        <img id="mainPhoto" src="<?=$logged_user->getUserImage('original')?>" alt="Hlavní fotografie" width="<?=MAX_ORIGINAL_WIDTH?>" />
                                                    </span>
                                                    <span class="infoHelp">Náhledy miniatur, které se používají například v profilu, komentářích nebo soupisce vidíte v pravo.</span>
                                                </p>

                                                <p class="profilePhoto">
                                                    <span class="crop picture_main">
                                                        <img id="profilePhoto" src="<?=$logged_user->getUserImage('original')?>" alt="" />
                                                    </span>
                                                </p>

                                                <p class="commentPhoto">
                                                    <span class="crop picture_main">
                                                        <img id="commentPhoto" src="<?=$logged_user->getUserImage('original')?>" alt="" />
                                                    </span>
                                                </p>

                                                <p class="teamPhoto">
                                                    <span class="crop picture_main">
                                                        <img id="teamPhoto" src="<?=$logged_user->getUserImage('original')?>" alt="" />
                                                    </span>
                                                </p>

                                                <p class="controls">
                                                    <a id="saveCropFoto" class="close" href="#" title="Uložit výřez a zavřít okno">uložit výřez fotografie</a>
                                                </p>

                                            </div><!-- .container -->

                                        </div><!-- .fancybox-popup -->
                                    </div>
                                </div><!-- .left -->

                                <div class="right">
                                    <div class="group">
                                        <p class="entries cleaned">
                                            <label class="label" for="sex">Pohlaví:<span class="star">*</span></label>
                                            <select name="profile[sex]">
                                                <option value="male">Muž</option>
                                                <option value="female"<?=(isset($_POST['profile']['sex']) && $_POST['profile']['sex'] == 'female' ? ' selected="selected"' :'')?>>Žena</option>
                                            </select>
                                        </p>

                                        <p class="entries cleaned">
                                            <label class="label" for="firstname">Jméno:<span class="star">*</span></label>
                                            <input class="input text" type="text" id="firstname" name="profile[fname]" value="<?php echo (isset($_POST['profile']['fname']) ? $_POST['profile']['fname'] : $logged_user->fname); ?>" />
                                        </p>

                                        <p class="entries cleaned">
                                            <label class="label" for="surname">Příjmení:<span class="star">*</span></label>
                                            <input class="input text" type="text" id="surname" name="profile[sname]" value="<?php echo (isset($_POST['profile']['sname']) ? $_POST['profile']['sname'] : $logged_user->sname); ?>" />
                                        </p>
                                    </div><!-- .group -->

                                    <div class="group">
                                        <p class="entries cleaned">
                                            <label class="label">Můj hlavní sport:<span class="star">*</span></label>
                                            <?php
        		                            echo CHtml::getSelect('profile[sport]', SportDAO::getSelectList(), (isset($_POST['profile']['sport']) ? $_POST['profile']['sport'] : $logged_user->sport), '');
        		                            ?>
                                        </p>

                                        <?php
                                        if(count($logged_user->teams) > 0)
                                        {
                                        ?>
                                        <p class="entries cleaned">
                                            <label class="label">Můj hlavní tým:<span class="star">*</span></label>
                                            <?php
                                            foreach($logged_user->teams as $steam)
                                            {
                                                $select[$steam->id] = $steam->getName();
                                            }
        		                            echo CHtml::getSelect('profile[team]', $select, (isset($_POST['profile']['team']) ? $_POST['profile']['team'] : $logged_user->team), '');
        		                            ?>
                                            <span class="help"><!-- --></span><span class="tooltip">Vybraný tým je po přihlášení nastaven jako aktivní, ve veřejných diskuzích bude uveden za vaším jménem.</span>
                                        </p>
                                        <?php
                                        }
                                        ?>
                                    </div><!-- .group -->

                                    <div class="group">
                                        <p class="entries cleaned">
                                            <label class="label" for="height">Výška:</label>
                                            <input class="input text small" maxlength="3" type="text" id="height" name="profile[height]" value="<?php echo (isset($_POST['profile']['height']) ? $_POST['profile']['height'] : $logged_user->height); ?>" /> cm
                                            <span class="help"><!-- --></span><span class="tooltip">Vhodné, pokud chcete vstoupit do týmu</span>
                                        </p>

                                        <p class="entries cleaned">
                                            <label class="label" for="weight">Váha:</label>
                                            <input class="input text small" maxlength="3" type="text" id="weight" name="profile[weight]" value="<?php echo (isset($_POST['profile']['weight']) ? $_POST['profile']['weight'] : $logged_user->weight); ?>" /> kg
                                            <span class="help"><!-- --></span><span class="tooltip">Vhodné, pokud chcete vstoupit do týmu</span>
                                        </p>
                                    </div><!-- .group -->

                                    <div class="group">
                                        <p class="entries cleaned">
                                            <label class="label">Datum narození:<span class="star">*</span></label>
                                            <?php
                                            $array = array(0 => '--');
                                            for($i = 1;$i <= 31; $i++)
                                            {
                                                $array[$i] = $i;
                                            }

                                            if(isset($postbirth['j']))
                                            {
                                                $date = $postbirth['j'];
                                            }
                                            elseif($logged_user->birth != '0000-00-00')
                                            {
                                                $date = date('j', strtotime($logged_user->birth));
                                            }
                                            else
                                            {
                                                $date = 0;
                                            }
                                            echo CHtml::getSelect('profile[birth][j]', $array, $date, 'class="day"');
                                            ?>

                                            <?php
                                            $array = array(0 => '--');
                                            for($i = 1;$i <= 12; $i++)
                                            {
                                                $array[$i] = $i;
                                            }
                                            if(isset($postbirth['n']))
                                            {
                                                $date = $postbirth['n'];
                                            }
                                            elseif($logged_user->birth != '0000-00-00')
                                            {
                                                $date = date('n', strtotime($logged_user->birth));
                                            }
                                            else
                                            {
                                                $date = 0;
                                            }

                                            echo CHtml::getSelect('profile[birth][n]', $array, $date, 'class="month"');
                                            ?>

                                            <?php
                                            $array = array(0 => '----');
                                            for($i = date('Y');$i >= 1910; $i--)
                                            {
                                                $array[$i] = $i;
                                            }
                                            if(isset($postbirth['Y']))
                                            {
                                                $date = $postbirth['Y'];
                                            }
                                            elseif($logged_user->birth != '0000-00-00')
                                            {
                                                $date = date('Y', strtotime($logged_user->birth));
                                            }
                                            else
                                            {
                                                $date = 0;
                                            }
                                            echo CHtml::getSelect('profile[birth][Y]', $array, $date, 'class="year"');
                                            ?>
                                        </p>

                                        <p class="entries place selectOne cleaned">
                                            <label class="label">Místo pobytu:<span class="star">*</span></label>
                                            <?php /*
                                            <input type="hidden" id="locationID" name="profile[locationID]" value="<?php echo (isset($_POST['profile']['locationID']) ? $_POST['profile']['locationID'] : $logged_user->locationID);?>" />
                                            <input class="input text" type="text" id="location" name="profile[city]" value="<?php echo (isset($_POST['profile']['city']) ? $_POST['profile']['city'] : $logged_user->city);?>" />
                                            */ ?>
                                            <select id="locationID" name="profile[locationID]"></select>
                                            <input type="hidden" id="location" name="profile[city]" value="<?=(!empty($_POST['profile']['city']) ? $_POST['profile']['city'] : $logged_user->city)?>" data-id="<?=(!empty($_POST['profile']['locationID']) ? $_POST['profile']['locationID'] : $logged_user->locationID)?>" />
                                            <span class="help"><!-- --></span><span class="tooltip">Místo, kde se najčastěji nacházíte</span>
                                        </p>

                                        <?php /*<label class="label">Kraj:<span class="star">*</span></label>
                                        <?php echo CHtml::getSelect('profile[region]', $_regions, (isset($_POST['profile']['region']) ? $_POST['profile']['region'] : $logged_user->region), '');?> */?>
                                    </div><!-- .group -->

                                    <div class="group">
                                        <p class="entries cleaned">
                                            <label class="label" for="email">E-mail:<span class="star">*</span></label>
                                            <input class="input text" type="text" id="email" name="profile[email]" value="<?php echo (isset($_POST['profile']['email']) ? $_POST['profile']['email'] : $logged_user->email); ?>" />
                                            <span class="help"><!-- --></span><span class="tooltip">Můžete si nastavit, komu se bude kontakt zobrazovat</span>
                                        </p>

                                        <p class="entries cleaned">
                                            <label class="label" for="icq">ICQ:</label>
                                            <input class="input text" type="text" id="icq" name="profile[icq]" value="<?php echo (isset($_POST['profile']['icq']) ? $_POST['profile']['icq'] : $logged_user->icq); ?>" />
                                            <span class="help"><!-- --></span><span class="tooltip">Můžete si nastavit, komu se bude kontakt zobrazovat</span>
                                        </p>

                                        <p class="entries cleaned">
                                            <label class="label" for="phone-number">Tel:</label>
                                            <input class="input text" type="text" id="phone-number" name="profile[phone]" value="<?php echo (isset($_POST['profile']['phone']) ? $_POST['profile']['phone'] : $logged_user->phone); ?>" />
                                            <span class="help"><!-- --></span><span class="tooltip">Můžete si nastavit, komu se bude kontakt zobrazovat</span>
                                        </p>

                                        <p class="entries cleaned">
                                            <label class="label" for="www">www:</label>
                                            <input class="input text" type="text" id="www" name="profile[web]" value="<?php echo (isset($_POST['profile']['web']) ? $_POST['profile']['web'] : $logged_user->web); ?>" />
                                        </p>
                                    </div><!-- .group -->
                                </div><!-- .right -->

                            </div><!-- .inner -->

                            <div class="navigation">
                                <ul>
                                    <li class="first current">
                                        <a href="#messageboardsetting">
                                            <span class="hidden-xxs">Nastavení nástěnky</span>
                                            <span class="visible-xxs">Nástěnka</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#privacysetting">
                                            <span class="hidden-xxs">Nastavení soukromí</span>
                                            <span class="visible-xxs">Soukromí</span>
                                        </a>
                                    </li>
                                    <?php if($action != 'registration') { ?>
                                    <li>
                                        <a href="#profilesetting">
                                            <span class="hidden-xxs">Nastavení účtu</span>
                                            <span class="visible-xxs">Účet</span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div><!-- .navigation -->



                            <div class="tabs">

                            <!-- BOX 1 -->

                            <div id="messageboardsetting" class="innerBox">

                            <p class="title-noborder">
                                <strong>Aktuality</strong><small> - nastavte si, z jakých sportů chcete na své nástěnce zobrazovat aktuality.</small>
                            </p>

                            <?php
                            $sports = SportDAO::getListWithRSS();
                            $sources = $logged_user->getRssSources();
                            $selectedSport = false;
                            if($action == 'registration')
                            {
                                $selectedSport = (isset($_POST['profile']['sport']) ? $_POST['profile']['sport'] : $logged_user->sport);
                            }
                            foreach($sports as $id => $sport)
                            {
                                if(isset($sport['branches']))
                                {
                                ?>
                                    <p class="titleRss">
                                        <strong><?php echo $sport['name'];?></strong>
                                    </p>

                                    <ul class="checkboxes cleaned">
                                    <?php
                                    $i = 0;
                                    foreach($sport['branches'] as $key => $val)
                                    {
                                        $source = parse_url($val['link'], PHP_URL_HOST);
                                        $source = preg_replace("/^www\./", "", $source);
                                    ?>
                                        <li class="checkbox"><input type="checkbox" value="1" id="sources<?=$key?>" name="sources[<?=$key?>]" <?php if (((in_array($key, $sources) && !isset($_POST['sources'])) || (isset($_POST['sources']) && in_array($key, array_keys($_POST['sources'])))) || (!isset($_POST['profile']) && $id == $selectedSport)) echo ' checked="checked"';?> /><label for="sources<?php echo $key;?>"><?php echo $val['title'];?> <small>&nbsp;(<?=$source?>)</small></label></li>
                                    <?php
                                        $i++;
                                    }
                                    ?>
                                    </ul>
                                <?php
                                }
                            }
                            ?>

                            <?php if($action == 'registration') { ?>

                            <p class="buttonBox">
                                <button class="button buttonA" type="submit">dokončit registraci</button>
                            </p>

                            <?php } else { ?>

                            <p class="buttonBox">
                                <button class="button buttonA" type="submit">uložit nastavení</button>
                            </p>

                            <?php } ?>

                            </div>

                            <!-- BOX 2 -->

                            <div id="privacysetting" class="innerBox">

                            <?php
                            //echo CHtml::getSelect('profile[team]', $select, (isset($_POST['profile']['team']) ? $_POST['profile']['team'] : $logged_user->team), '');
                            $privacy = array(
                                UserSettings::VAL_SHOW_ALL => $tr->tr('Zobrazit všem uživatelům'),
                                UserSettings::VAL_SHOW_TEAMMATES => $tr->tr('Zobrazit pouze spoluhráčům'),
                                UserSettings::VAL_SHOW_NOBODY => $tr->tr('Nezobrazovat nikomu')
                            );
                            ?>

                            <p class="title-noborder">
                                <strong>Soukromí</strong><small> - nastavte si, komu budete zobrazovat vaše kontaktní informace.</small>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Datum narození</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_BIRTH.']', $privacy, (isset($_POST['privacy'][UserSettings::KEY_BIRTH]) ? $_POST['privacy'][UserSettings::KEY_BIRTH] : @$logged_user->settings[UserSettings::KEY_BIRTH]), '');
                                ?>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Místo pobytu</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_LOCATION.']', $privacy, (isset($_POST['privacy'][UserSettings::KEY_LOCATION]) ? $_POST['privacy'][UserSettings::KEY_LOCATION] : @$logged_user->settings[UserSettings::KEY_LOCATION]), '');
                                ?>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">E-mailová adresa</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_EMAIL.']', $privacy, (isset($_POST['privacy'][UserSettings::KEY_EMAIL]) ? $_POST['privacy'][UserSettings::KEY_EMAIL] : @$logged_user->settings[UserSettings::KEY_EMAIL]), '');
                                ?>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">ICQ</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_ICQ.']', $privacy, (isset($_POST['privacy'][UserSettings::KEY_ICQ]) ? $_POST['privacy'][UserSettings::KEY_ICQ] : @$logged_user->settings[UserSettings::KEY_ICQ]), '');
                                ?>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Telefonní číslo</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_PHONE.']', $privacy, (isset($_POST['privacy'][UserSettings::KEY_PHONE]) ? $_POST['privacy'][UserSettings::KEY_PHONE] : @$logged_user->settings[UserSettings::KEY_PHONE]), '');
                                ?>
                            </p>

                            <?php
                            $notices = array(
                                UserSettings::VAL_NOTICE_YES  => $tr->tr('Upozorňovat'),
                                UserSettings::VAL_NOTICE_NO   => $tr->tr('Neupozorňovat')
                            );
                            ?>
                            <p class="title-border">
                                <strong>Upozornění na myteamworld.com</strong><small> - nastavte si, kdy a jaká vám budou chodit upozornění na webu.</small>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Reakce k veřejné zprávě</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_PUBLIC_COMMENT.']', $notices, (isset($_POST['privacy'][UserSettings::KEY_PUBLIC_COMMENT]) ? $_POST['privacy'][UserSettings::KEY_PUBLIC_COMMENT] : @$logged_user->settings[UserSettings::KEY_PUBLIC_COMMENT]), '');
                                ?>
                            </p>

                            <p class="title-border">
                                <strong>Upozornění na e-mail</strong><small> - nastavte si, kdy a jaká vám budou chodit upozornění na e-mail.</small>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Týdenní souhrn událostí</label>
                                <?php
                                $notices = array(
                                    UserSettings::VAL_EMAIL_YES  => $tr->tr('Upozorňovat'),
                                    UserSettings::VAL_EMAIL_NO   => $tr->tr('Neupozorňovat')
                                );

                                echo CHtml::getSelect('privacy['.UserSettings::KEY_EMAIL_EVENT_WEEK.']', $notices, (isset($_POST['privacy'][UserSettings::KEY_EMAIL_EVENT_WEEK]) ? $_POST['privacy'][UserSettings::KEY_EMAIL_EVENT_WEEK] : @$logged_user->settings[UserSettings::KEY_EMAIL_EVENT_WEEK]), '');
                                ?>
                                </select>
                                <span class="help"><!-- --></span><span class="tooltip">Na začátku každého týdne budete upozorněni na události, kterých se tento týden máte zúčastnit</span>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Upozornění na událost</label>
                                <?php
                                $notices = array(
                                    UserSettings::VAL_EMAIL_YES  => $tr->tr('Upozorňovat'),
                                    UserSettings::VAL_EMAIL_NO   => $tr->tr('Neupozorňovat')
                                );

                                echo CHtml::getSelect('privacy['.UserSettings::KEY_EMAIL_EVENT_DAY.']', $notices, (isset($_POST['privacy'][UserSettings::KEY_EMAIL_EVENT_DAY]) ? $_POST['privacy'][UserSettings::KEY_EMAIL_EVENT_DAY] : @$logged_user->settings[UserSettings::KEY_EMAIL_EVENT_DAY]), '');
                                ?>
                                <span class="help"><!-- --></span><span class="tooltip">Den před událostí budete upozorněni, kdy a kde se koná, případně na doplnění docházky.</span>
                            </p>
                            <?php
                            /*
                            $email_notices = array(
                                UserSettings::VAL_NOTICE_2DAYS  => $tr->tr('2 dny před událostí'),
                                UserSettings::VAL_NOTICE_1DAY   => $tr->tr('1 den před událostí'),
                                UserSettings::VAL_NOTICE_CDAY   => $tr->tr('V den události'),
                                UserSettings::VAL_NOTICE_NO     => $tr->tr('Neupozorňovat')
                            );
                            ?>
                            <p class="title-border">
                                <strong>Upozornění na e-mail</strong><small> - nastavte si, kdy a jaká vám budou chodit upozornění na e-mail.</small>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Upozornění na zápas</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_EMAIL_MATCH.']', $email_notices, (isset($_POST['privacy'][UserSettings::KEY_EMAIL_MATCH]) ? $_POST['privacy'][UserSettings::KEY_EMAIL_MATCH] : @$logged_user->settings[UserSettings::KEY_EMAIL_MATCH]), '');
                                ?>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Upozornění na trénink</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_EMAIL_TRAINING.']', $email_notices, (isset($_POST['privacy'][UserSettings::KEY_EMAIL_TRAINING]) ? $_POST['privacy'][UserSettings::KEY_EMAIL_TRAINING] : @$logged_user->settings[UserSettings::KEY_EMAIL_TRAINING]), '');
                                ?>
                            </p>

                            <?php
                            $email_notices = array(
                                UserSettings::VAL_NOTICE_YES  => $tr->tr('Poslat upozornění'),
                                UserSettings::VAL_NOTICE_NO   => $tr->tr('Neposílat upozornění')
                            );
                            ?>
                            <p class="entries cleaned">
                                <label class="label">Akce k mému příspěvku</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_EMAIL_MY_ENTRY.']', $email_notices, (isset($_POST['privacy'][UserSettings::KEY_EMAIL_MY_ENTRY]) ? $_POST['privacy'][UserSettings::KEY_EMAIL_MY_ENTRY] : @$logged_user->settings[UserSettings::KEY_EMAIL_MY_ENTRY]), '');
                                ?>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Nová týmová zpráva</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_NEW_TEAM_NESSAGE.']', $email_notices, (isset($_POST['privacy'][UserSettings::KEY_NEW_TEAM_NESSAGE]) ? $_POST['privacy'][UserSettings::KEY_NEW_TEAM_NESSAGE] : @$logged_user->settings[UserSettings::KEY_NEW_TEAM_NESSAGE]), '');
                                ?>
                            </p>


                            <?php
                            */
                            $messages_notices = array(
                                UserSettings::VAL_MESSAGE_YES  => $tr->tr('Přijímat od všech uživatelů'),
                                UserSettings::VAL_MESSAGE_TEAM => $tr->tr('Přijímat pouze od spoluhráčů'),
                                UserSettings::VAL_MESSAGE_NO   => $tr->tr('Nepřijímat')
                            );
                            ?>
                            <p class="title-border">
                                <strong>Ostatní nastavení</strong><small> - nastavte si další možnosti na myteamworld.com.</small>
                            </p>
                            <p class="entries cleaned">
                                <label class="label">Osobní zprávy</label>
                                <?php
                                echo CHtml::getSelect('privacy['.UserSettings::KEY_MESSAGES.']', $messages_notices, (isset($_POST['privacy'][UserSettings::KEY_MESSAGES]) ? $_POST['privacy'][UserSettings::KEY_MESSAGES] : @$logged_user->settings[UserSettings::KEY_MESSAGES]), '');
                                ?>
                            </p>

                            <?php if($action == 'registration') { ?>

                            <p class="buttonBox">
                                <button class="button buttonA" type="submit">dokončit registraci</button>
                            </p>

                            <?php } else { ?>

                            <p class="buttonBox">
                                <button class="button buttonA" type="submit">uložit nastavení</button>
                            </p>

                            <?php } ?>

                            </div>


                            <?php
                            if($action != 'registration')
                            {
                            ?>
                            <!-- BOX 3 -->

                            <div id="profilesetting" class="innerBox">

                            <p class="title-noborder">
                                <strong>Heslo</strong><small> - nastavte si nové heslo pro přístup do vašeho účtu.</small>
                            </p>

                            <p class="entries cleaned">
                                <label class="label" for="oldPass">Staré heslo:</label>
                                <input class="input text" type="password" name="password[old]" value="" id="oldPass" />
                            </p>
                            <p class="entries cleaned">
                                <label class="label" for="newPass">Nové heslo:</label>
                                <input class="input text" type="password" name="password[new_one]" value="" id="newPass" />
                            </p>
                            <p class="entries cleaned">
                                <label class="label" for="newPassAgain">Nové heslo znovu:</label>
                                <input class="input text" type="password" name="password[new_two]" value="" id="newPassAgain" />
                            </p>

                            <?php if($action == 'registration') { ?>

                            <p class="buttonBox">
                                <button class="button buttonA" type="submit">dokončit registraci</button>
                            </p>

                            <?php } else { ?>

                            <p class="buttonBox">
                                <button class="button buttonA" type="submit">uložit nastavení</button>
                            </p>

                            <?php } ?>

                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        </form>
                    </div><!-- .contentAbox -->
                </div><!-- #contentA .registration -->




<?php /*
<script type="text/javascript">
/************************************************************
    Jcrop - orez obrazku - zavolat pro orez obrazku
************************************************************ /
function ajaxCrop(){

    $('#picture_big').html('');
    $('<span id="loading"><!-- --></span>').appendTo('#picture_big');

    $.ajax({
        url: "/ajax.php",
        type: "get",
        <?php if($action == 'registration') { ?>
    	data: "hash=<?=addslashes($_GET['hash'])?>&email=<?=addslashes($_GET['email'])?>&crop=1&x="+$("#x").val()+"&y="+$("#y").val()+"&width="+$("#w").val(),
        <?php } else { ?>
        data: "crop=1&x="+$("#x").val()+"&y="+$("#y").val()+"&width="+$("#w").val(),
        <?php } ?>
        //data: dataa,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (res) {

            if(res.status == "ok")
            {
                $("#picture_big").html(res.img_big); // tim se prepise loading
                var re1 = /width="\d+" height="\d+"/;
                var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                $(".picture_medium").html(res.img_medium);
                $(".picture_small").html(res.img_small);
                $(".picture_main").html(res.img_original.replace(re1,replace));
                $('#mainPhoto').Jcrop({
                    onChange:       showPreview,
                    onSelect:       showPreview,
                    setSelect:      [ 50, 50, 250, 250 ],
                    minSize:        [ 200, 200 ],
                    aspectRatio:    1
                });
            }
            else
            {
                alert('Nastala chyba při resizu obrázku.');
            }

        },
        complete: function(){

            var messagePlace    = $('#contentA');

            messagePlace.find('.msg:not(.noRemove)').remove();
            messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně změněna.</p></div>');
        }
    });
}


$('#deleteProfilePhoto').live('click', function() {

    // confirm na smazani zpravy
    $.confirm({
        'title'     : 'Potvrdit smazání fotografie',
        'message'   : 'Opravdu si přejete smazat svoji profilovou fotografii?',
        'buttons'   : {
            'potvrdit'  : {
                'class' : 'buttonA icon confirm',
                'action': function(){

                    $('#picture_big').html('');
                    $('<span id="loading"><!-- --></span>').appendTo('#picture_big');

                    $.ajax({
                        url: '/ajax.php',
                        type: 'GET',
                        <?php if($action == 'registration') { ?>
                        data: 'hash=<?=addslashes($_GET['hash'])?>&email=<?=addslashes($_GET['email'])?>&delete_profile_photo=1',
                        <?php } else { ?>
                        data: 'delete_profile_photo=1',
                        <?php } ?>
                        dataType: 'json',
                        success: function (res) {
                            if(res.status == 'ok')
                            {
                                $("#picture_big").html(res.img_big);
                                var re1 = /width="\d+" height="\d+"/;
                                var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                                $(".picture_medium").html(res.img_medium);
                                $(".picture_small").html(res.img_small);
                                $(".picture_main").html(res.img_original.replace(re1,replace));
                            }
                            else
                            {
                                alert('Nastala chyba při ukládání obrázku.');
                            }
                        },
                        complete: function(){

                            var messagePlace    = $('#contentA'),
                                controlsPlace   = $('#contentA .inner .left .list');

                            controlsPlace.find('.edit').remove();
                            controlsPlace.find('.delete').remove();
                            messagePlace.find('.msg:not(.noRemove)').remove();
                            messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně smazána.</p></div>');
                        }
                    });

                }
            },
            'zrušit'    : {
                'class' : 'buttonA',
                'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
            }
        }
    });

    return false;
});


var input = document.getElementById("uploadPhoto");
var formdata = false;

input.addEventListener("change", function (evt) {
	var i = 0, len = this.files.length, img, reader, file;

	file = this.files[i];

    // pokud je to podporovany typ fotek
    if ( file.type.match(/image.jpeg/) || file.type.match(/image.jpg/) || file.type.match(/image.png/) || file.type.match(/image.gif/) )
    {
        if (window.FormData) {
            formdata = new FormData();
            //document.getElementById("btn").style.display = "none";
        }
        if ( window.FileReader ) {
            reader = new FileReader();
            reader.onloadend = function (e) {
                //showUploadedItem(e.target.result, file.fileName);
            };
            reader.readAsDataURL(file);
        }
        if (formdata) {
            formdata.append("image", file);
        }
    }
    else
    {
        return false;
    }

	if (formdata) {

        $('#picture_big').html('');
        $('<span id="loading"><!-- --></span>').appendTo('#picture_big');

		$.ajax({
    		<?php if($action == 'registration') { ?>
        	url: "/ajax.php?hash=<?=addslashes($_GET['hash'])?>&email=<?=addslashes($_GET['email'])?>&file",
            <?php } else { ?>
            url: "/ajax.php?file",
            <?php } ?>
			type: "POST",
			data: formdata,
            dataType: 'json',
			processData: false,
			contentType: false,
			success: function (res) {
                if(res.status == "ok")
                {
                    $("#picture_big").html(res.img_big);
                    var re1 = /width="\d+" height="\d+"/;
                    var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                    $(".picture_medium").html(res.img_medium);
                    $(".picture_small").html(res.img_small);
                    $(".picture_main").html(res.img_original.replace(re1,replace));
                    $('#mainPhoto').Jcrop({
                        onChange:       showPreview,
                        onSelect:       showPreview,
                        setSelect:      [ 50, 50, 250, 250 ],
                        minSize:        [ 200, 200 ],
                        aspectRatio:    1
                    });
                    //location.reload();

                    // nepouziva se - moznost menit pouze SRC vsech obrazku a ne generovat cely tag img
                    /*$("#picture_big img").attr('src',res.img_big);
                    $(".picture_medium img").attr('src',res.img_medium);
                    $(".picture_small img").attr('src',res.img_small);
                    $(".picture_big img").attr('src',res.img_big);
                    var re1 = /height="\d+"/;
                    var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";
                    $(".picture_original img").attr('src',res.img_original.replace(re1,''));

                    $(".picture_main img").attr('src',res.img_original.replace(re1,replace));* /
                }
                else
                {
                    alert('Nastala chyba při ukládání obrázku.');
                }
			},
            complete: function(){

                var messagePlace    = $('#contentA'),
                    controlsPlace   = $('#contentA .inner .left .list');

                controlsPlace.find('.edit').remove();
                controlsPlace.find('.delete').remove();
                messagePlace.find('.msg:not(.noRemove)').remove();
                messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně změněna.</p></div>');
                controlsPlace.append('<li class="item edit">' +
                                            '<a title="Upravit vřez profilové fotografie" class="popUpBig" href="#editProfilePhoto">Upravit výřez fotografie</a>' +
                                        '</li>' +
                                        '<li class="item delete">' +
                                            '<a title="Smazat profilovou fotografie" id="deleteProfilePhoto" href="#">Smazat profilovou fotografii</a>' +
                                        '</li>');
            }
		});
	}
}, false);


</script>
*/ ?>