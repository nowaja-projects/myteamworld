<div id="contentA" class="module">

<?php
if($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id)
{
?>
    <div id="admin">
        <a href="#addPhotos" id="addPhotosButton" class="button buttonB icon add popUpMedium" title="Nahrát nové fotografie">nahrát fotografie</a>
    </div>
<?php
}

$list = $gallery->getPhotoList();
?>
    <div class="contentAbox clear">
    	<h2 class="headline">Detail galerie "<?=$gallery->getName()?>"</h2>
        <?php
        if(count($list))
        {
        ?>
    	<ul id="galleryDetail" class="cleaned" data-id="<?=$gallery->id?>">
            <?php
            foreach($list as $photo)
            {
                echo print_gallery_item($gallery, $photo, $team);
            }
            ?>
    	</ul>
        <?php
        }
        ?>
    </div>

    <p class="backLink">
        <a href="<?=$team->getProfileLink('gallery')?>" class="button buttonC">zpět na výpis galerií</a>
    </p>


    <?php
    if($logged_user->getActiveTeam()->id == $team->id && $team->isAdmin($logged_user))
    {
    ?>

    <div class="forBlind">

        <div id="addPhotos" class="fancybox-popup">
            <h3 class="title">Nahrát fotografie do galerie</h3>

            <form id="uploadPhotos" class="popUpForm dropzone" action="<?=PATH_WEB_ROOT?>gallery_ajax.php">
                <fieldset class="cleaned">
                    <legend class="forBlind">Nahrát nové fotografie</legend>

                    <input class="galleryID" type="hidden" name="gallery_id" value="<?=$gallery->id?>" />
                    <input class="galleryHash" type="hidden" name="gallery_hash" value="<?=getGalleryHash($team->id, $gallery->id)?>" />

                </fieldset>
            </form>

            <p class="link">
                <a href="#" title="hotovo, zavřít">hotovo</a>
            </p>
        </div><!-- #addPhotos -->

        <div id="renameItem" class="fancybox-popup">
            <h3 class="title">Editovat popis fotografie</h3>

            <form class="popUpForm" action="">
                <fieldset>
                    <legend class="forBlind">Editovat popis fotografie</legend>

                    <p class="entries first">
                        <label>Popis fotografie</label><br />
                        <textarea class="renameID" name="photo_name" data-id="0" cols="48" rows="3"></textarea>
                    </p>

                    <p class="entries submit">
                        <button type="submit">uložit a zavřít</button>
                    </p>
                </fieldset>
            </form>
        </div><!-- #renameItem -->

    </div><!-- .forBlind -->
    
    <?php
    }
    ?>

</div>