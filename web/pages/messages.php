<?php
$conv = PrivateMessages::getConversations($logged_user->id);
?>
<div id="contentA">

    <?php
    if($logged_user->hasTeam())
    {
    ?>
    <div id="admin">
        <? /*<a href="#ignoreList" class="button buttonB icon edit popUpMedium">blokovaní uživatelé</a>*/ ?>
        <a href="#sendNewMessage" class="button buttonB icon add popUpMedium">napsat novou zprávu</a>
    </div>
    <?php
    }
    ?>

    <div class="contentAbox">    

        <h2 class="headline">Zprávy</h2>

        <?php
        if(count($conv) > 0)
        {
        ?>
            <p class="title cleaned">konverzace s uživatelem<span class="date">poslední zpráva</span></p>
        <?php  
            $all = array();
            // musime si je nejprve seradit podle posledniho prispevku
            foreach($conv as $data)
            {
                $pm = new PrivateMessage($data);
                $lastUser = new User($data);
                
                // nacteme si detail konverzace
                $detail = PrivateMessages::getConversationDetail($pm->conversation_id, $logged_user->id);
                
                // pro jistotu nastavime pole na zacatek
                reset($detail);
                
                // zjistime si cas pridani posledniho prispevku
                $last = current($detail);
                
                // ulozime kvuli vypisu
                $all[$last['time'] . '-' . $last['id'] . $last['users']] = array(
                    'pm'        => $pm,
                    'lastUser'  => $lastUser
                );
            }
        
            // seřadíme podle klíče
            krsort($all);
        
            foreach($all as $data)
            {
                $pm = $data['pm'];
                $lastUser = $data['lastUser'];
                
                // udelame pole vsech uzivatelu
                $users = explode('-', substr($pm->users, 1, -1));
                
                // najdeme si prihlaseneho uzivatele
                $lu = array_search($logged_user->id, $users);
                
                // prihlaseny uzivatel v te konverzaci vubec neni to je divne
                if($lu === false)
                {
                    continue;
                }
                // toho vypisovat nechceme
                unset($users[$lu]);
                
                $count = count($users);
                
                $userArray = array();
                
                // nacteme si vsechny prijemce v konverzaci
                foreach($users as $user_id)
                {
                    $mainUser = UserDAO::get($user_id);
                    if(!empty($mainUser->team))
                    {
                        $userTeam = TeamDAO::get($mainUser->team);
                    }
                    else
                    {
                        $userTeam = false;
                    }
                    
                    $userArray[] = array(
                        'user' => $mainUser,
                        'team' => $userTeam
                    );
                }
                
                // nacteme si detail konverzace
                $detail = PrivateMessages::getConversationDetail($pm->conversation_id, $logged_user->id);
                
                // nemáme příjemce, tak je někde bug, takže zprávu pošlem do háje
                if(count($userArray) < 1 || $detail === false)
                {
                    continue;
                }
                
                echo printMessageBox($detail, $userArray);
            }
        }
        else
        {
        ?>
        <p class="msg warning noClose">Nemáte žádné otevřené konverzace.</p>
        <?php
        }
        ?>
  
    </div><!-- .contentAbox -->

    <div class="forBlind">
        <div id="sendNewMessage" class="fancybox-popup">
            <h3 class="title">Poslat novou zprávu</h3>

            <form id="popUpFormSendNewMessage" class="popUpForm" action="" method="post">
                <fieldset>
                    <legend class="forBlind">Poslat novou zprávu</legend>

                    <p class="entries first">
                        <label for="messageRecipients">Příjemci zprávy</label><br />
                        <select id="messageRecipients" name="pm[to]"></select>
                    </p>

                    <p class="entries">
                        <label for="messageText">Text zprávy</label><br />
                        <textarea id="messageText" rows="10" cols="5" name="pm[text]"></textarea>
                    </p>

                    <p class="entries submit">
                        <button type="submit">poslat zprávu</button>
                    </p>
                </fieldset>
            </form>
        </div><!-- #sendNewMessage -->
    </div>


</div><!-- #contentAbox -->