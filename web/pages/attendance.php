<div id="contentA" class="module">

  <?php
  if($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id)
  {
  /*
  ?>
      <div id="admin">
        <a href="#" class="button buttonB icon add">statistiky docházky</a>
      </div>
  <?php
  */
  }
  ?>

    <div class="contentAbox" id="attendBox">          
    <?php
    $positions = SportDAO::getPositions($team->sport_id);
    $roster = Rosters::getTeamRoster($team->id);
    $start = date('Y-m-d H:i:s');
    $limit = 4;
    $events = EventDAO::getTeamEvents($team->id, array(), 'future', $start, 0, array(), $limit);
    
    // kdyz nejsou udalosti, ostatni veci nas nezajimaji
    if(count($events) == 0)
    {
        $events = EventDAO::getTeamEvents($team->id, array(), 'past', $start, 0, array(), 1);
        //$events = array_reverse($events);
        array_pop($events);
    }
    
    if(count($events) == 0)
    {
        echo '<div class="msg warning noMarginBottom noClose">'.$tr->tr('Tým momentálně nemá naplánované žádné události.').'</div>';
    }
    else
    {
        //$events = array_reverse($events);
        $stopLeft = false;
        $stopRight = false;

        $pom = EventDAO::getTeamEvents($team->id, array(), 'past', $start, 0, array(), 0);
        if(count($pom) == 0)
        {
            $stopLeft = true;
        }

        // pokud jsme jich nacetli vic nez limit, tak muzeme jeste pokracovat
        if(count($events) == ($limit + 1))
        {
            array_pop($events);
        }
        else
        {
            $repeat = ($limit - count($events));
            for($a = $repeat; $a > 0; $a--)
            {
                $events = $events + array($a => 'nic');
            }

            $stopRight = true;
        }
        
        $attend = AttendDAO::getAttendForEvents($events);
        foreach($positions as $position)
        {
            if(isset($roster[$position->id]))
            {
                $players = $roster[$position->id];
            }
            else
            {
                $players = array();
            }
            
            if($position->staff != 1)
            {
                echo printPositionAttend($position, $players, $attend, $events, $stopLeft, $stopRight, $team);
            }
            elseif(count($players))
            {
                echo printPositionAttend($position, $players, $attend, $events, $stopLeft, $stopRight, $team);
                // FIXME !!!
                //echo printStaffAttend($position, $players, $attend, $events); 
            }
        }

        // zkusime vypsat i nezarazene hrace
        if(isset($roster[0]))
        {
            $data = array(
                'id'        => '0',
                'sport_id'  => $position->sport_id,
                'name'      => 'Nezařazení hráči',
                'name2'     => 'Nezařazení hráči',
                'short'     => '',
                'order'     => '',
                'staff'     => '0',
                'stats'     => ''
            );
            $position = new Position($data);
            
            $players = $roster[0];
            echo printPositionAttend($position, $players, $attend, $events, $stopLeft, $stopRight, $team);
        }
    }
    ?>
    </div><!-- .contentAbox -->
    
</div><!-- #contentA .module -->

<? /* FIXME - toto dole oifovat tak, aby to k poslani zpravy mohl videt jen uzivatel co muze poslat zpravu, to k doplneni dochazky videl jen ten kdo muze doplnit dochazku */ ?>
<div class="forBlind">

    <div id="addAttendance" class="fancybox-popup">
        <h3 class="title">Vyplnit docházku</h3>

        <form id="popUpFormAddAttendance" class="popUpForm" action="" method="get">
            <fieldset>
                <legend class="forBlind">Vyplnit docházku</legend>

                <div class="entries first">
                    <input type="hidden" name="attendance[event-id]" id="eventID" />
                
                    <label>Vyberte odpověď</label><br />
                    <ul class="attStatus list cleaned">
                      <li class="yes"><a href="#">Zúčastním se</a></li>
                      <li class="no"><a href="#">Nezúčastním se</a></li>
                      <li class="na"><a href="#">Zatím nevím</a></li>
                    </ul>
                    <span class="forBlind">
                      <input class="yes" value="yes" type="radio" name="attendance[status]" />
                      <input class="no" value="no" type="radio" name="attendance[status]" />
                      <input class="na" value="na" type="radio" name="attendance[status]" />
                    </span>
                    <span class="errorMsg">Vyberte, prosím, vaši odpověď.</span>
                </div>

                <?php
                if($team->isAdmin($logged_user))
                {
                ?>
                <div class="entries admin">
                    <ul class="attStatus list cleaned">
                      <li class="blackDot"><a href="#">Neomluvená neúčast</a></li>
                    </ul>
                    <span class="forBlind">
                      <input class="blackDot" value="blackDot" type="radio" name="attendance[status]" />
                    </span>
                </div>
                <?php
                }
                ?>

                <p class="entries">
                    <label for="attendanceText">Text - důvod</label><br />
                    <textarea id="attendanceText" rows="10" cols="2" name="attendance[text]"></textarea>
                </p>

                <p class="entries submit">
                    <button id="attendanceSubmit" class="submitButton" type="submit">uložit a zavřít</button>
                </p>
            </fieldset>
        </form>
    </div><!-- #modalboxAttendance -->

    <div id="sendNewMessage" class="fancybox-popup">
        <h3 class="title">Poslat novou zprávu</h3>

        <form id="popUpFormSendNewMessage" class="popUpForm" action="" method="post">
                <fieldset>
                    <legend class="forBlind">Poslat novou zprávu</legend>

                    <p class="entries first">
                        <label for="messageRecipients">Příjemci zprávy</label><br /> 
                        <input id="messageRecipients" type="text" disabled="disabled" value="" />
                        <input id="messageRecipientsID" type="hidden" name="pm[to][]" value="" />
                    </p>

                    <p class="entries">
                        <label for="messageText">Text zprávy</label><br />
                        <textarea id="messageText" rows="10" cols="5" name="pm[text]"></textarea>
                    </p>

                    <p class="entries submit">
                        <button type="submit">poslat zprávu</button>
                    </p>
                </fieldset>
            </form>
    </div><!-- #sendNewMessage -->

</div>