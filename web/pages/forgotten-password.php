<section class="sidebar left colSmall">
    <!-- -->
</section><!-- .side-column .left -->

<section id="content" class="colBig">
    <div id="contentA" class="text">
    	<div class="contentAbox">

    		<h2 class="headline">Obnovit zapomenuté heslo</h2>

    		<?php

            if($_project['message']->isWarning())
            {
            ?>
            <div class="warning">

                <?php foreach($_project['message']->getWarnings() as $text) { ?>
                <?php echo $text;?>
                <?php } ?>

            </div> <!-- #msg -->
            <?php
            }

            $_project['message']->clear();
            ?>

    		<p>Pokud jste zapomněli své heslo pro přihlášení do vašeho účtu na myteamworld.com, zadejte váš e-mail uvedený na myteamworld.com, který zároveň slouží jako přihlašovací jméno, a my vám na něj pošleme nové heslo.</p>

    	    <form action="" method="post">
    	    	<fieldset>
    	    		<input class="text" type="text" placeholder="e-mail" autocomplete="off" name="password[email]" />
    	    		<button type="submit" class="button buttonA small">Odeslat</button>
    	    	</fieldset>
    	    </form>

    	</div><!-- .contentAbox -->
    </div><!-- #contentA -->
</section><!-- #content -->