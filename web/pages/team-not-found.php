
<div id="contentA" class="module">

    <h2 class="bigTitle">Tým nebyl nalezen</h2>
    <p class="bigInfo">Je nám líto, tým se nám nepodařilo nalézt.</p>

    <h3 class="title">Jak dál?</h3>
    <ul>
    	<li>Pokud jste adresu požadované stránky zadávali ručně, zkontrolujte, prosím, zda jste ji zadali správně.</li>
    	<li>Je možné, že tým změnil svůj název. Zkuste najít tým znovu ve vyhledávání.</li>
		<li>Můžete se vrátit na <a href="/">hlavní stránku myteamworld.com</a>.</li>
    </ul>
    
</div><!-- #contentA .module -->