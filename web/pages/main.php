        
            <h2 class="title">Přihlášení a registrace uživatele</h2>

            <?php

            if($_project['message']->isWarning())
            {
            ?>
            <div class="warning">
            	
                <?php foreach($_project['message']->getWarnings() as $text) { ?>
                <?php echo $text;?>
                <?php } ?>
            	
            </div> <!-- #msg -->
            <?php
            }
            ?>
            
            <?php
            if($_project['message']->isDone())
            {
            ?>
            <div class="done">
            	
                <?php foreach($_project['message']->getDones() as $text) { ?>
                <?php echo $text;?>
                <?php } ?>
            	
            </div> <!-- #msg -->
            <?php
            }
            
            $_project['message']->clear();

            //include(BLOCK_PATH.'print-messagess.php');
            ?>

            <form id="login" action="<?php echo PATH_WEB_ROOT;?>" method="post">
                <fieldset>
                    <p class="entries first">
                        <label for="email">e-mail</label><br />
                        <input class="text" type="text" id="email" name="login[login]"/>
                    </p>
                    <p class="entries">
                        <label for="password">heslo</label><br />
                        <input class="text" type="password" id="password" name="login[password]"/>
                    </p>
                    <button class="button buttonA" type="submit" name="" value="">přihlásit</button>
                </fieldset>
            </form>

            <form id="registration" action="<?php echo PATH_WEB_ROOT;?>" method="post">
                <fieldset>
                    <p class="entries">
                        <label for="firstname">Jméno</label>
                        <input type="text" id="firstname" class="text" name="registration[fname]" autocomplete="off" value="<?php echo !empty($_POST['registration']['fname']) ? $_POST['registration']['fname'] : ''; ?>"/>
                    </p>
                    <p class="entries">
                        <label for="surname">Příjmení</label>
                        <input type="text" id="surname" class="text" name="registration[sname]" autocomplete="off" value="<?php echo !empty($_POST['registration']['sname']) ? $_POST['registration']['sname'] : ''; ?>"/>
                    </p>
                    <p class="entries">
                        <label for="email-2">E-mail</label>
                        <input type="text" id="email-2" class="text" name="registration[email]" autocomplete="off" value="<?php echo !empty($_POST['registration']['email']) ? $_POST['registration']['email'] : ''; ?>"/>
                    </p>
                    <p class="entries">
                        <label for="password-2">Heslo</label>
                        <input type="password" id="password-2" class="text" name="registration[password]"/>
                    </p>
                    <?php /* TODO - stare zadavani bydliste, nahodit na naseptavac
                    <p class="entries">
                        <label for="region">Moje bydliště</label>
                        <?php
                        $selected_region = !empty($_POST['registration']['region']) ? $_POST['registration']['region'] : 0;
                        echo CHtml::getSelect('registration[region]', $_regions, $selected_region, 'id="region"');
                        ?>
                    </p>
                    */ ?>
                    <p class="entries selectOne">
                        <label for="location">Místo pobytu</label>
                        <? /*
                        <input type="hidden" id="locationID" name="registration[locationID]" value="<?php echo !empty($_POST['registration']['locationID']) ? $_POST['registration']['locationID'] : ''; ?>" />
                        <input type="text" id="location" class="text" name="registration[city]" value="<?php echo !empty($_POST['registration']['city']) ? $_POST['registration']['city'] : ''; ?>"/>
                        */ ?>
                        <select id="locationID" name="registration[locationID]"></select>
                        <input type="hidden" id="location" name="registration[city]" value="<?php echo !empty($_POST['registration']['city']) ? $_POST['registration']['city'] : ''; ?>" data-id="<?php echo !empty($_POST['registration']['locationID']) ? $_POST['registration']['locationID'] : ''; ?>" />
                    </p>                      
                    <p class="entries">
                        <label for="sport">Můj hlavní sport</label>
                        <?php
                        $sport_list = array(0 => '-- Vyberte --') + SportDAO::getSelectList();
                        $selected_sport = !empty($_POST['registration']['sport']) ? $_POST['registration']['sport'] : 0;
                        echo CHtml::getSelect('registration[sport]', $sport_list, $selected_sport,'id="sport"');
                        ?>
                    </p>
                    <p class="entries submit cleaned">
                        <a class="agreement" href="/terms/">Registrací souhlasím s podmínkami</a>
                        <button class="button buttonA" type="submit" name="" value="">vytvořit účet</button>
                    </p>
                </fieldset>
            </form>