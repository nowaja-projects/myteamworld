
<div id="contentA" class="module">

<?php
if($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id)
{
?>
    <div id="admin">
      <a href="<?php echo PATH_WEB_ROOT;?>event-manage/" class="button buttonB icon add">vytvořit událost</a>
    </div>
<?php
}
?>

<?php include(BLOCK_PATH . 'print-messagess.php'); ?>

    <div class="contentAbox">            
    
        <div class="topFilter cleaned" data-type="eventsFilter">
            <div class="filterBox">
            <?php
            $team_seasons = $team->getSeasons();
            if(is_array($team_seasons) && count($team_seasons) > 0)
            {
            ?>
                sezóna: <span class="activeFilter year"><a href="#" data-order="1" data-id="all">vše</a></span>
                <ul class="filterSelect">
                    <?php
                    $i = 2;
                    foreach($team_seasons as $season)
                    {
                    ?>
                    <li><a href="#" data-order="<?=$i++?>" data-id="<?=$season->id?>"><?=$season->name?></a></li>
                    <?php
                    }
                    ?>
                </ul>
            <?php
            }
            ?>
            </div>

            <?php /*
            <div class="filterBox">
                soutěž: <span class="activeFilter competition"><a href="#" data-order="1" data-id="vse">all</a></span>
                <ul class="filterSelect">
                    <li><a href="#" data-order="2" data-id="6.-turnaj-AFL">6. turnaj AFL</a></li>
                    <li><a href="#" data-order="3" data-id="5.-turnaj-AFL">5. turnaj AFL</a></li>
                    <li><a href="#" data-order="4" data-id="4.-turnaj-AFL">4. turnaj AFL</a></li>
                </ul>
            </div>
            */ ?>

            <div class="filterBox">
                druh události: <span class="activeFilter type"><a href="#" data-order="1" data-id="all">vše</a></span>
                <ul class="filterSelect">
                    <li><a href="#" data-order="2" data-id="<?=Event::FRIENDLY?>">přátelský zápas</a></li>
                    <li><a href="#" data-order="3" data-id="<?=Event::COMPETITION?>">soutěžní zápas</a></li>
                    <?php
                    if($team->isPlayer($logged_user))
                    {
                    ?>
                    <li><a href="#" data-order="4" data-id="<?=Event::TRAINING?>">trénink</a></li>
                    <li><a href="#" data-order="5" data-id="<?=Event::ACTION?>">týmová akce</a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
                  
        <h2 class="headline">Události</h2>
        <?php
        // přihlášený uživatel uvidí všechny druhy událostí
        if($team->isPlayer($logged_user))
        {
            $types = array(
                Event::COMPETITION,
                Event::FRIENDLY,
                Event::ACTION,
                Event::TRAINING
            );
        }
        else
        {
            $types = array(
                Event::COMPETITION,
                Event::FRIENDLY
            );
        }
        
        $events = $team->getEvents(array(), $types);
        if(count($events))
        {
        ?>
            <table class="table events last" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th class="type">typ</th>
                        <th class="teamLogo left hidden-xxs">logo</th>
                        <th class="teamName left">název týmu</th>
                        <th class="competitionName left hidden-xxs">název / soutěž</th>
                        <th class="date left">datum</th>
                        <th class="result">výsledek</th>                               
                    </tr>
                </thead>
                <tbody>
        <?php
            echo printEvents($events, $team, $logged_user, true);
        
?>
                </tbody>
            </table>
        <?php
        }
        else
        {
            
        }
        
        //EventDAO::getTeamEvents();
        /*
        ?>
            <table class="table events last" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="width: 6%;">&nbsp;</th>
                        <th style="width: 6%;" class="left">logo</th>
                        <th style="width: 30%;" class="name left">název</th>
                        <th style="width: 26%;" class="left">soutěž</th>
                        <th style="width: 12%;" class="left">datum</th>
                        <th style="width: 20%;">výsledek</th>                               
                    </tr>
                </thead>
                <tbody>
                    
                        <td class="resultlost"><a href="#" title="Zobrazit detail zápasu">2:4</a></td>
                        <td class="resultdraw"><a href="#" title="Zobrazit detail zápasu">7:7</a></td>
                        <td class="resultwin"><a href="#" title="Zobrazit detail zápasu">10:3</a></td>
                    <tr>                                      
                        <td class="flag training"><div class="deleteBig"><a title="Smazat událost" href="#"><!-- --></a></div></td>
                        <td class="team"><a href="#" class="img"><img src="/teamdata/image26.jpg" alt="Tomáš Brožek" /></a></td>
                        <td class="team left"><span class="name"><span class="firstName">Týmový</span><br /><span class="lastName">Trénink</span></span></td>
                        <td class="left">Rosice - hala</td>
                        <td class="left">01. 12. 2010</td>
                        <td class="result">-&nbsp;-&nbsp;-</td>
                        <td><div class="editResult"><a title="Editovat výsledek" href="#"><!-- --></a></div></td>                                  
                    </tr>
                </tbody>
            </table>                                                                
        <?php
        */
       

        ?>

<?php /*
    <table cellspacing="0" cellpadding="0" class="table events last">
        <thead>
            <tr>
                <th style="width: 6%;">typ</th>
                <th class="left" style="width: 6%;">logo</th>
                <th class="name left" style="width: 29%;">název týmu</th>
                <th class="left" style="width: 27%;">název / soutěž</th>
                <th class="left" style="width: 12%;">datum</th>
                <th style="width: 20%;">výsledek</th>                               
            </tr>
        </thead>

        <tbody>
            <tr data-id="3-2" class="item first">
                <td class="flag match">
                    <div class="controlsBox">
                        <div class="controls left">
                            <a title="Smazat událost" class="deleteBig" href="#"><!-- --></a>
                        </div>
                    </div>
                </td>
                <td class="team"><span class="img"><img title="FBŠ Bohemians" alt="FBŠ Bohemians" src="/teamdata/2/event3/image26.jpg?53068b356fd5b"></span></td>
                <td class="team left"><span title="FBŠ Bohemians" class="name"><span class="lastName">FBŠ Bohemians</span></span></td>
                <td class="left title"><span title="Soutěžní zápas" class="name">Soutěžní zápas</span></td>
                <td class="left date"><span class="help">21. 02. 2014</span><span class="tooltip">od 18:00, 21. 02. 2014 <br> do 10:00, 22. 02. 2014</span></td>
                <td class="result">
                    <div class="controlsBox">
                        <div class="controls two cleaned">
                            <a title="Editovat událost" class="editBig" href="http://beta.myteamworld.com/event-manage/3-2/"><!-- --></a>
                            <a title="Zadat výsledek zápasu" class="editResult" href="http://beta.myteamworld.com/event-edit-result/3-2/"><!-- --></a>
                        </div>
                        <a title="Zobrazit detail zápasu" href="http://beta.myteamworld.com/event-detail/3-2/">3 : 4</a>
                    </div>
                </td>                                
            </tr>

            <tr data-id="2-2" class="item unofficial">
                <td class="flag match">
                    <div class="controlsBox">
                        <div class="controls left">
                            <a title="Odmítnout událost" class="deleteBig" href="#"><!-- --></a>
                        </div>
                    </div>
                </td>
                <td class="team"><a class="img" href="http://beta.myteamworld.com/tj-cechie-zastavka-muzi-1/team-profile/"><img title="TJ Čechie Zastávka" alt="TJ Čechie Zastávka" src="/teamdata/1/image26.jpg?5312794ede31f"></a></td>
                <td class="team left"><a class="name" title="Detail týmu TJ Čechie Zastávka" href="http://beta.myteamworld.com/tj-cechie-zastavka-muzi-1/team-profile/"><span class="firstName">TJ</span><br><span class="lastName">Čechie Zastávka</span></a></td>
                <td class="left title"><span title="Testovací soutěžní zápas" class="name">Testovací soutěžní zápas</span></td>
                <td class="left date"><span class="help">16. 02. 2014</span><span class="tooltip">od 19:30, 16. 02. 2014 <br> do 21:00, 16. 02. 2014</span></td>
                <td class="result">
                    <div class="controlsBox">
                        <div class="controls two cleaned">
                            <a title="Editovat událost" class="editBig" href="http://beta.myteamworld.com/event-manage/2-2/"><!-- --></a>
                            <a title="Přijmout událost" class="checkBig" href="#"><!-- --></a>
                        </div>
                        <a title="Zobrazit detail zápasu" href="http://beta.myteamworld.com/event-detail/2-2/">- : -</a>
                    </div>
                </td>                                
            </tr>

            <tr data-id="2-2" class="item unofficial">
                <td class="flag match">
                    <div class="controlsBox">
                        <div class="controls left">
                            <a title="Odmítnout událost" class="deleteBig" href="#"><!-- --></a>
                        </div>
                    </div>
                </td>
                <td class="team"><a class="img" href="http://beta.myteamworld.com/tj-cechie-zastavka-muzi-1/team-profile/"><img title="TJ Čechie Zastávka" alt="TJ Čechie Zastávka" src="/teamdata/1/image26.jpg?5312794ede31f"></a></td>
                <td class="team left"><a class="name" title="Detail týmu TJ Čechie Zastávka" href="http://beta.myteamworld.com/tj-cechie-zastavka-muzi-1/team-profile/"><span class="firstName">TJ</span><br><span class="lastName">Čechie Zastávka</span></a></td>
                <td class="left title"><span title="Testovací soutěžní zápas" class="name">Testovací soutěžní zápas</span></td>
                <td class="left date"><span class="help">16. 02. 2014</span><span class="tooltip">od 19:30, 16. 02. 2014 <br> do 21:00, 16. 02. 2014</span></td>
                <td class="result">
                    <div class="controlsBox">
                        <div class="controls two cleaned">
                            <a title="Editovat výsledek a statistiky zápasu" class="editResult" href="http://beta.myteamworld.com/event-edit-result/3-2/"><!-- --></a>
                            <a title="Přijmout výsledek a statistiky zápasu" class="checkBig" href="#"><!-- --></a>
                        </div>
                        <a title="Zobrazit detail zápasu" href="http://beta.myteamworld.com/event-detail/2-2/">5 : 6 pp</a>
                    </div>
                </td>                                
            </tr>
        </tbody>
    </table>
*/ ?>

    </div><!-- .contentAbox -->

</div><!-- #contentA .module -->