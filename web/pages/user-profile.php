
<div id="contentA" class="profile">

    <div class="contentAbox">
    <?php
    if($user instanceof User)
    {
    ?>
        <h2 class="headline">Profil uživatele</h2>

        <div class="inner cleaned">

            <div class="left">
                <p class="picture">
                    <img src="<?=$user->getUserImage('big')?>" alt="<?=$user->getName()?>" />
                </p>   
            </div><!-- .left -->

            <div class="right">

                <div class="head cleaned">
                    <ul class="name">
                        <li class="item firstName"><?php echo $user->fname;?></li>
                        <li class="item surName"><?php echo $user->sname;?></li>
                    </ul>
                    <?php
                    if(!$user->isFakeUser())
                    {
                    ?>
                    <ul class="rightPanel">
                    <?php
                    if(!$logged_user->getActiveTeam()->isPlayer($user) && $logged_user->getActiveTeam()->isAdmin($logged_user))
                    {
                        // zatim nastaveno na aktivni mozna pozdeji udelat popup s nastavenim do ktereho tymu ho chce pozvat
                        if($team->isInvited($user))
                        {
                            echo print_invite_leave($logged_user->getActiveTeam(), $user);
                        }
                        else
                        {
                            echo print_invite_user($logged_user->getActiveTeam(), $user);
                        }
                    ?>
                    <?php
                    }
                    
                    // 
                    if(!empty($logged_user->id) && $logged_user->id != $user->id && $user->canReceivePm($logged_user))
                    {
                    ?>
                        <li class="item message"><a href="#sendNewMessage" class="popUpMedium" title="Napsat zprávu uživateli <?=$user->getName()?>">napsat zprávu</a></li>
                    <?php
                    }
                    ?>
                    </ul>
                    <?php
                    }
                    ?>
                </div>
            
                <div class="group">
                    <p class="entries cleaned"><span class="label">Hlavní sport:</span> <span class="input"><?php echo $gsports[$user->sport];?></span></p>
                    <?php 
                    if(!empty($user->team) && count($user->getTeamList()) > 0 && isset($user->teams[$user->team]))
                    {
                    ?>
                        <p class="entries cleaned"><span class="label">Hlavní tým:</span> <span class="input"><a href="<?=$user->teams[$user->team]->getProfileLink()?>"><?=$user->teams[$user->team]->getName()?></a></span></p>
                    <?php
                        $players = $user->teams[$user->team]->getPlayers(false, true);
                        $positions = SportDAO::getPositions($user->teams[$user->team]->sport_id);
                    
                        if(!empty($players[$user->id]) && !empty($positions[$players[$user->id]['position_id']]->name))
                        {
                    ?>
                        <p class="entries cleaned"><span class="label">Pozice:</span> <span class="input"><?=$positions[$players[$user->id]['position_id']]->name?></span></p>
                    <?php
                        }
                    }
                    ?>
                </div><!-- .group -->

                <div class="group">
                    <?php if(!empty($user->height)) { ?><p class="entries cleaned"><span class="label">Výška:</span> <span class="input"><?php echo $user->height;?> cm</span></p><?php } ?>
                    <?php if(!empty($user->weight)) { ?><p class="entries cleaned"><span class="label">Váha:</span> <span class="input"><?php echo $user->weight;?> kg</span></p><?php } ?>
                </div><!-- .group -->

                <div class="group">
                    <?php 
                    if(!empty($user->birth) && $user->birth != '0000-00-00' && 
                        (   $user->id == $logged_user->id ||
                            (isset($user->settings[UserSettings::KEY_BIRTH]) && $user->settings[UserSettings::KEY_BIRTH] == UserSettings::VAL_SHOW_ALL) || 
                            (isset($user->settings[UserSettings::KEY_BIRTH]) && $user->settings[UserSettings::KEY_BIRTH] == UserSettings::VAL_SHOW_TEAMMATES && $logged_user->isTeamMate($user->id))
                        )
                    )
                    { 
                    ?>
                        <p class="entries cleaned"><span class="label">Datum narození:</span> <span class="input"><?php echo date('d. m. Y', strtotime($user->birth));?></span></p>
                    <?php
                    }
                    
                    
                    $city = $user->getResidence();
                    if(!empty($city) && 
                        (   $user->id == $logged_user->id ||
                            (isset($user->settings[UserSettings::KEY_LOCATION]) && $user->settings[UserSettings::KEY_LOCATION] == UserSettings::VAL_SHOW_ALL) || 
                            (isset($user->settings[UserSettings::KEY_LOCATION]) && $user->settings[UserSettings::KEY_LOCATION] == UserSettings::VAL_SHOW_TEAMMATES && $logged_user->isTeamMate($user->id))
                        )
                    )
                    { 
                    ?>
                    <p class="entries city cleaned"><span class="label">Místo pobytu:</span> <span class="input"><?php echo $city['name'];?>,<br /><?php echo $city['region'];?>,<br /><?php echo $city['country'];?></span></p>
                    <?php
                    }
                    
                    ?>
                </div><!-- .group -->

                <div class="group">
                    <?php 
                    if(!empty($user->email) && 
                        (   $user->id == $logged_user->id ||
                            (isset($user->settings[UserSettings::KEY_EMAIL]) && $user->settings[UserSettings::KEY_EMAIL] == UserSettings::VAL_SHOW_ALL) || 
                            (isset($user->settings[UserSettings::KEY_EMAIL]) && $user->settings[UserSettings::KEY_EMAIL] == UserSettings::VAL_SHOW_TEAMMATES && $logged_user->isTeamMate($user->id))
                        )
                    )
                    { 
                    ?>
                        <p class="entries cleaned"><span class="label">E-mail:</span> <span class="input"><a href="mailto:<?php echo $user->email;?>"><?php echo $user->email;?></a></span></p>
                    <?php
                    }
                     
                    if(!empty($user->icq) && 
                        (   $user->id == $logged_user->id ||
                            (isset($user->settings[UserSettings::KEY_ICQ]) && $user->settings[UserSettings::KEY_ICQ] == UserSettings::VAL_SHOW_ALL) || 
                            (isset($user->settings[UserSettings::KEY_ICQ]) && $user->settings[UserSettings::KEY_ICQ] == UserSettings::VAL_SHOW_TEAMMATES && $logged_user->isTeamMate($user->id))
                        )
                    )
                    { 
                    ?>
                        <p class="entries cleaned"><span class="label">ICQ:</span> <span class="input"><?php echo $user->icq;?></span></p>
                    <?php
                    }

                    if(!empty($user->phone) && 
                        (   $user->id == $logged_user->id ||
                            (isset($user->settings[UserSettings::KEY_PHONE]) && $user->settings[UserSettings::KEY_PHONE] == UserSettings::VAL_SHOW_ALL) || 
                            (isset($user->settings[UserSettings::KEY_PHONE]) && $user->settings[UserSettings::KEY_PHONE] == UserSettings::VAL_SHOW_TEAMMATES && $logged_user->isTeamMate($user->id))
                        )
                    )
                    { 
                    ?>
                        <p class="entries cleaned"><span class="label">Tel:</span> <span class="input"><?php echo $user->phone;?></span></p>
                    <?php
                    }
                    
                    if(!empty($user->web))
                    {
                    ?>
                        <p class="entries cleaned"><span class="label">www:</span> <span class="input"><a href="<?=fix_link($user->web, true, false)?>" title="Přejít na <?php echo $user->web;?>"><?php echo fix_link($user->web, false, true);?></a></span></p>
                    <?php
                    }
                    ?>
                </div><!-- .group -->                              
            </div><!-- .right -->

        </div><!-- .inner.cleaned -->
        <?php
        if(!$user->isFakeUser())
        {
        ?>
            <div class="navigation">
                <ul>
                <?php 
                if(!empty($logged_user->id))
                {
                ?>
                    <li class="first current"><a href="#activite">Poslední aktivity</a></li> 
                    <li><a href="#teams">Týmy</a></li>
                <?php
                }
                ?>
                    <li><a href="#stats">Statistiky</a></li>
                </ul>
            </div>
            
            
            <div class="tabs">

            <!-- BOX 1 -->
            <?php
            if(!empty($logged_user->id))
            {
                $now = date('Y-m-d H:i:s');
                $limit = LAST_ACTIVITIES_LIMIT + 1;
                $all = $user->getLastActivities($now, $limit);
                if(count($all) == $limit)
                {
                    $print_more = true;
                    array_pop($all);
                }
                else
                {
                    $print_more = false;
                }

                echo '<div id="activite" class="innerBox">
                <p class="title">Poslední aktivity uživatele '. $user->fname . '</p>';

                echo print_last_activities($user, $logged_user, $all, $print_more);

                echo '</div>';
                ?>
                <?php
                    //printr($all);
                    /*
                    ?>
                    <p class="lastActivite iconType"><strong><?php echo $user->fname;?></strong> okomentoval <a href="#" title="Zobrazit tiskovou zprávu">tiskovou zprávu</a> teamu <a href="#" title="Zobrazit profil teamu FBC Royal Gigolos">FBC Royal Gigolos</a>.</p>
                    <p class="lastActivite iconFans"><strong><?php echo $user->fname;?></strong> se stal fanouškem teamu <a href="#" title="Zobrazit profil teamu HC Kometa Brno">HC Kometa Brno</a>.</p>
                    <p class="lastActivite iconPhoto"><strong><?php echo $user->fname;?></strong> byl označen na <strong>6</strong> fotografiích teamu <a href="#" title="Zobrazit profil teamu FBC Royal Gigolos">FBC Royal Gigolos</a>.</p>
                    <p class="imagesLast">
                        <a href="#" class="first"><img width="109" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
                        <a href="#"><img width="109" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
                        <a href="#"><img width="109" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
                        <a href="#"><img width="109" class="last" alt="FBC Royal Gigolos" src="<?php echo TEAM_PATH;?>fbc-royal-gigolos/fbc-royal-gigolos.png" /></a>
                    </p>
                    <p class="lastActivite iconMatch"><strong><?php echo $user->fname;?></strong> okomentoval <a href="#" title="Zobrazit zápas">zápas</a> teamu <a href="#" title="Zobrazit profil teamu FBC Royal Gigolos">FBC Royal Gigolos</a>.</p>
                    <p class="lastActivite iconList">Uživateli <strong><?php echo $user->fname;?></strong> se líbí akce na <a href="#" title="Zobrazit soupisku">soupisce</a> teamu <a href="#" title="Zobrazit profil teamu HC Kometa Brno">HC Kometa Brno</a>.</p>
                <?php */ 
                /*
                
                <!-- BOX 3 -->
                
                <div id="photogallery" class="innerBox">
                    <p class="title">Poslední fotografie uživetele <?php echo $user->fname;?></p>
                    <p class="lastActivite iconType"><strong><?php echo $user->fname;?></strong> okomentoval <a href="#" title="Zobrazit tiskovou zprávu">tiskovou zprávu</a> teamu <a href="#" title="Zobrazit profil teamu FBC Royal Gigolos">FBC Royal Gigolos</a>.</p>
                </div><!-- #photogallery -->
                */
                ?>
                <!-- BOX 2 -->
                
                <div id="teams" class="innerBox">
                    <?php
                    $list = '';
                    if(count($user->teams) > 0)
                    {
                        $list .= '<ul class="list cleaned">';
                        $title = $user->fname . ' ' . $tr->tr('je členem') . ' ' . count($user->teams) . ' ' . decline(count($user->teams), 'týmu', 'týmů');

                        foreach($user->teams as $foundTeam)
                        {
                            $list .= '<li class="teamBox cleaned">'.
                                print_searched_team($foundTeam).'
                            </li>';
                        }
                        $list .= '</ul>';
                    }
                    else
                    {
                        $title = $user->fname . ' ' . $tr->tr('není členem žádného týmu');
                    }
                    ?>
                    <p class="title"><?=$title?></p>
                    <?=$list?>


                    <?php
                    $list = '';
                    if(count($user->fanTeams) > 0)
                    {
                        $list .= '<ul class="list cleaned">';
                        $title = $user->fname . ' ' . $tr->tr('je fanouškem') . ' ' . count($user->fanTeams) . ' ' . decline(count($user->fanTeams), 'týmu', 'týmů');

                        foreach($user->fanTeams as $foundTeam)
                        {
                            $list .= '<li class="teamBox cleaned">'.
                                print_searched_team($foundTeam).'
                            </li>';
                        }
                        $list .= '</ul>';
                    }
                    else
                    {
                        $title = $user->fname . ' ' . $tr->tr('není fanouškem žádného týmu');
                    }
                    ?>
                    <p class="title"><?=$title?></p>
                    <?=$list?>


                    <?php
                    $list = '';
                    if(count($user->observeTeams) > 0)
                    {
                        $list .= '<ul class="list cleaned">';
                        $title = $user->fname . ' ' . $tr->tr('je odběratelem') . ' ' . count($user->observeTeams) . ' ' . decline(count($user->observeTeams), 'týmu', 'týmů');

                        foreach($user->observeTeams as $foundTeam)
                        {
                            $list .= '<li class="teamBox cleaned">'.
                                print_searched_team($foundTeam).'
                            </li>';
                        }
                        $list .= '</ul>';
                    }
                    else
                    {
                        $title = $user->fname . ' ' . $tr->tr('není odběratelem žádného týmu');
                    }
                    ?>
                    <p class="title"><?=$title?></p>
                    <?=$list?>


                </div><!-- #fan -->
            <?php
            }
            ?>

                <!-- BOX 3 -->
                
                <div id="stats" class="innerBox">
                    <?php
                    if(count($user->teams) > 0)
                    {
                        $title = 'Statistiky uživatele ' . $user->fname;
                    }
                    else
                    {
                        $title = $user->fname . ' ' . $tr->tr('není členem žádného týmu');
                    }
                    ?>
                    <p class="title"><?=$title?></p>
                    <?=print_user_stats($user->teams, $user)?>
                </div><!-- #statistic -->

            
            </div><!-- #tabs -->
            <?php
        }
        }
        else
        {
            // title je v action
        ?>
        <h2 class="headline"><?=$title?></h2>
        <?php
        }
        ?>
    </div><!-- .contentAbox -->

    <?php 
    if($user->id != $logged_user->id)
    {
    /* FIXME - toto nemuze byt na strance pokud uzivateli nemuzu napsat zpravu nebo jsem to ja, oifovat */ ?>

    <div class="forBlind">
        <div id="sendNewMessage" class="fancybox-popup">
            <h3 class="title">Poslat novou zprávu</h3>

            <form id="popUpFormSendNewMessage" class="popUpForm" action="" method="post">
                <fieldset>
                    <legend class="forBlind">Poslat novou zprávu</legend>

                    <p class="entries first">
                        <label for="messageRecipients">Příjemci zprávy</label><br /> 
                        <input id="messageRecipients" type="text" disabled="disabled" value="<?=$user->getName()?>" />
                        <input type="hidden" name="pm[to][]" value="<?=$user->id . '-' . getRecipientHash($user->id)?>" />
                    </p>

                    <p class="entries">
                        <label for="messageText">Text zprávy</label><br />
                        <textarea id="messageText" rows="10" cols="5" name="pm[text]"></textarea>
                    </p>

                    <p class="entries submit">
                        <button type="submit">poslat zprávu</button>
                    </p>
                </fieldset>
            </form>
        </div><!-- #sendNewMessage -->
    </div>
    <?php
    }
    ?>

</div><!-- #contentA .profile -->