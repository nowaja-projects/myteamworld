<div id="contentA" class="module">

    <?php
    
    if($team->isAdmin($logged_user))
    {
        // nepublikované tiskové zprávy
        $unpublished = PressReleases::getList(array($team->id), 'unpublished');
    ?>
    <div id="admin">
        <?php if(count($unpublished) > 0) { ?> 
        <a href="#unpublishedPressReleases" id="unpublishedButton" class="button buttonB icon edit popUpMedium" title="Zobrazit nepublikované tiskové zprávy">nepublikované zprávy</a>
        <?php } ?>
        <a href="#addPost" id="addEditButton" class="button buttonB icon add popUpMedium" title="Napsat novou tiskovou zprávu">napsat zprávu</a>
    </div>
    <?php
    }
    ?>


    <?php
    $wall = PressReleases::getList(array($team->id));
    if(count($wall) > 0)
    {
        foreach($wall as $key => $val)
        {
            $wall[$key]['comments'] = WallDAO::getWallList(null, array('reply'), $val['team_id'] . '_' . $val['id'] . '_press');
            $wall[$key]['comments_count'] = count($wall[$key]['comments']);
            $wall[$key]['comments'] = array_slice($wall[$key]['comments'], 0, COMMENT_LIMIT);
            $wall[$key]['comments'] = array_reverse($wall[$key]['comments']);
        }
        echo getWall($wall);
    }
    else
    {
        echo '<div class="msg warning icon noClose">';
        echo $tr->tr('Tým momentálně nemá žádné tiskové zprávy k zobrazení.');
        echo '</div>';
    }
    
    if($team->isAdmin($logged_user))
    {
    ?>

    <div class="forBlind">
        <div id="addPost" class="fancybox-popup" data-id="">
            <h3 class="title">Napsat tiskovou zprávu</h3>

            <form id="popUpFormAddPost" class="popUpForm" action="" method="post">
                <fieldset class="cleaned">
                    <legend class="forBlind">Napsat tiskovou zprávu</legend>

                    <p class="entries first">
                        <label>Nadpis zprávy</label><br />
                        <input class="text heading" type="text" name="press[heading]" />
                        <span class="errorMsg">Vyplňte, prosím, toto pole.</span>
                    </p>

                    <p class="entries">
                        <label>Úvodní text zprávy</label><br />
                        <textarea class="perex" rows="2" cols="5" name="press[perex]"></textarea>
                        <span class="errorMsg">Vyplňte, prosím, toto pole.</span>
                    </p>

                    <p class="entries">
                        <label>Text zprávy</label><br />
                        <textarea class="message" rows="10" cols="5" name="press[text]"></textarea>
                        <span class="errorMsg">Vyplňte, prosím, toto pole.</span>
                    </p>

                    <p class="entries <?php /* left */ ?>">
                        <label>Datum a čas zveřejnění zprávy</label><br />
                        <input class="start date datepicker" type="text" name="press[start]" value="<?=date('Y-m-d')?>" />
                        <input class="time" type="text" name="press[start_time]" value="<?=date('H:i')?>" maxlength="5" />
                        <?php /* <span class="errorMsg">Vyplňte, prosím, toto pole.</span> */ ?>
                    </p>
                    
                    <?php /* <!--<p class="entries right">
                        <label>Datum a čas odstranění zprávy</label><br />
                        <input class="end date datepicker" type="text" name="press[end]" />
                        <input class="time" type="text" name="press[end_time]" />
                        < ?php /* <span class="errorMsg">Vyplňte, prosím, toto pole.</span> * / ? >
                    </p>--> */ ?>

                    <p class="entries submit">
                        <button type="submit" name="add_press">uložit a zavřít</button>
                    </p>
                </fieldset>
            </form>
        </div><!-- #addPost -->

        <div id="unpublishedPressReleases" class="fancybox-popup">
            <h3 class="title">Nepublikované tiskové zprávy</h3>
            <ul class="list">
            <?php
                foreach($unpublished as $key => $val)
                {
                    $val['value'] = unserialize($val['value']);
                    echo printUnpublishedRow($val);
                }
            ?>
            </ul>
        </div><!-- #unpublishedPressReleases -->
    </div><!-- .forBlind -->
    
    <?php
    }
    ?>


</div><!-- #contentAbox .module -->