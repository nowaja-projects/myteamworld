
<div id="contentA" class="module">

    <h2 class="bigTitle">404 - Stránka nebyla nalezena</h2>
    <p class="bigInfo">Je nám líto, požadovanou stránku se nepodařilo nalézt.</p>

    <h3 class="title">Kam dál?</h3>
    <ul>
    	<li>Pokud jste adresu požadované stránky zadávali ručně, zkontrolujte, prosím, zda jste ji zadali správně.</li>
		<li>Můžete se vrátit na <a href="/">hlavní stránku myteamworld.com</a>.</li>
    </ul>
    
</div><!-- #contentA .module -->