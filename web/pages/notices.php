                <div id="contentA">

                    <div class="contentAbox">
                    <h2 class="headline">Upozornění</h2>    
<?php

$notices = Notifications::getAllNotificationsForUser($logged_user->id);

$types = $gnot->getTypes();

$important = array(
    'CONFIRM-TEAM-ENTRY-REQUEST',
    'REQUEST-TEAM-ENTRY',
    'TEAM-INVITATION',
    'CANCEL-TEAM-INVITATION',
    'EVENT-INVITATION',
    'EVENT-ADD-CONFIRM-REQUEST',
    'EVENT-RESULT-CONFIRM-REQUEST',
    'EVENT-EDIT-CONFIRM-REQUEST'
);

// budeme si ukladat do kterych tymu uz byl pozvan a nebudeme stejny tym vickrat davat do dulezitych
$invited = array();

$oldNotices = $newNotices = array();

$last = 'today';
$importantNotifications = array();
$otherNotifications = array();

if(count($notices))
{
    foreach($notices as $key => $notification)
    {
        // tyto zprávy nezobrazujeme
        if($notification->type_id == 'NEW-PERSONAL-MESSAGE')
        {
            continue;
        }
        
		// priznak zda byla notifickace pouzita jako dulezita
		$imp = false;

        // pokud je to dulezita notifikace
        if(in_array($notification->type_id, $important))
        {
            $info = unserialize($notification->info);

            // pokud je to pozvanka do tymu
            if($notification->type_id == 'TEAM-INVITATION')
            {
                $data = array(
                    'id'      => $info['invite_id'],
                    'user_id' => $logged_user->id
                );
                $invite = RosterInvites::getInvite($data);

               	// tak musi byt neodpovezena, aby zustala v dulezitych
                if(count($invite) > 0 && $invite['status'] == 'invited' && !in_array($invite['team_id'], $invited))
                {
                	$imp = true;
                    $invited[] = $invite['team_id'];
                    $importantNotifications[] = $notification;
                }
            }
            // do dulezitych to jde jen kdyz to je neprecteny
            elseif($notification->read == 0)
            {
            	$imp = true;
                $importantNotifications[] = $notification;
            }
        }
        
        // nedulezite
        if($imp === false)
        {
            $otherNotifications[] = $notification;
        }
    }

    if(count($importantNotifications))
    {
        echo '<p class="title">'.$tr->tr('Důležité').'</p>';
            
    	foreach($importantNotifications as $notification)
    	{
			if(!isset($types[$notification->type_id]))
            {
                continue;
            }

			$info = unserialize($notification->info);
            $notification->text = $types[$notification->type_id];
            $notification->replace_patterns();

            switch($notification->type_id)
            {
                case 'TEAM-INVITATION':
                    $class = 'invite inviteTeam';
                    break;

                case 'REQUEST-TEAM-ENTRY':
                    $class = 'invite inviteMember';
                    break;

                case 'EVENT-INVITATION':
                    $class = 'invite';
                    break;

                case 'CANCEL-TEAM-INVITATION':
                    $class = 'cancelSmall';
                    break;

                case 'EVENT-EDIT-CONFIRM-REQUEST':
                case 'EVENT-RESULT-CONFIRM-REQUEST':
                case 'EVENT-ADD-CONFIRM-REQUEST':
                    $class = 'warningSmall';
                    break;
                    
                default:
                    $class = 'nic';
                    break;
            };

            echo '<p class="item icon '.$class.'">';
            echo $notification->text ;
            echo '</p>';
    	}
    }

    $i = 0;
    $today = new DateTime(date('Y-m-d'));
    if(count($otherNotifications) > 0)
    {
        foreach($otherNotifications as $notification)
        {
            if(!isset($types[$notification->type_id]))
            {
                continue;
            }

            $info = unserialize($notification->info);
            $notification->text = $types[$notification->type_id];
            
            switch($notification->type_id)
            {
                case 'ARTICLE-COMMENT':
                case 'NEW-TEAMPOST':
                case 'NEW-OBJECT-ON-TEAM-WALL':
                case 'NEW-PRESS-RELEASE':
                case 'NEW-PRESS-COMMENT':
                    $class = 'comment';
                    break;

                case 'REQUEST-TEAM-ENTRY':
                    $class = 'invite inviteMember';
                    break;

                case 'TEAM-INVITATION':
                    $class = 'invite inviteMember';
                    break;
                	//$notification->text = str_replace('{TEAM-LINKS}', '', $notification->text);

                case 'CONFIRM-TEAM-INVITATION':
                case 'CONFIRM-TEAM-ENTRY-REQUEST':
                case 'NEW-PLAYER-IN-TEAM':
                case 'EVENT-INVITATION':
                    $class = 'invite';
                    break;

                case 'DECLINE-TEAM-INVITATION':
                case 'DECLINE-TEAM-ENTRY-REQUEST':
                case 'DELETE-PLAYER-IN-TEAM':
                    $class = 'leave';
                    break;

                case 'CANCEL-EVENT':
                case 'CANCEL-TEAM-INVITATION':
                case 'EVENT-CANCEL-INVITATION':
                case 'EVENT-DECLINED':
                    $class = 'cancelSmall';
                    break;

                case 'EVENT-CHANGE':
                    $class = 'teamEdit';
                    break;

                case 'EVENT-CONFIRMED':
                case 'RESULT-CONFIRMED':
                    $class = 'confirmSmall';
                    break;

                case 'EVENT-EDIT-CONFIRM-REQUEST':
                case 'EVENT-RESULT-CONFIRM-REQUEST':
                case 'EVENT-ADD-CONFIRM-REQUEST':
                    $class = 'warningSmall';
                    break;

                case 'NEW-EVENT':
                    $class = 'calendarSmall';
                    break;
                    
                default:
                    $class = '';
                    break;
            };
            
            // pokud uz byla vypsana pozvanka do tymu, tak zrusime moznost ji prijmout
            /*if(!$team_invitation)
            {
                $notification->text = str_replace('{TEAM-LINKS}', '', $notification->text);
            }*/
            $notification->replace_patterns();
            
            // dnes
            //$days = ceil((time() - $notification->timestamp)/86400);
            //$date = date('Y-m-d', $notification->timestamp);
            
            // zjistime rozdil dnu
            $datetime1 = new DateTime(date('Y-m-d', $notification->timestamp));
            $interval = $datetime1->diff($today, true);
            $days = $interval->format('%a');

            if($days == 0 && $i++ == 0)
            {
                $title = $tr->tr('Dnes');
                $last = 'today'; 
            }
            // včera
            elseif($days == 1 && $last != 'yesterday')
            {
                $title = $tr->tr('Včera');
                $last = 'yesterday';
            }
            // týden
            elseif($days > 1 && $days < 8 && $last != 'week')
            {
                $title = $tr->tr('Týden');
                $last = 'week';
            }
            elseif($days >= 8 && $last != 'older')
            {
                $title = $tr->tr('Starší');
                $last = 'older';
            }
            else
            {
                $title = '';
            }
            
            if(!empty($title))
            {
                echo '<p class="title">'.$title.'</p>';
            }
            
            echo '<p class="item icon '.$class.'">';
            echo $notification->text ;
            echo '</p>';
            
        }
    }
    else
    {
        echo '<div class="msg warning noClose">'.$tr->tr('Momentálně nemáte žádná upozornění k zobrazení.').'</div>';
    }
}
else
{
    echo '<div class="msg warning noClose">'.$tr->tr('Momentálně nemáte žádná upozornění k zobrazení.').'</div>';
}

/*
?>
                         

                        <p class="title">Dnes</p>

                        <p class="item ico comment"><strong>5 uživatelů</strong> přidalo komentář k článku "<strong><a href="#">Straka: Jsem přesvědčený, že kdokoliv jiný než Švancara by gól nedal</a></strong>".</p> 
                        <p class="item ico comment"><strong><a href="#">Martin Bíza</a></strong> a <strong>5 dalších uživatelů</strong> přidalo komentéř k vašemu příspěvku na nástěnce týmu <strong><a href="#">FBC Royal Gigolos</a></strong>.</p> 
                        <p class="item ico like"><strong>3 uživatelům</strong> se líbí váš příspěvek na nástěnce týmu <strong><a href="#">FBC Royal Gigolos</a></strong>.</p>
                        <p class="item ico comment"><strong><a href="#">Martin Bíza</a></strong> přidal nový příspěvek na nástěnku týmu <strong><a href="#">FBC Royal Gigolos</a></strong>.</p>
                        <p class="item ico comment"><strong><a href="#">Martin Bíza</a></strong> tě pozval do týmu <strong><a href="#">FBC Royal Gigolos</a></strong>. Chceš vstoupit do týmu? <strong><a href="#">ANO</a></strong> / <strong><a href="#">NE</a></strong></p>


                        <p class="title">Včera</p>

                        <p class="item ico newPlayer">Váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> přidal na soupisku nového hráče "<strong><a href="#">Jakub Nowak</a></strong>".</p>
                        <p class="item ico newPlayer">Váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> odstranil ze soupisky hráče "<strong><a href="#">Jakub Nowak</a></strong>".</p>
                        <p class="item ico training">Váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> vytvořil pro den 24. 12. 2013 novou událost "<strong><a href="#">Týmový trénink</a></strong>".</p>
                        <p class="item ico training">Váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> odstranil pro den 24. 12. 2013 událost "<strong><a href="#">Týmový trénink</a></strong>".</p>
                        <p class="item ico newPlayer">Váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> zveřejnil tiskovou zprávu "<strong><a href="#">Straka: Jsem přesvědčený, že kdokoliv jiný než Švancara by gól nedal</a></strong>".</p>


                        <p class="title">Starší</p>

                        <p class="item ico match">Dnes <strong>v 20:00 hod.</strong> odehraje váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> zápas proti <strong><a href="#">FBC Royal Gigolos</a></strong>. Pro tuto událost ještě nemáte vyplněnou <strong><a href="#">docházku</a></strong>!</p>
                        <p class="item ico match">Zítra <strong>v 20:00 hod.</strong> začíná trénink vašeho týmu <strong><a href="#">FBC Royal Gigolos</a></strong></p>
                        <p class="item ico match">Dnes <strong>v 20:00 hod.</strong> začíná vašemu týmu <strong><a href="#">FBC Royal Gigolos</a></strong> zápas proti <strong><a href="#">FBC Royal Gigolos</a></strong>.</p>


                        <p class="title">Jen pro majitele a editory týmů</p>

                        <p class="item ico newPlayer">Váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> obdržel novou výzvu na přátelský zápas od týmu <strong><a href="#">FBC Royal Gigolos</a></strong>.</p>
                        <p class="item ico newPlayer">Váš tým <strong><a href="#">FBC Royal Gigolos</a></strong> ještě neodpověděl na výzvu týmu <strong><a href="#">FBC Royal Gigolos</a></strong>.</p>
                        <p class="item ico newPlayer">K zápasu <strong><a href="#">FBC Royal Gigolos</a></strong>, zítra <strong>v 20:00 hod.</strong>, ještě nevyplnili docházku všichni hráči.</p>
                        <p class="item ico newPlayer">K tréninku <strong><a href="#">FBC Royal Gigolos</a></strong>, zítra <strong>v 20:00 hod.</strong>, mají vyplňenou docházku všichni hráči.</p>
<?php
*/
?>
                    </div><!-- .contentAbox -->


                </div><!-- #contentAbox .module -->