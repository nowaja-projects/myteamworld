<div id="contentA" class="module">

    <?php
    if($logged_user->getActiveTeam()->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id)
    {
    ?>
    <div id="admin">
        <a href="#editRoster" id="editRosterButton" class="button buttonB icon edit popUpMedium">upravit soupisku</a>
        <a href="#addUser" class="button buttonB icon add popUp">přidat člena</a>
    </div>
    <?php
    }
    ?>

    <div class="contentAbox">    
    <?php
    $positions = SportDAO::getPositions($team->sport_id);
    $roster = Rosters::getTeamRoster($team->id);
    $position_select = '';
    foreach($positions as $position)
    {
        $position_select .= '<option value="'.$position->id.'"'.(!empty($position->staff) ? '  data-staff="1"' : '') .'>'.$position->name.'</option>';
        if(isset($roster[$position->id]))
        {
            $players = $roster[$position->id];
        }
        else
        {
            $players = array();
        }
        
        if($_GET['action'] == 'team-profile')
        {
            $showAll = false;
        }
        else
        {
            $showAll = true;
        }

        $showAll = false;

        if($position->staff != 1)
        {
            echo printPositionRoster($position, $players, $showAll);
        }
        else
        {
            echo printStaffRoster($position, $players, $showAll); 
        }
    }
    ?>
    </div><!-- .contentAbox -->



    <?php
    if($team->isAdmin($logged_user))
    {
    ?>

    <div class="forBlind">
        <div id="addUser" class="fancybox-popup">
            <h3 class="title">Pozvat uživatele do týmu</h3>

            <form id="userTypeSwitch" class="popUpForm">
                <fieldset>
                    <legend class="forBlind">Zvolit typ uživatele</legend>

                    <div class="entries first">
                        <h4 class="label">Vyberte typ uživatele</h4>
                        <span class="checkboxBox"><input id="checkboxUser" class="checkbox" type="radio" name="xxx" /><label class="checkbox" for="checkboxUser">Kmenový člen týmu</label></span>
                        <span class="checkboxBox"><input id="checkboxHost" class="checkbox" type="radio" name="xxx" /><label class="checkbox" for="checkboxHost">Hostující hráč</label></span>
                    </div>
                </fieldset>
            </form>

            <form id="popUpFormAddUser" class="popUpForm" action="" method="get" style="display: none;">
                <fieldset>
                    <legend class="forBlind">Pozvat kmenového uživatele do týmu</legend>
                    <input type="hidden" name="player[team_id]" value="<?=$team->id?>" />

                    <p class="entries first">
                        <label>E-mail uživatele</label><br />
                        <input class="text email" type="text" name="player[email]" placeholder="example@email.com" />
                        <span class="errorMsg">Vyplňte, prosím, správně toto pole.</span>
                    </p>

                    <p class="entries last">
                        Nebo můžete uživatele vyhledat na myteamworld.com a pozvat ho do vašeho týmu rovnou!
                    </p>

                    <p class="entries submit">
                        <button type="submit">odeslat pozvánku</button>
                    </p>
                </fieldset>
            </form>

            <form id="popUpFormAddHost" class="popUpForm" action="" method="get" style="display: none;">
                <fieldset>
                    <legend class="forBlind">Pozvat hostujícího hráče do týmu</legend>
                    <input type="hidden" name="host_player[team_id]" value="<?=$team->id?>" />

                    <p class="entries first">
                        <label>E-mail uživatele</label><br />
                        <input class="text email" type="text" name="host_player[email]" placeholder="example@email.com" />
                        <span class="errorMsg one">Vyplňte, prosím, správně následující pole.</span>
                        <span class="errorMsg two">Zvolte, prosím, pouze jednu variantu.</span>
                    </p>

                    <p class="entries last">
                        Nebo pokud se uživatel na myteamworld.com nenachází a&nbsp;není z&nbsp;nějakého důvodu možné, aby se na myteamworld.com sám zaregistroval, můžete ho do vašeho týmu přidat ručně. Hráči bude vytvořen fiktivní účet pouze v&nbsp;rámci toho týmu a&nbsp;budou se k&nbsp;němu ukládat pouze hráčské statistiky!
                        <span class="group">
                            <label class="forBlind">Jméno</label>
                            <input class="text firstname" type="text" name="host_player[name]" placeholder="Jméno" />
                            <label class="forBlind">Příjmení</label>
                            <input class="text surname" type="text" name="host_player[surname]" placeholder="Příjmení" />
                        </span>
                    </p>

                    <p class="entries submit">
                        <button type="submit">odeslat pozvánku</button>
                    </p>
                </fieldset>
            </form>
        </div><!-- #addPlayer -->
    
        <div id="editRoster" class="fancybox-popup">
        <?php
            echo printEditRoster($positions, $roster, $position_select, $team);
        ?>
        </div><!-- #editRoster -->
        
    </div><!-- .forBlind -->
    <?php
    } // team is admin
    ?>



</div><!-- #contentAbox .module -->