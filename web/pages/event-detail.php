<div id="contentA" class="module eventResultDetail">
<?php
include(BLOCK_PATH . 'print-messagess.php');
?>

    <div class="contentAbox">            
     	
	    <h2 class="headline"><?=$title?></h2>

        <?php
        $txt = '';
        if($event->type == Event::FRIENDLY || $event->type == Event::COMPETITION)
        {
	        if(!$isResult)
	        {
	            $txt = $tr->tr('Tento zápas ještě nebyl odehrán, nebo jeho výsledek ještě nebyl zadán.');
	        }
	        elseif(!$event->isResultConfirmed())
	        {
	            $team_name = '';
	            if(!$event->isResultConfirmed($event_data['homeTeam']))
	            {
	                $team_name = $event_data['homeTeam']->getName();
	            }
	            else
	            {
	                $team_name = $event_data['awayTeam']->getName();
	            }
	            $txt = $tr->tr('Výsledek ještě nebyl potvrzen týmem ') . $team_name . '.';
	        }
	    }

        if(!empty($txt))
        {
        ?>
        <div class="infoText">
            <p class="msg warning noMarginLeft noMarginBottom noClose"><?=$txt?></p>
        </div>
        <?php
        }
        ?>

        <?php
        if($event->type == Event::COMPETITION || $event->type == Event::FRIENDLY)
        {
    	?>
        <div id="resultHeader" class="cleaned">
    	    <p class="homeTeam">
                <?php
                if(!$event->isDeleted($event_data['homeTeam']))
                {
                ?>
                    <a class="logo" href="<?=$event_data['homeTeam']->getProfileLink()?>" title="Zobrazit detail týmu <?=$event_data['homeTeam']->getName()?>">
                        <img src="<?=$event_data['homeTeam']->getTeamLogo('big')?>" alt="Logo <?=$event_data['homeTeam']->getName()?>" />
                    </a>
                    <?=$event_data['homeTeam']->getTeamLink()?>
                <?php
                }
                else
                {
                ?>
                    <span class="logo">
                        <img src="<?=$event_data['homeTeam']->getTeamLogo('big')?>" alt="Logo <?=$event_data['homeTeam']->getName()?>" />
                    </span>
                
                    <span><?=$event_data['homeTeam']->getName()?></span>
                <?php
                }
                ?>
            </p>

            <p class="resultBox">
                <span class="date"><?=date('d. m. Y H:i', strtotime($event->start))?></span>
                <?php
                $class = '';
                if(!is_array($event->result) || empty($event->result))
                {
                    $class = ' noResults';
                }


              ?>
                  <span class="finalResult<?=$class?>"><span class="finalResultHome"><?=@$event->result['home']['final']?></span>&nbsp;-&nbsp;<span class="finalResultAway"><?=@$event->result['away']['final']?><span class="type"><?=$event->getExtraTime()?></span></span></span>
                  <?php 
                  if(is_array($event->result) && !empty($event->result))
                  {
                      $pers = '';
                      $periods = array();
                      //printr($event->result);
                      if(!empty($event->result['periods']) && $event->result['periods'] > 1)
                      {
                          for($i = 1; $i <= $event->result['periods']; $i++)
                          {
                                $periods[] = '<span class="resultHome-'.$i.'">'.$event->result['home']['period'.$i].'</span>:<span class="resultAway-'.$i.'">'.$event->result['away']['period'.$i].'</span>';
                          }
                          $pers .= implode(',&nbsp;', $periods);
                      }

                      if(!empty($event->result['isOvertime']))
                      {
                            $pers .= '<span class="overtimeBox">&nbsp;-&nbsp;<span class="overtimeHome">'.$event->result['home']['overtime'].'</span>:<span class="overtimeAway">'.$event->result['away']['overtime'].'</span></span>';
                      }

                      if(!empty($pers))
                      {
                      ?>
                      <span class="results">(&nbsp;<?=$pers?>&nbsp;)</span>
                  <?php
                    }
                  }
                  ?>
                  <span class="place"><?=$event->info[0]['place']?></span>
            </p>
	    	
            <?php
            if($event_data['awayTeam']->id > 0)
            {
            ?> 
            <p class="awayTeam">
              <a class="logo" href="<?=$event_data['awayTeam']->getProfileLink()?>" title="Zobrazit detail týmu <?=$event_data['awayTeam']->getName()?>">
                <img src="<?=$event_data['awayTeam']->getTeamLogo('big')?>" alt="Logo <?=$event_data['awayTeam']->getName()?>" />
              </a>
              <?=$event_data['awayTeam']->getTeamLink()?>
            </p>
            <?php
            }
            else
            {
              //printr($event->info);
              //die;
            ?>
            <p class="awayTeam">
                <span class="logo">
                    <img src="<?=$event_data['homeTeam']->getTeamLogo('big', '/event'.$event->id.'/')?>" alt="Logo <?=$event->info[0]['team_name']?>" />
                </span>
            
              <span><?=$event->info[0]['team_name']?></span>
            </p>
            <?php
            }
            ?>
	    </div><!-- .resultHeader -->
	    <?php
		}
		// trenink nebo tymova akce
		else
		{
		?>
            <div id="eventHeader">
    			<a class="logo" href="<?=$event_data['homeTeam']->getProfileLink()?>" title="Zobrazit detail týmu <?=$event_data['homeTeam']->getName()?>">
                    <img src="<?=$event_data['homeTeam']->getTeamLogo('big')?>" alt="Logo <?=$event_data['homeTeam']->getName()?>" />
                </a>
                <?=$event_data['homeTeam']->getTeamLink()?>
                <span class="info"><?=date('d. m. Y H:i', strtotime($event->start))?>, <?=$event->info[0]['place']?></span>
            </div><!-- #eventHeader -->
        <?php
		}
		?>

	    <div id="resultsContainer" class="cleaned">
        <?php
        if($isResult)
        {
        ?>
            <ul id="stepsNavigation">
                <!--<li class=""><a class="summaryBox" href="#">přehled</a></li>-->
                <li class="active"><a class="rosterBox" href="#">sestavy</a></li>
                <li class=""><a class="statsBox" href="#">statistiky</a></li>
                <li class=""><a class="playersBox" href="#">hráči</a></li>
            </ul><!-- #stepsNavigation -->

            
            <?php /* SUMMARY * / ?>
            <div id="summaryBox" class="tab active cleaned">
            	<table cellspacing="0" cellpadding="0">
            		<tbody>
            			<tr>
            				<td class="time"><!-- --></td>
            				<td class="event"><!-- --></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><span class="name">Hofbrucker P.</span><span class="info">(vlastní gól)</span></td>
            				<td class="time rightSide"><span class="ownGoal" title="vlastní gól">2'</span></td>
            			</tr>
            			<tr>
            				<td class="time"><span class="goal" title="gól">3'</span></td>
            				<td class="event"><span class="name">Bíza M.</span><span class="info">(Maxmilánský M.)</span></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><!-- --></td>
            				<td class="time rightSide"><!-- --></td>
            			</tr>
            			<tr>
            				<td class="time"><span class="yellowCard" title="žlutá karta">25'</span></td>
            				<td class="event"><span class="name">Nowak J.</span></span></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><!-- --></td>
            				<td class="time rightSide"><!-- --></td>
            			</tr>
            			<tr>
            				<td class="time"><span class="yellowRedCard" title="druhá žlutá / červená karta">27'</span></td>
            				<td class="event"><span class="name">Nowak J.</span></span></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><!-- --></td>
            				<td class="time rightSide"><!-- --></td>
            			</tr>
            			<tr>
            				<td class="time"><!-- --></td>
            				<td class="event"><!-- --></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><span class="name">Hofbrucker P.</span></td>
            				<td class="time rightSide"><span class="redCard" title="červená karta">27'</span></td>
            			</tr>
            			<tr>
            				<td class="time"><!-- --></td>
            				<td class="event"><!-- --></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><span class="name">Maxmilánský M.</span><span class="info">(Kovalchuk I.)</span></td>
            				<td class="time rightSide"><span class="subtitions" title="střídání">52'</span></td>
            			</tr>
            			<tr>
            				<td class="time"><span class="goal" title="gól">45+2'</span></td>
            				<td class="event"><span class="name">Maxmilánský M.</span><span class="info">(penalta)</span></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><!-- --></td>
            				<td class="time rightSide"><!-- --></td>
            			</tr>
            			<tr>
            				<td class="separator" colspan="2"><span>2. poločas</span></td>
            				<td class="space"><!-- --></td>
            				<td class="separator" colspan="2"><span>2. poločas</span></td>
            			</tr>
            			<tr>
            				<td class="time"><span class="subtitions" title="střídání">52'</span></td>
            				<td class="event"><span class="name">Maxmilánský M.</span><span class="info">(Kovalchuk I.)</span><span class="info">(Plekanec T.)</span></td>
            				<td class="space"><!-- --></td>
            				<td class="event rightSide"><!-- --></td>
            				<td class="time rightSide"><!-- --></td>
            			</tr>
            		</tbody>
            	</table>
	        </div><!-- #summaryBox -->
              <?php */ ?>

	        <?php /* SESTAVY */ ?>
            <div id="rosterBox" class="tab active cleaned">
	            <div class="left">
              <?php
                function print_rosters($players, $team, $players_count)
                {
                    global $event_data;

                    $return = '';
                    if(!empty($players))
                    {
                        $i = 0;
                        foreach($players as $row)
                        {
                            // pokud uzivatel neexistuje, tak ho muzeme otrimovat
                            $row['player'] = trim($row['player']);
                            
                            if($row['player'] == '000' || empty($row['player']))
                            {
                                continue;
                            }

                            // nacteme si pozici
                            $position = Positions::get($row['position'], $event_data['homeTeam']->sport_id);
                             
                            if(!empty($row['exists']))
                            {
                                // nacteme si info o hraci
                                $player = explode('-', $row['player']);
                                $player = UserDAO::get($player[0]);
                                if($player->isDeleted())
                                {
                                    continue;
                                }
                                
                                $name = '<a class="name" href="'.$player->getProfileLink().'">' . $player->getName() . '</a>';
                            }
                            else
                            {
                                $name = '<span class="name">' . $row['player'] . '</span>';
                            }

                            $return .= '<p class="cleaned'.($i == $players_count && $players_count > 0 ? ' substitutes' : '').'"><span class="position" title="'.$position->name.'">'.$position->short.'</span><span class="number">#'.intval($row['number']).'</span>'.$name.'</p>';

                            $i++;
                        }
                    }
                    else
                    {
                        $return .= '<p class="cleaned">Žádní hráči na soupisce.</p>';
                    }

                    return $return;
                }

                $rosters = unserialize($event->rosters);
                
                if(empty($rosters['home']))
                {
                    $rosters['home'] = array();
                }

                if(empty($rosters['away']))
                {
                    $rosters['away'] = array();
                }

                echo print_rosters($rosters['home'], 'home', @$event->result['players_count']);
              /*
        	        <!--<p class="cleaned"><span class="position">B</span><span class="number">#1</span><span class="name">Maxmiliánský M.</span></p>
                  <p class="cleaned"><span class="position">LO</span><span class="number">#9</span><span class="name">Bíza M.</span></p>
                  <p class="cleaned"><span class="position">SO</span><span class="number">#33</span><span class="name">Nowak J.</span></p>
                  <p class="cleaned"><span class="position">SOZ</span><span class="number">#25</span><span class="name">Flight B.</span></p>
                  <p class="cleaned"><span class="position">HÚ</span><span class="number">#68</span><span class="name">Nowaja J.</span></p>
                  <p class="separator cleaned"><span class="position">B</span><span class="number">#2</span><span class="name">Hofbrucker P.</span></p>
                  <p class="cleaned"><span class="position">SZ</span><span class="number">#17</span><span class="name">Bečka A.</span></p>-->*/
                  ?>
	            </div>

	            <div class="right">
	            	  <?php
                  echo print_rosters($rosters['away'], 'away', @$event->result['players_count']);
                  ?>
	            </div>
	        </div><!-- #rosterBox -->


			<?php /* STATISTIKY */ ?>
			<? /* 0 - 13 - least, 14 - 27 - little , 28 - 41 - less , 42 - 58 - medium , 59 - 72 - more, 73 - 86 - much, 87 - 100 - most */ ?>
               <div id="statsBox" class="tab cleaned">
                    <?php
                    $sport = SportDAO::get($event_data['homeTeam']->sport_id);
                    $available_stats = unserialize($sport->team_stats);
                    $team_stats = unserialize($event->team_stats);
                    $statsHome = array();
                    $statsAway = array();

                    // projdeme vsechny statistiky, ktere mohou nastat
                    foreach($available_stats as $key => $name)
                    {
                        // pokud je statistika zadana
                        if(isset($team_stats[$key]))
                        {
                            // pokud zadal pomlcku tak nevypisujeme
                            if((isset($team_stats[$key]['home']) && $team_stats[$key]['home'] == '-') || (isset($team_stats[$key]['away']) && $team_stats[$key]['away'] == '-'))
                            {
                                continue;
                            }

                            if(empty($team_stats[$key]['home']))
                            {
                                $team_stats[$key]['home'] = 0;
                            }
                            if(empty($team_stats[$key]['away']))
                            {
                                $team_stats[$key]['away'] = 0;
                            }


                            $total = $team_stats[$key]['home'] + $team_stats[$key]['away'];

                            if($total == 0)
                            {
                                $home = 50;
                            }
                            elseif($team_stats[$key]['home'] == 0)
                            {
                                $home = 0;
                            }
                            else
                            {
                                $home = round(100 / $total * $team_stats[$key]['home']);
                            }
                            $away = 100 - $home;

                            $awayClass = $homeClass = 'least';
                            if(inRange($home, 0, 13))
                            {
                                $homeClass = 'least';
                                $awayClass = 'most';
                            }
                            if(inRange($home, 14, 27))
                            {
                                $homeClass = 'little';
                                $awayClass = 'much';
                            }
                            if(inRange($home, 28, 41))
                            {
                                $homeClass = 'less';
                                $awayClass = 'more';
                            }
                            if(inRange($home, 42, 58))
                            {
                                $homeClass = 'medium';
                                $awayClass = 'medium';
                            }
                            if(inRange($home, 59, 72))
                            {
                                $homeClass = 'more';
                                $awayClass = 'less';
                            }
                            if(inRange($home, 73, 86))
                            {
                                $homeClass = 'much';
                                $awayClass = 'little';
                            }
                            if(inRange($home, 87, 100))
                            {
                                $homeClass = 'most';
                                $awayClass = 'least';
                            }

                            $statsHome[] = '<p class="entries cleaned"><span class="name">'.$name.'</span><span class="count">'.$team_stats[$key]['home'].'</span><span class="barBox"><span class="bar '.$homeClass.'" style="width: '.$home.'%;"><!-- --></span></span></p>';
                            $statsAway[] = '<p class="entries cleaned"><span class="name">'.$name.'</span><span class="count">'.$team_stats[$key]['away'].'</span><span class="barBox"><span class="bar '.$awayClass.'" style="width: '.$away.'%;"><!-- --></span></span></p>';
                        }
                  }
                  ?>
                  <div class="left">
	            	<?php echo implode("\n", $statsHome);?>
	            </div>

	            <div class="right">
	            	<?php echo implode("\n", $statsAway);?>
	            </div>
	        </div><!-- #statsBox -->


            <?php /* HRACI */ ?>
            <div id="playersBox" class="tab cleaned">
            <?php
            $stats = StatsDAO::getStatsForEvent($event->id . '-' . $event->team_id, $sport->stat_table);
            $rosters = unserialize($event->rosters);

            if(!isset($stats['home']) || !is_array($stats['home']))
            {
                $stats['home'] = array();
            }

            if(!isset($stats['away']) || !is_array($stats['away']))
            {
                $stats['away'] = array();
            }

            if(!isset($rosters['home']) || !is_array($rosters['home']))
            {
                $rosters['home'] = array();
            }

            if(!isset($rosters['away']) || !is_array($rosters['away']))
            {
                $rosters['away'] = array();
            }

            echo print_players_stats_static($sport->getPositions(), $rosters['home'], 'home', $stats['home']);

            echo print_players_stats_static($sport->getPositions(), $rosters['away'], 'away', $stats['away']);
            ?>
	            	
	        </div><!-- #playersBox -->
        <?php
        }
        else
        {
        	if($team->userCanViewModule('attendance', $logged_user) && ($logged_user->getActiveTeam()->id == $event->team_id || $logged_user->getActiveTeam()->id == $event->oponent_id))
        	{
	        	$positions = SportDAO::getPositions($team->sport_id);
	        	$attend = AttendDAO::getAttendForEvent($event->team_id, $event->id);
	        	$roster = Rosters::getTeamRoster($team->id);

	        	$i = 0;
	        	foreach($positions as $position)
		        {
		            if(isset($roster[$position->id]))
		            {
		                $players = $roster[$position->id];
		            }
		            else
		            {
		                $players = array();
		            }
		            
		            if(count($players))
		            {
			            if($position->staff != 1)
			            {
			                echo eventDetailPossitionAttend($position, $players, $attend, $event, ($i++ == 0 ? ' first' : ''));
			            }
			            else
			            {
			                echo eventDetailPossitionAttend($position, $players, $attend, $event, ($i++ == 0 ? ' first' : ''));
			                // FIXME !!!
			                //echo printStaffAttend($position, $players, $attend, $events); 
			            }
			        }
		        }

		        if(isset($roster[0]))
		        {
		            $data = array(
		                'id'        => '0',
		                'sport_id'  => $position->sport_id,
		                'name'      => 'Nezařazení hráči',
		                'name2'     => 'Nezařazení hráči',
		                'short'     => '',
		                'order'     => '',
		                'staff'     => '0',
		                'stats'     => ''
		            );
		            $position = new Position($data);
		            
		            $players = $roster[0];
		            echo eventDetailPossitionAttend($position, $players, $attend, $event, ($i++ == 0 ? ' first' : ''));
		        }
		    }

/*
        ?>

        <h3 class="attendanceTitle first">Brankáři</h3>
        <table cellspacing="0" cellpadding="0" class="table attendance">
            <thead>
                <tr>
                    <th class="number">#</th>
                    <th class="player">jméno a příjmení</th>
                    <th data-id="2-14" class="attendanceDate">docházka</th>
                </tr>
            </thead>
            <tbody>
                <tr class="first">
                    <td class="number">3</td>
                    <td class="player">
                        <a class="img" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><img alt="Allen Iverson" src="/cache/56-profileimage--small-533c8976bf392-b933385360596a318e898157acd0573ef19ebb66.jpg"></a>
                        <a class="name" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><span class="firstName">Allen</span><br><span class="lastName">Iverson</span></a>
                    </td>
                    <td title="Zúčastním se" data-id="2-14-56@dd024586f3202c0451fd8052b178233ff6a39264a7357dba091d60a196185096" class="attStatus edited yesComment editable"><span class="help"><!-- --></span><span class="tooltip">jjjj</span></td>
                </tr>
                <tr>
                    <td class="number">33</td>
                    <td class="player">
                        <a class="img" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><img alt="Allen Iverson" src="/cache/56-profileimage--small-533c8976bf392-b933385360596a318e898157acd0573ef19ebb66.jpg"></a>
                        <a class="name" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><span class="firstName">Allen</span><br><span class="lastName">Iverson</span></a>
                    </td>
                    <td title="Zúčastním se" data-id="2-14-56@dd024586f3202c0451fd8052b178233ff6a39264a7357dba091d60a196185096" class="attStatus edited yesComment editable"><span class="help"><!-- --></span><span class="tooltip">jjjj</span></td>
                </tr>
            </tbody>
        </table>

        <h3 class="attendanceTitle">Obránci</h3>
        <table cellspacing="0" cellpadding="0" class="table attendance">
            <thead>
                <tr>
                    <th class="number">#</th>
                    <th class="player">jméno a příjmení</th>
                    <th data-id="2-14" class="attendanceDate">docházka</th>
                </tr>
            </thead>
            <tbody>
                <tr class="first">
                    <td class="number">3</td>
                    <td class="player">
                        <a class="img" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><img alt="Allen Iverson" src="/cache/56-profileimage--small-533c8976bf392-b933385360596a318e898157acd0573ef19ebb66.jpg"></a>
                        <a class="name" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><span class="firstName">Allen</span><br><span class="lastName">Iverson</span></a>
                    </td>
                    <td title="Zúčastním se" data-id="2-14-56@dd024586f3202c0451fd8052b178233ff6a39264a7357dba091d60a196185096" class="attStatus edited yesComment editable"><span class="help"><!-- --></span><span class="tooltip">jjjj</span></td>
                </tr>
                <tr>
                    <td class="number">33</td>
                    <td class="player">
                        <a class="img" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><img alt="Allen Iverson" src="/cache/56-profileimage--small-533c8976bf392-b933385360596a318e898157acd0573ef19ebb66.jpg"></a>
                        <a class="name" title="Zobrazit uživatele Allen Iverson" href="http://beta.myteamworld.com/user-profile/allen-iverson-56/"><span class="firstName">Allen</span><br><span class="lastName">Iverson</span></a>
                    </td>
                    <td title="Zúčastním se" data-id="2-14-56@dd024586f3202c0451fd8052b178233ff6a39264a7357dba091d60a196185096" class="attStatus edited yesComment editable"><span class="help"><!-- --></span><span class="tooltip">jjjj</span></td>
                </tr>
            </tbody>
        </table>
*/?>
        <div class="forBlind">
            <div id="addAttendance" class="fancybox-popup">
                <h3 class="title">Vyplnit docházku</h3>

                <form id="popUpFormAddAttendance" class="popUpForm" action="" method="get">
                    <fieldset>
                        <legend class="forBlind">Vyplnit docházku</legend>

                        <div class="entries first">
                            <input type="hidden" name="attendance[event-id]" id="eventID" />
                        
                            <label>Vyberte odpověď</label><br />
                            <ul class="attStatus list cleaned">
                              <li class="yes"><a href="#">Zúčastním se</a></li>
                              <li class="no"><a href="#">Nezúčastním se</a></li>
                              <li class="na"><a href="#">Zatím nevím</a></li>
                            </ul>
                            <span class="forBlind">
                              <input class="yes" value="yes" type="radio" name="attendance[status]" />
                              <input class="no" value="no" type="radio" name="attendance[status]" />
                              <input class="na" value="na" type="radio" name="attendance[status]" />
                            </span>
                            <span class="errorMsg">Vyberte, prosím, vaši odpověď.</span>
                        </div>

                        <?php
                        if($team->isAdmin($logged_user))
                        {
                        ?>
                        <div class="entries admin">
                            <ul class="attStatus list cleaned">
                              <li class="blackDot"><a href="#">Neomluvená neúčast</a></li>
                            </ul>
                            <span class="forBlind">
                              <input class="blackDot" value="blackDot" type="radio" name="attendance[status]" />
                            </span>
                        </div>
                        <?php
                        }
                        ?>

                        <p class="entries">
                            <label for="attendanceText">Text - důvod</label><br />
                            <textarea id="attendanceText" rows="10" cols="2" name="attendance[text]"></textarea>
                        </p>

                        <p class="entries submit">
                            <button id="attendanceSubmit" class="submitButton" type="submit">uložit a zavřít</button>
                        </p>
                    </fieldset>
                </form>
            </div><!-- #modalboxAttendance -->
        </div>

        <?php  
        }
        ?>
	    </div><!-- .resultContainer -->
                   
                 
    </div><!-- .contentAbox -->

</div><!-- #contentA .module -->