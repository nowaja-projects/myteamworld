<?php
if(empty($team_profile))
{
    $team_profile = false;
}
?>


<div id="contentA" class="module">
<?php
    include(BLOCK_PATH . 'print-messagess.php');

    if(!$team_profile)
    {

        // nacteme si aktualni stav filteru ze session
        $filter = $_project['session']->get('filter');
        
        if(!is_array($filter))
        {
            $filter = array();
        }

        if(isset($filter['types']) && count($logged_user->getTeamList()) == 0)
        {
            $filter = array();
        }
        ?>
    
        <!-- FILTR PRO UZIVATELE BEZ TEAMU -->
        <?php
        // filtr zobrazime pouze, pokud ma uzivatel vybrany nejaky novinky
        $sports = $logged_user->getUserSports();
        if(count($sports) > 0 && count($logged_user->getTeamList()) == 0)
        {
        ?>
            <div id="filter" class="smallFilter cleaned">
                <div class="inner">
                    <span class="filterButton">nastavit filtr zpráv</span>
                </div><!-- .inner -->    
            </div><!-- .smallFilter -->
    
        <!-- KONEC FILTR PRO UZIVATELE BEZ TEAMU -->
    
        <?php 
        }
        elseif(count($logged_user->getTeamList()) > 0 && !empty($logged_user->getActiveTeam()->id))
        {
            include(BLOCK_PATH.'wall-add-post.php');
        }
        
        ?>
        
        <div id="filterDiv" class="box filter clear<?=(count($filter) > 0 ? ' open' : '')?>">
            <div class="content">
            <form action="" method="post">
                <fieldset>
                    <legend class="forBlind">Filtr</legend>
                    <p class="title"><input name="filter[category][]" value="categorySport" type="checkbox" id="filterCategorySport"<?=(count($filter) == 0 || (isset($filter['category']) && in_array('categorySport', $filter['category'])) ? ' checked="checked"' : '')?> /><label for="filterCategorySport">Sportovní novinky</label></p>
                    <ul class="list news cleaned">
                    <?php
                    if(isset($sports) && is_array($sports))
                    {
                        foreach($sports as $id => $name)
                        {
                            if(!empty($name))
                            {
                        ?>
                            <li class="item"><input name="filter[sports][]" <?=((count($filter) == 0 || @in_array($id, @$filter['sports'])) ? ' checked="checked"' : '')?> value="<?=$id?>" type="checkbox" id="filter-<?=friendly_url($name)?>" /><label for="filter-<?=friendly_url($name)?>"><?=$name?></label></li>
                        <?php
                            } 
                        }
                    }
                    ?>
                    </ul><!-- .news -->
                </fieldset>
                <?php
                $logged_user->getTeamList();
                if(count($logged_user->teams) || count($logged_user->fanTeams) || count($logged_user->observeTeam))
                {
                    $types = 4;
                ?>
                
                <fieldset>
                    <p class="title"><input name="filter[category][]" value="categoryTeam" type="checkbox" id="filterCategoryTeam"<?=(count($filter) == 0 || (isset($filter['category']) && in_array('categoryTeam', $filter['category'])) ? ' checked="checked"' : '')?> /><label for="filterCategoryTeam">Týmové novinky</label></p>
                    <ul class="list teamNews cleaned">
                        <li class="item"><input name="filter[types][]" <?=(count($filter) == 0 || (is_array(@$filter['types']) && in_array('event-result', @$filter['types'])) ? ' checked="checked"' : '')?> value="event-result" type="checkbox" id="filterEventResult"/><label for="filterEventResult">Výsledky zápasů</label></li>

                        <?php if(count($logged_user->teams)) { 
                            $types++;
                        ?>
                        <li class="item"><input name="filter[types][]" <?=(count($filter) == 0 || (is_array(@$filter['types']) && in_array('teampost', @$filter['types'])) ? ' checked="checked"' : '')?> value="teampost" type="checkbox" id="filterTeamPost"/><label for="filterTeamPost">Týmové vzkazy</label></li>
                        <?php } ?>

                        <li class="item"><input name="filter[types][]" <?=(count($filter) == 0 || (is_array(@$filter['types']) && in_array('press', @$filter['types'])) ? ' checked="checked"' : '')?> value="press" type="checkbox" id="filterPressReleases"/><label for="filterPressReleases">Tiskové zprávy</label></li>
                        <li class="item"><input name="filter[types][]" <?=(count($filter) == 0 || (is_array(@$filter['types']) && in_array('roster', @$filter['types'])) ? ' checked="checked"' : '')?> value="roster" type="checkbox" id="filterRoster"/><label for="filterRoster">Soupisky</label></li>
                        <li class="item"><input name="filter[types][]" <?=(count($filter) == 0 || (is_array(@$filter['types']) && in_array('gallery', @$filter['types'])) ? ' checked="checked"' : '')?> value="gallery" type="checkbox" id="filterGallery"/><label for="filterGallery">Fotogalerie</label></li>
                    </ul><!-- .news -->
                </fieldset>

                <?php
                }

                $count = count(@$filter['sports']) + count(@$filter['types']);
                ?>
    
                <fieldset class="last cleaned">
                    <div class="searchAll">
                        <input type="checkbox"<?=($count == 0 || $count == (count($sports) + $types) ? ' checked="checked"' : '')?> class="check-all" id="filterAll" /><label for="filterAll">Vybrat všechny novinky</label>
                    </div><!-- .searchAll -->
    
                    <div class="searchButton">
                        <input type="hidden" value="yes" name="filter[sent]" />
                        <button class="button buttonA small" type="submit">použít fitr</button>
                    </div><!-- .searchButton -->
                </fieldset>
    
            </form>
            </div><!-- .content -->
        </div><!-- .filter -->

    <?php
        // start, forceload, filter
        $wall = $logged_user->getWall(0, true, $filter);
    }
    else
    {
        $wall = $team->getWall(0, true, $logged_user);
    }
    
    echo getWall($wall, false, true);
    ?>

    

</div><!-- #contentA .module -->