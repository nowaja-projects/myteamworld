<div id="contentA" class="module eventResultEdit">

    <div class="contentAbox">            
     
     	<h2 class="headline"><?=$title?></h2> 
     	<?php /* toto bude po nastaveni
	    <h2 class="headline">Zadat výsledek události</h2>
	     */ ?>

	    <div id="resultHeader" class="cleaned">
	    	<p class="homeTeam">
	    		<a class="logo" href="<?=$event_data['homeTeam']->getProfileLink()?>" title="Zobrazit detail týmu <?=$event_data['homeTeam']->getName()?>">
	    			<img src="<?=$event_data['homeTeam']->getTeamLogo('big')?>" alt="Logo <?=$event_data['homeTeam']->getName()?>" />
	    		</a>
	    		<?=$event_data['homeTeam']->getTeamLink()?>
	    	</p>

	    	<p class="resultBox">
	    		<span class="date"><?=date('d. m. Y H:i', strtotime($event->start))?></span>
	    		<?php
	    		$class = '';
	    		if(!is_array($event->result) || empty($event->result))
	    		{
	    			$class = ' noResults';
	    		}

	    		$showOvertime = $showPenalties = $showShootout = false;
	    		// pokud muze mit sport prodlouzeni
	    		if(!empty($sport->overtime))
	    		{
	    			$showOvertime = true;
	    		}
	    		
	    		if(!empty($sport->penalties))
    			{
    				$showPenalties = true;
    			}
    			
    			// nebo samostatne najezdy?
    			if(!empty($sport->shootout))
    			{
    				$showShootout = true;
    			}

    			$extra = '';
    			if(!empty($event->result['isPenalties']) && $showPenalties)
    			{
    				$extra = 'pen.';
    			}
    			elseif(!empty($event->result['isShootout']) && $showShootout)
    			{
    				$extra = 'sn';
    			}
    			elseif(!empty($event->result['isOvertime']) && $showOvertime)
    			{
    				$extra = 'pp';
    			}

    			?>
	    		<span class="finalResult<?=$class?>"><span class="finalResultHome"><?=@$event->result['home']['final']?></span>&nbsp;-&nbsp;<span class="finalResultAway"><?=@$event->result['away']['final']?><span class="type"><?=$extra?></span></span></span>
	    		<?php 
	    		$periodsCount = 3;
	    		if(is_array($event->result) && !empty($event->result))
	    		{
	    			$periodsCount = intval($event->result['periods']);
					$pers = '';
					$periods = array();
					//printr($event->result);
					if($periodsCount > 1)
					{
						for($i = 1; $i <= $periodsCount; $i++)
						{
						    $periods[] = '<span class="resultHome-'.$i.'">'.$event->result['home']['period'.$i].'</span>:<span class="resultAway-'.$i.'">'.$event->result['away']['period'.$i].'</span>';
						}
						$pers .= implode(',&nbsp;', $periods);
					}					

					// pokud bylo prodlouzeni
					if(!empty($event->result['isOvertime']) && $showOvertime)
					{
					    $pers .= '<span class="overtimeBox">&nbsp;-&nbsp;<span class="overtimeHome">'.$event->result['home']['overtime'].'</span>:<span class="overtimeAway">'.$event->result['away']['overtime'].'</span></span>';
					}

					if(!empty($pers))
					{
					?>
					<span class="results">(&nbsp;<?=$pers?>&nbsp;)</span>
				<?php
					}
	    		}
	    		?>
	    		<span class="place"><?=$event->info[0]['place']?></span>
	    	</p>


	    	<?php
	    	if($event_data['awayTeam']->id > 0)
	    	{
	    	?> 
	    	<p class="awayTeam">
	    		<a class="logo" href="<?=$event_data['awayTeam']->getProfileLink()?>" title="Zobrazit detail týmu <?=$event_data['awayTeam']->getName()?>">
	    			<img src="<?=$event_data['awayTeam']->getTeamLogo('big')?>" alt="Logo <?=$event_data['awayTeam']->getName()?>" />
	    		</a>
	    		<?=$event_data['awayTeam']->getTeamLink()?>
	    	</p>
	    	<?php
	    	}
	    	else
	    	{
	    		//printr($event->info);
	    		//die;
    		?>
    		<p class="awayTeam">
    			<span class="logo">
    				<img src="<?=$event_data['homeTeam']->getTeamLogo('big', '/event'.$event->id.'/')?>" alt="Logo <?=$event->info[0]['team_name']?>" />
				</span>
				
	    		<span><?=$event->info[0]['team_name']?></span>
	    	</p>
    		<?php
	    	}
	    	?>



	    </div><!-- .resultHeader -->

	    <form id="resultsContainer" class="cleaned">
	    	<div class="left">
            	<div class="eventOptions">
			    	<fieldset>
                        <input type="hidden" name="result[settings]" value="sent" />
                        <input type="hidden" name="result[event_id]" value="<?=$event->id?>" />
                        <input type="hidden" name="result[team_id]" value="<?=$event->team_id?>" />
			    		<p class="entries cleaned">
			    			<label>části zápasu</label>
			    			<select class="position" name="result[periods]">
					    		<option value="1"<?=$periodsCount == 1 ? ' selected="selected"' : ''?>>bez částí</option>
					    		<option value="2"<?=$periodsCount == 2 ? ' selected="selected"' : ''?>>2 části</option>
					    		<option value="3"<?=$periodsCount == 3 ? ' selected="selected"' : ''?>>3 části</option>
					    		<option value="4"<?=$periodsCount == 4 ? ' selected="selected"' : ''?>>4 části</option>
					    		<option value="5"<?=$periodsCount == 5 ? ' selected="selected"' : ''?>>5 části</option>
					    		<option value="6"<?=$periodsCount == 6 ? ' selected="selected"' : ''?>>6 části</option>
					    	</select>
					    </p>
                        <?php
                        $sport = SportDAO::get($logged_user->getActiveTeam()->sport_id);
                        $sport->players = unserialize($sport->players);
                        ?>
			    		<p class="entries cleaned">
					    	<label>počet hráčů</label>
					    	<select class="position" name="result[players]">
                            <?php
                            foreach($sport->players as $key => $p)
                            {
                            ?>
					    		<option value="<?=$key?>"<?=(@$event->result['players_count'] == $key ? ' selected="selected"' : '')?>><?=$p?></option>
                            <?php
                            }
                            ?>
					    	</select>
					    </p>
					</fieldset>
				</div>
				<p class="saveOptions">
	            	<a class="button buttonC">uložit nastavení události</a>
	            </p>
			</div> <!-- left -->
	    </form> <!-- resultsContainer -->

	    <? /* * / ?>
	    <p id="stepError" class="msg error noClose" style="display: none;">Než přejdete na další krok, je nutné vyplnit správně následující položky.</p>

	    <div id="resultsContainer" class="cleaned">
            <ul id="stepsNavigation">
                <li class="active"><a class="resultBox" href="#">výsledek</a></li>
                <li class="disabled"><a class="rosterBox" href="#">sestavy</a></li>
                <li class="disabled"><a class="statsBox" href="#">statistiky</a></li>
                <li class="disabled"><a class="playersBox" href="#">hráči</a></li>
                <li class="disabled"><a class="summaryBox" href="#">přehled</a></li>
            </ul><!-- #stepsNavigation -->


            <?php /* VYSLEDEK * / ?>
            <div id="resultBox" class="tab active cleaned">
	            <div class="left">
	            	<form class="resultEdit" action="">
				    	<fieldset>

				    		<p class="entries cleaned">
						    	<label>konečný výsledek</label>
						    	<input class="input text required" data-name="finalResultHome" type="text" />
						    </p>

						    <p class="entries cleaned">
					    		<label>po 1. části</label>
					    		<input class="input text" data-name="resultHome-1" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>po 2. části</label>
					    		<input class="input text" data-name="resultHome-2" type="text" />
					    	</p>

					    	<p class="entries typeCheckbox cleaned">
					    		<label>prodloužení</label>
					    		<input class="checkbox overtime" type="checkbox" data-name="overtime" value="1" />
					    	</p>

					    	<p class="entries typeCheckbox cleaned">
					    		<label>sam. nájezdy</label>
					    		<input class="checkbox penalty" type="checkbox" data-name="penalty" value="" />
					    	</p>

					    	<p class="entries overtime forBlind cleaned">
					    		<label>prodloužení</label>
					    		<input class="input text" data-name="overtimeHome" type="text" />
					    	</p>

						</fieldset>
				    </form>   
	            </div>

	            <div class="right">
	            	<form class="resultEdit" action="">
				    	<fieldset>

				    		<p class="entries cleaned">
						    	<label>konečný výsledek</label>
						    	<input class="input text required" data-name="finalResultAway" type="text" />
						    </p>

						    <p class="entries cleaned">
					    		<label>po 1. části</label>
					    		<input class="input text" data-name="resultAway-1" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>po 2. části</label>
					    		<input class="input text" data-name="resultAway-2" type="text" />
					    	</p>

					    	<p class="entries overtime forBlind cleaned">
					    		<label>prodloužení</label>
					    		<input class="input text" data-name="overtimeAway" type="text" />
					    	</p>

						</fieldset>
				    </form>  
	            </div>

	            <p class="nextLink">
	            	<a class="button buttonC">uložit a přejít na další krok</a>
	            </p>
	        </div><!-- #resultBox -->

	        <?php /* SESTAVY * / ?>
            <div id="rosterBox" class="tab cleaned">
	            <div class="left">
	            	<form class="rosterEdit" action="">
				    	<fieldset>

				    		<p class="entries cleaned">
						    	<select class="position" data-name="positionHome-1">
						    		<option selected="selected" value="Brankář">Brankář</option>
						    		<option value="Obránce">Obránce</option>
						    		<option value="Levé křídlo">Levé křídlo</option>
						    		<option value="Pravé křídlo">Pravé křídlo</option>
						    		<option value="Center">Center</option>
						    	</select>
						    	<select class="number" data-name="yyy">
						    		<option selected="selected" value="000">0</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<select class="player required" data-name="playerHome-1">
						    		<option value="000">Hráč</option>
						    		<option value="biza">Bíza M.</option>
						    		<option value="nowak">Nowak J.</option>
						    		<option value="maxmiliansky">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option selected="selected" value="">Obránce</option>
						    		<option value="">Brankář</option>
						    		<option value="">Levé křídlo</option>
						    		<option value="">Pravé křídlo</option>
						    		<option value="">Center</option>
						    	</select>
						    	<select class="number" data-name="yyy">
						    		<option selected="selected" value="000">0</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<select class="player required" data-name="yxz">
						    		<option selected="selected" value="000">Hráč</option>
						    		<option value="biza">Bíza M.</option>
						    		<option value="nowak">Nowak J.</option>
						    		<option value="maxmiliansky">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option selected="selected" value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levý obránce</option>
						    		<option value="">Pravý obránce</option>
						    		<option value="">Stoper</option>
						    	</select>
						    	<select class="number" data-name="zzz">
						    		<option selected="selected" value="000">0</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<select class="player required" data-name="yxz">
						    		<option selected="selected" value="000">Hráč</option>
						    		<option value="biza">Bíza M.</option>
						    		<option value="nowak">Nowak J.</option>
						    		<option value="maxmiliansky">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="entries substitutes cleaned">
						    	<select class="position" data-name="xxx">
						    		<option selected="selected" value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levý obránce</option>
						    		<option value="">Pravý obránce</option>
						    		<option value="">Stoper</option>
						    	</select>
						    	<select class="number" data-name="zzz">
						    		<option selected="selected" value="9">9</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<select class="player" data-name="yxz">
						    		<option selected="selected" value="0">Náhradník</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option selected="selected" value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levý obránce</option>
						    		<option value="">Pravý obránce</option>
						    		<option value="">Stoper</option>
						    	</select>
						    	<select class="number" data-name="zzz">
						    		<option selected="selected" value="9">9</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<select class="player" data-name="yxz">
						    		<option selected="selected" value="0">Náhradník</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="more addPlayer">
						   		<a href="#" class="button buttonB icon add">přidat dalšího hráče</a>
						   	</p>

						</fieldset>
				    </form>   
	            </div>

	            <div class="right">
	            	<form class="rosterEdit" action="">
				    	<fieldset>

				    		<p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levé křídlo</option>
						    		<option value="">Pravé křídlo</option>
						    		<option value="">Center</option>
						    	</select>
						    	<select class="number" data-name="yyy">
						    		<option selected="selected" value="33">33</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<select class="player" data-name="yxz">
						    		<option selected="selected" value="0">Hráč</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levé křídlo</option>
						    		<option value="">Pravé křídlo</option>
						    		<option value="">Center</option>
						    	</select>
						    	<select class="number" data-name="yyy">
						    		<option selected="selected" value="99">99</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<select class="player" data-name="yxz">
						    		<option selected="selected" value="0">Hráč</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levé křídlo</option>
						    		<option value="">Pravé křídlo</option>
						    		<option value="">Center</option>
						    	</select>
						    	<select class="number" data-name="zzz">
						    		<option selected="selected" value="9">9</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<input type="text" class="input text" />
						    </p>

						    <p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levé křídlo</option>
						    		<option value="">Pravé křídlo</option>
						    		<option value="">Center</option>
						    	</select>
						    	<select class="number" data-name="zzz">
						    		<option selected="selected" value="9">9</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<input type="text" class="input text" />
						    </p>

						   	<p class="entries substitutes cleaned">
						    	<select class="position" data-name="xxx">
						    		<option value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levé křídlo</option>
						    		<option value="">Pravé křídlo</option>
						    		<option value="">Center</option>
						    	</select>
						    	<select class="number" data-name="zzz">
						    		<option selected="selected" value="9">9</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<input type="text" class="input text" />
						    </p>

						    <p class="entries cleaned">
						    	<select class="position" data-name="xxx">
						    		<option value="">Brankář</option>
						    		<option value="">Obránce</option>
						    		<option value="">Levé křídlo</option>
						    		<option value="">Pravé křídlo</option>
						    		<option value="">Center</option>
						    	</select>
						    	<select class="number" data-name="zzz">
						    		<option selected="selected" value="9">9</option>
						    		<option value="1">1</option>
						    		<option value="2">2</option>
						    		<option value="3">3</option>
						    		<option value="4">4</option>
						    	</select>
						    	<input type="text" class="input text" />
						    </p>

						   	<p class="more addPlayer">
						   		<a href="#" class="button buttonB icon add">přidat dalšího hráče</a>
						   	</p>

						</fieldset>
				    </form>  
	            </div>

	            <p class="nextLink">
	            	<a class="button buttonC">uložit a přejít na další krok</a>
	            </p>
	        </div><!-- #rosterBox -->

	        <?php /* STATISTIKY * / ?>
            <div id="statsBox" class="tab cleaned">
	            <div class="left">
	            	<form class="statsEdit" action="">
				    	<fieldset>

	            			<p class="entries cleaned">
						    	<label>Držení míče</label>
						    	<input class="input text" data-name="ballPositionHome" type="text" />
						    </p>

						    <p class="entries cleaned">
					    		<label>Střely celkem</label>
					    		<input class="input text" data-name="shotsHome" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Střely na branku</label>
					    		<input class="input text" data-name="shotsOnGoalHome" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Rohové kopy</label>
					    		<input class="input text" data-name="cornersHome" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Ofsajdy</label>
					    		<input class="input text" data-name="offsidesHome" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Fauly</label>
					    		<input class="input text" data-name="faulsHome" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Žluté karty</label>
					    		<input class="input text" data-name="yellowCardHome" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Červené karty</label>
					    		<input class="input text" data-name="redCardHome" type="text" />
					    	</p>

					    </fieldset>
					</form>
	            </div>

	            <div class="right">
	            	<form class="statsEdit" action="">
				    	<fieldset>
				    		
	            			<p class="entries cleaned">
						    	<label>Držení míče</label>
						    	<input class="input text" data-name="ballPositionAway" type="text" />
						    </p>

						    <p class="entries cleaned">
					    		<label>Střely celkem</label>
					    		<input class="input text" data-name="shotsAway" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Střely na branku</label>
					    		<input class="input text" data-name="shotsOnGoalAway" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Rohové kopy</label>
					    		<input class="input text" data-name="cornersAway" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Ofsajdy</label>
					    		<input class="input text" data-name="offsidesAway" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Fauly</label>
					    		<input class="input text" data-name="faulsAway" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Žluté karty</label>
					    		<input class="input text" data-name="yellowCardAway" type="text" />
					    	</p>

					    	<p class="entries cleaned">
					    		<label>Červené karty</label>
					    		<input class="input text" data-name="redCardAway" type="text" />
					    	</p>

					    </fieldset>
					</form>
	            </div>

	            <p class="nextLink">
	            	<a class="button buttonC">uložit a přejít na další krok</a>
	            </p>
	        </div><!-- #statsBox -->


	        <?php /* HRACI * / ?>
            <div id="playersBox" class="tab cleaned">
	            <div class="left">
	            	<form class="playersEdit" action="">
				    	<fieldset>

				    		<p class="entries legend cleaned">
				    			<span>S</span><span>Z</span><span>TM</span>
				    		</p>

	            			<p class="entries cleaned">
						    	<label class="cleaned"><span class="number">#33</span><span class="name">Bíza M.</span></label>
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    </p>

						    <p class="entries legend cleaned">
				    			<span>G</span><span>A</span><span>+/-</span><span>TM</span>
				    		</p>

						    <p class="entries cleaned">
						    	<label class="cleaned"><span class="number">#2</span><span class="name">Nowak J.</span></label>
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    </p>

						    <p class="entries cleaned">
						    	<label class="cleaned"><span class="number">#47</span><span class="name">Maxmiliánský M.</span></label>
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    </p>

					    </fieldset>
					</form>
	            </div>

	            <div class="right">
	            	<form class="playersEdit" action="">
				    	<fieldset>
				    		
	            			<p class="entries legend cleaned">
				    			<span>S</span><span>Z</span><span>TM</span>
				    		</p>

	            			<p class="entries cleaned">
						    	<label class="cleaned"><span class="number">#33</span><span class="name">Bíza M.</span></label>
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    </p>

						    <p class="entries legend cleaned">
				    			<span>G</span><span>A</span><span>+/-</span><span>TM</span>
				    		</p>

						    <p class="entries cleaned">
						    	<label class="cleaned"><span class="number">#2</span><span class="name">Nowak J.</span></label>
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    </p>

						    <p class="entries cleaned">
						    	<label class="cleaned"><span class="number">#47</span><span class="name">Maxmiliánský M.</span></label>
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    	<input class="input text" data-name="xxx" type="text" />
						    </p>

					    </fieldset>
					</form>
	            </div>

	            <p class="nextLink">
	            	<a class="button buttonC">uložit a přejít na další krok</a>
	            </p>
	        </div><!-- #playersBox -->


	        <?php /* PREHLED * / ?>
            <div id="summaryBox" class="tab cleaned">
	            <div class="left">
	            	<form class="summaryEdit" action="">
				    	<fieldset>

	            			<p class="entries cleaned">
						    	<select class="time" data-name="xxx">
						    		<option selected="selected" value="1">1´</option>
						    		<option value="2">2´</option>
						    		<option value="3">33´</option>
						    		<option value="45+2">45+2´</option>
						    	</select>
						    	<select class="type" data-name="zzz">
						    		<option selected="selected" value="">Gól ze hry</option>
						    		<option value="">Gól z přímého kopu</option>
						    		<option value="">Gól z penalty</option>
						    		<option value="">Neproměněná penalta</option>
						    		<option value="">Žlutá karta</option>
						    		<option value="">Červená karta po 2 žluté</option>
						    		<option value="">Červená karta</option>
						    		<option value="">Střídání</option>
						    	</select>
						    	<select class="first player" data-name="yyy">
						    		<option selected="selected" value="0">Autor gólu</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    	<select class="second player" data-name="yxz">
						    		<option selected="selected" value="0">Asistence</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="entries cleaned">
						    	<select class="time" data-name="xxx">
						    		<option selected="selected" value="1">1´</option>
						    		<option value="2">2´</option>
						    		<option value="3">33´</option>
						    		<option value="45+2">45+2´</option>
						    	</select>
						    	<select class="type" data-name="zzz">
						    		<option selected="selected" value="">Gól ze hry</option>
						    		<option value="">Gól z přímého kopu</option>
						    		<option value="">Gól z penalty</option>
						    		<option value="">Neproměněná penalta</option>
						    		<option value="">Žlutá karta</option>
						    		<option value="">Červená karta po 2 žluté</option>
						    		<option value="">Červená karta</option>
						    		<option value="">Střídání</option>
						    	</select>
						    	<select class="first player small" data-name="yyy">
						    		<option selected="selected" value="0">Autor gólu</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    	<select class="second player small" data-name="yxz">
						    		<option selected="selected" value="0">Asistence</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    	<select class="third player small" data-name="yxz">
						    		<option selected="selected" value="0">Asistence</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="more addSummary">
						   		<a href="#" class="button buttonB icon add">přidat další událost</a>
						   	</p>

						</fieldset>
					</form>
	            </div>

	            <div class="right">
	            	<form class="summaryEdit" action="">
				    	<fieldset>

	            			<p class="entries cleaned">
						    	<select class="time" data-name="xxx">
						    		<option selected="selected" value="1">1´</option>
						    		<option value="2">2´</option>
						    		<option value="3">33´</option>
						    		<option value="45+2">45+2´</option>
						    	</select>
						    	<select class="type" data-name="zzz">
						    		<option selected="selected" value="">Gól ze hry</option>
						    		<option value="">Gól z přímého kopu</option>
						    		<option value="">Gól z penalty</option>
						    		<option value="">Neproměněná penalta</option>
						    		<option value="">Žlutá karta</option>
						    		<option value="">Červená karta po 2 žluté</option>
						    		<option value="">Červená karta</option>
						    		<option value="">Střídání</option>
						    	</select>
						    	<select class="first player" data-name="yyy">
						    		<option selected="selected" value="0">Autor gólu</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    	<select class="second player" data-name="yxz">
						    		<option selected="selected" value="0">Asistence</option>
						    		<option value="">Bíza M.</option>
						    		<option value="">Nowak J.</option>
						    		<option value="">Maxmiliánský M.</option>
						    	</select>
						    </p>

						    <p class="more addSummary">
						   		<a href="#" class="button buttonB icon add">přidat další událost</a>
						   	</p>

						</fieldset>
					</form>
	            </div>

	            <p class="nextLink finish">
	            	<a class="button buttonC">uložit a dokončit</a>
	            </p>
	        </div><!-- #resultBox -->

	    </div><!-- .resultContainer -->



	    <form id="resultForm" class="forBlin" method="get" action="">
	    	<fieldset>

		    	<input class="finalResultHome" type="text" />
		    	<input class="finalResultAway" type="text" />

		    	<input class="resultHome-1" type="text" />
		    	<input class="resultAway-1" type="text" />

		    	<input class="overtime" type="text" />

		    	<input class="positionHome-1" type="text" />
		    	<input class="playerHome-1" type="text" />

		    	<input class="ballPositionHome" type="text" />
		    	<input class="ballPositionAway" type="text" />

		    	<input class="shotsHome" type="text" />
		    	<input class="shotsAway" type="text" />

		    	<input class="shotsOnGoalHome" type="text" />
		    	<input class="shotsOnGoalAway" type="text" />

		    	<input class="cornersHome" type="text" />
		    	<input class="cornersAway" type="text" />

		    	<input class="offsidesHome" type="text" />
		    	<input class="offsidesAway" type="text" />

		    	<input class="faulsHome" type="text" />
		    	<input class="faulsAway" type="text" />

		    	<input class="yellowCardHome" type="text" />
		    	<input class="yellowCardAway" type="text" />

		    	<input class="redCardHome" type="text" />
		    	<input class="redCardAway" type="text" />

                <button type="submit">Odeslat formulář</button>

			</fieldset>
	    </form>    

	    */ ?>                    
                 
    </div><!-- .contentAbox -->

</div><!-- #contentA .module -->
