<?php

if(isset($_GET['id']) && !empty($_GET['id']) && $_GET['subaction'] != 'gallery-detail' && $_GET['subaction'] != 'event-detail')
{
    require_once(PAGE_PATH . 'news-detail.php');
}
else
{
    switch(@$_GET['subaction'])
    {
        case 'wall':
            $team_profile = true;
            require_once(PAGE_PATH . 'wall.php');
            break;
        
        case 'gallery':
            require_once(PAGE_PATH . 'gallery.php');
            break;
        
        case 'gallery-detail':
            //require_once('action/gallery-detail.action.php');
            require_once(PAGE_PATH . 'gallery-detail.php');
            break;

        case 'stats':
            require_once(PAGE_PATH . 'stats.php');
            break;
        
        case 'roster':
            require_once(PAGE_PATH . 'roster.php');
            break;
        
        case 'events':
            require_once(PAGE_PATH . 'events.php');
            break;

        case 'event-detail':
            require_once(PAGE_PATH . 'event-detail.php');
            break;
        
        case 'press-releases':
            require_once(PAGE_PATH . 'press-releases.php');
            break;
        
        case 'team-profile':
        default:
            require_once(BLOCK_PATH . 'team-profile-profile.php');
            break;
    }
}
