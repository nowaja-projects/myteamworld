<div id="contentA" class="module eventManage">

<?php include(BLOCK_PATH . 'print-messagess.php'); ?>

    <div class="contentAbox">            
	    <h2 class="headline"><?=$title?></h2> 
<?php

$team_seasons = $logged_user->getActiveTeam()->getSeasons();
$active_season = $logged_user->getActiveTeam()->getActiveSeason();

if(count($team_seasons) > 0 && !empty($active_season))
{
?>

	    <form method="post" action="" enctype="multipart/form-data">
	    	<fieldset>
			    <p class="title-noborder">
			        <strong>Vyberte typ události, kterou chcete vytvořit</strong>
			    </p>
			    <p class="entries type">
                    <input type="hidden" name="event[type]" value="<?=$is_update ? $event->type : 0?>" id="eventTypeInput" />
                    <input type="hidden" name="event[id]" value="<?=$is_update ? $event->id : 0?>" />
                    <input type="hidden" name="event[team_id]" value="<?=$is_update ? $event->team_id : $logged_user->getActiveTeam()->id?>" />
			    	<select id="eventType"<?=$is_update ? ' disabled="disabled"' : ''?>>
			    		<option value="0">---</option>
			    		<option value="<?=Event::COMPETITION?>" <?=($is_update && $event->type == Event::COMPETITION ? ' selected="selected"' : '')?>>Soutěžní zápas</option>
			    		<option value="<?=Event::FRIENDLY?>" <?=($is_update && $event->type == Event::FRIENDLY ? ' selected="selected"' : '')?>>Přátelský zápas</option>
			    		<option value="<?=Event::TRAINING?>" <?=($is_update && $event->type == Event::TRAINING ? ' selected="selected"' : '')?>>Trénink</option>
			    		<option value="<?=Event::ACTION?>" <?=($is_update && $event->type == Event::ACTION ? ' selected="selected"' : '')?>>Týmová akce</option>
			    	</select>
			    </p>
                    
                <?php
                if($is_update)
                {
                    include(BLOCK_PATH . 'event-manage-data.php');
                    
                    switch($event->type)
                    {
                        case Event::COMPETITION:
                            echo $event_data['team'] . $event_data['logo'] . $event_data['name'] . $event_data['place'] . $event_data['date'] . $event_data['attend'] . $event_data['save'];
                        break;
                        
                        case Event::FRIENDLY:
                            echo $event_data['team'] . $event_data['logo'] . $event_data['name'] . $event_data['place'] . $event_data['date'] . $event_data['attend'] . $event_data['save'];
                        break;
                         
                        case Event::TRAINING:
                            echo $event_data['name'] . $event_data['place'] . $event_data['date'] . $event_data['attend'] . $event_data['save'];
                        break;
                        
                        case Event::ACTION:
                            echo $event_data['name'] . $event_data['place'] . $event_data['date_team_action'] . $event_data['attend'] . $event_data['save'];
                        break;
                    }
                }
                ?>    
                                
			</fieldset>
	    </form>
<?php
}
elseif(!count($team_seasons))
{
    echo '<div class="msg noClose">' . $tr->tr('Momentálně <strong>nemůžete</strong> spravovat události, protože nemáte vytvořenou <strong>žádnou sezónu</strong>. Sezóny si můžete vytvořit a spravovat na stránce ') . '<a href="' . PATH_WEB_ROOT . 'edit-team-profile/#teamsettings">' . $tr->tr('nastavení týmu') . '</a>.</div>';
}
else //if(empty($active_season))
{
    echo '<div class="msg noClose">' . $tr->tr('Momentálně <strong>nemůžete</strong> spravovat události, protože nemáte nastavenou žádnou <strong>aktivní sezónu</strong>. Sezóny si můžete nastavit a spravovat na stránce ') . '<a href="' . PATH_WEB_ROOT . 'edit-team-profile/#teamsettings">' . $tr->tr('nastavení týmu') . '</a>.</div>';
}

?>
    </div><!-- .contentAbox -->
</div><!-- #contentA .module -->