<div class="sidebar left colSmall">

	<div class="fixedBlock"> <? /* musi byt zacilen v js a nasteven v css */ ?>
		<div id="navigation">

			<h2 class="blockTitle">Navigace</h2>

			<div class="content">

				<ul>
					<li><a class="anchor" href="#definition-of-terms">Definice pojmů</a></li>
					<li><a class="anchor" href="#general-provisions">Všeobecná ustanovení</a></li>
					<li><a class="anchor" href="#user-registration">Registrace uživatele</a></li>
					<li><a class="anchor" href="#team-registration">Registrace týmu</a></li>
					<li><a class="anchor" href="#rights-and-obligations-user">Práva a povinnosti uživatelů</a></li>
					<li><a class="anchor" href="#rights-and-obligations-provider">Práva a povinnosti provozovatele</a></li>
					<li><a class="anchor" href="#privacy">Ochrana osobních údajů</a></li>
					<li><a class="anchor" href="#final-provisions">Závěrečná ustanovení</a></li>
				</ul>

			</div><!-- .content -->

		</div><!-- #navigation -->
	</div><!-- .fixedBlock -->
	
</div><!-- .sidebar.left.colSmall -->

<section id="content" class="colBig">
	<div id="contentA" class="text">

		<div class="contentAbox">

		<h2 class="headline">Podmínky použití</h2>
		<p class="perex">Užíváním těchto stránek na serveru <a href="http://www.myteamworld.com">www.myteamworld.com</a> potvrzujete, že jste se seznámili s těmito podmínkami, souhlasíte s nimi a zavazujete se, že je budete dodržovat. Jestliže s těmito podmínkami nesouhlasíte, nepoužívejte tento webový server.</p>


		<h3 id="definition-of-terms" class="title">1. Definice pojmů</h3>
		<ol>
			<li>Provozovatelem serveru se rozumí myteamworld.com.</li>
			<li>Webovým serverem nebo serverem se rozumí server <a href="http://www.myteamworld.com">www.myteamworld.com</a> (dále také jen "myteamworld.com"), který zřídil a udržuje provozovatel.</li>
			<li>Uživatelem se rozumí každá osoba, která užívá stránky serveru <a href="http://www.myteamworld.com">www.myteamworld.com</a>.</li>
		</ol>

		<h3 id="general-provisions" class="title">2. Všeobecná ustanovení</h3>
		<ol>
			<li>Server myteamworld.com je určen zejména pro sportovce a sportovní fanoušky. Shromažďuje novinky z různých sportovních odvětví a umožňuje organizaci profesionálních i amatérských sportovních týmů a oddílů.</li>
			<? /* <li>Všechny služby na serveru myteamworld.com jsou pro uživatele dostupné zdarma.</li> */ ?>
			<li>Podmínky použití jsou platné od data poslední aktualizace viz, konec dokumentu, a provozovatel serveru myteamworld.com si je dovoluje kdykoliv upravit.</li>
		</ol>

		<h3 id="user-registration" class="title">3. Registrace uživatele</h3>
		<ol>
			<li>Uživatelé serveru myteamworld.com poskytují svá skutečná jména a pravdivé informace.</li>
			<li>Žádná osoba nesmí bez povolení vytvářet účet pro někoho jiného.</li>
			<li>Uživatel nesmí vytvořit více jak jeden osobní účet.</li>
			<li>Uživatel nesmí používat server myteamworld.com ke svému komerčnímu obohacení.</li>
			<li>Uživatel se zavazuje udržovat své kontaktní informace přesné a aktuální.</li>
			<li>Uživatel se zavazuje, že nikomu nesdělí své heslo, neumožní nikomu přístup ke svému účtu, ani nepodnikne žádné jiné kroky, které by mohly ohrozit bezpečnost jeho účtu.</li>
		</ol>

		<h3 id="team-registration" class="title">4. Registrace týmu</h3>
		<ol>
			<li>Uživatel serveru myteamworld.com registrací týmu potvrzuje, že je vlastník týmu nebo byl vlastníkem týmu pověřen založit a spravovat tento tým.</li>
			<li>Pokud vyberete pro svůj tým název nebo jiný identifikátor, který budeme považovat za nevhodný (například v případě stížnosti vlastníka ochranné známky na název, logo týmu nebo jeho profilovou fotografii), vyhrazujeme si právo jej odebrat nebo opakovat vznesení nároku.</li>
		</ol>

		<h3 id="rights-and-obligations-user" class="title">5. Práva a povinnosti uživatelů</h3>
		<ol>
			<li>Uživatel nese zodpovědnost za své aktivity na myteamworld.com a souhlasí s tím, že jeho jednání na myteamworld.com bude v souladu se zákonem a dobrými mravy.</li>
			<li>Vloží-li uživatel fotografie či videa, zavazuje se tímto, že nezasáhne do autorských práv třetích osob, jakožto práv na ochranu osobnosti třetích osob. Uživatel prohlašuje, že je-li na fotografii vyobrazena jiná osoba než uživatel, má uživatel její souhlas s užitím a zveřejněním jejího vyobrazení. Ukáže-li se toto prohlášení jako nepravdivé, je uživatel povinen provozovateli nahradit veškerou škodu z toho vzniklou a poskytovatel má právo tento obsah odstranit.</li>
		</ol>

		<h3 id="rights-and-obligations-provider" class="title">6. Práva a povinnosti provozovatele</h3>
		<ol>
			<li>Provozovatel nenese žádnou odpovědnost za obsah vložený uživatelem nebo týmem. Za obsah vložený uživatelem nebo týmem na myteamworld.com jsou plně odpovědní uživatelé, jejichž uživatelské účty se k těmto účtům vztahují.</li>
			<li>Provozovatel je oprávněn odebrat jakýkoli vámi zveřejněný obsah nebo informace na myteamworld.com, pokud se domnívá, že porušuje tyto podmínky.</li>
			<li>Uživatel nemá nárok na jakoukoli náhradu v souvislosti se zrušením uživatelského účtu nebo obsahu, který porušoval tyto podmínky.</li>
			<li>Provozovatel si vyhrazuje právo jakýchkoliv změn a úprav na myteamworld.com bez nutnosti předchozího oznámení uživatelům.</li>
			<li>Provozovatel nenese odpovědnost za případnou nedostupnost či nefunkčnost služeb a za případnou ztrátu nebo poškození dat uložených či vytvořených na myteamworld.com, vzniklou technickými problémy nebo administrativním zásahem provozovatele, jakož ani za možnou škodu takovýmto způsobem vzniklou.</li>
			<li>Provozovatel nemůže zaručit, že používání myteamworld.com povede ke zlepšeným sportovním výkonům uživatelů a týmů.</li>
		</ol>

		<h3 id="privacy" class="title">7. Ochrana osobních údajů</h3>
		<ol>
			<li>Provozovatel veškeré osobní údaje uvedené uživatelem ukládá pouze v elektronické podobě.</li>
			<li>Uživatel prohlašuje, že souhlasí se zpracováním osobních údajů.</li>
		</ol>

		<h3 id="final-provisions" class="title">8. Závěrečná ustanovení</h3>
		<ol>
			<li>Otázky výslovně neupravené těmito podmínkami se řídí platným právním řádem České republiky, zejména občanským zákoníkem.</li>
			<li>Uživatel i poskytovatel se zavazují řešit veškeré vzniklé spory především smírnou cestou. Poskytovatel a uživatel proto souhlasí, že případné spory vzniklé ze vztahů mezi poskytovatelem a uživatelem nebo v souvislosti s nimi budou poskytovatel a uživatel řešit nejprve za účasti nezávislého mediátora a teprve v případě neúspěchu mediace uplatní své sporné právo v soudním řízení. Poskytovatel a uživatel prohlašují, že mediátor bude jmenován z osob vedených na seznamu mediátorů akreditovaných u České advokátní komory nebo seznamu mediátorů.</li>
			<li>Tyto podmínky použití jsou platné od 29. 8. 2014.</li>
		</ol>

	</div><!-- .contentAbox -->

	</div><!-- #contentA -->
</section><!-- #content -->