<div id="contentA" class="module">

    <div class="contentAbox clear">    

        <?php
        $team_seasons = $team->getSeasons();
        if(is_array($team_seasons) && count($team_seasons) > 0)
        {
        ?>
        <div class="topFilter cleaned" data-type="statsFilter">
            <div class="filterBox">
                sezóna: <span class="activeFilter year"><a href="#" data-order="1" data-id="all">vše</a></span>
                <ul class="filterSelect">
                    <?php
                    $i = 2;
                    foreach($team_seasons as $season)
                    {
                    ?>
                    <li><a href="#" data-order="<?=$i++?>" data-id="<?=$season->id?>"><?=$season->name?></a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <?php
        }
        ?>


        <div class="statsBox">
        <?php
        $season_id = 0;

        $positions 	= SportDAO::getPositions($team->sport_id);
		$roster 	= Rosters::getTeamRoster($team->id);
		$sport 		= SportDAO::get($team->sport_id);
		$team_stats = StatsDAO::getAllTeamStats($team, $sport, $season_id);

        if(!empty($team_stats))
        {
            echo print_stats($positions, $roster, $sport, $team_stats);
        }
        else
        {
            echo '<div class="msg warning noMarginBottom noClose">Tým nemá zadané žádné statisky k zobrazení.</div>';
        }

        ?>
        </div> <!-- .statsBox -->
    	
    </div><!-- .contentAbox -->

</div><!-- #contentAbox .module -->