(function(){

    $.fn.browserDetection = function(config){

        function browserDetection(settings){
            var self = this;

            // Apply settings
            self.config = $.extend({
                browser: true
            }, settings);

            function init()
            {
                if (self.config.browser)
                {
                    html();
                }
            }

            function html()
            {
                for (var bname in $.browser)
                {

                    if (bname == 'mobile')
                    {
                        continue;
                    }
                    if (bname == 'version')
                    {
                        var ver = parseInt($.browser[bname]);
                    }
                    else
                    {
                        var bodyclass = bname;
                    }
                }
                $('html').addClass(bodyclass).addClass(bodyclass+'-'+ver);

                // old browser warning
                var browserHtml = false;
                var mob = false;

                if (bodyclass == 'msie' && ver < '9') {
                   browserHtml = true;
                }
                else if (bodyclass == 'webkit' && ver < '26')
                {
                   browserHtml = true;
                }
                else if (bodyclass == 'mozilla' && ver < '16')
                {
                   browserHtml = true;
                }
                else if (bodyclass == 'opera' && ver < '12')
                {
                   browserHtml = true;
                }

                if (bodyclass == 'mozilla' && ver == '11') { // fix MSIE 11, nevim proc se tvari jako mozilla 11
                    browserHtml = false;
                }

                // je to mobilni zarizeni?
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|WPDesktop|Opera Mini/i.test(navigator.userAgent) ) {
                    mob = true;
                }
                
                if ( browserHtml == true && mob == false )
                {
                    $('body').prepend('<div id="browserWarning"><div class="centered">Váš prohlížeč nemusí být podporován! Pokud chcete, aby vám vše na myteamworld.com fungovalo tak jak má, aktualizujte prosím váš prohlížeč na nejnovější verzi.</div></div>');
                }
            }

            init();

            return self;
        }

        new browserDetection(config);

    };


    $.confirm = function(params){
        
        if($('#confirmOverlay').length){
            // A confirm is already shown on the page:
            return false;
        }
        
        var buttonHTML = '';
        $.each(params.buttons,function(name,obj){
            
            // Generating the markup for the buttons:
            
            buttonHTML += '<a href="#" class="button '+obj['class']+'">'+name+'</a>';
            
            if(!obj.action){
                obj.action = function(){};
            }
        });
        
        var markup = [
            '<div id="confirmOverlay">',
            '<div class="confirm-skin">',
            '<div id="confirmBox" class="confirm-container">',
            '<h3 class="title">',params.title,'</h3>',
            '<p class="text">',params.message,'</p>',
            '<div id="confirmButtons">',
            buttonHTML,
            '</div></div></div></div>'
        ].join('');
        
        $(markup).hide().appendTo('body').fadeIn();
        
        var buttons = $('#confirmBox .button'),
            i = 0;

        $.each(params.buttons,function(name,obj){
            buttons.eq(i++).click(function(){
                
                // Calling the action attribute when a
                // click occurs, and hiding the confirm.
                
                obj.action();
                $.confirm.hide();
                return false;
            });
        });
    }

    $.confirm.hide = function(){
        $('#confirmOverlay').fadeOut(function(){
            $(this).remove();
        });
    }


    // Numeric only control handler
    $.fn.numericOnly = function() {
      return this.each(function()
      {
          $(this).keydown(function(e)
          {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers, dvojtecka
            // and keypad numbers ONLY
            return (
              key == 8 ||
              key == 9 ||
              key == 46 ||
              (key >= 37 && key <= 40) ||
              (key >= 48 && key <= 57) ||
              key == 59 || // dvojtecka
              key == 190 || // dvojtecka
              (key >= 96 && key <= 105));
          });
      });
    };

})(jQuery);


/************************************************************
    topFilter
************************************************************/
function topFilter(){

    // hover
    $('.topFilter .filterBox .activeFilter').delegate('a', 'mouseenter', function(){
        if ( $(window).width() > 750 )
        {
            var $this         = $(this),
                $filterSelect = $this.closest('.filterBox').find('.filterSelect');

            $filterSelect.removeClass('noActive');
            $filterSelect.addClass('active');
        }
    });

    $('.topFilter .filterBox .activeFilter').delegate('a', 'mouseleave',function(){
        if ( $(window).width() > 750 )
        {
            var $this         = $(this),
                $filterSelect = $this.closest('.filterBox').find('.filterSelect');

            $filterSelect.removeClass('active');
        }
    });

    $('.topFilter .filterBox .activeFilter').delegate('a', 'click',function(){
        // pokud jde o mobilni prohlizec
        if ( $(window).width() < 751 )
        {
            var $this         = $(this),
                $filterSelect = $this.closest('.filterBox').find('.filterSelect');

            if ( $filterSelect.hasClass('noActive') )
            {
                $filterSelect.removeClass('noActive');
                $filterSelect.removeClass('active');
            }
            $filterSelect.toggleClass('active');
        }
        return false;
    });

    
    // kliknuti na novy prvek
    $('.topFilter .filterSelect').delegate('a', 'click', function(){

        var $this              = $(this),
            $thisLi            = $(this).closest('li'),
            thisID             = $this.attr('data-id'),
            thisOrder          = $this.attr('data-order'),
            $filterSelect      = $this.closest('.filterSelect'),
            $activeFilter      = $this.closest('.filterBox').find('.activeFilter'),
            activeFilterText   = $activeFilter.html(),
            $activeFilterA     = $activeFilter.find('a'),
            $activeFilterOrder = $activeFilterA.attr('data-order'),
            order              = parseInt($activeFilterOrder) - 1,
            $tbody             = $('#contentA table tbody'),
            loading            = '<p id="loading"><!-- --></p>';


        // vytvorim promenou kde si najdu li za ktere zaradim predchozi aktivni <a>
        var orderObject = $filterSelect.find('a[data-order="' + order + '"]').closest('li');

        // pokud je to nula, zaradim na zacatek
        if ( order == 0 )
        {
            $('<li>' + activeFilterText + '</li>').prependTo( $filterSelect );
        }
        else
        {
            $('<li>' + activeFilterText + '</li>').insertAfter( orderObject );
        }

        // vymenim v activeFilter za <a> na ktere jsem klikl
        $activeFilter.html( $this );

        // odstranim li v kterem bylo <a> na ktere jsem klikl
        $thisLi.remove();

        $filterSelect.addClass('noActive');


        // sestavim data pro poslani ajaxem
        var $topFilter   = $('.topFilter'),
            filterType   = $topFilter.attr('data-type'),
            year         = $topFilter.find('.activeFilter.year a').attr('data-id'),
            //competition  = $topFilter.find('.activeFilter.competition a').attr('data-id'),
            type         = $topFilter.find('.activeFilter.type a').attr('data-id'),
            team         = $('#teamInfo').attr('data-id'),
            //data_to_send = 'eventsFiltr=1&fyear=' + year + '&fcompetition=' + competition + '&ftype=' + type;
            data_to_send = filterType + '=1&fyear=' + year + '&ftype=' + type + '&fteam=' + team;

        if ( filterType == 'statsFilter' )
        {
            $('#contentA .statsBox').html( loading );
        }
        else
        {
            $tbody.html( '<tr><td colspan="6" style="padding-top: 25px;">' + loading + '</td></tr>' );
        }

        $.ajax({
            url: '/ajax.php', 
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            type: 'get',
            data: data_to_send,
            success: function(response){
                if ( response.status == 'ok' )
                {
                    if ( filterType == 'statsFilter' )
                    {
                        $('#loading').remove();
                        $('#contentA .statsBox').html( response.response );
                    }
                    else
                    {
                        $('#loading').closest('tr').remove();
                        $tbody.html( response.response );
                    }
                }
                else
                {
                    alert('Oops. Something went wrong.');
                }
            }
        });

        return false;

    });

}


/************************************************************
    Add team message
************************************************************/
function addTeamMessage(){

    var defaultText = $('#status textarea.resizable').val();

    // Aktivni textarea komentare
    $('#status').delegate('textarea.resizable', 'focus', function(){
        $(this).autoresize();
        if ($(this).hasClass('noActive'))
        {
            $(this).val('');
            $(this).removeClass('noActive');
            $(this).addClass('active');
        }
        else
        {
            $(this).removeClass('wasActive');
            $(this).addClass('active');
        }
    });

    // Neaktivni textarea komentare
    $('#status').delegate('textarea.resizable', 'blur', function(){
        if ($(this).val() == '')
        {
            $(this).removeClass('active');
            $(this).addClass('noActive');
            $(this).val(defaultText);
            $(this).css('height', '');
        }
        else
        {
            $(this).removeClass('active');
            $(this).addClass('wasActive');
        }
    });

    // Kliknuti na tlacitko odeslat komentar
    $('#status .submit').delegate('.button', 'submit', function(){

        var textArea = $(this).find('textarea'),
            text     = textArea.val(),
            formId   = $(this).attr('id'),
            formId   = '#' + formId;


        if ( text === defaultText )
        {
            // neudelame nic
        }
        else
        {

        // odesleme komentar
        ajaxSendComment(formId);

        textArea.css('height', '');
        textArea.removeClass('wasActive');
        textArea.addClass('noActive');
        textArea.val(defaultText);

        }

        return false;
    });
}


/************************************************************
    Send wall filter
************************************************************/
function wallFilter(){

    $('#filterDiv form').submit(function(){

        ga('send', 'event', 'Wall filter', 'click');

        var data_to_send = $(this).serialize();

        data_to_send += '&wallSetFilter=1';

        $.ajax({
            url: '/ajax.php',
            //contentType: "application/json; charset=utf-8", kdzy je to POST, s timto to nefunguje, funguje jen s GET
            dataType: 'json',
            type: 'post',
            data: data_to_send,
            success: function(response){

                // odeslani e-mailu je ok
                if ( response.status == 'ok' )
                {

                    // zavru filter
                    $('#filterDiv').hide();
                    
                    $('#contentA .box.news').remove();
                    $('#noMoreMessages').remove();

                    $(response.response).insertAfter( $('#filterDiv') );

                    // znovu nactu timeago aby se u nove pridanych zprav chytil cas
                    $('#contentA .timeago').timeago();

                    // znovu nactu komentare, mazani komentaru, lajky, mazani team postu
                    // TODO po otestovani smazat
                    /*comments();

                    deleteComment();

                    likeUnlike();

                    deleteTeamPost();*/

                    // znovu nactu funkci, aby scroll fungoval porad dokola
                    $(window).scroll( infiniteScroll );

                }
                else
                {
                    alert(response.message);
                } 

            }
        });

        return false;
    });

}


/************************************************************
    Comments
************************************************************/
function comments(){

    var defaultText = $('.box .content textarea.resizable').val();

    // Pridat prvni komentar
    $('#contentA').delegate('.box .content .firstComment', 'click', function(){

        var commentInput = $(this).closest('.content').find('form');

        // pridam bubline class active
        $(this).closest('.content').find('.commentsBox .bubble-comment').addClass('active');

        $(this).parent().find('.separator').remove();
        $(this).remove();
        commentInput.removeClass('hidden');
        commentInput.addClass('myComment');

        return false;
    });

    // Pridat prvni komentar pres bublinu
    $('#contentA').delegate('.box .content .bubble-comment.first', 'click', function(){

        var commentInput = $(this).closest('.content').find('form');

        // pridam bubline class active
        $(this).addClass('active');

        $(this).closest('.content').find('.firstComment').remove();
        $(this).closest('.content').find('.separator').remove();
        commentInput.removeClass('hidden');
        commentInput.addClass('myComment');
    });


    // Aktivni textarea komentare
    $('#contentA').delegate('.box .content textarea.resizable', 'focus', function(){

        $(this).autoresize();
        if ($(this).hasClass('noActive'))
        {
            $(this).val('');
            $(this).removeClass('noActive');
            $(this).addClass('active');
            $(this).closest('.box.news').addClass('activeComment');
        }
        else
        {
            $(this).removeClass('wasActive');
            $(this).addClass('active');
            $(this).closest('.box.news').addClass('activeComment');
        }
    });

    // Neaktivni textarea komentare
    $('#contentA').delegate('.box .content textarea.resizable', 'blur', function(){

        if ($(this).val() == '')
        {
            $(this).removeClass('active');
            $(this).addClass('noActive');
            $(this).val(defaultText);
            $(this).css('height', '');
        }
        else
        {
            $(this).removeClass('active');
            $(this).addClass('wasActive');
        }

        if ($(this).val() == defaultText)
        {
            $(this).closest('.box.news').removeClass('activeComment');
        }
    });

    // Kliknuti na tlacitko odeslat komentar
    $('#contentA').delegate('.box .content .myComment', 'submit', function(){

        var textArea = $(this).find('textarea'),
            text     = textArea.val(),
            formId   = $(this).attr('id'),
            formId   = '#' + formId;


        if ( text == defaultText )
        {
            // neudelame nic
        }
        else
        {

            // odesleme komentar
            sendComment(formId);

            textArea.css('height', '');
            textArea.removeClass('wasActive');
            textArea.addClass('noActive');
            textArea.val(defaultText);

            $(this).closest('.box.news').removeClass('activeComment');

        }

        return false;
    });
}


/************************************************************
    Message
************************************************************/
function message(){

    var defaultText = $('.contentAbox textarea.resizable').val();


    // Aktivni textarea zpravy
    $('.contentAbox').delegate('textarea.resizable', 'focus', function(){

        $(this).autoresize();
        if ($(this).hasClass('noActive'))
        {
            $(this).val('');
            $(this).removeClass('noActive');
            $(this).addClass('active');
        }
        else
        {
            $(this).removeClass('wasActive');
            $(this).addClass('active');
        }
    });

    // Neaktivni textarea zpravy
    $('.contentAbox').delegate('textarea.resizable', 'blur', function(){

        if ($(this).val() == '')
        {
            $(this).removeClass('active');
            $(this).addClass('noActive');
            $(this).val(defaultText);
            $(this).css('height', '');
        }
        else
        {
            $(this).removeClass('active');
            $(this).addClass('wasActive');
        }
    });

    // Kliknuti na tlacitko odeslat zpravu
    $('.contentAbox').delegate('.myComment', 'submit', function(){

        var textArea = $(this).find('textarea'),
            text     = textArea.val(),
            formId   = $(this).attr('id'),
            formId   = '#' + formId;


        if ( text == defaultText )
        {
            // neudelame nic
        }
        else
        {

            // odesleme komentar
            // pokud je to rychla odpoved ve zpravach, poslu jiny ajax
            if ( $(this).hasClass('myMessage') )
            {
                // vytvorim si promenou pro zjisteni ve funkci sendMessage(formId);
                popupMessage = false;

                sendMessage(formId);
            }
            else
            {
                sendComment(formId);
            }

        textArea.css('height', '');
        textArea.removeClass('wasActive');
        textArea.addClass('noActive');
        textArea.val(defaultText);

        }

        return false;
    });
}


/************************************************************
    Send comment
************************************************************/
function sendComment(form)
{
    var str = $(form).serialize();
    str += '&add_comment=1';

    $.ajax({
        url: '/ajax.php',
        //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
        dataType: 'json',
        type: 'post',
        data: ""+str+"",
        success: function(response){

            if ( response.status == 'ok' )
            {
                var $commentsBox = $(form).closest('.content').find('.commentsBox'),
                    $bubbleComment = $commentsBox.find('.bubble-comment');

                // zobrazim komentar
                $(response.response).insertBefore($bubbleComment).hide().slideDown(500);

                // aktualizuju cas
                $commentsBox.find('.timeago').timeago();

                // aktualizuju pocty komentaru
                $bubbleComment.removeClass('first active').replaceWith(response.count_bubble);

                // pokud to neni detail zpravy, zaktualizuju celkovy pocet kometaru a link na detail
                if ( path != 'news-detail' )
                {
                    $(form).closest('.content').find('.links .allComments').replaceWith(response.count_all_comments);
                }

                var text = $(form).find('.textarea textarea');
                $(text).blur(function() {
                    $(this).hide();
                    $(this).parent().find('span').show();
                });
            }
            else if ( response.status == 'error-deleted' )
            {
                alert( response.message );

                // smazu danou zpravu / novinku
                $(form).closest('.box').remove();
            }
            else
            {
                alert('Nastala chyba při přidávání příspěvku.');
            }

        }
    });
}


/************************************************************
    Send message
************************************************************/
function sendMessage(form)
{
    var str = $(form).serialize();
    
    $.ajax({
        url: '/ajax.php',
        //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
        dataType: 'json',
        type: 'post',
        data: str,
        success: function(response){

            if ( response.status == 'ok' )
            {

                var conversation = '#' + response.id;

                // je to jiz existujici konverzace
                if ( response.type == 'exists' )
                {

                    // je to pridani zpravy pres popup
                    if ( popupMessage == true ) // popupMessage si definuji v init.js nad volanim teto funkce a ve funkci message()
                    {
                        // skryju starou posledni zpravu a odstranim ji class="last"
                        var $lastMessage = $('#contentA ' + conversation).find('.containerMessage.last');
                        $lastMessage.addClass('forBlind');
                        $lastMessage.removeClass('last');

                        // pridam novou zpravu
                        $(response.response).appendTo('#contentA ' + conversation);
                    }
                    else
                    {
                        // odstranim stare posledni zprave class="last"
                        var $lastMessage = $('#contentA ' + conversation).find('.containerMessage.last');
                        $lastMessage.removeClass('last');

                        //pridam novou zpravu
                        $(response.response).appendTo('#contentA ' + conversation + ' .inner').hide().slideDown(500);

                        // zascrolluji v konverzaci uplne dolu - FIXME: misto tohoto cisla bych mel spocitat vysku vsech zobrazenych zprav
                        var thisContainerMessagesHeight = 10000;

                        // kvuli animaci pridani zpravy, musim zascrollovat az se dokonci
                        setTimeout(function() {
                            $(conversation).animate({
                                scrollTop: thisContainerMessagesHeight
                            }, 1000);
                        }, 500);
                    }

                    // pokud posilam zpravu z vyhledavani nebo detailu uzivatele nebo z dochazky
                    if ( path == 'search' || path == 'user-profile' || path == 'attendance' )
                    {
                        $('#contentA .msg').remove();
                        $(response.message).prependTo('#contentA');
                    }

                }
                // je to nova konverzace
                if ( response.type == 'new' )
                {
                    var $contentAbox  = $('#contentA .contentAbox'),
                        $contentAboxP = $contentAbox.find('.headline').next('p');

                    //pridam novou konverzaci
                    $(response.response).insertAfter( $contentAboxP );

                    // odstranim msg pokud je
                    $contentAbox.find('.msg').remove();

                    // pokud posilam zpravu z vyhledavani nebo detailu uzivatele
                    if ( path == 'search' || path == 'user-profile' )
                    {
                        $('#contentA .msg').remove();
                        $(response.message).prependTo('#contentA');
                    }

                }

                // aktualizuji cas
                $('#contentA .contentAbox .timeago').timeago();

            }
            else
            {
                alert('Nastala chyba při posílání zprávy. Zkuste to prosím později.');
            }

        }
    });
}


/************************************************************
    Delete comment
************************************************************/
function deleteComment()
{
    $('#contentA').delegate('.commentsBox .deleteComment', 'click', function() {

        var $this           = $(this),
            id              = $this.data('id'),
            team            = $this.data('team-id'),
            $bubbleComment  = $this.closest('.content').find('.bubble-comment'),
            $allComments    = $this.closest('.content').find('.links .allComments'),
            $form           = $this.closest('.content').find('form'),
            $boxTitle       = $this.closest('.box').find('.boxTitle').text();

        data_to_send = "delete_comment=1&team="+team+"&id=" + id;


        // confirm na smazani zpravy       
        $.confirm({
            'title'     : 'Potvrdit smazání komentáře',
            'message'   : 'Opravdu si přejete smazat svůj komentář k příspěvku "' + $boxTitle + '"?',
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        $.ajax({
                            url: '/ajax.php', 
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            type: 'get',
                            data: data_to_send,
                            success: function(response){

                                if (typeof(response) == 'object' && response != null)
                                {
                                    if(response.response == true)
                                    {
                                        // odstranim komentar
                                        $this.closest('.comment-container').slideUp(500,
                                            function() {
                                                $(this).remove();
                                            }
                                        );

                                        // aktualizuju pocty komentaru
                                        $bubbleComment.replaceWith(response.count_bubble);
                                        $allComments.html(response.count_all_comments).show();

                                        // pokud smazu posledni komentar, pridam bubline class="first" a schovam form
                                        $bubbleComment = $this.closest('.content').find('.bubble-comment');
                                        if ( $bubbleComment.find('span').text() == 0 )
                                        {
                                            $bubbleComment.addClass('first');
                                            $form.removeClass('myComment').addClass('hidden');
                                        }
                                    }
                                    else
                                    {
                                        alert('Nastala chyba při mazání příspěvku.');
                                    }
                                }
                                else
                                {
                                    alert('Nastala chyba při mazání příspěvku.');
                                }
                            }
                        });
                    
                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });

        return false;
    });
}


/************************************************************
    Like & Unlike
************************************************************/
function likeUnlike()
{
    $('#contentA').delegate('.box .content .likeIt', 'click', function() {

        var $this   = $(this),
            id      = $this.data('id');
            //user_id = $this.data('user-id');

        data_to_send = "like=1&id=" + id/* + "&user_id=" + user_id*/ + "&type=link";
        
        $.ajax({
            url: '/ajax.php', 
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if (response.status == 'ok')
                {                    
                    $("#like-"+id).html(response.count);
                    $("#popup-like-"+id+"").html(response.response);

                    if( $this.hasClass('bubble-like') ) // klik na bublinu lajkovou
                    {
                        $this.closest('.content').find('.post .links .likeIt').text(response.text);
                        $this.closest('.content').find('.post .links .likeIt').attr('title', response.text);
                    }
                    else
                    {
                        $this.text(response.text);
                        $this.attr('title', response.text);
                    }
                }
                else if ( response.status == 'error-deleted' )
                {
                    alert( response.message );

                    // smazu danou zpravu / novinku
                    $this.closest('.box').remove();
                }
                else
                {
                    alert('Oops. Something went wrong.');
                }
            }
        });
        
        return false;
    });
}


/************************************************************
    Notifications
************************************************************/
function notificationsAndNews()
{
    var $notification         = $('#notification .notification'),
        $notificationCount    = $notification.find('.count'),
        $notificationMsg      = $('#notification .messages'),
        $notificationMsgCount = $notificationMsg.find('.count'),
        $title                = $('title'),
        titleText             = $title.text(),
        sendInterval          = 55000,
        hideInterval          = 10000,
        hideAnimation         = 2500,
        showIntervalTitle     = 10000,
        hideIntervalTitle     = 5000;


    setInterval(function() {

        var $lastNews    = $('#contentA .box.news:first'),
            lastNewsDate = $lastNews.find('.bubble-date').attr('data-time'),
            data_to_send = '';

        // pokud jsem na nástěnce, chci donačítat i nové novinky
        if ( path == 'wall' )
        {
            data_to_send = "last_news=" + lastNewsDate;
        }

        $.ajax({
            url: '/ajax.php?getNotifications',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                // pridam nove novinky pokud jsou
                if( response.status == 'ok' && response.response != null )
                {
                    var html = response.response,
                        arr  = [],
                        status = 'ok';

                    // najdu si ve vracenem html vsechny id postu
                    $(html).find('.content').each(function() {

                        arr.push( $(this).attr('data-id') );

                    });

                    // projdu vsechny novy id a pokud uz jsou na nastence, odstranim ty stare
                    $.each( arr, function( index, value ) {

                        var oldNews = $('#contentA .box.news .content[data-id="' + value + '"]').closest('.box');

                        if( !oldNews.hasClass('activeComment') )
                        {
                            oldNews.remove();
                        }
                        else
                        {
                            // nedelam nic
                            // TODO ale mel bych si vytahnout z html ten prispevek ktery ma uzivatel aktivni a pridat jen ty zbyle ostatni

                            status = 'ko';
                        }
                      
                    });

                    if ( status == 'ok')
                    {
                        $(html).insertBefore( $('#contentA .box.news:first') ); // nemuze byt vyuzita promenna $lastNews, protoze uz muze byt v te chvili smazana
                    }

                    // aktualizuji cas
                    $('#contentA .news .timeago').timeago();
                }

                // zaktualizuji pocet upozorneni
                $notificationCount.text(response.count);

                // pokud je nejake nove, pridam class new
                if( response.count > 0 )
                {
                    $notification.addClass('new');
                }

                // zaktualizuji pocet zprav
                $notificationMsgCount.text(response.messageCount);

                // pokud je nejaka nova, pridam class new
                if( response.messageCount > 0 )
                {
                    $notificationMsg.addClass('new');
                }

                // pokud je nejake upozorneni
                if( response.count > 0 || response.messageCount > 0 )
                {
                    // zobrazim upozorneni na notifikace v levem spodnim rohu
                    $('body').append('<div id="notificationsAlert">' + response.alert + '</div>');
    
                    // po 10 vterinach odstranim zobrazene upozorneni na notifikace
                    setTimeout(function() {
    
                        $('#notificationsAlert .notificationAlert').fadeOut(hideAnimation);
    
                        setTimeout(function() {
    
                            $('#notificationsAlert').remove();
    
                        }, hideAnimation);
    
                    }, hideInterval);


                    // zmenim title stranky
                    var flashing = setInterval(function() {
        
                        if ( response.messageCount > 0 && response.count == 0 )
                        {
                            if ( response.messageCount == 1 )
                            {
                                $title.text(response.messageCount + ' nová zpráva | myteamworld.com');
                            }
                            else if ( response.messageCount > 1 && response.messageCount < 5 )
                            {
                                $title.text(response.messageCount + ' nové zprávy | myteamworld.com');
                            }
                            else
                            {
                                $title.text(response.messageCount + ' nových zpráv | myteamworld.com');
                            }

                            // zase ho prepnu zpet, vytvorim tim blikani
                            setTimeout(function() {
    
                                $title.text(titleText);
        
                            }, hideIntervalTitle);
                        }
                        else if ( response.count > 0 && response.messageCount == 0 )
                        {
                            if ( response.count == 1 )
                            {
                                $title.text(response.count + ' nové upozornění | myteamworld.com');
                            }
                            else if ( response.count > 1 && response.count < 5 )
                            {
                                $title.text(response.count + ' nová upozornění | myteamworld.com');
                            }
                            else
                            {
                                $title.text(response.count + ' nových upozornění | myteamworld.com');
                            }

                            // zase ho prepnu zpet, vytvorim tim blikani
                            setTimeout(function() {
    
                                $title.text(titleText);
        
                            }, hideIntervalTitle);
                        }
                        else if ( response.count > 0 && response.messageCount > 0 )
                        {
                            if ( response.messageCount == 1 )
                            {
                                if ( response.count == 1 )
                                {
                                    $title.text(response.messageCount + ' nová zpráva, ' + response.count + ' nové upozornění | myteamworld.com');
                                }
                                else if ( response.count > 1 && response.count < 5 )
                                {
                                    $title.text(response.messageCount + ' nová zpráva, ' + response.count + ' nová upozornění | myteamworld.com');
                                }
                                else
                                {
                                    $title.text(response.messageCount + ' nová zpráva, ' + response.count + ' nových upozornění | myteamworld.com');
                                }
                            }
                            else if ( response.messageCount > 1 && response.messageCount < 5 )
                            {
                                if ( response.count == 1 )
                                {
                                    $title.text(response.messageCount + ' nové zprávy, ' + response.count + ' nové upozornění | myteamworld.com');
                                }
                                else if ( response.count > 1 && response.count < 5 )
                                {
                                    $title.text(response.messageCount + ' nové zprávy, ' + response.count + ' nová upozornění | myteamworld.com');
                                }
                                else
                                {
                                    $title.text(response.messageCount + ' nové zprávy, ' + response.count + ' nových upozornění | myteamworld.com');
                                }
                            }
                            else
                            {
                                if ( response.count == 1 )
                                {
                                    $title.text(response.messageCount + ' nových zpráv, ' + response.count + ' nové upozornění | myteamworld.com');
                                }
                                else if ( response.count > 1 && response.count < 5 )
                                {
                                    $title.text(response.messageCount + ' nových zpráv, ' + response.count + ' nová upozornění | myteamworld.com');
                                }
                                else
                                {
                                    $title.text(response.messageCount + ' nových zpráv, ' + response.count + ' nových upozornění | myteamworld.com');
                                }
                            }

                            // zase ho prepnu zpet, vytvorim tim blikani
                            setTimeout(function() {
    
                                $title.text(titleText);
        
                            }, hideIntervalTitle);
                        }
    
                    }, showIntervalTitle);

                    flashing;

                    setInterval(function() {
                        sendInterval = sendInterval - 1;
                        clearInterval(flashing);
                    }, sendInterval);

                }

            }
        });
        
    }, sendInterval);
}


function notificationsAndNewsFirst()
{
    var $notification         = $('#notification .notification'),
        $notificationCount    = $notification.find('.count'),
        $notificationMsg      = $('#notification .messages'),
        $notificationMsgCount = $notificationMsg.find('.count'),
        data_to_send          = '';

    $.ajax({
        url: '/ajax.php?getNotifications',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        type: 'get',
        data: data_to_send,
        success: function(response){

            // zaktualizuji pocet upozorneni
            $notificationCount.text(response.count);

            // pokud je nejake nove, pridam class new
            if( response.count > 0 )
            {
                $notification.addClass('new');
            }

            // zaktualizuji pocet zprav
            $notificationMsgCount.text(response.messageCount);

            // pokud je nejaka nova, pridam class new
            if( response.messageCount > 0 )
            {
                $notificationMsg.addClass('new');
            }

        }
    });

}


function readNotifications()
{
    $('#notificationsAlert .notificationAlert a').live('click', function() {

        var objectID = $(this).closest('.notificationAlert').data('id'),
            url      = $(this).attr('href');

        $.ajax({
            url: '/ajax.php?readNotification=' + objectID, 
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            complete: function(){

                window.location.href = url;

            }
        });

        return false;

    });
}


/************************************************************
    #contentA & sidebar height
************************************************************/
function equalizer()
{
    if ( $('.sidebar').length )
    {
        var sidebarLeft        = $('.sidebar.left'),
            sidebarRight       = $('.sidebar.right'),
            sidebarLeftHeight  = sidebarLeft.height(),
            sidebarRightHeight = sidebarRight.height(),
            content            = $('#content'),
            contentHeight      = content.height();
        
        // content je nejvetsi
        if ( contentHeight >= sidebarLeftHeight && contentHeight >= sidebarRightHeight )
        {
            sidebarLeft.css('min-height', contentHeight);
            sidebarRight.css('min-height', contentHeight);
        }
        // levy sidebar je nejvetsi
        else if ( sidebarLeftHeight >= contentHeight && sidebarLeftHeight >= sidebarRightHeight )
        {
            sidebarRight.css('min-height', sidebarLeftHeight);
            content.css('min-height', sidebarLeftHeight);
        }
        /* pravy sidebar nebude nikdy nejvetsi, protoze ted ma vzdycky vysku 1px
        else if ( sidebarRightHeight >= contentHeight && sidebarRightHeight >= sidebarLeftHeight )
        {
            
        }
        */
    }
}


/************************************************************
    Fixed scroll
************************************************************/
function fixedScroll (options)
{
    options = $.extend({
        $element: $('#main .sidebar.right .fixedBlock'),
        $area: $('#main .sidebar.right')
    }, options);

    var start = options.$element.offset().top - 85;                     // misto, od ktereho chci fixnout element - 85px je offset fixniho headeru + mezery

    $(window).scroll(function(){

        var element         = options.$element,                         // co fixneme
            area            = options.$area,                            // element v kterem fixujeme
            areaBottom      = area.offset().top + area.height(),        // spodni hrana elementu ve kterem fixujeme
            elementTop      = element.offset().top,                     // vzdalenost vrchni hrany elementu od vrchu dokumentu
            elementBottom   = element.offset().top + element.height(),  // vzdalenost spodni hrany elementu od vrchu dokumentu + 18px margin zespodu
            scrollTop       = $(window).scrollTop() + 85;               // vzdalenost spodni hrany obalu od vrchu dokumentu + 85px je offset fixniho headeru + mezery

        // prevent fixing of summarybox on mobile
        /*
        if(jQuery.browser.mobile)
        {
            return;
        }
        */

        if ( scrollTop >= start && elementBottom < areaBottom )
        {
            element.addClass('fixed');
            
            /*
            if ( scrollTop >= areaBottom - element.height() )
            {
                element.removeClass('fixed');
                element.addClass('stop');
            }
            */
        }
        if ( elementBottom >= areaBottom )
        {
            element.removeClass('fixed');
            element.addClass('stop');
        }
        if ( scrollTop < elementTop && elementTop > start )
        {
            element.removeClass('stop');
            element.addClass('fixed');
        }
        if ( scrollTop < start )
        {
            element.removeClass('fixed');
            element.removeClass('stop');
        }

    });

    $('body').trigger('scroll');

};


/************************************************************
    viewMoreWallPosts - funkce pro nacitani dalsich novinek
************************************************************/
function viewMoreWallPosts(start,behaviours)
{
    var loading = '<p id="loading"><!-- --></p>';

    // zobrazim loading
    $( loading ).appendTo('#contentA');
   
    data_to_send = "viewMore=1&start=" + start;

    $.ajax({
        url: '/ajax.php', 
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        type: 'get',
        data: data_to_send,
        success: function(response){
            if (typeof(response) == 'object' && response != null)
            {
                // pokud je to ok a vrati to nejake novinky
                if( response.status == 'ok' )
                {
                    //skryji loading
                    $("#loading").remove();

                    $(response.response).appendTo('#contentA');

                    // znovu nactu timeago aby se u nove pridanych zprav chytil cas
                    $('#contentA .timeago').timeago();

                    // znovu nactu komentare, mazani komentaru, lajky, mazani team postu
                    // TODO po otestovani smazat
                    /*comments();

                    deleteComment();

                    likeUnlike();

                    deleteTeamPost();*/

                    // zascrolluju aby se chytila reklama vpravo
                    $(window).trigger('scroll');

                    // znovu nactu funkci, aby scroll fungoval porad dokola
                    $(window).scroll( infiniteScroll );
                }

                // pokud je to ok a vrati to ze uz nejsou dalsi zpravy k zobrazeni
                else if ( response.status == 'ok-no-more')
                {
                    //skryji loading
                    $("#loading").remove();

                    $(response.response).appendTo('#contentA');
                }

                else
                {
                    alert('Oops. Something went wrong.');
                }
            }
            else
            {
                alert('Oops. Something went wrong.');
            }
        }
    });
}



/************************************************************
    team post - poslani vzkazu na nastenku pro cleny aktivniho tymu
************************************************************/
function teamPost(){

    var defaultText = $('#teamPost textarea.resizable').val();

    $('#teamPost').submit(function() {

        var $form    = $(this),
            textArea = $(this).find('textarea'),
            text     = textArea.val();

        if ( text == defaultText )
        {
            // neudelame nic
        }
        else
        {
            // odesleme komentar
            sendTeamPost($form);

            textArea.css('height', '');
            textArea.removeClass('wasActive');
            textArea.addClass('noActive');
            textArea.val(defaultText);
        }

        return false;

    });
}


function sendTeamPost(form)
{
    var str = $(form).serialize();
    
    $.ajax({
        url: '/ajax.php',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        type: 'get',
        data: str,
        success: function(response){

            if ( response.status == 'ok' )
            {
                // vypisu novy vzkaz
                $(response.response).insertAfter( $('#filterDiv') ).hide().slideDown(500);
                
                // odstranim vsechny predchozi hlasky
                //$('#contentA .msg').remove();

                // pridam hlasku o pridani zpravy
                //$('<div class="msg done">'+response.message+'</div>').insertAfter('#contentA #filterDiv');
                
                // znovu nactu timeago aby se u nove pridaneho vzkazu chytil cas
                $('#contentA .timeago').timeago();

                // znovu nactu komentare, mazani komentaru, lajky
                // TODO po otestovani smazat
                /*
                comments();

                deleteComment();

                likeUnlike();
                */
            }
            else
            {
                alert(response.message);
            }

        }
    });
}


// Smazani vzkazu na nastence
function deleteTeamPost()
{
    $('#contentA').delegate('.box.teampost .deleteBig', 'click', function(){

        var $this = $(this)/*,
            title = $(this).closest('.box').find('.title').text()*/;

        // id vzkazu ke smazani
        var id = $this.data('id');

        // confirm na smazani zpravy       
        $.confirm({
            'title'     : 'Potvrdit smazání vzkazu',
            'message'   : 'Opravdu si přejete smazat tento týmový vzkaz?',
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        data_to_send = "delete_teampost=" + id;

                        $.ajax({
                            url: '/ajax.php',
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            type: 'get',
                            data: data_to_send,
                            success: function(response){

                                if( response.status == 'ok' )
                                {
                                    // odstranim smazanou zpravu
                                    $this.closest('.box').slideUp(500,
                                        function() {
                                            $(this).remove(); 
                                        }
                                    );

                                    // kdyz je to detail zobrazim i hlasku
                                    if ( path == 'teampost-team-profile' )
                                    {
                                        // odstranim vsechny predchozi hlasky
                                        $('#contentA .msg').remove();

                                        // pridam hlasku o smazani zpravy
                                        $('#contentA').prepend( response.message );
                                    }
                                }
                                else
                                {
                                    alert(response.message);
                                }

                            }
                        });

                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
        // konec

        return false;
    });
}


/************************************************************
    Infinite scroll
************************************************************/
function infiniteScroll()
{

    if ( $(window).height() + $(window).scrollTop() >= $(document).height() - 400 )
    {
        $(window).unbind('scroll', infiniteScroll);
        viewMoreWallPosts( $('#contentA .box').length - 1 );
    }

}


/************************************************************
    MSG close
************************************************************/
function msgClose()
{

    var msg   = $('.msg'),
        close = $('<span class="close" title="Zavřít"><!-- --></span>');

    if( !msg.hasClass('noClose') )
    {
    close.appendTo( msg );
    }

    close.click(function(){
        msg.remove();
    });

}


/************************************************************
    Fixed navigation
************************************************************/
/*
function fixedNavigation()
{

    $(window).scroll(function(){

        var element         = $('.side-column.left'),                       // co fixneme
            area            = $('#contentA'),                               // element v kterem fixujeme
            areaBottom      = area.offset().top + area.height() + 75,       // spodni hrana elementu ve kterem fixujeme
            elementTop      = element.offset().top,                         // vzdalenost vrchni hrany elementu od vrchu dokumentu
            elementBottom   = element.offset().top + element.height(),      // vzdalenost spodni hrany elementu od vrchu dokumentu
            start           = area.offset().top + 75,                       // misto, od ktereho chci fixnout element
            scrollTop       = $(window).scrollTop();                        // vzdalenost spodni hrany obalu od vrchu dokumentu

        if ( scrollTop >= start && elementBottom < areaBottom )
        {
            element.addClass('fixed');
        }
        if ( elementBottom >= areaBottom )
        {
            element.removeClass('fixed');
            element.addClass('stop');
        }
        if ( scrollTop < elementTop && elementTop > start )
        {
            element.removeClass('stop');
            element.addClass('fixed');
        }
        if ( scrollTop < start )
        {
            element.removeClass('fixed');
        }

    });

}
*/


/************************************************************
    FANS AND OBSERVERS
************************************************************/
function fansAndObservers()
{
    $('#main .sidebar.left, #main #contentA .result').delegate('.becameFan, .becameObserver, .removeFan, .removeObserver, .removeFan, .removeObserver, .becameMember, .removeMember', 'click', function() {

        var id     = $(this).attr('data-id').match(/team_(\d+)_(.*)/),
            $this  = $(this),
            search = '';

            // pokud je to vyhledavani
            if ( $('body').attr('id') == 'search' )
            {
                search = '&search=1';

                $this.closest('.contentAbox').find('.result').removeClass('active');
                $this.closest('.result').addClass('active');
            }
            
            // chci byt fanousek tymu
            if( $this.hasClass('becameFan') )
            {
                data_to_send = 'becameFan=' + id[1] + '&team_hash=' + id[2] + search ;
            }

            // chci sledovat deni v tymu
            if( $this.hasClass('becameObserver') )
            {
                data_to_send = 'becameObserver=' + id[1] + '&team_hash=' + id[2] + search ;
            }

            // nechci byt fanouskem tymu
            if( $this.hasClass('removeFan') )
            {
                data_to_send = 'removeFan=' + id[1] + '&team_hash=' + id[2] + search ;
            }

            // nechci sledovat deni v tymu
            if( $this.hasClass('removeObserver') )
            {
                data_to_send = 'removeObserver=' + id[1] + '&team_hash=' + id[2] + search ;
            }

            // chci byt clenem tymu
            if( $this.hasClass('becameMember') )
            {
                data_to_send = 'becameMember=' + id[1] + '&team_hash=' + id[2] ;
            }

            // nechci byt clenem tymu
            if( $this.hasClass('removeMember') )
            {
                data_to_send = 'removeMember=' + id[1] + '&team_hash=' + id[2] ;
            }
        
        $.ajax({
            url: '/team_ajax.php', 
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if (typeof(response) == 'object' && response != null && response.status == 'ok')
                {
                    // pokud to je vyhledavani
                    if ( $('body').attr('id') == 'search' )
                    {
                        var $result = $('.result.active'),
                            $right  = $result.find('.right');

                        $right.find('a').remove();
                        $right.html(response.html);
                        $result.find('.info .fans').text(response.fansCount);
                        $result.find('.info .observers').text(response.observersCount);
                    }
                    else
                    {
                        var $teamOptionLinks = $('#teamOptionLinks');

                        $teamOptionLinks.find('.removeTeamLink').remove();
                        $teamOptionLinks.prepend(response.html);
                        $('#fansCount').html(response.fansCount);
                        $('#observersCount').html(response.observersCount);
                    }
                }
                // pokud to je zadost o stat se clenem tymu
                else if (response.status == 'member-ok')
                {
                    var $teamOptionLinks = $('#teamOptionLinks');

                    $teamOptionLinks.find('.removeTeamLink').remove();
                    $teamOptionLinks.prepend(response.html);
                }
                else
                {
                    alert(response.message);
                }
            }
        });
        
        return false;
    });
}



/************************************************************
    SEARCH
************************************************************/

// naseptavac hlavniho vyhledavani
function searchToggle(){

    $('#searchToggle, #toggleMobileSearch').click(function(){

        $(this).closest('#pageHeader').find('#mainSearchOverlay').removeClass('forBlind');
        $(this).closest('#pageHeader').find('#mainSearch .maininput').focus();

        return false;
    });

    $('#mainSearch .close').click(function(){

        $(this).closest('#pageHeader').find('#mainSearchOverlay').addClass('forBlind');

        return false;
    });

}


// naseptavac hlavniho vyhledavani
function searchComplete(){

    // pokud stisknu enter a jsou vyplnene aspon 2 znaky, odeslu formular
    // dal funkce $('#mainSearch').submit nize
    $('#mainSearch').keydown(function( event ){

        var mainSearchSelect = $('#mainSearch').find('.maininput').val();

        if ( event.which == 13 && mainSearchSelect.length > 1 )
        {
            $('#mainSearch').submit();
        }

    });

    // naseptavac
    $('#mainSearchSelect').fcbkcomplete({
        json_url: '/ajax.php?quick_search',
        cache: true,
        width: '300',
        addontab: true, // nefunguje, check
        firstselected : true,
        maxitems: 1,
        filter_selected: true,
        input_min_size: 1,
        maxshownitems: 7,
        height: 7,
        complete_text: '',
        delay : 500,
        onselect: function(){
            $('#mainSearchSelect_annoninput').hide();

            var mainSearchUrl = $('#mainSearchSelect option:selected').val();
                
            window.location.href = mainSearchUrl;
        }/*,
        onremove: function(){
            $('#mainSearchInput_annoninput').show();
            $('#mainSearchInput_annoninput').click();
        }*/
    });


    // vlozeni placeholderu
    $('#mainSearch .holder .maininput').attr('placeholder', 'Hledat tým nebo uživatele');

    // odeslani formulare pres enter, tzn uzivatel nikoho nevybral a hleda svuj vlastni vyraz
    $('#mainSearch').submit(function(){

        var mainSearchSelect = $('#mainSearch').find('.maininput').val();

        $('#mainSearchInput').val(mainSearchSelect);

        if ( $('#mainSearchInput').val() == 0 )
        {
            return false
        }

    });

}


function showMoreSearch(){

    $('#contentA .results').delegate('.viewMore a', 'click', function(){

        var $this     = $(this),
            $viewMore = $this.closest('.viewMore'),
            $results  = $this.closest('.results'),
            url       = document.location.href,
            word      = url.split('=').pop()
            type      = '',
            start     = $results.find('.result').length,
            loading   = '<p id="loading"><!-- --></p>';

        // zobrazim loading
        $( loading ).insertBefore( $viewMore );

        // odstranim predchozi .viewMore
        $viewMore.remove();

        // zjisteni typu
        if ( $results.hasClass('teams') )
        {
            type = 'teams';
        }
        if ( $results.hasClass('users') )
        {
            type = 'users';
        }

        var data_to_send = 'more_search=' + word + '&type=' + type + '&start=' + start

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // odstranim loading
                    $('#loading').remove();

                    if ( type == 'teams' )
                    {
                        $( response.response ).insertAfter( '.results.teams .result:last' );
                    }
                    if ( type == 'users' )
                    {
                        $( response.response ).insertAfter( '.results.users .result:last' );
                    }
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;

    });

}


/************************************************************
    MENU TOGGLE
************************************************************/
// prepinac mobilniho menu a ovladani
function menuToggle(){

    $('#toggleMobileMenu').click(function(){

        // zavru menu moreOptions jesti je otevrene
        $('#user .moreOptions').removeClass('showDropdown');

        $(this).toggleClass('active');
        $(this).closest('nav').find('#mainMenu').toggleClass('showDropdown');

        return false;
    });


    $('#mainMenu > li.open').click(function(){

        if ( $(window).width() < 751 )
        {
            if ( $(this).hasClass('showDropdown') )
            {
                $(this).removeClass('showDropdown');
            }
            else
            {
                $('#mainMenu li.open').removeClass('showDropdown');
                $(this).toggleClass('showDropdown');
            }
        }

    });

    $('#user .moreOptions').click(function(){

        if ( $(window).width() < 751 )
        {
            // zavru mainMenu jesti je otevrene a odstranim active class
            $('#mainMenu').removeClass('showDropdown');
            $('#toggleMobileMenu').removeClass('active');

            if ( $(this).hasClass('showDropdown') )
            {
                $(this).removeClass('showDropdown');
            }
            else
            {
                $(this).addClass('showDropdown');

                // zjisteni vysky okna a menu, pokud je menu vetsi nez okno, pridam .scroll
                var moreOptions  = $('#user .right .moreOptions > li > ul'),
                    windowHeight = $(window).height() - $('#pageHeader').height(),
                    menuHeight   = moreOptions.height();

                if ( windowHeight < menuHeight )
                {
                    $(this).addClass('scroll');
                }
            }
        }

    });

    // pokud zmenim velikost okna a je vetsi nez mobilni zobrazeni, odkryju vsechny menu a obracene
    $(window).resize(function () {

        // zavru mainMenu jesti je otevrene
        $('#mainMenu').removeClass('showDropdown');

        // zavru menu moreOptions jesti je otevrene
        $('#user .moreOptions').removeClass('showDropdown');

    });

}


/************************************************************
    PREPNUTI AKTIVNIHO TYMU
************************************************************/

function switchTeam() {
    // kliknuti na zmenu tymu v hlavicce i popupu
    $('.switchTeam').click(function(){
        var id = $(this).attr('id').match(/team_(\d+)/);
        data_to_send = 'switchTeam=' + id[1];
        
        $.ajax({
            url: '/team_ajax.php', 
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){
                if (typeof(response) == 'object' && response != null && response.status == 'ok')
                {
                    window.location.href = '/';
                }
                else
                {
                    alert('Nastala chyba při změně týmu.');
                }
            }
        });
        
        return false;

    });
}



/************************************************************
    PRESS RELEASE
************************************************************/

// po zavreni facyboxu ho vyprazdnim
function clearPressReleaseForm()
{
    $('.fancybox-close').live('click', function(){

        var $addPost        = $('#addPost'),
            $addEditButton  = $('#addEditButton');

        $addPost.attr( 'data-id', '' );
        $addPost.find('input.heading').val('');
        $addPost.find('textarea.perex').val('');
        $addPost.find('textarea.message').val('');
        $addPost.find('input.date').val('');
        $addPost.find('input.time').val('');

        $addPost.find('.error').removeClass('error');
        $addPost.find('.errorMsg').hide();

        $addEditButton.removeClass('edit');

    });
}


// odeslani tiskove zpravy
function sendPressRelease(form)
{
    var str     = $(form).serialize(),
        formID  = $('#addPost').attr('data-id');

    // pokud id neni, je to nova zprava
    if ( formID == '' )
    {
        str += '&add_press=1';
    }
    else
    {
        str += '&edit_press_id=' + formID;
    }

    $.ajax({
        url: '/ajax.php',
        //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
        dataType: 'json',
        type: 'post',
        data: ""+str+"",
        success: function(response){

            // pridani nove publikovane zpravy je ok
            if ( response.status == 'ok' )
            {
                // pokud je vse ok, zavru popup
                $('#addPost').closest('.fancybox-wrap ').find('.fancybox-close').click();

                // nactu prave pridanou tiskovou zpravu
                $( response.response ).insertAfter('#contentA #admin');

                // odstranim vsechny predchozi hlasky
                $('#contentA .msg').remove();

                // pridam hlasku o pridani zpravy
                $('<div class="msg done">'+response.message+'</div>').insertAfter('#contentA #admin');
            }

            // pridani nove nepublikovane zpravy je ok
            else if ( response.status == 'unpublished-ok' )
            {
                // pokud je vse ok, zavru popup
                $('#addPost').closest('.fancybox-wrap ').find('.fancybox-close').click();

                // nactu prave pridanou tiskovou zpravu
                $('#unpublishedPressReleases .list').prepend( response.response );

                // pokud jeste nejsou nejake nezverejnene zpravy
                if ( $('#admin #unpublishedButton').length == 0 )
                {
                    // pridam tlacitko nezverejnenych zprav
                    $('#admin').prepend('<a id="unpublishedButton" class="button buttonB icon edit popUpMedium" href="#unpublishedPressReleases">nepublikované zprávy</a>');
                }

                // odstranim vsechny predchozi hlasky
                $('#contentA .msg').remove();

                // pokud uz neni zadna dalsi tiskova zprava
                if ( $('#contentA .box').length == 0 )
                {
                    // pridam hlasku
                    $( '<div class="msg warning icon noClose">Tým momentálně nemá žádné tiskové zprávy k zobrazení.</div>' ).insertAfter('#contentA #admin');
                }

                // pridam hlasku o pridani zpravy
                $('<div class="msg done">'+response.message+'</div>').insertAfter('#contentA #admin');
            }

            // ulozeni zeditovane zpravy ktera je publikovana a je ok
            else if ( response.status == 'edit-ok' )
            {
                // pokud je vse ok, zavru popup
                $('#addPost').closest('.fancybox-wrap ').find('.fancybox-close').click();

                // pokud je v listu publikovanych zprav nejaka editovana, pridam ji pred ni = editoval jsem zpravu publikovanou
                if ( $('#contentA .box.edit').length !== 0 )
                {
                    // nactu prave editovanou tiskovou zpravu
                    $( response.response ).insertAfter('#contentA .box.edit');

                    // odstranim starou zpravu
                    $('#contentA .box.edit').remove();
                }
                // jinak je jasny ze jsem editoval zpravu nepublikovanou
                else
                {
                    // nactu prave pridanou tiskovou zpravu
                    $( response.response ).insertAfter('#contentA #admin');

                    // odstranim starou zpravu
                    $('#unpublishedPressReleases .item.edit').remove();

                    // pokud uz neni zadna dalsi nepublikovana zprava v listu
                    if ( $('#unpublishedPressReleases .item').length == 0 )
                    {
                        // odstranim tlacitko nezverejnenych zprav
                        $('#admin #unpublishedButton').remove();
                    }
                }

                // odstranim vsechny predchozi hlasky
                $('#contentA .msg').remove();
                
                // pridam hlasku o editaci zpravy
                $('<div class="msg done">'+response.message+'</div>').insertAfter('#contentA #admin');
            }

            // ulozeni zeditovane zpravy ktera jeste nebyla publikovana a je ok
            else if ( response.status == 'edit-unpublished-ok' )
            {
                // pokud je vse ok, zavru popup
                $('#addPost').closest('.fancybox-wrap ').find('.fancybox-close').click();

                // pokud je v listu nepublikovanych zprav nejaka editovana, pridam ji pred ni = editoval jsem zpravu nepublikovanou
                if ( $('#unpublishedPressReleases .item.edit').length !== 0 )
                {
                    // nactu prave pridanou tiskovou zpravu
                    $( response.response ).insertAfter('#unpublishedPressReleases .item.edit');

                    // odstranim starou zpravu
                    $('#unpublishedPressReleases .item.edit').remove();
                }
                // jinak je jasny ze jsem editoval zpravu publikovanou
                else
                {
                    // nactu prave pridanou tiskovou zpravu
                    $('#unpublishedPressReleases .list').prepend( response.response);

                    // odstranim starou zpravu
                    $('#contentA .box.edit').remove();

                    // pokud jeste nejsou nejake nezverejnene zpravy
                    if ( $('#admin #unpublishedButton').length == 0 )
                    {
                        // pridam tlacitko nezverejnenych zprav
                        $('#admin').prepend('<a id="unpublishedButton" class="button buttonB icon edit popUpMedium" href="#unpublishedPressReleases">nepublikované zprávy</a>');
                    }
                }

                // odstranim vsechny predchozi hlasky
                $('#contentA .msg').remove();

                // pokud uz neni zadna dalsi tiskova zprava
                if ( $('#contentA .box').length == 0 )
                {
                    // pridam hlasku
                    $( '<div class="msg warning icon noClose">'+response.messageWarning+'</div>' ).insertAfter('#contentA #admin');
                }
                
                // pridam hlasku o editaci zpravy
                $('<div class="msg done">'+response.message+'</div>').insertAfter('#contentA #admin');
            }

            else
            {
                alert(response.message);
            } 

            // znovu nactu timeago aby se u nove pridanych zprav chytil cas
            $('#contentA .timeago').timeago(); 

        }
    });
}


// Editace tiskove zpravy
function editPressRelease()
{
    $('#contentA .box .editBig, #unpublishedPressReleases .control .edit').live('click', function(){

        var $this               = $(this),
            $addEditButton      = $('#addEditButton'),
            $addPost            = $('#addPost');

        // pokud je to editace zverejnene zpravy
        if ( $this.hasClass('editBig') )
        {
            var id = $this.data('id');

            // odstranim classu edit pokud je u zpravy v listu nezverejnenych zprav
            $('#unpublishedPressReleases .item').removeClass('edit');

            // odstranim classu edit pokud je u zpravy v listu zverejnenych zprav
            $('#contentA .box').removeClass('edit');

            // pridam classu edit editovane zprave v listu
            $this.closest('.box').addClass('edit');
        }

        // pokud je to editace nezverejnene zpravy
        if ( $this.hasClass('edit') )
        {
            var id = $this.data('id');

            // odstranim classu edit pokud je u zpravy v listu zverejnenych zprav
            $('#contentA .box').removeClass('edit');

            // odstranim classu edit pokud je u zpravy v listu nezverejnenych zprav
            $('#unpublishedPressReleases .item').removeClass('edit');

            // pridam classu edit editovane zprave v listu
            $this.closest('.item').addClass('edit');
        }

        data_to_send = "edit_press=" + id;

        $.ajax({
            url: '/ajax.php',
            //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
            dataType: 'json',
            type: 'post',
            data: data_to_send,
            success: function(response){

                if( response.status == 'ok' )
                {
                    // do inputu narvu vracene texty
                    $addPost.attr( 'data-id', response.id );
                    $addPost.find('input.heading').val( response.heading );
                    $addPost.find('textarea.perex').val( response.perex );
                    $addPost.find('textarea.message').val( response.text );
                    $addPost.find('input.date').val( response.date );
                    $addPost.find('input.time').val( response.time );

                    // pridam class edit / potrebuju vedet kdz neaktualizovat datum a cas vydani a kdy jo
                    $addEditButton.addClass('edit');
                    // otevru facybox
                    $addEditButton.click();
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;
    });
}


// Smazani tiskove zpravy
function deletePressRelease()
{
    $('#contentA .box .deleteBig, #unpublishedPressReleases .control .delete').live('click', function(){

        var $this = $(this),
            title = $(this).closest('.box').find('.title').text();

        // pokud je to smazani zverejnene zpravy
        if ( $this.hasClass('deleteBig') )
        {
            var id = $this.data('id');
        }

        // pokud je to smazani nezverejnene zpravy
        if ( $this.hasClass('delete') )
        {
            var id = $this.data('id');
            title  = $(this).closest('.item').find('.headLine').text();
        }

        // confirm na smazani zpravy       
        $.confirm({
            'title'     : 'Potvrdit smazání tiskové zprávy',
            'message'   : 'Opravdu si přejete smazat tiskovou zprávu "' + title + '"?',
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        data_to_send = "delete_press=" + id;

                        $.ajax({
                            url: '/ajax.php',
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            type: 'get',
                            data: data_to_send,
                            success: function(response){

                                if( response.status == 'ok' )
                                {
                                    // pokud je to smazani zverejnene zpravy
                                    if ( $this.hasClass('deleteBig') )
                                    {
                                        // odstranim smazanou zpravu
                                        $this.closest('.box').remove();

                                        // odstranim vsechny predchozi hlasky
                                        $('#contentA .msg').remove();

                                        // pokud uz neni zadna dalsi tiskova zprava
                                        if ( $('#contentA .box').length == 0 )
                                        {
                                            // pridam hlasku
                                            $( '<div class="msg warning icon noClose">'+response.messageWarning+'</div>' ).insertAfter('#contentA #admin');
                                        }
                                        
                                        // pridam hlasku o smazani zpravy
                                        $('<div class="msg done">'+response.message+'</div>').insertAfter('#contentA #admin');
                                    }

                                    // pokud je to smazani nezverejnene zpravy
                                    if ( $this.hasClass('delete') )
                                    {
                                        // odstranim smazanou zpravu
                                        $this.closest('.item').remove();

                                        // pokud uz neni zadna dalsi nepublikovana tiskova zprava
                                        if ( $('#unpublishedPressReleases .item').length == 0 )
                                        {
                                            // odstranim tlacitko nezverejnenych zprav
                                            $('#admin #unpublishedButton').remove();

                                            // zavru prazdny fancybox
                                            $('#unpublishedPressReleases').closest('.fancybox-wrap ').find('.fancybox-close').click();
                                        }

                                        // odstranim vsechny predchozi hlasky
                                        $('#contentA .msg').remove();

                                        // pokud uz neni zadna dalsi tiskova zprava
                                        if ( $('#contentA .box').length == 0 )
                                        {
                                            // pridam hlasku
                                            $( '<div class="msg noClose error">'+response.messageWarning+'</div>' ).insertAfter('#contentA #admin');
                                        }
                                        
                                        // pridam hlasku o smazani zpravy
                                        $('<div class="msg done">'+response.message+'</div>').insertAfter('#contentA #admin');
                                    }
                                }
                                else
                                {
                                    alert(response.message);
                                }

                            }
                        });

                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
        // konec

        return false;
    });
}


// Kliknuti na tlacitko odeslat v fancyboxu
function pressRelease(){

    $('#popUpFormAddPost').live('submit', function(){

        var formId   = $(this).attr('id'),
            formId   = '#' + formId,
            heading  = $(this).find('.heading'),
            perex    = $(this).find('.perex'),
            message  = $(this).find('.message'),
            start    = $(this).find('.start'),
            end      = $(this).find('.end');

        var valid = true;

        $(this).find('.error').removeClass('error');
        $(this).find('.errorMsg').hide();

        if ( !heading.val() )
        {
            heading.closest('.entries').addClass('error');
            heading.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( !perex.val() )
        {
            perex.closest('.entries').addClass('error');
            perex.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( !message.val() )
        {
            message.closest('.entries').addClass('error');
            message.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        /*
        if ( !start.val() )
        {
            start.closest('.entries').addClass('error');
            start.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( !end.val() )
        {
            end.closest('.entries').addClass('error');
            end.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        */

        // odesleme komentar
        if ( valid == true )
        {
            sendPressRelease(formId);
        }

        return false;
    });
}



/************************************************************
    ROSTER
************************************************************/

// odeslani pozvanky e-mailem
function sendInviteByEmail(formId)
{
    var str        = $(formId).serialize(),
        $submit    = $(formId).find('.submit button'),
        submitText = $submit.text();

    $submit.addClass('disabled').text('... čekejte prosím, odesílám');

    if ( formId == '#popUpFormAddUser' )
    {
        str += '&add_player=1';
    }
    if ( formId == '#popUpFormAddHost' )
    {
        str += '&add_host_player=1';
    }

    $.ajax({
        url: '/ajax.php',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        type: 'get',
        data: ""+str+"",
        success: function(response){

            // odeslani e-mailu je ok
            if ( response.status == 'ok' )
            {
                // pokud je vse ok, zavru popup
                $('#addUser').closest('.fancybox-wrap ').find('.fancybox-close').click();

                // odstranim predchozi hlasky
                $('#contentA > .msg').remove();

                // napisu done hlasku
                $( '<div class="msg noClose done">' + response.message + '</div>' ).insertAfter('#contentA #admin');

                // vyprazdnim inputy v popupu
                $('#popUpFormAddHost input.text').val('');

                // odstranim disabled z butonu
                $submit.removeClass('disabled').text(submitText);
            }
            else if ( response.status == 'host-add-ok' )
            {
                // pokud je vse ok, zavru popup
                $('#addUser').closest('.fancybox-wrap ').find('.fancybox-close').click();

                // pokud je nejaka hlaska, odstranim ji
                $('#contentA > .msg').remove();

                // odstranim starou soupisku
                $('#contentA .contentAbox').remove();

                // pridam aktualizovanou spupisku
                $( response.response ).insertAfter('#contentA #admin');

                // vymenim html v popupu editRoster
                $('#editRoster').html( response.editRoster );

                // napisu done hlasku
                $( '<div class="msg noClose done">' + response.message + '</div>' ).insertAfter('#contentA #admin');

                // vyprazdnim inputy v popupu
                $('#popUpFormAddHost input.text').val('');

                // odstranim disabled z butonu
                $submit.removeClass('disabled').text(submitText);
            }
            else
            {
                alert(response.message);

                // odstranim disabled z butonu
                $submit.removeClass('disabled').text(submitText);
            } 

        }
    });
}


// prepinani formularu podle typu uzivatele a zmena nazvu odesilaciho tlacitka
function userTypeSwitch(){

    $('#userTypeSwitch input').live('change', function(){

        var $popUpFormAddUser = $('#popUpFormAddUser'),
            $popUpFormAddHost = $('#popUpFormAddHost');

        if ( $(this).attr('id') == 'checkboxUser' )
        {
           $popUpFormAddUser.show();
           $popUpFormAddHost.hide();
        }
        else if ( $(this).attr('id') == 'checkboxHost' )
        {
            $popUpFormAddUser.hide();
            $popUpFormAddHost.show();
        }

        $(window).trigger('resize');

        return false;
    });

    // zmena nazvu submit buttonu
    $('#popUpFormAddHost .firstname, #popUpFormAddHost .surname').keyup(function(){

        var $submitButton = $('#popUpFormAddHost .submit button');

        // pokud je jeden z inputu jmeno nebo prijmeni vyplnen
        if ( $('#popUpFormAddHost .firstname').val().length > 0 || $('#popUpFormAddHost .surname').val().length > 0 )
        {
            $submitButton.text('vytvořit fiktivní účet');
        }
        else
        {
            $submitButton.text('odeslat pozvánku');
        }
        
    });

}


// kontrola odeslani formulare - pozvanka e-mailem
function inviteByEmail(){

    $('#popUpFormAddUser').live('submit', function(){

        var formId   = $(this).attr('id'),
            formId   = '#' + formId,
            email    = $(this).find('.email');

        var valid = true;

        $(this).find('.error').removeClass('error');
        $(this).find('.errorMsg').hide();

        if ( !email.val() || !email.val().match( /^([\w\+-\.]+@([\w-]+\.)+[\w-]{2,4})?$/ ) )
        {
            email.closest('.entries').addClass('error');
            email.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }

        // odesleme formular
        if ( valid == true )
        {
            sendInviteByEmail(formId);
        }

        return false;
    });

    $('#popUpFormAddHost').live('submit', function(){

        var formId    = $(this).attr('id'),
            formId    = '#' + formId,
            email     = $(this).find('.email'),
            firstname = $(this).find('.firstname'),
            surname   = $(this).find('.surname');

        var valid = true;

        $(this).find('.error').removeClass('error');
        $(this).find('.errorMsg').hide();
        firstname.removeClass('valid');
        surname.removeClass('valid');

        if ( (!email.val() || !email.val().match( /^([\w\+-\.]+@([\w-]+\.)+[\w-]{2,4})?$/ )) && !firstname.val() && !surname.val() )
        {
            email.closest('.entries').addClass('error');
            email.closest('.entries').find('.errorMsg.one').fadeIn('fast');
            valid = false;
        }
        else if ( !email.val() && !firstname.val() && !surname.val() )
        {
            firstname.closest('.entries').addClass('error');
            email.closest('.entries').find('.errorMsg.one').fadeIn('fast');
            valid = false;
        }
        else if ( !email.val() && firstname.val() && !surname.val() )
        {
            firstname.closest('.entries').addClass('error');
            email.closest('.entries').find('.errorMsg.one').fadeIn('fast');
            firstname.addClass('valid');
            valid = false;
        }
        else if ( !email.val() && !firstname.val() && surname.val() )
        {
            firstname.closest('.entries').addClass('error');
            email.closest('.entries').find('.errorMsg.one').fadeIn('fast');
            surname.addClass('valid');
            valid = false;
        }
        else if ( email.val() && (firstname.val() || surname.val()) )
        {
            email.closest('.entries').addClass('error');
            firstname.closest('.entries').addClass('error');
            email.closest('.entries').find('.errorMsg.two').fadeIn('fast');
            valid = false;
        }

        // odesleme formular
        if ( valid == true )
        {
            sendInviteByEmail(formId);
        }

        return false;
    });
}


// odeslani uprav v soupisce
function sendEditRoster(form)
{
    var $submit = $(form).find('.submit button');

    $submit.addClass('disabled').text('... čekejte prosím, ukládám');

    // nastaveni zobrazeni zmen na soupisce na nastence
    // vepisu parametry do formulare nez ho serializuji
    // group zarazeni
    $('#popUpFormEditRoster #inclusions .entries').each(function(){

        var $playerStatusAdded   = $(this).find('.playerStatusAdded'),
            $playerStatusRemoved = $(this).find('.playerStatusRemoved');

        // je na soupisce a je videt
        if ( $(this).find('select.position').val() != '0' && $(this).find('.hostShow').hasClass('active') )
        {
            // nemel uz jako vychozi hodnutu ze je videt
            if ( $playerStatusAdded.attr('data-status') != '1' )
            {
                $playerStatusAdded.val('1');
            }
            else
            {
                $playerStatusAdded.val('');
            }
        }
        // bud neni na soupisce, nebo neni videt nebo oboji
        else
        {
            // nemel uz jako vychozi hodnutu ze nejde videt
            if ( $playerStatusRemoved.attr('data-status') != '1' )
            {
                $playerStatusRemoved.val('1');
            }
            else
            {
                $playerStatusRemoved.val('');
            }
        }

    });

    // group nezarazeni
    $('#popUpFormEditRoster #notInclusions .entries').each(function(){

        var $playerStatusAdded   = $(this).find('.playerStatusAdded');

        // pouze pokud maji pozici a zobrazit
        if ( $(this).find('select.position').val() != '0' && $(this).find('.hostShow').hasClass('active') )
        {
            $playerStatusAdded.val('1');
        }

    });

    var str = $(form).serialize();

    str += '&edit_roster=1';

    $.ajax({
        url: '/ajax.php',
        //contentType: "application/json; charset=utf-8", kdzy je to POST, s timto to nefunguje, funguje jen s GET
        dataType: 'json',
        type: 'post',
        data: ""+str+"",
        success: function(response){

            // odeslani e-mailu je ok
            if ( response.status == 'ok' )
            {
                // FIXME - musim si vytvorit, protoze ji pouzivam u funkce editRosterClose(), vymyslet lip
                var $editRoster = '';

                // pokud je vse ok, zavru popup
                $('#editRoster').closest('.fancybox-wrap ').find('.fancybox-close').click();

                // pokud je nejaka hlaska, odstranim ji
                $('.msg').remove();

                // odstranim starou soupisku
                $('#contentA .contentAbox').remove();

                // pridam aktualizovanou spupisku
                $( response.response ).insertAfter('#contentA #admin');

                // vymenim html v popupu editRoster
                $('#editRoster').html( response.editRoster );

                // znovu nactu tooltip
                $('.help').removeData('tooltip');
                $('.help').tooltip({ relative: true, offset: [-10, 0] });

                // napisu done hlasku
                $( '<div class="msg noClose done">' + response.message + '</div>' ).insertAfter('#contentA #admin');

            }
            else
            {
                alert(response.message);
            } 

        }
    });

}


// kontrola odeslani formulare - soupiska edit
function editRoster(){

    $('#popUpFormEditRoster').live('submit', function(){

        var formId   = $(this).attr('id'),
            formId   = '#' + formId;

        var valid = true;

        $(this).find('.error').removeClass('error');
        $(this).find('.errorMsg').hide();


        // projdu vsechny inclusions
        $(this).find('#inclusions .group .entries:not(.deleted)').each(function(){

            var number   = $(this).find('.number'),
                position = $(this).find('.position');

            if ( (!number.val() && position.val() != '0') && (!number.val() && position.find('option:selected').attr('data-staff') != '1') )
            {
                number.closest('.entries').addClass('errors');
                number.addClass('error');
                number.closest('.group').find('.errorMsg').fadeIn('fast');
                valid = false;
            }

            /*
            if ( !position.val() )
            {
                position.closest('.entries').addClass('errors');
                position.addClass('error');
                position.closest('.group').find('.errorMsg').fadeIn('fast');
                valid = false;
            }
            */

        })


        // projdu vsechny notInclusions
        $(this).find('#notInclusions .group .entries:not(.deleted)').each(function(){

            var number   = $(this).find('.number');
                position = $(this).find('.position');

            if ( (!number.val() && position.val() != '0') && (!number.val() && position.find('option:selected').attr('data-staff') != '1') )
            {
                number.closest('.entries').addClass('errors');
                number.addClass('error');
                number.closest('.group').find('.errorMsg').fadeIn('fast');
                valid = false;
            }

        })


        // odesleme komentar
        if ( valid == true )
        {
            sendEditRoster(formId);
        }

        return false;
    });
}


// zavreni popupu editace soupisky
function editRosterClose(){

    $editRoster = $('#editRoster').html();

    // kliknuti na tlacitko, ktere otevira popup
    $('#editRosterButton').click(function(){

        // ulozim si html co bylo v popupu
        $editRoster = $('#editRoster').html();

        // nastaveni vychozich hodnot do formulare pro dalsi zpracovani pri odesilani
        $('#popUpFormEditRoster .entries').each(function(){

            // neni v kategorii nezarazenych a zaroven je zobrazeny na soupisce
            if ( $(this).find('select.position').val() != '0' && $(this).find('.hostShow').hasClass('active') )
            {
                $(this).find('.playerStatusAdded').attr('data-status', '1');
                $(this).find('.playerStatusRemoved').attr('data-status', '0');
            }
            else
            {
                $(this).find('.playerStatusAdded').attr('data-status', '0');
                $(this).find('.playerStatusRemoved').attr('data-status', '1');
            }

        });

    });

    // kliknu na zavrit popup - FIXME deje se to pri jakemkoliv popupu na strance
    $('.fancybox-close').live('click', function(){

        // vlozim ulozene html co bylo puvodne v popupu
        $('#editRoster').html( $editRoster );

        // odstranim hlasky pokud byly
        $('#contentA > .msg').remove();

    });

}


// nastaveni hostujiciho hrace
function hostPlayerControls(){

    /*
    $('#editRoster .hostControls .hostInput:not(:checked)').each(function(){

        $(this).closest('.hostControls').find('.hostShow').hide();

    });
    */

    $('#editRoster .hostControls input:not(.disabled)').live('change', function(){

        var $this          = $(this),
            $labelHost     = $this.closest('.hostControls').find('.host'),
            $labelShowHost = $this.closest('.hostControls').find('.hostShow');

        // pokud je to input H
        if ( $this.hasClass('hostInput') )
        {
            // pridam labelu H class
            $labelHost.toggleClass('active');

            // pokud jsem input checknul
            /*
            if ( $(this).is(':checked') )
            {
                $labelShowHost.show();
            }
            else
            {
                $labelShowHost.hide();
            }
            */
        }
        // pokud je to input Z
        else if ( $this.hasClass('hostShowInput') )
        {
            // vymenim labelu Z class
            $labelShowHost.toggleClass('active');
        }

    });

}


// odstraneni hrace z tymu
function playerControls(){

    // smazani hrace
    $('#editRoster .delete').live('click', function(){

        var name                 = $(this).closest('.entries').find('.name').text(),
            $player              = $(this).closest('.entries'),
            $playerStatus        = $player.find('.playerStatus'),
            $playerStatusRemoved = $player.find('.playerStatusRemoved');

        // confirm na smazani zpravy       
        $.confirm({
            'title'     : 'Odstranit hráče z týmu',
            'message'   : 'Opravdu si přejete odstranit hráče "' + name + '" z týmu?<br />Hráč bude po uložení soupisky z týmu odstraněn a budou mu zamezeny všechny aktivity spojené s týmem!',
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        $playerStatus.val('1');
                        $playerStatusRemoved.val('1');
                        $player.addClass('deleted').hide();

                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
        // konec

    });

}


/************************************************************
    CONFIRM DECLINE INVITATION
************************************************************/

// potvrzeni / odmitnuti vstupu do tymu
function confirmDeclineInvitationTeam(){

    $('#contentA .inviteTeam .response').click(function(){

        var link          = $(this).attr('data-href'),
            $responseText = $(this).closest('.responseText'),
            data_to_send  = link;

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                // vse je ok a zadost byla prijata
                if ( response.status == 'confirm' )
                {
                    // vlozim done hlasku primo do textu
                    $( response.messageConfirm ).insertAfter( $responseText );

                    // odstranim predchozi text
                    $responseText.remove();

                    // odstranim msg pokud nejaky je
                    $('#contentA .msg').remove();

                    // vlozim done box
                    $('#contentA').prepend( response.messageConfirmBox );
                }
                // vse je ok a zadost byla odmitnuta
                else if ( response.status == 'decline' )
                {
                    // vlozim hlasku primo do textu
                    $( response.messageDecline ).insertAfter( $responseText );

                    // odstranim predchozi text
                    $responseText.remove();

                    // odstranim box pokud nejaky je
                    $('#contentA .msg').remove();

                    // vlozim done box
                    $('#contentA').prepend( response.messageDeclineBox );
                }
                else
                {
                    alert(response.message);
                } 

            }
        });

        return false;

    });

}


/************************************************************
    CONFIRM DECLINE MEMBER
************************************************************/

// potvrzeni / odmitnuti vstupu clena do tymu
function confirmDeclineInvitationMember(){

    $('#contentA .inviteMember .response').click(function(){

        var link          = $(this).attr('data-href'),
            $responseText = $(this).closest('.responseText'),
            data_to_send  = link;

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                // vse je ok a zadost byla prijata
                if ( response.status == 'confirm' )
                {
                    // vlozim done hlasku primo do textu
                    $( response.messageConfirm ).insertAfter( $responseText );

                    // odstranim predchozi text
                    $responseText.remove();

                    // odstranim msg pokud nejaky je
                    $('#contentA .msg').remove();

                    // vlozim done box
                    $('#contentA').prepend( response.messageConfirmBox );
                }
                // vse je ok a zadost byla odmitnuta
                else if ( response.status == 'decline' )
                {
                    // vlozim hlasku primo do textu
                    $( response.messageDecline ).insertAfter( $responseText );

                    // odstranim predchozi text
                    $responseText.remove();

                    // odstranim box pokud nejaky je
                    $('#contentA .msg').remove();

                    // vlozim done box
                    $('#contentA').prepend( response.messageDeclineBox );
                }
                else
                {
                    alert(response.message);
                } 

            }
        });

        return false;

    });

}


/************************************************************
    MESSAGES
************************************************************/

// ovladani messages po rozbaleni vsech zprav
function messages(){

    $('#contentA').delegate('.message .containerMessages', 'click', function() {

        var $thisContainerMessages      = $(this),
            $messageContainer           = $('#contentA .message-container'),
            $thisMessageContainer       = $(this).closest('.message-container');


        // pokud kliknu na jiz aktivni box, nedelam nic
        if ( $thisMessageContainer.hasClass('active') )
        {
            return false;
        }
        else
        {
            $('#contentA .message-container .containerMessage').not('.last').addClass('forBlind');
            $thisMessageContainer.find('.containerMessage').removeClass('forBlind');

            $messageContainer.removeClass('active');
            $messageContainer.find('.controlPanel').addClass('forBlind');

            $thisMessageContainer.addClass('active');
            $thisMessageContainer.find('.controlPanel').removeClass('forBlind');
        }


        // zascrolluji v konverzaci uplne dolu
        var thisContainerMessagesHeight = $thisMessageContainer.find('.inner').height(),
            thisContainerMessagesHeight = thisContainerMessagesHeight + 100; // + 100px pro jistotu

        $thisContainerMessages.animate({
            scrollTop: thisContainerMessagesHeight
        }, 1);


        // zascrolluji oknem na vrsek konverzace
        $('html, body').animate({ scrollTop: $thisMessageContainer.offset().top - 60 }, 750 ); // - 60 je vyska fixniho headeru a 10px offset aby to bylo pekne


        // zjisteni jestli uz je novinka prectena
        var conversationId = $(this).closest('.containerMessages').attr('id'),
            data_to_send   = 'readConversation=1&id=' + conversationId;

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                // vse je ok
                if ( response.status == 'ok' )
                {
                    var $notificationMsg      = $('#notification .messages'),
                        $notificationMsgCount = $notificationMsg.find('.count');

                    $notificationMsgCount.text(response.messageCount);

                    // odeberu stitek o neprectene zprave
                    $('.message-container.active .containerName .unread').remove();

                    // pokud neni nejaka nova, odeberu class new
                    if( response.messageCount == 0 )
                    {
                        $notificationMsg.removeClass('new');
                    }
                }
                else
                {
                    alert(response.message);
                } 

            }
        });

        return false;
    });

}


// ovladani konverzace
function messageControls()
{
    // smazat konverzaci
    $('#contentA').delegate('.controls .delete, .controls .leave', 'click', function(){

        var $this          = $(this),
            $conversation  = $this.closest('.message-container'),
            conversationID = $conversation.find('.message .containerMessages').attr('id');

        if ( $this.hasClass('delete') )
        {
            var message = 'Opravdu si přejete smazat tuto konverzaci a všechny zprávy v ní nenávratně odstranit?';
            var data_to_send = 'deleteConversation=1&id=' + conversationID;
        }
        else if ( $this.hasClass('leave') )
        {
            var message = 'Opravdu si přejete opustit a smazat tuto konverzaci? Další zprávy z této konverzace vám již nebudou doručovány!';
            var data_to_send = 'leaveConversation=1&id=' + conversationID;
        }

        // confirm na smazani zpravy       
        $.confirm({
            'title'     : 'Potvrdit smazání konverzace',
            'message'   : message,
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        $.ajax({
                            url: '/ajax.php',
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            type: 'get',
                            data: data_to_send,
                            success: function(response){

                                // vse je ok
                                if ( response.status == 'ok' )
                                {
                                    // odstranim zpravu
                                    $conversation.remove();

                                    // aktualizuju pocet neprectenych zprav
                                    var $notificationMsg      = $('#notification .messages'),
                                        $notificationMsgCount = $notificationMsg.find('.count');

                                    $notificationMsgCount.text(response.messageCount);

                                    // pokud je nejaka nova, pridam class new
                                    if( response.messageCount == 0 )
                                    {
                                        $notificationMsg.removeClass('new');
                                    }

                                    // pokud uz neni zadna zprava, odstranim title a pridam hlasku
                                    /*
                                    if ( response.message != '' )
                                    {
                                        var $headline = $('#contentA .contentAbox .headline');

                                        // pridam message
                                        $( response.message ).insertAfter( $headline );

                                        // odstranim title
                                        $headline.next('.title').remove();
                                    }
                                    */
                                }
                                else
                                {
                                    alert(response.message);
                                } 

                            }
                        });
                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
        // konec

        return false;
    });
}


// po zavreni popupu krizkem ho vyprazdnim
function messagePopupClose()
{
    $('.fancybox-close').live('click', function(){

        var $popUpFormSendNewMessage = $('#popUpFormSendNewMessage');

        // odstranim prijemce ze selectu
        $popUpFormSendNewMessage.find('#messageRecipients option').remove();
        // odstranim prijemce z naseptavace
        $popUpFormSendNewMessage.find('.holder .bit-box').remove();
        // odstranim text zpravy
        $popUpFormSendNewMessage.find('#messageText').val('');
        
    });
}


// odeslani zpravy z popupu
function messagePopup()
{
    // kliknuti na odeslani zpravy v popupu
    $('#popUpFormSendNewMessage').live('submit', function(){

        var formId = '#popUpFormSendNewMessage';

        // vytvorim si promenou pro zjisteni ve funkci sendMessage(formId);
        popupMessage = true;

        // odesleme zpravu
        sendMessage(formId);

        $(this).find('input#messageText').val('');
        //$(this).find('input.heading').val('');

        $('.fancybox-close').click();

        return false;
    });
}


// donacteni dalsich zprav po scrollovani
function viewMoreMessages(start)
{   
    var loading            = '<p id="loading"><!-- --></p>',
        $containerMessages = $('#contentA .message-container.active .containerMessages'),
        $inner             = $('#contentA .message-container.active .containerMessages .inner'),
        conversationID     = $containerMessages.attr('id');

    // zobrazim loading a zascroluju o vysku loadingu
    //$( loading ).prependTo( $inner ); vypnuto kvuli scrollu

    data_to_send = 'viewMoreMessages=1&id=' + conversationID + '&start=' + start;

    $.ajax({
        url: '/ajax.php', 
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        type: 'get',
        data: data_to_send,
        success: function(response){

            if (typeof(response) == 'object' && response != null)
            {
                // pokud je to ok a vrati to nejake zpravy
                if( response.status == 'ok' )
                {
                    //$("#loading").remove();

                    // zjistim si puvodni vysku a zascrollovani
                    var position = $containerMessages.scrollTop(),
                        height   = $inner.height();

                    // pridam nove zpravy
                    $(response.response).prependTo( $inner );

                    // zascrolluji tam kde jsem byl pred nactenim novych zprav
                    var newHeight   = $inner.height();
                        newPosition = newHeight - height,
                        newScroll   = position + newPosition;

                    $( $containerMessages ).animate({ scrollTop: newScroll }, 0);

                    // znovu nactu timeago aby se u nove pridanych zprav chytil cas
                    $('#contentA .message-container.active .containerMessages .timeago').timeago();

                    // znovu nactu funkci, aby scroll fungoval poĹ™Ăˇd dokola
                    $( $containerMessages ).scroll( infiniteScrollMessages );
                }

                // pokud je to ok a vrati to ze uz nejsou dalsi zpravy k zobrazeni
                else if ( response.status == 'ok-no-more')
                {
                    //$("#loading").remove();

                    $(response.response).prependTo( $inner );
                }

                else
                {
                    alert(response.message);
                }
            }
            else
            {
                alert(response.message);
            }
        }
    });
}


function infiniteScrollMessages()
{

    if ( $('#contentA .message-container.active .containerMessages').height() >= 400 && $('#contentA .message-container.active .containerMessages').scrollTop() < 200 )
    {
        $('#contentA .message-container.active .containerMessages').unbind('scroll', infiniteScrollMessages);

        var start    = $('#contentA .message-container.active .containerMessage:first').attr('data-id');
            //position = $('#contentA .message-container.active .containerMessages').scrollTop(),
            //height   = $('#contentA .message-container.active .containerMessages .inner').height();

        viewMoreMessages( start );
    }

}



/************************************************************
    EVENT MANAGE
************************************************************/

// vyber typu udalosti
function selectEventType(){

    $('#contentA .type select').change(function(){

        var $this        = $(this),
            $thisVal     = $(this).val(),
            data_to_send = 'manageEvent=' + $thisVal,
            $hiddenInput = $('#eventTypeInput'),
            loading      = '<p id="loading"><!-- --></p>';

        // zobrazim loading
        $( loading ).appendTo('#contentA.eventManage form fieldset');

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                // vse je ok
                if ( response.status == 'ok' )
                {
                    // odstranim loading
                    $('#loading').remove();

                    // vlozim spravny content
                    $(response.response).appendTo('#contentA.eventManage form fieldset');

                    // vlozim hodnutu ze selectu do hdden inputu
                    $hiddenInput.val( $thisVal );

                    // disabluju select
                    $this.attr('disabled', 'disabled');

                    // naseptavac tymu
                    $('#teamSearch').fcbkcomplete({
                        json_url: '/ajax.php?eventOpponent=1',
                        width: '200',
                        addontab: true, // nefunguje, check
                        firstselected : true,
                        maxitems: 1,
                        filter_selected: true,
                        input_min_size: 0,
                        maxshownitems: 5,
                        height: 5,
                        cache: true, // nefunguje, check
                        complete_text: '',
                        delay : 500,
                        onselect: function(){
                            $('#teamSearch_annoninput').hide();
                        },
                        onremove: function(){
                            $('#teamSearch_annoninput').show();
                            $('#teamSearch_annoninput').click();
                        }
                    });

                    // kalendar
                    $('#contentA .datepicker').datepicker({
                        //minDate: 0,
                        //maxDate: "+1M +10D",
                        dateFormat: "yy-mm-dd"
                    });

                    // kontrola pro psani pouze cisel do inputu
                    $('#contentA .dateAndTime .from, #contentA .dateAndTime .to').numericOnly();
                }
                else
                {
                    alert(response.message);
                } 

            }
        });

    });
}

// pokracovani na dalsi krok
function eventNextStep(){

    $('#contentA').delegate('#eventNextStep, #eventSubmit', 'click', function(){

        var $this             = $(this),
            $form             = $(this).closest('form'),
            $teamSearch       = $('#teamSearch'),
            $teamSearchInput  = $('#teamSearch_annoninput'),
            $teamSearchHolder = $form.find('.holder'),
            $name             = $form.find('.name input'),
            $place            = $form.find('.place input'),
            $date             = $form.find('.dateAndTime .date'),
            $from             = $form.find('.dateAndTime .from'),
            $to               = $form.find('.dateAndTime .to'),
            $repeatCheckbox   = $form.find('#repeat'),
            $repeatDate       = $form.find('.dateAndTime .repeatDate'),
            $repeatSelect     = $form.find('.repeatSelect'),
            $allMembers       = $form.find('#allMembers'),
            $membersInputs    = $form.find('.members input');

        var valid = true;


        $form.find('.error').removeClass('error');
        $form.find('.errorMsg').hide();


        if ( $teamSearch.length && !$teamSearch.find('option').length && !$teamSearchInput.find('.maininput').val() )
        {
            $teamSearch.closest('.entries').addClass('error');
            $teamSearch.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( $name.length && !$name.val() )
        {
            $name.closest('.entries').addClass('error');
            $name.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( $place.length && !$place.val() )
        {
            $place.closest('.entries').addClass('error');
            $place.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }

        // kvuli vic inputu v jednom entries
        $date.addClass('valid');
        $from.addClass('valid');
        $to.addClass('valid');
        $repeatDate.addClass('valid');
        $repeatSelect.addClass('valid');

        if ( $date.length && !$date.val() )
        {
            $date.closest('.entries').addClass('error');
            $date.removeClass('valid');
            $date.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( $from.length && !$from.val() )
        {
            $from.closest('.entries').addClass('error');
            $from.removeClass('valid');
            $from.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( $to.length && !$to.val() )
        {
            $to.closest('.entries').addClass('error');
            $to.removeClass('valid');
            $to.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( $repeatDate.length && $repeatCheckbox.is(':checked') && !$repeatDate.val() )
        {
            $repeatDate.closest('.entries').addClass('error');
            $repeatDate.removeClass('valid');
            $repeatDate.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }
        if ( $allMembers.length && !$allMembers.is(':checked') && !$membersInputs.is(':checked') )
        {
            $allMembers.closest('.entries').addClass('error');
            $allMembers.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }


        // vse je validni, pokracujeme
        if ( valid == true && $this.attr('id') != 'eventSubmit' )
        {

            var $teamHiddenName  = $('#hiddenTeamName'),
                data_to_send     = 'manageEvent=eventUploadLogo',
                loading          = '<p id="loading"><!-- --></p>';

            // zobrazim loading
            $( loading ).appendTo('#contentA.eventManage form fieldset');

            // je vybran tym na mtw nebo je to mezikrok u tymu co neni na mtw
            if ( $teamSearch.find('option').length || $this.hasClass('uploadLogo') )
            {
                data_to_send = 'manageEvent=teamSelected';

                // pridam classu disable
                $teamSearchHolder.closest('.selectOne').addClass('disabled');

                // odstranim krizek pro odebrani tagu
                $teamSearchHolder.find('.closebutton').remove();

                // odstranim input pro psani, pokud uz neni disabled
                // pokud je disabled, je to mezikrok u tymu co neni na mtw a uz to je osetrene
                if( !$teamSearchInput.find('.maininput').attr('disabled', 'disabled') )
                {
                    $teamSearchInput.remove();
                }
            }

            // pokud je to novy tym
            if ( !$teamSearch.find('option').length && $teamSearchInput.find('.maininput').val() )
            {
                var teamSearchInput = $teamSearchInput.find('.maininput').val();
                
                $teamHiddenName.val( teamSearchInput );

                // pridam classu disable
                $teamSearchInput.closest('.selectOne').addClass('disabled');

                // udelam input disabled
                $teamSearchInput.find('.maininput').attr('disabled', 'disabled');
            }

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    // vse je ok
                    if ( response.status == 'ok' )
                    {
                        // odstranim loading
                        $('#loading').remove();

                        // vlozim spravny content
                        $(response.response).appendTo('#contentA.eventManage form fieldset');

                        $this.closest('p').remove();

                        // kalendar
                        $('#contentA .datepicker').datepicker({
                            //minDate: 0,
                            //maxDate: "+1M +10D",
                            dateFormat: "yy-mm-dd"
                        });

                        // kontrola pro psani pouze cisel do inputu
                        $('#contentA .dateAndTime .from, #contentA .dateAndTime .to').numericOnly();
                        
                    }
                    else
                    {
                        alert(response.message);
                    } 

                }
            });

        }

        if ( valid == true && $this.attr('id') == 'eventSubmit' )
        {          
            if ( ga.hasOwnProperty('loaded') && ga.loaded === true )
            {
                if ( $form.find('.email').length && $form.find('.email input').val().length )
                {
                    var email = $form.find('.email input').val();

                    ga('send', 'event', {
                        'eventCategory' : 'Send team invite from event',
                        'eventAction' : 'submit',
                        'eventLabel' : email,
                        'hitCallback' : function () {
                            return true;
                        }
                    });
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }

    });

}



// Smazani a odmitnuti udalosti
function deleteEvent()
{
    $('#contentA').delegate('.events .deleteBig', 'click', function(){

        var $this = $(this),
            id    = $this.closest('tr').attr('data-id'),
            title = $this.closest('tr').find('.title').text(),
            date  = $this.closest('tr').find('.date .help').text();

        if ( $this.hasClass('declineEvent') )
        {
            // confirm odmitnuti zpravy       
            $.confirm({
                'title'     : 'Potvrdit odmítnutí události',
                'message'   : 'Opravdu si přejete odmítnout událost "' + title + '"?',
                'buttons'   : {
                    'potvrdit'  : {
                        'class' : 'buttonA icon confirm',
                        'action': function(){

                            var data_to_send = 'declineEvent=' + id;

                            $.ajax({
                                url: '/ajax.php',
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                type: 'get',
                                data: data_to_send,
                                success: function(response){

                                    if( response.status == 'ok' )
                                    {
                                        // smazu radek smazane udalosti
                                        $this.closest('tr').remove();

                                        // pokud zustane v udalostech posledni radek, pridam mu class first
                                        var $tr      = $this.closest('tbody').find('tr.item'),
                                            trLength = $tr.length;

                                        if( trLength == 1 )
                                        {
                                            $tr.addClass('first');
                                        }
                                    }
                                    else
                                    {
                                        alert(response.message);
                                    }

                                }
                            });

                        }
                    },
                    'zrušit'    : {
                        'class' : 'buttonA',
                        'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
            // konec
        }
        else
        {
            // confirm na smazani zpravy       
            $.confirm({
                'title'     : 'Potvrdit smazání události',
                'message'   : 'Opravdu si přejete smazat událost "' + title + ', ' + date + '"?',
                'buttons'   : {
                    'potvrdit'  : {
                        'class' : 'buttonA icon confirm',
                        'action': function(){

                            var data_to_send = 'deleteEvent=' + id;

                            $.ajax({
                                url: '/ajax.php',
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                type: 'get',
                                data: data_to_send,
                                success: function(response){

                                    if( response.status == 'ok' )
                                    {
                                        // smazu radek smazane udalosti
                                        $this.closest('tr').remove();

                                        // pokud zustane v udalostech posledni radek, pridam mu class first
                                        var $tr      = $this.closest('tbody').find('tr.item'),
                                            trLength = $tr.length;

                                        if( trLength == 1 )
                                        {
                                            $tr.addClass('first');
                                        }
                                    }
                                    else
                                    {
                                        alert(response.message);
                                    }

                                }
                            });

                        }
                    },
                    'zrušit'    : {
                        'class' : 'buttonA',
                        'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
            // konec
        }

        return false;
    });
}


// potvrzení události
function confirmEvent()
{
    $('#contentA').delegate('.events .checkBig', 'click', function(){

        var $this = $(this),
            id    = $this.closest('tr').attr('data-id'),
            title = $this.closest('tr').find('.title').text();

        // je to link na info, ze tym nema vytvorenou zadnou sezonu a nechci posilat ajax
        if ( $this.hasClass('noAjax') )
        {

        }
        // je to potvrzeni vysledku
        else if ( $this.hasClass('result') )
        {
            // confirm na prijmuti vysledku     
            $.confirm({
                'title'     : 'Přijmout výsledek a statistiky',
                'message'   : 'Opravdu si přejete přijmout výsledek a statistiky události "' + title + '"? Událost poté již nebude možné editovat ani smazat.',
                'buttons'   : {
                    'potvrdit'  : {
                        'class' : 'buttonA icon confirm',
                        'action': function(){

                            var data_to_send = 'confirmResult=' + id;
                            
                            $.ajax({
                                url: '/ajax.php',
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                type: 'get',
                                data: data_to_send,
                                success: function(response){

                                    if ( response.status == 'ok' )
                                    {
                                        // zmenim cely radek
                                        $this.closest('tr').replaceWith( response.response );

                                        // prvnimu tr pridam class first, at sem ho upravil nebo ne
                                        $('#contentA .events tbody tr:first').addClass('first');
                                    }
                                    else
                                    {
                                        alert(response.message);
                                    }

                                }
                            });

                        }
                    },
                    'zrušit'    : {
                        'class' : 'buttonA',
                        'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
            // konec

            return false;
        }
        // je to potvrzeni udalosti
        else
        {
            var season = $this.attr('data-season');

            // pokud zvu celý tým
            if ( $this.attr('data-team') == 'true' )
            {
                var teamInvite = ' a na kterou budou pozváni všichni hráči z vašeho týmu';
            }
            else
            {
                var teamInvite = '';
            }

            // confirm na prijmuti udalosti
            $.confirm({
                'title'     : 'Přijmout událost',
                'message'   : 'Opravdu si přejete přijmout událost "' + title + '", která bude zařazena do sezóny: "' + season + '"' + teamInvite + '?<br />Po potvrzení již nastavení události nepůjde změnit!',
                'buttons'   : {
                    'potvrdit'  : {
                        'class' : 'buttonA icon confirm',
                        'action': function(){

                            var data_to_send = 'confirmEvent=' + id;

                            $.ajax({
                                url: '/ajax.php',
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                type: 'get',
                                data: data_to_send,
                                success: function(response){

                                    if ( response.status == 'ok' )
                                    {
                                        // zmenim cely radek
                                        $this.closest('tr').replaceWith( response.response );

                                        // prvnimu tr pridam class first, at sem ho upravil nebo ne
                                        $('#contentA .events tbody tr:first').addClass('first');
                                    }
                                    else
                                    {
                                        alert(response.message);
                                    }

                                }
                            });

                        }
                    },
                    'zrušit'    : {
                        'class' : 'buttonA',
                        'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                    }
                }
            });
            // konec

            return false;
        }

    });
}


// donacteni vice udalosti
function moreEvents()
{
    $('#contentA').delegate('.events .more a', 'click', function(){

        var $this  = $(this),
            thisID = $this.attr('id');


        // podle typu toho co chci donacist si ulozim id posledni udalosti
        if( thisID == 'future' )
        {
            var $thisTr = $this.closest('tbody').find('tr.item:first'),
                start   = $thisTr.attr('data-id');
        }
        if( thisID == 'past' )
        {
            var $thisTr = $this.closest('tbody').find('tr.item:last'),
                start   = $thisTr.attr('data-id');
        }

        // vytvorim data k odeslani
        var $topFilter   = $('.topFilter'),
            year         = $topFilter.find('.activeFilter.year a').attr('data-id'),
            type         = $topFilter.find('.activeFilter.type a').attr('data-id'),
            team         = $('#teamInfo').attr('data-id'),
            data_to_send = 'viewMoreEvents=1&type=' + thisID + '&start=' + start + '&fyear=' + year + '&ftype=' + type + '&fteam=' + team;


        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if( response.status == 'ok-future' )
                {
                    $(response.response).insertBefore( $thisTr );

                    // znovu nactu tooltip
                    $('.help').removeData('tooltip');
                    $('.help').tooltip({ relative: true, offset: [-10, 0] });

                    // pokud uz nejsou dalsi prispevky, odeberu button
                    if ( response.end )
                    {
                        $this.closest('tr').remove();
                    }
                }
                else if( response.status == 'ok-past' )
                {
                    $(response.response).insertAfter( $thisTr );

                    // znovu nactu tooltip
                    $('.help').removeData('tooltip');
                    $('.help').tooltip({ relative: true, offset: [-10, 0] });
                    
                    // pokud uz nejsou dalsi prispevky, odeberu button
                    if ( response.end )
                    {
                        $this.closest('tr').remove();
                    }
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;
    });
}



/************************************************************
    EVENT EDIT RESULT
************************************************************/

// navigace
function editResultNavi(){

    // kliknuti na button - ulozit nastaveni
    $('#resultsContainer .saveOptions .button').click(function(){

        var loading = '<p id="loading"><!-- --></p>';

        // zobrazim loading
        $( loading ).appendTo('#resultsContainer');

        var $form         = $('#resultsContainer'),
            data_to_send  = $form.serialize();

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if( response.status == 'ok' )
                {
                    // skryji loading
                    $('#loading').remove();

                    // vymenim title
                    $('#contentA .headline').text( response.title );

                    // pokud jsou nejake mezivysledky
                    if (response.results.length)
                    {
                        $('#contentA .finalResult').removeClass('noResults');
                    }

                    if ( $('#resultHeader .results').length )
                    {
                        // pridam mezivysledky
                        $('#resultHeader .results').replaceWith(response.results);
                    }
                    else
                    {
                        $(response.results).insertAfter('#contentA .finalResult');
                    }

                    // odstranim predchozi #resultsContainer
                    $('#resultsContainer').remove();

                    // vlozim novy
                    $(response.response).insertAfter('#resultHeader');

                    // pokud neni v mezivysledku zadane prodlouzeni, skryju ho
                    var $overtimeBox  = $('#resultHeader .resultBox .overtimeBox'); // znovu si ulozim ty, co me vratil ajax

                    if ( !$overtimeBox.find('.overtimeHome').text().length || !$overtimeBox.find('.overtimeAway').text().length )
                    {
                        $overtimeBox.hide();
                    }

                    // znovu nactu tooltip
                    $('.help').removeData('tooltip');
                    $('.help').tooltip({ relative: true, offset: [-10, 0] });

                    /* spravne nastaveni polozek v selectech s hraci a cisly */
                    $('#rosterBox .left select.player option:selected:not([value="000"])').each(function() {
                        var optionVal = $(this).val();
                        $('#rosterBox .left select.player option[value="' + optionVal + '"]:not(:selected)').remove();
                    });
                    $('#rosterBox .right select.player option:selected:not([value="000"])').each(function() {
                        var optionVal = $(this).val();
                        $('#rosterBox .right select.player option[value="' + optionVal + '"]:not(:selected)').remove();
                    });
			        $('#rosterBox .left select.number option:selected:not([value="000"])').each(function() {
                        var optionVal = $(this).val();
                        $('#rosterBox .left select.number option[value="' + optionVal + '"]:not(:selected)').remove();
                    });
                    $('#rosterBox .right select.number option:selected:not([value="000"])').each(function() {
                        var optionVal = $(this).val();
                        $('#rosterBox .right select.number option[value="' + optionVal + '"]:not(:selected)').remove();
                    });
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;

    });

    
    // kliknuti na button - dalsi krok
    $('#contentA').delegate('.nextLink .button', 'click', function(){

        var $stepsNavigation  = $('#stepsNavigation'),
            $activeItem       = $stepsNavigation.find('.active'),
            $resultsContainer = $('#resultsContainer'),
            $activeTab        = $resultsContainer.find('.tab.active');

        // projdu vsechny povinne prvky
        $activeTab.find('.required').each(function(){
            // pokud ma nejakou hodnotu
            if ( $(this).val() && $(this).val() !== '000' )
            {
                // odstranim class error jestli je
                $(this).closest('.entries').removeClass('error');            
            }
            else
            {
                // pridam class error
                $(this).closest('.entries').addClass('error'); 
            }
        });

        // pokud ma nejaky entries error
        if ( $activeTab.find('.entries.error').length )
        {
            // vypisu error hlasku a nedelam nic
            $('#stepError').show();
        }
        else
        {
            // skryju error hlasku
            $('#stepError').hide();

            // pridam dalsi polozce class active, odeberu ji disabled a aktualni ji odeberu
            $activeItem.next().addClass('active');
            $activeItem.next().removeClass('disabled');
            $activeItem.removeClass('active');

            // zobrazim dalsi tab a aktualni skryju
            $activeTab.next().addClass('active').show();
            $activeTab.removeClass('active');
        }

        if ( $(this).closest('p').hasClass('sendAjax') )
        {
            // kvuli IE ktery neumi serializovat primo dany fieldset to musim ojebat
            var fieldsetLeft  = $('#rosterBox .left .rosterEdit fieldset'),
                fieldsetRight = $('#rosterBox .right .rosterEdit fieldset');

            // pridam novy prazdny form
            $('body').append('<form id="fakeFieldset" style="visibility:hidde;"></form>');

            // naklonuji levy fieldset do prazdneho formulare a serializuji si ho
            $('#fakeFieldset').html($(fieldsetLeft).clone());
            var data_to_send_left = $('#fakeFieldset').serialize();

            // vyprazdnim formular
            $('#fakeFieldset').html('');

            // naklonuji pravy fieldset do prazdneho formulare a serializuji si ho
            $('#fakeFieldset').html($(fieldsetRight).clone());
            var data_to_send_right = $('#fakeFieldset').serialize();

            // odstranim form
            $('#fakeFieldset').remove();

            var data_to_send = data_to_send_left + '&' + data_to_send_right + '&getResultPlayers=1';

            /* stare reseni, neumi to IE
            var data_to_send_left  = $('#rosterBox .left .rosterEdit fieldset').serialize(),
                data_to_send_right = $('#rosterBox .right .rosterEdit fieldset').serialize(),
                data_to_send = data_to_send_left + '&' + data_to_send_right + '&getResultPlayers=1';*/
            

            $.ajax({
                url: '/ajax.php',
                // contentType: "application/json; charset=utf-8", nefunguje s postem
                dataType: 'json',
                type: 'post',
                data: data_to_send,
                success: function(response){

                    if( response.status == 'ok' )
                    {
                        $('#playersBox .left .playersEdit fieldset').replaceWith( response.responseLeft );
                        $('#playersBox .right .playersEdit fieldset').replaceWith( response.responseRight );
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }

        return false;

    });


    // kliknuti na tlacitka v navigaci
    $('#contentA').delegate('#stepsNavigation a', 'click', function(){

        var $this             = $(this),
            thisClass         = $this.attr('class'),
            $activeItem       = $('#stepsNavigation').find('.active'),
            $resultsContainer = $('#resultsContainer'),
            $activeTab        = $resultsContainer.find('.tab.active');

        // pokud je to prave aktivni nebo disabled tlacitko, nedelam nic
        if ( $this.closest('li').hasClass('active') || $this.closest('li').hasClass('disabled') )
        {
            // nedelam nic
        }
        else
        {
            // pridam vybrane polozce class active a aktualni ji odeberu
            $this.closest('li').addClass('active');
            $activeItem.removeClass('active');

            // pokud je to event-edit-result, vsem dalsim zalozkam dam disabled, aby uzivatel po editaci neceho musel jit dal pres tlacitka ulozit
            if ( path == 'event-edit-result' )
            {
                $('#stepsNavigation').find('.active').nextAll('li').addClass('disabled');
            }

            // zobrazim pozadovany tab a aktualni skryju
            $('#' + thisClass).addClass('active').show();
            $activeTab.removeClass('active');
        }

        return false;

    });

}


function editResult(){

    // zadani vysledku do inputu a jeho prepsani do formu
    $('#contentA').delegate('#resultsContainer input:text', 'keyup', function(){

        var thisData     = $(this).val(),
            thisDataName = $(this).attr('data-name');

        // pokud zadavam vysledek, zapisu ho i do headeru
        if ( $(this).closest('div').hasClass('resultEdit') )
        {
            $('#resultHeader .resultBox .' + thisDataName).text( thisData );
        }

    });
    
    /*
    // zadani vysledku do selectu a jeho prepsani do formu
    $('#resultsContainer').delegate('select', 'change', function(){

        var thisData     = $(this).val(),
            thisDataName = $(this).attr('data-name'),
            $resultForm  = $('#resultForm');

        $resultForm.find('input.' + thisDataName).val( thisData );

    });

    // zadani vysledku do checkboxu a jeho prepsani do formu
    $('#resultsContainer').delegate('input:checkbox', 'click', function(){

        var thisData     = $(this).val(),
            thisDataName = $(this).attr('data-name'),
            $resultForm  = $('#resultForm');

        
        if ( $(this).is(':checked') )
        {
            $resultForm.find('input.' + thisDataName).val( thisData );
        }
        else
        {
            $resultForm.find('input.' + thisDataName).val('');
        }

    });
    */

    // zatrhnuti checkboxu - pp nebo sam. najezdy
    $('#contentA').delegate('#resultBox .typeCheckbox .checkbox', 'click', function(){

        var $ovetimeEntries      = $(this).closest('form').find('.entries.overtime'),
            $ovetimeEntriesRight = $(this).closest('#resultBox').find('.right .entries.overtime'),
            $ovetimeBox          = $('#resultHeader .resultBox .overtimeBox'),
            $resultHeaderType    = $('#resultHeader .resultBox .type');

        // pokud je to prodlouzeni a je checked
        if ( $(this).is(':checked') && $(this).attr('data-name') == 'overtime' )
        {
            $ovetimeEntries.removeClass('forBlind');
            $ovetimeEntriesRight.removeClass('forBlind');
            $ovetimeBox.show();

            // je penalty checked
            if ( $('.checkbox.penalty').is(':checked') )
            {
                $resultHeaderType.text('pen');
            }
            else if ( $('.checkbox.shootout').is(':checked') )
            {
                $resultHeaderType.text('sn');
            }
            else
            {
                $resultHeaderType.text('pp');
            }
        }
        // pokud je to prodlouzeni a neni checked
        else if ( !$(this).is(':checked') && $(this).attr('data-name') == 'overtime' )
        {
            // je penalty checked
            if ( $('.checkbox.penalty').is(':checked') )
            {
                $ovetimeEntries.addClass('forBlind');
                $ovetimeEntriesRight.addClass('forBlind');
                $resultHeaderType.text('pen');
                $ovetimeBox.hide();
            }
            if ( $('.checkbox.shootout').is(':checked') )
            {
                $ovetimeEntries.addClass('forBlind');
                $ovetimeEntriesRight.addClass('forBlind');
                $resultHeaderType.text('sn');
                $ovetimeBox.hide();
            }
            else
            {
                $ovetimeEntries.addClass('forBlind');
                $ovetimeEntriesRight.addClass('forBlind');
                $resultHeaderType.text('');
                $ovetimeBox.hide();
            }
        }
        // pokud jsou to najezdy a jsou checked
        else if ( $(this).is(':checked') && $(this).attr('data-name') == 'penalty' )
        {
            $resultHeaderType.text('pen');
        }
        // pokud jsou to najezdy a nejsou checked
        else if ( !$(this).is(':checked') && $(this).attr('data-name') == 'penalty' )
        {
            // je overtime checked
            if ( $('.checkbox.overtime').is(':checked') )
            {
                $resultHeaderType.text('pp');
            }
            else
            {
                $resultHeaderType.text('');
            }
        }
        // pokud jsou to najezdy a jsou checked
        else if ( $(this).is(':checked') && $(this).attr('data-name') == 'shootout' )
        {
            $resultHeaderType.text('sn');
        }
        // pokud jsou to najezdy a nejsou checked
        else if ( !$(this).is(':checked') && $(this).attr('data-name') == 'shootout' )
        {
            // je overtime checked
            if ( $('.checkbox.overtime').is(':checked') )
            {
                $resultHeaderType.text('pp');
            }
            else
            {
                $resultHeaderType.text('');
            }
        }

    });

    // pridani dalsiho radku s hracem nebo s udalosti
    $('#contentA').delegate('#rosterBox .addPlayer .button, #summaryBox .addSummary .button', 'click', function(){

        var entries = $('#rosterBox .addPlayer .button').closest('p').prev('.entries').html(),
            name    = $('#rosterBox .addPlayer .button').closest('p').prev('.entries').find('input').attr('name'),
            match   = name.match(/result\[players\]\[(home|away)\]\[(.+)\]\[exists\]/),
            newid   = Math.random().toString(36).substr(2),
            re      = new RegExp("\\["+match[2]+"\\]","g");

        // replacenu predchozi id novym
        entries = entries.replace(re, "["+newid+"]");

        $( '<p class="entries cleaned added">' + entries + '</p>' ).insertBefore( $(this).closest('p') );

        // reset inputu
        $('.entries.added input.text').attr('value', ''); // musim pres attr pres val to sice odstranili ale ve zdrojaku zustalo

        // reset selectu number
        $('.entries.added select.number option:selected').attr('selected', false);
        $('.entries.added select.number option:first').attr('selected', 'selected');

        // reset selectu position
        $('.entries.added select.position option:selected').attr('selected', false);
        $('.entries.added select.position option:first').attr('selected', 'selected');

        // odstranim pomocnou classu added
        $('.entries.added').removeClass('added');

        return false;

    });


    // pridani priznaku kdyz uzivatel zmeni select position nebo number
    $('#contentA').delegate('#rosterBox select.position, #rosterBox select.number', 'change', function(){
        
        var $this = $(this);

        // ulozim si vybrany option
        $this.addClass('userChange');

    });


    /*** vyfiltrovani a kontrola zvolene polozky v selectu v zadavani a editaci vysledku, uprava selectu pri zacatku editace je ve funkci editResultNavi() ***/
    var beforeChangeOption   = '';

    $('#contentA #rosterBox select.player, #contentA #rosterBox select.number').live('focus click', function(){
        
        // ulozim si vybrany option
        beforeChangeOption = $(this).find('option:selected');

    }).live('change', function(){

        var $this         = $(this),
            $thisOption   = $this.find('option:selected'),
            thisOptionVal = $thisOption.val(),
            beforeChangeOptionVal      = beforeChangeOption.val(),
            beforeChangeOptionPosition = beforeChangeOption.attr('data-position'),
            beforeChangeOptionNumber   = beforeChangeOption.attr('data-number'),
            beforeChangeOptionText     = beforeChangeOption.text();

        // pridam prave zmenenemu selectu identifikator
        $this.addClass('changed');

        if ( $this.hasClass('player') )
        {
            $this.closest('.rosterEdit').find('select.player').each(function(){

                // pokud neni prave zmeneny
                if ( !$(this).hasClass('changed') )
                {
                    // pokud to neni defaultni option
                    if ( thisOptionVal !== '000' )
                    {
                        // odstranim prave vybrany option
                        $(this).find('option[value="' + thisOptionVal + '"]').remove();
                    }

                    // pokud neni, pridam prave uvolneny option
                    if ( $(this).find('option[value="' + beforeChangeOptionVal + '"]').length == 0 )
                    {
                        // pokud je to defaultni option s value="000"
                        if ( beforeChangeOptionVal == '000' )
                        {
                            $(this).append( '<option value="' + beforeChangeOptionVal + '" data-position="" data-number="">' + beforeChangeOptionText + '</option>' );
                        }
                        else
                        {
                            $(this).append( '<option value="' + beforeChangeOptionVal + '" data-position="' + beforeChangeOptionPosition + '" data-number="' + beforeChangeOptionNumber + '">' + beforeChangeOptionText + '</option>' );
                        }
                    }
                }
            });
        }
        else if ( $this.hasClass('number') )
        {
            $this.closest('.rosterEdit').find('select.number').each(function(){

                // pokud neni prave zmeneny
                if ( !$(this).hasClass('changed') )
                {
                    // pokud to neni defaultni option
                    if ( thisOptionVal !== '000' )
                    {
                        // odstranim prave vybrany option
                        $(this).find('option[value="' + thisOptionVal + '"]').remove();
                    }

                    // pokud neni, pridam prave uvolneny option
                    if ( $(this).find('option[value="' + beforeChangeOptionVal + '"]').length == 0 )
                    {
                        $(this).append( '<option value="' + beforeChangeOptionVal + '">' + beforeChangeOptionText + '</option>' );

                        var newBeforeChangeOptionVal = $(this).find('option:selected').val(),
                            selectList = $(this).find('option');

                        // odstranim predchozi attr selected
                        $(this).find('option').attr('selected', false);

                        // seradim podle value vzestupne
                        selectList.sort(function(a,b){
                            a = a.value;
                            b = b.value;

                            return a-b;
                        });

                        $(this).html(selectList);

                        // pridam predtim vybranemu selectu attr selected
                        $(this).find('option[value="' + newBeforeChangeOptionVal + '"]').attr('selected', 'selected');

                    }
                }
            });
        }

        // automaticke doplneni pozice a cisla dresu
        if ( $this.hasClass('player') )
        {
            var position = $thisOption.attr('data-position'),
                number   = $thisOption.attr('data-number'),
                $selectPosition = $this.closest('.entries').find('select.position'),
                $selectNumber   = $this.closest('.entries').find('select.number');

            // pokud uz neni position uzivatelem zmeneny
            if ( !$selectPosition.hasClass('userChange') )
            {
                $selectPosition.find('option[value="' + position + '"]').attr('selected', 'selected');
            }
            // pokud uz neni number uzivatelem zmeneny
            if ( !$selectNumber.hasClass('userChange') )
            {
                var beforeChangeOptionVal = $selectNumber.val();

                $selectNumber.find('option[value="' + number + '"]').attr('selected', 'selected');
                $selectNumber.addClass('changed');

                // odstranim cislo dresu z ostatnich selectu
                $this.closest('.rosterEdit').find('select.number').each(function(){

                    // pokud neni prave zmeneny
                    if ( !$(this).hasClass('changed') )
                    {
                        // pokud to neni defaultni option
                        if ( number !== '000' )
                        {
                            // odstranim prave vybrany option
                            $(this).find('option[value="' + number + '"]').remove();
                        }

                        // pokud neni, pridam prave uvolneny option
                        if ( $(this).find('option[value="' + beforeChangeOptionVal + '"]').length == 0 )
                        {
                            $(this).append( '<option value="' + beforeChangeOptionVal + '">' + beforeChangeOptionVal + '</option>' );

                            var newBeforeChangeOptionVal = $(this).find('option:selected').val(),
                                selectList = $(this).find('option');

                            // odstranim predchozi attr selected
                            $(this).find('option').attr('selected', false);

                            // seradim podle value vzestupne
                            selectList.sort(function(a,b){
                                a = a.value;
                                b = b.value;

                                return a-b;
                            });

                            $(this).html(selectList);

                            // pridam predtim vybranemu selectu attr selected
                            $(this).find('option[value="' + newBeforeChangeOptionVal + '"]').attr('selected', 'selected');

                        }
                    }
                });
            }
        }

        // odtranim pomocnou class
        $this.closest('.rosterEdit').find('select.player').removeClass('changed');
        $this.closest('.rosterEdit').find('select.number').removeClass('changed');

    });


    /***** odstraneni stareho selected a vlozeni noveho, kvuli spolupraci s clonovanim fieldsetu, ktere neslo v IE viz funkce kousek vys *****/
    $('#contentA #rosterBox select').live('change', function(){
        var val = $(this).val();

        $(this).find('option').attr('selected', false);
        $(this).find('option[value=' + val + ']').attr('selected', 'selected');
    });


    /***** skryti nevybranych, nehrajicich hracu v statistikach *****/
    $('#contentA').delegate('#rosterBox .nextLink .button', 'click', function(){

        // zobrazim vsechny hrace, pokud uz nekteri byli sryti
        $('#playersBox .left .playersEdit .entries.playerBox, #playersBox .right .playersEdit .entries.playerBox').show();

        // pole domacich vybranych hracu
        var selectedHomePlayers = $('#rosterBox .left .rosterEdit select.player option:selected').map(function () {
          return this.text;
        }).get();

        // pole hostujicich vybranych hracu
        var selectedAwayPlayers = $('#rosterBox .right .rosterEdit select.player option:selected').map(function () {
          return this.text;
        }).get();

        // pole vsech domacich hracu ve statistikach
        var homePlayers = [];
        $('#playersBox .left .playersEdit .entries.playerBox').each(function() {
          homePlayers.push($(this).attr('data-name'));
        });

        // pole vsech hostujicich hracu ve statistikach
        var awayPlayers = [];
        $('#playersBox .right .playersEdit .entries.playerBox').each(function() {
          awayPlayers.push($(this).attr('data-name'));
        });

        // nevybrani domaci hraci
        var noSelectedHomePlayers = $.grep(homePlayers, function(element) {
            return $.inArray(element, selectedHomePlayers) == -1;
        });

        // nevybrani hostujici hraci
        var noSelectedAwayPlayers = $.grep(awayPlayers, function(element) {
            return $.inArray(element, selectedAwayPlayers) == -1;
        });

        //console.log(noSelectedHomePlayers, newSelectedHomePlayers);

        // skryju nevybrane domaci hrace
        $.each( noSelectedHomePlayers, function( i, val ) {
            $('#playersBox .left .playersEdit .entries.playerBox[data-name="' + val + '"]').hide();
        });

        // skryju nevybrane hostujici hrace
        $.each( noSelectedAwayPlayers, function( i, val ) {
            $('#playersBox .right .playersEdit .entries.playerBox[data-name="' + val + '"]').hide();
        });


        /*** zmena cisla dresu ***/
        $('#rosterBox .left .rosterEdit .entries').each(function(){

            var playerName   = $(this).find('select.player option:selected').text(),
                playerNumber = $(this).find('select.number option:selected').text();

            $('#playersBox .left .playersEdit .entries.playerBox[data-name="' + playerName + '"] label .number').text('#' + playerNumber);

        });

        $('#rosterBox .right .rosterEdit .entries').each(function(){

            var playerName   = $(this).find('select.player option:selected').text(),
                playerNumber = $(this).find('select.number option:selected').text();

            $('#playersBox .right .playersEdit .entries.playerBox[data-name="' + playerName + '"] label .number').text('#' + playerNumber);

        }); 

    });

}



/************************************************************
    ATTENDANCE
************************************************************/

// popUp zadani dochazky
function addAttendance(){

    // kliknuti na button policko se statusem
    $('#contentA .attendance .editable').live('click', function(){

        var thisID = $(this).attr('data-id');

        $(this).addClass('active');
        $('#eventID').val( thisID );

    });


    // kliknuti na status v popUpu
    $('#popUpFormAddAttendance .attStatus a').live('click', function(){

        var thisClass = $(this).closest('li').attr('class');

        // vsem odeberu class active
        $(this).closest('fieldset').find('a').removeClass('active');

        // vybranemu pridam class active
        $(this).addClass('active');

        // kliknu na spravny radio button
        $('#popUpFormAddAttendance span.forBlind input.' + thisClass).click();

        return false;

    });


    // definice popupu a jeho funkci po otevreni a zavreni
    $('#contentA .attendance .editable').fancybox({   
        width       : 400,
        height      : 'auto',
        fitToView   : false,
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        title       : false,
        padding     : 10,
        live        : true,
        href        : '#addAttendance',
        afterShow   : function(){

            var close         = $('.fancybox-close, .popUpForm .submitButton'),
                $activeStatus = $('#contentA .attendance .editable.active'),
                activeComment = $('#contentA .attendance .editable.active .tooltip').text(),
                form          = $('#popUpFormAddAttendance'),
                $popUpStatus  = $('#popUpFormAddAttendance .attStatus'),
                $popUpText    = $('#popUpFormAddAttendance #attendanceText');

            // vyprazdnim formular
            resetAttendanceForm();

            // odstranim pomocnou classu .save
            form.removeClass('save');

            // pokud je to uz zeditovany status
            if ( $activeStatus.hasClass('edited') )
            {
                if ( $activeStatus.hasClass('yes') ) { $popUpStatus.find('li.yes a').click(); }
                if ( $activeStatus.hasClass('no') ) { $popUpStatus.find('li.no a').click(); }
                if ( $activeStatus.hasClass('na') ) { $popUpStatus.find('li.na a').click(); }
                if ( $activeStatus.hasClass('blackDot') ) { $popUpStatus.find('li.blackDot a').click(); }
                if ( $activeStatus.hasClass('yesComment') ) { $popUpStatus.find('li.yes a').click(); $popUpText.val( activeComment ); }
                if ( $activeStatus.hasClass('noComment') ) { $popUpStatus.find('li.no a').click(); $popUpText.val( activeComment ); }
                if ( $activeStatus.hasClass('naComment') ) { $popUpStatus.find('li.na a').click(); $popUpText.val( activeComment ); }
                if ( $activeStatus.hasClass('blackDotComment') ) { $popUpStatus.find('li.blackDot a').click(); $popUpText.val( activeComment ); }
            }

            close.live('click', function(){

                // pridani pomocne classy .save pro obsluhu po zavreni popUpu odeslanim formu
                if ( $(this).hasClass('submitButton') )
                {
                    form.addClass('save');
                }
                else
                {
                    $.fancybox.close();
                }

            });
        },
        afterClose  : function(){

            // pokud ma form pomocnou classu .save
            if ( $('#popUpFormAddAttendance').hasClass('save') )
            {
                var $activeStatus   = $('#contentA .attendance .editable.active'),
                    $newStatusClass = $('#popUpFormAddAttendance span.forBlind input:checked').attr('class'),
                    $newStatusText  = $('#popUpFormAddAttendance #attendanceText').val(),
                    isStatusText    = '';

                // vymazu predchozi status
                $activeStatus.html('');

                // pokud je napsany nejaky text ke statusu, vytvorim si do isStatusText classu a vlozim novy status do tooltipu
                if ( $newStatusText.length )
                {
                    isStatusText = 'Comment';

                    // vlozim novy status
                    $('<span class="help"><!-- --></span><span class="tooltip">' + $newStatusText + '</span>').appendTo( $activeStatus );

                    // znovu nactu tooltip
                    $('.help').removeData('tooltip');
                    $('.help').tooltip({ relative: true, offset: [-10, 0] });
                }

                // odstranim predchozi classu a pridam novou
                $activeStatus.removeClass('noInfo na yes no blackDot naComment yesComment noComment blackDotComment');
                $activeStatus.addClass( $newStatusClass + isStatusText );
                $activeStatus.addClass('edited');
            }

            // odstranim classu active
            $('#contentA .attendance .attStatus').removeClass('active');

        }
    });

    
    // kliknuti na tlacitko ulozit dochazku
    $('#popUpFormAddAttendance').live('submit', function(){

        var $form      = $(this),
            $attStatus = $form.find('.attStatus');

        var valid = true;

        $form.find('.error').removeClass('error');
        $form.find('.errorMsg').hide();


        if ( !$('#popUpFormAddAttendance span.forBlind input').is(':checked') )
        {
            $attStatus.closest('.entries').addClass('error');
            $attStatus.closest('.entries').find('.errorMsg').fadeIn('fast');
            valid = false;
        }

        // vse je validni
        if ( valid == true )
        {
            // odeslu formular
            sendAttendance($form);
        }

        return false;
    });

}

// reset formu
function resetAttendanceForm(){

    $('#popUpFormAddAttendance .attStatus a').removeClass('active');
    $('#popUpFormAddAttendance span.forBlind input:checked').removeAttr('checked');
    $('#popUpFormAddAttendance #attendanceText').val('');

}

// odeslani formularu
function sendAttendance(form){

    var str = $(form).serialize();
    
    $.ajax({
        url: '/ajax.php',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        type: 'get',
        data: str,
        success: function(response){

            if ( response.status == 'ok' )
            {
                $.fancybox.close();
            }
            else
            {
                alert(response.message);
            }

        }
    });
}


// donacteni vice udalosti u dochazky
function moreEventsAttendance()
{
    $('#contentA').delegate('.prevAttendances, .nextAttendances', 'click', function(){

        var $this  = $(this),
            thisClass = $this.attr('class');


        // rozlisim next a prev
        if( thisClass == 'prevAttendances' )
        {
            var start = $this.closest('th').next('th').attr('data-id'),
                type  = 'prev';
        }
        if( thisClass == 'nextAttendances' )
        {
            var start = $this.closest('th').prev('th').attr('data-id'),
                type  = 'next';
        }

        // vytvorim data k odeslani
        var data_to_send = 'viewMoreEventsAttendances=1&type=' + type + '&start=' + start;


        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if( response.status == 'ok' )
                {
                    $('#attendBox').html(response.response);

                    // znovu nactu tooltip
                    $('.help').removeData('tooltip');
                    $('.help').tooltip({ relative: true, offset: [-10, 0] });
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;
    });
}


// reset formu
function quickAttendance(){

    $('#nextEvents .answer a').click(function(){

        var $this  = $(this),
            status = $this.attr('class'),
            attID  = $this.attr('data-id');

        var data_to_send = 'quickAttendance=1&attendance%5Bevent-id%5D=' + attID + '&attendance%5Bstatus%5D=' + status;

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    $this.closest('.answer').text(response.response);
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;

    });

}


/************************************************************
    USER PROFILE
************************************************************/
function latestActivity(){

    $('#contentA.profile').delegate('#activite .viewMore a', 'click', function(){

        var $this  = $(this),
            dataUser = $this.attr('data-user'),
            dataLast = $this.attr('data-last');

        var data_to_send = 'viewMoreUserActivities=1&user=' + dataUser + '&last=' + dataLast;

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // pridam dalsi pred tlacitko nacitani
                    $(response.response).insertBefore( $this.closest('.viewMore') );

                    // odstranim tlacitko
                    $this.remove();
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;

    });

}


/************************************************************
    TEAM PROFILE
************************************************************/
// pridani / editace adminu
function adminsEdit(){

    $('#popUpEditAdmins').delegate('.entries a', 'click', function(){

        var $this        = $(this),
            label        = $this.closest('.entries').find('.adminName'),
            adminName    = $this.closest('.entries').find('select').val(),
            adminID      = $this.closest('.entries').attr('data-id'),
            deleteControls = $this.closest('.entries').find('.controls.deleteBox'),
            saveControls = $this.closest('.entries').find('.controls.save');

        if ( $this.hasClass('delete') )
        {
            var data_to_send = 'delete_admin=1&admin[id]=' + adminID;

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        $this.closest('.entries').remove();

                        // vymenim optiony v selectu
                        $('#teamOwner').html( response.select );
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }
        else if ( $this.hasClass('save') )
        {

            var data_to_send = 'add_admin=1&admin[id]=' + adminName;

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        // odstranim vsechny entries
                        $this.closest('#popUpEditAdmins').find('.entries').remove();

                        // pridam vsechny entries
                        $('#popUpEditAdmins').prepend(response.response);

                        // vymenim optiony v selectu
                        $('#teamOwner').html( response.select );
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }

        return false;

    });

    // pridani noveho radku
    $('#popUpEditAdmins .addAdmin .button.add').click(function(){

        var playersOptions = $('#adminsEdit .playersOptions select').html();

        $( '<p class="entries cleaned">' +
                '<select>' + playersOptions + '</select>' +
                '<span class="save controls">' +
                    '<a href="#" class="save" title="Uložit admina"></a>' +
                '</span>' +
            '</p>' ).insertBefore( $(this).closest('p') );

        return false;

    });

}


// pridani / editace sezony
function seasonEdit(){

    $('#popUpEditSeason .entries a').live('click', function(){

        var $this        = $(this),
            label        = $this.closest('.entries').find('.seasonName'),
            labelText    = $this.closest('.entries').find('.seasonName').text(),
            input        = $this.closest('.entries').find('input.text'),
            seasonName   = $this.closest('.entries').find('input.text').val(),
            seasonID     = $this.closest('.entries').attr('data-id'),
            editControls = $this.closest('.entries').find('.controls.editDelete'),
            saveControls = $this.closest('.entries').find('.controls.saveStorno');

        if ( $this.hasClass('edit') )
        {
            label.addClass('forBlind');
            editControls.addClass('forBlind');
            input.removeClass('forBlind');
            saveControls.removeClass('forBlind');
            input.val( labelText );
        }
        else if ( $this.hasClass('storno') )
        {
            input.addClass('forBlind');
            saveControls.addClass('forBlind');
            label.removeClass('forBlind');
            editControls.removeClass('forBlind');
        }
        else if ( $this.hasClass('delete') )
        {
            var data_to_send = 'delete_season=1&season[id]=' + seasonID;

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        $this.closest('.entries').remove();

                        // pokud jeste jsou nejake sezony
                        if ( response.select.length )
                        {
                            // vymenim optiony v selectu
                            $('#activeSeason').html( response.select );
                        }
                        else
                        {
                            // odstranim select
                            $('#activeSeason').remove();

                            // vymenim text v buttonu
                            $('#addSeasonButton').text(response.button);
                        }
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }
        else if ( $this.hasClass('save') )
        {

            var data_to_send = 'manage_season=1&season[name]=' + seasonName + '&season[id]=' + seasonID;

            // pokud je to nova sezona
            if ( $this.closest('.entries').hasClass('new') )
            {
                data_to_send = 'manage_season=1&season[name]=' + seasonName;
            }

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok-edit' )
                    {
                        label.text(seasonName);
                        input.addClass('forBlind');
                        saveControls.addClass('forBlind');
                        label.removeClass('forBlind');
                        editControls.removeClass('forBlind');

                        // vymenim optiony v selectu
                        $('#activeSeason').html( response.select );
                    }
                    else if ( response.status == 'ok-new' )
                    {
                        // odstranim vsechny entries
                        $this.closest('#popUpEditSeason').find('.entries').remove();

                        // pridam vsechny entries
                        $('#popUpEditSeason').prepend(response.response);

                        // pokud neni vytvoreny select, pridavam prvni sezonu
                        if ( !$('#activeSeason').length )
                        {
                            $( '<select id="activeSeason" name="activeSeason">' + response.select + '</select>' ).insertBefore( $('#addSeasonButton') );

                            // vymenim text v buttonu
                            $('#addSeasonButton').text(response.button);
                        }
                        else
                        {
                            // vymenim optiony v selectu
                            $('#activeSeason').html( response.select );
                        }
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }

        return false;

    });

    // pridani noveho radku
    $('#popUpEditSeason .addSeason .button.add').click(function(){

        $( '<p class="entries new cleaned">' +
                '<input class="text" type="text" name="" />' +
                '<span class="saveStorno controls">' +
                    '<a href="#" class="save" title="Uložit změnu"></a>' +
                '</span>' +
            '</p>' ).insertBefore( $(this).closest('p') );

        return false;

    });

}


// pridani / editace adminu
function mergeStats(){

    $('#mergeStats').click(function(){

        var $this          = $(this),
            selectFrom     = $this.closest('.entries').find('select.from option:selected'),
            selectFromText = selectFrom.text(),
            selectFromVal  = $this.closest('.entries').find('select.from').val(),
            selectToText   = $this.closest('.entries').find('select.to option:selected').text(),
            selectToVal    = $this.closest('.entries').find('select.to').val();

        var data_to_send = 'mergeStats=1&fromPlayer=' + selectFromVal + '&toPlayer=' + selectToVal;

        // confirm na smazani zpravy       
        $.confirm({
            'title'     : 'Potvrdit spárování účtů',
            'message'   : 'Opravdu si přejete přenést statistiky z fiktivního uživatelského účtu "' + selectFromText + '" na uživatelský účet "' + selectToText + '"? Fiktivní uživatelský účet "' + selectFromText + '" bude po přenesení nenávratně odstraněn.',
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        $.ajax({
                            url: '/ajax.php',
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            type: 'get',
                            data: data_to_send,
                            success: function(response){

                                if ( response.status == 'ok' )
                                {
                                    // odstranim fiktivni ucet ze selectu
                                    selectFrom.remove();

                                    // pridam done hlasku
                                    $('#contentA').prepend( response.response );

                                    // zascrolluju nahoru, aby bylo videt hlasku
                                    $('html, body').animate({
                                        scrollTop: $('#contentA').offset().top
                                    }, 1);

                                    // pokud to byl posledni fiktivni ucet v tymu, odstranim celou moznost spravy fiktivnich uctu
                                    if ( $this.closest('.entries').find('select.from option').length == 0 )
                                    {
                                        $this.closest('.entries').prev('.title-border').remove();
                                        $this.closest('.entries').remove();
                                    }
                                }
                                else
                                {
                                    alert(response.message);
                                }

                            }
                        });
                    
                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
    
        return false;

    });

}


// navaznosti modulu
function mudulesManage(){

    $('#modules select[data-name="events"]').change(function(){

        var selectStats      = $('#modules select[data-name="stats"]'),
            selectAttendance = $('#modules select[data-name="attendance"]');

        if ( $(this).val() === 'no' )
        {
            selectStats.attr('disabled', 'disabled');
            selectAttendance.attr('disabled', 'disabled');

            // nastavim vychozi hodnotu na neaktivní
            var optionsStats = selectStats.find('option[value="no"]');
            var optionsAttendance = selectAttendance.find('option[value="no"]');

            // odstranim predchozi selected
            selectStats.find('option:selected').attr('selected', false);
            selectAttendance.find('option:selected').attr('selected', false);

            // pridam vychozimu
            optionsStats.attr('selected', 'selected');
            optionsAttendance.attr('selected', 'selected');
        }
        else
        {
            selectStats.attr('disabled', false);
            selectAttendance.attr('disabled', false);
        }

    });

}


/************************************************************
    POZVANI DO TYMU
************************************************************/
function userInvite(){

    $('#contentA').delegate('.result .button.invite, .result .button.leave, .rightPanel .invite a, .rightPanel .leave a', 'click', function(){

        var $this     = $(this),
            dataID    = $this.attr('data-id'),
            dataTeam  = $this.attr('data-team'),
            search    = '';

        // pokud je to vyhledavani
        if ( $('body').attr('id') == 'search' )
        {
            search = '&search=1';
        }

        // je to pozvani hrace
        if ( $this.hasClass('invite') || $this.closest('.item').hasClass('invite') )
        {
            var data_to_send = 'add_player_quick=' + dataID + '&team_id=' + dataTeam + search;
        }

        // je to zruseni pozvani hrace
        if ( $this.hasClass('leave') || $this.closest('.item').hasClass('leave') )
        {
            var data_to_send = 'remove_player_quick=' + dataID + '&team_id=' + dataTeam + search;
        }

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // pokud to je vyhledavani
                    if ( $('body').attr('id') == 'search' )
                    {
                        $this.replaceWith( response.response );
                    }
                    else
                    {
                        $this.closest('.item').replaceWith( response.response );
                    }
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;

    });

}



/************************************************************
    OPUSTENI TYMU
************************************************************/
function leaveTeam(){

    $('#teamOptionLinks').delegate('.leaveTeam', 'click', function(){

        var $this = $(this),
            thisHref = $this.attr('href'),
            teamName = $('#teamInfo .head .name').text();

        // confirm na opusteni tymu       
        $.confirm({
            'title'     : 'Potvrdit opuštění týmu',
            'message'   : 'Opravdu si přejete opustit tým ' + teamName + '?',
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        window.location.href=thisHref;

                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });
        // konec

        return false;

    });

}


/************************************************************
    DOPORUCIT OSTATNIM
************************************************************/
function recommend(){

    $('#recommend form').submit(function(){

        var email = $(this).find('input');

        var valid = true;

        $(this).find('.msg.new, msg.done').remove();
        $(this).find('.msg').hide();

        if ( !email.val() || !email.val().match( /^([\w\+-\.]+@([\w-]+\.)+[\w-]{2,4})?$/ ) )
        {
            email.closest('fieldset').find('.msg.error').fadeIn('fast');
            valid = false;
        }

        // odesleme formular
        if ( valid == true )
        {
            ga('send', 'event', 'Recommend - left panel', 'submit');

            var data_to_send = $(this).serialize();

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        $(response.response).prependTo('#recommend form fieldset').show();
                        email.val('');
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }

        return false;

    });

}


/************************************************************
    GALLERY
************************************************************/
function createGallery(){
    // ulozim si puvodni obsah popupu
    var addPhotosHtml = $('#addPhotos').html();

    // po kliknuti na button vytvorit galerii vzdy nahraju puvodni obsah
    $('#addPhotosButton').click(function(){
        $('#addPhotos').html(addPhotosHtml);

        // inicializuji plugin 
        $('#uploadPhotos').dropzone({
            init: function() {
                this.on("success", function(file, finished) {
                    if (finished.status == 'ok')
                    {
                        // pokud je, odstranim warning hlasku
                        $('#contentA .contentAbox .msg.warning').remove();

                        if ( $('#galleryList').length )
                        {
                            // pridam novou galerii do listu
                            $('#galleryList').prepend(finished.response);

                            // show done button
                            $('#addPhotos .link').show();
                        }
                        else
                        {
                            $('<ul id="galleryList" class="cleaned"></ul>').insertAfter( $('#contentA .contentAbox .headline') );
                            // pridam novou galerii do listu
                            $('#galleryList').prepend(finished.response);

                            // show done button
                            $('#addPhotos .link').show();
                        }

                        // inicializuji funkce
                        renameGalleryPhoto();
                        deleteGalleryPhoto();
                        sortableGalleryList();
                    }
                });
            }
        });
    });


    // kliknuti na submit tlacitko - ulozit a pokracovat
    $('#createGallery').live('submit', function(){

        var $galleryName   = $('#createGallery .galleryName'),
            galleryNameVal = $galleryName.val(),
            $galleryID     = $('#uploadPhotos .galleryID'),
            $galleryHash   = $('#uploadPhotos .galleryHash'),
            loading        = '<p id="loading"><!-- --></p>';

        // pridam loading
        $('#createGallery').append( loading );

        var data_to_send = 'add_gallery=' + galleryNameVal;

        $.ajax({
            url: '/gallery_ajax.php',
            //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
            dataType: 'json',
            type: 'post',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    $('#loading').remove();

                    $galleryName.attr('disabled', 'disabled');

                    $galleryID.val(response.id);
                    $galleryHash.val(response.hash);

                    // show uplodPhotos / dropzone
                    $('#uploadPhotos').show();
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        $(this).find('.submit').closest('fieldset').remove();

        return false;
    });


    // kliknuti na tlacitko hotovo
    $('#addPhotos .link a').live('click', function(){

        // zavru popup
        $('#addPhotos').closest('.fancybox-wrap ').find('.fancybox-close').click();

        return false;
    });

}


function renameGalleryPhoto(){

    $('#galleryList, #galleryDetail').delegate('.overlay .edit', 'click', function(){

        var itemID     = $(this).closest('.item').attr('data-id'),
            renameItem = $('#renameItem');

        // pridam pomocnou class
        $(this).closest('.item').addClass('rename');

        if ( $('#galleryList').length )
        {
            var name = $(this).closest('.item').find('.title a').text();
        }
        else if ( $('#galleryDetail').length )
        {
            var name = $(this).closest('.item').attr('data-name');
        }

        renameItem.find('.renameID').val(name);
        renameItem.find('.renameID').attr('data-id', itemID);

    });
}


function renameGalleryPhotoPopup(){

    $('#renameItem form').submit(function(){

        var itemID = $(this).find('.renameID').attr('data-id'),
            name   = $(this).find('.renameID').val(),
            type   = '';

        if ( $('#galleryList').length )
        {
            var data_to_send = 'name_gallery=1&gallery=' + itemID + '&name=' + name;
        }
        else if ( $('#galleryDetail').length )
        {
            var galleryID = $('#galleryDetail').attr('data-id');
            var data_to_send = 'name_photo=1&gallery=' + galleryID + '&photo=' + itemID + '&name=' + name;
        }

        $.ajax({
            url: '/gallery_ajax.php',
            //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
            dataType: 'json',
            type: 'post',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // pokud je vse ok zmenim nazev
                    if ( $('#galleryList').length )
                    {
                        $('#galleryList .item.rename .title a').text(name);
                        $('#galleryList .item.rename .picture').attr('title', response.response);

                        // odstranim pomocne classy
                        $('#galleryList .item').removeClass('rename');
                    }
                    else if ( $('#galleryDetail').length )
                    {
                        $('#galleryDetail .item.rename').attr('data-name', name);
                        $('#galleryDetail .item.rename .picture').attr('title', response.response);

                        // odstranim pomocne classy
                        $('#galleryDetail .item').removeClass('rename');
                    }

                    // a zavru popup
                    $('#renameItem').closest('.fancybox-wrap ').find('.fancybox-close').click();

                }
                else
                {
                    alert(response.message);

                    // odstranim pomocne classy
                    $('#galleryList .item').removeClass('rename');
                    $('#galleryDetail .item').removeClass('rename');
                }

            }
        });

        return false;
    });
}


function mainGalleryPhoto(){

    $('#galleryDetail .adminButtons .star').click(function(){

        var itemID = $(this).closest('.item').attr('data-id'),
            galleryID = $(this).closest('#galleryDetail').attr('data-id');

        $(this).closest('#galleryDetail').find('.item .star').removeClass('active');
        $(this).addClass('active');

        var data_to_send = 'main_photo=1&gallery_id=' + galleryID + '&photo_id=' + itemID;

        $.ajax({
            url: '/gallery_ajax.php',
            //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
            dataType: 'json',
            type: 'post',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // nedelam nic
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;
    });
}


function deleteGalleryPhoto(){

    $('#galleryList, #galleryDetail').delegate('.overlay .delete', 'click', function(){

        var $this  = $(this),
            itemID = $this.closest('.item').attr('data-id'),
            titleText = '',
            messageText = '';

        // je to vypis galerii
        if ( $('#galleryList').length )
        {
            var name = $this.closest('.item').find('.title a').text();

            var data_to_send = 'delete_gallery=' + itemID;

            titleText = 'Potvrdit smazání galerie'; 
            messageText  = 'Opravdu si přejete smazat galerii "' + name + '"?';
        }
        // je to vypis fotografii
        else if ( $('#galleryDetail').length )
        {
            //var name = $this.closest('.item').attr('data-name');
            var galleryID = $('#galleryDetail').attr('data-id');

            var data_to_send = 'gallery_id=' + galleryID + '&delete_photo=' + itemID;

            titleText = 'Potvrdit smazání fotografie';
            messageText  = 'Opravdu si přejete smazat tuto fotografii?';

        }

        // confirm na smazani galerie / fotky       
        $.confirm({
            'title'     : titleText,
            'message'   : messageText,
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        $.ajax({
                            url: '/gallery_ajax.php',
                            //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
                            dataType: 'json',
                            type: 'post',
                            data: data_to_send,
                            success: function(response){

                                if ( response.status == 'ok' )
                                {
                                    // odstranim smazanou polozku
                                    $this.closest('.item').remove();
                                }
                                else
                                {
                                    alert(response.message);
                                }

                            }
                        });
                    
                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });

        return false;
    });
}


function sortableGalleryList(){

    $('#galleryList').sortable({
        items: '> .item',
        update: function() {
            var sortedIDs = $(this).sortable('toArray', { attribute:'data-id' });
            var data_to_send = 'change_gallery_order=' + sortedIDs;

            $.ajax({
                url: '/gallery_ajax.php',
                //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
                dataType: 'json',
                type: 'post',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        // nedelam nic
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }
    });

}


function sortableGalleryDetail(){

    $('#galleryDetail').sortable({
        items: '> .item',
        update: function() {
            var sortedIDs = $(this).sortable('toArray', { attribute:'data-id' });
            var galleryID = $('#galleryDetail').attr('data-id');
            var data_to_send = 'gallery_id=' + galleryID + '&change_items_order=' + sortedIDs;

            $.ajax({
                url: '/gallery_ajax.php',
                //contentType: "application/json; charset=utf-8", nefunguje s postem, funguje jen s getem
                dataType: 'json',
                type: 'post',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        // nedelam nic
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }
    });

}


function openPhotoFromHash(){

    if ( window.location.hash )
    {
        var hash = window.location.hash,
            hash = window.location.hash.substring(1);

        console.log(hash);

        $('#galleryDetail .item[data-id="' + hash + '"] .picture').click();

    }

}


/************************************************************
    CASHBOX
************************************************************/
function cashInfo(){

    $('.table.cashbox .showInfo').click(function(){

        var id = $(this).attr('data-id')
            tbody = $(this).closest('tbody');

        // zavru pedchozi otevreny detail
        tbody.find('.info').addClass('forBlind');
        tbody.find('tr.active').removeClass('active');

        // otevru pozadovany
        tbody.find('.info[data-id=' + id + ']').removeClass('forBlind');
        $(this).closest('tr').addClass('active');

        return false;
    });

}


// pridani / editace poplatku
function chargesEdit(){

    $('#chargesEdit #season select').live('change', function(){
        var selectedSeason = $(this).val();

        var data_to_send = 'change_charges_season=1&season=' + selectedSeason;

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // odstranim vsechny entries
                    $('#popUpEditCharge').find('.entries').remove();

                    // pridam vsechny entries
                    $('#popUpEditCharge').prepend(response.response);
                }
                else
                {
                    alert(response.message);
                }

            }
        });
    });


    // button editovat fixni poplatky
    $('#chargesEdit .entries a').live('click', function(){

        var $this          = $(this),
            labelName      = $this.closest('.entries').find('.chargeName'),
            labelNameText  = $this.closest('.entries').find('.chargeName').text(),
            labelPrice     = $this.closest('.entries').find('.chargePrice'),
            labelPriceText = $this.closest('.entries').find('.chargePrice').text(),
            inputName      = $this.closest('.entries').find('input.name'),
            chargeName     = $this.closest('.entries').find('input.name').val(),
            inputPrice     = $this.closest('.entries').find('input.price'),
            chargePrice    = $this.closest('.entries').find('input.price').val(),
            chargeID       = $this.closest('.entries').attr('data-id'),
            editControls   = $this.closest('.entries').find('.controls.editDelete'),
            saveControls   = $this.closest('.entries').find('.controls.saveStorno')
            season         = $this.closest('#chargesEdit').find('#season select').val();

        if ( $this.hasClass('edit') )
        {
            labelName.addClass('forBlind');
            labelPrice.addClass('forBlind');
            editControls.addClass('forBlind');
            inputName.removeClass('forBlind');
            inputPrice.removeClass('forBlind');
            saveControls.removeClass('forBlind');
            inputName.val( labelNameText );
            inputPrice.val( labelPriceText );
        }
        else if ( $this.hasClass('storno') )
        {
            inputName.addClass('forBlind');
            inputPrice.addClass('forBlind');
            saveControls.addClass('forBlind');
            labelName.removeClass('forBlind');
            labelPrice.removeClass('forBlind');
            editControls.removeClass('forBlind');
        }
        else if ( $this.hasClass('delete') )
        {
            var data_to_send = 'delete_charge=1&charge[id]=' + chargeID + '&season=' + season;

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        $this.closest('.entries').remove();
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }
        else if ( $this.hasClass('save') )
        {

            var data_to_send = 'manage_charge=1&charge[name]=' + chargeName + '&charge[price]=' + chargePrice + '&charge[id]=' + chargeID + '&season=' + season;

            // pokud je to novy poplatek
            if ( $this.closest('.entries').hasClass('new') )
            {
                data_to_send = 'manage_charge=1&charge[name]=' + chargeName + '&charge[price]=' + chargePrice + '&season=' + season;
            }

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok-edit' )
                    {
                        labelName.text(chargeName);
                        labelPrice.text(chargePrice);
                        inputName.addClass('forBlind');
                        inputPrice.addClass('forBlind');
                        saveControls.addClass('forBlind');
                        labelName.removeClass('forBlind');
                        labelPrice.removeClass('forBlind');
                        editControls.removeClass('forBlind');
                    }
                    else if ( response.status == 'ok-new' )
                    {
                        // odstranim vsechny entries
                        $this.closest('#popUpEditCharge').find('.entries').remove();

                        // pridam vsechny entries
                        $('#popUpEditCharge').prepend(response.response);
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }

        return false;

    });

    // pridani noveho radku
    $('#popUpEditCharge .addCharge .button.add').click(function(){

        $( '<p class="entries new cleaned">' +
                '<input class="text name" type="text" name="" placeholder="název poplatku" />' +
                '<input class="text price" type="text" name="" placeholder="částka" />' +
                '<span class="saveStorno controls">' +
                    '<a href="#" class="save" title="Uložit změnu"></a>' +
                '</span>' +
            '</p>' ).insertBefore( $(this).closest('p') );

        return false;

    });


    // editovat poplatky hrace
    $('#playerChargesEdit .entries a').live('click', function(){

        var $this          = $(this),
            labelName      = $this.closest('.entries').find('.chargeName'),
            labelNameText  = $this.closest('.entries').find('.chargeName').text(),
            labelPrice     = $this.closest('.entries').find('.chargePrice'),
            labelPriceText = $this.closest('.entries').find('.chargePrice').text(),
            labelEvent     = $this.closest('.entries').find('.chargeEvent'),
            labelEventText = $this.closest('.entries').find('.chargeEvent').text(),
            inputName      = $this.closest('.entries').find('input.name'),
            chargeName     = $this.closest('.entries').find('input.name').val(),
            inputPrice     = $this.closest('.entries').find('input.price'),
            chargePrice    = $this.closest('.entries').find('input.price').val(),
            selectEvent    = $this.closest('.entries').find('.event'),
            chargeEvent    = $this.closest('.entries').find('.event').val(),
            chargeID       = $this.closest('.entries').attr('data-id'),
            editControls   = $this.closest('.entries').find('.controls.editDelete'),
            saveControls   = $this.closest('.entries').find('.controls.saveStorno'),
            season         = $this.closest('#playerChargesEdit').find('#seasonName').val(),
            player         = $this.closest('#playerChargesEdit').find('#playerId').val();

        if ( $this.hasClass('edit') )
        {
            labelName.addClass('forBlind');
            labelPrice.addClass('forBlind');
            labelEvent.addClass('forBlind');
            editControls.addClass('forBlind');
            inputName.removeClass('forBlind');
            inputPrice.removeClass('forBlind');
            selectEvent.removeClass('forBlind');
            saveControls.removeClass('forBlind');
            inputName.val( labelNameText );
            inputPrice.val( labelPriceText );
            selectEvent.find('option[value="' + labelEventText + '"]').attr('selected', 'selected')
        }
        else if ( $this.hasClass('storno') )
        {
            inputName.addClass('forBlind');
            inputPrice.addClass('forBlind');
            selectEvent.addClass('forBlind');
            saveControls.addClass('forBlind');
            labelName.removeClass('forBlind');
            labelPrice.removeClass('forBlind');
            labelEvent.removeClass('forBlind');
            editControls.removeClass('forBlind');
            selectEvent.find('option:selected').attr('selected', false);
        }
        else if ( $this.hasClass('delete') )
        {
            var data_to_send = 'delete_player_charge=1&charge[id]=' + chargeID + '&season=' + season + '&player=' + player;

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' )
                    {
                        $this.closest('.entries').remove();
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }
        else if ( $this.hasClass('save') )
        {

            var data_to_send = 'manage_player_charge=1&charge[name]=' + chargeName + '&charge[price]=' + chargePrice + '&charge[event]=' + chargeEvent + '&charge[id]=' + chargeID + '&season=' + season + '&player=' + player;

            // pokud je to novy poplatek
            if ( $this.closest('.entries').hasClass('new') )
            {
                data_to_send = 'manage_player_charge=1&charge[name]=' + chargeName + '&charge[price]=' + chargePrice + '&charge[event]=' + chargeEvent + '&season=' + season + '&player=' + player;
            }

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok-edit' )
                    {
                        labelName.text(chargeName);
                        labelPrice.text(chargePrice);
                        labelEvent.text(chargeEvent);
                        inputName.addClass('forBlind');
                        inputPrice.addClass('forBlind');
                        selectEvent.addClass('forBlind');
                        saveControls.addClass('forBlind');
                        labelName.removeClass('forBlind');
                        labelPrice.removeClass('forBlind');
                        labelEvent.removeClass('forBlind');
                        editControls.removeClass('forBlind');
                    }
                    else if ( response.status == 'ok-new' )
                    {
                        // odstranim vsechny entries
                        $this.closest('#popUpEditCharge').find('.entries').remove();

                        // pridam vsechny entries
                        $('#popUpEditCharge').prepend(response.response);
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });
        }

        return false;

    });

    
    // editovat poplatky hrace
    $('#playerChargesEdit .entries .paid').live('change', function(){

        var $this    = $(this),
            player   = $this.closest('#playerChargesEdit').find('#playerId').val(),
            chargeID = $this.closest('.entries').attr('data-id');

        if ( $(this).is(':checked') )
        {
            var data_to_send = 'paid_player_charge=1&player=' + player + '&charge[id]=' + chargeID;
        }
        else
        {
            var data_to_send = 'paid_player_charge=0&player=' + player + '&charge[id]=' + chargeID;            
        }

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // loading
                }
                else
                {
                    alert(response.message);
                }

            }
        });
    });


    // pridani noveho radku s fixnim poplatkem u hrace
    $('#playerChargesEdit .addCharge .button.add.fixed').click(function(){

        var $this = $(this);

        var data_to_send = 'add_charge_fixed_row=1';

        $.ajax({
            url: '/ajax.php',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'get',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // loading
                    $(response.response).insertBefore( $this.closest('p') );
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;

    });
    $('#playerChargesEdit .addCharge .button.add.noFixed').click(function(){

        $( '<p class="entries new cleaned">' +
                '<input class="text name first" type="text" name="" placeholder="název poplatku" />' +
                '<input class="text price" type="text" name="" placeholder="suma" />' +
                '<input class="text event" type="text" name="" placeholder="název události" />' +
                '<span class="saveStorno controls">' +
                    '<a href="#" class="save" title="Uložit změnu"></a>' +
                '</span>' +
            '</p>' ).insertBefore( $(this).closest('p') );

        return false;

    });

}


/************************************************************
    NAHRANI A OREZ PROFILOVYCH OBRAZKU
************************************************************/

// funkce na zjisteni parametru v url
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}


// Jcrop - orez obrazku - nastaveni obrazku
function showPreview(coords)
{
    var rx = 200 / coords.w;
    var ry = 200 / coords.h;

    $('.profilePhoto .crop img').css({
        width: Math.round(rx * 300) + 'px',
        //height: Math.round(ry * 300) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
    });

    var rx = 46 / coords.w;
    var ry = 46 / coords.h;

    $('.commentPhoto .crop img').css({
        width: Math.round(rx * 300) + 'px',
        //height: Math.round(ry * 300) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
    });

    var rx = 26 / coords.w;
    var ry = 26 / coords.h;

    $('.teamPhoto .crop img').css({
        width: Math.round(rx * 300) + 'px',
        //height: Math.round(ry * 300) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
    });

    // nacteni souradnic do inputu
    $('#x').val(coords.x);
    $('#y').val(coords.y);
    $('#w').val(coords.w);
    $('#h').val(coords.h);
}


function ajaxCrop(){

    var x = $('#x').val(),
        y = $('#y').val(),
        w = $('#w').val();

    // je to uzivatel
    if ( path == 'registration' )
    {
        var $pictureBig  = $('#picture_big'),
            url          = '/ajax.php',
            $pictureMed  = $('.picture_medium'),
            $pictureMain = $('.picture_main');
    }
    // je to tym
    else if ( path == 'team-registration' )
    {
        var $pictureBig  = $('#team_picture_big'),
            url          = '/team_ajax.php',
            $pictureMed  = $('.team_picture_medium'),
            $pictureMain = $('.team_picture_main');
    }

    $pictureBig.html('<span id="loading"><!-- --></span>');

    // sestaveni dat k odeslani
    // registrace uzivatele
    if( (typeof getHash != 'undefined') && (typeof getEmail != 'undefined') && path == 'registration' )
    {
        var data_to_send = 'hash=' + getHash + '&email=' + getEmail + '&crop=1&x=' + x + '&y=' + y + '&width=' + w;
    }
    // editace uzivatele
    else if ( path == 'registration' )
    {
        var data_to_send = '&crop=1&x=' + x + '&y=' + y + '&width=' + w;
    }
    // registrace nebo editace tymu
    else if ( path == 'team-registration' )
    {
        var data_to_send = '&crop=1&x='+ x + '&y=' + y + '&width=' + w + '&page=' + window.location.pathname.replace(/\//g,'');
    }

    $.ajax({
        url: url,
        type: 'get',
        data: data_to_send,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (res) {

            if(res.status == 'ok')
            {
                var re1 = /width="\d+" height="\d+"/;
                var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";

                $pictureBig.html(res.img_big); // tim se prepise loading
                $pictureMed.html(res.img_medium);

                if ( path == 'registration' )
                {
                    // vymenim profilovku uzivatel
                    $('#user .left .picture').html(res.img_small);
                }
                else if ( path == 'team-registration' )
                {
                    if(!res.serialized)
                    {
                        // vymenim logo v team info
                        $('#teamInfo .team_picture_small').html(res.img_small);
                        // vymenim logo v last match
                        $('#lastMatch .team-1 .logo').html(res.img_medium);
                    }
                    else
                    {
                        $('#tmpImages').val(res.serialized);
                    }
                }

                $pictureMain.html(res.img_original.replace(re1,replace));
                $('#mainPhoto').Jcrop({
                    onChange:    showPreview,
                    onSelect:    showPreview,
                    setSelect:   [ 50, 50, 250, 250 ],
                    minSize:     [ 200, 200 ],
                    aspectRatio: 1
                });
            }
            else
            {
                alert('Nastala chyba při resizu obrázku.');
            }

        },
        complete: function(){

            var messagePlace = $('#contentA');

            // pokud je to uprava profilu zobrazim done hlasku, u registraci ne
            if ( $('#edit-user-profile').length )
            {
                messagePlace.find('.msg:not(.noRemove)').remove();
                messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně změněna.</p></div>');
            }
            // pokud je to uprava tymu zobrazim done hlasku, u registraci ne
            else if ( $('#edit-team-profile').length )
            {
                messagePlace.find('.msg:not(.noRemove)').remove();
                messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Logo týmu bylo úspěšně změněno.</p></div>');
            }
        }
    });
}


function deletePhoto(){
    $('#contentA').delegate('#deleteProfilePhoto', 'click', function() {

        // je to uzivatel
        if ( path == 'registration' )
        {
            var title   = 'Potvrdit smazání fotografie',
                message = 'Opravdu si přejete smazat svoji profilovou fotografii?',
                $pictureBig = $('#picture_big'),
                url          = '/ajax.php',
                $pictureMed  = $('.picture_medium'),
                $pictureMain = $('.picture_main');;
        }
        // je to tym
        else if ( path == 'team-registration' )
        {
            var title   = 'Potvrdit smazání loga',
                message = 'Opravdu si přejete smazat vaše týmové logo?',
                $pictureBig  = $('#team_picture_big'),
                url          = '/team_ajax.php',
                $pictureMed  = $('.team_picture_medium'),
                $pictureMain = $('.team_picture_main');
        }

        // confirm na smazani zpravy
        $.confirm({
            'title'     : title,
            'message'   : message,
            'buttons'   : {
                'potvrdit'  : {
                    'class' : 'buttonA icon confirm',
                    'action': function(){

                        $pictureBig.html('<span id="loading"><!-- --></span>');

                        if( (typeof getHash != 'undefined') && (typeof getEmail != 'undefined') && path == 'registration' )
                        {
                            var data_to_send = 'hash=' + getHash + '&email=' + getEmail + '&delete_profile_photo=1';
                        }
                        else if ( path == 'registration' )
                        {
                            var data_to_send = 'delete_profile_photo=1';
                        }
                        else if ( path == 'team-registration' )
                        {
                            var data_to_send = 'delete_profile_photo=1&page=' + window.location.pathname.replace(/\//g,'');
                        }

                        $.ajax({
                            url: url,
                            type: 'get',
                            data: data_to_send,
                            dataType: 'json',
                            success: function (res) {

                                if(res.status == 'ok')
                                {
                                    var re1 = /width="\d+" height="\d+"/;
                                    var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";

                                    $pictureBig.html(res.img_big); // tim se prepise loading
                                    $pictureMed.html(res.img_medium);

                                    if ( path == 'registration' )
                                    {
                                        // vymenim profilovku uzivatel
                                        $('#user .left .picture').html(res.img_small);
                                    }
                                    else if ( path == 'team-registration' )
                                    {
                                        if(!res.serialized)
                                        {
                                            // vymenim logo v team info
                                            $('#teamInfo .team_picture_small').html(res.img_small);
                                            // vymenim logo v last match
                                            $('#lastMatch .team-1 .logo').html(res.img_medium);
                                        }
                                        else
                                        {
                                            $('#tmpImages').val(res.serialized);
                                        }
                                    }

                                    $pictureMain.html(res.img_original.replace(re1,replace));
                                }
                                else
                                {
                                    alert('Nastala chyba při ukládání obrázku.');
                                }

                            },
                            complete: function(){

                                var messagePlace    = $('#contentA'),
                                    controlsPlace   = $('#contentA .inner .left .list');

                                controlsPlace.find('.edit').remove();
                                controlsPlace.find('.delete').remove();

                                // pokud je to uprava profilu zobrazim done hlasku, u registraci ne
                                if ( $('#edit-user-profile').length )
                                {
                                    messagePlace.find('.msg:not(.noRemove)').remove();
                                    messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně smazána.</p></div>');
                                }
                                // pokud je to uprava tymu zobrazim done hlasku, u registraci ne
                                else if ( $('#edit-team-profile').length )
                                {
                                    messagePlace.find('.msg:not(.noRemove)').remove();
                                    messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Logo týmu bylo úspěšně smazáno.</p></div>');
                                }
                            }
                        });

                    }
                },
                'zrušit'    : {
                    'class' : 'buttonA',
                    'action': function(){}   // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });

        return false;
    });
}



function uploadPhoto(){
    var formdata = false;

    $('#uploadPhoto').live('change',function(){
        var i = 0, len = this.files.length, img, reader, file;

        file = this.files[i];

        // pokud je to podporovany typ fotek
        if ( file.type.match(/image.jpeg/) || file.type.match(/image.jpg/) || file.type.match(/image.png/) || file.type.match(/image.gif/) )
        {
            if (window.FormData) {
                formdata = new FormData();
                //document.getElementById("btn").style.display = "none";
            }
            if ( window.FileReader ) {
                reader = new FileReader();
                reader.onloadend = function (e) {
                    //showUploadedItem(e.target.result, file.fileName);
                };
                reader.readAsDataURL(file);
            }
            if (formdata) {
                formdata.append("image", file);
            }
        }
        else
        {
            alert('Tento typ souboru není podporován! Nahrajte prosím podporovaný formát JPEG, PNG nebo GIF.');
            return false;
        }

        if (formdata) {

            // je to uzivatel
            if ( path == 'registration' )
            {
                var $pictureBig = $('#picture_big'),
                    $pictureMed  = $('.picture_medium'),
                    $pictureMain = $('.picture_main');;
            }
            // je to tym
            else if ( path == 'team-registration' )
            {
                var $pictureBig  = $('#team_picture_big'),
                    $pictureMed  = $('.team_picture_medium'),
                    $pictureMain = $('.team_picture_main');
            }

            $pictureBig.html('<span id="loading"><!-- --></span>');

            if( (typeof getHash != 'undefined') && (typeof getEmail != 'undefined') && path == 'registration' )
            {
                var url = '/ajax.php?hash=' + getHash + '&email=' + getEmail + '&file';
            }
            else if ( path == 'registration' )
            {
                var url = '/ajax.php?file';
            }
            else if ( path == 'team-registration' )
            {
                var url = '/team_ajax.php?file=' + window.location.pathname.replace(/\//g,'');
            }

            $.ajax({
                url: url,
                type: 'post',
                data: formdata,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (res) {
                    if(res.status == "ok")
                    {
                        var re1 = /width="\d+" height="\d+"/;
                        var replace = "id=\"mainPhoto\" alt=\"Hlavní fotografie\"";

                        $pictureBig.html(res.img_big); // tim se prepise loading
                        $pictureMed.html(res.img_medium);

                        if ( path == 'registration' )
                        {
                            // vymenim profilovku uzivatel
                            $('#user .left .picture').html(res.img_small);
                        }
                        else if ( path == 'team-registration' )
                        {
                            if(!res.serialized)
                            {
                                // vymenim logo v team info
                                $('#teamInfo .team_picture_small').html(res.img_small);
                                // vymenim logo v last match
                                $('#lastMatch .team-1 .logo').html(res.img_medium);
                            }
                            else
                            {
                                $('#tmpImages').val(res.serialized);
                            }
                        }

                        $pictureMain.html(res.img_original.replace(re1,replace));
                        $('#mainPhoto').Jcrop({
                            onChange:    showPreview,
                            onSelect:    showPreview,
                            setSelect:   [ 50, 50, 250, 250 ],
                            minSize:     [ 200, 200 ],
                            aspectRatio: 1
                        });
                    }
                    else
                    {
                        alert('Nastala chyba při ukládání obrázku.');
                    }
                },
                complete: function(){

                    var messagePlace    = $('#contentA'),
                        controlsPlace   = $('#contentA .inner .left .list');

                    controlsPlace.find('.edit').remove();
                    controlsPlace.find('.delete').remove();

                    // pokud je to uprava profilu zobrazim done hlasku, u registraci ne
                    if ( $('#edit-user-profile').length )
                    {
                        messagePlace.find('.msg:not(.noRemove)').remove();
                        messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Profilová fotografie byla úspěšně změněna.</p></div>');
                    }
                    // pokud je to uprava tymu zobrazim done hlasku, u registraci ne
                    else if ( $('#edit-team-profile').length )
                    {
                        messagePlace.find('.msg:not(.noRemove)').remove();
                        messagePlace.prepend('<div class="msg done"><h3 class="title">Hotovo!</h3><p class="text">Týmové logo bylo úspěšně změněno.</p></div>');
                    }

                    if ( path == 'registration')
                    {
                        controlsPlace.append('<li class="item edit">' +
                                                '<a title="Upravit výřez profilové fotografie" class="popUpBig" href="#editProfilePhoto">Upravit výřez fotografie</a>' +
                                            '</li>' +
                                            '<li class="item delete">' +
                                                '<a title="Smazat profilovou fotografii" id="deleteProfilePhoto" href="#">Smazat profilovou fotografii</a>' +
                                            '</li>');
                    }
                    else if ( path == 'team-registration' )
                    {
                        controlsPlace.append('<li class="item edit">' +
                                                '<a title="Upravit výřez týmového loga" class="popUpBig" href="#editProfilePhoto">Upravit výřez týmového loga</a>' +
                                            '</li>' +
                                            '<li class="item delete">' +
                                                '<a title="Smazat týmové logo" id="deleteProfilePhoto" href="#">Smazat týmové logo</a>' +
                                            '</li>');
                    }
                }
            });
        }
    });
}


/************************************************************
    NÁPOVĚDA OVERLAY
************************************************************/
function helpControls() {

    $('#helpOverlay .helpBox').prepend('<span class="border leftTop"><!-- --></span><span class="border rightTop"><!-- --></span><span class="border leftBottom"><!-- --></span><span class="border rightBottom"><!-- --></span>');
    $('#helpOverlay .info').prepend('<span class="arrow"><!-- --></span>');

    $('#helpOverlay .close').click(function(){
        $('#helpOverlay').prev('link').remove();
        $('#helpOverlay').remove();
        return false;
    });

}

function helpOpen() {

    $('#helpButton').click(function(){

        // pokud jde o mobilni prohlizec
        if ( $(window).width() < 751 )
        {
            return true;
        }
        else
        {
            var url = $(this).attr('href');

            var data_to_send = 'getHelp=1&action=' + path;

            $.ajax({
                url: '/ajax.php',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                type: 'get',
                data: data_to_send,
                success: function(response){

                    if ( response.status == 'ok' && response.response != '' )
                    {
                        $(response.response).appendTo('body');

                        helpControls();
                    }
                    else if ( response.response == '' )
                    {
                        window.location.href = url;
                    }
                    else
                    {
                        alert(response.message);
                    }

                }
            });

            return false;
        }

    });

}


/************************************************************
    FEEDBACK
************************************************************/
function feedback() {

    $('#feddbackPopup form').live('submit', function(){

        var data_to_send = 'feedback=1&';

        data_to_send += $(this).serialize();

        $.ajax({
            url: '/ajax.php',
            //contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'post',
            data: data_to_send,
            success: function(response){

                if ( response.status == 'ok' )
                {
                    // odstranim pripadnou predchozi hlasku
                    $('#contentA > .msg').remove();

                    // vlozim done hlasku
                    $(response.response).prependTo( $('#contentA') );

                    // zavru popup
                    $('#feddbackPopup').closest('.fancybox-wrap ').find('.fancybox-close').click();

                    // vyprazdnim formular
                    $('#feddbackPopup form .text').val('');
                }
                else
                {
                    alert(response.message);
                }

            }
        });

        return false;

    });

}


/************************************************************
    ANCHOR SCROLL
************************************************************/
function anchorScroll() {

    $('.anchor').click(function(){

        var href = $(this).attr('href'),
            hash = href.substring(1);

        // zascrolluju na dany element 
        $('html, body').animate({
            scrollTop: $('#' + hash).offset().top - 60
        }, 750);

        return false;

    });

}


/************************************************************
    ANCHOR SCROLL ON LOAD PAGE
************************************************************/
function anchorScrollOnLoad() {

    // pokud je v url hash
    if ( window.location.hash )
    {
        var hash = window.location.hash,
            hash = hash.substring(1);

        // zascrolluju na dany element 
        $('html, body').animate({
            scrollTop: $('#' + hash).offset().top - 60
        }, 750);

    }

}