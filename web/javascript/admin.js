 /******************************
    READY
******************************/
$(document).ready(function(){

	// smazani radku
    $('.delete').click(function(){
        $(this).closest('.entries').remove();

        return false;
    });


    // pridani dalsiho radku
    $('.add').click(function(){

        var entries = $(this).prev('.entries').html();

        $( '<p class="entries added">' + entries + '</p>' ).insertBefore( $(this) );

        // reset inputu
        $('.entries.added input').val('');

        // reset selectu number
        /*
        $('.entries.added select.number option:selected').attr('selected', false);
        $('.entries.added select.number option:first').attr('selected', 'selected');

        // reset selectu position
        $('.entries.added select.position option:selected').attr('selected', false);
        $('.entries.added select.position option:first').attr('selected', 'selected');
        */

        // odstranim pomocnou classu added
        $('.entries.added').removeClass('added');

        return false;

    });

});