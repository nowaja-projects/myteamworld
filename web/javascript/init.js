 /******************************
    READY
******************************/
$(document).ready(function(){

    // alert( $(window).width() );

    $('html').browserDetection();

    // posunuti #teamMenu v html do #contentA
    $('#teamMenu').prependTo('#contentA');


    // Tabs    
    $('.navigation').tabs(
        '.tabs > div',
        {
            history: true
        }
    );


    // Tooltip
    $('.help').tooltip({ relative: true, offset: [-8, 0] });


    // Fixnuty pravy sidebar pouze na strankach kde se vyskytuje
    if ( $('.sidebar.right').length == 1 )
    {
        //fixedScroll();
    }

    $('.fancybox').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        nextEffect  : 'none',
        prevEffect  : 'none',
        padding     : 10,
        helpers     : {
            title : {
                type : 'over'
            }
        }
    });


    $('.popUp').fancybox({   
        width       : 400,
        height      : 'auto',
        fitToView   : false,
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        title       : false,
        padding     : 10,
        live        : true,
        afterShow   : function(){
            var close = $('.fancybox-close, .fancybox-popup .close');

            close.live('click', function(){
                $.fancybox.close();
            });
        }
    });
            
    $('.popUpAjax').fancybox({   
        width       : 400,
        height      : 'auto',
        fitToView   : false,
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        title       : false,
        padding     : 10,
        type        : 'ajax',
        live        : true,
        afterShow   : function(){
            var close = $('.fancybox-close, .fancybox-popup .close');

            close.live('click', function(){
                $.fancybox.close();
            });
        }
    });

    $('.popUpMedium').fancybox({
        width       : 540,
        height      : 'auto',
        fitToView   : false,
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        title       : false,
        padding     : 10,
        live        : true,
        afterShow   : function(){
            var close = $('.fancybox-close, .fancybox-popup .close');

            close.live('click', function(){
                $.fancybox.close();
            });
        }
    });

    $('.popUpMediumAjax').fancybox({
        width       : 540,
        height      : 'auto',
        fitToView   : false,
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        title       : false,
        padding     : 10,
        type        : 'ajax',
        live        : true,
        afterShow   : function(){
            var close = $('.fancybox-close, .fancybox-popup .close');

            close.live('click', function(){
                $.fancybox.close();
            });
        }
    });

    $('.popUpBig').fancybox({
        width       : 660,
        height      : 'auto',
        fitToView   : false,
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        title       : false,
        padding     : 10,
        live        : true,
        afterShow   : function(){
            var close = $('.fancybox-close, .fancybox-popup .close');

            close.live('click', function(){
                $.fancybox.close();
            });
        }
    });

    $('.popUpBigAjax').fancybox({
        width       : 660,
        height      : 'auto',
        fitToView   : false,
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        title       : false,
        padding     : 10,
        type        : 'ajax',
        live        : true,
        afterShow   : function(){
            var close = $('.fancybox-close, .fancybox-popup .close');

            close.live('click', function(){
                $.fancybox.close();
            });
        }
    });


    // nevim kde vsude se bude pouzivat, urcite ve vyheldavani a na strankach tymu cizihio?
    fansAndObservers();


    // krizek pro zavreni info zprav
    msgClose();
    

    // nacitani notifikaci
    // TODO - musi tu byt vyreseny i jiny stranky, kde to nema byt
    switch (path) {

        case 'main':
        case 'what-is-myteamworld':
            // nedelam nic
        break;

        default:
            notificationsAndNewsFirst();
            notificationsAndNews();
            readNotifications();
        break;

    };

    // zadani dochazky u nejblizsi udalosti
    if ( $('#nextEvents').length )
    {
        quickAttendance();
    }

    searchToggle();

    searchComplete();

    menuToggle();

    switchTeam();

    leaveTeam();

    recommend();

    // help overlay
    if ( $('#helpOverlay').length )
    {
        helpControls();
    }
    
    //helpOpen(); docasne vypnuta napoveda po kliknuti na tlacitko v menu

    // feedback
    if ( $('#feedback').length )
    {
        feedback();
    }

    // nastaveni kalendare
    $.datepicker.regional['cs'] = {
        closeText: 'Zavřít',
        prevText: 'Předchozí',
        nextText: 'Následující',
        currentText: 'Dnes',
        monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
        monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'],
        dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'],
        dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',],
        dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'],
        firstDay: 1
    };
    /*
    $.datepicker.regional['sk'] = {
        closeText: 'Zavrieť',
        prevText: 'Predchádzajúci',
        nextText: 'Nasledujúci',
        currentText: 'Dnes',
        monthNames: ['Január','Február','Marec','Apríl','Máj','Jún','Júl','August','September','Október','November','December'],
        monthNamesShort: ['Jan','Feb','Mar','Apr','Máj','Jún','Júl','Aug','Sep','Okt','Nov','Dec'],
        dayNames: ['Nedela','Pondelok','Utorok','Streda','Štvrtok','Piatok','Sobota'],
        dayNamesShort: ['Ned','Pon','Uto','Str','Štv','Pia','Sob'],
        dayNamesMin: ['Ne','Po','Ut','St','Št','Pia','So'],
        firstDay: 1
    };
    */
     
    $.datepicker.setDefaults($.datepicker.regional['cs']);

    // snazim se volat az po vsem moznem, kdy je vyska vsech prvku ustalena
    /*
    if ( $('.sidebar').length )
    {
        equalizer();

        $(window).scroll(function(){
            equalizer();
        });
    }
    */
    // pokud to neni mobil
    if ( $(window).width() > 767 )
    {
        equalizer();
    }

    anchorScroll();

    // preskladavani sloupcu pro mensi rozliseni
    $(window).resize(function(){
        // pokud pravy sidebar je a zaroven jsem ve velikosti pro tablety
        if ( $('.sidebar.right.colSmall').length && $(window).width() < 975 && $(window).width() > 767 )
        {
            var $sidebarLeft = $('.sidebar.left.colSmall'),
                $ag = $('#ag'),
                $nextEvents = $('#nextEvents');

            // presunu bloky z praveho sloupce do leveho
            $( $nextEvents ).appendTo( $sidebarLeft );
            $( $ag ).appendTo( $sidebarLeft );
        }
        // pokud pravy sidebar je a zaroven jsem ve velikosti vetsi nez pro tablety a mobily
        else if ( $('.sidebar.right.colSmall').length && $(window).width() >= 975 )
        {
            var $sidebarRight = $('.sidebar.right.colSmall'),
                $ag = $('#ag'),
                $nextEvents = $('#nextEvents');

            // presunu bloky z leveho sloupce zpet do praveho
            $( $nextEvents ).appendTo( $sidebarRight );
            $( $ag ).appendTo( $sidebarRight );
        }
        // pokud pravy sidebar je a zaroven jsem ve velikosti pro mobily
        else if ( $('.sidebar.right.colSmall').length && $(window).width() <= 767 )
        {
            var $sidebarRight = $('.sidebar.right.colSmall'),
                $ag = $('#ag'),
                $nextEvents = $('#nextEvents');

            // presunu bloky z leveho sloupce zpet do praveho
            $( $nextEvents ).appendTo( $sidebarRight );
            $( $ag ).appendTo( $sidebarRight );
        }
    });


    // pridani tlacitka na zobrazeni teamOptions
    $(window).resize(function(){
        if ( $('#teamOptions').length && $(window).width() < 444 )
        {
            if ( !$('#toggleTeamOptions').length )
            {
                $('#teamOptions').hide();

                $('#teamInfo').append('<span id="toggleTeamOptions"><!-- --></span>');
            
                $('#toggleTeamOptions').click(function(){
                    $(this).toggleClass('active');
                    $('#teamOptions').toggle();
                });
            }
        }
        else
        {
            $('#toggleTeamOptions').remove();
            $('#teamOptions').show();
        }
    });

    // vynutim resize po reloadu
    $(window).trigger('resize');


    /*** GA ***/
    // advert user click
    $('#ag .title a, #ag .img, #ag .text a').mousedown(function(e){

        if ( e.which <= 2 )
        {
            var name = $(this).closest('.aBox').attr('data-name');

            ga('send', 'event', 'Advert - right panel', 'click', name);
        }

    });

    // team registration click left panel
    if ( $('#teamRegistration').length )
    {
        $('#teamRegistration .link a').mousedown(function(e){

            if ( e.which <= 2 )
            {
                ga('send', 'event', 'Team registration - left panel', 'click');
            }

        });
    }

    // feedback user click
    if ( $('#feedback').length )
    {
        $('#feedback a').click(function(){

            ga('send', 'event', 'Feedback open', 'click');

        });
    }

    // facebook like
    if ( $(window).width() >= 583 )
    {
        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/cs_CZ/sdk.js#xfbml=1&appId=344484536227&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }


    /******************************************************************

        NACITANI PODLE AKTIVNI STRANKY

    ******************************************************************/

    // path se uklada v index.php
    console.log(path);
    switch (path) {


        case 'main':

            // naseptavac mest a obci
            $('#locationID').fcbkcomplete({
                json_url: '/ajax.php?city',
                width: '159',
                addontab: true, // nefunguje, check
                firstselected : true,
                maxitems: 1,
                filter_selected: true,
                input_min_size: 1,
                maxshownitems: 5,
                height: 5,
                cache: true, // nefunguje, check
                complete_text: '',
                delay : 500,
                onselect: function(){
                    $('#locationID_annoninput').hide();
                },
                onremove: function(){
                    $('#locationID_annoninput').show();
                    $('#locationID_annoninput').click();
                }
            });

            // defaultne vyplnim uz vybrane mesto, pokud je
            var city   = $('#location').val(),
                cityID = $('#location').attr('data-id');

            if ( city.length && cityID.length )
            {
                $('#locationID').trigger("addItem",[{"title": city, "value": cityID}]);
            }

        break;


        // ----------------- //


        case 'wall':

            $('#contentA .timeago').timeago();

            addTeamMessage();

            comments();

            deleteComment();

            likeUnlike();

            if ( $('#teamPost').length )
            {
                teamPost();
            }

            deleteTeamPost();

            wallFilter();
            

            // infiniteScroll;
            if ( !$('#contentA #noMoreMessages').length )
            {
                $(window).scroll( infiniteScroll );
            }

            // Filter - uzivatel bez tymu
            $("#contentA .smallFilter .filterButton").click(function(){
                $(this).closest('#contentA').find('#filterDiv').slideToggle();

                return false;
            });

            // Filter - uzivatel s tymem
            $("#status .smallFilter a").click(function(){
                $(this).closest('#contentA').find('#filterDiv').slideToggle();

                return false;
            });

            // Check & uncheck all checkboxes
            $('#filterAll').click(function(){
                $(this).closest('form').find(':checkbox').attr('checked', this.checked);
            });

            // Check & uncheck category sport
            $('#filterCategorySport').click(function(){
                $(this).closest('fieldset').find('.list :checkbox').attr('checked', this.checked);
            });
            
            // Check & uncheck category team
            $('#filterCategoryTeam').click(function(){
                $(this).closest('fieldset').find('.list :checkbox').attr('checked', this.checked);
            });


            // scrollTop
            $(window).scroll(function(){
                var scrollHeight = $(window).scrollTop();

                if ( scrollHeight > 1250 )
                {
                    $('#scrollTop').fadeIn(1250);
                }
                else
                {
                    $('#scrollTop').fadeOut(1250);
                }
            }).scroll();

            $(window).resize(function(){
                var scrollHeight = $(window).scrollTop(),
                    width = $(window).width();

                if ( width < 960 )
                {
                    $('#scrollTop').hide();
                }
                if ( width >= 960 && scrollHeight > 1250 )
                {
                    $('#scrollTop').fadeIn(1250);
                }
            }).resize();

            $('#scrollTop a').click(function(){
                $('html, body').animate({ scrollTop: '0' }, 1000 );

                return false;
            });

            // nastaveni GA - uzivatel sel na detail rss novinky
            $('#contentA').delegate('.box.news.rss .boxTitle a', 'mousedown', function(e){

                if ( e.which <= 2 )
                {
                    var source = $(this).closest('.content').find('.post .links .ico span').text();
                    var source = source.substr(source.indexOf(' ') + 1);

                    ga('send', 'event', 'Wall - show full article', 'click', source);
                }

            });

            // nastaveni GA - uzivatel sel na stranky serveru novinky
            $('#contentA').delegate('.box.news.rss .post .links a', 'mousedown', function(e){

                if ( e.which <= 2 )
                {
                    var source = $(this).find('span').text();
                    var source = source.substr(source.indexOf(' ') + 1);

                    ga('send', 'event', 'Wall - show source website', 'click', source);
                }

            });

        break;


        // ----------------- //


        case 'news-detail':

            $('#contentA .timeago').timeago();

            comments();

            deleteComment();

            likeUnlike();

        break;


        // ----------------- //


        case 'teampost-team-profile':
        case 'wall-team-profile':

            $('#contentA .timeago').timeago();

            comments();

            deleteComment();

            likeUnlike();

            deleteTeamPost();

        break;


        // ----------------- //


        case 'registration':  // i editace uzivatele

            // fix history tabu po ulozeni formulare
            // pokud mam nejakou hlasku, chci ji videt
            if ( $('#contentA > .msg').length )
            {
                $('html, body').animate({
                    scrollTop: 0
                }, 1);
            }

            // naseptavac mest a obci
            $('#locationID').fcbkcomplete({
                json_url: '/ajax.php?city',
                width: '178',
                addontab: true, // nefunguje, check
                firstselected : true,
                maxitems: 1,
                filter_selected: true,
                input_min_size: 1,
                maxshownitems: 5,
                height: 5,
                cache: true, // nefunguje, check
                complete_text: '',
                delay : 500,
                onselect: function(){
                    $('#locationID_annoninput').hide();
                },
                onremove: function(){
                    $('#locationID_annoninput').show();
                    $('#locationID_annoninput').click();
                }
            });

            // defaultne vyplnim uz vybrane mesto, pokud je
            var city   = $('#location').val(),
                cityID = $('#location').attr('data-id');

            if ( city.length && cityID.length )
            {
                $('#locationID').trigger("addItem",[{"title": city, "value": cityID}]);
            }

            // ulozim do globalni promenne hash a email z url
            if ( $('#registration').length )
            {
                getHash  = getUrlParameter('hash');
                getEmail = getUrlParameter('email');
            }

            deletePhoto();
            uploadPhoto();

            // dialogove okno nahrat profilove foto
            $('#contentA .left .upload a').click(function(){
                $(this).parent().parent().parent().find('#uploadPhoto').click();
                return false;
            });


            // pokud se bude menit, tak zmenit i pri uploadu profilovky
            $('#mainPhoto').Jcrop({
                onChange:    showPreview,
                onSelect:    showPreview,
                setSelect:   [ 50, 50, 250, 250 ],
                minSize:     [ 200, 200 ],
                aspectRatio: 1
            });


            // ulozeni noveho vyrezu fotky
            $('#saveCropFoto').live('click', function(){
                ajaxCrop();
                return false;
            });

        break;


        // ----------------- //


        case 'user-profile':  // i editace uzivatele

            latestActivity();

            messagePopupClose();

            messagePopup();

            userInvite();

        break;


        // ----------------- //


        case 'press-releases':

            $('#contentA .timeago').timeago();

            clearPressReleaseForm();

            editPressRelease();

            deletePressRelease();

            comments();

            deleteComment();
            
            likeUnlike();

            pressRelease();

            // po kliknuti na tlacitko
            $('#addEditButton').click(function(){

                // pokud nema classu edit
                if ( !$(this).hasClass('edit') )
                {
                    var currentTime         = new Date(),
                        hours               = (currentTime.getHours()<10?'0':'') + currentTime.getHours(),
                        minutes             = (currentTime.getMinutes()<10?'0':'') + currentTime.getMinutes(),
                        day                 = (currentTime.getDate()<10?'0':'') + currentTime.getDate(),
                        month               = ((currentTime.getMonth() + 1)<10?'0':'') + (currentTime.getMonth() + 1),
                        year                = currentTime.getFullYear(),
                        $addPost            = $('#addPost');

                    // aktualizuju datum
                    $addPost.find('input.date').val( year + '-' + month + '-' + day );

                    // aktualizuju cas
                    $addPost.find('input.time').val( hours + ':' + minutes );
                }

            });

            $('.datepicker').datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd'
            });

        break;


        // ----------------- //


        case 'press-releases-team-profile':

            $('#contentA .timeago').timeago();

            comments();

            deleteComment();

            likeUnlike();

        break;


        // ----------------- //


        case 'events':

            topFilter();

            deleteEvent();

            confirmEvent();

            moreEvents();

        break;


        // ----------------- //


        case 'events-team-profile':
        case 'event-detail':

            editResultNavi();

            topFilter();

            moreEvents();

            addAttendance();

        break;


        // ----------------- //


        case 'event-manage':

            selectEventType();
            eventNextStep();

            // zobrazit/skryt opakovani udalosti
            $('#contentA .dateAndTime #repeat').live('change', function(){

                var $repeatBox    = $(this).closest('.entries').find('#repeatBox'),
                    $dateInput    = $repeatBox.find('.datepicker'),
                    $repeatSelect = $repeatBox.find('.repeatSelect');

                /*
                if ( $(this).is(':checked') )
                {
                    //nedelam nic
                }
                else
                {
                    $dateInput.val('');
                    $repeatSelect.val('');
                }
                */

                $(this).closest('.entries').find('#repeatBox').toggleClass('forBlind');

            });


            // kliknuti na vytvorit udalost pro cely tym
            $('#allMembers').live('change', function(){

                var $popUpOpen     = $(this).closest('.entries').find('.rosterAttendance'),
                    $allCheckboxes = $(this).closest('.entries').find('.members input:checkbox'),
                    $info          = $(this).closest('.entries').find('.info');

                // pokud ho odchecknu, zobrazim popup
                if ( !$(this).is(':checked') )
                {
                    $popUpOpen.click();
                    $allCheckboxes.attr('checked', this.checked);

                    // zobrazim info text
                    $info.removeClass('forBlind');
                    // aktualizuji pocet vybranych clenu
                    $info.find('.count').text('0');
                }

                // pokud ho checknu
                if ( $(this).is(':checked') )
                {
                    // Check & uncheck all checkboxes
                    $allCheckboxes.attr('checked', this.checked);

                    // odstranim z popupu vsechny classy checked
                    $('#rosterAttendance .fakeCheckbox').removeClass('checked');

                    // skryju info text
                    $info.addClass('forBlind');
                }

            });


            // kliknuti na fake checkbox v popupu
            $('#rosterAttendance .fakeCheckbox').live('click', function(){

                var user = $(this).attr('data-id');

                // pridam classu checked
                $(this).toggleClass('checked');

                // checknu skryty input
                $('#contentA .roster').find( '.' + user ).click();

                // aktualizuji pocet vybranych clenu
                var userChecked = $(this).closest('#rosterAttendance').find('.fakeCheckbox.checked').length;

                $('#contentA .roster .count').text( userChecked );

                return false;

            });


            // zavreni popupu pres tlacitko hotovo
            $('#rosterAttendance .link a').live('click', function(){

                $(this).closest('.fancybox-skin').find('.fancybox-close').click();

                return false;

            });


            // kalendar pri editaci udalosti
            $('#contentA .datepicker').datepicker({
                //minDate: 0,
                //maxDate: "+1M +10D",
                dateFormat: "yy-mm-dd"
            });


        break;


        // ----------------- //


        case 'event-edit-result':

            editResultNavi();

            editResult();

        break;


        // ----------------- //


        case 'stats':

            topFilter();

        break;


        // ----------------- //


        case 'stats-team-profile':

            topFilter();

        break;


        // ----------------- //


        case 'attendance':

            // doplnim do popupu id uzivatele a jeho jmeno
            $('#attendBox').delegate('.sendMessage', 'click', function(){

                var id      = $(this).attr('data-id'),
                    name    = $(this).closest('tr').find('.player .name .firstName').text(),
                    surName = $(this).closest('tr').find('.player .name .lastName').text();

                $('#messageRecipients').val(name + ' ' + surName);
                $('#messageRecipientsID').val(id);

            });

            addAttendance();

            moreEventsAttendance();

            messagePopupClose();

            messagePopup();

        break;


        // ----------------- //


        case 'roster':

            // odchecknuti vsech zachecknutych inputu v userTypeSwitch
            $('#userTypeSwitch input:checked').attr('checked', false);

            userTypeSwitch();

            inviteByEmail();

            editRoster();

            editRosterClose();

            hostPlayerControls();

            playerControls();

            $('#popUpFormEditRoster .number').numericOnly();

            $('#popUpFormEditRoster .number').click(function(){
                $(this).select();
            });

        break;


        // ----------------- //


        case 'gallery':

            createGallery();
            renameGalleryPhoto();
            renameGalleryPhotoPopup();
            deleteGalleryPhoto();
            sortableGalleryList();

        break;


        // ----------------- //


        case 'gallery-detail-team-profile':

            renameGalleryPhoto();
            renameGalleryPhotoPopup();
            deleteGalleryPhoto();
            sortableGalleryDetail();
            mainGalleryPhoto();

            // inicializuji plugin 
            $('#uploadPhotos').dropzone({
                init: function() {
                    this.on("success", function(file, finished) {
                        if (finished.status == 'ok')
                        {
                            if ( $('#galleryDetail').length )
                            {
                                // pridam novou fotografii do listu
                                $('#galleryDetail').append(finished.photo_response);

                                // show done button
                                $('#addPhotos .link').show();
                            }
                            // pokud jeste neni vytvoreny #galleryDetail, vytvorim ho prvne
                            else
                            {
                                $('<ul id="galleryDetail" class="cleaned"></ul>').insertAfter( $('#contentA .contentAbox .headline') );

                                // pridam novou fotografii do listu
                                $('#galleryDetail').append(finished.photo_response);

                                // show done button
                                $('#addPhotos .link').show();
                            }

                            // inicializuji funkce
                            renameGalleryPhoto();
                            deleteGalleryPhoto();
                            sortableGalleryDetail();
                        }
                    });
                }
            });

            // kliknuti na tlacitko hotovo
            $('#addPhotos .link a').click(function(){

                // zavru popup
                $('#addPhotos').closest('.fancybox-wrap ').find('.fancybox-close').click();

                return false;
            });

            openPhotoFromHash();

        break;


        // ----------------- //


        case 'cashbox':

            cashInfo();

            chargesEdit();

        break;


        // ----------------- //


        case 'team-registration':

            // fix history tabu po ulozeni formulare
            // pokud mam nejakou hlasku, chci ji videt
            if ( $('#contentA > .msg').length )
            {
                $('html, body').animate({
                    scrollTop: $('#contentA').offset().top
                }, 1);
            }

            // naseptavac mest a obci
            $('#locationID').fcbkcomplete({
                json_url: '/ajax.php?city',
                width: '178',
                addontab: true, // nefunguje, check
                firstselected : true,
                maxitems: 1,
                filter_selected: true,
                input_min_size: 1,
                maxshownitems: 5,
                height: 5,
                cache: true, // nefunguje, check
                complete_text: '',
                delay : 500,
                onselect: function(){
                    $('#locationID_annoninput').hide();
                },
                onremove: function(){
                    $('#locationID_annoninput').show();
                    $('#locationID_annoninput').click();
                }
            });

            // defaultne vyplnim uz vybrane mesto, pokud je
            var city   = $('#location').val(),
                cityID = $('#location').attr('data-id');

            if ( city.length && cityID.length )
            {
                $('#locationID').trigger("addItem",[{"title": city, "value": cityID}]);
            }


            deletePhoto();
            uploadPhoto();

            // dialogove okno nahrat profilove foto
            $('#contentA .left .upload a').click(function(){
                $(this).closest('.left').find('#uploadPhoto').click();
                return false;
            });


            // pokud se bude menit, tak zmenit i pri uploadu profilovky
            $('#mainPhoto').Jcrop({
                onChange:       showPreview,
                onSelect:       showPreview,
                setSelect:      [ 50, 50, 250, 250 ],
                minSize:        [ 200, 200 ],
                aspectRatio:    1
            });


            // ulozeni noveho vyrezu fotky
            $('#saveCropFoto').live('click', function(){
                ajaxCrop();
                return false;
            });

            adminsEdit();

            seasonEdit();

            mergeStats();

            mudulesManage();

        break;


        // ----------------- //


        case 'messages':

            $.timeago.settings.strings = {
                prefixAgo: null,
                prefixFromNow: null,
                suffixAgo: null,
                suffixFromNow: null,
                seconds: "právě teď",
                minute: "asi před minutou",
                minutesConj: "před %d minutami",
                minutes: "před %d minutami",
                hour: "asi před hodinou",
                hoursConj: "před %d hodinami",
                hours: "před %d hodinami",
                day: "včera",
                daysConj: "před %d dny",
                days: "před %d dny",
                month: "před 1 měsícem",
                months: "před %d měsíci",
                year: "před 1 rokem",
                years: "před %d roky",
                wordSeparator: " ",
                numbers: []
            };

            $('#contentA .timeago').timeago();

            messages();

            messageControls();

            messagePopupClose();

            messagePopup();

            message();

            // naseptavac jmen
            $('#messageRecipients').fcbkcomplete({
                json_url: '/ajax.php?messageRecipients=1',
                width: '512',
                addontab: true, // nefunguje, check
                maxitems: 10,
                filter_selected: true,
                input_min_size: 0,
                maxshownitems: 5,
                height: 5,
                cache: true, // nefunguje, check
                complete_text: '',
                delay : 500
            });

            // infiniteScroll
            $('#contentA .message-container .containerMessages').scroll( infiniteScrollMessages );

        break;


        // ----------------- //


        case 'notices':

            confirmDeclineInvitationTeam();

            confirmDeclineInvitationMember();

        break;


        // ----------------- //


        case 'search':

            // doplnim do popupu id uzivatele a jeho jmeno
            $('#contentA .result .sendMessage').live('click', function(){

                var id      = $(this).attr('data-id'),
                    name    = $(this).closest('.result').find('.left .name span').text(),
                    surName = $(this).closest('.result').find('.left .name a').text();

                $('#messageRecipients').val(name + ' ' + surName);
                $('#messageRecipientsID').val(id);

            });

            showMoreSearch();

            messagePopupClose();

            messagePopup();

            userInvite();

        break;


        // ----------------- //


        case 'what-is-myteamworld':

            anchorScrollOnLoad();

            fixedScroll({
                $element: $('#main .sidebar.left .fixedBlock'),
                $area: $('#main .sidebar.left')
            });

        break;


        // ----------------- //


        case 'terms':

            fixedScroll({
                $element: $('#main .sidebar.left .fixedBlock'),
                $area: $('#main .sidebar.left')
            });

        break;


        // ----------------- //


        case 'help':
        case 'help-without-team':

            fixedScroll({
                $element: $('#main .sidebar.left .fixedBlock'),
                $area: $('#main .sidebar.left')
            });

        break;


        // ----------------- //


        default :
        break;

    };
   

});