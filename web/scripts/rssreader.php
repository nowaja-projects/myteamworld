<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RSS Reader</title>
<style>
.box{
border:1px solid #6699FF;
}
a.rsstitle{
color:#FF9900;
font-family:Arial, Helvetica, sans-serif;
font-size:11px;
font-weight:bold;
letter-spacing:1px;
text-decoration:none;
}
a.rsstitle:hover{
color:#FF9900;
font-family:Arial, Helvetica, sans-serif;
font-size:11px;
font-weight:bold;
letter-spacing:1px;
text-decoration:underline overline;
}
a.rsslink{
color:#6699FF;
font-family:Arial, Helvetica, sans-serif;
font-size:11px;
letter-spacing:1px;
text-decoration:none;
}
a.rsslink:hover{
color:#6699FF;
font-family:Arial, Helvetica, sans-serif;
font-size:11px;
letter-spacing:1px;
text-decoration:underline;
}
.rsstext{
color:#666666;
font-family:Arial, Helvetica, sans-serif;
font-size:11px;
}
.rssdate{
color:#999999;
font-family:Arial, Helvetica, sans-serif;
font-size:10px;
}
</style>
</head>

<body>
<?php
include("./files_includes/RSSReader.inc.php");

$rss = new RSSReader("http://www.php.net/news.rss");
$nn = $rss->getNumberOfNews();

?>
<table width="40%"  border="0" cellspacing="0" cellpadding="5" class="box">
  <tr>
    <td><?php echo $rss->getImage();?></td>
  </tr>
  <tr>
    <td><?php echo $rss->getChannelTitle("rsstitle");?></td>
  </tr>
  <tr>
    <td height="3"></td>
  </tr>
  <?php for($i=0;$i<$nn;$i++){?>
  <tr>
    <td><?php echo $rss->getItemTitle("rsslink",$i);?></td>
  </tr>
  <tr>
    <td><?php echo $rss->getItemDescription("rsstext",$i);?></td>
  </tr>
  <tr>
    <td><?php echo $rss->getItemPubdate("rssdate",$i);?></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php }?>
</table>

</body>
</html>
