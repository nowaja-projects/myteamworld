<?php
//versăo 2 para PHP 4
//autor: José Valente mailto:jcvalente@netvisao.pt
//2005 Portugal
include("feedReader.inc.php");

class RSSReader extends feedReader{

var $data;

function RSSReader($url){
	$this->setFeedUrl($url);
	$this->parseFeed();
	$this->data = $this->getFeedOutputData();
}

//********************* CHANNEL **********************************
function getChannelTitle(){
	return @$this->data['image']['title'];
}

function getChannelDescription($class=""){
	$html = "<span ";
	if(isset($class)){
		$html .= "class=\"".$class."\" ";
	}
	$html .= ">".$this->data['channel']['description'];
	$html .= "</span>";
	return $html;
}

function getChannelCopyright($class=""){
	if(isset($this->data['channel']['copyright'])){
		$html = "<span ";
		if(isset($class)){
			$html .= "class=\"".$class."\" ";
		}
		$html .= ">".$this->data['channel']['copyright'];
		$html .= "</span>";
		return $html;
	}
}

function getChannelLanguage($class=""){
	if(isset($this->data['channel']['language'])){
		$html = "<span ";
		if(isset($class)){
			$html .= "class=\"".$class."\" ";
		}
		$html .= ">".$this->data['channel']['language'];
		$html .= "</span>";
		return $html;
	}
}
//********************* IMAGE *****************************
function getImage(){
	if(isset($this->data['image']['link'])){
		$html = "<a href=\"".$this->data['image']['link']."\" target=\"_blank\">";
		$html .= "<img border=\"0\" ";
		if(isset($this->data['image']['height'])){
			$html .= "height=\"".$this->data['image']['height']."\" ";
		}
		if(isset($this->data['image']['width'])){
			$html .= "width=\"".$this->data['image']['width']."\" ";
		}
		$html .= "src=\"".$this->data['image']['url']."\" title=\"".$this->data['image']['title']."\" />";
		$html .= "</a>";
		return $html;
	}
}
//*********************** ITEM ****************************
function getItemTitle($item){
	return $this->data['item']['title'][$item];
}

function getItemLink($item){
	return $this->data['item']['link'][$item];
}

function getItemDescription($item){
  return $this->data['item']['description'][$item];
}

function getItemPubdate($item){
  if(isset($this->data['item']['pubdate']))
  {
    return $this->data['item']['pubdate'][$item];
  }
  else
  {
    return date('Y-m-d H:i:s');
  }
}

function getNumberOfNews(){
	return $this->getFeedNumberOfNodes();
}

}
?>