<?php
$_project['message']->loadMessages();
if($_project['message']->isWarning())
{
?>

<div class="msg warning">
    <?php foreach($_project['message']->getWarnings() as $text)
    {
        echo $text;
    }
    ?>
</div>

<?php
}

if($_project['message']->isDone())
{
?>

<div class="msg done">
    <?php foreach($_project['message']->getDones() as $text)
    {
        echo $text;
    }
    ?>
</div>

<?php
}
$_project['message']->clear();
$_project['message']->saveMessages();
?>