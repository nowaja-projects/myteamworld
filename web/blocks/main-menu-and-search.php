<?php /* ********** translated ************ */ ?>
<?php
if((!empty($logged_user->id) || (in_array($action, $not_logged_access))) && $action != 'registration') // uzivatel je prihlasen a neni to aktivace uctu
{
?>
    <nav role="navigation" class="cleaned">
        <a id="toggleMobileSearch" href="#"><span class="forBlind">Zobrazit vyhledávání</span></a>
        <a id="toggleMobileMenu" href="#"><span class="forBlind">Zobrazit menu</span></a>
        <ul id="mainMenu" class="cleaned">
            <li class="search"><a id="searchToggle" href="#" title=""><span class="hidden-sm hidden-xs">Hledat</span></a></li>
            <li class="<?php if($action == 'wall') echo ' selected';?>"><a href="<?php echo PATH_WEB_ROOT;?>wall/" title="">Nástěnka</a></li>
            <?php
            if($logged_user->hasTeam())
            {
                $team_modules = $logged_user->getActiveTeam()->getModules();

                if(is_array($team_modules))
                {
                    echo '<li class="open">';
                        echo '<span>Týmové stránky</span>';
                        echo '<ul>';
                        foreach($team_modules as $key => $module)
                        {
                            if(!isset($global_modules[$key]))
                            {
                                continue;
                            }
                            
                            if($logged_user->getActiveTeam()->userCanViewModule($key, $logged_user))
                            {
                            
                            ?>
                            <li<?php if($action == $key) echo ' class="selected"';?>><a href="<?php echo PATH_WEB_ROOT . $key;?>/" title="<?=$global_modules[$key]['name']?>"><?=$global_modules[$key]['name']?></a></li>
                             <?php
                            }
                        }
                        echo '</ul>';
                    echo '</li>';
                }
            }
            ?>
            
            <li class="open">
                <span>Ostatní</span>
                <ul class="cleaned">
                    <li><a href="<?php echo PATH_WEB_ROOT;?>team-registration/"><?=$tr->tr('Zaregistrovat nový tým')?></a></li>
                    <li><a id="helpButton" href="<?php echo PATH_WEB_ROOT;?><?=($logged_user->hasTeam() ? 'help' : 'help-without-team')?>/" title="Zobrazit nápovědu"><?=$tr->tr('Nápověda')?></a></li>
                </ul>
            </li>
        </ul><!-- #mainMenu -->

        <?php
        $notices = $logged_user->getNoticesCount();
        $messages = $logged_user->getMessagesCount();
        ?>
        <ul id="notification">
            <li>
                <a href="<?=PATH_WEB_ROOT?>messages/" title="<?=$tr->tr('Zprávy')?>" class="messages<?=($messages > 0 ? ' new' : '')?>"><span class="count"><?=$messages?></span></a>
            </li>
            <li>
                <a href="<?=PATH_WEB_ROOT?>notices/" title="<?=$tr->tr('Upozornění')?>" class="notification<?=($notices > 0 ? ' new' : '')?>"><span class="count"><?=$notices?></span></a>
            </li>
        </ul>    
        <?php
        unset($notices);
        unset($messages);
        ?>
    </nav>

    <div id="mainSearchOverlay" class="forBlind">
        <form action="<?=PATH_WEB_ROOT . 'search/'?>" id="mainSearch" method="get">
            <fieldset class="selectOne noRemove cleaned">
                <select id="mainSearchSelect"></select>
                <input id="mainSearchInput" type="hidden" name="search" value="<?=@$_GET['search']?>" />
                <button type="submit" class="button buttonC small forBlind" title="<?=$tr->tr('Vyhledat')?>"><?=$tr->tr('Vyhledat')?></button>
                <a class="close" href="#" title="<?=$tr->tr('Zavřít vyhledávání')?>"><!-- --></a>
            </fieldset>
        </form>
    </div>
<?php
}
?>