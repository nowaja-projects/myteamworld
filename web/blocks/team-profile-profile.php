<div id="contentA" class="profile team">
    <?php
    include(BLOCK_PATH . 'messages.php');
    $_project['message']->clear();
    $_project['message']->saveMessages();
    ?>

    <div class="contentAbox">

        <h2 class="headline">Profil týmu</h2>

        <div class="inner cleaned">
            <div class="left">
                <p class="picture">
                    <img src="<?=$team->getTeamLogo('big')?>" alt="<?=$team->getName()?>" width="200" height="200" />
                </p>
            </div><!-- .left -->

            <div class="right">
                <div class="head cleaned">
                    <ul class="name">
                        <?php if(!empty($team->prefix)) { ?>
                        <li class="item firstName"><?=$team->prefix?></li>
                        <?php } ?>
                        <li class="item surName"><?=$team->name?></li>
                    </ul>
                    <?php /* TODO
                    <ul class="rightPanel">
                        <li class="item message"><a href="#" title="Napsat zprávu týmu FBC Royal Gigolos">napsat zprávu</a></li>
                    </ul>
                    */ ?>
                </div>

                <div class="group">
                    <p class="entries cleaned"><span class="label">Sport:</span> <span class="input"><?=$gsports[$team->sport_id]?></span></p>
                    <?php if(!empty($team->category)) { ?>
                    <p class="entries cleaned"><span class="label">Kategorie:</span> <span class="input"><?=$team->category?></span></p>
                    <?php } ?>
                </div><!-- .group -->

                <div class="group">
                    <?php if(!empty($team->league)) { ?>
                    <p class="entries cleaned"><span class="label">Soutěž:</span> <span class="input"><?=$team->league?></span></p>
                    <?php } ?>
                    
                    <?php if(!empty($team->league_web)) { ?>
                    <p class="entries cleaned"><span class="label">Web soutěže:</span> <span class="input"><a href="<?=fix_link($team->league_web, true)?>" title="Odkaz na jiný web - <?=fix_link($team->league_web)?>"><?=fix_link($team->league_web, false, true)?></a></span></p>
                    <?php } ?>
                </div><!-- .group -->

                <div class="group">
                    <?php if(!empty($team->city)) { ?>
                    <p class="entries city cleaned"><span class="label">Město:</span> <span class="input"><?=str_replace(', ', ', <br />', $team->city)?></span></p>
                    <?php } ?>
                    <?/*<p class="entries cleaned"><span class="label">Kraj:</span> <span class="input">Jihomoravský kraj</span></p>*/?>
                </div><!-- .group -->

                <div class="group">
                    <?php if(!empty($team->founded)) { ?>
                    <p class="entries cleaned"><span class="label">Rok založení klubu:</span> <span class="input"><?=$team->founded?></span></p>
                    <?php } ?>
                    
                    <?php if(!empty($team->colors)) { ?>
                    <p class="entries cleaned"><span class="label">Klubové barvy:</span> <span class="input"><?=$team->colors?></span></p>
                    <?php } ?>
                </div><!-- .group -->

                <div class="group">
                    <p class="entries cleaned"><span class="label">Kontaktní osoba:</span> <span class="input"><?=$team->contact_name?></span></p>
                    <p class="entries cleaned"><span class="label">E-mail:</span> <span class="input"><a href="mailto:<?=$team->contact_email?>"><?=$team->contact_email?></a></span></p>
                    
                    <?php if(!empty($team->contact_phone)) { ?>
                    <p class="entries cleaned"><span class="label">Tel:</span> <span class="input"><?=$team->contact_phone?></span></p>
                    <?php } ?>
                    
                    <?php if(!empty($team->web)) { ?>
                    <p class="entries cleaned"><span class="label">Klubové stránky:</span> <span class="input"><a href="<?=fix_link($team->web, true)?>" title="Odkaz na jiný web - <?=fix_link($team->web)?>"><?=fix_link($team->web, false, true)?></a></span></p>
                    <?php } ?>
                </div><!-- .group -->                              
            </div><!-- .right -->
        </div><!-- .inner.cleaned -->
        
        <div class="navigation">
            <ul>
                <li class="first current"><a href="#teamphoto">Týmová fotografie</a></li> 
            </ul>
        </div>
        
        
        <div class="tabs">

            <!-- BOX 1 -->
            
            <div id="teamphoto" class="innerBox">
                <p class="teamPhoto">
                    <a class="picture fancybox" href="<?=$team->getTeamPhoto('big')?>"><img src="<?=$team->getTeamPhoto('medium')?>" alt="<?=$team->getName()?>" /></a>
                </p>
            </div>
        
        </div><!-- #tabs -->

    </div><!-- #contentAbox -->
</div><!-- #contentA .profile -->