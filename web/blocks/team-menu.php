<div id="teamMenu">
    <ul class="cleaned">
    <li<?php if($subaction == 'wall') echo ' class="selected"';?>><a href="<?=$team->getProfileLink('wall')?>" title="">Nástěnka</a></li>
<?php
    $team_modules = $team->getModules();
    if(is_array($team_modules))
    {
        foreach($team_modules as $key => $module)
        {
            if(!isset($global_modules[$key]))
            {
                continue;
            }
            
            if($team->userCanViewModule($key, $logged_user))
            {
    ?>
            <li<?=($subaction == $key) ? ' class="selected"' : ''?>><a href="<?=$team->getProfileLink($key)?>" title="<?=$global_modules[$key]['name']?>"><?=$global_modules[$key]['name']?></a></li>
    <?php
            }
        }
    }
?>
  </ul>

  <?php

include(BLOCK_PATH . 'print-messagess.php');
?>
</div>

