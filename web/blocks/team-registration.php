<?php /* ********** translated ************ */ ?>

<div id="teamRegistration" class="cleaned">

    <h2 class="blockTitle"><?=$tr->tr('Vstupte do týmu')?></h2>

    <div class="content">

        <p class="text"><?=$tr->tr('Pokud se zde váš tým již nachází, najděte si ho ve vyhledávání a zažádejte o vstup do týmu. Pokud váš tým ještě není vytvořený, zaregistrujte ho!')?></p>
        <p class="link"><a href="<?php echo PATH_WEB_ROOT;?>team-registration/" class="button buttonA small"><?=$tr->tr('zaregistrovat nový tým')?></a></p>

    </div><!-- .content -->

</div><!-- #recommend -->