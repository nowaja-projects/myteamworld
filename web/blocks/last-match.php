<?php

$last = $team->getLastMatch();

if($last)
{
    $result = unserialize($last['result']);
    $info = unserialize($last['info']);

    if($last['team_id'] == $team->id)
    {

        $res_home = $result['home'];
        $res_away = $result['away'];
        $home = $team;

        if(empty($last['oponent_id']))
        {
            $away = false;
        }
        else
        {
            $away = TeamDAO::get($last['oponent_id']);
        }
    }
    elseif($last['oponent_id'] == $team->id)
    {
        $res_home = $result['away'];
        $res_away = $result['home'];

        $away = $team;
        $home = TeamDAO::get($last['team_id']);
    }
?>

<div id="lastMatch">

    <h2 class="blockTitle">Poslední zápas</h2>

    <div class="content cleaned">

        <div class="team-1 cleaned">
            <a class="logo" href="<?=$home->getProfileLink()?>" title="Zobrazit profil týmu <?=$home->getName()?>"><img src="<?=$home->getTeamLogo('medium')?>" alt="<?=$home->getName()?>" /></a><br />
            <h3 class="name"><a href="<?=$home->getProfileLink()?>" title="Zobrazit profil týmu <?=$home->getName()?>"><?=$home->getName()?></a></h3>
        </div>

        <p class="result">
            <a href="<?=($team->getProfileLink('event-detail') . $last['id'] . '-' . $last['team_id'] . '/')?>" title="Zobrazit detail zápasu"><?=$res_home['final']?>:<?=$res_away['final']?><?php
            if(!empty($result['isPenalties']))
            {
                echo ' pen';
            }
            elseif(!empty($result['isShootout']))
            {
                echo ' sn';
            }
            elseif(!empty($result['isOvertime']))
            {
                echo ' pp';
            }
            ?></a>
            <?php /*
            <br />
            <?php
            $periods = array();
            for($i = 1; $i <= $result['periods']; $i++)
            {
                if(isset($res_home['period'.$i]) && isset($res_away['period'.$i]))
                {
                    $periods[] = $res_home['period'.$i] . ':' . $res_away['period'.$i];
                }

            }
            ?>
            <span><?=('(' . implode('), (', $periods) . ')')?></span>
            */ ?>
        </p>
        <?php
        if($away)
        {
        ?>
        <div class="team-2 cleaned">
            <a class="logo" href="<?=$away->getProfileLink()?>" title="Zobrazit profil týmu <?=$away->getName()?>"><img src="<?=$away->getTeamLogo('medium')?>" alt="<?=$away->getName()?>" /></a><br />
            <h3 class="name"><a href="<?=$away->getProfileLink()?>" title="Zobrazit profil týmu <?=$away->getName()?>"><?=$away->getName()?></a></h3>
        </div>
        <?php
        }
        else
        {
        ?>
        <div class="team-2 cleaned">
            <img class="logo" src="<?=$home->getTeamLogo('medium', '/event'.$last['id'].'/')?>" alt="<?=$info[0]['team_name']?>" /><br />
            <h3 class="name"><?=$info[0]['team_name']?></h3>
        </div>
        <?php
        }
        ?>

    </div><!-- .content -->

</div><!-- #lastMatch -->

<?php
}
?>