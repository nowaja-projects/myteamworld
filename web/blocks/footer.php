<?php /* ********** translated ************ */ ?>

<footer id="pageFooter">
    <div class="container cleaned">
    	
        <p class="text">Copyright © 2015, myteamworld.com</p>
        <ul class="links cleaned">
        	<li><a href="/terms/" title="<?=$tr->tr('Zobrazit podmínky použití serveru myteamworld.com')?>"><?=$tr->tr('Podmínky použití')?></a></li>
            <?php if(!empty($logged_user->id)) { ?><li><a href="<?php echo PATH_WEB_ROOT;?><?=($logged_user->hasTeam() ? 'help' : 'help-without-team')?>/" title="Zobrazit nápovědu"><?=$tr->tr('Nápověda')?></a></li><?php } ?>
            <li><a href="<?=PATH_WEB_ROOT?>contact/" title="Kontaktujte nás"><?=$tr->tr('Kontakt')?></a></li>
        </ul>

    </div><!-- .container -->
</footer> <!-- #pageFooter -->