<?php /* ********** translated ************ */ ?>

<div id="recommend" class="cleaned">

    <h2 class="blockTitle"><?=$tr->tr('Doporučit ostatním')?></h2>

    <div class="content">

        <p class="text"><?=$tr->tr('Pozvěte další týmy, své přátelé, spoluhráče i soupeře a mějte o všem dokonalý přehled!')?></p>

        <form method="post" action="">
            <fieldset class="cleaned">
                <p class="msg error noClose">Zadejte validní e-mail!</p>
                <input class="text" type="text" value="" name="recommend-email" placeholder="<?=$tr->tr('Zadejte e-mail')?>" title="<?=$tr->tr('Zadejte e-mail osoby, které chcete myteamworld.com doporučit')?>" />
                <button class="button buttonA small" id="recommendButton" type="submit" name="" value="" title="<?=$tr->tr('Pozvat')?>"><?=$tr->tr('pozvat')?></button>
            </fieldset>
        </form>

    </div><!-- .content -->

</div><!-- #recommend -->