<?php
if(!isset($is_update))
{
    $is_update = false;
}

$event_data['team'] = sprintf('<p class="title-border">
        <strong>Zadejte název týmu soupeře</strong><small> -  pokud se tým na myteamworld.com již nachází, začněte psát a vyberte ho.</small>
    </p>
    <p class="entries teamName selectOne">
    	<label class="forBlind">Název týmu</label>
    	<select id="teamSearch" class="" name="event[team]"></select>
        <input id="hiddenTeamName" type="hidden" name="event[team_name]" value=""/>
        <span class="errorMsg">Vyplňte, prosím, název soupeřova týmu.</span>
    </p>'
);

$event_data['button1'] = '<p class="buttonBox">
<a class="button buttonA" href="#" id="eventNextStep">uložit a pokračovat</a>
</p>';

$event_data['button2'] = '<p class="buttonBox">
<a class="button buttonA uploadLogo" href="#" id="eventNextStep">uložit a pokračovat</a>
</p>';

$event_data['logo'] = '<p class="title-border">
    <strong>Nahrajte logo týmu soupeře</strong><small> -  pokud máte logo soupeřova týmu, nahrajte ho.</small>
</p>
<p class="entries teamLogo">
	<label class="forBlind">Logo týmu</label>
	<input class="input" type="file" name="team_logo" />
</p>';

$event_data['invite'] = '<p class="title-border">
    <strong>Zadejte e-mail týmu</strong><small> -  na zadaný e-mail bude zaslána pozvánka k registraci týmu na myteamworld.com.</small>
</p>
<p class="entries email">
	<label class="forBlind">E-mail týmu</label>
	<input class="input text" type="text" name="event[invite_email]" />
</p>';


$team_seasons = $logged_user->getActiveTeam()->getSeasons();
$event_data['name'] = '<p class="title-border">
        			        <strong>Vyberte sezónu</strong><small> -  použije se pro zapsání statistik do sezóny, filtry atd.</small>
        			    </p>
                        <p class="entries season">
                        <select name="event[season_id]">';
$event_data['name'] .= print_season_select($team_seasons, $logged_user->getActiveTeam()->getActiveSeason());
$event_data['name'] .= '</select></p>';
$event_data['name'] .= '<p class="title-border">
    <strong>Zadejte název události</strong><small> -  např.: ligový zápas, 7. kolo</small>
</p>
<p class="entries name">
	<label class="forBlind">Název události</label>
	<input class="input text" type="text" name="event[name]" maxlength="25" />
    <span class="errorMsg">Vyplňte, prosím, název události.</span>
</p>';


$event_data['place'] = '<p class="title-border">
    <strong>Zadejte místo události</strong><small> -  např.: Název haly, Město</small>
</p>
<p class="entries place">
	<label class="forBlind">Místo události</label>
	<input class="input text" type="text" name="event[place]" maxlength="25" />
    <span class="errorMsg">Vyplňte, prosím, místo události.</span>
</p>';

$event_data['repeat'] = '<span class="right cleaned">
        <span class="containerBox cleaned">
            <input id="repeat" class="" type="checkbox" '.(!empty($event->info[0]['repeat']) ? 'checked="checked" ': '').'value="1" name="event[repeat]" />
            <label for="repeat">opakovat událost</label>
        </span>

        <span id="repeatBox" class="forBlind">
            <label>do</label>
            <input class="input text repeatDate datepicker" type="text" name="event[repeat_to]" placeholder="0000-00-00" />

            <select class="repeatSelect" name="event[repeat_interval]">
                <option value="day">den</option>
                <option value="week">týden</option>
                <option value="month">měsíc</option>
                <option value="year">rok</option>
            </select>
        </span>
    </span>';

$event_data['date'] = '<p class="title-border">
    <strong>Datum a čas události</strong>'.(!$is_update || !empty($event->info[0]['repeat']) ? '<small> -  událost můžete nechat opakovat do určitého dne např. každý týden.</small>' : '') . '
</p>
<p class="entries dateAndTime cleaned">
    <span class="left cleaned">
        <label class="forBlind">Datum události</label>
        <input value="'.($is_update ? date('Y-m-d', strtotime($event->start)) : '').'" class="input text date datepicker" type="text" name="event[start_date]" autocomplete="off" placeholder="0000-00-00" />

        <label>od</label>
        <input class="input text from" value="'.($is_update ? date('H:i', strtotime($event->start)) : '').'" type="text" name="event[start_time]" placeholder="00:00" />

        <label>do</label>
        <input class="input text to" value="'.($is_update ? date('H:i', strtotime($event->end)) : '').'" type="text" name="event[end_time]" placeholder="00:00" />
    </span>

    '.
    (!$is_update ? $event_data['repeat'] : '')
    .'
    <span class="errorMsg">Vyplňte, prosím, správně datum a čas.</span>
</p>';


$event_data['date_team_action'] = '<p class="title-border">
    <strong>Datum a čas události</strong><small> -  událost můžete nechat opakovat např. každé pondělí.</small>
</p>
<p class="entries dateAndTime cleaned">
    <span class="left cleaned">
        <span class="inner top cleaned">
            <label class="first">začátek</label>
            <input class="input text date datepicker" value="'.($is_update ? date('Y-m-d', strtotime($event->start)) : '').'" type="text" name="event[start_date]" autocomplete="off" placeholder="0000-00-00" />

            <label>od</label>
            <input class="input text from" type="text" value="'.($is_update ? date('H:i', strtotime($event->start)) : '').'" name="event[start_time]" placeholder="00:00" />
        </span>

        <span class="inner bottom cleaned">
            <label class="first">konec</label>
            <input class="input text date datepicker" value="'.($is_update ? date('Y-m-d', strtotime($event->end)) : '').'" type="text" name="event[end_date]" autocomplete="off" placeholder="0000-00-00" />

            <label>do</label>
            <input class="input text to" type="text" value="'.($is_update ? date('H:i', strtotime($event->end)) : '').'" name="event[end_time]" placeholder="00:00" />
        </span>
    </span>
    '.
        (!$is_update || empty($event->info[0]['repeat']) ? $event_data['repeat'] : '')
    .'
    <span class="errorMsg">Vyplňte, prosím, správně datum a čas.</span>
</p>';


$attend_team_key = 'attend_team';
$attend_key = 'attend';
if($is_update)
{
    // pokud jsme hostujici tym a editujeme udalost
    if($event->oponent_id == $logged_user->getActiveTeam()->id)
    {
        $attend_team_key .= '_away';
        $attend_key .= '_away';
    }
}

if(isset($event) && $event instanceof Event)
{
	$info = $event->info;

	if(!is_array($info))
	{
		$info = unserialize($event->info);
	}
}
else
{
	$info = array();
}

$checked = 'checked="checked" ';
if(isset($info[0]) && empty($info[0][$attend_team_key]))
{
    $checked = '';
}

if(!isset($info[0][$attend_key]) || !is_array($info[0][$attend_key]))
{
    $info[0][$attend_key] = array();
}
$event_data['attend'] = '<p class="title-border">
    <strong>Docházka</strong><small> -  pokud událost není pro celý tým, vyberte pouze uživatele, kterých se týká.</small>
</p>

<p class="entries roster">
	<span class="containerBox cleaned">
		<input id="allMembers" class="" type="checkbox" '.$checked.'name="event['. $attend_team_key .']" value="1" />
    	<label for="allMembers">vytvořit událost pro celý tým</label>
    	<a href="#rosterAttendance" class="rosterAttendance popUp" style="display: none"><!-- --></a>
    </span>

    <span class="'.(!empty($checked) ? 'forBlind ' : '').'info">Celkem máte vybráno členů: <strong class="count">'.(!empty($checked) ? 'celý tým' : count($info[0][$attend_key])).'</strong> <a href="#rosterAttendance" class="icon edit popUp">upravit</a></span>

    <span class="forBlind members">
    ';
        $players = $logged_user->getActiveTeam(true)->getPlayers(true, true);
        
        if(count($players))
        {
            $checkboxs = array();
            foreach($players as $data)
            {
                // jen hraci ze soupisky se budou zobrazovat
                if(empty($data['position_id']))
                {
                    continue;
                }
                unset($data['id']);
                $player = new User($data);

                $checked = ' checked="checked"';
                if($is_update && isset($info[0][$attend_key]) && in_array($player->id, array_keys($info[0][$attend_key])))
                {
                    $checked = ' checked="checked"';
                }
                elseif(isset($info[0]) && !empty($info[0][$attend_team_key]))
                {
                    $checked = ' checked="checked"';
                }
                elseif($is_update)
                {
                    $checked = '';
                }
                
                $checkboxs[] = '<input class="'.friendly_url($player->getName() . '-' . $player->id).'" type="checkbox" '.$checked.'name="event['.$attend_key.']['.$player->id.']" value="'.getRecipientHash($player->id).'" />
                <label>'.$player->getName().'</label>';
            }
            $event_data['attend'] .= implode('<br />', $checkboxs);
        }
            	
    	
    $event_data['attend'] .=  '</span>
    <span class="errorMsg">Vyberte, prosím, alespoň jednoho člena.</span>
</p>
<div class="forBlind">
	<div id="rosterAttendance" class="fancybox-popup">
	    <h3 class="title">Vyberte uživatele</h3>';
    
        //$players = $logged_user->getActiveTeam(true)->getPlayers(true, true);
        if(count($players))
        {
            $event_data['attend'] .= '<ul class="list">';
            $i = 0;
            $positions = SportDAO::getPositions($logged_user->getActiveTeam()->sport_id);
            foreach($players as $data)
            {
                // jen hraci ze soupisky se budou zobrazovat
                if(empty($data['position_id']))
                {
                    continue;
                }
                unset($data['id']);
                $player = new User($data);
                $class = '';
                if($i++ == 0)
                {
                    $class = ' first';
                }
            
                $checked = ' ';
                if($is_update && isset($info[0]) && !empty($info[0][$attend_team_key]))
                {
                    $checked = ' ';
                }
                elseif($is_update && isset($info[0][$attend_key]) && in_array($player->id, array_keys($info[0][$attend_key])))
                {
                    $checked = ' checked';
                }
            
            $event_data['attend'] .= '<li class="item'.$class.' cleaned">
            	<p class="fakeCheckboxBox"><a href="#" class="fakeCheckbox'.$checked.'" data-id="'.friendly_url($player->getName() . '-' . $player->id).'"><!-- --></a></p>
                <p class="picture"><img width="46" height="46" alt="'.$player->getName().'" src="'.$player->getUserImage('medium').'" /></p>
                <h5 class="name">'.$player->getName().'</h5>
                <h5 class="post">'.$positions[$data['position_id']]->name.'</h5>
            </li>';
            }
            $event_data['attend'] .= '</ul>';
        }
    $event_data['attend'] .= '<p class="link"><a href="#" title="hotovo, zavřít">hotovo</a></p></div><!-- #rosterAttendance -->
</div><!-- .forBlind -->';

$event_data['save'] = '<p class="buttonBox">
    <button type="submit" class="button buttonA" id="eventSubmit">uložit událost</button>
</p>';



/**
 *pokud editujeme udalost, tak musime data vypsat trochu jinak
 * 
 */
if($is_update)
{
    /**
     * Název týmu soupeře pro editaci týmu
     */ 
    $event_team = getEventOponentTeam($event);
    
    $event_data['team'] = sprintf('<p class="title-border">
            <strong>Název týmu soupeře</strong>%s
        </p>
        <p class="entries teamName selectOne">
        	<label class="forBlind">Název týmu</label>
            <input type="text" class="input text" required="required" '.($event_team instanceof Team ? 'disabled="disabled" ' : '').'name="event[team_name]" value="%s"/>
        </p>',
        ($event_team instanceof Team ? '<small> - název soupeře nemůžete změnit, protože se již nachází na myteamworld.com</small>' : '<small> - můžete změnit název týmu</small>'),
        ($event_team instanceof Team ? $event_team->getName() : (isset($info[0]['team_name']) ? $info[0]['team_name'] : ''))
    );
    
    
    
    
    // máme vybraný tým který je na MTW
    if($event_team !== false)
    {
        $event_data['logo'] = '';
    }
    else
    {
        $logo_src = $team->getTeamLogo('small', '/event'.$event->id.'/');
        $event_data['logo'] = sprintf('<p class="title-border">
                <strong>Nahrajte logo týmu soupeře</strong><small> -  pokud máte logo soupeřova týmu, nahrajte ho.</small>
            </p>
            <p class="entries teamLogo">
            	<label class="forBlind">Logo týmu</label>
                <img src="%s" />
            	<input class="input" type="file" name="team_logo" />
            </p>',
            $logo_src
        );
    }
    
    
    $team_seasons = $logged_user->getActiveTeam()->getSeasons();
    $event_data['name'] = '<p class="title-noborder">
            			        <strong>Vyberte sezónu, do které se má událost přiřadit</strong>
            			    </p>
                            <p class="entries season">
                            <select name="event[season_id]">';

    
    $event_data['name'] .= print_season_select($team_seasons, $event->season_id);
    $event_data['name'] .= '</select></p>';
    $event_data['name'] .= sprintf('<p class="title-border">
        <strong>Zadejte název události</strong><small> -  např.: ligový zápas, 7. kolo</small>
    </p>
    <p class="entries name">
    	<label class="forBlind">Název události</label>
    	<input class="input text" type="text" name="event[name]" maxlength="25" value="%s" />
        <span class="errorMsg">Vyplňte, prosím, název události.</span>
    </p>',
    $info[0]['name']
    );
    
    
    $event_data['place'] = sprintf('<p class="title-border">
        <strong>Zadejte místo události</strong><small> -  např.: Název haly, Město</small>
    </p>
    <p class="entries place">
    	<label class="forBlind">Místo události</label>
    	<input class="input text" type="text" name="event[place]" maxlength="25" value="%s" />
        <span class="errorMsg">Vyplňte, prosím, místo události.</span>
    </p>',
    $info[0]['place']
    );


    // pokud editujeme udalost, tak jiz nemuzeme opakovat
    $event_data['repeat'] = '';

}