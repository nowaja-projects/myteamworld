<?php /* ********** translated ************ */ ?>

<div id="teamOptions" class="cleaned">

    <div class="content">
        <ul class="options cleaned" id="teamOptionLinks">
            <?php 

            if($team->isPlayer($logged_user))
            {
            ?>

            <li class="item">
                <a class="icon leave leaveTeam" href="<?=$team->getProfileLink()?>?leave_team=yes" title="<?=$tr->tr('Odejít z týmu')?> <?=$team->getName()?>"><?=$tr->tr('odejít z týmu')?></a>
            </li>
            
            <?php 
            }
            else
            {
                $data = array(
                    'team_id' => $team->id,
                    'user_id' => $logged_user->id
                );
                // kouknem jestli uz uzivatel nazazadal
                $oldinvite = RosterInvites::getRequestByTeam($data);

                $canSendRequest = false;
                // pokud mu byla posledni zadost zamitnuta, nebo jeste zadnou neposlal, tak mu dovolime poslat
                if((count($oldinvite) && $oldinvite['status'] != 'requested') || !count($oldinvite))
                {
                    $canSendRequest = true;
                }

                if(!$team->isFan($logged_user->id) && !$team->isObserver($logged_user->id))
                {
                    require(BLOCK_PATH.'team-links-visitor.php');
                }
                elseif($team->isFan($logged_user->id))
                {
                    require(BLOCK_PATH.'team-links-fan.php');                
                }
                elseif($team->isObserver($logged_user->id))
                {
                    require(BLOCK_PATH.'team-links-observer.php');    
                }

                // nacita se v tech jednotlivych blocich
                echo $linksHtml;
            }

            
            /* TODO
            <li class="item">
                <a class="icon friendlyMatch" href="#" title="<?=$tr->tr('Domluvit s týmem')?> <?=$team->getName()?> <?=$tr->tr('přátelské utkání')?>"><?=$tr->tr('domluvit přátelské utkání')?></a>
            </li>
            <li class="item">
                <a class="icon sendMessage" href="#" title="<?=$tr->tr('Napsat týmu')?> <?=$team->getName()?> <?=$tr->tr('zprávu')?>"><?=$tr->tr('napsat týmu zprávu')?></a>
            </li>
            */ ?>
        </ul>
        
    </div><!-- .content -->

</div><!-- #teamOptions --> 