<?php

if($_project['message']->isWarning())
{
?>
<div class="msg error">
	
    <?php foreach($_project['message']->getWarnings() as $text) { ?>
    <?php echo $text;?>
    <?php } ?>
	
</div> <!-- #msg -->
<?php
}
?>

<?php
if($_project['message']->isDone())
{
?>
<div class="msg done">
	
    <?php foreach($_project['message']->getDones() as $text) { ?>
    <?php echo $text;?>
    <?php } ?>
	
</div> <!-- #msg -->
<?php
}

?>