<header id="pageHeader">
    <div class="container">

        <p id="logo">
            <a title="Přejít na úvodní stránku" href="<?=PATH_WEB_ROOT?>wall/">
                <img class="visible-lg" src="<?php echo IMAGES_PATH;?>logo-myteamworld.png" alt="Logo myteamworld.com" width="210" height="33" />
                <img class="hidden-lg" src="<?php echo IMAGES_PATH;?>logo-myteamworld-mobile.png" alt="Logo myteamworld.com" width="41" height="33" />
            </a>
        </p>

        <h1 class="pageTitle forBlind"><?=!empty($title) ? $title:''?></h1>

        <?php 
        if(!empty($logged_user->id))
        {
            require_once(BLOCK_PATH . 'main-menu-and-search.php'); 
        }
        ?>

        <?php
        // jestlize nejsem na svem aktivnim tymu, tak zobraz tymove menu 
        if($action == 'team-profile'
        &&
        ($logged_user->getActiveTeam()->id > 0  && $team->id != $logged_user->getActiveTeam()->id) || 
        ($logged_user->getActiveTeam()->id == 0 && !empty($team->id)))
        { 
            require_once(BLOCK_PATH . 'team-menu.php');
        } 
        ?>

        <?php
        /* Uzivatel je prihasen */ 
        if(!empty($logged_user->id) && $action != 'registration') // uzivatel je prihlasen a neni to aktivace uctu
        {
        ?>
        <div id="user" class="cleaned">
            <div class="left">
                <a class="picture" href="<?=$logged_user->getProfileLink();?>" title="<?=$tr->tr('Zobrazit profil')?> <?php echo $logged_user->getName();?>">
                    <img src="<?=$logged_user->getUserImage('medium')?>" alt="<?php echo $logged_user->getName();?>" width="30" height="30" />
                </a>
            </div><!-- .left -->

            <div class="right">
                <h2 class="name"><a href="<?=$logged_user->getProfileLink();?>" title="<?=$tr->tr('Zobrazit profil')?> <?=$logged_user->getName();?>"><?=$logged_user->getName();?></a></h2>

                <h3 class="team">
                <?php
                if($logged_user->getActiveTeam() instanceof Team && $logged_user->activeTeam->id > 0)
                {
                ?>
                <a href="<?=$logged_user->activeTeam->getProfileLink()?>" title="<?=$tr->tr('Zobrazit profil týmu')?> <?=$logged_user->activeTeam->getName()?>"><?=$logged_user->activeTeam->getName()?></a>
                <?php
                }
                else
                {
                ?>
                    Volný hráč
                <?php
                }
                ?>
                </h3>

                <ul class="moreOptions" title="Zobrazit další nastavení">
                    <li>
                        <span class="forBlind">Další nastavení</span>
                        <ul>
                            <?php if(count($logged_user->getTeamList(true)) > 0) { ?>
        
                                <?php
                                $i = 0;
                                foreach($logged_user->getTeamList() as $switchTeam)
                                {
                                    if($switchTeam->id != $logged_user->activeTeam->id)
                                    {
                                ?>
                                <li>
                                    <a href="#" id="team_<?=$switchTeam->id?>" class="switchTeam" title="<?=$tr->tr('Nastavit tým')?> <?=$switchTeam->getName()?> <?=$tr->tr('jako aktivní')?>">
                                        <img src="<?=$switchTeam->getTeamLogo('small')?>" alt="<?=$switchTeam->getName()?>" width="26" height="26" />
                                        <span class="text"><?=$switchTeam->getName()?></span>
                                    </a>
                                </li>
                                <?php
                                        $i++;
                                    }

                                    /*
                                    if(count($logged_user->teams) > 4 && $i == 2)
                                    {
                                        break;
                                    }
                                    */
                                }
                                /*
                                if(count($logged_user->teams) > 4)
                                {
                                ?>
                                <li>
                                    <a href="<?=PATH_WEB_ROOT . 'ajax.php?getSwitchTeams=1'?>" class="popUpAjax cleaned" title="<?=$tr->tr('Zobrazit všechny týmy')?>">
                                        <span class="text"><?=$tr->tr('Všechny týmy')?></span>
                                        <img src="<?=IMAGES_PATH;?>more-teams-blank.jpg" alt="<?=$tr->tr('Všechny týmy')?>" width="26" height="26" />
                                    </a>
                                </li>
                                <?php
                                }
                                */
                                ?>

                            <?php } ?>
                            <li><a href="<?=PATH_WEB_ROOT;?>edit-user-profile/" title="<?=$tr->tr('upravit profil')?>"><?=$tr->tr('Upravit profil')?></a></li>
                            <li><a href="<?=PATH_WEB_ROOT;?>?logout=1" title="<?=$tr->tr('odhlásit se')?>"><?=$tr->tr('Odhlásit se')?></a></li>
                        </ul>
                    </li>
                </ul>

            </div><!-- .right -->
        </div><!-- #user -->

        <?php 
        }
        /* Uzivatel neni prihasen */
        else
        {
        ?>

            <ul id="secondMenu">
                <?php
                if($action == 'main')
                {
                ?>
                    <li><a href="<?=PATH_WEB_ROOT?>what-is-myteamworld/"><?=$tr->tr('Co je Myteamworld?')?></a></li>
                <?php
                }
                else
                {
                ?>
                    <li><a href="<?=PATH_WEB_ROOT?>"><?=$tr->tr('Zpět na úvodní stránku')?></a></li>
                <?php
                }
                ?>
            </ul>

        <?php 
        }
        ?>

    </div><!-- .container -->
</header>