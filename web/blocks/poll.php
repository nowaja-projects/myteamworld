<div id="poll">

    <h2 class="blockTitle">Anketa</h2>

    <div class="content">

        <p class="text">Má šanci tento projekt na úspěch? Nebo to máme rovnou zabalit...</p>

        <p class="option">
            <a href="#" title="">Ano, je to vynikající nápad!</a><br />
            <img src="<?php echo IMAGES_PATH;?>poll-bar.png" width="70" alt="Poll Bar" /><span class="result">70%</span>
        </p>

        <p class="option">
            <a href="#" title="">Ne, můžete to rovnou zabalit...</a><br />
            <img src="<?php echo IMAGES_PATH;?>poll-bar.png" width="30" alt="Poll Bar" /><span class="result">30%</span>
        </p>

        <p class="count">Hlasovalo: 1286 uživatelů</p>

    </div><!-- .content -->

</div><!-- #poll -->