<?php /* ********** translated ************ */ ?>

<?php
/* TODO Odkazy na profil registrovaneho uzivatele zkontrolovat jestli neni prasarna */?>
<div id="header">
    <div class="centered cleaned">
        <div id="user" class="cleaned">

            <div class="left">
                <a class="picture picture_medium" href="<?=$logged_user->getProfileLink();?>" title="<?=$tr->tr('Zobrazit profil')?> <?php echo $logged_user->getName();?>">
                    <img src="<?=$logged_user->getUserImage('medium')?>" alt="<?php echo $logged_user->getName();?>" width="46" height="46" />
                </a>
            </div><!-- .left -->

            <div class="right">
                <h2 class="name"><a href="<?=$logged_user->getProfileLink();?>" title="<?=$tr->tr('Zobrazit profil')?> <?=$logged_user->getName();?>"><?=$logged_user->getName();?></a></h2>

                <h3 class="team">
                <?php
                if($logged_user->getActiveTeam() instanceof Team && $logged_user->activeTeam->id > 0)
                {
                ?>
                <a href="<?=$logged_user->activeTeam->getProfileLink()?>" title="<?=$tr->tr('Zobrazit profil týmu')?> <?=$logged_user->activeTeam->getName()?>"><?=$logged_user->activeTeam->getName()?></a>
                <?php
                }
                else
                {
                ?>
                    Volný hráč
                <?php
                }
                ?>
                </h3>
            </div><!-- .right -->

            <?php
            $notices = $logged_user->getNoticesCount();
            $messages = $logged_user->getMessagesCount();
            ?>
            <div id="options">
                <a href="<?=PATH_WEB_ROOT;?>edit-user-profile/" title="<?=$tr->tr('upravit profil')?>"><?=$tr->tr('upravit profil')?></a><br />
                <a href="<?=PATH_WEB_ROOT;?>?logout=1" title="<?=$tr->tr('odhlásit se')?>"><?=$tr->tr('odhlásit se')?></a><br />
                <p id="notification">
                    <a href="<?=PATH_WEB_ROOT?>messages/" title="<?=$tr->tr('Zprávy')?>" class="messages<?=($messages > 0 ? ' new' : '')?>"><?=$messages?></a><br />
                    <a href="<?=PATH_WEB_ROOT?>notices/" title="<?=$tr->tr('Upozornění')?>" class="notification<?=($notices > 0 ? ' new' : '')?>"><?=$notices?></a>
                </p><!-- #notification -->
            </div><!-- #options -->
            <?php
            unset($notices);
            unset($messages);
            ?>

        </div><!-- #user -->

        <?php if(count($logged_user->getTeamList(true)) > 0) { ?>

        <div id="userTeam" class="cleaned">
            <?php
            $i = 0;
            foreach($logged_user->getTeamList() as $switchTeam)
            {
                if($switchTeam->id != $logged_user->activeTeam->id)
                {
            ?>
            <a href="#" id="team_<?=$switchTeam->id?>" class="switchTeam cleaned" title="<?=$tr->tr('Nastavit tým')?> <?=$switchTeam->getName()?> <?=$tr->tr('jako aktivní')?>">
                <span class="text"><?=$switchTeam->getName()?></span>
                <img src="<?=$switchTeam->getTeamLogo('small')?>" alt="<?=$switchTeam->getName()?>" width="26" height="26" />
            </a>
            <?php
                    $i++;
                }

                if(count($logged_user->teams) > 4 && $i == 2)
                {
                    break;
                }
            }

            if(count($logged_user->teams) > 4)
            {
            ?>
            <a href="<?=PATH_WEB_ROOT . 'ajax.php?getSwitchTeams=1'?>" class="popUpAjax cleaned" title="<?=$tr->tr('Zobrazit všechny týmy')?>">
                <span class="text"><?=$tr->tr('Všechny týmy')?></span>
                <img src="<?=IMAGES_PATH;?>more-teams-blank.jpg" alt="<?=$tr->tr('Všechny týmy')?>" width="26" height="26" />
            </a>
            <?php
            }
            ?>

        </div><!-- #userTeam -->

        <?php } ?>

    </div><!-- .centered -->
</div><!-- #header -->