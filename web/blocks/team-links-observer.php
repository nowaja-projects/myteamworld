<?php
// ve vyhledavani
if((isset($foundTeam) && $foundTeam instanceof Team) || (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_GET['search'])))
{
    if(!isset($foundTeam) || !($foundTeam instanceof Team))
    {
        $foundTeam = $team;
    }
    
    $linksHtml = '
        <a class="button buttonC icon becameFan" data-id="'.$foundTeam->getFanAttr().'" href="#" title="'.$tr->tr('stát se fanouškem týmu').' '.$foundTeam->getName().' '.$tr->tr('a získávat novinky o dění v týmu').' '.$foundTeam->getName().' '.$tr->tr('na svou nástěnku').'">'.$tr->tr('stát se fanouškem týmu').'</a>
        <a class="button buttonC icon removeObserver" data-id="'.$foundTeam->getObserverAttr().'" href="#" title="' . $tr->tr('Nechci získávat novinky o dění v týmu') . ' ' . $foundTeam->getName() . ' ' . $tr->tr('na svou nástěnku') . '">' . $tr->tr('nechci sledovat dění v týmu') . '</a>
    ';
}
else
{
    $linksHtml = '
    <li class="item removeTeamLink">
        <a class="icon becameFan" data-id="'.$team->getFanAttr().'" href="#" title="'.$tr->tr('stát se fanouškem týmu').' '.$team->getName().' '.$tr->tr('a získávat novinky o dění v týmu').' '.$team->getName().' '.$tr->tr('na svou nástěnku').'">'.$tr->tr('stát se fanouškem týmu').'</a>
    </li>
    <li class="item removeTeamLink">
        <a class="icon removeObserver" data-id="'.$team->getObserverAttr().'" href="#" title="' . $tr->tr('Nechci získávat novinky o dění v týmu') . ' ' . $team->getName() . ' ' . $tr->tr('na svou nástěnku') . '">' . $tr->tr('nechci sledovat dění v týmu') . '</a>
    </li>';

    if(!empty($team->show_request))
    {
        if(!isset($canSendRequest))
        {
            $data = array(
                'team_id' => $team->id,
                'user_id' => $logged_user->id
            );
            // kouknem jestli uz uzivatel nazazadal
            $oldinvite = RosterInvites::getRequestByTeam($data);
            
            $canSendRequest = false;
            // pokud mu byla posledni zadost zamitnuta, nebo jeste zadnou neposlal, tak mu dovolime poslat
            if((count($oldinvite) && $oldinvite['status'] != 'requested') || !count($oldinvite))
            {
                $canSendRequest = true;
            }
        }

        if($canSendRequest)
        {
            $linksHtml .= '<li class="item removeTeamLink">
                <a class="icon becameMember invite" data-id="'.$team->getEntryAttr().'" href="#" title="'.$tr->tr('Zažádat administrátory týmu').' '.$team->getName().' '.$tr->tr('o vstup do týmu').'">'.$tr->tr('zažádat o vstup do týmu').'</a>
            </li>';
        }
        else
        {
            $linksHtml .= '<li class="item removeTeamLink">
                <a class="icon removeMember leave" data-id="'.$team->getEntryAttr().'" href="#" title="'.$tr->tr('Zrušit žádost o vstup do týmu').' '.$team->getName().'">'.$tr->tr('zrušit žádost o vstup').'</a>
            </li>';
        }
    }
}
?>