<?php /* ********** translated ************ */ ?>

<?php
//printr($team);
?>

<div id="teamInfo" class="cleaned" data-id=<?=($team->id . '-' . getTeamHash($team->id))?>>

    <h2 class="blockTitle"><?=$tr->tr('Tým info')?></h2>

    <div class="content">

        <div class="head cleaned">
            <h3 class="name"><a href="<?=$team->getProfileLink()?>" title="<?=$tr->tr('Zobrazit profil týmu')?> <?=$team->getName()?>"><?=$team->getName()?></a></h3>
            <a class="logo team_picture_small" href="<?=$team->getProfileLink()?>" title="<?=$tr->tr('Zobrazit profil týmu')?> <?=$team->getName()?>">
                <img src="<?=$team->getTeamLogo('small')?>" alt="<?=$team->getName()?>" width="28" height="28" />
            </a>
            <span class="sport"><?=$gsports[$team->sport_id]?></span>
        </div>

        <p class="teamPhoto">
            <img src="<?=$team->getTeamPhoto('small')?>" alt="<?=$team->getName()?>" />
        </p>

        <?php if(!empty($team->league_web)) { ?>
        <p class="leagueInfo">
            <?=$team->league?><br />
            <a href="<?=fix_link($team->league_web, true)?>" title="<?=$tr->tr('Odkaz na jiný web')?> - <?=fix_link($team->league_web)?>"><?=fix_link($team->league_web, false, true)?></a>
        </p>
        <?php } ?>

        <p class="teamCity">
            <?=str_replace(', ', ', <br />', $team->city)?>
        </p>

        <p class="teamStats cleaned">
            Členové: <span id="playersCount"><?=intval($team->players)?></span><br />
            Fanoušci: <span id="fansCount"><?=intval($team->fans)?></span><br />
            Odběratelé: <span id="observersCount"><?=intval($team->observers)?></span>
        </p>

        <?php
        if($team->isAdmin($logged_user) && $team->id == $logged_user->getActiveTeam()->id)
        {
        ?>
        <ul class="teamAdmin cleaned">
            <?php /*
            <li class="item">
                <a class="icon friendlyMatch" href="#" title="<?=$tr->tr('Zobrazit nabídky na přátelské utkání týmu')?>">
                    <?=$tr->tr('přátelská utkání')?>
                    <span>10</span>
                </a>
            </li>
            <li class="item">
                <a class="icon sendTeamMessage" href="#" title="<?=$tr->tr('Zobrazit týmové zprávy')?>">
                    <?=$tr->tr('týmové zprávy')?>
                    <span>103</span>
                </a>
            </li>
            */?>
            <li class="item">
                <a class="icon teamEdit" href="<?=PATH_WEB_ROOT;?>edit-team-profile/" title="<?=$tr->tr('Nastavení týmu')?>">
                    <?=$tr->tr('nastavení týmu')?>
                </a>
            </li>
        </ul>
        <?php
        }
        ?>
        
    </div><!-- .content -->

</div><!-- #teamInfo --> 