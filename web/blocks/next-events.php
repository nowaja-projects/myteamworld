<?php
// chceme jen aktivní tým, předtím to bylo jinak 
$userTeams = array($team->id);

if(is_array($userTeams) && count($userTeams))
{
    // TRÉNINK
    $training = EventDAO::getEarliestEvent(array(Event::TRAINING), $userTeams);
    
    // ZÁPASY
    $match = EventDAO::getEarliestEvent(array(Event::COMPETITION,Event::FRIENDLY), $userTeams);
    
    if(($training instanceof Event && !empty($training->id)) || ($match instanceof Event && !empty($match->id)))
    {
?>
    
    <div id="nextEvents" class="cleaned">
    
        <h2 class="blockTitle">Nejbližší události</h2>
    
        <div class="content">
            <?php            
            if($training instanceof Event && !empty($training->id))
            {
                $info = unserialize($training->info);
                $day = date('l', strtotime($training->start));
                
                $team_id = 0;

                if(in_array($training->team_id, $userTeams))
                {
                    $team_id = $training->team_id;
                }
                elseif(in_array($training->oponent_id, $userTeams))
                {
                    $team_id = $training->oponent_id;
                }
                
                $found = false;
                $attend = AttendDAO::getAttendForEvent($team_id, $training->id);
                if($attend)
                {
                    foreach($attend as $row)
                    {
                        if($row['user_id'] == $logged_user->id)
                        {
                            $found = true;
                            break;
                        }
                    }
                    $attend = $row;
                }

                if(!$found)
                {
                    $attend = array();
                }

                //printr(unserialize($training->info));
            ?>
            <h3 class="title training"><a href="<?php echo PATH_WEB_ROOT;?>events/" title="">Nejbližší trénink</a>:</h3>
            <p class="text"><?=$_days_of_week[$day]?>, <?=date('d. m. H:i', strtotime($training->start))?> - <?=$info[0]['place']?></p>
            <?php
                if(!is_array($attend) || count($attend) == 0 || (!empty($attend['attend']) && $attend['attend'] == 'na'))
                {
                    $id = $training->team_id . '-' . $training->id . '-' . $logged_user->id . '@' . getRecipientHash($logged_user->id);
            ?>
                    <p class="answer"><a href="#" class="yes" data-id="<?=$id?>" title="Zúčastním se tohoto tréninku">zúčastním se</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" class="no" data-id="<?=$id?>" title="Nezúčastním se tohoto tréninku">nezúčastním se</a></p>
            <?php
                }
                elseif(isset($attend['attend']) && $attend['attend'] == 'yes')
                {
            ?>
                    <p class="answer">zúčastním se</p>
            <?php
                }
                elseif(isset($attend['attend']) && $attend['attend'] == 'no')
                {
            ?>
                    <p class="answer">nezúčastním se</p>
            <?php        
                }
            }
            
            
            
            
            
            if($match instanceof Event && !empty($match->id))
            {
                $info = unserialize($match->info);
                $day = date('l', strtotime($match->start));
                
                $team_id = 0;
                
                if(in_array($match->team_id, $userTeams))
                {
                    $team_id = $match->team_id;
                }
                elseif(in_array($match->oponent_id, $userTeams))
                {
                    $team_id = $match->oponent_id;
                }
                
                $attend = AttendDAO::getAttendForEvent($team_id, $match->id);
//printr($attend);
                if($attend)
                {
                    foreach($attend as $row)
                    {
                        if($row['user_id'] == $logged_user->id)
                        {
                            break;
                        }
                    }
                    $attend = $row;
                }
            ?>
            <h3 class="title match"><a href="<?php echo PATH_WEB_ROOT;?>events/" title="">Nejbližší zápas</a>:</h3>
            <p class="text"><?=$_days_of_week[$day]?>, <?=date('d. m. H:i', strtotime($match->start))?> - <?=$info[0]['place']?></p>
            <?php
                if(!is_array($attend) || count($attend) == 0 || (!empty($attend['attend']) && $attend['attend'] == 'na') || !is_array($attend))
                {
                    $id = $match->team_id . '-' . $match->id . '-' . $logged_user->id . '@' . getRecipientHash($logged_user->id);
            ?>
                    <p class="answer"><a href="#" class="yes" data-id="<?=$id?>" title="Zúčastním se tohoto zápasu">zúčastním se</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" class="no" data-id="<?=$id?>" title="Nezúčastním se tohoto zápasu">nezúčastním se</a></p>
            <?php
                }
                elseif(isset($attend['attend']) && $attend['attend'] == 'yes')
                {
            ?>
                    <p class="answer">zúčastním se</p>
            <?php
                }
                elseif(isset($attend['attend']) && $attend['attend'] == 'no')
                {
            ?>
                    <p class="answer">nezúčastním se</p>
            <?php        
                }
            }
            ?>
    
            <? /*<h3 class="title task"><a href="#" title="">Nejbližší úkol</a>:</h3>
            <p class="text">po, 26. 10. 19:00 - Posilovat</p> */ ?>
    
        </div><!-- .content -->
    </div><!-- #nextEvents -->

<?php
    }
}
?>