<?php
define('ADVERT_LIMIT', 1);


function filter_sport($var)
{
    global $logged_user;

    if(empty($logged_user->id))
    {
        return true;
    }

    if(empty($var['sport']) || in_array($logged_user->sport, $var['sport']))
    {
        return true;
    }

    return false;
}


/*
sport_id    sport_name
1   Atletika
2   Basketbal
3   Florbal
4   Fotbal
5   Futsal
6   Golf
7   Házená
8   Hokej
9   Hokejbal
10  Lyže a snowboard
11  Motorsport
12  Tenis
13  Volejbal
 */

$adverts = array(
    /* KONEC
    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportsen.cz-chelsea-manc">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/premier-league/chelsea-vs-manchester-city" title="" target="_blank">Fotbalové zájezdy!</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/premier-league/chelsea-vs-manchester-city" title="" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/sportsen-chelsea-manc.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/premier-league/chelsea-vs-manchester-city" title="" target="_blank">Splň si sen a jeď na fotbalový zápas svého oblíbeného týmu! Velký výběr z top lig a týmů!</a>
                            </p>
                        </div>',
    ),
    */
    /*
    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportsen.cz-barcelona-real">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/primera-division/fc-barcelona-vs-real-madrid" title="" target="_blank">Fotbalové zájezdy!</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/primera-division/fc-barcelona-vs-real-madrid" title="" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/sportsen-barcelona-real.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/primera-division/fc-barcelona-vs-real-madrid" title="" target="_blank">Splň si sen a jeď na fotbalový zápas svého oblíbeného týmu! Velký výběr z top lig a týmů!</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportsen.cz-dortmund-bayern">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/bundesliga/borussia-dortmund-vs-bayern-mnichov" title="" target="_blank">Fotbalové zájezdy!</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/bundesliga/borussia-dortmund-vs-bayern-mnichov" title="" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/sportsen-durtmund-bayern.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportsen.cz/zajezdy-na-fotbal/bundesliga/borussia-dortmund-vs-bayern-mnichov" title="" target="_blank">Splň si sen a jeď na fotbalový zápas svého oblíbeného týmu! Velký výběr z top lig a týmů!</a>
                            </p>
                        </div>',
    ),
    */
    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="bodyestetic.cz-1">
                            <h3 class="title"><a href="http://www.bodyestetic.cz" title="" target="_blank">Zregenerujte své síly!</a></h3>
                            <a class="img" href="http://www.bodyestetic.cz" title="" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/bodyestetic.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.bodyestetic.cz" title="" target="_blank">Jste unaveni po své sportovní aktivitě a potřebujete doplnit síly nebo si odpočinout? Navštivte naše studio!</a>
                            </p>
                        </div>',
    ),
    /*
    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-1">
                            <h3 class="title"><a href="http://www.kqzyfj.com/click-7650345-11493954" title="" target="_blank">Nová sezóna startuje!</a></h3>
                            <a class="img" href="http://www.kqzyfj.com/click-7650345-11493954" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/sportobchod.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.kqzyfj.com/click-7650345-11493954" title="" target="_blank">Vyberte si u nás z velkého množství florbalových holí, florbalové obuvi, oblečení a různých doplňků.</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-salming-shoes-blue-yellow">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/salova-obuv-salming-race-r1-autumn.htm" title="" target="_blank">Sálová obuv Salming</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/salova-obuv-salming-race-r1-autumn.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/boty-salming-race-r1.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/salova-obuv-salming-race-r1-autumn.htm" title="" target="_blank">Extra lehké sálové boty Salming Race R1 Autumn Edt. nabité novými technologiemi nyní v&nbsp;e-shopu sportobchod.cz v akci!</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-1-salming-shoes-black-yellow">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/salova-obuv-salming-x-factor-3.htm" title="" target="_blank">Sálová obuv Salming</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/salova-obuv-salming-x-factor-3.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/boty-salming-x-factor-3.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/salova-obuv-salming-x-factor-3.htm" title="" target="_blank">Sálové boty Salming X-Factor 3. Neváhejte a kupujte kvalitu Salming s nejnovějšími technologiemi v akci na e-shopu!</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-1-salming-stick-black-white">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-salming-quest-kickzone-27.htm" title="" target="_blank">Florbalka Salming Quest</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-salming-quest-kickzone-27.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/florbalka-salming-quest-kickzone-27.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-salming-quest-kickzone-27.htm" title="" target="_blank">Florbalová hokejka Salming Quest Kickzone 27 s lehkou čepelí a razantní střelou nyní v&nbsp;e-shopu se slevou!</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-1-xzone-stick-white-red">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-zone-flash-ultralight-27-greed-95-cm.htm" title="" target="_blank">Florbalka Zone Flash</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-zone-flash-ultralight-27-greed-95-cm.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/florbalka-zone-flash-ultralight-27.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-zone-flash-ultralight-27-greed-95-cm.htm" title="" target="_blank">Florbalová hokejka Zone Flash Ultralight 27 Greed s lehkým karbonovým shaftem v&nbsp;e-shopu sportobchod.cz ve výprodeji!</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-tempish-sniper-2">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-tempish-sniper-ii.htm" title="" target="_blank">Florbalka Tempish Sniper</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-tempish-sniper-ii.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/florbalka-tempish-sniper-2.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-tempish-sniper-ii.htm" title="" target="_blank">Florbalová hokejka Tempish Sniper II je určena pro nenáročné rekreační hráče. Dostatečně tvrdá a&nbsp;přitom levná!</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-unihoc-cavity-super-top-light-26">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-unihoc-cavity-super-top-light-26.htm" title="" target="_blank">Florbalka Unihoc Cavity</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-unihoc-cavity-super-top-light-26.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/florbalka-unihoc-cavity-super-top-light-26.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalova-hokejka-unihoc-cavity-super-top-light-26.htm" title="" target="_blank">Lehká florbalová hokejka Unihoc Cavity STL 26 z&nbsp;karbon kompozitu pro náročné hráče s&nbsp;prvotřídní čepelí Cavity!</a>
                            </p>
                        </div>',
    ),
    */
    /* DOCASNE VYPRODANO
    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(3),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-vak-salming-tour-stickbag">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalovy-vak-salming-tour-stickbag-13.htm" title="" target="_blank">Florbalový vak Salming</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalovy-vak-salming-tour-stickbag-13.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/florbalovy-vak-salming-tour-stickbag.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/florbalovy-vak-salming-tour-stickbag-13.htm" title="" target="_blank">Z jedné strany modrý a&nbsp;z&nbsp;druhé strany černý vak až na 3&nbsp;seniorské florbalové hokejky!</a>
                            </p>
                        </div>',
    ),
    */
    /*
    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-rozlisovaci-dres-sportobchod">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/rozlisovaci-dres-sportobchod.htm" title="" target="_blank">Rozlišovací dres</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/rozlisovaci-dres-sportobchod.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/rozlisovaci-dres-sportobchod.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/rozlisovaci-dres-sportobchod.htm" title="" target="_blank">Rozlišovací dres je dostupný v&nbsp;těchto barvách - tmavě modrá, žlutá, zelená, oranžová, světle modrá, červená a černá!</a>
                            </p>
                        </div>',
    ),

    array(
        'sex'       => array('male', 'female'),
        'sport'     => array(),
        'age'       => array(
            'from'  => 0, 
            'to'    => 99
        ),
        'code'      => '<div class="aBox" data-name="sportobchod.cz-praskovy-napoj-isostar-hydrate-400g">
                            <h3 class="title"><a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/praskovy-napoj-isostar-hydrate-perform-400-g.htm" title="" target="_blank">Práškový nápoj Isostar</a></h3>
                            <a class="img" href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/praskovy-napoj-isostar-hydrate-perform-400-g.htm" target="_blank">
                                <img src="'.IMAGES_PATH.'ag/praskovy-napoj-isostar-hydrate-400g.jpg" alt="" />
                            </a>
                            <p class="text">
                                <a href="http://www.anrdoezrs.net/links/7650345/type/dlg/http://www.sportobchod.cz/praskovy-napoj-isostar-hydrate-perform-400-g.htm" title="" target="_blank">Práškový nápoj Isostar Hydrate &amp; Perform je isotonická směs pro přípravu nápoje ve třech skvělých příchutích!</a>
                            </p>
                        </div>',
    )
    */
);

// vyfiltrujeme podle sportu
$adverts = array_filter($adverts, 'filter_sport');


// zamichame, aby se zobrazovaly nahodne
shuffle($adverts);
$adverts = array_slice($adverts, 0, ADVERT_LIMIT);
?>


<div id="ag" class="cleaned">

    <h2 class="blockTitle">Reklama</h2>

    <div class="content cleaned">

    <?php
    foreach($adverts as $adv)
    {
        echo $adv['code'];
    }
    ?>
        <!--<div class="aBox" data-name="sportobchod.cz-1">
            <h3 class="title"><a href="http://www.kqzyfj.com/click-7650345-11493954" title="" target="_blank">Nová sezóna startuje!</a></h3>
            <a class="img" href="http://www.kqzyfj.com/click-7650345-11493954" target="_blank">
                <img src="<?php echo IMAGES_PATH;?>ag/sportobchod.jpg" alt="" />
            </a>
            <p class="text">
                <a href="http://www.kqzyfj.com/click-7650345-11493954" title="" target="_blank">Vyberte si u nás z velkého množství florbalových holí, florbalové obuvi, oblečení a různých doplňků.</a>
            </p>
        </div>

        <div class="aBox" data-name="bodyestetic.cz-1">
            <h3 class="title"><a href="http://www.bodyestetic.cz" title="" target="_blank">Zregenerujte své síly!</a></h3>
            <a class="img" href="http://www.bodyestetic.cz" title="" target="_blank">
                <img src="<?php echo IMAGES_PATH;?>ag/bodyestetic.jpg" alt="" />
            </a>
            <p class="text">
                <a href="http://www.bodyestetic.cz" title="" target="_blank">Jste unaveni po své sportovní aktivitě a potřebujete doplnit síly nebo si odpočinout? Navštivte naše studio!</a>
            </p>
        </div>-->

    </div><!-- .content -->

    <?php if($page == 'wall')
    {
    ?>
    <ul class="list links">
        <li class="item"><a href="<?=PATH_WEB_ROOT?>contact/" title="Kontaktujte nás, pokud máte zájem o reklamu na myteamworld.com"><?=$tr->tr('Reklamy')?></a></li>
        <li class="item"><a href="<?php echo PATH_WEB_ROOT;?><?=($logged_user->hasTeam() ? 'help' : 'help-without-team')?>/" title="Zobrazit nápovědu"><?=$tr->tr('Nápověda')?></a></li>
        <li class="item"><a href="<?=PATH_WEB_ROOT?>contact/" title="Kontaktujte nás"><?=$tr->tr('Kontakt')?></a></li>
    </ul>
    <?php
    }
    else
    {
    ?>
    <ul class="list links">
        <li class="item"><a href="<?=PATH_WEB_ROOT?>contact/" title="Kontaktujte nás, pokud máte zájem o reklamu na myteamworld.com"><?=$tr->tr('Mám zájem o reklamu')?></a></li>
    </ul>
    <?
    }
    ?>
</div><!-- #advertising -->