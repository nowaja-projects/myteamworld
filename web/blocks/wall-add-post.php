<?php /* ********** translated ************ */ ?>
<?php
$activeId = $logged_user->getActiveTeam()->id;
?>
<div id="status" class="box cleaned">
    <form action="" method="post" class="cleaned" id="teamPost">
        <p class="entries textarea">
            <input type="hidden" name="teampost[team]" value="<?=$activeId . '-'. getTeamHash($activeId)?>" />
            <textarea class="resizable noActive" name="teampost[text]" cols="75" rows="1" title="<?=$tr->tr('Napište vzkaz na nástěnku týmu')?>"><?=$tr->tr('Napište vzkaz na nástěnku týmu')?> <?=$team->getName()?>...</textarea>
        </p>
        <p class="entries submit">
            <button type="submit" class="button buttonA small" title="<?=$tr->tr('Odeslat')?>"><?=$tr->tr('odeslat')?></button>
        </p>
    </form>

<?php
    /*
    <!-- FILTR PRO UZIVATELE S TEAMEM -->

        <div class="box">
            <div class="top"></div>
            <div class="content cleaned">
                <form action="" id="status">
                    <div class="textarea">
                        <span>Napiš vzkaz na nástěnku teamu NÁZEV TEAMU...</span>
                        <textarea name="name" cols="1" rows="1"></textarea>
                    </div>
                    <div class="bottom-light"></div>
                    <div><input type="image" src="<?php echo IMAGES_PATH;?>form-button-odeslat.png" alt="odeslat" /></div>
                </form>
                <span class="filterButton">nastavit filtr zpráv</span>
            </div><!-- .content -->
            <div class="bottom"></div>
        </div><!-- .box -->

        <div class="box filter">
            <div class="top"></div>
            <div class="content">
            <form action="" class="cleaned">

                <div class="news inner cleaned">
                    <div class="group">
                        <input type="checkbox" /> Atletika<br />
                        <input type="checkbox" /> Basketbal<br />
                        <input type="checkbox" /> Florbal<br />
                        <input type="checkbox" /> Fotbal<br />
                        <input type="checkbox" /> Futsal
                    </div>
                    <div class="group">
                        <input type="checkbox" /> Golf<br />
                        <input type="checkbox" /> Hokej<br />
                        <input type="checkbox" /> Lyžování<br />
                        <input type="checkbox" /> Motorsport<br />
                        <input type="checkbox" /> Tenis
                    </div>
                </div><!-- .news -->

                <div class="message inner cleaned">
                    <div class="group">
                        <input type="checkbox" /> Tiskové zprávy<br />
                        <input type="checkbox" /> Události<br />
                        <input type="checkbox" /> Soupisky<br />
                        <input type="checkbox" /> Docházka<br />
                        <input type="checkbox" /> Galerie
                    </div>
                </div><!-- .message -->

                <div class="search inner cleaned">
                    <div class="group">
                        <input type="text" class="input" value="Hledaný výraz" spellcheck="false">
                        <input type="checkbox" class="check-all" /> Vybrat vše
                    </div>
                </div><!-- .search -->

            </form>
        </div><!-- .content -->
            <div class="bottom"></div>
        </div><!-- .filter -->

    <!-- KONEC FILTR PRO UZIVATELE S TEAMEM -->
    */
    ?>
    <div class="smallFilter">
        <a href="#">
            <span class="visible-lg"><?=$tr->tr('nastavit filtr zpráv')?></span>
            <span class="hidden-lg"><?=$tr->tr('filtr zpráv')?></span>
        </a>
    </div><!-- #filter -->
</div><!-- .box -->