<?php 

/* uzivatel bez tymu nastenka */ 
$help['wall']['without-team'] = '
<link href="http://fonts.googleapis.com/css?family=Patrick+Hand&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
<div id="helpOverlay" class="wall userNoTeam">
    <div class="helpBox big wall">
        <div class="info">
            <h4 class="title"><span class="textBg">Nástěnka</span></h4>
            <p class="text"><span class="textBg">tady budete mít všechny</span><br /><span class="textBg">důležité informace pěkně</span><br /><span class="textBg">pohromadě</span></p>
        </div>
    </div>
    <div class="helpBox helpButton">
        <div class="info">
            <h4 class="title"><span class="textBg">Nápověda</span></h4>
            <p class="text"><span class="textBg">pokud budete chtít osvěžit hlavní</span><br /><span class="textBg">prvky této stránky, klikněte sem</span></p>
        </div>
    </div>
    <div class="helpBox search">
        <div class="info">
            <h4 class="title"><span class="textBg">Vyhledávání</span></h4>
            <p class="text"><span class="textBg">chcete-li najít tým nebo</span><br /><span class="textBg">uživatele na mtw,</span><br /><span class="textBg">je to jednoduché</span></p>
        </div>
    </div>
    <div class="helpBox small noticesMessages">
        <div class="info">
            <h4 class="title"><span class="textBg">Upozornění a zprávy</span></h4>
            <p class="text"><span class="textBg">abyste o nic důležitého</span><br /><span class="textBg">nepřišli, sledujte svoje upozornění</span><br /><span class="textBg">a osobní zprávy</span></p>
        </div>
    </div>
    <div class="close">
        <a href="#">zavřít</a>
    </div>
</div>';


/* uzivatel s tymem nastenka */ 
$help['wall']['with-team'] = '
<link href="http://fonts.googleapis.com/css?family=Patrick+Hand&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
<div id="helpOverlay" class="wall userWithTeam">
    <div class="helpBox big wall">
        <div class="info">
            <h4 class="title"><span class="textBg">Nástěnka</span></h4>
            <p class="text"><span class="textBg">tady budete mít všechny</span><br /><span class="textBg">důležité informace pěkně</span><br /><span class="textBg">pohromadě</span></p>
        </div>
    </div>
    <div class="helpBox helpButton">
        <div class="info">
            <h4 class="title"><span class="textBg">Nápověda</span></h4>
            <p class="text"><span class="textBg">pokud budete chtít osvěžit hlavní</span><br /><span class="textBg">prvky této stránky, klikněte sem</span></p>
        </div>
    </div>
    <div class="helpBox search">
        <div class="info">
            <h4 class="title"><span class="textBg">Vyhledávání</span></h4>
            <p class="text"><span class="textBg">chcete-li najít tým nebo</span><br /><span class="textBg">uživatele na mtw,</span><br /><span class="textBg">je to jednoduché</span></p>
        </div>
    </div>
    <div class="helpBox small noticesMessages">
        <div class="info">
            <h4 class="title"><span class="textBg">Upozornění a zprávy</span></h4>
            <p class="text"><span class="textBg">abyste o nic důležitého</span><br /><span class="textBg">nepřišli, sledujte svoje upozornění</span><br /><span class="textBg">a osobní zprávy</span></p>
        </div>
    </div>
    <div class="close">
        <a href="#">zavřít</a>
    </div>
</div>';


/* uzivatel s tymem na detailu jineho tymu */ 
$help['team-profile'] = '
<link href="http://fonts.googleapis.com/css?family=Patrick+Hand&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
<div id="helpOverlay" class="teamProfile userWithTeam">
    <div class="helpBox teamInfo">
        <div class="info">
            <h4 class="title"><span class="textBg">Tým info</span></h4>
            <p class="text"><span class="textBg">základní informace o týmu,</span><br /><span class="textBg">na který se momentálně díváte</span></p>
        </div>
    </div>
    <div class="helpBox helpButton">
        <div class="info">
            <h4 class="title"><span class="textBg">Nápověda</span></h4>
            <p class="text"><span class="textBg">pokud budete chtít osvěžit hlavní</span><br /><span class="textBg">prvky této stránky, klikněte sem</span></p>
        </div>
    </div>
    <div class="helpBox teamMenu">
        <div class="info">
            <h4 class="title"><span class="textBg">Navigace ostatních týmů</span></h4>
            <p class="text"><span class="textBg">slouží k prohlížení modulů, které má tento tým aktivní</span></p>
        </div>
    </div>
    <div class="close">
        <a href="#">zavřít</a>
    </div>
</div>';


/* prepinani tymu */ 
$help['switch-teams'] = '
<link href="http://fonts.googleapis.com/css?family=Patrick+Hand&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
<div id="helpOverlay" class="teamProfile userWithTeam">
    <div class="helpBox small switchTeamInfo">
        <div class="info">
            <h4 class="title"><span class="textBg">Přepínání týmů</span></h4>
            <p class="text"><span class="textBg">vaše týmy můžete přepínat zde,</span><br /><span class="textBg">tým, který máte právě aktivní,</span><br /><span class="textBg">vidíte pod vaším jménem</span></p>
        </div>
    </div>
    <div class="helpBox helpButton">
        <div class="info">
            <h4 class="title"><span class="textBg">Nápověda</span></h4>
            <p class="text"><span class="textBg">pokud budete chtít osvěžit hlavní</span><br /><span class="textBg">prvky této stránky, klikněte sem</span></p>
        </div>
    </div>
    <div class="close">
        <a href="#">zavřít</a>
    </div>
</div>';

?>