<?php
Header("Content-Type:application/json; charset=utf-8");

require_once 'include/config.php';
require_once 'include/functions.php';
require_once 'include/init.php';
require_once 'include/preload.php';
require_once 'action/'.$page.'.action.php';

/* *****************************************************************************
  SOUBOR, KTERY BUDE ZPRACOVAVAT VESKERE AJAX POZADAVKY 
  ****************************************************************** */
  
// nahrani profilove fotky
if(isset($_GET['file']))
{
    $return = array(
        'status' => 'error'
    );

    if(!empty($logged_user->id))
    {
        $team = $logged_user->getActiveTeam();
        if(!empty($_FILES['image']) && $team instanceof Team)
        {
            if($_GET['file'] == 'edit-team-profile')
            {
                // musime preulozit info o verzi a zmenit session a cookie
                $team->version = uniqid();
                $team->updateVersion();
                
                $logged_user->setActiveTeam($team, false);
                $logged_user->getTeamList(true);
                $_project['session']->set('logged', $logged_user);
                
                $img = $team->uploadImage(true);
            }
            elseif($_GET['file'] == 'team-registration')
            {
                $tmp = new Team();
                $dir = session_id();
                $dir = "/$dir/";
                $img = $tmp->uploadImage(true, 'image', false, $dir);
            }
            
            if(is_array($img))
            {
                $return = array(
                    'status'     => 'ok',
                    'serialized' => ($_GET['file'] == 'team-registration' ? $dir : false)
                );
                $return = array_merge($return, $img);
            }

        }
        echo json_encode($return);
    }
    else // uzivatel neni prihlasen
    {
        $return = array(
            'status' => 'error',
            'message' => 'Not logged'
        );
        
        echo json_encode($return);
    }
}


// smazani profilovou fotografii
if(isset($_GET['delete_profile_photo']) && !empty($logged_user->id))
{
    $team = $logged_user->getActiveTeam();
    $return = array();
    
    if($_GET['page'] == 'team-registration')
    {
        $tmp = new Team();
        $dir = session_id();
        $dir = "/$dir/";
        
        @unlink(TEAM_DATADIR . intval($tmp->id) . $dir . ORIGINAL_IMAGE_NAME);
        @unlink(TEAM_DATADIR . intval($tmp->id) . $dir . ORIGINAL_IMAGE_NAME_CROPPED);

        foreach($default_users as $key => $image)
        {
            $return['img_'.$key] = '<img src="'.$tmp->getTeamLogo($key, $dir).'" alt="'.$team->getName().'" width="'.intval($sizes_user[$key][0]).'" height="'.intval($sizes_user[$key][1]).'">';
        }
        
        $return['status'] = 'ok';
        $return['serialized'] = $dir;
    }
    else
    {
        if($team instanceof Team && $team->id > 0)
        {
            $team->version = uniqid();
            $team->updateVersion();
            
            $logged_user->setActiveTeam($team, false);
            $logged_user->getTeamList(true);
            $_project['session']->set('logged', $logged_user);
            // FIXME
            //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
                
            foreach($global_user_images as $key => $image)
            {
                @unlink(TEAM_DATADIR . $team->id . '/' . $image['name']);
                $return['img_'.$key] = '<img src="'.PATH_WEB_ROOT . TEAM_DATADIR . $image['name'].'" alt="'.$team->getName().'" width="'.intval($image['size'][0]).'" height="'.intval($image['size'][1]).'">';
            }
            
            $return['status'] = 'ok';
        }
        else
        {
            $return = array(
                'status' => 'error',
                'message' => 'no team'
            );
        }
    }
    
    echo json_encode($return);
}


if(isset($_GET['crop']) && !empty($logged_user->id))
{
    // registrace tymu
    if($_GET['page'] == 'team-registration')
    {
        $dir = session_id();
        $dir = "/$dir/";
        $tmp = new Team();
        
        $img = $tmp->cropImage($_GET['width'], $_GET['x'], $_GET['y'], true, false, $dir);
        if(is_array($img))
        {
            $return = array(
                'status'     => 'ok',
                'serialized' => $dir
            );
            $return = array_merge($return, $img);
        }
    }
    // editace profilu
    else
    {
        $team = $logged_user->getActiveTeam();
        
        // musime preulozit info o verzi a zmenit session a cookie
        $team->version = uniqid();
        $team->updateVersion();
            
        $logged_user->setActiveTeam($team, false);
        $logged_user->getTeamList(true);
        $_project['session']->set('logged', $logged_user);
        // FIXME
        //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
    
        $img = $team->cropImage($_GET['width'], $_GET['x'], $_GET['y']);
        if(is_array($img))
        {
            $return = array(
                'status'     => 'ok',
            );
            $return = array_merge($return, $img);
        }
    }
    
    echo json_encode($return);
}


if(isset($_GET['switchTeam']) && !empty($logged_user->id) && intval($_GET['switchTeam']) > 0)
{
    $id = intval($_GET['switchTeam']);
    $team = TeamDAO::get($id);

    if($team instanceof Team && $team->id > 0 && $team->isPlayer($logged_user))
    {
        $logged_user->setActiveTeam($team);
        $logged_user->getTeamList(true);
        
        updateLoggedUser($_project, $logged_user);
    
        $return = array(
            'status'     => 'ok',
        );
    }
    else
    {
        $return = array(
            'status'     => 'error',
        );
    }
    
    echo json_encode($return);
}

// FIXME jestli se nechce stat fanouskem tymu, kde uz je hracem
if(isset($_GET['becameFan']) && !empty($logged_user->id) && intval($_GET['becameFan']) > 0 && isset($_GET['team_hash']) && !empty($_GET['team_hash']))
{
    $id = intval($_GET['becameFan']);
    $team = TeamDAO::get($id);
    $fans = $team->getFanList();
    if(!in_array($logged_user->id, $fans))
    {
        if($_GET['team_hash'] == $team->getHash(TEAM_SALT))
        {
            $team->removeObserver($logged_user->id);
            $team->addFan($logged_user->id);
            $logged_user->getTeamList(true);
            $_project['session']->set('logged', $logged_user);
            // FIXME
            //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');

            require(BLOCK_PATH.'team-links-fan.php');
            
            $fanCount = intval(count($fans) + 1);
            $observerCount = intval(count($team->getObserverList()));
            
            if(isset($_GET['search']) && $_GET['search'] == 1)
            {
                $fanCount .= ' ' . decline($fanCount, 'fanoušek', 'fanoušci', 'fanoušků'); 
                $observerCount .= ' ' . decline($observerCount, 'odběratel', 'odběratelé', 'odběratelů');
            }
            
            $return = array(
                'status'     => 'ok',
                'fansCount'  => $fanCount,
                'observersCount'  => $observerCount,
                'html'       => $linksHtml
            );
        }
        else
        {
            $return = array(
                'status'     => 'error',
                'message' => $tr->tr('Nastala chyba. Zkuste to později znovu.')
            );
        }
    }
    else
    {
        $return = array(
            'status'     => 'error',
            'message'   => $tr->tr('Již jste fanouškem tohoto týmu.')
        );
    }
    
    echo json_encode($return);
}


if(isset($_GET['becameObserver']) && !empty($logged_user->id) && intval($_GET['becameObserver']) > 0 && isset($_GET['team_hash']) && !empty($_GET['team_hash']))
{
    $id = intval($_GET['becameObserver']);
    $team = TeamDAO::get($id);
    $fans = $team->getFanList();
    $observers = $team->getObserverList();
    if(!in_array($logged_user->id, $fans) && !in_array($logged_user->id, $observers))
    {
        if($_GET['team_hash'] == $team->getHash(TEAM_SALT_OBS))
        {
            $team->addObserver($logged_user->id);
            $logged_user->getTeamList(true);
            $_project['session']->set('logged', $logged_user);
            // FIXME WTF
            //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
            
            require(BLOCK_PATH.'team-links-observer.php');
        
            $fanCount = intval(count($fans));
            $observerCount = intval(count($observers) + 1);
            
            if(isset($_GET['search']) && $_GET['search'] == 1)
            {
                $fanCount .= ' ' . decline($fanCount, 'fanoušek', 'fanoušci', 'fanoušků'); 
                $observerCount .= ' ' . decline($observerCount, 'odběratel', 'odběratelé', 'odběratelů');
            }
        
            $return = array(
                'status'     => 'ok',
                'fansCount'  => $fanCount,
                'observersCount'  => $observerCount,
                'html'       => $linksHtml
            );
        }
        else
        {
            $return = array(
                'status'     => 'error',
                'message' => $tr->tr('Nastala chyba. Zkuste to později znovu.')
            );
        }
    }
    else
    {
        $return = array(
            'status'     => 'error',
            'message'   => $tr->tr('Již jste odběratelem tohoto týmu.')
        );
    }
    
    echo json_encode($return);
}


/**
 * Zadost o vstup do týmu
 **/
if(isset($_REQUEST['becameMember']) && !empty($_REQUEST['team_hash']))
{
    $return = array(
        'status'    => 'error',
        'message'   => 'Nastala chyba při posílání žádosti o vstup do týmu.' . ' #314'
    );
    // FIXME - jestli uz uzivatel neni do týmu pozván, kdyztak update notifikace

    $time = time();
    $team = TeamDAO::get($_REQUEST['becameMember']);

    if($team instanceof Team && $team->id > 0 && !$team->isPlayer($logged_user) && $_GET['team_hash'] == $team->getHash(TEAM_SALT_ENTRY) && !empty($team->show_request))
    {
        // abychom si mohli vytáhnout starou žádost
        $data = array(
            'user_id'   => $logged_user->id,
            'team_id'   => $team->id
        );

        // vytvoreni kontrolniho hashe, pro prijeti nebo odmitnuti pozvanky
        $hash = getRequestHash($team->id, $logged_user->id);

        // kouknem jestli uz nahodou neni pozvanka tam
        $oldinvite = RosterInvites::getRequestByTeam($data);
            
        // pozvanka uz existuje ale uzivatel ji odmitnul, nebo vubec neexistuje jeste, tak ho muzeme pozvat znovu
        if((count($oldinvite) > 0 && $oldinvite['status'] != 'requested') || count($oldinvite) == 0)
        {
            // pozvánka data
            $invite = array(
                'user_id'   => $logged_user->id,
                'team_id'   => $team->id,
                'hash'      => $hash
            );
                
            // ulozime samotnou zadost do DB
            $invite_id = RosterInvites::insertRequest($invite);
            if($invite_id)
            {
                $canSendRequest = false;
                if($team->isFan($logged_user))
                {
                    require(BLOCK_PATH.'team-links-fan.php');
                }
                elseif($team->isObserver($logged_user))
                {
                    require(BLOCK_PATH.'team-links-observer.php');
                }
                else
                {
                    require(BLOCK_PATH.'team-links-visitor.php');
                }

                $return = array(
                    'status'     => 'member-ok',
                    'html'       => $linksHtml
                );

                $data = array();
                $admins = $team->getAdmins(true);
                
                foreach($admins as $admin_id)
                {
                    $admin = UserDAO::get($admin_id);
                    if(!$admin instanceof User || empty($admin->id))
                    {
                        continue;
                    }
                    SendMail::sendTeamEntryRequest($admin->email, $team, $logged_user);

                    // info, které si uložíme
                    // přihlášený uzivatel kvuli notifikaci
                    // team kvuli odkazu na tym
                    $array = array(
                        'team'          => $team->id,
                        'requested_user'  => $logged_user->id,
                        'logged_user'  => $logged_user->id,
                        'request_id'    => $invite_id
                    );
                    
                    // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                    $data[] = array(
                        'user_id'    => $admin_id,
                        'object_id'  => $team->id,
                        'type_id'    => 'REQUEST-TEAM-ENTRY',
                        'info'       => serialize($array),
                        'object_info'=> '',
                        'timestamp'  => $time,
                        'read'       => '0'
                    );
                }
                
                if(count($data))
                {
                    // pokud existuje uzivatel, tak vložíme ještě notifikaci
                    if(!Notifications::insertNotifications($data))
                    {
                        // musime odebrati tu pozvanku, aby ho mohl zkusit pozvat znovu
                        $invite = array(
                            'team_id'   => $team->id,
                            'user_id'   => $array['requested_user']
                        );
                        RosterInvites::removeMember($invite);

                        $return['status'] = 'error';
                        $return['message'] = $tr->tr('Nastala chyba při posílání žádosti.') . ' error code: #100101';
                    }
                }
            }
        }
        // uživatel již byl pozván a čekáme až odpoví
        else
        {
            $return = array(
                'status'  => 'error',
                'message' => $tr->tr('Již jste zažádal o vstup do tohoto týmu, vyčkejte na potvrzení správcem týmu.')
            );
        }
    }
    
    echo json_encode($return);
    die;
}



if(isset($_GET['removeFan']) && !empty($logged_user->id) && intval($_GET['removeFan']) > 0 && isset($_GET['team_hash']) && !empty($_GET['team_hash']))
{
    $id = intval($_GET['removeFan']);
    $team = TeamDAO::get($id);
    if($_GET['team_hash'] == $team->getHash(TEAM_SALT))
    {
        $team->removeFan($logged_user->id);
        $logged_user->getTeamList(true);
        $_project['session']->set('logged', $logged_user);
        // FIXME
        //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
        
        // nacteme odkazy, ktere budeme zobrazovat
        require(BLOCK_PATH.'team-links-visitor.php');

        $fanCount = intval(count($team->getFanList()));
        $observerCount = intval(count($team->getObserverList()));
        
        if(isset($_GET['search']) && $_GET['search'] == 1)
        {
            $fanCount .= ' ' . decline($fanCount, 'fanoušek', 'fanoušci', 'fanoušků'); 
            $observerCount .= ' ' . decline($observerCount, 'odběratel', 'odběratelé', 'odběratelů');
        }

        $return = array(
            'status'     => 'ok',
            'fansCount'  => $fanCount,
            'observersCount'  => $observerCount,
            'html'       => $linksHtml
        );
    }
    else
    {
        $return = array(
            'status'     => 'error',
            'message' => $tr->tr('Nastala chyba. Zkuste to později znovu.')
        );
    }
    
    echo json_encode($return);
}


if(isset($_GET['removeObserver']) && !empty($logged_user->id) && intval($_GET['removeObserver']) > 0 && isset($_GET['team_hash']) && !empty($_GET['team_hash']))
{
    $id = intval($_GET['removeObserver']);
    $team = TeamDAO::get($id);
    if($_GET['team_hash'] == $team->getHash(TEAM_SALT_OBS))
    {
        $team->removeObserver($logged_user->id);
        $logged_user->getTeamList(true);
        $_project['session']->set('logged', $logged_user);
        // FIXME
        //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS, '/');
        
        // nacteme odkazy, ktere budeme zobrazovat
        require(BLOCK_PATH.'team-links-visitor.php');

        $fanCount = intval(count($team->getFanList()));
        $observerCount = intval(count($team->getObserverList()));
        
        if(isset($_GET['search']) && $_GET['search'] == 1)
        {
            $fanCount .= ' ' . decline($fanCount, 'fanoušek', 'fanoušci', 'fanoušků'); 
            $observerCount .= ' ' . decline($observerCount, 'odběratel', 'odběratelé', 'odběratelů');
        }

        $return = array(
            'status'     => 'ok',
            'fansCount'  => $fanCount,
            'observersCount'  => $observerCount,
            'html'       => $linksHtml
        );
    }
    else
    {
        $return = array(
            'status'     => 'error',
            'message' => $tr->tr('Nastala chyba. Zkuste to později znovu.')
        );
    }
    
    echo json_encode($return);
}

if(isset($_GET['removeMember']) && !empty($logged_user->id) && intval($_GET['removeMember']) > 0 && isset($_GET['team_hash']) && !empty($_GET['team_hash']))
{
    $id = intval($_GET['removeMember']);
    $team = TeamDAO::get($id);
    if($_GET['team_hash'] == $team->getHash(TEAM_SALT_ENTRY))
    {   
        // musime odebrati tu pozvanku, aby ho mohl zkusit pozvat znovu
        $request = array(
            'team_id'   => $team->id,
            'user_id'   => $logged_user->id
        );
        if(RosterInvites::cancelRequest($request))
        {
            // nacteme odkazy, ktere budeme zobrazovat
            require(BLOCK_PATH.'team-links-visitor.php');
                    
            $return = array(
                'status'     => 'ok',
                'html'       => $linksHtml
            );
        }

        // nacteme odkazy, ktere budeme zobrazovat
        if($team->isFan($logged_user))
        {
            require(BLOCK_PATH.'team-links-fan.php');
        }
        elseif($team->isObserver($logged_user))
        {
            require(BLOCK_PATH.'team-links-observer.php');
        }
        else
        {
            require(BLOCK_PATH.'team-links-visitor.php');
        }

        $fanCount = intval(count($team->getFanList()));
        $observerCount = intval(count($team->getObserverList()));
        
        if(isset($_GET['search']) && $_GET['search'] == 1)
        {
            $fanCount .= ' ' . decline($fanCount, 'fanoušek', 'fanoušci', 'fanoušků'); 
            $observerCount .= ' ' . decline($observerCount, 'odběratel', 'odběratelé', 'odběratelů');
        }

        $return = array(
            'status'     => 'ok',
            'fansCount'  => $fanCount,
            'observersCount'  => $observerCount,
            'html'       => $linksHtml
        );
    }
    else
    {
        $return = array(
            'status'     => 'error',
            'message' => $tr->tr('Nastala chyba. Zkuste to později znovu.')
        );
    }
    
    echo json_encode($return);
}
