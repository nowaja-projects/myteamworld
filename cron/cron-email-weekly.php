<?php

define('PATH', '/var/www/myteamworld.com/beta/');

require_once __DIR__ . '/../include/config.php';

ini_set('display_erros', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/../include/functions.php';
require_once __DIR__ . '/../include/init.php';

//require_once __DIR__ . '/../include/preload.php';


$sql = 'SELECT * 
		FROM `user_settings`
		WHERE `key` = "'.UserSettings::KEY_EMAIL_EVENT_WEEK.'" AND `value` = "'.UserSettings::VAL_EMAIL_YES.'"';

$db = new Database();

$attend_cache = array();

if($db->query($sql))
{
	while($row = $db->readrow('aarr'))
	{
		$email_events = array();
		$processed = array();
		$send = false;

		$user = UserDAO::get($row['user_id']);

		if(!$user || empty($user->id))
		{
			continue;
		}

		// poustime vzdy v pondeli rano
		$from = date('Y-m-d', strtotime('this monday')) . ' 00:00:00';
		$to = date('Y-m-d', strtotime('next sunday')) . ' 23:59:59';

		$teams = $user->getTeamList();

		$events = EventDAO::getEventsForUser(array_keys($teams), $from, $to, $user);

		foreach($events as $event)
		{
			// pokud je uzivatel pozvany a zaroven ta udalost jeste nebyla zpracovana
			if($event->isInvited($user) && !in_array($event->team_id . '-' . $event->id, $processed))
			{
				if(isset($attend_cache[$event->team_id . '-' . $event->id]))
				{
					$attend = $attend_cache[$event->team_id . '-' . $event->id];
				}
				else
				{
					$a = AttendDAO::getAttendForEvent($event->team_id, $event->id);
					$attend = array();
					foreach($a as $val)
					{
						$attend[$val['user_id']] = $val['attend'];
					}

					$attend_cache[$event->team_id . '-' . $event->id] = $attend;
				}

				// pokud uz uzivatel vyplnil dochazku a nezucastni se, tak ho s tim nebudem otravovat
				if(isset($attend[$user->id]) && ($attend[$user->id] == 'na' || $attend[$user->id] == 'blackdot'))
				{
					continue;
				}

				// uzivateluv tym je domaci
				if(in_array($event->team_id, array_keys($teams)))
				{
					$user_team = $event->team_id;
				}
				else
				{
					$user_team = $event->oponent_id;
				}

				$email_events[$user_team][$event->start . '-' . $event->team_id . '-' . $event->id] = array(
					'event' => $event,
					'attend_set' => isset($attend[$user->id])
				);
				$send = true;
				$processed[] = $event->team_id . '-' . $event->id;
			}
		}

		if($send)
		{
			echo '<p>Odesláno uživateli ' . $user->getName() . ' na adresu ' . $user->email . "</p>\n";

			SendMail::sendEventOverview($user->email, $email_events, 'week');
		}
	}
}