<?php
class AttendDAO extends ObjectDAO
{
    public static function getAttendForEvent($team_id = 0, $event_id = 0)
	{
        $team_id = intval($team_id);
        $event_id = intval($event_id);
        
        if(!empty($team_id) && !empty($event_id))
        {
    		$sql = 'SELECT * 
                    FROM `attend`
                    WHERE `team_id` = '.$team_id.' AND `event_id` = ' . $event_id;
                    
            $return = array(); 
            $db = new Database();
            if($db->query($sql))
            {
                while($row = $db->readrow('aarr'))
                {
                    $return[$row['user_id']] = $row;
                }
            }

            return $return;
        }
        
        return array();
	}
    
	public static function getAttendForEvents($events = array())
	{
        $where = array();
        $add_where = '';
        if(count($events) > 0)
        {
    		foreach($events as $event)
            {
                if(!empty($event->id) && !empty($event->team_id))
                {
                    $where[] = '(`event_id` = '.$event->id.' AND `team_id` = '.$event->team_id.')';
                }
            }
            $add_where .= ' WHERE ' . implode(' OR ', $where);
        }
        
        $db = new Database();
        $sql = 'SELECT * 
                FROM `attend`
                ' . $add_where;
                   
        $return = array(); 
        if($db->query($sql))
        {
            while($row = $db->readrow('aarr'))
            {
                $key = $row['team_id'] . '-' . $row['event_id'];
                $return[$key][$row['user_id']] = $row;
            }
        }
        
        return $return;
	}
    
    public static function insert($data = array())
    {
        $db = new Database();
        
        $sql = 'INSERT INTO `attend` 
                SET 
                    `team_id`       = "'.intval($data['team_id']).'", 
                    `event_id`      = "'.intval($data['event_id']).'",
                    `user_id`       = "'.intval($data['user_id']).'",
                    `date`          = NOW(),
                    `attend`        = "'.mysqli_real_escape_string($db->mysqli(), $data['attend']).'",
                    `note`          = "'.mysqli_real_escape_string($db->mysqli(), $data['note']).'",
                    `inserted_by`   = "'.mysqli_real_escape_string($db->mysqli(), $data['inserted_by']).'"';
                    
        return self::nonEmpty($sql); 
    }
    
    public static function update($data = array())
    {
        $db = new Database();
        
        $sql = 'UPDATE `attend` 
                SET 
                    `date`          = NOW(),
                    `attend`        = "'.mysqli_real_escape_string($db->mysqli(), $data['attend']).'",
                    `note`          = "'.mysqli_real_escape_string($db->mysqli(), $data['note']).'",
                    `inserted_by`   = "'.mysqli_real_escape_string($db->mysqli(), $data['inserted_by']).'"
                WHERE 
                    `team_id`       = "'.intval($data['team_id']).'" AND 
                    `event_id`      = "'.intval($data['event_id']).'" AND
                    `user_id`       = "'.intval($data['user_id']).'"';
                    
        return self::nonEmpty($sql);
    }
    
    public static function get($data = array())
    {
        $sql = 'SELECT * 
                FROM `attend`
                WHERE 
                    `team_id`       = "'.intval($data['team_id']).'" AND 
                    `event_id`      = "'.intval($data['event_id']).'" AND
                    `user_id`       = "'.intval($data['user_id']).'"';
                    
        return self::load($sql);
    }
}
