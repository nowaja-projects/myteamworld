<?php
class Object
{
	protected $table 	= '';
	protected $columns 	= array();
	protected $data 	= array();
	protected $errors 	= array();
	
	public function __set($name,$value)
	{
		return $this->set($name,$value);
	}
	
	public function set($var,$value)
	{
		if($this->exists($var))
		{
			$this->data[$var] = $value;
		}
		
		return $this;	
	}
	
	public function __get($name)
	{
		return $this->get($name);
	}
	
	public function get($var)
	{
		if($this->isDataSet($var))
		{
			return $this->data[$var];
		}
		
		return null;
	}
	
	public function exists($key = '')
	{
		return array_key_exists($key,$this->columns);
	}
	
	public function __isset($key)
	{
		return $this->isDataSet($key);
	}
	
	public function isDataSet($key = '')
	{
		return isset($this->data[$key]);
	}
	
	public function setTable($table = '')
	{
		$this->table = $table;
	}
	
	public function getTable()
	{
		return $this->table;
	}
	
	public function getAllData()
	{
		return $this->data;
	} 
	
	public function parseAndSet($data = array())
	{
		foreach($data as $key => $value)
		{
			$key = str_replace($this->getTable().'_','',$key);
			
			$this->set($key,$value);
		}
		/*
		foreach($this->columns as $key => $value)
		{
			if(isset($data[$key]))
			{
				$key = str_replace($this->getTable().'_','',$key);
			
				$this->set($key, $data[$key]);
			}
		}
		 */	
	}
	
	public function validate()
	{
		$valid = true;
        
        $this->errors = array();
        
		foreach($this->columns as $key => $type)
		{
            if(!empty($type) && !Validate::valid($type, $this->get($key)))
            {
			    $this->errors[$key] = $key;
                $valid = false;   
            }

		}
		
		return $valid;	
	}
    
    public function getErrors()
    {
        return $this->errors;
    }
}