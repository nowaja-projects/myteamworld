<?php
class SearchDAO extends ObjectDAO
{
    public static function lookupTeams($phrase = '', &$view_more_teams = false, &$team_count = 0, $start = 0, $limit = SEARCH_LIMIT)
	{
        $searched = array();
        $sphinx = new SphinxClient();
        $sphinx->SetServer('localhost', 9312);

        $sphinx->setLimits(0, 200);
        $index = 'index_' . SPHINX_PREFIX . 'teams';
        $result = $sphinx->Query($phrase, $index, 'vyhledavani');

        if($result['status'] === 0)
        {
            if(isset($result['matches']))
            {
                $team_count = count($result['matches']);

                if($team_count > $limit)
                {
                    $result['matches'] = array_slice($result['matches'], $start, $limit + 1);
                    if(count($result['matches']) > $limit)
                    {
                        $view_more_teams = true;
                    }
                    $result['matches'] = array_slice($result['matches'], 0, $limit);
                }

                foreach($result['matches'] as $info)
                {
                    $team = TeamDAO::get($info['attrs']['id']);
                    
                    if($team instanceof Team && $team->id > 0)
                    {
                        $searched[] = $team;
                    }
                }
            }
            else
            {
                $team_count = 0;
            }
        }
        elseif(TESTIP)
        {
            printr($sphinx->getLastError());

            return array();
        }

        return $searched;
	}

    public static function lookupUsers($phrase = '', &$view_more_users = false, &$user_count = 0, $start = 0, $limit = SEARCH_LIMIT)
	{
        $searched_users = array();
        $sphinx = new SphinxClient();
        $sphinx->SetServer('localhost', 9312);

        $sphinx->SetFieldWeights(array(
            'user_fname'  => 10,
            'user_sname'  => 10,
            'user_email'  => 5
        ));

        // $sphinx->setSortmode(SPH_SORT_EXTENDED, '@weight desc, usr_article_date_show desc');
        // 
        $sphinx->setLimits(0, 200);
        $index = 'index_' . SPHINX_PREFIX . 'users';
        $result = $sphinx->Query($phrase, $index, 'vyhledavani');

        if($result['status'] === 0)
        {
            if(isset($result['matches']))
            {
                $user_count = count($result['matches']);

                if($user_count > $limit)
                {
                    $result['matches'] = array_slice($result['matches'], $start, $limit + 1);
                    if(count($result['matches']) > $limit)
                    {
                        $view_more_users = true;
                    }
                    $result['matches'] = array_slice($result['matches'], 0, $limit);
                }

                foreach($result['matches'] as $info)
                {
                    $user = UserDAO::get($info['attrs']['id']);
                    if($user instanceof $user && $user->id > 0)
                    {
                        $user->getTeamList();
                        $searched_users[] = $user;
                    }
                }
            }
            else
            {
                $user_count = 0;
            }
        }
        elseif(TESTIP)
        {
            printr($sphinx->getLastError());

            return array();
        }

        return $searched_users;
	}


    public static function lookupCities($phrase = '')
    {
        $searched = array();
        $sphinx = new SphinxClient();
        $sphinx->SetServer('localhost', 9312);

        $sphinx->SetFieldWeights(array(
            'name'      => 100,
            'region'    => 10,
            'country'   => 1
        ));

        $sphinx->setLimits(0, 3);
        $index = 'index_' . SPHINX_PREFIX . 'cities';
        $result = $sphinx->Query($phrase, $index, 'vyhledavani');

        if($result['status'] === 0)
        {
            if(isset($result['matches']))
            {
                return $result['matches'];
            }
            else
            {
                return array();
            }
        }
        elseif(TESTIP)
        {
            printr($sphinx->getLastError());

            return array();
        }
    }
}
?>