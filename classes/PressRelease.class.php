<?php
class PressRelease extends Object 
{
    protected $columns = array(
        'id'         => '',
        'parent_id'  => '',
        'user_id'    => '',
        'team_id'    => '',
        'type'       => '',
        'created'    => '',
        'heading'    => '',
        'perex'      => '',
        'text'       => '',
        'date_begin' => '',
        'date_end'   => ''
    );

    public function __construct($data = array())
  	{
        $this->setTable('object');
        
		if(is_array($data) && count($data))
		{
		  	$this->parseAndSet($data);
        }
  	}
    
    public function getLink()
    {
        // FIXME
        return '#';
    }
}
?>