<?php

class Gallery extends Object
{
	protected $columns = array(
		'id'       		=> 'integer',
		'team_id'  		=> '',
		'name'     		=> '',
		'order'	   		=> '',
		'item_count' 	=> '',
		'main_photo_id'	=> '',
		'main_photo'	=> '',
		'main_extension'=> '',
		'date'			=> ''
	);
	

	public function __construct($data = array())
	{
		$this->setTable('gallery');

		if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}        
	}

	public function getName()
	{
		return $this->get('name');
	}

	public function getLink()
	{
		$team = TeamDAO::get($this->get('team_id'));
		
		return $team->getProfileLink('gallery-detail') . $this->get('team_id') . '-' . $this->get('id') . '/';
	}
	
	public function getPhotoList()
	{
		return GalleryDAO::getPhotoList($this->team_id, $this->id);
	}

	public function getPhotoPath()
	{
		return TEAM_DATADIR . $this->team_id . '/gallery' . $this->id . '/';
	}

	public function getLastUpdate()
	{
		return date('j. n. Y', strtotime($this->date));
	}
}
