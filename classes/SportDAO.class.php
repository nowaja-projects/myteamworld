<?php
class SportDAO extends ObjectDAO
{
	public static function get($id = 0)
	{
		$sql = 'SELECT * FROM `sport` WHERE `sport_id` = '.intval($id);

		return self::load($sql, 'Sport');
	}
	
	public static function getSelectList()
	{
		$db = new Database();
		
		$return = array();
		
		$sql = 'SELECT * FROM sport WHERE sport_list = 1 ORDER BY sport_name';
		
		if($db->query($sql))
		{
			while($res = $db->readrow('aarr'))
			{
				$return[$res['sport_id']] = $res['sport_name'];
			}
		}
		return $return;
	}
    
    public static function getAdjectiveList()
	{
		$db = new Database();
		
		$return = array();
		
		$sql = 'SELECT * FROM `sport` ORDER BY `sport_name`';
		
		if($db->query($sql))
		{
			while($res = $db->readrow('aarr'))
			{
				$return[$res['sport_id']] = $res['sport_adjective'];
			}
		}

		return $return;
	}
	
	public static function getList()
	{
		$sql = 'SELECT * FROM sport WHERE sport_list = 1 ORDER BY sport_name';
		
		return self::loadArray($sql,'Sport');
	}
    
    public static function getListWithRSS()
    {
        $db = new Database();
        
        $sql = 'SELECT * 
                FROM sport
                ORDER BY sport_id';
        
        $db->query($sql);
        while($a = $db->readrow('aarr'))
        {
            $sport[$a['sport_id']]['name'] = $a['sport_name'];
            $sport[$a['sport_id']]['source'] = $a['sport_source'];
        }
        
        
        $sql = 'SELECT * 
                FROM `rss_source`';
        
        $db->query($sql);
        while($a = $db->readrow('aarr'))
        {
            $sport[$a['rss_source_sport_id']]['branches'][$a['rss_source_id']]['title'] = $a['rss_source_title'];
            $sport[$a['rss_source_sport_id']]['branches'][$a['rss_source_id']]['link'] = $a['rss_source_link'];
        }
        
        return $sport;
    }
    
    public static function getPositions($id = 0)
    {
        if(intval($id) > 0)
        {
            $sql = 'SELECT *
                    FROM `roster_position`
                    WHERE `sport_id` = '.intval($id) . '
                    ORDER BY `order` ASC';
            
            return self::loadArray($sql, 'Position');
        }
        
        return array();
    }

    public static function getPosition($id = 0)
    {
        if(intval($id) > 0)
        {
            $sql = 'SELECT *
                    FROM `roster_position`
                    WHERE `id` = '.intval($id) . '
                    ORDER BY `order` ASC';
            
            return self::load($sql, 'Position');
        }
        
        return false;
    }
}
?>