<?php
class RosterItem extends Object
{
	protected $columns = array(
        'user_id'      => 'required',
        'position_id'  => '',
		'number'       => '',
        'function'     => '',
        'host'		   => '',
        'show'		   => ''
	);
	
	public function __construct($data = array())
	{
		$this->setTable('user_in_team');
		
        if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}
	}

	public function isHost()
	{
		return !empty($this->host);
	}
    
}
?>