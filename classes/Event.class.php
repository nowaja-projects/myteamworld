<?php
class Event extends Object 
{
    const COMPETITION = 'match';
    const FRIENDLY    = 'friendlymatch';
    const TRAINING    = 'training';
    const ACTION      = 'action'; 
    
    public $columns = array(
        'id'            => '',
        'user_id'       => '',
        'team_id'       => '',
        'season_id'     => '',
        'oponent_season_id' => '',
        'oponent_id'    => '',
        'type'          => '',
        'created'       => '',
        'start'         => '',
        'end'           => '',
        'info'          => '',
        'confirmed_home'     		=> '',
        'confirmed_away'     		=> '',
        'result_confirmed_home'     => '',
        'result_confirmed_away'     => '',
        'deleted_home'  => '',
        'deleted_away'  => '',
        'result'        => '',
        'rosters'       => '',
        'team_stats'    => ''
    );

    public function __construct($data = array())
  	{
        $this->setTable('object');
        
		if(is_array($data) && count($data))
		{
		  	$this->parseAndSet($data);
        }
  	}
    
    public function save()
    {
        if($this->get('id'))
		{
			return EventDAO::update($this->getAllData());
		}
		else
		{
			return EventDAO::insert($this->getAllData());
		}
    }
    
    public function userCanEdit($logged_user)
    {
        // nacteme si tym, ktery udalost vytvoril
        $team = TeamDAO::get($this->team_id);
        
        // pokud je uzivatel adminem tohoto tymu, 
        if($team->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id)
        {
            return true;
        }
        
        if(empty($this->oponent_id))
        {
            return false;
        }
        else
        {
            // tym proti kteremu hrajeme
            $team = TeamDAO::get($this->oponent_id);
            
            // pokud je uzivatel adminem tohoto tymu
            if($team->isAdmin($logged_user) && $logged_user->getActiveTeam()->id == $team->id)
            {
                return true;
            }
        }
        
        return false;
    }

    public function isConfirmed($team = false)
    {
        if($team instanceof Team)
        {
        	// hoste potvrdili, domaci jeste ne
        	if(!empty($this->confirmed_away) && empty($this->confirmed_home))
        	{
        		if($now >= date('Y-m-d H:i:s', strtotime($this->confirmed_away . ' +3days')))
        		{
        			// FIXME potvrdit domaci
        		}
        	}

        	// domaci potvrdili, hoste jeste ne
        	if(!empty($this->confirmed_home) && empty($this->confirmed_away))
        	{
        		if($now >= date('Y-m-d H:i:s', strtotime($this->confirmed_home . ' +3days')))
        		{
        			// FIXME potvrdit hoste
        		}
        	}

            // pokud nas zajima domaci tym
            if($this->team_id == $team->id)
            {
                return !empty($this->confirmed_home);
            }
            elseif($this->oponent_id == $team->id)
            {
                return !empty($this->confirmed_away);
            }
        }

        return (!empty($this->confirmed_home) && !empty($this->confirmed_away));
    }

    public function isResultConfirmed($team = false)
    {
    	$now = date('Y-m-d H:i:s');

    	// hoste potvrdili, domaci jeste ne
    	if(!empty($this->result_confirmed_away) && empty($this->result_confirmed_home))
    	{
    		if($now >= date('Y-m-d H:i:s', strtotime($this->result_confirmed_away . ' +3days')))
    		{
    			// FIXME poslat notifikaci tymu, vlozit na nastenku
    			EventDAO::confirmResult($this->team_id, $this->id, 'home');

    			return true;
    		}
    	}

    	// domaci potvrdili, hoste jeste ne
    	if(!empty($this->result_confirmed_home) && empty($this->result_confirmed_away))
    	{
    		if($now >= date('Y-m-d H:i:s', strtotime($this->result_confirmed_home . ' +3days')))
    		{
    			// FIXME poslat notifikaci tymu, vlozit na nastenku
    			EventDAO::confirmResult($this->team_id, $this->id, 'away');

    			return true;
    		}
    	}

        if($team instanceof Team)
        {
        	// pokud nas zajima domaci tym
            if($this->team_id == $team->id)
            {
                return !empty($this->result_confirmed_home);
            }
            elseif($this->oponent_id == $team->id)
            {
                return !empty($this->result_confirmed_away);
            }
        }
        
        return (!empty($this->result_confirmed_home) && !empty($this->result_confirmed_away));
    }

    public function hasResult()
    {
        return !empty($this->result);
    }

    public function isDeleted($team = false)
    {
        if($team instanceof Team)
        {
            // pokud nas zajima domaci tym
            if($this->team_id == $team->id)
            {
                return !empty($this->deleted_home);
            }
            elseif($this->oponent_id == $team->id)
            {
                return !empty($this->deleted_away);
            }
        }

        return (!empty($this->deleted_home) && !empty($this->deleted_away));
    }

    public function isPartDeleted($team = false)
    {
        return (!empty($this->deleted_home) || !empty($this->deleted_away));
    }

    public function getExtraTime()
    {
        if(!is_array($this->result))
        {
            $this->result = unserialize(@$this->result);
        }

        if(!is_array($this->result))
        {
            return '';
        }

        $team = TeamDAO::get($this->team_id);
        if(empty($team->id))
        {
            return '';
        }

        $sport = SportDAO::get($team->sport_id);
        if(empty($sport->id))
        {
            return '';
        }

        $showOvertime = $showPenalties = $showShootout = false;
        // pokud muze mit sport prodlouzeni
        if(!empty($sport->overtime))
        {
            $showOvertime = true;
        }
        
        if(!empty($sport->penalties))
        {
            $showPenalties = true;
        }
        
        // nebo samostatne najezdy?
        if(!empty($sport->shootout))
        {
            $showShootout = true;
        }

        $extra = '';
        if(!empty($this->result['isPenalties']) && $showPenalties)
        {
            $extra = 'pen.';
        }
        elseif(!empty($this->result['isShootout']) && $showShootout)
        {
            $extra = 'sn';
        }
        elseif(!empty($this->result['isOvertime']) && $showOvertime)
        {
            $extra = 'pp';
        }

        return $extra;
    }

    public function isInvited(User $user)
    {
        if(is_array($this->info))
        {
            $info = $this->info;
        }
        else
        {
            $info = unserialize($this->info);
        }

        $home = TeamDAO::get($this->team_id);

        if($home->isPlayer($user))
        {
            if(!empty($info[0]['attend_team']) || (is_array($info[0]['attend']) && in_array($user->id, array_keys($info[0]['attend']))))
            {
                return true;
            }
        }

        if(!empty($this->oponent_id))
        {
            $away = TeamDAO::get($this->oponent_id);

            if($away->isPlayer($user))
            {
                if(!empty($info[0]['attend_team_away']) || in_array($user->id, array_keys($info[0]['attend_away'])))
                {
                    return true;
                }
            }            
        }

        return false;
    }
}
