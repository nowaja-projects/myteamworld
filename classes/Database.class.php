<?php
class Database
{
	protected $mysqli;
	protected $result;
	protected $rows;
	protected $inserted_id;
	protected $error;
	protected $show_errors;
	
	public function __construct()
	{
		global $config;
		
		$this->show_errors = $config['db']['show_errors'];
		
		$this->mysqli = new mysqli(
			$config['db']['host'],
			$config['db']['user'],
			$config['db']['password'],
			$config['db']['name']
		);
		$this->mysqli->set_charset($config['db']['charset']);
	}
	
	public function setShowErrors($show = true)
	{
		$this->show_errors = $show;
	}
	
	public function mysqli()
	{
		return $this->mysqli;
	}
	
	public function query($sql)
	{
	    /**
         * pro pocitani poctu dotazu
         **/  
        if(!isset($_SESSION['queries']))
        {
            $_SESSION['queries'] = 0;
        }
        $_SESSION['queries']++;

    	if(!isset($_SESSION['queries_info']))
        {
        	$_SESSION['queries_info'] = array();
        }
        $_SESSION['queries_info'][] = $sql;
        
		$this->result = $this->mysqli->query($sql);
		
		if($this->result)
		{
			$this->setResultInfo($sql);
			return true;
		}
		else
		{
			$this->error = 'Database error no.:'.$this->mysqli->errno.': '.$this->mysqli->error . ': ' . $sql;
			if($this->show_errors)
			{
				echo $this->error;
			}
			return false;
		}
		
	}
	
	public function readrow($type = 'aarr')
	{
		switch($type)
		{
			case 'arr' : 
				return $this->result->fetch_row();
			break;
			
			case 'obj' : 
				return $this->result->fetch_object();
			break;
			
			default : 
				return $this->result->fetch_assoc();
			break;
		}
		
	}
	
	public function getAllRows($type = 'aarr')
	{
		switch($type)
		{
			case 'arr' : 
				return $this->result->fetch_all(MYSQLI_NUM);
			break;
			
			case 'obj' : 
				return $this->result->fetch_fields();
			break;
			
			default : 
				return $this->result->fetch_all(MYSQLI_ASSOC);
			break;
		}
	}
	
	public function getRows()
	{
		return $this->rows;
	}
	
	public function getInsertedId()
	{
		return $this->inserted_id;
	}
	
	private function setResultInfo($sql)
	{
		preg_match("/(SHOW|SELECT|DELETE|INSERT|UPDATE)/", strtoupper($sql), $match);
		switch($match[1])
		{
			case 'SELECT': 
			case 'SHOW':
				$this->rows = $this->result->num_rows;	
			break;
				
			case 'INSERT': 
				$this->rows = $this->mysqli->affected_rows;
				$this->inserted_id = $this->mysqli->insert_id;
			break;
			
			default: 
				$this->rows = $this->mysqli->affected_rows;
			break;
		}
	}
	
	public function getErrorMessage()
	{
		return $this->error;
	}
	
	public function escapeString($string)
	{
		return $this->mysqli->escape_string($string);
	}
	
	public function insertArray($data = array(),$table = '')
	{
		$columns = '';
		$values = '';
		
		if(!$table || !is_array($data))
		{
			return false;
		}
		
		$first = true;
		
		foreach($data as $key => $value)
		{
			if(!$first)
			{
				$columns .= ',';
				$values .= ',';
			}
			else
			{
				$first = false;
			}
			
			if(is_string($value))
			{
				$value = '"'.$this->escapeString($value).'"';
			}
			
			$columns .= '`'.$table.'_'.$key.'`';
			$values .= $value;
		}
		
		$sql = 'INSERT INTO `'.$table.'` ('.$columns.') VALUES ('.$values.')';

		return $this->query($sql);
	}
	
	public function updateArray($data = array(),$table = '',$table_id = false)
	{
		$values = '';
		
		if(!$table || !is_array($data))
		{
			return false;
		}
		
		if($table_id === false)
		{
			$table_id = $table.'_id';
		}
		
		$id_value = array_shift($data);
		
		$first = true;
		
		foreach($data as $key => $value)
		{
			if(!$first)
			{
				$values .= ',';
			}
			else
			{
				$first = false;
			}
			
			if(is_string($value))
			{
				$value = '"'.$this->escapeString($value).'"';
			}
			
			$values .= '`'.$table.'_'.$key.'` = '.$value;
		}
		
     	$sql = 'UPDATE `'.$table.'` SET '.$values.' WHERE `'.$table_id.'` = '.$id_value;
		
		return $this->query($sql);
	}
}
?>