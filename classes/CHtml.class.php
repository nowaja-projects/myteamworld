<?php
class CHtml
{
	public static function sanitizeArray($data = array())
	{
		$return = array();
		
		foreach($data as $key => $value)
		{
			if(is_array($value))
			{
				$return[$key] = self::sanitizeArray($value);
			}
			else
			{
				$return[$key] = htmlspecialchars($value);
			}
		}
		
		return $return;
	}
	
	public static function checkGet($name)
	{
		return !empty($_GET[$name]);
	}
	
	public static function checkPost($name)
	{
		return !empty($_POST[$name]);
	}
	
	public static function checkAndSetGet($name,$value)
	{
		if(!self::checkGet($name))
		{
			return $value;
		}
		else
		{
			return $_GET[$name];
		}
	}
	
	public static function checkAndSetPost($name,$value)
	{
		if(!self::checkPost($name))
		{
			return $value;
		}
		else
		{
			return $_POST[name];
		}
	}
	
	public static function getSelect($name = '', $list = array(), $selected = '', $options = '')
	{
		$return  = sprintf('<select name="%s"%s>',
							$name,
							!empty($options) ? ' '.$options : ''
		);
		foreach($list as $key => $value)
		{
			$return .= sprintf('<option value="%s"%s>%s</option>',
								$key,
								((string)$selected == $key ? ' selected="selected"' : ''),
								$value
			);
		}
		
		$return .= '</select>';
		
		return $return;
	}
}
?>