<?php

class GalleryDAO extends ObjectDAO
{
	public static function getGalleryList($team_id = 0)
	{
		$sql = 'SELECT 
					`gallery`.*,
					`gallery_items`.`id` as `main_photo_id`,
					`gallery_items`.`filename` as `main_photo`,
  					`gallery_items`.`extension` as `main_extension`,
					(SELECT `date` FROM `gallery_items` ORDER BY `date` DESC LIMIT 1) as `date`
				FROM `gallery`
				LEFT JOIN `gallery_items` ON `gallery_items`.`gallery_id` = `gallery`.`id`
				WHERE `gallery_items`.`main` = 1 AND `gallery`.`team_id` = ' . intval($team_id) . '
				ORDER BY `gallery`.`order` DESC';

		return self::loadArray($sql, 'Gallery');
	}


	public static function getGallery($team_id = 0, $id = 0)
	{
		$sql = 'SELECT
					`gallery`.*,
					`gallery_items`.`id` as `main_photo_id`,
					`gallery_items`.`filename` as `main_photo`,
  					`gallery_items`.`extension` as `main_extension`,
					(SELECT `date` FROM `gallery_items` ORDER BY `date` DESC LIMIT 1) as `date`
				FROM `gallery`
				LEFT JOIN `gallery_items` ON `gallery_items`.`gallery_id` = `gallery`.`id`
				WHERE `gallery_items`.`main` = 1 AND `gallery`.`team_id` = ' . intval($team_id) . ' AND `gallery`.`id` = ' . intval($id) . '
				ORDER BY `gallery_items`.`date` DESC';

		return self::load($sql, 'Gallery');
	}
	
	public static function getPhotoList($team_id = 0, $gallery_id = 0)
	{
		$sql = 'SELECT * 
				FROM `gallery_items`
				WHERE `gallery_id` = ' . intval($gallery_id) . ' AND `team_id` = ' . intval($team_id) . '
				ORDER BY `order` ASC';

		return self::loadArray($sql);
	}

	public static function setMainPhoto($team_id, $gallery_id, $photo_id = 0)
	{
		$photo_id = intval($photo_id);

		if(!empty($photo_id))
		{
			$sql = 'UPDATE `gallery_items` 
					SET `main` = 0
					WHERE `team_id` = ' . intval($team_id) . ' AND `gallery_id` = ' . intval($gallery_id);

			self::nonEmpty($sql);
		}

		$sql = 'UPDATE `gallery_items` 
				SET `main` = 1
				WHERE `team_id` = ' . intval($team_id) . ' AND `gallery_id` = ' . intval($gallery_id) . ($photo_id ? ' AND `id` = ' . intval($photo_id) : '') . '
				LIMIT 1';

		return self::nonEmpty($sql);

	}
	
	public static function getMaxGalleryId($team_id = 0)
	{
		$db = new Database();

		$sql = 'SELECT MAX(`order`) as maximumorder
				FROM `gallery`
				WHERE `team_id` = ' . intval($team_id);

		$return = array(
			'order'	=> 10
		);

		if($db->query($sql))
		{
			$row = $db->readrow('aarr');

			$return = array(
				'order'	=> ($row['maximumorder'] + 10)
			);
		}
		
		return $return;
	}

	public static function insertGallery($data = array())
	{
		$db = new Database();

		$maximum = self::getMaxGalleryId($data['team_id']);

		$sql = 'INSERT INTO `gallery`
				SET 
					`team_id`		= "'.intval($data['team_id']).'",
					`name`			= "'.mysqli_real_escape_string($db->mysqli(), $data['name']).'",
					`order` 		= "'.intval($maximum['order']).'",
					`item_count`	= "0"';

	  	if($db->query($sql))
	  	{
	  		if(!self::createGalleryFolder($data['team_id'], $db->getInsertedId()))
	  		{
	  			return false;
	  		}

	  		$maximum['id'] = $db->getInsertedId();
			return $maximum;
		}

		return false;
	}

	public static function updateGallery($data = array())
	{
		$db = new Database();

		$sql = 'UPDATE `gallery`
				SET `name` = "'.mysqli_real_escape_string($db->mysqli(), $data['name']).'"
				WHERE `team_id` = ' . intval($data['team_id']) . ' AND `id` = ' . intval($data['id']);

		return self::nonEmpty($sql);
	}


	public static function updateGalleryItem($data = array())
	{
		$db = new Database();

		$sql = 'UPDATE `gallery_items`
				SET `description` = "'.mysqli_real_escape_string($db->mysqli(), $data['name']).'"
				WHERE `team_id` = ' . intval($data['team_id']) . ' AND `id` = ' . intval($data['id']) . ' AND `gallery_id` = ' . intval($data['gallery_id']);

		return self::nonEmpty($sql);
	}

	public static function deleteGallery($team_id, $gallery_id)
	{
		$sql = 'DELETE FROM `gallery_items` 
				WHERE `team_id` = ' . intval($team_id) . ' AND `gallery_id` = ' . intval($gallery_id);
		self::nonEmpty($sql);


		$sql = 'DELETE FROM `gallery` 
				WHERE `team_id` = ' . intval($team_id) . ' AND `id` = ' . intval($gallery_id);
		return self::nonEmpty($sql);		
	}

	public static function deleteItem($team_id, $gallery_id, $photo_id)
	{
		$sql = 'SELECT * FROM `gallery_items` 
				WHERE `team_id` = ' . intval($team_id) . ' AND `gallery_id` = ' . intval($gallery_id) . ' AND `id` = ' . intval($photo_id) . ' AND `main` = 1';

		$main = 0;
		if($row = self::load($sql))
		{
			$main = 1;
		}

		$sql = 'DELETE FROM `gallery_items` 
				WHERE `team_id` = ' . intval($team_id) . ' AND `gallery_id` = ' . intval($gallery_id) . ' AND `id` = ' . intval($photo_id);

		$return = self::nonEmpty($sql);

		self::setMainPhoto($team_id, $gallery_id);

		return $return;
	}

	public static function getMaxItemId($team_id = 0, $gallery_id = 0)
	{
		$db = new Database();

		$sql = 'SELECT MAX(`order`) as maximumorder
				FROM `gallery_items`
				WHERE `team_id` = ' . intval($team_id) . ' AND `gallery_id` = ' . intval($gallery_id);

		$return = array(
			'id'	=> 1,
			'order'	=> 10
		);

		if($db->query($sql))
		{
			$row = $db->readrow('aarr');

			$return = array(
				'order'	=> ($row['maximumorder'] + 10)
			);
		}

		return $return;
	}

	public static function insertItem($data = array())
	{
		$maximum = self::getMaxItemId($data['team_id'], $data['gallery_id']);

		$db = new Database();

		$sql = 'INSERT INTO `gallery_items`
				SET 
					`team_id`		= "'.intval($data['team_id']).'",
					`gallery_id`	= "'.intval($data['gallery_id']).'",
					`filename`		= "'.mysqli_real_escape_string($db->mysqli(), $data['filename']).'",
					`extension`		= "'.mysqli_real_escape_string($db->mysqli(), $data['extension']).'",
					`description`	= "'.mysqli_real_escape_string($db->mysqli(), $data['description']).'",
					`order`			= "'.intval($maximum['order']).'",
					`date`			= "'.mysqli_real_escape_string($db->mysqli(), $data['date']).'",
					`main`			= "'.intval($data['main']).'"';

		if($a = $db->query($sql))
		{
			self::updateGalleryCount($data['team_id'], $data['gallery_id']);
		}

		$maximum['id'] = $db->getInsertedId();
		return $maximum;
	}

	public static function updateGalleryCount($team_id, $gallery_id)
	{
		$sql = 'UPDATE 
				  `gallery` 
				SET
				  `item_count` = (SELECT COUNT(*) FROM `gallery_items` WHERE `team_id` = '.intval($team_id).' AND `gallery_id` = '.intval($gallery_id).')';

		  $db = new Database();

		  return $db->query($sql);
	}

	public static function uploadPhotos($files, $team_id, $gallery_id)
	{
		$path = TEAM_DATADIR . $team_id . '/gallery' . $gallery_id . '/';

		if(!empty($files['file']) && !empty($files['file']['name']) && !empty($files['file']['tmp_name']))
		{
			$new_name 	= intval(microtime(true) * 100000000) . '_' . hash_file('sha256', $files['file']['tmp_name']);
			$ext 		= mb_strtolower('.' . pathinfo($files['file']['name'], PATHINFO_EXTENSION));
			$tmp 		= $files['file']['tmp_name'];

			// zajisteni nekolize
			while(file_exists($path . $new_name . 's' . $ext))
			{
				$new_name = intval(microtime(true) * 100000000) . '_' . hash_file('sha256', $tmp);
			}

			$big_name		= $new_name . 'b' . $ext;
			// $medium_name	= $new_name . 'm' . $ext;
			// $small_name		= $new_name . 's' . $ext;

		
			GenerateGalleryThumbnail($tmp, $path . $big_name, 		GALLERY_BIG_W, GALLERY_BIG_H, 90);
			// GenerateGalleryThumbnail($tmp, $path . $medium_name, 	GALLERY_MED_W, GALLERY_MED_H, 90);
			// GenerateThumbnail($tmp, $path . $small_name, 	GALLERY_SMA_W, GALLERY_SMA_H, 90, false);

			//move_uploaded_file($files['file']['tmp_name'], $path . $big_name);

			return array(
				'description' 	=> '',
				'filename'		=> $new_name,
				'extension'		=> $ext
			);
		}
	}

	public static function createGalleryFolder($team_id, $gallery_id)
	{
		$folder = TEAM_DATADIR . $team_id . '/gallery' . $gallery_id . '/';
		
		if(!file_exists($folder))
		{
			if(!mkdir($folder))
			{
				return false;
			}
		}

		return true;
	}
}
