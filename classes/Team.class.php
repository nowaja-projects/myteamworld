<?php
class Team extends Object
{
	protected $columns = array(
		'id'          => '',
        'user_id'     => '',
        'prefix'      => '',
		'name'        => 'required',
        'key'         => '',
		'sport_id'    => 'required',
        'category'    => '',
		'logo'        => '',
		'league'      => '',
		'league_web'  => '',
		'city'        => '',
        'locationID'  => 'integer',
		'founded'     => '',
		'colors'      => '',
		'contact_name'    => 'required',
		'contact_phone'   => '',
		'contact_email'   => 'required',
        'web'             => '',
        'version'         => '',
        'modules'         => 'required',
        'players'         => '',
        'fans'            => '',
        'observers'       => '',
        'show_request'    => ''
	);
	
    public $fanList         = false;
    public $observerList    = false;
    public $adminList       = false;
    public $playerList      = false;
    public $playerListO     = false;
    public $invitationList  = false;
    public $wall            = false;
    public $seasons         = false;
    public $activeSeason    = false;
    
	public function __construct($data = array())
	{
		$this->setTable('team');
		
        if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}
	}
    
    public function setModules(array $modules)
    {
        $this->modules = serialize($modules);
        
        return $this;
    }
    
    public function getModules()
    {
        $imodules = unserialize($this->modules);
        $modules = array();

        // musime zkontrolovat, zda ma nejakou hodnotu pro vsechny dostupne moduly
        global $global_modules;

        foreach($global_modules as $key => $module)
        {
            if(!isset($imodules[$key]))
            {
                $modules[$key] = $module['default'];
            }
            // je to tak schvalne, abychom zachovali poradi modulu, jak jsou nadefinovany v configu
            else
            {
                $modules[$key] = $imodules[$key];
            }
        }

        return $modules;
    }
    
    public function userCanViewModule($key, $logged_user, $check_active = true)
    {
        // nacteme si vsechny moduly
        $team_modules = $this->getModules();
        
        if($key == 'gallery-insert')
        {
            $key = 'gallery';
        }
        elseif(in_array($key, array('roster-insert', 'roster-delete')))
        {
            $key = 'roster';
        }
        elseif($key == 'press')
        {
            $key = 'press-releases';
        }
        elseif($key == 'teampost')
        {
            return $this->isPlayer($logged_user);
        }
        elseif($key == 'event-result')
        {
            $key = 'events';
        }

        if(isset($team_modules[$key]))
        {
            // pokud je modul nastaveny na "all" maji k nemu pristup vsichni
            if($team_modules[$key] == 'all')
            {
                return true;
            }
            
            // kdyz je nastaven na "members", mohou do nej jen lidi z tymu
            if($team_modules[$key] == 'members')
            {
                // pokud je uzivatel clenem tymu
                if($this->isPlayer($logged_user) || $this->isAdmin($logged_user))
                {
                    if(!$check_active)
                    {
                        return true;
                    }


                    // tak zkontrolujeme, zda je to jeho aktivni tym
                    if($logged_user->getActiveTeam()->id == $this->id)
                    {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    public function getName()
    {
        return ((!empty($this->prefix) ? $this->prefix . ' ' : '') . $this->name);
    }
    
    public function getTeamLink()
    {
        return '<a href="'.$this->getProfileLink().'" title="Zobrazit detail týmu '.$this->getName().'">'.$this->getName().'</a>';
    }
    
    public function save()
	{
		if($this->get('id'))
		{
			return TeamDAO::update($this->data);
		}
		else
		{
			return TeamDAO::insert($this->data);
		}
	}
    
    public function getSeasons($forceload = false)
    {
        if($this->seasons === false || $forceload)
        {
            $this->seasons = Seasons::getTeamSeasons($this->id);

            Cache::store(TeamDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        return $this->seasons;
    }
    
    public function getActiveSeason($forceload = false)
    {
        if($this->activeSeason === false || $forceload)
        {
            $this->activeSeason = Seasons::getTeamSeasons($this->id, true);

            Cache::store(TeamDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        return $this->activeSeason;
    }
    
    public function setActiveSeason($season_id, $inactive = true)
    {
        Cache::delete(TeamDAO::KEY_PREFIX . $this->id);

        return Seasons::setActiveTeamSeason($this->id, $season_id, $inactive);
    }
    
    public function updateVersion()
    {
        Cache::delete(TeamDAO::KEY_PREFIX . $this->id);

        TeamDAO::updateVersion($this->id, $this->version);
        
        Cache::store(TeamDAO::KEY_PREFIX . $this->id, serialize($this));
        
        return $this;
    }
    
    public function userIsInTeam(User $user)
    {
        return ($this->isAdmin($user) || $this->isPlayer($user) || $this->isFan($user->id) || $this->isObserver($user->id));
    }
	
    public function addUser($user_id, $is_admin, $is_player, $is_active, $is_host = 0)
    {
        TeamDAO::deleteUser($user_id, $this->id);

        return TeamDAO::addUser($user_id, $this->id, $is_admin, $is_player, $is_active, $is_host);
    }
    
    public function getProfileLink($module = 'team-profile')
    {
        return (PATH_WEB_ROOT . friendly_url($this->prefix.' '. $this->name .' ' . $this->category) . '-' . $this->id . '/'.$module.'/');
    }
    
    public function getPressDetailLink($title, $object_id)
    {
        return $this->getProfileLink('press-releases') . friendly_url($title) . '-' . $object_id . '/';
    }
    
    public function getTeampostDetailLink($title, $object_id)
    {
        return $this->getProfileLink('teampost') . friendly_url($title) . '-' . $object_id . '/';
    }

    public function getGalleryChangeLink($title, $object_id)
    {
        return $this->getProfileLink('gallery-change') . friendly_url($title) . '-' . $object_id . '/';
    }
    
    public function getAdmins($forceload = false)
    {
        if($this->adminList === false || $forceload)
        {
            $this->adminList = TeamDAO::getAdmins($this->id);

            Cache::store(TeamDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        return $this->adminList;
    }
    
    public function isAdmin(User $logged_user)
    {
        return (in_array($logged_user->id, $this->getAdmins(false)) && in_array($this->id, array_keys($logged_user->getTeamList())) && !empty($this->id));
    }
    
    public function getPlayers($forceload = false, $objects = false)
    {
        if($objects)
        {
            if($this->playerListO === false || $forceload)
            {
                $this->playerListO = TeamDAO::getPlayerList($this->id, $objects);
                $this->players = count($this->playerListO);

                Cache::store(TeamDAO::KEY_PREFIX . $this->id, serialize($this));
            }

            $this->players = count($this->playerListO);

            return $this->playerListO;
        }
        else
        {
            if($this->playerList === false || $forceload)
            {
                $this->playerList = TeamDAO::getPlayerList($this->id, $objects);
                $this->players = count($this->playerList);

                Cache::store(TeamDAO::KEY_PREFIX . $this->id, serialize($this));
            }

            $this->players = count($this->playerList);

            return $this->playerList;
        }
    }
    
    public function getInvitations($forceload = false)
    {
        if($this->invitationList === false || $forceload)
        {
            $this->invitationList = TeamDAO::getInvitationList($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        return $this->invitationList;
    }

    public function isPlayer(User $logged_user)
    {
        return (in_array($logged_user->id, $this->getPlayers()));
    }
    
    public function isInvited(User $logged_user)
    {
        return (in_array($logged_user->id, $this->getInvitations()));
    }

    public function hasImage($dir = '/', $type = 'big')
    {
        global $default_logos;
        
        if($this->id < 1)
        {
            $dir = '/'.session_id().'/';
        }
        
        $im = TEAM_DATADIR . intval($this->id) . $dir . $default_logos['original'];
        
        return file_exists($im);
    }
    
    /**
     * vrati adresu obrazku ktery potrebujeme podle typu (smmal, medium, big, original, original_cropped) 
     */ 
    public function getTeamLogo($type = 'small', $dir = '/')
    {
        global $resizes_logo, $default_logos;

        // slozka kde se bude ukladat resizovany obrazek - relativni cesta
        $image_dir = TEAM_DATADIR . intval($this->id) . $dir;

        // plna cesta ke slozce
        $full_image_dir = PATH_ROOT . $image_dir;

        if($type == 'original')
        {
            $orig = ORIGINAL_IMAGE_NAME;
        }
        else
        {
            $orig = ORIGINAL_IMAGE_NAME_CROPPED;
        }

        // pokud v danem tymu neni hledany soubor, tak musime vratit defaultni
        if(!file_exists($full_image_dir . $orig))
        {
            return '/' . TEAM_DATADIR . $default_logos[$type];
        }

        // nastaveni resizu
        $modifier = @$resizes_logo[$type];

        // novy nazev obrazku, puvodni je "original_cropped"
        $new_name = Image::CACHE_PATH . intval($this->id) . '-logo-' . friendly_url($dir) . '-' . $type . '-' . (empty($this->version) ? time() : $this->version) . '-' . Image::hash($image_dir . '-' . $this->id . '-' . $type, $modifier) . '.jpg';

        $im = Image::get($full_image_dir, $orig, $new_name, $modifier);

        $im = pathinfo($im, PATHINFO_BASENAME);

        $return = CACHE_RELATIVE_PATH . $im;

        // pokud v danem tymu neni hledany soubor, tak musime vratit defaultni
        if(!file_exists(PATH_ROOT . $return))
        {
            return '/' . TEAM_DATADIR . $default_logos[$type];
        }
    
        return '/' . $return;
    }
    
    
    /**
     * type = big, small, original, original_cropped
     */ 
    public function getTeamPhoto($type = 'small', $dir = '/')
    {
        global $resizes_team, $default_teams;

        // slozka kde se bude ukladat resizovany obrazek - relativni cesta
        $image_dir = TEAM_DATADIR . intval($this->id) . $dir;

        // plna cesta ke slozce
        $full_image_dir = PATH_ROOT . $image_dir;

        if($type == 'original')
        {
            $orig = ORIGINAL_TEAM_IMAGE_NAME;
        }
        else
        {
            $orig = ORIGINAL_TEAM_IMAGE_NAME;
        }

        // pokud v danem tymu neni hledany soubor, tak musime vratit defaultni
        if(!file_exists($full_image_dir . $orig))
        {
            return '/' . TEAM_DATADIR . $default_teams[$type];
        }

        // nastaveni resizu
        $modifier = @$resizes_team[$type];

        // novy nazev obrazku, puvodni je "original_cropped"
        $new_name = Image::CACHE_PATH . intval($this->id) . '-team-' . friendly_url($dir) . '-' . $type . '-' . (empty($this->version) ? time() : $this->version) . '-' . Image::hash($image_dir . '-' . $this->id . '-' . $type, $modifier) . '.jpg';

        $im = Image::get($full_image_dir, $orig, $new_name, $modifier);

        $im = pathinfo($im, PATHINFO_BASENAME);

        $return = CACHE_RELATIVE_PATH . $im;

        // pokud v danem tymu neni hledany soubor, tak musime vratit defaultni
        if(!file_exists(PATH_ROOT . $return))
        {
            return '/' . TEAM_DATADIR . $default_teams[$type];
        }
    
        return '/' . $return;        


        /*global $global_team_images;
        
        $im = TEAM_DATADIR . intval($this->id) . $dir . $global_team_images[$type]['name'];
        
        if(file_exists($im))
        {
            return '/' . $im . '?' . (!empty($this->version) ? $this->version : time());
        }
        
        return '/' . TEAM_DATADIR . $global_team_images[$type]['name']; */

        $info = $this->id . '-' . $type . '-' . getTeamImageHash($this->id, $type, $dir) . '-' . $dir;
        return '/image.jpg?team&amp;info=' . $info;
    }
    
    
    /**
     * metoda se pouziva ve volani ajaxu
     * 
     * vezme aktualni main picture a cropne z neho vyrez, ktery nam posle uzivatel ajaxem
     * zaroven vytvori nahledy ve vsech velikostech
     **/ 
    public function cropImage($width, $x, $y, $returnImg = true, $teamPhoto = false, $dir = '/')
    {
        global $global_user_images, $global_team_images, $default_logos, $sizes_logo;

        if($teamPhoto)
        {
            $images = $global_team_images;
        
            // file names
            $orig = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_TEAM_IMAGE_NAME;
            $crop = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_TEAM_IMAGE_NAME_CROPPED;
        }
        else
        {
            $images = $default_logos;

            // file names
            $orig = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_IMAGE_NAME;
            $crop = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_IMAGE_NAME_CROPPED;
        }


        // odstranime stary crop obrazek
        @unlink($crop);
        
        // vytvorime novy
        cropOriginal($orig, $crop, $width, $x, $y);
        
        // vytvorime zmenseniny
        $this->createThumbs($crop, false, $dir);
        
        if($returnImg)
        {
            $img = array();
            foreach($images as $key => $image)
            {
                $img['img_'.$key] = '<img src="'.($teamPhoto ? $this->getTeamPhoto($key, $dir) : $this->getTeamLogo($key, $dir)).'" alt="'.$this->getName().'" width="'.intval($sizes_logo[$key][0]).'" height="'.intval($sizes_logo[$key][1]).'">';
                //$img['img_'.$image] = $this->getUserImage($image);
            }
            return $img;
        }
        
        return true;
    }
    
    
    /**
     * globalni promenne jsou v configu
     * $_FILES by melo byt vzdy vyplneno, jinak by se tato fce nemela volat
     * kontrola na zacatku je jen pro jistotu
     */ 
    public function uploadImage($returnImg = false, $inputName = 'image', $teamPhoto = false, $dir = '/')
    {
        global $global_user_images, $global_team_images, $default_logos, $sizes_logo;

        $images = $global_user_images;
        
        if(!empty($_FILES[$inputName]))
        {
            if(!is_dir(TEAM_DATADIR . intval($this->id) . $dir))
            {
                if(!mkdir(TEAM_DATADIR . intval($this->id) . $dir, 0777, true))
                {
                    return false;
                }
            }
            
            if($teamPhoto)
            {
                $images = $global_team_images;
                // file names
                $tmp = TEAM_DATADIR . intval($this->id) . $dir . $_FILES[$inputName]['name'].'.tmp';
                $orig = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_TEAM_IMAGE_NAME;
                $crop = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_TEAM_IMAGE_NAME_CROPPED;
            }
            else
            {
                $images = $default_logos;
                // file names
                $tmp = TEAM_DATADIR . intval($this->id) . $dir . $_FILES[$inputName]['name'].'.tmp';
                $orig = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_IMAGE_NAME;
                $crop = TEAM_DATADIR . intval($this->id) . $dir . ORIGINAL_IMAGE_NAME_CROPPED;    
            }   
                 
                     
            if(move_uploaded_file($_FILES[$inputName]['tmp_name'], $tmp))
            {
                if(file_exists($orig))
                {
                    @unlink($orig);
                }
                
                // vytvorime si main picture ze ktereho pak budeme vyrezavat ctverce
                generateMainPictures($tmp, $orig, $crop, ($teamPhoto ? MAX_ORIGINAL_TEAM_WIDTH : MAX_ORIGINAL_WIDTH));
                
                if($teamPhoto)
                {
                    // vytvorime zmenseniny
                    $this->createThumbs($orig, $teamPhoto, $dir);
                }
                else
                {
                    // vytvorime zmenseniny
                    $this->createThumbs($crop, $teamPhoto, $dir);
                }
                
                // odstranime temp
                @unlink($tmp);
            }
            else
            {
                return false;
            }
        }
        
        if($returnImg)
        {
            $img = array();
            foreach($images as $key => $image)
            {
                $img['img_'.$key] = '<img src="'.($teamPhoto ? $this->getTeamPhoto($key, $dir) : $this->getTeamLogo($key, $dir)).'" alt="'.$this->getName().'" width="'.intval($sizes_logo[$key][0]).'" height="'.intval($sizes_logo[$key][1]).'">';
                //$img['img_'.$image] = $this->getUserImage($image);
            }
            return $img;
        }
        
        return true;
    }
    
    
    /**
     * 
     * vytvori vsechny male obrazky (small, medimu, big)
     * 
     */ 
    protected function createThumbs($crop, $teamPhoto = false, $dir = '/')
    {
        global $global_user_images, $global_team_images; 
        
        if($teamPhoto)
        {
            $images = $global_team_images;
        }
        else
        {
            $images = $global_user_images;
        }
        
        foreach($images as $key => $image)
        {
            if($image['show'] == true)
            {
                $new = TEAM_DATADIR . intval($this->id) . $dir . $image['name'];
                // promazaeme stare obrazky
                if(file_exists($new))
                {
                    @unlink($new);
                }
            
                if($teamPhoto)
                {
                    // vygeneruje a ulozi thumbnail
                    $ok = cropTeamPhoto($crop, $new, $image['size'][0], $image['size'][1]);
                }
                else
                {
                    // vygeneruje a ulozi thumbnail
                    $ok = GenerateThumbnail($crop, $new, $image['size'][0], $image['size'][1]);
                }
                
                if($ok != 99)
                {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    public function getFanAttr()
    {
        return 'team_' . $this->id . '_' . $this->getHash(TEAM_SALT);
    }
    
    public function getObserverAttr()
    {
        return 'team_' . $this->id . '_' . $this->getHash(TEAM_SALT_OBS);
    }

    public function getEntryAttr()
    {
        return 'team_' . $this->id . '_' . $this->getHash(TEAM_SALT_ENTRY);
    }
    
    public function getHash($salt = '')
    {
        return sha1($this->name . ' - - - ' . $this->id . $this->prefix . $salt);
    }
    
    public function addFan($user_id = 0)
    {
        if($user_id > 0)
        {
            return TeamDAO::addFan($this->id, $user_id);
        }
        
        return false;
    }
    
    public function removeFan($user_id = 0)
    {
        if($user_id > 0)
        {
            return TeamDAO::removeFan($this->id, $user_id);
        }
        
        return false;
    }
    
    public function addObserver($user_id = 0)
    {
        if($user_id > 0)
        {
            return TeamDAO::addObserver($this->id, $user_id);
        }
        
        return false;
    }
    
    public function removeObserver($user_id = 0)
    {
        if($user_id > 0)
        {
            return TeamDAO::removeObserver($this->id, $user_id);
        }
        
        return false;
    }
    
    public function removePlayer($user_id = 0)
    {
        if($user_id > 0)
        {
            $data = array(
                'user_id' => $user_id,
                'team_id' => $this->id
            );
            RosterInvites::deleteInviteByTeam($data);
            // smazeme starou notifikaci
            Notifications::deleteNotifications($user_id, $this->id, array('TEAM-INVITATION', 'CANCEL-TEAM-INVITATION'));
            return TeamDAO::deleteUser($user_id, $this->id);
        }
        
        return false;
    }    

    public function getFanList($forceload = false)
    {
        if($this->fanList == false || $forceload)
        {
            $this->fanList = TeamDAO::getFanList($this->id);
            $this->fans = count($this->fanList);

            if(CACHE_ENABLED)
            {
                apc_store('apc_team_' . $this->id, serialize($this));
            }
        }
        $this->fans = count($this->fanList);

        return $this->fanList;
    }
    
    public function isFan($user_id)
    {
        if($user_id instanceof User)
        {
            $user_id = $user_id->id;
        }

        return in_array($user_id, $this->getFanList());
    }
    
    public function getObserverList($forceload = false)
    {
        if($this->observerList == false || $forceload)
        {
            $this->observerList = TeamDAO::getObserverList($this->id);
            $this->observers = count($this->observerList);

            if(CACHE_ENABLED)
            {
                apc_store('apc_team_' . $this->id, serialize($this));
            }
        }
        $this->observers = count($this->observerList);
        
        return $this->observerList;
    }
    
    public function isObserver($user_id)
    {
        if($user_id instanceof User)
        {
            $user_id = $user_id->id;
        }
        
        return in_array($user_id, $this->getObserverList(false));
    }
    
    public function userCanChangePress(User $logged_user)
    {
        return $this->isAdmin($logged_user);
    }
    
    public function userCanChangeRoster(User $logged_user)
    {
        return $this->isAdmin($logged_user);
    }
    
    public function getEvents($filter = array(), $types = array())
    {
        return EventDAO::getDefaultTeamEvents($this->get('id'), $types, $filter);
        //return EventDAO::getTeamEvents($this->get('id'), $passed, $types);
    }

    public function getLists()
    {
        $this->getPlayers();
        $this->getObserverList();
        $this->getFanList();
    }
    
    public function getWall($start = 0, $forceload = true, User $logged_user)
    {
        if($this->wall === false || $forceload)
        {
            // typy k nacteni
            $types = array('press', 'roster-insert', 'roster-delete', 'gallery-insert');
            
            // tymove moduly
            $modules = $this->getModules();
            
            // udalosti jsou bud pro vsechny, nebo jen pro hrace
            if($modules['events'] == 'all' || ($modules['events'] == 'members' && $this->isPlayer($logged_user)))
            {
                $types[] = 'event-result';
            }
            
            // pokud je prihlaseny uzivatel hráčem toho tymu, tak uvidi i tymposty 
            if($this->isPlayer($logged_user))
            {
                $types[] = 'teampost';
            }
            
            // id tymu
            $team = array($this->id);
            
            // nacteme si prispevky na zdi
            // $team_id = array(), $type = false, $parent_id = 0, $sources = array(), $start = 0, $limit = WALL_LIMIT, $filter = array(), $debug = false, $add_where = ''
            $this->wall = WallDAO::getWallList($team, $types, false, array(), $start);
            //$this->wall = WallDAO::getWallList($team, array("news","event","press"), false, $this->getRssSources(), $start, WALL_LIMIT, $filter, $debug);
        }
        
        // pro kazdy objekt zdi musime nacist i komentare
        foreach($this->wall as $key => $val)
        {
            // nacteme vsechny komentare
            $this->wall[$key]['comments'] = WallDAO::getWallList(null, array('reply'), (empty($val['team_id']) ? '0' : $val['team_id']) . '_' .$val['id'] . '_' . $val['type']);
            
            // abychom zjistili celkovy pocet
            $this->wall[$key]['comments_count'] = count($this->wall[$key]['comments']);
            
            // zobrazujeme ale jen urcity pocet
            $this->wall[$key]['comments'] = array_slice($this->wall[$key]['comments'], 0, COMMENT_LIMIT);
            
            // a jeste k tomu obracene
            $this->wall[$key]['comments'] = array_reverse($this->wall[$key]['comments']);
        }
        
        return $this->wall;
    }

    public function getLastMatch()
    {
        return TeamDAO::getLastMatch($this->id);
    }
}
?>