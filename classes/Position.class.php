<?php
class Position extends Object
{
	protected $columns = array(
		'id'        => 'integer',
        'sport_id'  => 'integer',
        'name'      => 'required',
        'name2'     => 'required',
        'short'		=> '',
        'order'     => 'integer',
        'staff'     => 'integer',
        'stats'		=> ''
	);
	
	public function __construct($data = array())
	{
		$this->setTable('roster_position');
        $this->set('id', 0);
        
		if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}
	}
}
?>