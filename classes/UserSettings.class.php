<?php
class UserSettings extends Object
{
    const KEY_BIRTH             = 'birth';      
    const KEY_LOCATION          = 'location';
    const KEY_EMAIL             = 'email';
    const KEY_ICQ               = 'icq';
    const KEY_PHONE             = 'phone';
    const KEY_PUBLIC_COMMENT    = 'comment';
    const KEY_MESSAGES          = 'private_messages';
    
    // emailove upozorneni
    const KEY_EMAIL_EVENT_DAY   = 'email_event_day';
    const KEY_EMAIL_EVENT_WEEK  = 'email_event_week';
    const KEY_NEW_TEAM_NESSAGE  = 'team_message';
    
    // komu zobrazovat informace o uzivateli
    const VAL_SHOW_ALL          = 'all';
    const VAL_SHOW_TEAMMATES    = 'teammates';
    const VAL_SHOW_NOBODY       = 'noshow';
    
    // zobrazovat nebo nezobrazovat upozornění
    const VAL_NOTICE_YES        = 'notice_yes';
    const VAL_NOTICE_NO         = 'notice_no';
    
    // posilat nebo neposilat emaily
    const VAL_EMAIL_YES        = 'email_yes';
    const VAL_EMAIL_NO         = 'email_no';

    // posilat upozorneni na email?
    const VAL_NOTICE_2DAYS      = '2days';
    const VAL_NOTICE_1DAY       = '1day';
    const VAL_NOTICE_CDAY       = 'current_day';
    //const VAL_NOTICE_NO         = 'notice_no';
    
    // soukrome zpravy
    const VAL_MESSAGE_YES       = 'message_yes';
    const VAL_MESSAGE_TEAM      = 'message_teammates';
    const VAL_MESSAGE_NO        = 'message_no';
    
    private $allowed = false;
    
    public function __construct()
    {
        // kvůli překladu
        global $tr;
        
        $this->allowed = array(
            self::VAL_SHOW_ALL,
            self::VAL_SHOW_TEAMMATES,
            self::VAL_SHOW_NOBODY,
            self::VAL_NOTICE_YES,
            self::VAL_NOTICE_NO,
            self::VAL_MESSAGE_YES,
            self::VAL_MESSAGE_TEAM,
            self::VAL_MESSAGE_NO,
            self::VAL_EMAIL_YES,
            self::VAL_EMAIL_NO
        );
    }
    
    public function getAllowed()
    {
        return $this->allowed;
    } 
    
    public function getSettings($user_id)
    {
        $db = new Database();
        
        $sql = 'SELECT * 
                FROM `user_settings`
                WHERE `user_id` = ' . intval($user_id);
        
        $db->query($sql);
        
        $return = array();
        while($a = $db->readrow('aarr'))
        {
            $return[$a['key']] = $a['value'];
        }
        
        return $return;
    }
    
    public function updateSettings($user_id, array $data)
    {
         if(count($data) > 0)
         {
            $db = new Database();
            $sql = 'DELETE FROM `user_settings` WHERE `user_id` = ' . intval($user_id);
            $db->query($sql);
            
            $sql = 'INSERT INTO `user_settings`(`user_id`, `key`, `value`) VALUES ';
            $insert = array();
            foreach($data as $key => $val)
            {
                $insert[] = '('.$user_id.', "'.$key.'", "'.$val.'")';
            }
            $sql .= implode(', ', $insert);

            return $db->query($sql);
         }
         return false;
    }
}

?>