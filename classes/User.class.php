<?php
class User extends Object
{
	protected $columns = array(
		'id'       => '',
		'nick'     => '',
		'email'    => 'email',
        'sex'      => 'string',
		'fname'    => 'string',
		'sname'	   => 'string',
        'key'      => 'string',
        'password' => '',
        'sport'    => 'required', 
        'team'     => '',
		'height'   => '',
		'weight'   => '',
		'birth'    => 'date',
        'locationID' => 'required',
		'city'     => 'required',
        'region'   => '',
		'icq'      => '',
		'phone'    => '',
		'web'      => '',
        'hash'     => '',
        'verified' => '',
        'version'  => '',
        'deleted'  => '',
        'show_switch_help'  => '',
        'show_team_help'    => ''
	);
	
	public $teams = false;
    public $fanTeams = false; 
    public $observeTeams = false;
	protected $wall  = false;
    protected $errors = false;
    protected $sources = false;
    protected $sports = false;
    public $activeTeam = false;
    public $noticesCount = false;
    public $messagesCount = false;
    
    public $settings = false; 
	
	public function __construct($data = array())
	{
		$this->setTable('user');
        $this->set('id', 0);
		if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
            
            if(!empty($data['id']))
            {
                // nastaveni profilu
                $settings = new UserSettings();
                $this->settings = $settings->getSettings($this->id);
            }
		}
        if(!empty($this->id))
        {
            $this->getNoticesCount(true);
        }
	}
    
    public function isFakeUser()
    {
        return empty($this->password);
    }

    public function getSettings($forceload = false)
    {
        if($this->settings === false || $forceload)
        {
            $settings = new UserSettings();
            
            $this->settings = $settings->getSettings($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        return $this->settings;
    }
    
    public function updateSettings(array $data)
    {
        $settings = new UserSettings();
        $settings->updateSettings($this->id, $data);
        $this->settings = $settings->getSettings($this->id);
        
        Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));

        return true;
    }
	
	public function getName()
	{
        return $this->fname.' '.$this->sname;
	}
	
    public function getShortName()
    {
        return $this->sname . ' ' . mb_substr($this->fname, 0, 1) . '.';
    }
    
    public function getProfileLink()
    {
        return (PATH_WEB_ROOT . 'user-profile/' . $this->key .'-' . $this->id . '/');
    }
    
    public function getAnchor()
    {
        return '<a href="'.$this->getProfileLink().'" title="Zobrazit uživatele '.$this->getName().'">'.$this->getName().'</a>';
    }
    
	public function setData($data = array())
	{
		if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}
	}
	
	public function emailExists()
	{
		return UserDAO::emailExists($this->get('email'), $this->get('id'));
	}
	
	public function save()
	{
		if($this->get('id'))
    	{
			return UserDAO::update($this->data);
		}
		else
		{
			$this->id = UserDAO::insert($this->data);

            return $this->id;
		}
	}
	
    public function updateVersion()
    {
        UserDAO::updateVersion($this->get('id'), $this->get('version'));
    
        Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));

        return $this;
    }
    
    public function isTeammate($user_id)
    {
        $user_id = intval($user_id);
        
        if($user_id > 0)
        {
            $teams = $this->getTeamList(false);
            if(empty($teams) || !is_array($teams))
            {
                return false;
            }
            
            // nacteme vsechny hrace, kteri jsou v mych tymech
            $players = TeamDAO::getTeamMates(array_keys($teams));
            
            return in_array($user_id, $players); 
        }
        
        return false;
    }
    
    public function getTeamList($forceload = false)
    {
        if($this->teams === false || $forceload)
        {
            $this->teams = UserDAO::getUserTeamList($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        if($this->fanTeams === false || $forceload)
        {
            $this->fanTeams = UserDAO::getUserFanTeamList($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        if($this->observeTeams === false || $forceload)
        {
            $this->observeTeams = UserDAO::getUserObserveTeamList($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        return $this->teams;
    }
    
    public function getActiveTeam($forceload = false)
    {
        if($this->activeTeam === false || $forceload)
        {
            $this->activeTeam = UserDAO::getActiveTeam($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        return $this->activeTeam;
    }
    
    public function setActiveTeam($team, $db = true)
    {
        if($team instanceof Team)
        {
            if(!empty($team->id) && $db)
            {
                UserDAO::setActiveTeam($this->id, $team->id);

                Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
            }
            
            $this->activeTeam = $team;
        }
        elseif(is_numeric($team))
        {
            if(!empty($team) && $db)
            {
                UserDAO::setActiveTeam($this->id, $team);

                Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
            }
            
            $this->activeTeam = TeamDAO::get($team);
        }
    }
    
    public function createNewPassword()
    {
        $pass = uniqid();
        
        if($this->changePassword($pass))
        {
            return $pass;
        }
        
        return false;
    }
    
    public function changePassword($password = '')
    {
        return UserDAO::changePassword($password, $this->id);
    }
    
    public function getWall($start = 0, $forceload = false, $filter = array(), $debug = false, $max_date = '')
    {
        if($this->wall === false || $forceload)
        {
            // nacteme si seznamy tymu, pro pozdejsi pouziti
            $this->getTeamList();
            
            // pozdeji naplnime                        
            $types = array('nothing');
            
            // máme nastavený filtr?
            if(isset($filter['sent']) && $filter['sent'] == 'yes')
            {
                // pokud chce uživatel zobrazit nějaké novinky ze sportů
                if(isset($filter['sports']) && count($filter['sports']) > 0)
                {
                    // víme, že chce zobrazovat novinky
                    $types[] = 'news';
                }
                
                // má vybráno i něco jiného než jenom novinky 
                if(isset($filter['types']) && count($filter['types']) > 0)
                {
                    foreach($filter['types'] as $type)
                    {
                        // soupisku mame rozdelenou na dva ruzne typy
                        if($type == 'roster')
                        {
                            $types[] = 'roster-delete';
                            $types[] = 'roster-insert';
                        }
                        elseif($type == 'gallery')
                        {
                            $types[] = 'gallery-insert';
                        }
                        else
                        {
                            // přidáme daný typ objektů k výpisu
                            $types[] = $type;
                        }
                    }
                }
            }
            // jelikoz nema poslany filter, tak nastavime defaultni zobrazeni
            else
            {
                $this->getTeamList();

                if(count($this->teams) || count($this->fanTeams) || count($this->observeTeams))
                {
                    $types = array('news', 'press', 'event', 'teampost', 'roster-delete', 'roster-insert', 'gallery-insert', 'event-result');
                }
                else
                {
                    $types = array('news');
                }
            }
            

            // pokud nemá žádný tým, tak nesmí videt tymposty
            if(!count($this->getTeamList()))
            {
                if(($index = array_search('teampost', $types)) !== false)
                {
                    unset($types[$index]);
                }
            }

            /*
            if(!count($this->fanTeams) && !count($this->observeTeams)) 
            {
                unset($types[array_search('press', $types)]);
                unset($types[array_search('roster-insert', $types)]);
                unset($types[array_search('roster-delete', $types)]);
            }
            */
            
            // z jeho nastaveni si vytahneme vsechny zdroje, ze kterych chce novinky
            $sources = $this->getRssSources();
            
            // zjistime si ID vsech tymu, ze kterych pak budeme tahat udalosti
            if($this->teams !== false)
            {
                $teams = array_keys($this->teams);
                $fanTeams = array_keys($this->fanTeams);
                $observerTeams = array_keys($this->observeTeams);
                
                $team = array_merge($teams, $fanTeams, $observerTeams);
            }
            else
            {
                $team = array();
            }
            
            // nacteme si prispevky na zdi
            // $team_id = array(), $type = false, $parent_id = 0, $sources = array(), $start = 0, $limit = WALL_LIMIT, $filter = array(), $debug = false, $add_where = ''
            $this->wall = WallDAO::getWallList($team, $types, false, $sources, $start, WALL_LIMIT, $filter, $debug, '', $max_date);
            //$this->wall = WallDAO::getWallList($team, array("news","event","press"), false, $this->getRssSources(), $start, WALL_LIMIT, $filter, $debug);
        }
        
        // pro kazdy objekt zdi musime nacist i komentare
        foreach($this->wall as $key => $val)
        {
            // nacteme vsechny komentare
            $this->wall[$key]['comments'] = WallDAO::getWallList(null, array('reply'), (empty($val['team_id']) ? '0' : $val['team_id']) . '_' .$val['id'] . '_' . $val['type']);
            
            // abychom zjistili celkovy pocet
            $this->wall[$key]['comments_count'] = count($this->wall[$key]['comments']);
            
            // zobrazujeme ale jen urcity pocet
            $this->wall[$key]['comments'] = array_slice($this->wall[$key]['comments'], 0, COMMENT_LIMIT);
            
            // a jeste k tomu obracene
            $this->wall[$key]['comments'] = array_reverse($this->wall[$key]['comments']);
        }

        return $this->wall;
    }
    
    public function getObject($id)
    {
        $object = WallDAO::getObject($id);
        if(count($object) > 0)
        {
            foreach($object as $key => $val)
            {
                $object[$key]['comments'] = WallDAO::getWallList(null, array('reply'), (empty($val['team_id']) ? '0' : $val['team_id']) . '_' .$val['id'] . '_' . $val['type']);
                $object[$key]['comments_count'] = count($object[$key]['comments']);
                $object[$key]['comments'] = array_slice($object[$key]['comments'], 0, COMMENT_LIMIT_DETAIL);
                $object[$key]['comments'] = array_reverse($object[$key]['comments']);
            }
        }
        return $object;
    }
        
    public function getRssSources($forceload = false)
    {
        if($this->sources === false || $forceload)
        {
            $this->sources = UserDAO::getRssSources($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }

        return $this->sources;
    }    
    
    public function getUserSports($forceload = false)
    {
        if($this->sports === false || $forceload)
        {
            $this->sports = UserDAO::getUsersports($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        return $this->sports;
    }
        
    public function isVerified()
    {
        return ($this->verified == 1);
    }

    public function hasTeam()
    {
        $this->teams = $this->getTeamList();
        
        return count($this->teams) ? true : false;
    }

    public function setNoticesCount($count)
    {
        $this->noticesCount = intval($count);
    }

    public function getNoticesCount($forceload = false)
    {
        if($this->noticesCount === false || $forceload)
        {
            $not = Notifications::getUnreadNotificationsForUser($this->id, array(), array('NEW-PERSONAL-MESSAGE'));
            $this->noticesCount = count($not);

            //Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        return $this->noticesCount;
    }
    
    public function setMessagesCount($count)
    {
        $this->messagesCount = intval($count);
    }
    
    public function getMessagesCount($forceload = false)
    {
        if($this->messagesCount === false || $forceload)
        {
            $this->messagesCount = Notifications::getUnreadMessagesForUser($this->id);

            Cache::store(UserDAO::KEY_PREFIX . $this->id, serialize($this));
        }
        
        return $this->messagesCount;
    }
    
    public function getResidence()
    {
        $res = UserDAO::getResidence($this->locationID);
        return $res;
    }
    
    
    function hasImage()
    {
        global $default_users;
        
        $im = USER_DATADIR . $this->id . '/' . $default_users['original'];
        
        return file_exists($im);
    }
    
    /**
     * vrati adresu obrazku ktery potrebujeme podle typu (smmal, medium, big, original, original_cropped) 
     */ 
    function getUserImage($type)
    {
        global $resizes_user, $default_users;

        // slozka kde se bude ukladat resizovany obrazek - relativni cesta
        $image_dir = USER_DATADIR . intval($this->id) . '/';

        // plna cesta ke slozce
        $full_image_dir = PATH_ROOT . $image_dir;

        if($type == 'original')
        {
            $orig = ORIGINAL_IMAGE_NAME;
        }
        else
        {
            $orig = ORIGINAL_IMAGE_NAME_CROPPED;
        }

        // pokud v danem tymu neni hledany soubor, tak musime vratit defaultni
        if(!file_exists($full_image_dir . $orig))
        {
            return '/' . USER_DATADIR . $default_users[$type];
        }

        // nastaveni resizu
        $modifier = @$resizes_user[$type];

        // novy nazev obrazku, puvodni je "original_cropped"
        $new_name = Image::CACHE_PATH . $this->id . '-profileimage--' . $type . '-' . (empty($this->version) ? time() : $this->version) . '-' . Image::hash($image_dir . '-' . $this->id . '-' . $type, $modifier) . '.jpg';

        $im = Image::get($full_image_dir, $orig, $new_name, $modifier);

        $im = pathinfo($im, PATHINFO_BASENAME);

        $return = CACHE_RELATIVE_PATH . $im;

        // pokud v danem tymu neni hledany soubor, tak musime vratit defaultni
        if(!file_exists(PATH_ROOT . $return))
        {
            return '/' . USER_DATADIR . $default_users[$type];
        }
    
        return '/' . $return;
    }
    
    
    /**
     * metoda se pouziva ve volani ajaxu
     * 
     * vezme aktualni main picture a cropne z neho vyrez, ktery nam posle uzivatel ajaxem
     * zaroven vytvori nahledy ve vsech velikostech
     **/ 
    public function cropImage($width, $x, $y, $returnImg = true)
    {
        global $default_users, $sizes_user;

        // file names
        $orig = USER_DATADIR . $this->id . '/' . ORIGINAL_IMAGE_NAME;
        $crop = USER_DATADIR . $this->id . '/' . ORIGINAL_IMAGE_NAME_CROPPED;
        
        // odstranime stary crop obrazek
        @unlink($crop);
        
        // vytvorime novy
        cropOriginal($orig, $crop, $width, $x, $y);
        
        // vytvorime zmenseniny
        $this->createThumbs($crop);
        
        if($returnImg)
        {
            $img = array();
            foreach($default_users as $key => $image)
            {
                $img['img_'.$key] = '<img src="'.$this->getUserImage($key).'" alt="'.$this->fname.' '.$this->sname.'" width="'.intval($sizes_user[$key][0]).'" height="'.intval($sizes_user[$key][1]).'">';
                //$img['img_'.$image] = $this->getUserImage($image);
            }
            return $img;
        }
        
        return true;
    }
    
    
    /**
     * globalni promenne jsou v configu
     * $_FILES by melo byt vzdy vyplneno, jinak by se tato fce nemela volat
     * kontrola na zacatku je jen pro jistotu
     */ 
    public function uploadImage($returnImg = false)
    {
        global $default_users, $sizes_user;

        if(!empty($_FILES['image']))
        {
            if(!is_dir(USER_DATADIR . $this->id))
            {
                if(!mkdir(USER_DATADIR . $this->id, 0777, true))
                {
                    return false;
                }
            }
            
            
            // file names
            $tmp = USER_DATADIR . $this->id . '/' . $_FILES['image']['name'].'.tmp';
            $orig = USER_DATADIR . $this->id . '/' . ORIGINAL_IMAGE_NAME;
            $crop = USER_DATADIR . $this->id . '/' . ORIGINAL_IMAGE_NAME_CROPPED;
            
            if(move_uploaded_file($_FILES['image']['tmp_name'], $tmp))
            {
                if(file_exists($orig))
                {
                    @unlink($orig);
                }
                
                @chmod($tmp, 0777);

                // vytvorime si main picture ze ktereho pak budeme vyrezavat ctverce
                generateMainPictures($tmp, $orig, $crop, MAX_ORIGINAL_WIDTH);

                @chmod($orig, 0777);
                @chmod($crop, 0777);

                // vytvorime zmenseniny
                $this->createThumbs($crop);
                
                // odstranime temp
                @unlink($tmp);
            }
            else
            {
                return false;
            }
        }
        
        if($returnImg)
        {
            $img = array();
            foreach($default_users as $key => $image)
            {
                $img['img_'.$key] = '<img src="'.$this->getUserImage($key).'" alt="'.$this->fname.' '.$this->sname.'" width="'.intval($sizes_user[$key][0]).'" height="'.intval($sizes_user[$key][1]).'">';
                //$img['img_'.$image] = $this->getUserImage($image);
            }
            return $img;
        }
        
        return true;
    }
    
    
    /**
     * 
     * vytvori vsechny male obrazky (small, medimu, big)
     * 
     */ 
    protected function createThumbs($crop)
    {
        global $global_user_images;
        
        foreach($global_user_images as $key => $image)
        {
            if($image['show'] == true)
            {
                $new = USER_DATADIR . $this->id . '/' . $image['name'];
                // promazaeme stare obrazky
                if(file_exists($new))
                {
                    @unlink($new);
                }
            
                // vygeneruje a ulozi thumbnail
                $ok = GenerateThumbnail($crop, $new, $image['size'][0], $image['size'][1]);
                
                if($ok != 99)
                {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    public function getAge()
    {
        $now      = new DateTime();
        $birthday = new DateTime($this->birth);
        $interval = $now->diff($birthday);
        
        return $interval->format('%y'); // 39 years
    }

    public function isDeleted()
    {
        return $this->deleted == 1;
    }
    
    public function getLastActivities($from = false, $limit = 25)
    {
        return UserDAO::getLastActivities($this->id, $from, $limit);
    }

    public function canReceivePm($logged_user)
    {
        if(empty($this->settings))
        {
            $this->getSettings();
        }

        if(!is_array($this->settings))
        {
            $settings = unserialize($this->settings);
        }
        else
        {
            $settings = $this->settings;
        }

        return ((isset($settings[UserSettings::KEY_MESSAGES]) && $settings[UserSettings::KEY_MESSAGES] == UserSettings::VAL_MESSAGE_YES) || 
        (isset($settings[UserSettings::KEY_MESSAGES]) && $settings[UserSettings::KEY_MESSAGES] == UserSettings::VAL_MESSAGE_TEAM && $logged_user->isTeamMate($this->id)));
    }

    public function helpSwitchShowed()
    {
        return UserDAO::helpSwitchShowed($this->id);
    }

    public function helpTeamShowed()
    {
        return UserDAO::helpTeamShowed($this->id);
    }
}
?>