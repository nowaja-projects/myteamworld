<?php
class Sport extends Object
{
	protected $columns = array(
		'id'              => 'integer',
		'name'            => 'required',
        'adjective'       => '',
        'list'            => '',
        'source'          => '',
        'image'           => '',
        'players'         => '',
        'team_stats'      => '',
        'select_stats'	  => '',
        'stat_table'      => '',
        'overtime'        => '',
        'shootout'        => '',
        'penalties'       => ''
	);
	
    protected $table = 'sport';
    
	public function __construct($data = array())
	{
		$this->setTable('sport');

		if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}
	}
    
    /**
     * Sport::getPositions()
     * 
     * @return
     */
    public function getPositions()
    {
        return SportDAO::getPositions($this->id);
    }
}
?>