<?php

class PressReleases extends ObjectDAO
{
    public static function getList($team_id = array(), $active = 'published')
    {
        $date = date('Y-m-d H:i:s');
        if($active == 'published')
        {
            $where = ' AND (`object_date_begin` <= "'.$date.'" || `object_date_begin` = "0000-00-00 00:00:00") AND (`object_date_end` >= "'.$date.'" || `object_date_end` = "0000-00-00 00:00:00")';
        }
        elseif($active == 'unpublished')
        {
            $where = ' AND (`object_date_begin` > "'.$date.'")';   
        }
        else
        {
            $where = '';
        }
        
        return WallDAO::getWallList($team_id, array('press'), 0, array(), 0, WALL_LIMIT, array(), false, $where);
    }
    
    public static function insert($object, $team_id = 0)
    {
        return WallDAO::insertItem($object, $team_id);
    }
    
    public static function update($object)
    {
        return WallDAO::updateItem($object);
    }
}

?>