<?php
class Session 
{
	protected $base_name;
	
	public function __construct($base_name = '')
	{
		session_start();
		
		$this->base_name = $base_name;
	}
	
	public function set($name = '',$value)
	{
		if(!empty($name))
		{
			$_SESSION[$this->base_name][$name] = $value;
		}
		return $this;
	}
	
	public function get($name)
	{
		if($this->isSetSession($name))
		{
			return $_SESSION[$this->base_name][$name];
		}
		else
		{
			return null;
		}
	}
	
	public function isSetSession($name)
	{
		return isset($_SESSION[$this->base_name][$name]);
	}
	
	public function sunset($name)
	{
		if($this->isSetSession($name))
		{
			unset($_SESSION[$this->base_name][$name]);
		}
		return $this;
	}
}
?>