<?php
class Seasons extends ObjectDAO
{
    public static function insertSeason(array $data)
    {
        $db = new Database();
        
        $id = Seasons::getMaxId($data['team_id']);
        $sql = 'INSERT INTO `season`
                SET
                    `id`        = "'.$id.'",
                    `team_id`   = "'.intval($data['team_id']).'",
                    `name`      = "'.mysqli_real_escape_string($db->mysqli(), $data['name']).'"';
        
        if($db->query($sql))
        {
            Cache::delete(TeamDAO::KEY_PREFIX . $data['team_id']);

            return $id;
        }
        
        return false;
    }
    
    public static function getMaxId($team_id)
    {
        $db = new Database();
        
        $sql = 'SELECT MAX(`id`) as max 
                FROM `season`
                WHERE `team_id` = ' . intval($team_id);
            
        $db->query($sql);
        
        $row = $db->readrow('aarr');
        
        return ($row['max'] + 1);
    } 

    public static function get($team_id, $season_id)
    {
        $db = new Database();
        
        $sql = 'SELECT *
                FROM `season`
                WHERE `team_id` = "' . intval($team_id) . '" AND `id` = "'.intval($season_id).'"';
            
        return self::load($sql, 'Season');
    } 
    
    public static function updateSeason(array $data)
    {
        $db = new Database();

        $sql = 'UPDATE `season`
                SET
                    `name`      = "'.mysqli_real_escape_string($db->mysqli(), $data['name']).'"
                WHERE
                    `team_id`   = "'.intval($data['team_id']).'" AND
                    `id`        = "'.intval($data['id']).'"
                LIMIT 1';
        
        Cache::delete(TeamDAO::KEY_PREFIX . $data['team_id']);

        return $db->query($sql); 
    }
    
    public static function deleteSeason($team_id = 0, $id = 0)
    {
        $db = new Database();
        
        $sql = 'DELETE FROM `season` 
                WHERE 
                    `id` = "'.intval($id).'" AND 
                    `team_id` = "'.intval($team_id).'"
                LIMIT 1';
                
        return $db->query($sql);
    }
    
    public static function getTeamSeasons($team_id = 0, $active = false)
    {
        $db = new Database();
        
        $sql = 'SELECT * 
                FROM `season`
                WHERE `team_id` = ' . intval($team_id) . 
                ($active ? ' AND `active` = 1' : '').
                ' ORDER BY `id` ASC';

        if($active)
        {
            return self::load($sql, 'Season');
        }       
        
        $db->query($sql);
        $return = array();
        while($a = $db->readrow('obj'))
        {
            $return[$a->id] = $a;
        }
        return $return;
    }
    
    public static function setActiveTeamSeason($team_id, $season_id, $setInActive = true)
    {
        $db = new Database();
        
        Cache::delete(TeamDAO::KEY_PREFIX . $team_id);
        
        // mame nastavit ostatni sezony jako neaktivni?
        if($setInActive)
        {
            $sql = 'UPDATE `season`
                    SET `active` = 0
                    WHERE `team_id` = ' . intval($team_id);
                    
            $db->query($sql);
        }
        
        $sql = 'UPDATE `season`
                SET `active` = 1
                WHERE `id` = '.$season_id.' AND `team_id` = ' . intval($team_id);
                
        return $db->query($sql);
    }
}
?>