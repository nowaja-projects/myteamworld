<?php

class Translation extends Object
{
	protected $langs = array(
        'cz',
        'en'
    );
    
    protected $phrases = false;
    protected $language = false;
    protected $group = false;
    protected $defaultLang = 'cz';
	
	public function __construct($group = 'main', $lang = 'cz')
	{
        $this->phrases = TranslateDAO::getAll($group, $lang);
        
        $this->language = $lang;
        
        $this->group = $group;
	}
    
    public function createKey($text = '')
    {
        return sha1($text);
    }
    
    public function tr($text = '')
    {
        $translated = TranslateDAO::get($this->createKey($text), $this->language);
        
        if(empty($translated->id) || empty($translated->text))
        {
            $data = array(
                'key'           => $this->createKey($text),
                'lang'          => $this->language,
                'text'          => $text,
                'group'         => $this->group,
                'translated'	=> ($this->language == $this->defaultLang ? '1' : '0')
            );
            
            $translated = new Translate($data);
            $translated->save();
        }
        
        return $translated->text;
    }
    
    
}