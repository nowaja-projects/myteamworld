<?php
class Notifications extends ObjectDAO
{
    private static $types = false;

    public function __construct()
    {
        // kvůli překladu
        global $tr;

        self::$types = array(
            // při okomentování článku, který jsem okomentoval já předtím
            'ARTICLE-COMMENT'
                => 'Uživatel {USERS} {DECLINE:přidal;přidali;přidalo} komentář {OBJECT_NAME}.',

            // při okomentování článku, který jsem okomentoval já předtím
            //'NEW-PERSONAL-MESSAGE'
            //    => '{USERS} ti {DECLINE:poslal;poslali;poslalo} novou <strong><a href="'.PATH_WEB_ROOT.'messages/">soukromou zprávu</a></strong>.',

            // můj příspěvek na zdi týmu, který někdo okomentoval
            'MY-OBJECT-TEAM-WALL-COMMENT'
                => 'Uživatel {USERS} {DECLINE:přidal;přidali;přidalo} komentář k vašemu příspěvku na nástěnce týmu <strong>{TEAM-NAME}</strong>',

            'NEW-PRESS-COMMENT'
                => '{DECLINE2:Uživatel;Uživatelé} {USERS} {DECLINE:přidal;přidali;přidalo} komentář {OBJECT_NAME} vašeho týmu <strong>{TEAM-NAME}</strong>',

            // někdo lajkoval můj příspěvek na zdi týmu
            'MY-OBJECT-TEAM-WALL-LIKE'
                => '{DECLINE2:Uživateli;Uživatelům} {USERS} se líbí váš příspěvek na nástěnce týmu <strong>{TEAM-NAME}</strong>',

            // někdo přidal příspěvek na zeď týmu
            'NEW-OBJECT-ON-TEAM-WALL'
                => 'Uživatel {USERS} přidal nový příspěvek na nástěnku týmu <strong>{TEAM-NAME}</strong>',

            // někdo mě pozval do týmu
            'TEAM-INVITATION'
                => 'Uživatel <strong>{TEAM-LOGGED-USER}</strong> vás pozval do týmu <strong>{TEAM-NAME}</strong>. {TEAM-LINKS}',

            // někdo pozadal o vstup do tymu
            'REQUEST-TEAM-ENTRY'
                => 'Uživatel <strong>{TEAM-LOGGED-USER}</strong> zažádal o vstup do vašeho týmu <strong>{TEAM-NAME}</strong>. {TEAM-ENTRY-LINKS}',

            // někdo zršiil moje pozvání do tymu
            'CANCEL-TEAM-INVITATION'
                => 'Uživatel <strong>{TEAM-LOGGED-USER}</strong> zrušil vaše pozvání do týmu <strong>{TEAM-NAME}</strong>.',

            // chodí adminům týmu, že nekdo prijal pozvánku
            'CONFIRM-TEAM-INVITATION'
                => 'Uživatel <strong>{TEAM-LOGGED-USER}</strong> vstoupil do vašeho týmu <strong>{TEAM-NAME}</strong>.',

            'CONFIRM-TEAM-ENTRY-REQUEST'
                => 'Uživatel <strong>{TEAM-LOGGED-USER}</strong> potvrdil vaši žádost o vstup do týmu <strong>{TEAM-NAME}</strong>.',

            'DECLINE-TEAM-ENTRY-REQUEST'
                => 'Uživatel <strong>{TEAM-LOGGED-USER}</strong> zamítnul vaši žádost o vstup do týmu <strong>{TEAM-NAME}</strong>.',

            // chodí adminům týmu, že nekdo odmítl pozvánku
            'DECLINE-TEAM-INVITATION'
                => 'Uživatel <strong>{TEAM-LOGGED-USER}</strong> odmítl vstoupit do vašeho týmu <strong>{TEAM-NAME}</strong>.',

            // přidání hráče na soupisku
            'NEW-PLAYER-IN-TEAM'
                => 'Váš tým {TEAM-NAME} přidal na soupisku nového hráče "{PLAYER}"',

            // odstranění hráče ze soupisky
            'DELETE-PLAYER-IN-TEAM'
                => 'Váš tým {TEAM-NAME} odstranil ze soupisky hráče "{PLAYER}"',

            // nová událost
            'NEW-EVENT'
                => 'Váš tým {TEAM-NAME} vytvořil novou událost "<strong>{EVENT}</strong>" na den {DATE}',

            // zmeny v udalosti
            'EVENT-CHANGE'
                => 'Váš tým <strong>{TEAM-NAME}</strong> udělal změny v události "<strong>{EVENT}</strong>" s datem {EVENT-DATE}.',

            // zrušení události
            'CANCEL-EVENT'
                => 'Váš tým <strong>{TEAM-NAME}</strong> zrušil událost "<strong>{EVENT}</strong>" s datem {EVENT-DATE}.',

            // pozvánka na událost
            'EVENT-INVITATION'
                => 'Váš tým <strong>{USER-TEAM}</strong> vás pozval na událost "<strong>{EVENT}</strong>" s datem {EVENT-DATE}.',

            // zruseni pozvanky na udalost
            'EVENT-CANCEL-INVITATION'
                => 'Váš tým <strong>{TEAM-NAME}</strong> zrušil Vaši pozvánku na událost "<strong>{EVENT}</strong>" s datem {EVENT-DATE}.',

            // nová tisková zpráva
            'NEW-PRESS-RELEASE'
                => 'Váš tým <strong>{TEAM-NAME}</strong> zveřejnil novou tiskovou zprávu "<strong>{PRESS-NAME}</strong>".',

            // nový týmový vzkaz
            'NEW-TEAMPOST'
                => 'Uživatel {USERS} {DECLINE:přidal;přidali;přidalo} <strong>{NEW-TEAMPOST}</strong> na nástěnku týmu <strong>{TEAM-NAME}</strong>.',

            // jestlize druhy tym zedituje a ja ji mam potvrdit
            'EVENT-EDIT-CONFIRM-REQUEST'
                => 'Vašemu týmu <strong>{USER-TEAM}</strong> byla upravena událost: "<strong>{EVENT}</strong>", s datem {EVENT-DATE}, týmem <strong>{OPONENT-TEAM}</strong>. Zkontrolujte ji a pokud je správně, potvrďte.',

            // druhy tym ji prijal
            'EVENT-CONFIRMED'
                => 'Vašemu týmu <strong>{USER-TEAM}</strong> byla potvrzena událost týmem <strong>{OPONENT-TEAM}</strong>: "<strong>{EVENT}</strong>", s datem {EVENT-DATE}.',

            // druhy tym ji odmitl
            'EVENT-DECLINED'
                => 'TEST: Vašemu týmu <strong>{USER-TEAM}</strong> nebyla přijata událost: "<strong>{EVENT}</strong>", s datem {EVENT-DATE}, týmem <strong>{OPONENT-TEAM}</strong>.',

            // druhý tým potvrdil výsledek
            'RESULT-CONFIRMED'
                => 'Vašemu týmu <strong>{USER-TEAM}</strong> byl potvrzen výsledek události týmem <strong>{OPONENT-TEAM}</strong>: "<strong>{EVENT}</strong>", s datem {EVENT-DATE}.',

            // ze mam potvrdit vysledek udalosti
            'EVENT-RESULT-CONFIRM-REQUEST'
                => 'Vašemu týmu <strong>{USER-TEAM}</strong> byl upraven výsledek k události: "<strong>{EVENT}</strong>", s datem {EVENT-DATE}, týmem <strong>{OPONENT-TEAM}</strong>. Zkontrolujte upravený výsledek i statistiky a pokud je vše správně, událost potvrďte.',

            // nekdo pridal udalost a ja ji mam potvrdit
            'EVENT-ADD-CONFIRM-REQUEST'
                => 'Vašemu týmu <strong>{USER-TEAM}</strong> byla vytvořena událost: "<strong>{EVENT}</strong>", s datem {EVENT-DATE}, týmem <strong>{OPONENT-TEAM}</strong>. Zkontrolujte ji a pokud je správně, potvrďte.',

        );
    }

    public static function getTypes()
    {
        return self::$types;
    }

    public static function getMax($user_id = 0)
    {
        $db = new Database();

        $sql = 'SELECT MAX(`id`) as max
                FROM `notifications`
                WHERE `user_id` = ' . intval($user_id);

        $db->query($sql);

        $row = $db->readRow('aarr');

        return (++$row['max']);
    }

    public static function getUnreadNotificationsForUser($user_id = 0, $in = array(), $not_in = array())
    {
        $user_id = intval($user_id);

        $return = array();

        if(!empty($user_id))
        {
            $db = new Database();

            $sql = 'SELECT *
                    FROM `notifications`
                    WHERE `notifications`.`user_id` = ' . $user_id .
                    (count($in) > 0 ? ' AND `type_id` IN ("'.implode('","', $in).'")' : '').
                    (count($not_in) > 0 ? ' AND `type_id` NOT IN ("'.implode('","', $not_in).'")' : '').
                    ' AND `read` = "0" AND `timestamp` < '.(time() - (NOTIFICATIONS_TIME_LIMIT/1000)).'
                    ORDER BY `timestamp` DESC';

            $db->query($sql);

            while($a = $db->readrow('aarr'))
            {
                $return[$a['user_id'].'-'.$a['object_id']] = new Notification($a);
            }
        }

        return $return;
    }

    public static function getUnreadMessagesForUser($user_id = 0)
    {
        $user_id = intval($user_id);
        if($user_id > 0)
        {
            $db = new Database();

            $sql = 'SELECT *
                    FROM  `pm`
                    WHERE  `read` =0
                    AND `user_id` = '.$user_id.'
                    GROUP BY  `users`';

            $db->query($sql);

            return $db->getRows();
        }

        return false;
    }

    public static function getNewNotificationsForUser($user_id = 0)
    {
        $user_id = intval($user_id);

        $return = array();

        if(!empty($user_id))
        {
            $db = new Database();

            $sql = 'SELECT *
                    FROM `notifications`
                    WHERE `notifications`.`user_id` = ' . $user_id .
                    ' AND `read` = "0" AND `timestamp` > '.(time() - (NOTIFICATIONS_TIME_LIMIT/1000)).'
                    ORDER BY `timestamp` DESC';

            $db->query($sql);

            while($a = $db->readrow('aarr'))
            {
                $return[$a['user_id'].'-'.$a['object_id']] = new Notification($a);
            }
        }

        return $return;
    }

    public static function getAllUnreadNotificationsForUser($user_id)
    {
        $user_id = intval($user_id);

        $return = array();

        if(!empty($user_id))
        {
            $db = new Database();

            $sql = 'SELECT *
                    FROM `notifications`
                    WHERE `notifications`.`user_id` = ' . $user_id .
                    ' AND `read` = "0"
                    ORDER BY `timestamp` DESC';

            $db->query($sql);

            while($a = $db->readrow('aarr'))
            {
                $return[$a['user_id'].'-'.$a['object_id']] = new Notification($a);
            }
        }

        return $return;
    }

    public static function getAllNotificationsForUser($user_id)
    {
        $not = array(
            'NEW-PERSONAL-MESSAGE',
            'USER-REMOVED-FROM-TEAM'
        );
        $user_id = intval($user_id);

        $return = array();

        if(!empty($user_id))
        {
            $db = new Database();

            $sql = 'SELECT *
                    FROM `notifications`
                    WHERE `notifications`.`user_id` = ' . $user_id . ' AND `type_id` NOT IN ("'.implode('","', $not).'")
                    ORDER BY `timestamp` DESC';

            $db->query($sql);

            while($a = $db->readrow('aarr'))
            {
                $return[] = new Notification($a);
            }
        }

        return $return;
    }

    public static function getNotificationsByObject($object_id = 0)
    {
        $return = array();

        if(!empty($object_id))
        {
            $db = new Database();

            $sql = 'SELECT *
                    FROM `notifications`
                    WHERE `object_id` = "' . $object_id . '"
                    ORDER BY `timestamp` DESC';

            $db->query($sql);

            while($a = $db->readrow('aarr'))
            {
                $return[$a['user_id'].'-'.$a['object_id']] = new Notification($a);
            }
        }

        return $return;
    }

    public static function getNotification($user_id = 0, $object_id = 0)
    {
        $sql = sprintf('SELECT *
                FROM `notifications`
                WHERE `user_id` = "%s" AND `object_id` = "%s"'
                    , $not->user_id
                    , $not->object_id
                );

        return self::load($sql, 'Notification');
    }

    // vloží pole notifikací do DB
    public static function insertNotifications(array $data)
    {
        $db = new Database();

        if(count($data) > 0)
        {
            $sql = 'INSERT INTO `notifications`(
                        `id`,
                        `user_id`,
                        `object_id`,
                        `type_id`,
                        `info`,
                        `object_info`,
                        `timestamp`,
                        `read`) VALUES ';

            $deletes = array();
            $sql2 = 'DELETE
                    FROM `notifications`
                    WHERE 1 AND ';

            $maxs = array();

            foreach($data as $vals)
            {
                $deletes[] = '(`user_id` = "'.$vals['user_id'].'" AND `object_id` = "'.$vals['object_id'].'" AND `type_id` = "'.$vals['type_id'].'")';

                if(!isset($maxs[$vals['user_id']]))
                {
                    $maxs[$vals['user_id']] = Notifications::getMax($vals['user_id']);
                }
                else
                {
                    $maxs[$vals['user_id']]++;
                }

                foreach($vals as $key => $val)
                {
                    $vals[$key] = mysqli_real_escape_string($db->mysqli(), $val);
                }
                $sql .= '("'.$maxs[$vals['user_id']].'", "' . implode('","', $vals) . '"),';
            }


            $sql2 .= implode(' OR ', $deletes);
            //$db->query($sql2);

            $sql = substr($sql, 0, -1);

            return self::nonEmpty($sql);
        }

        return false;
    }

    public static function insertNotification(Notification $not)
    {
        $sql = sprintf('INSERT INTO `notifications`
                SET
                    `user_id` = "%s",
                    `object_id` = "%s",
                    `type_id` = "%s",
                    `info` = "%s",
                    `object_info` = "%s",
                    `timestamp` = "%s",
                    `read` = "%s"'
                    , $not->user_id
                    , $not->object_id
                    , $not->type_id
                    , $not->info
                    , $not->object_info
                    , $not->timestamp
                    , $not->read
                );

        return self::nonEmpty($sql);
    }

    public static function updateNotification(Notification $not)
    {
        $db = new Database();

        $sql = sprintf('UPDATE `notifications`
                SET
                    `type_id` = "%s",
                    `info` = "%s",
                    `object_info` = "%s",
                    `timestamp` = "%s",
                    `read` = "%s"
                WHERE `user_id` = "%s" AND `object_id` = "%s"'
                    , mysqli_real_escape_string($db->mysqli(), $not->type_id)
                    , mysqli_real_escape_string($db->mysqli(), $not->info)
                    , mysqli_real_escape_string($db->mysqli(), $not->object_info)
                    , mysqli_real_escape_string($db->mysqli(), $not->timestamp)
                    , mysqli_real_escape_string($db->mysqli(), $not->read)
                    , mysqli_real_escape_string($db->mysqli(), $not->user_id)
                    , mysqli_real_escape_string($db->mysqli(), $not->object_id)
                );

        return self::nonEmpty($sql);
    }

    public static function deleteNotifications($user_id, $object_id, array $types)
    {
        $db = new Database();

        $sql = 'DELETE FROM `notifications`
                WHERE '.(intval($user_id) > 0 ? '`user_id` = "'.intval($user_id).'" AND ': ''). '
                    `object_id` = "'.mysqli_real_escape_string($db->mysqli(), $object_id).'" AND
                    `type_id` IN ("'.mysqli_real_escape_string($db->mysqli(), implode('","', $types)).'")';

        return $db->query($sql);
    }


    public static function setNotificationsRead($user_id = 0, $object_id = '')
    {
        $db = new Database();
        $uid = intval($user_id);
        if($uid > 0)
        {
            $sql = 'UPDATE `notifications`
                    SET `read` = 1
                    WHERE `user_id` = ' . $uid .
                    (!empty($object_id) ? ' AND `object_id` = "'.mysqli_real_escape_string($db->mysqli(), $object_id).'"' : '');

            return self::nonEmpty($sql);
        }

        return false;
    }

    // vrátí vsechny uzivatele, kteri okomentovali nejaky object
    public static function getCommentUsers($object_id)
    {
        $sql = sprintf('SELECT *
                        FROM `object`
                        LEFT JOIN `user` ON `user`.`user_id` = `object`.`object_user_id`
                        WHERE `object`.`object_parent_id` = "%s" AND `object`.`object_type` = "reply"'
                        , $object_id
                );

        $array = self::loadArray($sql);

        $return = array();
        foreach($array as $data)
        {
            $user = new User($data);
            $user->getSettings();

            $return[$user->id] = $user;
        }

        return $return;
    }
}
?>