<?php
define('FOOTER', '<br /><p>Váš myteamworld.com tým<br /><a href="'.PATH_WEB_ROOT.'">www.myteamworld.com</a></p>');

class SendMail 
{
	protected static $header = '<p>Dobrý den,</p><br />';
	protected static $footer = FOOTER;
	
	protected static $client = 'PHPMailer';
	
	protected static function createMessage($message = '')
	{
		return self::$header.$message.self::$footer;
	}
	
	public static function registration($email= '', $hash = '', $pass = '')
	{
		$message = '<p>děkujeme za vaši registraci na <strong>www.myteamworld.com</strong>.</p>
					<p>Pro aktivaci vašeho účtu a dokončení registrace klikněte prosím na tento odkaz: <a href="'.PATH_WEB_ROOT.'?action=registration&email='.urlencode($email).'&hash='.$hash.'">'.PATH_WEB_ROOT.'?action=registration&email='.urlencode($email).'&hash='.$hash.'</a></p>
                    <p>Po aktivaci účtu a dokončení registrace budete moci využívat všech funkcí a výhod stránek <strong>www.myteamworld.com</strong>.</p>';
                    
		
		$mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = 'Registrace uživatele - ověření e-mailové adresy';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));

		return $mail->Send();
	}
	
	public static function verifyRegistration($email = '')
	{
		$message = '<p>vaše aktivace účtu na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a> proběhla úspěšně!</p>
					<p>K přihlášení použijte e-mailovou adresu <strong>'.$email.'</strong>.</p>
			        <p>Heslo si můžete kdykoliv změnit v editaci svého profilu v záložce "Nastavení účtu".</p>
			        <br />
			        <p>Začněte naplno využívat možností <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a>!</p>
			        <p>Buďte v centru dění! Nastavte si v editaci svého profilu v záložce "Nastavení nástěnky" odebírání novinek ze všech sportů, které vás zajímají. Na výběr máte z několika nejpopulárnějších sportů. Navíc se při konání zajímavých akcí (mistrovství světa, olympiáda a další) nabídka kanálů může rozšířit!</p>
			        <p>Mějte dokonalý přehled o svém vlastním týmu! Ať už jste rekreační nebo profesionální hráč, trenér, majitel týmu - zaregistrujte váš tým na myteamworld.com, pozvěte svoje spoluhráče a využívejte naplno všech funkcí myteamworld.com!</p>';
		
		$mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = 'Účet byl úspěšně aktivován!';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));
		
        return $mail->Send();
	} 
    
    public static function invitation($email, Team $team, User $user)
    {
        $message = '<p>uživatel '.$user->getAnchor().' vás pozval do týmu '.$team->getTeamLink().' na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a>.</p>
        			<p>Přijmout pozvánku do týmu '.$team->getTeamLink().' můžete na tomto odkazu: <a href="'.PATH_WEB_ROOT.'notices/">'.PATH_WEB_ROOT.'notices/</a>.</p>
        			<p>Před vstupem do týmu '.$team->getName().' si ho nejprve můžete prohlédnout na tomto odkazu '.$team->getTeamLink().'.</p>';
        
        $mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = $user->getName() . ' vás pozval do týmu ' . $team->getName() . ' na myteamworld.com';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));
		
        return $mail->Send();
    }


    public static function newUserInvitation($email, Team $team, User $user)
    {
        $message = '<p>uživatel '.$user->getAnchor().' vás pozval do týmu '.$team->getTeamLink().' na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a>.</p>
        			<p>Pokud chcete vstoupit do týmu '.$team->getTeamLink().', zaregistrujte se prosím na <a href="'.PATH_WEB_ROOT.'">'.PATH_WEB_ROOT.'</a>. Po dokončení registrace můžete přijmout pozvánku do týmu ve vašich <a href="'.PATH_WEB_ROOT.'notices/">upozorněních</a>.</p>
        			<p>Před vstupem do týmu '.$team->getName().' si ho nejprve můžete prohlédnout na tomto odkazu '.$team->getTeamLink().'.</p>';
        
        $mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = $user->getName() . ' vás pozval do týmu ' . $team->getName() . ' na myteamworld.com';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));
		
        return $mail->Send();
    }
    

    public static function userRecommend($email, User $user)
    {
        $message = '<p>uživatel '.$user->getAnchor().' vás pozval na <strong>myteamworld.com</strong>.</p>
        			<p>Pokud chcete zjistit, co je <strong>myteamworld.com</strong> nebo se zaregistrovat, klikněte prosím na tento odkaz <a href="'.PATH_WEB_ROOT.'">www.myteamworld.com</a>.</p>';
        
        $mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com', 'myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = 'Uživatel ' . $user->getName() . ' vás pozval na myteamworld.com';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));
		
        return $mail->Send();
    }

    
    public static function sendNewPassword(User $user, $pass)
	{
		$message = '<p>zasíláme vám nové heslo na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a>.</p>
                    <p>Vaše nové heslo je : '.$pass.'</p>
                    <p>Toto heslo bylo vygenerováno automaticky, po přihlášení na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a> si jej můžete změnit v <strong>editaci profilu</strong> -> záložka <strong>"nastavení účtu"</strong>.</p>';
		
		$mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($user->email);
		$mail->Subject = 'Zaslání zapomenutého hesla';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));

		return $mail->Send();
	}


	public static function inviteTeam($email, User $user, Team $team)
	{
		$message = '<p>tým ' . $team->getName() . ' vás pozval na <strong>myteamworld.com</strong>.</p>
					<p>Pokud chcete zjistit, co je <strong>myteamworld.com</strong> nebo se zaregistrovat, klikněte prosím na tento odkaz <a href="'.PATH_WEB_ROOT.'">www.myteamworld.com</a>.</p>';
		
		$mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com', 'myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = 'Tým ' . $team->getName() . ' vás pozval na myteamworld.com.';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));

		return $mail->Send();
	}
    

    public static function sendFeedback($text, $user)
	{
		$message = '<strong>Uživatel '.$user->getAnchor().' odeslal feedback s následujícím textem:</strong><br />
					'.nl2br(strip_tags($text));
		
		$mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo($user->email);
		$mail->SetFrom('feedback@myteamworld.com', 'myteamworld.com tým');
		$mail->AddAddress('feedback@myteamworld.com');
		$mail->Subject = 'Uživatel ' . $user->getName() . ' odeslal nový feedback';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML($message);

		return $mail->Send();
	}

	public static function sendTeamEntryRequest($email, Team $team, User $user)
    {
        $message = '<p>uživatel '.$user->getAnchor().' vás zažádal o vstup do vašeho týmu '.$team->getTeamLink().' na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a>.</p>
        			<p>Po přihlášení na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a> můžete jeho žádost potvrdit či zamítnout na tomto odkazu: <a href="'.PATH_WEB_ROOT.'notices/">'.PATH_WEB_ROOT.'notices/</a>.</p>';
        
        $mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = $user->getName() . ' vás zažádal o vstup do týmu ' . $team->getName() . ' na myteamworld.com';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));
		
        return $mail->Send();
    }	

	public static function sendConfirmRequest($email, Team $team, User $user)
    {
        $message = '<p>uživatel '.$user->getAnchor().' přijal vaši žádosti pozval do týmu '.$team->getTeamLink().' na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a>.</p>
        			<p>Nyní po přihlášení na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a> můžete využívat všechny moduly a funkce, které má tým '.$team->getTeamLink().'aktivní.</p>';
        
        $mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = $user->getName() . ' přijal vaši žádost o vstup do týmu ' . $team->getName() . ' na myteamworld.com';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));
		
        return $mail->Send();
    }

    public static function sendEventInvitation($email, Team $team, Event $event)
    {
		// info o události
        $einfo = unserialize($event->info);

        switch($einfo[0]['type'])
        {
            case Event::COMPETITION:
                $type = 'Soutěžní zápas';
            break;

            case Event::ACTION:
                $type = 'Týmová akce';
            break;

            case Event::FRIENDLY:
                $type = 'Přátelský zápas';
            break;

            case Event::TRAINING:
                $type = 'Trénink';
            break;
        }

        $message = '<p>váš tým '.$team->getTeamLink().' vás pozval na následující událost:</p><br />
        			<p>'.$type . ' - "' . $einfo[0]['name'] .'", dne '.date('d. m. Y \v H:i', strtotime($event->start)).' hod.</p><br />
        			<p>Vyjádřit se ke své účasti na této události můžete po přihlášení na <a href="'.PATH_WEB_ROOT.'">myteamworld.com</a> na tomto odkazu:  <a href="'.$team->getProfileLink('event-detail').$event->id . '-' . $event->team_id . '/">'.$team->getProfileLink('event-detail').$event->id . '-' . $event->team_id . '/</a></p><br />';
        
        $mail = new self::$client;
		$mail->CharSet = 'utf-8';
		$mail->AddReplyTo('info@myteamworld.com');
		$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
		$mail->AddAddress($email);
		$mail->Subject = 'Tým ' . $team->getName() . ' vás pozval na událost';
		$mail->AltBody = strip_tags($message);
		$mail->MsgHTML(self::createMessage($message));
		
        return $mail->Send();
    }

    public static function sendEventOverview($email, $events, $type = 'day')
    {
    	if(count($events))
    	{
    		if($type == 'day')
    		{
	    		$message = '<p>během zítřejšího dne se ve vašich týmech konají následující události:</p><br />';
	    	}
	    	else
	    	{
	    		$message = '<p>tento týden se ve vašich týmech konají následující události:</p><br />';
	    	}

			foreach($events as $team_id => $list)
			{
				if(count($list))
				{
					$team = TeamDAO::get($team_id);

					if(!$team || empty($team->id))
					{
						continue;
					}

					$message .= '<p><strong>' . $team->getTeamLink() . '</strong></p>';

					$message .= '<p>';
					foreach($list as $array)
					{
						// objekt udalosit
						$event = $array['event'];

						// zda ma vyplnenou dochazku
						$attend_set = $array['attend_set'];

						if(!is_array($event->info))
						{
							$event->info = unserialize($event->info);
						}
						$message .= $event->info[0]['name'] . ', ' . date('j. n. Y H:i', strtotime($event->start)) . ', ' . $event->info[0]['place'];
						if(!$attend_set)
						{
							$message .= ' (na tuto událost ještě nemáte vyplněnou <a href="'.$team->getProfileLink('event-detail').$event->id . '-' . $event->team_id . '">docházku</a>)';
						}
						$message .= "<br />\n";
					}
					$message .= '</p><br />';
				}
			}

	        $mail = new self::$client;
			$mail->CharSet = 'utf-8';
			$mail->AddReplyTo('info@myteamworld.com');
			$mail->SetFrom('info@myteamworld.com','myteamworld.com tým');
			$mail->AddAddress($email);
			if($type == 'day')
    		{
	    		$mail->Subject = 'Denní souhrn událostí';
	    	}
	    	else
	    	{
	    		$mail->Subject = 'Týdenní souhrn událostí';
	    	}
			
			$mail->AltBody = strip_tags($message);
			$mail->MsgHTML(self::createMessage($message));
			
	        return $mail->Send();
	    }
    	return true;
    }
}