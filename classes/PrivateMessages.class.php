<?php

class PrivateMessages extends ObjectDAO
{
    public static function getConversations($user_id)
    {
        $user_id = intval($user_id);
        
        /*$sql = 'SELECT *
                FROM `conversation`
                WHERE `users` LIKE "%-'.$user_id.'-%"';
        
        return self::loadArray($sql);*/
        
        $sql = 'SELECT * 
                FROM `pm`
                LEFT JOIN `user` ON `user`.`user_id` = `pm`.`sent_user_id`
                LEFT JOIN `conversation` ON `conversation`.`id` = `pm`.`conversation_id`
                WHERE `conversation`.`users` LIKE "%-' . $user_id . '-%" AND `pm`.`user_id` = '.$user_id.'
                GROUP BY `conversation`.`users`
                ORDER BY `pm`.`time` DESC';
        
        return self::loadArray($sql);
    }
    
    public static function getConversationDetail($conversation_id, $user_id, $last_id = 0, $limit = VIEW_MESSAGE_LIMIT)
    {
        $sql = 'SELECT * 
                FROM `pm`
                LEFT JOIN `user` ON `user`.`user_id` = `pm`.`sent_user_id`
                WHERE `pm`.`user_id` = '.intval($user_id).' AND `conversation_id` = "' . $conversation_id . '" '.($last_id > 0 ? ' AND `id` < ' . $last_id : '').'
                ORDER BY `id` DESC
                LIMIT '.$limit;
        
        return self::loadArray($sql);
    }
    
    public static function getConversationId($users = '', $user_id = 0)
    {
        $db = new Database();
        
        $sql = 'SELECT `id`
                FROM `conversation`
                WHERE `users` = "'.mysqli_real_escape_string($db->mysqli(), $users).'" AND `users` LIKE "%-'.intval($user_id).'-%"';

        $db->query($sql);
        
        // našli jsme tuto konverzaci
        if($db->getRows() > 0)
        {
            $a = $db->readrow('aarr');
            
            $sql = 'SELECT * FROM `pm` WHERE `conversation_id` = "'.$a['id'].'" AND `user_id` = ' . intval($user_id);
            $db->query($sql);
            if($db->getRows() > 0)
            {
                $type = 'exists';
                $subtype = '';
            }
            else
            {
                $type = 'new';
                $subtype = 'exists';
            }
            
            return array('id' => $a['id'], 'type' => $type, 'subtype' => $subtype);
        }
        
        // vygenerujeme nové ID konverzace
        $id = uniqid('', true).uniqid('', true).uniqid('', true).uniqid('', true);
        $id = str_replace('.', '', $id);
        return array('id' => $id, 'type' => 'new');
    }
    
    public static function getConversationUsers($id, $user_id = 0)
    {
        $db = new Database();
        
        $sql = 'SELECT `users`
                FROM `conversation`
                WHERE `id` = "'.mysqli_real_escape_string($db->mysqli(), $id).'" AND `users` LIKE "%-'.$user_id.'-%"
                LIMIT 1';
                  
        $db->query($sql);
        if($db->getRows() > 0)
        {
            $a = $db->readrow('aarr');
            
            return $a['users'];
        }
        
        return false;
    }
    
    public static function insertConversation($id, $users)
    {
        $db = new Database();
        
        $sql = 'INSERT INTO `conversation`
                SET
                    `id` = "'.mysqli_real_escape_string($db->mysqli(), $id).'",
                    `users` = "'.mysqli_real_escape_string($db->mysqli(), $users).'",
                    `name` = ""';
            
        return $db->query($sql);
    }
    
    public static function updateConversation($id, $users)
    {
        $db = new Database();
        
        $sql = 'UPDATE `conversation`
                SET
                    `users` = "'.mysqli_real_escape_string($db->mysqli(), $users).'"
                WHERE `id` = "'.mysqli_real_escape_string($db->mysqli(), $id).'"';
            
        return $db->query($sql);
    }
    
    public static function insertMessages(array $data)
    {
        if(is_array($data) && count($data) > 0)
        {
            $db = new Database();
            
            $sql = 'INSERT INTO `pm`(
                        `id`,
                        `user_id`,
                        `sent_user_id`,
                        `users`,
                        `conversation_id`,
                        `text`,
                        `time`,
                        `read`
                    )
                    VALUES ';
            
            $rows = array();
            foreach($data as $row)
            {
                $rows[] =  '(
                                "' . mysqli_real_escape_string($db->mysqli(), $row['id']) . '",
                                "' . mysqli_real_escape_string($db->mysqli(), $row['user_id']) . '",
                                "' . mysqli_real_escape_string($db->mysqli(), $row['sent_user_id']) . '",
                                "' . mysqli_real_escape_string($db->mysqli(), $row['users']) . '",
                                "' . mysqli_real_escape_string($db->mysqli(), $row['conversation_id']) . '",
                                "' . mysqli_real_escape_string($db->mysqli(), strip_tags($row['text'])) . '",
                                NOW(),
                                "' . intval($row['read']) . '"
                            )';
            }
                 
            $sql .= implode(',', $rows);
                   
            return self::nonEmpty($sql);
        }
        return false;
    }
    
    public static function deleteConversation($conversation_id, $user_id)
    {
        $db = new Database();
        
        $sql = 'DELETE FROM `pm`
                WHERE `conversation_id` = "'.mysqli_real_escape_string($db->mysqli(), $conversation_id).'" AND `user_id` = ' . intval($user_id);
        
        return $db->query($sql);
    }
    
    public static function getMax($conversation_id, $user_id)
    {
        $db = new Database();
        
        $sql = 'SELECT MAX(`id`) as maximum
                FROM `pm`
                WHERE `conversation_id` = "' . mysqli_real_escape_string($db->mysqli(), $conversation_id) . '" AND `user_id` = ' . $user_id;
        
        if($db->query($sql))
        {
            $row = $db->readrow('aarr');
            return ++$row['maximum'];
        }
        
        return 1;
    }
    
    public static function readConversation($conversation_id = '', $user_id = 0)
    {
        $db = new Database();
        
        $sql = 'UPDATE `pm` 
                SET `read` = 1
                WHERE `conversation_id` = "'.mysqli_real_escape_string($db->mysqli(), $conversation_id).'" AND `user_id` = '.intval($user_id).' AND `read` = 0';
        
        return $db->query($sql);
    }
    
    /*public static function conversationExists($users = '')
    {
        $db = new Database();
        
        $sql = 'SELECT count(*) as pocet 
                FROM `pm`
                WHERE `users` = "'.mysqli_real_escape_string($db->mysqli(), $users).'"';
                
        $db->query($sql);
        $row = $db->readRow('aarr');
        
        return ($row['pocet'] > 0);
    }*/
}

?>