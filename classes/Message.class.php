<?php
class Message
{
	protected $data;
	protected $index;
	
	public function __construct($index)
	{
        $this->index = $index;
		
        $this->data = array(
			'done' => array(),
			'warning' => array(),
			'confirm' => array()
		);
		
		$this->loadMessages();
	}
    
	/**
	 * Load messages from session
	 */
	public function loadMessages()
	{
		global $_project;

        $this->data = array(
			'done' => array(),
			'warning' => array(),
			'confirm' => array()
		);
        
		if($_project['session']->isSetSession($this->index))
		{
			$messages = $_project['session']->get($this->index);
			if(!empty($messages))
			{
				foreach($messages as $key => $value)
				{
					$this->data[$key] = $value;
				}
			}
		}
	}
	
	/**
	 * Save mesages to session
	 */
	public function saveMessages()
	{
		global $_project;
		
		if($_project['session'] instanceof Session)
		{
			$_project['session']->set($this->index,$this->data);
		}
		else
		{
			die('Setup session class first.');
		}
	}
	
	/**
	 * Return warning counts
	 * @return int
	 */
	public function isWarning()
	{
		return count($this->data['warning']);
	}
	
	/**
	 * Return done counts
	 * @return int
	 */
	public function isDone()
	{
		return count($this->data['done']);
	}
	
	/**
	 * Return confirm counts
	 * @return int
	 */
	public function isConfirm()
	{
		return count($this->data['confirm']);
	}
	
	/**
	 * Return warning array
	 * @return array
	 */
	public function getWarnings()
	{
		return $this->data['warning'];
	}
	
	public function getDones()
	{
		return $this->data['done'];
	}
	
	public function getConfirms()
	{
		return $this->data['confirm'];
	}
	
	protected function setMessage($type,$message)
	{
		$this->data[$type][] = $message;
	}
	
	public function addWarning($message = '')
	{
		if(!empty($message))
		{
			$this->setMessage('warning',$message);
		}
	}
	
	public function addDone($message = '')
	{
		if(!empty($message))
		{
			$this->setMessage('done',$message);
		}
	}
	
	public function addConfirm($message = '')
	{
		if(!empty($message))
		{
			$this->setMessage('confirm',$message);
		}
	}
	
	public function clear($type = 'all')
	{
		global $_project;
		
		switch($type)
		{
			case 'warning' : 
				$this->data['warning'] = array();
			break;
			
			case 'done' : 
				$this->data['done'] = array();
			break;
			
			case 'confirm' : 
				$this->data['confirm'] = array();
			break;
			
			default : 
				$this->data = array(
					'done' => array(),
					'warning' => array(),
					'confirm' => array()
				);
			break;
		}
		
		$_project['session']->set($this->index, $this->data);
	}
}
?>