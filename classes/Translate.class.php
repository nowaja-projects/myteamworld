<?php

class Translate extends Object
{
	protected $columns = array(
        'id'            => '',
        'key'           => '',
        'lang'          => '',
        'text'          => '',
        'group'         => '',
        'translated'	=> ''
	);
    
    protected $data = false;
    protected $table = 'translate';
    
	public function __construct($data = array())
	{
        $this->setTable('translate');
		if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}
	}
    
    public function save()
    {
        if(!empty($this->id) && !empty($this->key))
		{
			return TranslateDAO::update($this->getAllData());
		}
		else
		{
			return TranslateDAO::insert($this->getAllData());
		}
    }
    
}