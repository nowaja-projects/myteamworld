<?php
class UserDAO extends ObjectDAO
{
    const KEY_PREFIX = 'apc_user_';

	public static function get($id)
	{
		$id = intval($id);

        $user = Cache::getFromCache(self::KEY_PREFIX . $id);  

        if($user !== false && $user instanceof User && !empty($user->id))
        {
            return $user;
        }

		$sql = 'SELECT * 
                FROM `user` 
                WHERE `user_id` = '.$id;
		
        $user = self::load($sql, 'User');

        Cache::store(self::KEY_PREFIX . $id, serialize($user));

        return $user;
	}
    
    public static function getByEmail($email)
	{
        $db = new Database();
		$email = $db->escapeString($email);
		
        $sql = 'SELECT * 
                FROM `user` 
                WHERE `user_email` = "'.$email .'"
                LIMIT 1';
		
        return self::load($sql, 'User');
	}
    
    public static function getAll()
	{
		$id = intval($id);
		$sql = 'SELECT * 
                FROM `user`';
		
        return self::loadArray($sql, 'User');
	}
    
    public static function getByKey($id, $key)
	{
        $db = new Database();
		$id = intval($id);

        $user = Cache::getFromCache(self::KEY_PREFIX . $id);  
        if($user !== false && $user instanceof User && !empty($user->id) && $user->key == $key)
        {
            return $user;
        }

		$sql = 'SELECT * 
                FROM `user` 
                WHERE `user_key` = "'.mysqli_real_escape_string($db->mysqli(), $key).'" AND `user_id` = '.$id;
		$user = self::load($sql, 'User');

        Cache::store(self::KEY_PREFIX . $id, serialize($user));

        return $user;
	}
	
	public static function login($email,$password)
	{
		$db = new Database();
		$email = $db->escapeString($email);
		$password = self::encrypt($password);
		
		$sql = 'SELECT * 
                FROM `user` 
                WHERE `user_email` = "'.$email.'" AND `user_password` = "'.$password.'"';
		
		return self::load($sql, 'User', $db);
	}
    
    public static function changePassword($password, $user_id)
    {
        $user_id = intval($user_id);
        $password = self::encrypt($password);
        $sql = 'UPDATE `user`
                SET `user_password` = "'.$password.'" 
                WHERE `user_id` = ' . $user_id;

        Cache::delete(self::KEY_PREFIX . $user_id);

        return self::nonEmpty($sql);
    }
    
    public static function updateVersion($id, $version)
    {
        $sql = 'UPDATE `user` 
                SET `user_version` = "'.$version.'"
                WHERE `user_id` = ' . $id;

        Cache::delete(self::KEY_PREFIX . $id);
                
        return self::nonEmpty($sql);
    }
	
	public static function encrypt($string = '')
	{
		return sha1($string);
	}
	
	public static function insert($data = array())
	{
		$db = new Database();
		if($db->insertArray($data, 'user'))
		{
			return $db->getInsertedId();
		}
		else
		{
			return false;
		}
	}
	
	public static function emailExists($email, $user_id = 0)
	{
		$db = new Database();
		$email = $db->escapeString($email);
        $user_id = intval(@$user_id);

		$sql = 'SELECT NULL FROM `user` WHERE `user_id` != '.$user_id.' AND `user_email` = "'.$email.'"';
		
		if($db->query($sql))
		{
			if($db->getRows())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public static function update($data = array())
	{
		$db = new Database();
    
        Cache::delete(self::KEY_PREFIX . $data['id']);

		return $db->updateArray($data, 'user', 'user_id');

	}

    public static function delete($id)
    {
        $db = new Database();

        $sql = 'DELETE FROM `user_in_team` WHERE `user_id` = ' . intval($id);

        $db->query($sql);
        
        $sql = 'UPDATE `user` SET `user_deleted` = 1 WHERE `user_id` = ' . intval($id);

        Cache::delete(self::KEY_PREFIX . $id);
        
        return $db->query($sql);
    }
	
	public static function getUserTeamList($user_id = 0)
	{
		$db = new Database();
		
		$sql = 'SELECT *, SUM(`player`) as players, SUM(`fan`) AS fans, SUM(`observer`) as observers
				FROM `user_in_team` 
                LEFT JOIN `team` ON `team`.`team_id` = `user_in_team`.`team_id`
				WHERE `user_in_team`.`fan` = 0 AND `user_in_team`.`player` = 1 AND `user_id` = '.intval($user_id) . 
                ' GROUP BY  `user_in_team`.`team_id`';
		
        $db->query($sql);
        
        $return = array();
        while($a = $db->readrow(''))
        {
            if($a['team_id'] > 0)
            {
                $team = new Team($a);
                $team->getLists();
                $return[$a['team_id']] = $team;
            }
        }
        		
		return $return;
	}
    
    public static function getUserFanTeamList($user_id = 0)
	{
		$db = new Database();
		
        $sql = 'SELECT *, SUM(`player`) as players, SUM(`fan`) AS fans, SUM(`observer`) as observers
                FROM `team` 
                LEFT JOIN `user_in_team` ON `user_in_team`.`team_id` = `team`.`team_id`
                WHERE `team`.`team_id` IN (SELECT `team_id` FROM `user_in_team` WHERE `user_in_team`.`fan` = 1 AND `user_id` = '.intval($user_id).') 
                GROUP BY  `user_in_team`.`team_id`';
        
        $db->query($sql);
        
        $return = array();
        while($a = $db->readrow(''))
        {
            if($a['team_id'] > 0)
            {
                $team = new Team($a);
                $team->getLists();
                $return[$a['team_id']] = $team;
            }
        }
        		
		return $return;
	}
    
    public static function getUserObserveTeamList($user_id = 0)
	{
		$db = new Database();
		
        $sql = 'SELECT *, SUM(`player`) as players, SUM(`fan`) AS fans, SUM(`observer`) as observers
                FROM `team` 
                LEFT JOIN `user_in_team` ON `user_in_team`.`team_id` = `team`.`team_id`
                WHERE `team`.`team_id` IN (SELECT `team_id` FROM `user_in_team` WHERE `user_in_team`.`observer` = 1 AND `user_id` = '.intval($user_id).') 
                GROUP BY  `user_in_team`.`team_id`';
        
        $db->query($sql);
        
        $return = array();
        while($a = $db->readrow(''))
        {
            if($a['team_id'] > 0)
            {
                $team = new Team($a);
                $team->getLists();
                $return[$a['team_id']] = $team;
            }
        }
        		
		return $return;
	}
    
    public static function setActiveTeam($user_id = 0, $team_id = 0)
    {
        $db = new Database();
        
        $sql = 'UPDATE `user_in_team` 
                SET `active_team` = 0
                WHERE `user_id` = ' . intval($user_id);
        
        $db->query($sql);
        
        $sql = 'UPDATE `user_in_team` 
                SET `active_team` = 1
                WHERE `team_id` = '.intval($team_id).' AND `user_id` = ' . intval($user_id);

        Cache::delete(self::KEY_PREFIX . $user_id);

        return $db->query($sql);
    }
    
    public static function getActiveTeam($user_id = 0)
    {
        $db = new Database();
		
		$sql = 'SELECT `team`.*, `user_in_team`.`active_team`, `user_in_team`.`team_id`
				FROM `team` 
                LEFT JOIN `user_in_team` ON `user_in_team`.`team_id` = `team`.`team_id`
				WHERE `user_in_team`.`active_team` = 1 AND `user_in_team`.`user_id` = '.intval($user_id) .
                ' GROUP BY  `user_in_team`.`team_id`';
		
        $db->query($sql);
        		
        $a = $db->readrow('aarr');
        
        if($a['team_id'] > 0)
        {
            $return = TeamDAO::get($a['team_id']);
            //$return = new Team($a);
            //$return->fans = count(TeamDAO::getFanList($return->id));
            //$return->players = count(TeamDAO::getPLayerList($return->id));
            //$return->observers = count(TeamDAO::getObserverList($return->id));
        }
        else
        {
            $return = new Team();
        }
        
        return $return;
    }
    
    public static function getRssSources($id = 0)
    {
        $sql = 'SELECT * 
                FROM `rss_user` 
                WHERE `user_id` = ' . $id;
        
        $a = self::loadArray($sql);
        
        $return = array();
        
        foreach($a as $val)
        {
            $return[] = $val['source_id'];
        }
        
        return $return;
    }
    
    public static function getUserSports($id = 0)
    {
        $sql = 'SELECT * 
                FROM `rss_user` 
                LEFT JOIN `rss_source` ON `rss_source_id` = `rss_user`.`source_id`
                LEFT JOIN `sport` ON `sport_id` = `rss_source_sport_id`
                WHERE `user_id` = ' . $id . '
                GROUP BY `sport_name`';
        
        $a = self::loadArray($sql);
        
        $return = array();
        foreach($a as $val)
        {
            $return[$val['sport_id']] = $val['sport_name'];
        }
        
        return $return;
    }
       
    public static function getResidence($id = 0)
    {
        $id = intval($id);
        if($id > 0)
        {
            $db = new Database();
            $sql = 'SELECT * FROM `cities` WHERE `id` = ' . $id;
            
            $db->query($sql);
            return $db->readrow('aarr');
        }
        return array();
    } 


    public static function getLastActivities($user_id, $from = false, $limit = LAST_ACTIVITIES_LIMIT)
    {
        /**
         * CO VSECHNO BUDE NA NASTENCE?
         *
         * okomentovani tiskove zpravy/vzkazu na nastence/tiskove zpravy/zapasu (vse v tabulce object)
         * kdyz uzivatel vstoupi do tymu (user_in_team)
         * kdyz se uzivatel stal fanouskem nejakeho tymu (user_in_team)
         * kdyz se uzivatel stal odberatelem tymu (user_in_team)
         * ze se mu libi zmena na soupisce/odkaz/komentar (like)
         * 
         */
        
        $user_id    = intval($user_id);
        $limit      = intval($limit);
        $return     = array();
        
        if(!empty($user_id))
        {
            $db = new Database();
            $i = 0;
            
            // nacteme komentare
            $sql = 'SELECT *
                    FROM `object`
                    WHERE `object_user_id` = '.$user_id.' AND `object_type` IN ("reply")' . ($from ? ' AND `object_created` < "'.mysqli_real_escape_string($db->mysqli(), $from).'"' : '') . '
                    ORDER BY `object_created` DESC
                    LIMIT ' . $limit . '
                    ';
                    
            $db->query($sql);        
            while($a = $db->readrow('aarr'))
            {
                $return[$a['object_created'].';'.$i++] = array(
                    'type'  => 'reply',
                    'info'  => $a
                );
            }
                    
            // nacteme tymy kterych se stal fanouskem
            $sql = 'SELECT * 
                    FROM `user_in_team` 
                    WHERE `fan` = 1 AND `user_id` = ' . $user_id . ($from ? ' AND `added` < "'.mysqli_real_escape_string($db->mysqli(), $from).'"' : '') . '
                    ORDER BY `added` DESC
                    LIMIT ' . $limit;
                    
            $db->query($sql);
            while($a = $db->readrow('aarr'))
            {
                $return[$a['added'].';'.$i++] = array(
                    'type'  => 'user_in_team',
                    'info'  => $a
                );
            }
            
            // nacteme vsechno co se mu libilo
            $sql = 'SELECT *
                    FROM `like`
                    WHERE `user_id` = ' . $user_id . ($from ? ' AND `time` < "'.mysqli_real_escape_string($db->mysqli(), $from).'"' : '') . '
                    ORDER BY `time` DESC
                    LIMIT ' . $limit;
                    
            $db->query($sql);
            while($a = $db->readrow('aarr'))
            {
                $return[$a['time'].';'.$i++] = array(
                    'type'  => 'like',
                    'info'  => $a
                );
            }

            // seradime podle klice
            krsort($return);

            // chceme vratit jen spravny pocet prvku
            $return = array_slice($return, 0, $limit);
        }
        
        /*if(count($return) < $limit)
        {
            $return[]
        }*/

        return $return;
    }


    public static function helpSwitchShowed($user_id)
    {
        $sql = 'UPDATE `user` SET `user_show_switch_help` = 0 WHERE `user_id` = ' . intval($user_id);

        Cache::delete(self::KEY_PREFIX . $user_id);

        return self::nonEmpty($sql);
    }

    public static function helpTeamShowed($user_id)
    {
        $sql = 'UPDATE `user` SET `user_show_team_help` = 0 WHERE `user_id` = ' . intval($user_id);   

        Cache::delete(self::KEY_PREFIX . $user_id);

        return self::nonEmpty($sql);
    }
}
?>