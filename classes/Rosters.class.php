<?php
class Rosters extends ObjectDAO
{
	public static function getTeamRoster($team_id = 0, $positions = true)
    {
        $db = new Database();
        
        $sql = 'SELECT *, `user_in_team`.`user_id` AS id 
                FROM `user_in_team` 
                LEFT JOIN `roster_position` ON `roster_position`.`id` = `position_id`
                LEFT JOIN `user` ON `user`.`user_id` = `user_in_team`.`user_id` 
                WHERE `team_id` = ' . intval($team_id) . ' AND `player` = 1
                ORDER BY `user`.`user_sname` ASC';
        
        $db->query($sql);
        
        $return = array();
        
        while($a = $db->readrow('aarr'))
        {
            if($positions)
            {
                $return[$a['position_id']][$a['user_id']]['user'] = UserDAO::get($a['user_id']);
                $return[$a['position_id']][$a['user_id']]['info'] = new RosterItem($a);
            }
            else
            {
                $return[$a['user_id']]['user'] = UserDAO::get($a['user_id']);
                $return[$a['user_id']]['info'] = new RosterItem($a);
            }
        } 
        
        return $return;
    }
    
    public static function getUserInTeamPosition($team_id = 0, $user_id = 0)
    {
        $roster = Rosters::getTeamRoster($team_id);

        foreach($roster as $position_id => $players)
        {
            if(isset($players[$user_id]))
            {
                return Positions::get($position_id);
            }
        }

        return false;
    }

    public static function updateRosterItem($item = array())
    {
        return true;
    }
    
    public static function updateRoster($data = array(), Team $team)
    {
        if(count($data) > 0 && $team->id > 0)
        {
            $db = new Database();

            foreach($data as $player => $info)
            {
                // osetrime zda nam nechodi podvrhla data
                $pid = explode('-', $player);

                // kontrola hashe
                if($pid[1] != getRecipientHash($pid[0]))
                {
                    continue;
                }

                // nacteme si uzivatele
                $user = UserDAO::get($pid[0]);

                // zkontrolujeme zda je clenem tymu, nebo kdyz je majitel tak ho taky nesmime smazat
                if(!$team->isPlayer($user))
                {
                    continue;
                }



                // FIXME - kontrola zda existuje position_id
                if(!empty($info['deleted']) && $team->user_id != $user->id)
                {
                    $sql = 'DELETE FROM `roster_invite` 
                            WHERE `user_id` = "'.$user->id.'" AND `team_id` = "'.$team->id.'"';
                    $db->query($sql);

                    $sql = 'DELETE FROM `user_in_team` 
                            WHERE `user_id` = "'.$user->id.'" AND `team_id` = "'.$team->id.'"';

                    if(!$db->query($sql))
                    {
                        return false;
                    }
                }
                elseif(empty($info['deleted']))
                {
                    /*if($user->isFakeUser())
                    {
                        $host = 1;
                    }
                    else
                    {
                        
                    }*/

                    $host = intval(@$info['host']);
                    $sql = 'UPDATE `user_in_team` 
                            SET 
                                `number` = "'.$info['number'].'",
                                `position_id` = "'.$info['position_id'].'",
                                `host` = "'.$host.'",
                                `show` = "'.intval(@$info['show']).'",
                                `function` = "'.mysqli_real_escape_string($db->mysqli(), $info['function']).'"
                            WHERE `user_id` = "'.$user->id.'" AND `team_id` = "'.$team->id.'"';

                    if(!$db->query($sql))
                    {
                        return false;
                    }
                }
            }
            
            return true;
        }
        
        return false;
    }
    
}
?>