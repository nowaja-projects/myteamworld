<?php

class Image
{
	const CACHE_PATH 			= DEFAULT_CACHE_PATH;
	const GALLERY_CACHE_PATH 	= GALLERY_CACHE_PATH;


	/**
	 * Vrati nazev souboru podle zakladu.
	 * 
	 * @param string $path absolutni cesta k souboru
	 * @param string $old_name jmeno souboru
	 * @param string $modifier modifikatory pro imagemagick 
	 * @param boolean $check
	 * @param string $extension pripona 
	 * @return string
	 */
	public static function get($path, $old_name, $new_name, $modifier = '', $check = true, $extension='jpg', $cache_path = self::CACHE_PATH)
	{
		if(!empty($modifier))
		{
			$name = pathinfo($old_name, PATHINFO_FILENAME);
			$ext = pathinfo($old_name, PATHINFO_EXTENSION);
							
			if($check && !file_exists($new_name))
			{
				self::delete($new_name, $cache_path);
				self::resize($path . $old_name, $new_name, $modifier);
			}

			return $new_name;
		}
		
		return $path . $old_name;
	}
	
	/**
	 * Zmensi zadany obrazek podle parametru.
	 * Vrati uspech nebo neuspech.
	 * 
	 * @param string $original
	 * @param string $resized
	 * @param string $modifier modifikator pro imagemagick 
	 * @return boolean
	 */
	private static function resize($original, $resized, $modifier = '-quality 90 -resize 200x200')
	{
		if(file_exists($original) && strpos($modifier,';') === false)
		{
			@chmod($original,0777);
			@exec('convert '.$original.'\'[0]\' '.strval($modifier).' '.$resized.';');
			@chmod($resized,0777);
			
			// Zmena velikosti provedena, pokud existuje novy soubor
			return file_exists($resized);
		}
		
		// Original neexistuje
		return false;
	}
	
	/**
	 * Vytvori hash podle nazvu souboru.
	 * 
	 * @param string $original
	 * @return string
	 */
	public static function hash($file, $modifier)
	{
		return hash('sha1', $file . $modifier . SALT_TEAM);
	}
	
	/**
	 * Smaze zadany soubor.
	 * Podle parametru smaze vsechny se stejnym prefixem.
	 * 
	 * @param string $path
	 * @param string $old_name
	 * @param boolean $similar
	 * @return boolean
	 */
	public static function delete($name, $path = self::CACHE_PATH)
	{
		$info = pathinfo($name, PATHINFO_BASENAME);
		$parts = explode('-', $info);
		$prefix = $parts[0] . '-' . $parts[1] . '-' . $parts[2] . '-' . $parts[3] . '-';

		@exec('rm '.$path . $prefix.'*;');

		return true;
	}
}