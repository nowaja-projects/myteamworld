<?php
class Notification extends Object
{
    protected $columns = array(
        'id'         => '',
        'user_id'    => '',
        'object_id'  => '',
        'type_id'    => '',
        'info'       => '',
        'object_info' => '',
        'timestamp'  => '',
        'read'       => ''
    );

    public $text = false;

    public function __construct($data = array())
    {
        $this->setTable('notifications');

        if(is_array($data) && count($data))
        {
            $this->parseAndSet($data);
        }
    }

    public function replace_patterns($alert = false)
    {
        if(!empty($this->text))
        {
            $this->replace_event();

            $this->replace_date();

            $this->replace_team_links($alert);

            $this->replace_team_links_entry($alert);

            $this->replace_team_name();

            $this->replace_team_user();

            $this->replace_users();

            $this->replace_decline();

            $this->replace_decline2();

            $this->replace_object_name();

            $this->replace_press_name();

            $this->replace_user_team();

            $this->replace_oponent_team();

            if(!$this->replace_new_teampost())
            {
                $this->text = '';
            }
        }

    }

    private function replace_new_teampost()
    {
        global $tr;

        if(substr_count($this->text, '{NEW-TEAMPOST}'))
        {
            $this->object_info = unserialize($this->object_info);

            if(!$this->object_info)
            {
                return false;
            }

            $object_id = $this->object_info->team_id . '_' . $this->object_info->id . '_teampost';
            $object = WallDAO::getObject($object_id);
            $object = current($object);

            if(!$object)
            {
                return false;
            }

            $team = TeamDAO::get($this->object_info->team_id);

            if(!$team instanceof Team)
            {
                return false;
            }

            $replace = '<a href="'.$team->getTeampostDetailLink($tr->tr('tymovy-vzkaz'), $object_id).'">nový týmový vzkaz</a>';


            $this->text = str_replace('{NEW-TEAMPOST}', $replace, $this->text);
        }

        return true;
    }

    private function replace_team_links($alert = false)
    {
        if(substr_count($this->text, '{TEAM-LINKS}'))
        {
            if($alert)
            {
                 $replace = '<span class="responseText"><a class="response" href="'.PATH_WEB_ROOT.'notices/">Odpovědět</a></span>';
            }
            else
            {
                $info = unserialize($this->info);
                $data = array(
                    //'id'      => $this->id,
                    'id'      => $info['invite_id'],
                    'user_id' => $info['invited_user']
                );

                // info o pozvance
                $invite = RosterInvites::getInvite($data);

                if(!isset($invite['status']))
                {
                    $invite['status'] = '';
                }

                $replace = '';
                if($invite['status'] == 'invited')
                {
                    // vytvoreni kontrolniho hashe, pro prijeti nebo odmitnuti pozvanky
                    $hash = getInvitationHash($info['team'], $info['logged_user'], $info['invited_user']);

                    // adresa na potvrzeni a odmitnuti
                    $url = 'invite_id='.$info['invite_id'].'&amp;team_id='.$info['team'].'&amp;logged_user_id='.$info['logged_user'].'&amp;invited_user_id='.$info['invited_user'].'&amp;invitation_hash=' . $hash;
                    $confirm_url = $url . '&amp;confirmInvitation=1';
                    $decline_url = $url . '&amp;declineInvitation=1';

                    $replace = '
                    <span class="responseText">
                        Chceš vstoupit do týmu?&nbsp;<a class="response" href="#" data-href="'.$confirm_url.'">ANO</a>&nbsp;/&nbsp;<a class="response" href="#" data-href="'.$decline_url.'">NE</a>
                    </span>
                    ';
                }
                elseif($invite['status'] == 'declined')
                {
                    $replace = '<span class="decline">Odmítnuto!</span>';
                }
                elseif($invite['status'] == 'confirmed')
                {
                    $replace = '<span class="confirm">Přijato!</span>';
                }
            }

            $this->text = str_replace('{TEAM-LINKS}', $replace, $this->text);
        }
    }


    private function replace_team_links_entry($alert = false)
    {
        if(substr_count($this->text, '{TEAM-ENTRY-LINKS}'))
        {
            if($alert)
            {
                 $replace = '<span class="responseText"><a class="response" href="'.PATH_WEB_ROOT.'notices/">Odpovědět</a></span>';
            }
            else
            {
                $info = unserialize($this->info);
                $data = array(
                    //'id'      => $this->id,
                    'id'      => $info['request_id'],
                    'user_id' => $info['requested_user']
                );

                // info o pozvance
                $invite = RosterInvites::getRequest($data);

                if(!isset($invite['status']))
                {
                    $invite['status'] = '';
                }

                $replace = '';
                if($invite['status'] == 'requested')
                {
                    // vytvoreni kontrolniho hashe, pro prijeti nebo odmitnuti pozvanky
                    $hash = getRequestHash($info['team'], $info['logged_user'], $info['requested_user']);

                    // adresa na potvrzeni a odmitnuti
                    $url = 'request_id='.$info['request_id'].'&amp;team_id='.$info['team'].'&amp;logged_user_id='.$info['logged_user'].'&amp;request_hash=' . $hash;
                    $confirm_url = $url . '&amp;confirmRequest=1';
                    $decline_url = $url . '&amp;declineRequest=1';

                    $replace = '
                    <span class="responseText">
                        Chcete potvrdit žádost o vstup do týmu?&nbsp;<a class="response" href="#" data-href="'.$confirm_url.'">ANO</a>&nbsp;/&nbsp;<a class="response" href="#" data-href="'.$decline_url.'">NE</a>
                    </span>
                    ';
                }
                elseif($invite['status'] == 'declined')
                {
                    $replace = '<span class="decline">Zamítnuto!</span>';
                }
                elseif($invite['status'] == 'confirmed')
                {
                    $replace = '<span class="confirm">Přijato!</span>';
                }
            }

            $this->text = str_replace('{TEAM-ENTRY-LINKS}', $replace, $this->text);
        }
    }


    private function replace_press_name()
    {
        global $tr, $logged_user;

        // nazev objektu plus odkaz
        if(substr_count($this->text, '{PRESS-NAME}'))
        {
            $object = WallDAO::getObject($this->object_id);

            if(!count($object))
            {
                $this->text = '';

                return true;
            }

            $replace = '';


            foreach($object as $key => $val)
            {
                $object_id = $val['team_id'] . '_' . $val['id'] . '_press';
                $value = unserialize($val['value']);

                $team = new Team($val);
                // kvůli ajaxu
                if(empty($team->name))
                {
                    $team = TeamDAO::get($val['team_id']);
                }

                if(!$team->userCanViewModule('press-releases', $logged_user))
                {
                    $this->text = '';

                    return true;
                }

                $link = $team->getPressDetailLink($value->heading, $object_id);

                $replace = '<a href="'.$link.'">'.$value->heading.'</a>';
            }

            $this->text = str_replace('{PRESS-NAME}', $replace, $this->text);
        }
    }


    private function replace_object_name()
    {
        global $tr;

        // nazev objektu plus odkaz
        if(substr_count($this->text, '{OBJECT_NAME}'))
        {
            $object = WallDAO::getObject($this->object_id);

            if(!count($object))
            {
                $this->text = '';

                return true;
            }

            $replace = '';

            foreach($object as $key => $val)
            {
                $value = unserialize($val['value']);

                

                if(is_object($value))
                {
                    switch($value->type)
                    {
                        case 'press':
                            $team = TeamDAO::get($val['team_id']);
                            $replace = 'k tiskové zprávě "<strong><a href="'.$team->getPressDetailLink($value->heading, $key).'">'.$value->heading.'</a></strong>"';
                            break;

                        case 'teampost':
                            /*$before = 'k týmovému vzkazu ';
                            $team = TeamDAO::get($value->team_id);
                            $title = $tr->tr('Vzkaz pro uživatele týmu') . ' ' . $team->getName();*/

                            $team = TeamDAO::get($val['team_id']);
                            $replace = 'k "<strong><a href="'.$team->getTeampostDetailLink($tr->tr('tymovy-vzkaz'), $key).'">týmovému vzkazu</a></strong>"';
                            break;

                        default:
                            $replace = 'ke článku "<strong><a href="'.getNewsLink($key, $value->title).'">'.$value->title.'</a></strong>"';
                            break;
                    }

                    
                }
                else
                {
                    // TODO - komentar ke zmene na soupisce?
                    if($val['type'] == 'roster-delete' || $val['type'] == 'roster-insert')
                    {
                        $team = TeamDAO::Get($val['team_id']);

                        if(!$team instanceof Team)
                        {
                            $this->text = '';

                            return true;
                        }
                        $href = $team->getPressDetailLink('Změny na soupisce', $this->object_id);
                        $replace = 'ke "<strong><a href="'.$href.'">změně na soupisce</a></strong>" týmu <strong>'.$team->getTeamLink().'</strong>';
                    }
                }

            }

            $this->text = str_replace('{OBJECT_NAME}', $replace, $this->text);
        }
    }

    private function replace_decline2()
    {
        preg_match('/{DECLINE2:([^;]+);([^;]+);([^}]+)}/', $this->text, $matches);

        // skloňování pro tři tvary
        if(count($matches) > 0)
        {
            $users = unserialize($this->info);

            $replace = decline(count($users['users']), $matches[1], $matches[2], $matches[3]);

            $this->text = str_replace($matches[0], $replace, $this->text);

            return true;
        }

        // pro pouhe dva tvary
        preg_match('/{DECLINE2:([^;]+);([^}]+)}/', $this->text, $matches);

        // skloňování
        if(count($matches) > 0)
        {
            $users = unserialize($this->info);

            $replace = decline(count($users['users']), $matches[1], $matches[2]);
            
            $this->text = str_replace($matches[0], $replace, $this->text);

            return true;
        }
    }

    private function replace_decline()
    {
        preg_match('/{DECLINE:([^;]+);([^;]+);([^}]+)}/', $this->text, $matches);

        // skloňování pro tři tvary
        if(count($matches) > 0)
        {
            $users = unserialize($this->info);

            $replace = decline(count($users['users']), $matches[1], $matches[2], $matches[3]);

            $this->text = str_replace($matches[0], $replace, $this->text);

            return true;
        }

        // pro pouhe dva tvary
        preg_match('/{DECLINE:([^;]+);([^}]+)}/', $this->text, $matches);

        // skloňování
        if(count($matches) > 0)
        {
            $users = unserialize($this->info);



            $replace = decline(count($users['users']), $matches[1], $matches[2]);

            $this->text = str_replace($matches[0], $replace, $this->text);

            return true;
        }
    }

    private function replace_team_name()
    {
        // název týmu
        if(substr_count($this->text, '{TEAM-NAME}'))
        {
            $info = unserialize($this->info);
            $object = unserialize($this->object_info);

            if(!empty($info['team']))
            {
                $team_id = $info['team'];
            }
            elseif(!empty($object->team_id))
            {
                $team_id = $object->team_id;
            }
            elseif(!empty($object->value['team_id']))
            {
                $team_id = $object->value['team_id'];
            }
            elseif(!empty($this->object_id))
            {
                $team_id = $this->object_id;
            }
            else
            {
                return false;
            }

            $team = TeamDAO::get($team_id);

            if($team instanceof Team && !empty($team->id))
            {
                $this->text = str_replace('{TEAM-NAME}', $team->getTeamLink(), $this->text);
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    private function replace_user_team()
    {
        // název týmu
        if(substr_count($this->text, '{USER-TEAM}'))
        {
            $info = unserialize($this->info);

            $team = TeamDAO::get(@$info['user_team']);

            if($team instanceof Team && !empty($team->id))
            {
                $this->text = str_replace('{USER-TEAM}', $team->getTeamLink(), $this->text);
            }
            else
            {
                $this->text = '';
            }
        }

        return true;
    }

    private function replace_oponent_team()
    {
        // název týmu
        if(substr_count($this->text, '{OPONENT-TEAM}'))
        {
            $info = unserialize($this->info);

            $team = TeamDAO::get(@$info['oponent_team']);

            if($team instanceof Team && !empty($team->id))
            {
                $this->text = str_replace('{OPONENT-TEAM}', $team->getTeamLink(), $this->text);
            }
            else
            {
                $this->text = '';
            }
        }

        return true;
    }

    private function replace_event()
    {
        global $logged_user;
        // název týmu
        if(substr_count($this->text, '{EVENT}'))
        {
            $array = unserialize($this->info);

            // vytahneme si info o udalosti
            $object_id = explode('_', $this->object_id);
            $event_id = $object_id[1];
            $team_id = $object_id[0];
            $event = EventDAO::get(intval($event_id), intval($team_id));

            if($event instanceof Event)
            {
                $info = unserialize($this->info);
                if(!empty($info['team']))
                {
                    $team = $info['team'];
                }
                elseif(!empty($object->team_id))
                {
                    $team = $object->team_id;
                }
                elseif(!empty($object->value['team_id']))
                {
                    $team = $object->value['team_id'];
                }
                elseif(!empty($this->object_id))
                {
                    $team = $this->object_id;
                }
                else
                {
                    return false;
                }

                $team = TeamDAO::get($team);

                if(!is_array($event->info))
                {
                    $event->info = unserialize($event->info);
                }
                $this->text = str_replace('{EVENT}', '<a href="'.$team->getProfileLink('event-detail') . $event_id.'-'.$team_id.'">' . $event->info[0]['name'] . '</a>', $this->text);
            }
            else
            {
                $this->text = '';
            }
        }

        // název týmu
        if(substr_count($this->text, '{EVENTS-LINK}'))
        {
            $array = unserialize($this->info);

            // vytahneme si info o udalosti
            $object_id = explode('_', $this->object_id);
            $event_id = $object_id[1];
            $team_id = $object_id[0];
            $team = TeamDAO::get(intval($team_id));

            if($team instanceof Team)
            {
                $this->text = str_replace('{EVENTS-LINK}', $team->getProfileLink('events'), $this->text);
            }
            else
            {
                $this->text = '';
            }
        }
    }


    private function replace_date()
    {
        // název týmu
        if(substr_count($this->text, '{EVENT-DATE}'))
        {
            $array = unserialize($this->info);
            $object_id = explode('_', $this->object_id);
            $event_id = $object_id[1];
            $team_id = $object_id[0];
            $event = EventDAO::get(intval($event_id), intval($team_id));

            if($event instanceof Event)
            {
                if(!is_array($event->info))
                {
                    $event->info = unserialize($event->info);
                }
                $this->text = str_replace('{EVENT-DATE}', date('d.m.Y', strtotime($event->info[0]['start_date'])), $this->text);
            }
            else
            {
                $this->text = '';
            }
        }
    }

    private function replace_team_user()
    {
        // název týmu
        if(substr_count($this->text, '{TEAM-LOGGED-USER}'))
        {
            $info = unserialize($this->info);

            if(!empty($info['logged_user']))
            {
                $user = UserDAO::get($info['logged_user']);
            }
            elseif(!empty($info['user_id']))
            {
                $user = UserDAO::get($info['user_id']);
            }
            else
            {
                $this->text = '';

                return true;
            }

            if($user instanceof User && $user->id > 0)
            {
                $this->text = str_replace('{TEAM-LOGGED-USER}', $user->getAnchor(), $this->text);
            }
            else
            {
                $this->text = str_replace('{TEAM-LOGGED-USER}', 'Neexistující uživatel', $this->text);
            }
        }

        return true;
    }

    private function replace_users()
    {
        // výpis uživatelů
        if(substr_count($this->text, '{USERS}'))
        {
            // nacteme si seznam uzivatelu
            $users = unserialize($this->info);
            $users = @$users['users'];

            // nacteme si uzivatele pro vypis
            $tmp = array();
            $i = 0;
            // FIXME!
            if(!is_array($users))
            {
                return false;
            }
            $users = array_reverse($users, true);

            foreach(array_keys($users) as $user_id)
            {
                $user = UserDAO::get($user_id);
                if(!$user instanceof User || empty($user->id))
                {
                    continue;
                }

                $tmp[] = '<strong>' . $user->getAnchor() . '</strong>';
            }

            if(count($tmp) == 0)
            {
                $replace = '';
            }
            elseif(count($tmp) == 1)
            {
                $replace = current($tmp);
            }
            elseif(count($tmp) == NOTICE_USER_COUNT)
            {
                $replace = implode($tmp, ' a ');
            }
            elseif(count($tmp) > NOTICE_USER_COUNT)
            {
                $other = count($tmp) - NOTICE_USER_COUNT;
                $replace = implode(array_slice($tmp, 0, NOTICE_USER_COUNT, true), ', ') . ' a <strong>' . $other .' ' .decline($other, 'další', 'další', 'dalších') . ' ' . decline($other, 'uživatel', 'uživatelé', 'uživatelů') . '</strong>';
            }

            $this->text = str_replace('{USERS}', $replace, $this->text);
        }

        return true;
    }
}
?>