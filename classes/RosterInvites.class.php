<?php
class RosterInvites extends ObjectDAO
{
    public static function insert($data = array())
    {
        $db = new Database();
        
        $inserted_id = self::getMax($data['user_id']);
        $sql = 'INSERT INTO `roster_invite`
                SET
                    `id`      = "'.$inserted_id.'",
                    `team_id` = "'.intval($data['team_id']).'",
                    `user_id` = "'.intval($data['user_id']).'",
                    `is_host` = "'.intval($data['is_host']).'",
                    `logged_user_id` = "'.intval($data['logged_user_id']).'",
                    `email`   = "'.mysqli_real_escape_string($db->mysqli(), $data['email']).'",
                    `invited` = NOW(),
                    `status`  = "invited",
                    `hash`    = "'.mysqli_real_escape_string($db->mysqli(), $data['hash']).'"';
                    
        if($db->query($sql))
        {
            return $inserted_id;
        }
        
        return false;   
    }


    public static function insertRequest($data = array())
    {
        $db = new Database();
        
        $inserted_id = self::getMaxRequest($data['user_id']);
        $sql = 'INSERT INTO `roster_request`
                SET
                    `id`      = "'.$inserted_id.'",
                    `team_id` = "'.intval($data['team_id']).'",
                    `user_id` = "'.intval($data['user_id']).'",
                    `requested` = NOW(),
                    `status`  = "requested",
                    `hash`    = "'.mysqli_real_escape_string($db->mysqli(), $data['hash']).'"';
                    
        if($db->query($sql))
        {
            return $inserted_id;
        }
        
        return false;
    }


    public static function cancelRequest($data = array())
    {
        $db = new Database();
        
        $sql = 'DELETE FROM `roster_request`
                WHERE
                    `team_id` = "'.intval($data['team_id']).'" AND
                    `user_id` = "'.intval($data['user_id']).'" AND
                    `status` = "requested"';
        $db->query($sql);

        $team = TeamDAO::get($data['team_id']);

        foreach($team->getAdmins() as $admin_id)
        {
            $sql = 'DELETE FROM `notifications` 
                    WHERE `user_id` = "'.intval($admin_id).'" AND `type_id` = "REQUEST-TEAM-ENTRY" AND `object_id` = "'.intval($data['team_id']).'"';

            $db->query($sql);
        }

        return true;
    }

    
    public static function getMax($user_id = 0)
    {
        $db = new Database();
        
        $sql = 'SELECT MAX(`id`) as max
                FROM `roster_invite`
                WHERE `user_id` = ' . intval($user_id);
                
        $db->query($sql);
        
        $row = $db->readRow('aarr');
        
        return (++$row['max']);
    }

    public static function getMaxRequest($user_id = 0)
    {
        $db = new Database();
        
        $sql = 'SELECT MAX(`id`) as max
                FROM `roster_request`
                WHERE `user_id` = ' . intval($user_id);
                
        $db->query($sql);
        
        $row = $db->readRow('aarr');
        
        return (++$row['max']);
    }
    
    public static function updateInvite($data = array(), $status = '')
    {
        $db = new Database();
        
        // v�dy updatujeme jenom posledn� 
        $sql = 'UPDATE `roster_invite`
                SET
                    `status` = "'.mysqli_real_escape_string($db->mysqli(), $status).'"
                WHERE `id` = "'.intval($data['id']).'" AND `user_id` = "'.intval($data['user_id']).'"
                ORDER BY `invited` DESC
                LIMIT 1';
                    
        return $db->query($sql);
    }

    public static function updateRequest($data = array(), $status = '')
    {
        $db = new Database();
        
        // v�dy updatujeme jenom posledn� 
        $sql = 'UPDATE `roster_request`
                SET
                    `status` = "'.mysqli_real_escape_string($db->mysqli(), $status).'"
                WHERE `id` = "'.intval($data['id']).'" AND `user_id` = "'.intval($data['user_id']).'"
                ORDER BY `requested` DESC
                LIMIT 1';
                    
        return $db->query($sql);
    }
    
    public static function deleteInvite($data = array())
    {
        $db = new Database();
        
        $sql = 'DELETE FROM `roster_invite`
                WHERE `id` = "'.intval($data['id']).'" AND `user_id` = "'.intval($data['user_id']).'"';
                    
        return $db->query($sql);
    }

    public static function deleteRequest($data = array())
    {
        $db = new Database();
        
        $sql = 'DELETE FROM `roster_request`
                WHERE `id` = "'.intval($data['id']).'" AND `user_id` = "'.intval($data['user_id']).'"';
                    
        return $db->query($sql);
    }
    
    public static function getInviteByTeam($data = array())
    {
        $db = new Database();
        
        $sql = 'SELECT *
                FROM `roster_invite`
                WHERE `team_id` = "'.intval($data['team_id']).'" AND `user_id` = "'.intval($data['user_id']).'"
                '.(empty($data['user_id']) ? ' AND `email` = "'.mysqli_real_escape_string($db->mysqli(), $data['email']).'"' : '').'
                ORDER BY `id` DESC 
                LIMIT 1';
                    
        return self::load($sql);
    }

    public static function getRequestByTeam($data = array())
    {
        $db = new Database();
        
        $sql = 'SELECT *
                FROM `roster_request`
                WHERE `team_id` = "'.intval($data['team_id']).'" AND `user_id` = "'.intval($data['user_id']).'"
                ORDER BY `id` DESC 
                LIMIT 1';
                    
        return self::load($sql);
    }

    
    public static function getUserInvites($user_id)
    {
        $db = new Database();
        
        $sql = 'SELECT *
                FROM `roster_invite`
                WHERE `user_id` = "'.intval($user_id).'" AND status = "invited"
                ORDER BY `id` DESC ';
                
        return self::loadArray($sql);
    }
    
    public static function deleteInviteByTeam($data = array())
    {
        $db = new Database();
        
        $sql = 'DELETE
                FROM `roster_invite`
                WHERE `team_id` = "'.intval($data['team_id']).'" AND `user_id` = "'.intval($data['user_id']).'"';

        return self::nonEmpty($sql);
    }

    public static function getInvite($data = array())
    {
        $db = new Database();
        
        $sql = 'SELECT *
                FROM `roster_invite`
                WHERE `id` = "'.intval($data['id']).'" AND `user_id` = "'.intval($data['user_id']).'"
                ORDER BY `id` DESC 
                LIMIT 1';
                    
        return self::load($sql);
    }

    public static function getRequest($data = array())
    {
        $db = new Database();
        
        $sql = 'SELECT *
                FROM `roster_request`
                WHERE `id` = "'.intval($data['id']).'" AND `user_id` = "'.intval($data['user_id']).'"
                ORDER BY `id` DESC 
                LIMIT 1';
                    
        return self::load($sql);
    }
    
    public static function updateInvitesByEmail($email, $user_id)
    {
        $db = new Database();
        
        $sql = 'UPDATE `roster_invite`
                SET `user_id` = '.intval($user_id).'
                WHERE `email` = "'.mysqli_real_escape_string($db->mysqli(), $email).'" AND `status` = "invited"';

        return self::nonEmpty($sql);
    }

    public static function sendInvitation($email = '', Team $team, User $logged_user, $user_id = 0)
    {
        if($user_id > 0)
        {
            SendMail::invitation($email, $team, $logged_user);
        }
        else
        {
            SendMail::newUserInvitation($email, $team, $logged_user);
        }
        
        return true;
    }
}
?>