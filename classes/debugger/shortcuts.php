<?php

/**
 * This file is part of the Nette Framework (http://nette.org)
 *
 * Copyright (c) 2004 David Grudl (http://davidgrudl.com)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */



/**
 * Debugger::dump shortcut.
 */
function dump($var)
{
	foreach (func_get_args() as $arg) {
		Debugger::dump($arg);
	}
	return $var;
}

function debug($strict = false) 
{
	Debugger::$strictMode	= true;
	Debugger::$maxDepth		= 10;
	Debugger::$maxLen		= 500;
	Debugger::$showBar		= false;
	
	if(defined('TESTIP') && TESTIP)
	{
		Debugger::enable(Debugger::DEVELOPMENT);
	}
    elseif(defined('LOCAL') && LOCAL)
    {
		Debugger::enable(Debugger::DEVELOPMENT);
	}
	elseif(defined('LOG_PATH') && defined('LOG_EMAIL'))
	{
		Debugger::enable(Debugger::PRODUCTION, LOG_PATH, LOG_EMAIL);	
	}
	else
	{
		Debugger::enable(Debugger::PRODUCTION);
	}
	
	if($strict !== true)
	{
		error_reporting(E_ERROR | E_PARSE);
	}
	else
	{
		error_reporting(E_ALL);
	}
}
