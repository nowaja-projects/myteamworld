<?php
class CitiesDAO extends ObjectDAO
{
	public static function get($letters = '')
	{
	    $db = new Database();
       	
        $sql = 'SELECT * 
                FROM `cities` 
                WHERE `name` LIKE "'.mysqli_real_escape_string($db->mysqli(), $letters) . '%"
                ORDER BY `name` ASC';

		return self::loadArray($sql);
	}
	
    
    public static function getById($id = '')
	{
	    $db = new Database();
       	
        $sql = 'SELECT * 
                FROM `cities` 
                WHERE `id` = "'.mysqli_real_escape_string($db->mysqli(), $id) . '"
                LIMIT 1';

		$city = self::load($sql);
        
        if($city)
        {
            return $city['name'] . ', ' . $city['region'] . ', ' . $city['country']; 
        }
        
        return false;
        
	}
}
?>