<?php

class TranslateDAO extends ObjectDAO
{
	const KEY_PREFIX = 'apc_translate_';
	public static function getAll($group = '', $language = '')
	{
        // Pripojeni k databazi
		$db = new Database();
		
		// Vychozi podminky
        $where = 1;
        
        // Pokud omezeni na skupinu
		if(!empty($group))
        {
        	$where .= ' AND `group` = "'.mysqli_real_escape_string($db->mysqli(), $group).'"';
        }
        
        if(!empty($lang ))
        {
        	$where = ' AND `lang` = "'.mysqli_real_escape_string($db->mysqli(), $language).'"';
        }

        
    	$key = self::KEY_PREFIX . $group . '_' . $language;
    	$return = Cache::getFromCache($key);
    	if($return !== false)
    	{
    		return $return;
    	}


		$sql = 'SELECT *
                FROM `translate`
				WHERE '.$where.'
				ORDER BY `group`';
        
		// Vraceni vylsedku       
		$a = self::loadArray($sql);
        
		$return = array();

        foreach($a as $phrase)
        {
            $return[$phrase['key']] = $phrase['text'];
        }

        Cache::store($key, serialize($return));
        
        return $return;
	}
	
	/**
	 * Vr�t� p�eklady
	 * 
	 * @param string $key
	 * @return boolean
	 */
	public static function get($key = '', $lang = '')
	{
		$key_load = self::KEY_PREFIX . $key . '_' . $lang;
		$return = Cache::getFromCache($key_load);
    	if($return !== false)
    	{
    		return $return;
    	}

		// Pripojeni k databazi
		$db = new Database();
		
		// Dotaz na pritomnost v databazi
		$sql = 'SELECT *
				FROM `translate`
				WHERE `lang` = "'.mysqli_real_escape_string($db->mysqli(), $lang).'" AND `key` = "'.mysqli_real_escape_string($db->mysqli(), $key).'"
				LIMIT 1';
		
		// Vratit true pokud neni zaznam	
		$tr = self::load($sql, 'Translate');

		if(count($tr) > 0)
		{
	        Cache::store($key_load, serialize($tr));
	    }

		return $tr;
	}
	
	public static function update($data)
	{
        // Pripojeni k databazi
		$db = new Database();
        
		// Pokud je co ukladat
		if(count($data))
		{
			$sql = array();
			foreach($data as $key => $text)
			{
				$sql[] = '`'.$key.'` = "'.mysqli_real_escape_string($db->mysqli(), $text).'"';
			}

			if(count($sql))
			{
				// Vytvoreni dotazu
				$sql = 'UPDATE `translate`
						SET '.implode(',',$sql).'
						WHERE `key` = "'.mysqli_real_escape_string($db->mysqli(), $data['key']).'"
						LIMIT 1';

				Cache::delete(self::KEY_PREFIX . $data['key'] . '_' . $data['lang']);
				
				// Vraceni vysledku		
				return self::nonEmpty($sql);
			}

			return true;
		}
		
		// Vraceni chyby
		return false;
	}
	
	public static function insert($data = array())
	{
        // Pripojeni k databazi
		$db = new Database();
        
		// Pokud je co ukladat
		if(!empty($data))
		{
			// Vytvoreni dotazu pro vlozeni
			$sql = 'INSERT INTO `translate`(`key`, `lang`, `text`, `group`, `translated`) 
                    VALUES (
                        "'.mysqli_real_escape_string($db->mysqli(), $data['key']).'",
                        "'.mysqli_real_escape_string($db->mysqli(), $data['lang']).'",
                        "'.mysqli_real_escape_string($db->mysqli(), $data['text']).'",
                        "'.mysqli_real_escape_string($db->mysqli(), $data['group']).'",
                        "'.mysqli_real_escape_string($db->mysqli(), $data['translated']).'"
                    )';

			// Vraceni vysledku
			return self::nonEmpty($sql);
			//return true;
		}
		
		// Vraceni chyby
		return false;
	}
	
	/**
	 * Smaze zaznam z tabulky.
	 * 
	 * @param $key
	 * @return boolean
	 */
	public static function delete($key)
	{
		// Pripojeni k databazi
		$db = new Database();
		
		// Dotaz na pritomnost v databazi
		$sql = 'DELETE FROM `translation`
				WHERE `translation_key` = "'.mysqli_real_escape_string($db->mysqli(), $key).'"
				LIMIT 1';

		// Vratit true pokud neni zaznam	
		return self::isEmpty($sql);
	}
	
	public static function getGroups()
	{
		$db = new Database();
		
		$sql = 'SELECT DISTINCT
				    (`translation_group`) AS `group`
				FROM `translation`
				ORDER BY `translation_group`';
				
		$db->query($sql);
		while($res = $db->readrow('aarr'))
		{
			$return[$res['group']] = $res['group'];
		}
		
		return $return;
	}
	
}