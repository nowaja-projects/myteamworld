<?php

class Cache
{
	// porefix pro cache, aby se nam dohromady nachachovaly souboy z bety a z ostre
	const PREFIX = CACHE_PREFIX;

	// zda ma zapisovat d souboru log veskere cache komunikace
	const DEBUG = false;

	// jak dlouho maji byt soubory maximalne v cache
	const TTL 	= 600;

	private static function log($text, $state, $key)
	{
		$key = self::PREFIX . $key;

		if(self::DEBUG)
		{
			$fp = fopen('/var/www/myteamworld.com/beta/log/cache.log.csv', 'a');

			$backtrace = debug_backtrace();
			$files = '';
			foreach($backtrace as $trace)
			{
				$files .= ';' . $trace['file'] . ';' . $trace['line'];
			}

			fwrite($fp, $text . ';' . $state . ';' . $key . $files . "\n");

			fclose($fp);
		}
	}

	public static function getFromCache($key)
	{
		$key = self::PREFIX . $key;

		if(CACHE_ENABLED)
		{
			if(apc_exists($key))
	        {
	        	self::log('getFromCache', 'true', $key);

	            return unserialize(apc_fetch($key));
	        }

	        self::log('getFromCache', 'false', $key);
	    }

        return false;
	}

	public static function store($key, $data)
	{
		$key = self::PREFIX . $key;

		if(CACHE_ENABLED)
		{
			if(is_array($data) || is_object($data))
			{
				$data = serialize($data);
			}

			self::log('store', 'true', $key);
			apc_store($key, $data, self::TTL);
		}

		return true;
	}

	public static function delete($key)
	{
		$key = self::PREFIX . $key;
		
		if(CACHE_ENABLED)
		{
			self::log('delete', 'true', $key);
			apc_delete($key);
		}

		return true;
	}
}