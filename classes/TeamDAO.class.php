<?php
class TeamDAO extends ObjectDAO
{
    const KEY_PREFIX = 'apc_team_';

	public static function get($id)
	{
        if(empty($id))
        {
            return new Team();
        }
        $key_load = self::KEY_PREFIX . $id;
        $return = Cache::getFromCache($key_load);
            
        if($return !== false && $return instanceof Team && !empty($return->id))
        {
            return $return;
        }

		$sql = 'SELECT `team`.*, SUM(`player`) as players, SUM(`fan`) AS fans, SUM(`observer`) as observers
                FROM `team` 
                LEFT JOIN `user_in_team` ON `user_in_team`.`team_id` = `team`.`team_id`
                WHERE `team`.`team_id` = '.intval($id);
		
        $team = self::load($sql, 'Team');

        Cache::store($key_load, serialize($team));

        return $team;
	}
    
    public static function getByKey($id, $key)
	{

        $db = new Database();
		
        $key_load = self::KEY_PREFIX . $id;
        $return = Cache::getFromCache($key_load);
        if($return !== false && $return instanceof Team && !empty($return->id) && $return->key == $key)
        {
            return $return;
        }

        $sql = 'SELECT `team`.*, `user_in_team`.`active_team`, SUM(`player`) as players, SUM(`fan`) AS fans, SUM(`observer`) as observers
                FROM `team` 
                LEFT JOIN `user_in_team` ON `user_in_team`.`team_id` = `team`.`team_id`
                WHERE `team`.`team_key` = "'.mysqli_real_escape_string($db->mysqli(), $key).'" AND `team`.`team_id` = '.intval($id);        

		$team = self::load($sql, 'Team');

        Cache::store($key_load, serialize($team));

        return $team;
	}
	
	public static function insert($data = array())
	{
		$db = new Database();
		
		if($db->insertArray($data,'team'))
		{
			return $db->getInsertedId();
		}
		else
		{
			return false;
		}
	}
	
	public static function update($data = array())
	{
		$db = new Database();
       
        Cache::delete(self::KEY_PREFIX . $data['id']);

		return $db->updateArray($data, 'team', 'team_id');
	} 
    
    public static function updateVersion($id, $version)
    {
        $sql = 'UPDATE `team` 
                SET `team_version` = "'.$version.'"
                WHERE `team_id` = ' . $id;

        Cache::delete(self::KEY_PREFIX . $id);

        return self::nonEmpty($sql);
    }
    
    public static function addUser($user_id, $team_id, $team_admin, $player, $active, $is_host = 0)
    {
        $db = new Database();
        
        $sql = 'INSERT INTO `user_in_team`(`user_id`, `team_id`, `team_admin`, `player`, `active_team`, `host`, `show`) 
                VALUES ("'.intval($user_id).'","'.intval($team_id).'","'.intval($team_admin).'","'.intval($player).'", "'.intval($active).'", "'.intval($is_host).'", "1")';
        
        Cache::delete(self::KEY_PREFIX . $team_id);

        return self::nonEmpty($sql);
    }
    
    public static function deleteUser($user_id, $team_id)
    {
        $db = new Database();
        
        $sql = 'DELETE FROM `user_in_team` WHERE `user_id`= "'.intval($user_id).'" AND `team_id` = "'.intval($team_id).'"';
                
        Cache::delete(self::KEY_PREFIX . $team_id);

        return self::nonEmpty($sql);
    }

    public static function deleteAdmin($user_id, $team_id)
    {
        $db = new Database();

        $sql = 'UPDATE `user_in_team` SET `team_admin` = 0 WHERE `user_id` = ' . intval($user_id) . ' AND `team_id` = ' . intval($team_id);

        Cache::delete(self::KEY_PREFIX . $team_id);

        return $db->query($sql);
    }
    
    public static function addAdmin($user_id, $team_id)
    {
        $db = new Database();

        $sql = 'UPDATE `user_in_team` SET `team_admin` = 1 WHERE `user_id` = ' . intval($user_id) . ' AND `team_id` = ' . intval($team_id);

        Cache::delete(self::KEY_PREFIX . $team_id);

        return $db->query($sql);
    }

    public static function getAdmins($team_id)
    {
        $db = new Database();
        
        $sql = 'SELECT `user_id` FROM `user_in_team` WHERE `team_id` = ' . intval($team_id) . ' AND `team_admin` = 1';
        
        $db->query($sql);
        
        $return = array();
        
        while($a = $db->readrow('obj'))
        {
            $return[$a->user_id] = $a->user_id;
        }
        
        return $return;
    } 
    
    public static function addFan($team_id = 0, $user_id = 0)
    {
        $db = new Database();
        
        if($team_id > 0 && $user_id > 0)
        {
            $sql = 'INSERT INTO `user_in_team`(`user_id`, `team_id`, `fan`) 
                    VALUES ("'.$user_id.'","'.$team_id.'", "1")';
            
            Cache::delete(self::KEY_PREFIX . $team_id);

            return self::nonEmpty($sql);
        }
        
        return false;
    }
    
    public static function removeFan($team_id = 0, $user_id = 0)
    {
        $db = new Database();
        
        if($team_id > 0 && $user_id > 0)
        {
            $sql = 'DELETE FROM `user_in_team` 
                    WHERE `user_id` = "'.$user_id.'" AND `team_id` = "'.$team_id.'" AND `fan` = 1';
            
            Cache::delete(self::KEY_PREFIX . $team_id);

            return self::nonEmpty($sql);
        }
        
        return false;
    }
    
    public static function addObserver($team_id = 0, $user_id = 0)
    {
        $db = new Database();
        
        if($team_id > 0 && $user_id > 0)
        {
            $sql = 'INSERT INTO `user_in_team`(`user_id`, `team_id`, `observer`) 
                    VALUES ("'.$user_id.'","'.$team_id.'", "1")';
            
            Cache::delete(self::KEY_PREFIX . $team_id);

            return self::nonEmpty($sql);
        }
        
        return false;
    }
    
    public static function removeObserver($team_id = 0, $user_id = 0)
    {
        $db = new Database();
        
        if($team_id > 0 && $user_id > 0)
        {
            $sql = 'DELETE FROM `user_in_team` 
                    WHERE `user_id` = "'.$user_id.'" AND `team_id` = "'. $team_id.'" AND `observer` = 1';
            
            Cache::delete(self::KEY_PREFIX . $team_id);

            return self::nonEmpty($sql);
        }
        
        return false;
    }
    
    public static function getFanList($team_id = 0)
    {
        $db = new Database();
        
        $sql = 'SELECT `user_id` FROM `user_in_team` WHERE `team_id` = ' . intval($team_id) . ' AND `fan` = 1';
        
        $db->query($sql);
        
        $return = array();
        
        while($a = $db->readrow('obj'))
        {
            $return[$a->user_id] = $a->user_id;
        } 
        
        return $return;
    }
    
    public static function getObserverList($team_id = 0)
    {
        $db = new Database();
        
        $sql = 'SELECT `user_id` FROM `user_in_team` WHERE `team_id` = ' . intval($team_id) . ' AND `observer` = 1';
        
        $db->query($sql);
        
        $return = array();
        
        while($a = $db->readrow('obj'))
        {
            $return[$a->user_id] = $a->user_id;
        } 
        
        return $return;
    }

    public static function getInvitationList($team_id)
    {
        $db = new Database();

        $sql = 'SELECT * 
                FROM `roster_invite`
                WHERE `status` = "invited" AND `team_id` = ' . intval($team_id);

        $return = array();
        if($db->query($sql))
        {
            while($a = $db->readrow('aarr'))
            {
                $return[] = $a['user_id'];
            }
        }

        return $return;
    }
    
    public static function getPlayerList($team_id = 0, $return_objects = false)
    {
        $db = new Database();
        
        if($return_objects)
        {
            $sql = 'SELECT *, 1 AS `exists`
                    FROM `user_in_team`
                    LEFT JOIN `user` ON `user`.`user_id` = `user_in_team`.`user_id`
                    LEFT JOIN `roster_position` ON `roster_position`.`id` = `user_in_team`.`position_id` 
                    WHERE `team_id` = ' . intval($team_id) . ' AND `player` = 1
                    ORDER BY `roster_position`.`order` ASC, `user`.`user_sname` ASC';
        }
        else
        {
            $sql = 'SELECT `user_id` 
                    FROM `user_in_team`
                    WHERE `team_id` = ' . intval($team_id) . ' AND `player` = 1';
        }
        
        $return = array();
        
        if($db->query($sql))
        {
            while($a = $db->readrow('aarr'))
            {
                if($return_objects)
                {
                    unset($a['id']);
                    $return[$a['user_id']] = $a;
                }
                else
                {
                    $return[$a['user_id']] = $a['user_id'];
                }
            }
        } 
        
        return $return;
    }
    
    
    public static function getTeamMates($teams = array())
    {
        $db = new Database();
        
        $sql = 'SELECT DISTINCT `user_id` 
                FROM `user_in_team`
                WHERE `player` = 1 AND `team_id` IN ('.implode(',', $teams).')';
         
        $return = array();       
        if($db->query($sql))
        {
            while($a = $db->readrow('aarr'))
            {
                $return[$a['user_id']] = $a['user_id'];
            }
        }
        
        return $return;
    }


    public static function getLastMatch($team_id)
    {
        $sql = 'SELECT * 
                FROM `event`
                WHERE 
                    `type` IN ("match", "friendlymatch") 
                    AND (`team_id` = ' . intval($team_id) . ' OR `oponent_id` = ' . intval($team_id) . ')
                    AND `result` != ""
                    AND `end` <= "'.date('Y-m-d H:i:s').'"
                ORDER BY `end` DESC
                LIMIT 1';

        $db = new Database();
        if($db->query($sql))
        {
            return $db->readrow('aarr');
        }

        return false;
    }
}
?>