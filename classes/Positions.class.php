<?php
class Positions extends ObjectDAO
{
	public static function get($position_id, $sport_id = 0)
	{
		$sport_id = intval($sport_id);
		$sql = 'SELECT * 
				FROM `roster_position`
				WHERE `id` = ' . $position_id . (!empty($sport_id) ? ' AND `sport_id` = "'.intval($sport_id).'"' : '');

		return self::load($sql, 'Position');
	}
}
?>