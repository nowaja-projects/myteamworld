<?php
class StatsDAO extends ObjectDAO
{
    public static function deleteStatsForEvent($event_id, $table)
    {
        $db = new Database();
        
        $sql = 'DELETE FROM `'.$table.'` WHERE `event_id` = "'.mysqli_real_escape_string($db->mysqli(), $event_id).'"';

        return $db->query($sql);
    }

    public static function getStatsForEvent($event_id, $table)
    {
        $db = new Database();

        $sql = 'SELECT * 
                FROM `'.$table.'`
                WHERE `event_id` = "'.mysqli_real_escape_string($db->mysqli(), $event_id).'"
                ';
        
        $db->query($sql);

        $return = array();
        while($a = $db->readrow('aarr'))
        {
            if(isset($return[$a['team']][$a['user_id']]))
            {
                $not = array(
                    'id',
                    'user_id',
                    'team',
                    'team_id',
                    'event_id'
                );
                foreach($a as $k => $v)
                {
                    if(!in_array($k, $not))
                    {
                        $return[$a['team']][$a['user_id']][$k] += $v;
                    }
                }
            }
            else
            {
                $return[$a['team']][$a['user_id']] = $a;
            }
        }

        return $return;
    }

    public static function insertStatArray($data, $table)
    {
        $db = new Database();

        $last_keys = array();
        $vals = array();

        if(count($data))
        {
            // projdeme vsechna data
            foreach($data as $array)
            {
                // nacteme si nazvy sloupcu, do kterych budeme ukladat
                $keys = array_keys($array);

                // pokud mame zmenenou pozici
                if($keys != $last_keys)
                {
                    // pokud nejdeme prvni pruchod, tak musime uzavrit SQL dotaz a zacit vytvaret novy
                    if(!empty($last_keys))
                    {
                        $sql .= implode(', ', $vals);

                        $db->query($sql);

                        // a dalsi hodnoty musime ukladat taky odznovu
                        $vals = array();
                    }

                    $sql = 'INSERT INTO `'.$table.'` (`'.implode('`, `', $keys).'`) VALUES ';

                    // ulozime si posledni sadu klicu ktere ukladame
                    $last_keys = $keys;
                }

                // nacteme si hodnoty statistik
                $vals[] = '("'.implode('", "', $array).'")';
            }


            // pokud nam ve stacku zustaly nejake hodnoty
            if(!empty($vals))
            {
                $sql .= implode(', ', $vals);

                $db->query($sql);
            }
        }

        return true;
    }

    public static function getAllPositionsForUserAndTeam(Sport $sport, $user_id, $team_id)
    {
        $sql = 'SELECT DISTINCT `position_id` 
                FROM `'.$sport->stat_table.'`
                LEFT JOIN `roster_position` ON `roster_position`.`id` = `position_id`
                WHERE `roster_position`.`staff` != 1 AND `user_id` = ' . intval($user_id) . ' AND `team_id` = ' . intval($team_id) . '
                ORDER BY `position_id`';

        $db = new Database();

        $return = array();

        if($db->query($sql))
        {
            while($row = $db->readrow('aarr'))
            {
                $return[] = Positions::get($row['position_id']);
            }
        }

        return $return;
    }

    public static function getAllTeamStats(Team $team, Sport $sport, $season_id = 0, $user_id = 0, $position_id = 0)
    {
        $user_id = intval($user_id);
        $return = array();

        $db = new Database();

        if(!is_array($sport->select_stats))
        {
            $sport->select_stats = unserialize($sport->select_stats);
        }

        if(!is_array($sport->select_stats))
        {
            return $return;
        }

        // vytvorime si SQL dotaz
        $select = array();
        foreach($sport->select_stats as $col)
        {
            $select[] = str_replace('{table}', $sport->stat_table, $col['pattern']);
        } 
        

        $sql = 'SELECT `'.$sport->stat_table.'`.`user_id`, '.implode(', ', $select).' 
                FROM `'.$sport->stat_table.'`
                LEFT JOIN `event` ON CONCAT(`event`.`id`,"-",`event`.`team_id`) = `'.$sport->stat_table.'`.`event_id`
                WHERE `'.$sport->stat_table.'`.`team_id` = ' . $team->id . (!empty($user_id) ? ' AND `'.$sport->stat_table.'`.`user_id` = ' . $user_id : '') . (!empty($position_id) ? ' AND `position_id` = ' . intval($position_id) : '') . 
                (!empty($season_id) ? ' AND ((`team` = "home" AND `event`.`season_id` = "'.intval($season_id).'") OR (`team` = "away" AND `event`.`oponent_season_id` = "'.intval($season_id).'"))' : '').
                ' GROUP BY `'.$sport->stat_table.'`.`user_id`';

        if($db->query($sql))
        {
            while($row = $db->readrow('aarr'))
            {
                $return[$row['user_id']] = $row;
            }
        }
        
        return $return;
    }

    public static function getAllPlayerStats($user_id, $season_id = 0)
    {

    }


    public static function moveStatsToPlayer($from, $to, $sport)
    {

        if(!is_array($sport->select_stats))
        {
            $sport->select_stats = unserialize($sport->select_stats);
        }

        if(!is_array($sport->select_stats))
        {
            return true;
        }


        $db  = new Database();
        $db2 = new Database();

        $sql = 'SELECT * 
                FROM `'.$sport->stat_table.'`
                WHERE `user_id` = ' . $from;

        $events = array();
        $team_id = 0;

        if($db->query($sql))
        {
            while($row = $db->readrow('aarr'))
            {
                $team_id = $row['team_id'];

                // abychom vedeli o vsech udalostech, kde dany uzivatel vystupuje
                $events[] = $row['event_id'];

                $sql = 'SELECT * 
                        FROM `'.$sport->stat_table.'`
                        WHERE `user_id` = ' . $to . ' AND `event_id` = "' . $row['event_id'] . '"';

                if($db2->query($sql))
                {
                    $row2 = $db2->readrow('aarr');

                    if(!empty($row2['id']))
                    {
                        /// projdeme vsechny statistiky a vytvorime si SQL na update
                        $tmp = array();
                        foreach($sport->select_stats as $key => $stat)
                        {
                            if(!empty($stat['auto']))
                            {
                                continue;
                            }
                            $tmp[] = "`$key` = " . ($row[$key] + $row2[$key]);
                        }

                        // updatneme statistiky
                        $sql = 'UPDATE `'.$sport->stat_table.'` 
                                SET '.implode(', ', $tmp).'
                                WHERE `id` = ' . $row2['id'] . ' AND `user_id` = ' . $to;
                    }
                    else
                    {
                        // updatneme statistiky
                        $sql = 'UPDATE `'.$sport->stat_table.'` 
                                SET `user_id` = ' . $to . '
                                WHERE `id` = '.$row['id'].' AND `user_id` = ' . $from;
                    }
                    $db2->query($sql);
                }
                else
                {
                    return false;
                }
            }
        }

        // jeste musime projit vsechny udalosti a upravit jim soupisky
        if(empty($team_id) || empty($events))
        {
            return true;
        }

        $sql = 'SELECT * 
                FROM `event`
                WHERE CONCAT(`id`, "-", `team_id`) IN ("' . implode('","', $events) . '")';
        
        if($db->query($sql))
        {
            while($event = $db->readrow('aarr'))
            {
                // musime upravit jeho 
                $rosters = unserialize($event['rosters']);

                // projdeme domaci i hosty
                foreach($rosters as $key => $roster)
                {
                    // projdeme celou soupisku domacich i hostu
                    foreach($roster as $key1 => $row)
                    {
                        $player = explode('-', $row['player']);

                        // pokud se jedna
                        if($player[0] == $from)
                        {
                            $roster[$key1]['player'] = $to . '-' . getRecipientHash($to);
                        }
                    }

                    $rosters[$key] = $roster;
                }

                $sql = 'UPDATE `event` 
                        SET `rosters` = "' . mysqli_real_escape_string($db2->mysqli(), serialize($rosters)) . '"
                        WHERE `id` = ' . $event['id'] . ' AND `team_id` = ' . $event['team_id'] . '
                        LIMIT 1';

                $db2->query($sql);
            }
        }
        else
        {
            return false;
        }

        return true;
    }
}
