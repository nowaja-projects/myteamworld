<?php
class TeamPost extends Object
{
  	protected $columns = array(
        'parent_id'  => '',
        'user_id'    => '',
        'team_id'    => '',
        'type'       => '',
        'created'    => '',
        'date_begin' => '',
        'text'       => '',
        'value'      => ''
    );
    
    public function __construct($data = array())
  	{
        $this->setTable('object');
		
		if(is_array($data) && count($data))
		{
		  	$this->parseAndSet($data);
        }
  	}
}
?>