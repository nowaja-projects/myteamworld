<?php

class PrivateMessage extends Object 
{
    protected $columns = array(
        'id'                => '',
        'user_id'           => '',
        'sent_user_id'      => '',
        'users'             => '',
        'conversation_id'   => '',
        'text'              => '',
        'time'              => '',
        'read'              => ''
    );
    
    protected $table = 'pm';
    
    public function __construct($data = array())
    {
        $this->setTable($this->table);
        
        if(is_array($data))
        {
            $this->parseAndSet($data);
        }
    } 
    
    public function save()
    {
        if(isset($this->id) && $this->id > 0)
        {
            PrivateMessages::insertMessage($this->getAllData());
        }
    }
}

?>