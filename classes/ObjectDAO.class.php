<?php
class objectdao
{
	/**
	 * provede dotaz a vrati prislusny objekt, pokud neexistuje pak vraci pouze pole dat
	 * @param string $sql
	 * @param string $object
	 * @param object $db database
	 * @return object
	 */
	public static function load($sql,$object = false,&$db=null)
	{
		if(!is_a($db,'Database'))
		{
			$db = new Database();
		}

		if($db->query($sql))
        {
            $res = $db->readrow('aarr');
        }
        else
        {
            $res = null;
        }
		
		if(is_array($res) && count($res))
		{
			return self::fetchData($res,$object);
		}
		else
		{
			return array();
		}
	}
	
	/**
	 * provede dotaz a vrati pole objektu, pokud objekt neexistuje vrati pole dat
	 * @param string $sql
	 * @param string $object
	 * @param object $db database
	 * @param int $page
	 * @return object
	 */
	public static function loadArray($sql,$object = false,$db = null,$page = 0,$id_as_key = true, $id_key = null)
	{
		$return = array();
		
		if(!is_a($db,'Database'))
		{
			$db = new Database();
		}
		if($db->query($sql,$page) !== false)
		{
			while($res = $db->readrow('aarr'))
			{
				$new_object = self::fetchData($res,$object);
				if($id_as_key && is_a($new_object,$object) && method_exists($new_object,'get') && $new_object->validate())
				{
					$return[$new_object->get('id')] = $new_object;	
				}
				elseif(!empty($id_key))
				{
					$return[$new_object[$id_key]] = $new_object;
				}
                else
                {
                    $return[] = $new_object;
                }
			}
			
			return $return;
		}
		else
		{
			return array();
		}
	}
	
	public static function parse($data,$table)
	{
		foreach($data as $key => $value)
    	{
    		$return[str_replace($table.'_','',$key)] = $value;
    	}
    	
    	return $return;
	}
	
	public static function fetchData($data,$object)
	{
		if(class_exists($object,true) || in_array(strtolower($object),get_declared_classes()))
		{
			return new $object($data);	
		}
		else
		{
			return $data;
		}
	}
	
	public static function controlData($data)
    {
    	$data = is_object($data) ? (array)$data : $data;
    	
    	foreach ($data as $key=>$item)
    	{
    		if(is_string($item))
    		{
    			$return[$key] = mysql_real_escape_string($item);
    		}
    		else
    		{
    			$return[$key] = $item;
    		}
    	}
    	
    	return $return;
    }
	
	public static function nonEmpty($sql,$db = null)
	{
		if(!is_a($db,'Database'))
		{
			$db = new Database();
		}
		
		return $db->query($sql);
	} 	
}
?>