<?php
class Season extends Object
{
	protected $columns = array(
		'id'        => 'required',
        'team_id'   => 'required',
        'name'      => 'required',
        'active'    => ''
	);
	
	public function __construct($data = array())
	{
		$this->setTable('season');
        
		if(is_array($data) && count($data))
		{
			$this->parseAndSet($data);
		}
	}
}
?>