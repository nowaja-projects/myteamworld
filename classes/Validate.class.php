<?php
class Validate
{
	public static function valid($type,$value)
	{
		switch($type)
		{
			case 'email' :
				return self::email($value);
			break;
			
			case 'string' :
				return self::string($value);
			break;
			
			case 'required' :
				return self::required($value);
			break;
			
			case 'integer' :
				return self::integer($value);
			break;
            
            case 'date' :
				return self::date($value);
			break;
		}
	}
	
	private static function email($value)
	{
		return (bool)filter_var($value, FILTER_VALIDATE_EMAIL);
	}
	
	private static function string($value)
	{
		if($value && is_string($value))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private static function required($value)
	{
		return !empty($value);
	}
	
	private static function integer($value)
	{
		return (bool)is_numeric($value);
	}
    
    private static function date($value)
    {
        $vals = explode('-', $value);
        return checkdate($vals[1], $vals[2], $vals[0]);
    }
}
?>