<?php
class RosterEditObject extends Object 
{
    protected $columns = array(
        'id'         => '',
        'parent_id'  => '',
        'user_id'    => '',
        'team_id'    => '',
        'type'       => '',
        'created'    => '',
        'date_begin' => '',
        'date_end'   => '',
        'value'      => ''
    );

    public function __construct($data = array())
  	{
        $this->setTable('object');
        
		if(is_array($data) && count($data))
		{
		  	$this->parseAndSet($data);
        }
  	}
}
?>