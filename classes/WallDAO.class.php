<?php
class WallDAO extends ObjectDAO
{
    const KEY_PREFIX = 'apc_wall_';

	/**
	 * Vsechny prispevky na zed budou serializovane objekty prislusnych trid
	 * Zde budu resit pouze nacteni seznamu techto objektu
	 * Tyto objekty musi mit spolecne vlastnosti:
	 * Zakladatel,tym,cas vytvoreni, u udalosti doba platnosti jinak nevyplneno, typ (comment, udalost), parent_id defaultne 0
	 * Kazdy typ bude mit svou tridu pripadne dao tridu pokud bude treba nebo zahrnu zde
	 * Mozna nebude treba delat parent id pokud bude existovat trida reply jako reakce na prispevky
	 * Je otazka kolik to zabere mista, ale urcite se to vyplati
	 */
	 
	/**
	 * Insert serialized object 
	 * @param obj $object
	 * @param int $team_id
	 */ 
	public static function insertItem($object, $team_id = 0, $type = 'news')
	{
		$db = new Database();
	 	
	 	$object_id = self::getLastId($team_id, $type);
        
	 	$sql = sprintf('INSERT INTO object 
		 				(`object_id`,
                         `object_parent_id`,
                         `object_source_id`,
                         `object_user_id`,
                         `object_team_id`,
						 `object_type`,
						 `object_created`,
						 `object_date_begin`,
						 `object_date_end`,
						 `object_value`) 
		 				VALUES (%1$s,"%2$s",%3$s,%4$s,%5$s,"%6$s",%7$s,"%8$s","%9$s","%10$s")',
		 				$object_id,
                        ($object->parent_id ? mysqli_real_escape_string($db->mysqli(), $object->parent_id) : 0),
                        ($object->source_id ? intval($object->source_id) : 0),
                        ($object->user_id ? intval($object->user_id) : 0),
                        ($team_id ? mysqli_real_escape_string($db->mysqli(), $team_id) : 0),
		 				$object->type,
		 				$object->created ? '"'.mysqli_real_escape_string($db->mysqli(), $object->created) .'"': 'NOW()',
		 				empty($object->date_begin) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $object->date_begin),
		 				empty($object->date_end) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $object->date_end),
		 				mysqli_real_escape_string($db->mysqli(), serialize($object))
		);
        
        if($db->query($sql))
        {
            return $db->getInsertedId();
        }

        return false;
	}
    
    /**
	 * update serialized object pouze z import_news
	 * @param obj $object
	 * @param int $team_id
	 */ 
	public static function updateItem($object, $team_id = 0)
	{
		$db = new Database();
	 	
	 	$sql = sprintf('UPDATE `object` SET
                         `object_parent_id` = "%2$s",
                         `object_source_id` = %3$s,
                         `object_user_id` = %4$s,
                         `object_team_id` = %5$s,
    					 `object_created` = %7$s,
    					 `object_date_begin` = "%8$s",
    					 `object_date_end` = "%9$s",
    					 `object_value` = "%10$s"
                         WHERE `object_id` = "%1$s" AND `object_team_id` = "%5$s" AND `object_type` = "%6$s"',
		 				$object->id,
                        ($object->parent_id ? mysqli_real_escape_string($db->mysqli(), $object->parent_id) : 0),
                        ($object->source_id ? intval($object->source_id) : 0),
                        ($object->user_id ? intval($object->user_id) :0),
                        ($team_id ? mysqli_real_escape_string($db->mysqli(), $team_id) : 0),
		 				$object->type,
		 				$object->created ? '"'.mysqli_real_escape_string($db->mysqli(), $object->created) .'"': 'NOW()',
		 				empty($object->date_begin) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $object->date_begin),
		 				empty($object->date_end) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $object->date_end),
		 				mysqli_real_escape_string($db->mysqli(), serialize($object))
		);
        
        return $db->query($sql);
	}
	 
	/**
	 * Return last object id for given team
	 * @param int $team_id
	 * @return int
	 */
	public static function getLastId($team_id, $type)
	{
		$db = new Database();
	 	
        $sql = 'SELECT MAX(object_id) as `id` 
                FROM object 
                WHERE `object_team_id` = '.intval($team_id) . ' AND `object_type` = "'.mysqli_real_escape_string($db->mysqli(), $type).'"';
	 	
        if($db->query($sql))
	 	{
	 		$result = $db->readrow('aarr');
	 		return empty($result['id']) ? 1 : ($result['id'] + 1);
	 	}
	 	else
	 	{
	 		return 0;
	 	}
	}
	/**
	 * Delete wall object
	 * @param int $team_id
	 * @param int $item_id
	 * @return bool
	 */ 
	public static function deleteItem($team_id, $item_id, $type)
	{
	 	$db = new Database();
	 	$object_id = $team_id . '_' . $item_id . '_' . $type;

        @WallDAO::deleteLikes($object_id);

        // FIXME
        //@Notifications::deleteNotifications(0, $object_id);

        $sql = 'DELETE FROM `object`
                WHERE `object_parent_id` = "'.mysqli_real_escape_string($db->mysqli(), $object_id).'"';

        $db->query($sql);

        $sql = 'DELETE FROM `notifications`
                WHERE `object_id` = "'.mysqli_real_escape_string($db->mysqli(), $object_id).'"';

        $db->query($sql);
        
	 	$sql = 'DELETE FROM `object` 
		 		WHERE `object_team_id` = '.intval($team_id).' 
				AND `object_id` = '.intval($item_id) . 
                ' AND `object_type` = "'.mysqli_real_escape_string($db->mysqli(), $type) . '"
                LIMIT 1';
				
		return $db->query($sql);
	}
	
	/**
	 * Return wall items (array of objects) for given item or team
	 * @param int $team_id
	 * @return array()
	 */ 
	public static function getWallList($teams = array(), $types = false, $parent_id = 0, $sources = array(), $start = 0, $limit = WALL_LIMIT, $filter = array(), $debug = false, $add_where = '', $max_date = '')
	{
        global $logged_user;

	 	$db = new Database();
        
        if(empty($add_where))
        {
            $date = date('Y-m-d H:i:s');
            $add_where = ' AND (`object_date_begin` <= "'.$date.'" || `object_date_begin` = "0000-00-00 00:00:00") AND (`object_date_end` >= "'.$date.'" || `object_date_end` = "0000-00-00 00:00:00")';
        }

        if(!empty($max_date))
        {
            $add_where .= ' AND (`object_date_begin` > "'.$max_date.'")';
        }
        
        // musíme ošetřit i filtr
        $fsport = '';
        if(isset($filter['sports']) && count($filter['sports']) > 0)
        {
            $fsport = ' AND (`rss_source_sport_id` IS NULL OR `rss_source_sport_id` IN (0,'.implode(',', $filter['sports']).'))';
        }

        $team_and_types_sql = '';
        // pro kazdy tym musime zkontrolovat, ktere tymy muze uzivatel videt
        if(is_array($teams) && count($teams) > 0)
        {
            // pokud máme typy, které musíme kontrolvoat
            if(is_array($types) && count($types) > 0)
            {
                $tmp_types = array();
                foreach ($teams as $team_id)
                {
                    $team = TeamDAO::get($team_id);

                    if(!$team instanceof Team || !empty($team->deleted))
                    {
                        continue;
                    }
                    
                    foreach($types as $type_key)
                    {
                        // pokud muze uzivatel videt tento modul, tak ho pridame do SQL
                        if($team->userCanViewModule($type_key, $logged_user))
                        {
                            $tmp_types[] = '(`object_team_id` = '.$team_id.' AND `object_type` = "'.mysqli_real_escape_string($db->mysqli(), $type_key).'")';
                        }
                    }
                }

                $tmp_types[] = '(`object_team_id` = 0)';

                $team_and_types_sql = ' AND ('.implode(' OR ', $tmp_types).')';
            }
            else
            {
                $team_and_types_sql = ' AND (`object_team_id` IN ('.implode(',',$team_id) . ') OR `object_team_id` = 0)';
            }
        }

	 	/*
        $team_id = $teams;

        $sql = 'SELECT *
                FROM `object`
                LEFT JOIN `rss_source` ON `rss_source_id` = `object_source_id`
                LEFT JOIN `sport` ON `sport_id` = `rss_source_sport_id`
                LEFT JOIN `team` ON `team_id` = `object_team_id` 
		 		WHERE `object_created` <= "'.date('Y-m-d H:i:s').'"'.
                  $add_where.
                  $fsport.
                  (is_array($team_id) && count($team_id) > 0 ? ' AND (`object_team_id` IN ('.implode(',',$team_id) . ') OR `object_team_id` = 0)' : '').
                  ($parent_id !== false ? ' AND `object_parent_id` = "'.mysqli_real_escape_string($db->mysqli(), $parent_id) .'"' : '').
                  (is_array($types) && count($types) > 0 ? ' AND `object_type` IN ("'.implode('","', $types).'")' : '').
                  (is_array($sources) && count($sources) > 0 ? ' AND `object_source_id` IN (0,'.implode(',', $sources).')' : ' AND `object_source_id` = "0"').  
		 		' ORDER BY `object_date_begin` DESC' . 
                ($limit > 0 ? ' LIMIT ' . $start . ',' . $limit : '');*/

        $sql = 'SELECT *
                FROM `object`
                LEFT JOIN `rss_source` ON `rss_source_id` = `object_source_id`
                LEFT JOIN `sport` ON `sport_id` = `rss_source_sport_id`
                LEFT JOIN `team` ON `team_id` = `object_team_id` 
                WHERE `object_created` <= "'.date('Y-m-d H:i:s').'"'.
                  $add_where.
                  $fsport.
                  $team_and_types_sql . 
                  ($parent_id !== false ? ' AND `object_parent_id` = "'.mysqli_real_escape_string($db->mysqli(), $parent_id) .'"' : '').
                  (is_array($types) && count($types) > 0 ? ' AND `object_type` IN ("'.implode('","', $types).'")' : '').
                  (is_array($sources) && count($sources) > 0 ? ' AND `object_source_id` IN (0,'.implode(',', $sources).')' : ' AND `object_source_id` = "0"').  
                ' ORDER BY `object_date_begin` DESC' . 
                ($limit > 0 ? ' LIMIT ' . $start . ',' . $limit : '');

 		if($db->query($sql))
 		{
            $rows = $db->getRows() * 1;
            $wall = array();
 			while($data = $db->readrow('aarr'))
 			{
				$data = self::parse($data, 'object');
				$wall_key = self::createItemId($data);
				$wall[$wall_key] = $data;
                $wall[$wall_key]['count'] = $rows;
                $wall[$wall_key]['like'] = WallDAO::getLikeList($wall_key);

                if(!empty($data['user_id']) && $data['type'] != 'news')
                {
                    $user = UserDAO::get($data['user_id']);
                    if($user instanceof User)
                    {
                        $user->getTeamList();
                        $wall[$wall_key]['user'] = $user;
                    }
                }
 			}
            /*if($limit > 0)
            {
                return array_slice($wall, $start, $limit, true);
            }*/
 			return $wall;
 		}
 		else
 		{
 			return array();
 		}
	}
    
    public static function getObject($wall_key, $type = array())
    {
        $return = Cache::getFromCache(self::KEY_PREFIX . $wall_key);
        if($return !== false && is_array($return))
        {
            return $return;
        }

        $params = explode('_', $wall_key);
        $team_id = @$params[0];
        $obj_id  = @$params[1];
        $type_id = @$params[2];
        if(intval($obj_id) > 0)
        {
            $db = new Database();
    	 	$sql = 'SELECT *
                    FROM `object`
                    LEFT JOIN `rss_source` ON `rss_source_id` = `object_source_id`
                    LEFT JOIN `sport` ON `sport_id` = `rss_source_sport_id`
                    LEFT JOIN `team` ON `team_id` = `object_team_id` 
    		 		WHERE 1 AND `object_team_id` = '.intval($team_id).
                      ($obj_id > 0 ? ' AND `object_id` = '.intval($obj_id) : '').
                      (!empty($type_id) ? ' AND `object_type` = "'.mysqli_real_escape_string($db->mysqli(), $type_id).'"' : '').
                      (is_array($type) && count($type) > 0 ? ' AND `object_type` IN("'.implode('","', $type).'")' : '').   
    		 		' ORDER BY `object_created` DESC LIMIT 1';
    		 		
            $db->query($sql);
     		if($db->getRows() > 0)
     		{
                $wall = array();
     			$data = $db->readrow('aarr');
    			
                $data = self::parse($data, 'object');
    			//$wall_key = self::createItemId($data);
    			$wall[$wall_key] = $data;
                $wall[$wall_key]['count'] = 1;
                $wall[$wall_key]['like'] = WallDAO::getLikeList($wall_key);
    
                if(!empty($data['user_id']) && $data['type'] != 'news')
                {
                    $user = UserDAO::get($data['user_id']);
                    $user->getTeamList();
                    $wall[$wall_key]['user'] = $user;
                }

                Cache::store(self::KEY_PREFIX . $wall_key, serialize($wall));

     			return $wall;
     		}
     		else
     		{
     			return array();
     		}
        }
        
        return array();
    }
	
	/**
	 * Return object of respektive type based on $data['type'] parameter
	 * @param array $data
	 * @return obj
	 */ 
	public static function prepareObject($data = array())
	{
        switch($data['type'])
        {
            case 'reply':
                return new Reply($data);
            break;
            
            default:
                return new Object($data);
            break;
        }
    }
	 
	private static function createItemId($data = array())
	{
		return intval($data['team_id']).'_'.$data['id'] . '_' . $data['type'];
	}
    
    public static function getLikeList($wall_key)
    {
        $db = new Database();

        $load_key = self::KEY_PREFIX . 'likes_' . $wall_key;
        $return = Cache::getFromCache($load_key);

        if($return !== false && is_array($return))
        {
            return $return;
        }
        
        $sql = 'SELECT *
                FROM `like`
                LEFT JOIN `user` ON `user`.`user_id` = `like`.`user_id`
                WHERE `object_id` = "'.$wall_key.'"';
        
        $return = self::loadArray($sql, false, null, 0, true, 'user_id');

        Cache::store($load_key, serialize($return));

        return $return;
    }

    public static function getRosterChangeUsers($wall_key)
    {
        $db = new Database();
        
        $obj = explode('_', $wall_key);

        if(count($obj) <> 3)
        {
            return array();
        }

        $sql = 'SELECT *
                FROM `object`
                WHERE `object_team_id` = "'.$obj[0].'" AND `object_type` = "'.$obj[2].'" AND `object_id` = "'.$obj[1].'"';
        
        return self::load($sql, 'RosterEditObject');
    }
    
    public static function likeUnlike($wall_key = '', $user_id = 0, $type = '')
    {
        if(!empty($wall_key) && !empty($user_id) && !empty($type))
        {
            $db = new Database();
            $sql = 'SELECT * 
                    FROM `like`
                    WHERE `object_id` = "'.$wall_key.'" AND `user_id` = "'.$user_id.'"';
            
            $db->query($sql);
            if($db->getRows() > 0)
            {
                WallDAO::deleteLike($wall_key, $user_id);
                $load_key = self::KEY_PREFIX . 'likes_' . $wall_key;
                Cache::delete($load_key);
                return 'unlike';
            }
            else
            {
                WallDAO::insertLike($wall_key, $user_id, $type);
                $load_key = self::KEY_PREFIX . 'likes_' . $wall_key;
                Cache::delete($load_key);
                return 'like';
            }
        }
        return false;
    }
    
    private static function insertLike($wall_key = '', $user_id = 0, $type = '')
    {
        if(!empty($wall_key) && !empty($user_id) && !empty($type))
        {
            $sql = 'INSERT INTO `like`
                    SET 
                        `object_id` = "'.$wall_key.'",
                        `user_id`   = "'.$user_id.'",
                        `type`      = "'.$type.'",
                        `time`      = NOW()';
            
            $load_key = self::KEY_PREFIX . 'likes_' . $wall_key;
            Cache::delete($load_key);

            return self::nonEmpty($sql);
        }
        
        return false;
    } 
    
    private static function deleteLike($wall_key = '', $user_id = 0)
    {
        if(!empty($wall_key) && !empty($user_id))
        {
            $sql = 'DELETE
                    FROM `like`
                    WHERE `user_id` = "'.$user_id.'" AND `object_id` = "'.$wall_key.'"';

            $load_key = self::KEY_PREFIX . 'likes_' . $wall_key;
            Cache::delete($load_key);
            return self::nonEmpty($sql);
        }
        return false;
    }

    private static function deleteLikes($wall_key = '')
    {
        if(!empty($wall_key))
        {
            $sql = 'DELETE
                    FROM `like`
                    WHERE `object_id` = "'.$wall_key.'"';

            $load_key = self::KEY_PREFIX . 'likes_' . $wall_key;
            Cache::delete($load_key);

            return self::nonEmpty($sql);
        }
        return false;
    }

    public static function getLastTypeObject($type = 'insert', $team_id = 0, $parent_id = 0, $hour_limit = ROSTER_GROUP_LIMIT)
    {
        switch($type)
        {
            case 'gallery-insert':
                $db_type = 'gallery-insert';
            break;

            case 'event-result':
                $db_type = 'event-result';
            break;

            case 'delete':
                $db_type = 'roster-delete';
            break;

            default:
            case 'insert':
                $db_type = 'roster-insert';
            break;
        }

        $db = new Database();
        $sql = 'SELECT * 
                FROM `object`
                WHERE 
                    `object_type` = "'.$db_type.'"
                    AND `object_team_id` = ' . intval($team_id) . 
                    ($hour_limit > 0 ? ' AND `object_date_begin` >= "'.date('Y-m-d H:i:s', strtotime('-'.$hour_limit.'hours')).'"' : '').
                    ($parent_id > 0 ? ' AND `object_parent_id` = "' . mysqli_real_escape_string($db->mysqli(), $parent_id) . '"' : '') . '
                ORDER BY `object_date_begin` DESC
                LIMIT 1';
                
        $db->query($sql);
        
        $array = $db->readrow('aarr');

        if(count($array))
        {
            return $array;
        }

        return false;
    }


    public static function insertTypeItem($data)
    {
        $db = new Database();
        
        $object_id = self::getLastId($data['object_team_id'], $data['object_type']);
        
        $sql = sprintf('INSERT INTO object 
                        (`object_id`,
                         `object_parent_id`,
                         `object_source_id`,
                         `object_user_id`,
                         `object_team_id`,
                         `object_type`,
                         `object_created`,
                         `object_date_begin`,
                         `object_date_end`,
                         `object_value`) 
                        VALUES (%1$s,"%2$s",%3$s,%4$s,%5$s,"%6$s",%7$s,"%8$s","%9$s","%10$s")',
                        $object_id,
                        ($data['object_parent_id'] ? mysqli_real_escape_string($db->mysqli(), $data['object_parent_id']) : 0),
                        ($data['object_source_id'] ? intval($data['object_source_id']) : 0),
                        ($data['object_user_id'] ? intval($data['object_user_id']) : 0),
                        ($data['object_team_id'] ? mysqli_real_escape_string($db->mysqli(), $data['object_team_id']) : 0),
                        $data['object_type'],
                        !empty($data['object_created']) ? '"'.mysqli_real_escape_string($db->mysqli(), $data['object_created']) .'"': 'NOW()',
                        empty($data['object_date_begin']) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $data['object_date_begin']),
                        empty($data['object_date_end']) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $data['object_date_end']),
                        mysqli_real_escape_string($db->mysqli(), $data['object_value'])
        );
        
        if($db->query($sql))
        {
            return $object_id;
        }

        return false;
    }

    public static function updateTypeItem($data)
    {
        $db = new Database();
        
        $sql = sprintf('UPDATE `object` SET
                         `object_parent_id` = "%2$s",
                         `object_source_id` = %3$s,
                         `object_user_id` = %4$s,
                         `object_team_id` = %5$s,
                         `object_created` = %7$s,
                         `object_date_begin` = "%8$s",
                         `object_date_end` = "%9$s",
                         `object_value` = "%10$s"
                         WHERE `object_id` = "%1$s" AND `object_team_id` = "%5$s" AND `object_type` = "%6$s"',
                        $data['object_id'],
                        ($data['object_parent_id'] ? mysqli_real_escape_string($db->mysqli(), $data['object_parent_id']) : 0),
                        ($data['object_source_id'] ? intval($data['object_source_id']) : 0),
                        ($data['object_user_id'] ? intval($data['object_user_id']) : 0),
                        ($data['object_team_id'] ? mysqli_real_escape_string($db->mysqli(), $data['object_team_id']) : 0),
                        $data['object_type'],
                        $data['object_created'] ? '"'.mysqli_real_escape_string($db->mysqli(), $data['object_created']) .'"': 'NOW()',
                        empty($data['object_date_begin']) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $data['object_date_begin']),
                        empty($data['object_date_end']) ? '0000-00-00 00:00:00' : mysqli_real_escape_string($db->mysqli(), $data['object_date_end']),
                        mysqli_real_escape_string($db->mysqli(), $data['object_value'])
        );
        
        return $db->query($sql);
    }
}
?>