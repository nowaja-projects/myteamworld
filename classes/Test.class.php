<?php
class Test extends Object
{
	protected $columns = array(
		'id' => 'integer',
		'test' => '',
		'pozaduji' => 'required',
		'email' => 'email'
	);
	
	public function __construct($data = array())
	{
		$this->setTable('user');
		if(is_array($data) && count($data))
		{
			$this->parseAndSetData($data);
		}
	}
}
?>
