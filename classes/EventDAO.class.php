<?php

class EventDAO extends ObjectDAO
{
    public static function insert($data = array())
    {
        $db = new Database();
        
        $sql = 'INSERT INTO `event`
                SET
                    `id`                    = "'.mysqli_real_escape_string($db->mysqli(), $data['id']).'", 
                    `user_id`               = "'.mysqli_real_escape_string($db->mysqli(), $data['user_id']).'",
                    `team_id`               = "'.mysqli_real_escape_string($db->mysqli(), $data['team_id']).'",
                    `season_id`             = "'.mysqli_real_escape_string($db->mysqli(), $data['season_id']).'",
                    `oponent_id`            = "'.mysqli_real_escape_string($db->mysqli(), $data['oponent_id']).'",
                    `type`                  = "'.mysqli_real_escape_string($db->mysqli(), $data['type']).'",
                    `created`               = "'.mysqli_real_escape_string($db->mysqli(), $data['created']).'",
                    `start`                 = "'.mysqli_real_escape_string($db->mysqli(), $data['start']).'",
                    `end`                   = "'.mysqli_real_escape_string($db->mysqli(), $data['end']).'",
                    `info`                  = "'.mysqli_real_escape_string($db->mysqli(), $data['info']).'",
                    `confirmed_home`             = "'.mysqli_real_escape_string($db->mysqli(), @$data['confirmed_home']).'",
                    `confirmed_away`             = "'.mysqli_real_escape_string($db->mysqli(), @$data['confirmed_away']).'",
                    `result_confirmed_home`             = "'.mysqli_real_escape_string($db->mysqli(), @$data['result_confirmed_home']).'",
                    `result_confirmed_away`             = "'.mysqli_real_escape_string($db->mysqli(), @$data['result_confirmed_away']).'"
                ';
        
        return $db->query($sql);
    }
    
    public static function update($data = array())
    {
        $db = new Database();
        
        $sql = 'UPDATE `event`
                SET
                    `end`                   = "'.mysqli_real_escape_string($db->mysqli(), $data['end']).'" ,
                    `info`                  = "'.mysqli_real_escape_string($db->mysqli(), $data['info']).'", ' .
                    (isset($data['season_id'])              ? '`season_id`              = "'.mysqli_real_escape_string($db->mysqli(), $data['season_id']).'", ' : '') .
                    (isset($data['oponent_season_id'])      ? '`oponent_season_id`      = "'.mysqli_real_escape_string($db->mysqli(), $data['oponent_season_id']).'", ' : '') . 
                    (isset($data['confirmed_home'])         ? '`confirmed_home`         = "'.mysqli_real_escape_string($db->mysqli(), $data['confirmed_home']).'", ' : '') .
                    (isset($data['confirmed_away'])         ? '`confirmed_away`         = "'.mysqli_real_escape_string($db->mysqli(), $data['confirmed_away']).'", ' : '') .
                    (isset($data['result_confirmed_home'])  ? '`result_confirmed_home`  = "'.mysqli_real_escape_string($db->mysqli(), $data['result_confirmed_home']).'", ' : '') .
                    (isset($data['result_confirmed_away'])  ? '`result_confirmed_away`  = "'.mysqli_real_escape_string($db->mysqli(), $data['result_confirmed_away']).'", ' : '') .
                    (isset($data['deleted_home'])           ? '`deleted_home`           = "'.mysqli_real_escape_string($db->mysqli(), $data['deleted_home']).'", ' : '') .
                    (isset($data['deleted_away'])           ? '`deleted_away`           = "'.mysqli_real_escape_string($db->mysqli(), $data['deleted_away']).'", ' : '') .
                    '`start`                 = "'.mysqli_real_escape_string($db->mysqli(), $data['start']).'"
                WHERE 
                    `team_id`               = "'.mysqli_real_escape_string($db->mysqli(), $data['team_id']).'" AND
                    `id`                    = "'.mysqli_real_escape_string($db->mysqli(), $data['id']).'"
                ';
        
        return $db->query($sql);
    }

    public static function updateEventStats($data)
    {
        $db = new Database();
        
        $sql = 'UPDATE `event`
                SET
                    `result`                = "'.mysqli_real_escape_string($db->mysqli(), $data['result']).'",
                    `rosters`               = "'.mysqli_real_escape_string($db->mysqli(), $data['rosters']).'",
                    `team_stats`            = "'.mysqli_real_escape_string($db->mysqli(), $data['team_stats']).'"
                WHERE 
                    `team_id`               = "'.mysqli_real_escape_string($db->mysqli(), $data['team_id']).'" AND
                    `id`                    = "'.mysqli_real_escape_string($db->mysqli(), $data['id']).'"
                ';

        return $db->query($sql);
    }
    
    public static function getDefaultTeamEvents($team_id, $types = array(), $filter = array(), $limit = EVENT_LIMIT, $only_active = true)
    {
        $db = new Database();
        
        // FUTURE
        $sql = 'SELECT * 
                FROM `event`
                WHERE (
                    (`team_id` = "'.$team_id.'"'.($only_active ? ' AND `deleted_home` = 0' : '').')
                    OR 
                    (`oponent_id` = "'.$team_id.'"'.($only_active ? ' AND `deleted_away` = 0' : '').')
                )' .
                (!empty($types) ? ' AND `type` IN ("'.implode('","', $types).'")' : '').
                (!empty($filter['year']) ? ' AND ((`team_id` = "'.$team_id.'" AND `season_id` = "'.intval($filter['year']).'") OR (`oponent_id` = "'.$team_id.'" AND `oponent_season_id` = "'.intval($filter['year']).'"))' : '').
                (!empty($filter['type']) ? ' AND `type` = "'.$filter['type'].'"' : '').
                ' AND `start` >= NOW()
                 ORDER BY `start` ASC
                 LIMIT ' . (intval($limit / 2) + 1);
                
        $future = array();
        if($db->query($sql))
        {
            while($a = $db->readrow('aarr'))
            {
                $future[$a['id'].'-'.$a['team_id']] = new Event($a);
            }
        }
        $future = array_reverse($future, true);
        
        
        // PAST
        $sql = 'SELECT * 
                FROM `event`
                WHERE (
                    (`team_id` = "'.$team_id.'"'.($only_active ? ' AND `deleted_home` = 0' : '').')
                    OR 
                    (`oponent_id` = "'.$team_id.'"'.($only_active ? ' AND `deleted_away` = 0' : '').')
                )' .
                (!empty($types) ? ' AND `type` IN ("'.implode('","', $types).'")' : '').
                (!empty($filter['year']) ? ' AND ((`team_id` = "'.$team_id.'" AND `season_id` = "'.intval($filter['year']).'") OR (`oponent_id` = "'.$team_id.'" AND `oponent_season_id` = "'.intval($filter['year']).'"))' : '').
                (!empty($filter['type']) ? ' AND `type` = "'.mysqli_real_escape_string($db->mysqli(), $filter['type']).'"' : '').
                ' AND `start` < NOW()
                 ORDER BY `start` DESC
                 LIMIT ' . ($limit + 1);
        
        $past = array();
        if($db->query($sql))
        {
            while($a = $db->readrow('aarr'))
            {
                $past[$a['id'].'-'.$a['team_id']] = new Event($a);
            }
        }
        //$past = array_reverse($past, true);
        
        $more_future = 0;
        if(count($future) == intval($limit / 2) + 1)
        {
            $more_future = 1;
            array_shift($future);
        }
        
        $more_past = 0;
        if(count($past) == $limit + 1)
        {
            $more_past = 1;
            array_pop($past);
        }
        
        $return = array(
            'future' => $more_future,
            'past'   => $more_past,
            'events' => ($future + $past)
        );
        
        return $return;
    }
    
    public static function getTeamEvents($team_id, $types = array(), $type = '', $start = '', $event_id = '0', $filter = array(), $limit = EVENT_LIMIT, $only_active = true)
    {
        $team_id = intval($team_id);
        
        $db = new Database();
        
        if($type == 'past')
        {
            $i = '<';
            $desc = 'DESC';
        }
        elseif($type == 'future')
        {
            $i = '>';
            $desc = 'ASC';
        }
        $add_where = ' OR (`start` = "'.mysqli_real_escape_string($db->mysqli(), $start).'" AND (`team_id` = "'.$team_id.'" OR `oponent_id` = "'.$team_id.'") AND `id` <> "'.$event_id.'")';
        
        $sql = 'SELECT * 
                FROM `event`
                WHERE (
                    (`team_id` = "'.$team_id.'"'.($only_active ? ' AND `deleted_home` = 0' : '').')
                    OR 
                    (`oponent_id` = "'.$team_id.'"'.($only_active ? ' AND `deleted_away` = 0' : '').')
                )' .
                (!empty($types) ? ' AND `type` IN ("'.implode('","', $types).'")' : '').
                (!empty($filter['year']) ? ' AND (`season_id` = "'.intval($filter['year']).'" OR `oponent_season_id` = "'.intval($filter['year']).'")' : '').
                (!empty($filter['type']) ? ' AND `type` = "'.mysqli_real_escape_string($db->mysqli(), $filter['type']).'"' : '').
                ' AND CONCAT(`team_id`,"-",`id`) != "'.$team_id . '-' . $event_id.'" AND CONCAT(`oponent_id`,"-",`id`) != "'.$team_id . '-' . $event_id.'" 
                 AND (`start` '.$i.' "'.mysqli_real_escape_string($db->mysqli(), $start).'"
                 '.$add_where.')
                 ORDER BY `start` '.$desc.', `id` '.$desc.'
                 LIMIT ' . ($limit + 1);

        $array = array();
                
        if($db->query($sql))
        {
            while($a = $db->readrow('aarr'))
            {
                $array[$a['id'].'-'.$a['team_id']] = new Event($a);
            }
        }
        
        return $array;
        
        if($type == 'past')
        {
            return $array;
        }
        else
        {
            return array_reverse($array, true);
        }
    }
    
    public static function getUserEvents($teams = array(), $include_passed = false, $only_active = true)
    {
        $db = new Database();
        
        $sql = 'SELECT * 
                FROM `event`
                WHERE 
                (
                    (`team_id` IN ("'.implode('","', $teams).'")'.($only_active ? ' AND `deleted_home` = 0' : '').')
                    OR 
                    (`oponent_id` IN ("'.implode('","', $teams).'")'.($only_active ? ' AND `deleted_away` = 0' : '').')
                )'.
                (!$include_passed ? ' `end` >= "'.date('Y-m-d H:i:s').'"' : '').
                ' ORDER BY `start` ASC';
                
        return self::loadArray($sql, 'Event');
    }
    
    /**
	 * Return last event id for given team
	 * @param int $team_id
	 * @return int
	 */
	public static function getLastId($team_id)
	{
		$db = new Database();
	 	
        $sql = 'SELECT MAX(`id`) as `id` 
                FROM `event` 
                WHERE `team_id` = '.intval($team_id);
	 	
        if($db->query($sql))
	 	{
	 		$result = $db->readrow('aarr');
	 		return empty($result['id']) ? 1 : ($result['id'] + 1);
	 	}
	 	else
	 	{
	 		return 0;
	 	}
	}
    
    public static function deleteEvent(Event $event, User $logged_user)
    {
        // FIXME smazani dochazky
        // FIXME smazani z nastenky
        $db = new Database();
        
        $event_id = $event->team_id . '_' . $event->id;

        $sql = 'DELETE 
                FROM `notifications`
                WHERE `object_id` = "'.$event_id.'_event"';
        $db->query($sql);
        

        if($logged_user->getActiveTeam()->id == $event->team_id)
        {
            $team_type = 'home';
        }
        else
        {
            $team_type = 'away';
        }

        $sql = 'UPDATE `event` 
                SET `deleted_home` = 1, `deleted_away` = 1, `confirmed_'.$team_type.'` = 1, `result_confirmed_'.$team_type.'` = 1
                WHERE `id` = "'.$event->id.'" AND `team_id` = "'.$event->team_id.'"
                LIMIT 1';

        return self::nonEmpty($sql);
    }
    
    public static function get($id, $team_id, $active = true)
    {
        $db = new Database();
        
        $sql = 'SELECT * 
                FROM `event` 
                WHERE `id` = "'.intval($id).'" AND `team_id` = "'.intval($team_id).'"'.($active ? ' AND `deleted_home` = 0 AND `deleted_away` = 0' : '').'
                LIMIT 1';
                
        return self::load($sql, 'Event');
    }
    
    public static function getEarliestEvent($type = '', $team_id = '', $only_active = true)
    {
        if(!empty($type) && is_array($type) && !empty($team_id) && is_array($team_id))
        {
            $db = new Database();
            
            $sql = 'SELECT *
                    FROM `event`
                    WHERE 
                        `type` IN ("'.implode('", "', $type).'") 
                        AND 
                        (
                            (`team_id` IN ("'.implode('","', $team_id).'")'.($only_active ? ' AND `deleted_home` = 0' : '').')
                            OR 
                            (`oponent_id` IN ("'.implode('","', $team_id).'")'.($only_active ? ' AND `deleted_away` = 0' : '').')
                        )
                        AND `start` >= "'.date('Y-m-d H:i:s').'"
                    ORDER BY `start` ASC
                    LIMIT 1';
                    
            return self::load($sql, 'Event');
        }
        
        return false;
    }


    public static function getEventsForUser($teams, $from, $to, User $user)
    {
        $return = array();

        $teams = implode(',', $teams);
        $sql = 'SELECT * 
                FROM `event`
                WHERE ((`deleted_home` <> 1 AND `team_id` IN (' . $teams . ')) OR (`deleted_away` <> 1 AND `oponent_id` IN (' . $teams . '))) AND
                    `start` >= "'.$from.'" AND `start` <= "'.$to.'" 
                ORDER BY `start` ASC';

        $events = self::loadArray($sql, 'Event');
        foreach($events as $event)
        {
            if($event->isInvited($user))
            {
                $return[$event->id] = $event;
            }
        }

        return $return;
    }

    public static function confirmEvent($team_id, $event_id, $type = 'home')
    {
    	$sql = 'UPDATE `event`
    			SET `confirmed_'.$type.'` = NOW()
    			WHERE `team_id` = ' . intval($team_id) . ' AND `id` = ' . intval($event_id) . '
    			LIMIT 1';

		return self::nonEmpty($sql);
    }

    public static function confirmResult($team_id, $event_id, $type = 'home')
    {
    	$sql = 'UPDATE `event`
    			SET `result_confirmed_'.$type.'` = NOW()
    			WHERE `team_id` = ' . intval($team_id) . ' AND `id` = ' . intval($event_id) . '
    			LIMIT 1';

		return self::nonEmpty($sql);
    }

}