<?php

if(isset($_GET['id']) && isset($_GET['key']))
{
	$user = UserDAO::getByKey($_GET['id'], $_GET['key']);
    if($user instanceof User)
    {
        $user->getTeamList();
        $user->getSettings();
        $name = $user->fname . ' ' . $user->sname;
        $city = $user->getResidence();
    
        // titulek stranky
        $title = 'Profil uživatele ' . $name;
        $desc = 'Profil uživatele ' . $name;
        $keywords = 'Profil uživatele ' . $name;
    }
    else
    {
        Header('Location: ' . PATH_WEB_ROOT . 'user-not-found/');
    }
}
?>