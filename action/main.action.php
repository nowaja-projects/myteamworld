<?php
if(CHtml::checkGet('activate'))
{
	if(!empty($_GET['email']))
	{
		$user = UserDAO::getByEmail($_GET['email']);

		if($user instanceof User && !empty($user->id) && empty($user->verified))
		{
			if(SendMail::registration($user->email, $user->hash))
			{
				$text = '<p class="text">Na e&#8209;mailovou adresu <strong>'.$user->email.'</strong> byl odeslán e&#8209;mail s aktivačním klíčem.</p>
		            	<p class="lastText">Aktivujte prosím váš účet <strong>kliknutím na odkaz v e&#8209;mailu</strong> a dokončete vaši registraci.</p>';
		        $_project['message']->addDone($text);
		        $_project['message']->saveMessages();
		    }
		}
	}

	header('Location: '.PATH_WEB_ROOT);
	die;
}

if(CHtml::checkPost('login') && empty($logged_user->id))
{
	$logged_user = UserDAO::login($_POST['login']['login'],$_POST['login']['password']);
	if($logged_user instanceof User)
	{
        if($logged_user->isVerified())
        {
            // nastavime aktivní tym podle hlavniho
            $logged_user->setActiveTeam($logged_user->team);

    		$_project['session']->set('logged', $logged_user);
            setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS);
    		//echo PATH_WEB_ROOT.'?action=registration';

    		// pokud pred prihlasenim chtel na nejakou jinou stranku nez na nastenku
    		$last_page = $_S->get('last_page');
    		$last_page = mb_substr($last_page, 1);

    		/*if(!empty($last_page) && $last_page != 'favicon.ico')
    		{
    			$_S->sunset('last_page');
    			header('Location: '.PATH_WEB_ROOT.$last_page);
    			die;
    		}
    		else*/
    		{
    			$_S->sunset('last_page');
    			header('Location: '.PATH_WEB_ROOT.'wall/');
    			die;
    		}
        }
        else
        {
            $text = '<p class="text">Váš účet ještě nebyl aktivován.</p>
				<p class="lastText"><a href="'.PATH_WEB_ROOT.'?activate=1&email='.urlencode($logged_user->email).'" title="Zaslat znovu aktivační email">Zaslat znovu aktivační email?</a></p>';
            $_project['message']->addWarning($text);
            $logged_user = new User();
        }
	}
	else
	{
        $logged_user = new User();
		//echo 'nepovedlo se prihlasit uzivatele';
        $text = '<p class="text">Zadali jste <strong>neplatný e-mail</strong> nebo <strong>heslo</strong>!</p>
				<p class="lastText"><a href="'.PATH_WEB_ROOT.'forgotten-password/" title="Poslat si nové heslo">Zapomněli jste své heslo?</a></p>';
		$_project['message']->addWarning($text);
	}
}

if(CHtml::checkPost('registration'))
{
	$_POST['registration']['email'] = trim(@$_POST['registration']['email']);

    // on totiz posila pole a to je spatne
    $_POST['registration']['locationID'] = @$_POST['registration']['locationID'][0];
    $_POST['registration']['city'] = CitiesDAO::getById($_POST['registration']['locationID']);

    $err = array();
	$dataOK = true;

	//Kontrola jmena
	if(!Validate::valid('string',$_POST['registration']['fname']))
	{
		$err[] = 'jméno';
		$dataOK = false;
	}

	//Kontrola prijmeni
	if(!Validate::valid('string',$_POST['registration']['sname']))
	{
		$err[] = 'příjmení';
		$dataOK = false;
	}

	//Kontrola emailu
	if(!Validate::valid('email',$_POST['registration']['email']))
	{
		$err[] = 'e-mail';
		$dataOK = false;
	}
	//Kontrola existence emailu
	elseif(UserDAO::emailExists($_POST['registration']['email']))
	{
        $err[] = 'e-mail ('.$_POST['registration']['email'].' je již zaregistrovaný)';
		$dataOK = false;
	}

	//Kontrola hesla
	if(!Validate::valid('string',$_POST['registration']['password']))
	{
		$err[] = 'heslo';
		$dataOK = false;
	}

    //Kontrola bydliste
	if(!Validate::valid('string',$_POST['registration']['city']) || empty($_POST['registration']['locationID']))
	{
		$err[] = 'bydliště';
		$dataOK = false;
	}

    //Kontrola bydliste
	if(!Validate::valid('required',$_POST['registration']['sport']))
	{
		$err[] = 'sport';
		$dataOK = false;
	}

	//Data jsou v poradku muzu vkladat
	if($dataOK)
	{
		$user_data = $_POST['registration'];
        $user_data['hash'] = md5($user_data['email'].HASH_SALT);
        $user_data['verified'] = 0;
		//Nastavim hesh hesla
        $pass = $user_data['password'];
		$user_data['password'] = UserDAO::encrypt($user_data['password']);
		//Ulozim data
		$user_id = UserDAO::insert($user_data);
		//Pokud ulozeni probehlo v poradku
		if($user_id)
		{
            // vytvorime slozku pro uzivatele
            @mkdir(USER_DATADIR . $user->id, 0777, true);

            // vytvorime defaultni obrazky pro uzivatele
            foreach($global_user_images as $image)
            {
                @copy(USER_DATADIR . $image, USER_DATADIR . $user->id . '/' . $image);
            }

			//Nastavim uzivateli id
			$user_data['id'] = $user_id;
			//Vytvorim objekt User
			$user = new User($user_data);
			//Pridam objekt do session
			//$_project['session']->set('logged', $user);
			//Pridam zpravu
            $text = '<p class="text">Na e&#8209;mailovou adresu <strong>'.$user_data['email'].'</strong> byl odeslán e&#8209;mail s aktivačním klíčem.</p>
            			<p class="lastText">Aktivujte prosím váš účet <strong>kliknutím na odkaz v e&#8209;mailu</strong> a dokončete vaši registraci.</p>';
            SendMail::registration($user_data['email'], $user_data['hash'], $pass);
			$_project['message']->addDone($text);
			//Presmeruji
			//header('Location: '.PATH_WEB_ROOT.'?action=registration');
		}
		//Ulozeni neprobehlo v poradku
		else
		{
			$_project['message']->addWarning('<p class="text">Registrace uživatele se <strong>nezdařila</strong>!</p><p class="lastText">Zkuste to prosím později.</p>');
		}
	}
    else
    {
        $text = '<p class="text"><strong>Registrace uživatele se nezdařila</strong>! Zkontrolujte a vyplňte správně následující <strong>povinné položky</strong>:</p><p class="lastText">';
        $text .= implode(', ', $err);
        $text .= '</p>';
        $_project['message']->addWarning($text);
    }
}
?>