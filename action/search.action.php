<?php

if(isset($_GET['search']))
{
    if(!empty($_GET['search']))
    {
        $view_more_users = false;
        $searched_users = $searched_teams = array();
        
        $searched_teams = SearchDAO::lookupTeams($_GET['search'], $view_more_teams, $team_count);
        $searched_users = SearchDAO::lookupUsers($_GET['search'], $view_more_users, $user_count);
    }
}
else
{
    $searched_teams = $searched_users = false;
}

?>