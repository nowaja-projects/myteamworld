<?php
    // nacteme si info o tymu
    // info se nacita v preload.php

    if(!($team instanceof Team && $team->id > 0))
    {
        Header('Location: ' . PATH_WEB_ROOT . 'team-not-found/');
        die;
    }

    if($team->isPlayer($logged_user) && $team->id != $logged_user->getActiveTeam()->id)
    {
        $logged_user->setActiveTeam($team);
        $logged_user->getTeamList(true);
        
        updateLoggedUser($_project, $logged_user);
    }

    // opuštění týmu
    if(!empty($_GET['leave_team']) && $_GET['leave_team'] == 'yes')
    {
        $admins = $team->getAdmins();
    	// pokud je admin tak musi byt adminu vice nez jeden
    	if($team->isAdmin($logged_user) && count($admins) == 1)
    	{
    		$_project['message']->addWarning($tr->tr('Nemůžete opustit tým ') . $team->getName() . ', ' . $tr->tr('protože jste jeho jediným správcem.'));
    		$_project['message']->saveMessages();
    	}
    	elseif($team->removePlayer($logged_user->id))
    	{
            // posleme notifikaci adminum daneho tymu, ktery opustil FIXME
            $notification_data = array();
            $time = time();
            foreach($admins as $admin_id)
            {
                $notification_data[] = array(
                    'user_id'    => $admin_id,
                    'object_id'  => $team->id, 
                    'type_id'    => 'USER-REMOVED-FROM-TEAM',
                    'info'       => serialize(
                                        array(
                                            'user_id' => $logged_user->id
                                        )
                                    ),
                    'object_info'=> '',
                    'timestamp'  => $time,
                    'read'       => '0'
                );
            }

            // vlozime notifikace
            Notifications::insertNotifications($notification_data);

            // odebereme tym ze seznamu tymu uzivatele
            $teams = $logged_user->getTeamList();
            unset($teams[$team->id]);

            if($logged_user->team == $team->id)
            {
                // pokud mu zbyly nejaky tymy, kterych je clenem, tak jeden z nich nastavime jako hlavni
                if(count($teams) > 0 && $logged_user->team == $team->id)
                {
                    $logged_user->team = key($teams);
                    $logged_user->save();
                }
                elseif(count($teams) == 0)
                {
                    $logged_user->team = 0;
                    $logged_user->save();
                }
            }
            
            if(count($teams) > 0)
            {
                // nastavime novy aktivni tym
                $activeTeam = current($teams);
                $logged_user->setActiveTeam($activeTeam);
                $logged_user->getTeamList(true);
            }

            $_project['session']->set('logged', $logged_user);
            
	    	$_project['message']->addDone($tr->tr('Právě jste opustil(a) tým ') . $team->getName() . ' ' . $tr->tr('a již nejste dále jeho členem.'));
	    	$_project['message']->saveMessages();
	    }
	    else
	    {
	    	// FIXME log
	    }

        Header('Location:' . $team->getProfileLink());
        die;
    }


    if(!empty($_GET['subaction']) && $_GET['subaction'] == 'gallery-detail')
    {
        require_once('action/gallery-detail.action.php');
    }
    
    $title = 'Profil týmu ' . $team->getName();
    $desc = 'Profil týmu ' . $team->getName();
    $keywords = 'Profil týmu ' . $team->getName();

    if(!empty($_GET['subaction']) && $_GET['subaction'] == 'event-detail')
    {
        require_once('action/event-detail.action.php');

        $subaction = 'events';

        if($event->type == Event::COMPETITION || $event->type == Event::FRIENDLY)
        {
            $title = 'Detail zápasu';
            $desc = 'Detail zápasu';
            $keywords = 'Detail zápasu';
        }
        elseif($event->type == Event::ACTION)
        {
            $title = 'Detail týmové akce';
            $desc = 'Detail týmové akce';
            $keywords = 'Detail týmové akce';
        }
        elseif($event->type == Event::TRAINING)
        {
            $title = 'Detail tréninku';
            $desc = 'Detail tréninku';
            $keywords = 'Detail tréninku';
        }
        
    }

	 // titulek stranky
    