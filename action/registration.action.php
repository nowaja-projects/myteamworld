<?php
if(CHtml::checkPost('profile'))
{
    //$_POST['profile']['city'] = $_POST['profile']['city'][0];
    $_POST['profile']['locationID'] = @$_POST['profile']['locationID'][0];

    $_POST['profile']['city'] = CitiesDAO::getById($_POST['profile']['locationID']);
    /*
    tohle se nakonec kontroluje primo v preload.php
    if($action == 'registration')
    {
        $control_hash = md5($_GET['email'].HASH_SALT);
        if($control_hash != $_GET['hash']) // nesedi nam kontrolni hashe
        {
            Header('Location: ' . PATH_WEB_ROOT);
        }
    }*/

	if($_POST['profile']['email'] != $logged_user->email)
	{
		$emailOK = !$logged_user->emailExists();
	}
	else
	{
		$emailOK = true;
	}

    // resizne a uploadne obrazek
    /*if(!empty($_FILES['image']) && $logged_user instanceof User)
    {
        $logged_user->uploadImage();
    }*/

    // prevedeme datum narozeni na spravny format pro ulozeni
    $postbirth = $_POST['profile']['birth'];
    $_POST['profile']['birth'] = $_POST['profile']['birth']['Y'].'-'.$_POST['profile']['birth']['n'].'-'.$_POST['profile']['birth']['j'];
    $_POST['profile']['key'] = friendly_url($_POST['profile']['fname'] . '-' . $_POST['profile']['sname']);

    $user = new User($_POST['profile']);

	if($user->validate() && $emailOK)
	{
        $user->verified = 1;
		$user->id = $logged_user->id;
        $user->save();
        $user->password = $logged_user->password;

        $logged_user = $user;
        // musime updatnout i seznam tymu a aktivni tymy
        $logged_user->getTeamList(false);
        $logged_user->getActiveTeam(false);

        // FUCK THIS!!!!
        //setcookie(COOKIENAME, serialize($logged_user), time()+60*60*24*EXPIRE_DAYS);

        /* ulozeni predvoleb, na zobrazovani RSS zdroju */
        $db = new Database();
        $db->query('DELETE FROM `rss_user` WHERE `user_id` = ' . $user->id);
        if(isset($_POST['sources']) && count($_POST['sources']) > 0)
        {
            foreach($_POST['sources'] as $key => $val)
            {
                $rss[] = '('.$user->id.','.$key.')';
            }
            $rss = implode(',', $rss);
            $sql = 'INSERT INTO `rss_user` VALUES ' . $rss;
            $db->query($sql);
        }

        // ulozime info o soukromi - posila se zvlast, protoze se uklada do jine tabulky
        if(isset($_POST['privacy']))
        {
            $logged_user->updateSettings($_POST['privacy']);
        }


        // změna hesla
        if(isset($_POST['password']) && !empty($_POST['password']['new_one']))
        {
            // je spravne stare heslo?
            if($logged_user->password == UserDAO::encrypt($_POST['password']['old']))
            {
                // zmenime
                if($_POST['password']['new_one'] == $_POST['password']['new_two'])
                {
                    $logged_user->changePassword($_POST['password']['new_one']);
                }
                else
                {
                    $text = '<h3 class="title">Chyba!</h3><p class="text">Zadaná hesla se neshodují, heslo nebylo změněno!</p>';
                    $_project['message']->addWarning($text);
                }
            }
            else
            {
                $text = '<h3 class="title">Chyba!</h3><p class="text">Zadané staré heslo není správné, heslo nebylo změněno!</p>';
                $_project['message']->addWarning($text);
            }
        }


        if($action == 'registration')
        {
            $text = '<!-- Google Code for Registrace u&#382;ivatele Conversion Page -->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 960446376;
            var google_conversion_language = "en";
            var google_conversion_format = "3";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "7kKeCKi3vVgQqP_8yQM";
            var google_remarketing_only = false;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/960446376/?label=7kKeCKi3vVgQqP_8yQM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript><h3 class="title">Hotovo!</h3><p class="text">Váš účet byl úspěšně aktivován. Nyní můžete využívat všech funkcí myteamworld.com.</p>';
            $_project['message']->addDone($text);
            $_project['message']->saveMessages();
            SendMail::verifyRegistration($user->email);

            // vsechny jeho pozvanky musime upravit na jeho ID
            RosterInvites::updateInvitesByEmail($logged_user->email, $logged_user->id);

            // nacteme si vsechy pozvanky do tymu
            $invites = RosterInvites::getUserInvites($logged_user->id);

            if(is_array($invites) && count($invites) > 0)
            {
                // posleme mu notifikace o pozvankach do tymu
                $notification_data = array();
                $time = time();

                // projdeme vsechny pozvanky a posleme mu notifikaci
                foreach($invites as $invite)
                {
                    if(isset($invite['team_id'], $invite['logged_user_id'], $invite['user_id'], $invite['id']))
                    {
                        // info, které si uložíme
                        // přihlášený uzivatel kvuli notifikaci
                        // team kvuli odkazu na tym
                        $array = array(
                            'team'          => $invite['team_id'],
                            'logged_user'   => $invite['logged_user_id'],
                            'invited_user'  => $invite['user_id'],
                            'invite_id'     => $invite['id']
                        );

                        // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                        $notification_data[] = array(
                            'user_id'    => $logged_user->id,
                            'object_id'  => $invite['team_id'],
                            'type_id'    => 'TEAM-INVITATION',
                            'info'       => serialize($array),
                            'object_info'=> '',
                            'timestamp'  => $time,
                            'read'       => '0'
                        );
                    }
                }

                if(count($notification_data))
                {
                    // pokud existuje uzivatel, tak vložíme ještě notifikaci
                    Notifications::insertNotifications($notification_data);
                }
            }

            $_project['session']->set('logged', $logged_user);
            $_project['session']->set('firstVisit', 1);

            Header('Location: '.PATH_WEB_ROOT . 'wall/');
            die;
        }
        else
        {
            $_project['message']->addDone('<h3 class="title">Hotovo</h3><p class="text">Úprava profilu proběhla úspěšně!</p>');
        }

        $_project['session']->set('logged', $logged_user);
	}
	else
	{
        $error_list = $user->getErrors();

		$email_checked = false;
        $text = '<h3 class="title">Úprava profilu se nezdařila!</h3><p class="text">Zkontrolujte a vyplňte správně následující povinné položky:</p><ul>';

		foreach($error_list as $error)
		{
            $text .= '<li>';
			switch($error)
			{
				case 'email' :
					$email_checked = true;
					$text .= 'e-mail';
				break;

                case 'sex' :
                    $text .= 'pohlaví';
                break;

				case 'fname' :
					$text .= 'jméno';
				break;

				case 'sname' :
					$text .= 'příjmení';
				break;

                case 'city' :
					$text .= 'místo pobytu';
				break;

                case 'region' :
					$text .= 'kraj';
				break;

                case 'birth' :
					$text .= 'datum narození';
				break;

                case 'weight' :
					$text .= 'váha';
				break;

                case 'height' :
					$text .= 'výška';
				break;
			}
            $text .= '</li>';
		}
        $text .= '</ul>';
        $_project['message']->addWarning($text);

		if(!$email_checked && !$emailOK)
		{
			$_project['message']->addWarning('<h3 class="title">Chyba</h3><p class="text">E-mail je již zaregistrovaný.</p>');
		}
	}

}
?>