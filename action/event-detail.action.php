<?php
if(isset($_GET['id']) && $team->userCanViewModule('events', $logged_user))
{
    // id udalosti
    $id = explode('-', $_GET['id']);
    
    // musime mit presne dve ID
    if(count($id) != 2)
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }
    
    // nacteme si událost
    $event = EventDAO::get($id[0], $id[1], false);
    
    // udalost musi existovat
    if($event instanceof Event && !empty($event->id) && !$event->isPartDeleted())
    {
        $event->info = unserialize($event->info);
        $event->result = unserialize($event->result);
        $isResult = true;

        if(!is_array($event->result) || empty($event->result))
        {
            $isResult = false;
        }
    }
    else
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }

    if($event->type == Event::COMPETITION || $event->type == Event::FRIENDLY)
    {
        $title = 'Detail zápasu';
    }
    elseif($event->type == Event::ACTION)
    {
        if(!$team->isPlayer($logged_user))
        {
            Header('Location: ' . PATH_WEB_ROOT . 'events/');
            die;
        }
        $title = 'Detail týmové akce';
    }
    elseif($event->type == Event::TRAINING)
    {
        if(!$team->isPlayer($logged_user))
        {
            Header('Location: ' . PATH_WEB_ROOT . 'events/');
            die;
        }
        $title = 'Detail tréninku';
    }
    else
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }

    // pokud neni uzviatel prihlaseny, tak nemame tym
    if(!($team instanceof Team))
    {
        $team = TeamDAO::get($event->team_id);
    }
    
    // mame tym ktery je na MTW
    if(!empty($event->oponent_id))
    {
        // nacteme si data
        $event_data = array(
            'homeTeam' => TeamDAO::get($event->team_id),
            'awayTeam' => TeamDAO::get($event->oponent_id) 
        );
    }
    // FIXME neexistujici tym
    else
    {
        // nacteme si data
        $event_data = array(
            'homeTeam' => TeamDAO::get($event->team_id),
            'awayTeam' => new Team()
        );
    }
}
// musime mit ID, jinak zdarec
else
{
    Header('Location: ' . PATH_WEB_ROOT . 'events/');
    die;
}
