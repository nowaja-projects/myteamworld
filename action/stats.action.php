<?php

if(!$team->userCanViewModule('stats', $logged_user))
{
	Header('Location:' . PATH_WEB_ROOT);
	die;
}
