<?php
if($action == 'edit-team-profile')
{
    if(!$team->isAdmin($logged_user))
    {
        Header('Location:' . PATH_WEB_ROOT);
        die;
    }
}

// kvuli default hodnotam ve formulari
$tmp = new Team();
$error_list = array();

// kdyz mame odeslana data a zaroven prihlaseneho uzivatele, tak 
if(CHtml::checkPost('team') && $logged_user instanceof User)
{
    if(Validate::valid('email', $_POST['team']['contact_email']))
	{
		$emailOK = true;
	}
	else
	{
		$emailOK = false;
	}
   
    $_POST['team']['key'] = friendly_url(@$_POST['team']['prefix'] . '-' . @$_POST['team']['name'] . '-' . @$_POST['team']['category']);
    $_POST['team']['locationID'] = isset($_POST['team']['locationID'][0]) ? $_POST['team']['locationID'][0] : '';
    $_POST['team']['city'] = CitiesDAO::getById($_POST['team']['locationID']);


    $tmp = new Team($_POST['team']);
    
    // nastavime ID uzivatele, ktery tym vytvoril 
    if($action == 'team-registration')
    {
        $tmp->user_id = $logged_user->id;    
    }
    else
    {
        $owner = explode('-', @$_POST['teamOwner']);

        // nastaveni vlastnika
        if(@$owner[1] == getRecipientHash(@$owner[0]))
        {
            $tmp->user_id = $owner[0];
        }
    }

    
    // kdyz nastavuje i moduly
    if(CHtml::checkPost('module'))
    {
        // defaultne je vse OK
        $modulesOK = true;
        
        // projdeme vsechny poslane moduly a zkontrolujeme zda jsou v poradku
        foreach($_POST['module'] as $sent_key => $sent)
        {
            // kontrola zda zalsany klic je vubec povoleny
            if(!in_array($sent_key, array_keys($global_modules)))
            {
                $modulesOK = false;
                break;
            }
            // jestli je povolena zaslana hodnota
            elseif(!in_array($sent, array_keys($global_modules[$sent_key]['values'])))
            {
                $modulesOK = false;
                break;
            }
            // zkontrolujeme jeste zavislosti jednotlivych modulu na sebe
            else
            {
                // vypnute udalosti, musi mit vypnuty i zbytek
                if(empty($_POST['module']['events']) || $_POST['module']['events'] == 'no')
                {
                    $_POST['module']['stats'] = 'no';
                    $_POST['module']['attendance'] = 'no';
                }
            }
        }
        
        // ulozime moduly k tymu
        $tmp->setModules($_POST['module']);
    }
    // vzdycky by mel poslat i moduly, jinak je neco spatne
    else
    {
        $modulesOK = false;
    }
    
    // zkontrolujeme  aulozime
    if($tmp->validate() && $emailOK && $modulesOK && (($action != 'team-registration' && $tmp->isAdmin($logged_user)) || $action == 'team-registration'))
    {
        if($action == 'team-registration')
        {
            $tmp->id = $tmp->save();

            // pokud je to prvni tym, ktery jsem vytvoril, tak ho nastavim jako hlavni tym
            if(empty($logged_user->team))
            {
                $logged_user->team = $tmp->id;
            }

            $logged_user->setActiveTeam($tmp);
            $logged_user->getTeamList(true);
            $logged_user->save();

            updateLoggedUser($_project, $logged_user);
        }
        else
        {
            $tmp->save();
        }
        
        // jestlize nam poslal nastaveni sezony (asi by mel vzdy ale pro jistotu)
        if(isset($_POST['activeSeason']))
        {
            // musime kontrolovat zda nam nepodvrhnul neexistujici sezonu
            if(in_array($_POST['activeSeason'], array_keys($tmp->getSeasons())))
            {
                // nastavime aktivni sezonu
                $tmp->setActiveSeason($_POST['activeSeason'], true);
            }
            
        }
        
        if($tmp->id)
        {
            // musime se mrknout jestli nahodou neposlal uz fotku
            if(isset($_POST['tmpImages']) && !empty($_POST['tmpImages']))
            {
                if(!is_dir(TEAM_DATADIR . intval($tmp->id) . '/'))
                {
                    @mkdir(TEAM_DATADIR . intval($tmp->id) . '/', 0777, true);
                }
                
                // FIXME ME PICO
                $dir = TEAM_DATADIR . '0'. $_POST['tmpImages'];     
                if(!is_dir($dir))
                {
                    @mkdir($dir, 0777, true);
                }         
                $handle = opendir($dir);
                
                if ($handle)
                {
                    $delete = true;
                    while (false !== ($entry = readdir($handle)))
                    {
                        if($entry != '.' && $entry != '..')
                        {
                            $filename = TEAM_DATADIR . intval($tmp->id) . '/' . pathinfo($entry, PATHINFO_BASENAME);
                            if(copy($dir . $entry, $filename))
                            {
                                @unlink($dir . $entry);
                            }
                            else
                            {
                                $delete = false;
                            }
                        }
                    }
                    
                    if($delete)
                    {
                        @unlink($dir);
                    }
                
                    closedir($handle);
                }
            } 
            
            // pridame uzivatele do tymu pokud jsme na registraci
            if($action == 'team-registration')
            {
                $tmp->addUser($logged_user->id, 1, 1, 0);
            }

            // poslal tymove photo?
            if(!empty($_FILES['teamPhoto']))
            {
                $tmp->uploadImage(false, 'teamPhoto', true);
                
                $tmp->version = uniqid();
                $tmp->updateVersion();
            }

            $logged_user->setActiveTeam($tmp);

             // pokud je to prvni tym, do ktereho jsem vstoupil, tak ho nastavim jako hlavni tym
            if(empty($logged_user->team))
            {
                $logged_user->team = $tmp->id;
                $logged_user->getTeamList(true);
                $logged_user->save();
            }

            // aby se nam obnovil seznam tymu
            $logged_user->getTeamList(true);
            
            // ulozime usera do session
            updateLoggedUser($_project, $logged_user);

            // aktualizujeme nastaveni v objektu uzivatele
            $team = $logged_user->getActiveTeam();
            
            
            if($action == 'edit-team-profile')
            {
                $_project['message']->addDone('<h3 class="title">Hotovo!</h3><p class="text">Úprava týmu proběhla úspěšně!</p>');
            }
            else
            {
                $_project['message']->addDone('<h3 class="title">Hotovo!</h3><p class="text">Založení týmu bylo úspěšně dokončeno!</p>');
                $_project['message']->saveMessages();
                
                Header('Location: '.PATH_WEB_ROOT . 'wall/');
                die;
            }
        }
    }
    else
    {
        $error_list += $tmp->getErrors();
		$email_checked = false;
        $text = '<h3 class="title">Registrace týmu se nezdařila!</h3><p class="text">Zkontrolujte a vyplňte správně následující povinné položky:</p><ul>';
		foreach($error_list as $error)
		{
            $text .= '<li>';
			switch($error)
			{
				case 'name' : 
					$text .= 'název týmu';
				break;
				
				case 'sport_id' : 
					$text .= 'sport';
				break;
				
				case 'contact_name' : 
					$text .= 'kontaktní osoba';
				break;
                
                case 'contact_email' : 
                    $email_checked = true;
					$text .= 'e-mail na kontaktní osobu';
				break;

                case 'city':
                case 'locationID':
                    $text .= 'město';
                break;
                
                default:
                    $text .= $error;
                break;
			}
            $text .= '</li>';
		}
        $text .= '</ul>';
        $_project['message']->addWarning($text);
		
		if(!$email_checked && !$emailOK)
		{
			$_project['message']->addWarning('<h3 class="title">Chyba</h3><p class="text">E-mail není ve správném tvaru.</p>');
		}
    }
}
elseif($action == 'edit-team-profile')
{
    $team = $logged_user->activeTeam;
}

?>