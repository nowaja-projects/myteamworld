<?php

if(!$team->userCanViewModule('gallery', $logged_user))
{
	Header('Location:' . PATH_WEB_ROOT);
	die;
}