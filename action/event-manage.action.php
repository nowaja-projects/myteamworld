<?php
function checkDates($event, &$start, &$end)
{
    global $start_date;
    // uprava času, aby byl ve správném formátu
    $start_date = $event['start_date'];
    $start_time = substr($event['start_time'], 0, 5);
    $start_time .= ':00'; 
    $start_time = explode(':', $start_time);
    foreach($start_time as $i => $val)
    {
        // pokud ma jen jedno cislo, tak tam prihodime nulu
        $start_time[$i] = (strlen($val) == 2 ? $val : '0'.intval($val));
    }
    $start_time = implode(':', $start_time);
    $start = $start_date . ' ' . $start_time;
    
    // uprava času, aby byl ve správném formátu
    $end_time = substr($event['end_time'], 0, 5);
    $end_time .= ':00'; 
    $end_time = explode(':', $end_time);
    foreach($end_time as $i => $val)
    {
        // pokud ma jen jedno cislo, tak tam prihodime nulu
        $end_time[$i] = (strlen($val) == 2 ? $val : '0'.intval($val));
    }
    $end_time = implode(':', $end_time);
    if(isset($event['end_date']) && $event['type'] == Event::ACTION)
    {
        $end_date = $event['end_date'];
    }
    elseif($start_time > $end_time)
    {
        $end_date = date('Y-m-d', strtotime($start_date . '+1day'));
    }
    else
    {
        $end_date = $start_date;
    }
    $end = $end_date . ' ' . $end_time;
}

function checkAttendArray($attend = array(), &$players, $away_team, $insert = false)
{
    global $_project, $logged_user, $tr;

    // pokud vkladame udalost, tak vime jsme domaci
    if($insert)
    {
        $attend_team = @$attend['attend_team'];
        $attend      = @$attend['attend'];
    }
    // udalost editujeme, tak musime zjistit, zda jsme domaci nebo hoste
    else
    {
        // toto nastane jen pri editaci - editujeme udalost, kde jsme hostujici tym
        if($away_team instanceof Team && $logged_user->getActiveTeam()->id == $away_team->id)
        {
            $attend_team = @$attend['attend_team_away'];
            $attend      = @$attend['attend_away'];
        }
        else
        {
            // protoze predavame cely POST[event]  tak si z nej vytahneme potrebna data
            $attend_team = @$attend['attend_team'];
            $attend      = @$attend['attend'];
        }
    }
    
    $ok = true;
    
    if((!is_array($attend) || empty($attend)) && empty($attend_team))
    {
        $ok = false;
        $_project['message']->addWarning($tr->tr('Musíte vybrat uživatele, kteří budou do události pozvaní.'));
        $_project['message']->saveMessages();
    }
    elseif(is_array($attend))
    {
        // zkontrolujeme vsechny pozvany uzivatele, zda sedí hash
        foreach($attend as $id => $hash)
        {
            if($hash != getRecipientHash($id))
            {
                $ok = false;
                $_project['message']->addWarning($tr->tr('Vyberte uživatele z našeptávače.'));
                $_project['message']->saveMessages();
                break;
            }
            
            // nacteme si uzivatele
            $player = UserDAO::get($id);
            
            // pro pozdejsi pouziti
            $players[$player->id] = $player;

            // zjistime zda je uzivatel opravdu tymu, kterymu pridavame udalost
            if(!$logged_user->getActiveTeam()->isPlayer($player))
            {
                $ok = false;
                $_project['message']->addWarning($tr->tr('Můžete pozvat jen hráče z Vašeho týmu.'));
                $_project['message']->saveMessages();
                break;
            }
        }
    }
    
    return $ok;    
}


// nacteme si aktivni tym
$logged_user->getActiveTeam(true);

// musi byt admin aby mohl na tuto stranku
if(!$logged_user->getActiveTeam()->isAdmin($logged_user))
{
    Header('Location: ' . PATH_WEB_ROOT . 'events/');
    die;
}

$is_update = false;

if(isset($_GET['id']))
{
    // id udalosti
    $id = explode('-', $_GET['id']);
    
    // musime mit presne dve ID
    if(count($id) != 2)
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }
    
    // nacteme si událost
    $event = EventDAO::get($id[0], $id[1]);
    
    if($event instanceof Event && !empty($event->id))
    {
        $event->info = unserialize($event->info);
        $title = 'Upravit událost "' . $event->info[0]['name'] . '"';
        $is_update = true;
    }
    else
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }

    // uzivatel musi mit prava to vubec editovat
    if(!$event->userCanEdit($logged_user))
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }
}
else
{
    $title = 'Vytvořit novou událost pro tým ' . $logged_user->getActiveTeam()->getName();
}


// pokud editujeme událost
if(isset($event) && $event instanceof Event)
{
    $home_team = TeamDAO::get($event->team_id);
    $away_team = !empty($event->oponent_id) ? TeamDAO::get($event->oponent_id) : false;
    
    $event_team = getEventOponentTeam($event);
}


// neco je odeslany
if(!empty($_POST))
{
    // je vse v pohode?
    $ok = true;
    
    // pole s hraci v tymu
    $players = array();

    // editace události
    if(!empty($_POST['event']['id']) && isset($_POST['event']['team_id']))
    {
        // nacteme si událost
        $event = EventDAO::get($_POST['event']['id'], $_POST['event']['team_id']);
        
        $now = date('Y-m-d H:i:s');

        // pokud událost existuje, tak ji můžeme editovat
        if($event instanceof Event && !empty($event->id))
        {
            // uživatel musí být admin odeslaného týmu
            if(!$event->userCanEdit($logged_user))
            {
                // FIXME
                header('Location:' . PATH_WEB_ROOT . 'events/');
                die;
            }

            // udalost muzeme editovat pouze pokud neni potvrzena
            // muzeme ji vsak editovat, pokud mame neexistujiciho soupere
            if($event->isConfirmed() && !empty($event->oponent_id))
            {
                // FIXME
                header('Location:' . PATH_WEB_ROOT . 'events/');
                die;
            }


            // domaci tym
            $home_team = TeamDAO::get($event->team_id);

            // hostujici tym
            $away_team = !empty($event->oponent_id) ? TeamDAO::get($event->oponent_id) : false;

            // info o tymu oponenta
            $event_team = getEventOponentTeam($event);
            
            // ulozime si puvodni verzi udalosti, abychom se mohli podivat komu posilat notifikace atd.
            $original = clone $event;
            
            // info o tom, jak byla udalost puvodne
            $original->info = unserialize($original->info);

            // nesmí se změnit typ události, jinak je neco spatne
            if($event->type != $_POST['event']['type'])
            {
                header('Location:' . PATH_WEB_ROOT . 'events/');
                die;
            }
            
            // potřebujeme info o udalosti
            $data = unserialize($event->info);
            
            // naplnime data
            $data[0]['name'] = strip_tags($_POST['event']['name']);
            $data[0]['place'] = strip_tags($_POST['event']['place']);
            $data[0]['start_date'] = strip_tags($_POST['event']['start_date']);
            $data[0]['start_time'] = strip_tags($_POST['event']['start_time']);
            $data[0]['end_date'] = strip_tags(@$_POST['event']['end_date']);
            $data[0]['end_time'] = strip_tags(@$_POST['event']['end_time']);

            // my jsme domaci tym
            if($home_team->id == $logged_user->getActiveTeam()->id)
            {
                $event->season_id = strip_tags($_POST['event']['season_id']);
                $data[0]['season_id'] = strip_tags($_POST['event']['season_id']);

                // pokud mame existujici hostujici tym
                if($away_team)
                {
                    // FIXME ??
                }
                else
                {
                    // jelikoz máme kontrolu javascriptem, tak toto nesmi nastat
                    // kdyz to nastane, tak je mi jedno co uzivatel chtel udelat, nema se hrabat ve zdrojaku
                    if(empty($_POST['event']['team_name']) && in_array($_POST['event']['type'], array(Event::COMPETITION, Event::FRIENDLY)))
                    {
                        $_project['message']->addWarning($tr->tr('Tato událost již byla smazána.'));
                        $_project['message']->saveMessages();

                        $ok = false;
                    }

                    $data[0]['team_name'] = strip_tags(@$_POST['event']['team_name']);
                }
            }
            // my jsme hostujici tym
            elseif($away_team->id == $logged_user->getActiveTeam()->id)
            {
                $event->oponent_season_id = strip_tags($_POST['event']['season_id']);
                $data[0]['oponent_season_id'] = strip_tags($_POST['event']['season_id']);
            }
            // my nejsme zadny tym? nasrat
            else
            {
                header('Location:' . PATH_WEB_ROOT . 'events/');
                die;
            }

            // zkontrolujeme zda vsichni pozvani uzivatele jsou opravdu cleny tymu
            $ok = $ok && checkAttendArray($_POST['event'], $players, $away_team);
            
            if(!$ok)
            {
                header('Location:' . PATH_WEB_ROOT . 'events/');
                die;
            }

            // spravne nastaveni zacatku a konce
            $start = '';
            $end = '';
            $start_date = '';

            // nastavime spravne datum zacatku a konce 
            checkDates($_POST['event'], $start, $end);
            
            // upravime casy zacatku a konce 
            $event->start = $start;
            $event->end = $end;

            $oponent_team_id = 0;
            
            // pokud jsme domaci tym, tak z nasi strany je udalost potvrzena
            if($original->team_id == $logged_user->getActiveTeam()->id)
            {
                $data[0]['attend_team']         = intval(@$_POST['event']['attend_team']);
                $data[0]['attend']              = @$_POST['event']['attend'];
                $data[0]['attend_team_away']    = @$original->info[0]['attend_team_away'];
                $data[0]['attend_away']         = @$original->info[0]['attend_away'];

                $event->confirmed_home = $now;

                // kdyz mame existujici tym, tak je to nepotvrzene
                if(!empty($event->oponent_id))
                {
                    $event->confirmed_away = 0;
                    // FIXME a musime mu poslat notifikace
                    
                    $oponent_team_id = $event->oponent_id;
                }
                // neexistujici tym nas nezajima, je to potvrzene z obou stran
                else
                {
                    $event->confirmed_away = $now;
                }
            }
            // my jsme jako hostujici tym
            elseif($original->oponent_id == $logged_user->getActiveTeam()->id)
            {
                // jsme hoste 
                $data[0]['attend_team']         = @$original->info[0]['attend_team'];
                $data[0]['attend']              = @$original->info[0]['attend'];
                $data[0]['attend_team_away']    = intval(@$_POST['event']['attend_team_away']);
                $data[0]['attend_away']         = @$_POST['event']['attend_away'];

                // takze je to ode mne potvrzene
                $event->confirmed_away = $now;

                // a musi to potvrdit domaci, kterym posleme notifikaci
                $event->confirmed_home = 0;

                $oponent_team_id = $event->team_id;
            }
            // nejsme domaci, ani hoste, neco je spatne
            else
            {
                // FIXME
                Header('Location: ' . PATH_WEB_ROOT);
                die;
            }

            // nakonec data zase musime serializovat a ulozit k udalosti
            $event->info = serialize($data);

            // pokud se povedlo ulozit udalost
            if($event->save())
            {
                // jestli je to soutezni nebo pratelsky zapas a zaroven nemame tym ktery je na MTW
                if(($original->type == Event::COMPETITION || $original->type == Event::FRIENDLY) && $away_team === false && !empty($original->info[0]['team_name']))
                {
                    $hasImage = false;
                    // pokud se nám podařilo vložit událost, tak se pokusíme uploadovat logo
                    // upload loga týmu
                    if(!empty($_FILES['team_logo']))
                    {
                        $team->uploadImage(false, 'team_logo', false, '/event' . $original->id . '/');
                        $team->version = uniqid();
                        $team->updateVersion();
                        $hasImage = true;
                    }
                } 
                
                // data pro notifikace
                $notification_data = array();
                
                // dochazka, ktera byla u eventu puvodne
                // jsme domaci tym
                if($original->team_id == $logged_user->getActiveTeam()->id)
                {
                    $original_attend = @$original->info[0]['attend'];
                }
                // jsme hostujici tym
                else
                {
                    $original_attend = @$original->info[0]['attend_away'];
                }

                // puvodne asi nebyl pozvany nikdo
                if(empty($original_attend) || !is_array($original_attend))
                {
                    $original_attend = array();
                }
                
                // musime vsem pozvanym poslat notifikaci
                foreach($players as $key => $player)
                {
                    // nebudeme posilat notifikace prihlasenemu uzivateli 
                    if($player->id != $logged_user->id)
                    {
                        // nove pozvany uzivatel
                        if(!in_array($key, array_keys($original_attend)))
                        {
                            $notification_data[] = array(
                                'user_id'    => $player->id,
                                'object_id'  => $original->team_id . '_' . $original->id . '_' . 'event', 
                                'type_id'    => 'EVENT-INVITATION',
                                'info'       => serialize(
                                                    array(
                                                        'event_id'      => $event->id, 
                                                        'user_team'     => $logged_user->getActiveTeam()->id
                                                    )
                                                ),
                                'object_info'=> '',
                                'timestamp'  => strtotime($now),
                                'read'       => '0'
                            );

                            if($logged_user->getActiveTeam()->userCanViewModule('attendance', $player, false))
                            {
                                SendMail::sendEventInvitation($player->email, $logged_user->getActiveTeam(), $event);
                            }
                        }
                        // uzivatel uz byl pozvan, tak mu musime poslat notifikaci ze byla udalost zmenena
                        else
                        {
                            $notification_data[] = array(
                                'user_id'    => $player->id,
                                'object_id'  => $original->team_id . '_' . $original->id . '_' . 'event', 
                                'type_id'    => 'EVENT-CHANGE',
                                'info'       => serialize(
                                                    array(
                                                        'event_id'      => $event->id, 
                                                        'user_team'     => $logged_user->getActiveTeam()->id
                                                    )
                                                ),
                                'object_info'=> '',
                                'timestamp'  => strtotime($now),
                                'read'       => '0'
                            );
                            
                            // odebereme ho protoze jsme mu poslali notifikaci
                            // musi byt pryc protoze na konec posilame vsem co zustanou info o tom ze byli odebrani z udalosti
                            unset($original_attend[$key]);
                        }
                    }
                }
                
                
                // projdeme puvodne pozvany a posleme jim, ze uz pozvani nejsou chudacci
                foreach($original_attend as $key => $hash)
                {
                    if($key == $logged_user->id)
                    {
                        continue;
                    }
                    
                    // info o hraci
                    $player = UserDAO::get($key);
                    
                    $notification_data[] = array(
                        'user_id'    => $player->id,
                        'object_id'  => $original->team_id . '_' . $original->id . '_' . 'event', 
                        'type_id'    => 'EVENT-CANCEL-INVITATION',
                        'info'       => serialize(
                                            array(
                                                'event_id'      => $event->id, 
                                                'user_team'     => $logged_user->getActiveTeam()->id
                                            )
                                        ),
                        'object_info'=> '',
                        'timestamp'  => strtotime($now),
                        'read'       => '0'
                    );
                }


                // pokud vime kdo je souper
                if(!empty($oponent_team_id))
                {
                    // nacteme soupere
                    $oponent = TeamDAO::get($oponent_team_id);

                    // pokud existuje a zaroven ji souper nema smazanou
                    if($oponent instanceof Team && $oponent->id > 0 && !$event->isDeleted($oponent))
                    {
                        // nacteme si adminy a jim posleme notifikace, ze byla udalost zmenena
                        $admins = $oponent->getAdmins();

                        $data = array();
                        $time = time();

                        foreach($admins as $admin_id)
                        {
                            if($admin_id == $logged_user->id)
                            {
                                continue;
                            }
                            
                            // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                            $notification_data[] = array(
                                'user_id'    => $admin_id,
                                'object_id'  => $event->team_id . '_' . $event->id . '_event',
                                'type_id'    => 'EVENT-EDIT-CONFIRM-REQUEST',
                                'info'       => serialize(
                                                    array(
                                                        'event_id'      => $event->id,
                                                        'user_team'     => $oponent->id,
                                                        'oponent_team'  => $logged_user->getActiveTeam()->id
                                                    )
                                                ),
                                'object_info'=> '',
                                'timestamp'  => $time,
                                'read'       => '0'
                            );
                        }
                    }
                    else
                    {
                        // FIXME nejaky log, jinak je nam to jedno
                    }
                }
                
                // uložíme do DB nové notifikace
                if(!Notifications::insertNotifications($notification_data))
                {
                    // FIXME
                }
                
                $_project['message']->addDone($tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Událost byla úspěšně upravena.</p>'));
                $_project['message']->saveMessages();
                
                Header('Location: ' . PATH_WEB_ROOT . 'events/');
                die;
            }
            else
            {
                $ok = false;
                $_project['message']->addWarning($tr->tr('Nastala chyba při editaci události.'));
                $_project['message']->saveMessages();
                
                Header('Location: ' . PATH_WEB_ROOT . 'events/');
                die;
            }
        }
        else // událost neexistuje
        {
            $_project['message']->addWarning($tr->tr('Tato událost již byla smazána.'));
            $_project['message']->saveMessages();

            header('Location:' . PATH_WEB_ROOT . 'events/');
            die;
        }
    } // konec editace
    else // vložení
    {
        $oponent_team = false;
        $type_in_session = $_project['session']->get('eventManageType');
        if(empty($type_in_session))
        {
            $_project['message']->addWarning($tr->tr('Nejdříve musíte vybrat typ události.'));
            $_project['message']->saveMessages();
        }
        elseif(empty($_POST['event']['type']))
        {
            $_project['message']->addWarning($tr->tr('Nejdříve musíte vybrat typ události.'));
            $_project['message']->saveMessages();
        }
        elseif($_POST['event']['type'] != $type_in_session)
        {
            $_project['message']->addWarning($tr->tr('Nejdříve musíte vybrat typ události.'));
            $_project['message']->saveMessages();
        }
        else
        {
            // povolené hodnoty "typ události"
            $allowed = array(
                Event::ACTION,
                Event::COMPETITION,
                Event::FRIENDLY,
                Event::TRAINING
            );
            
            // jerslti nam nekdo nepodvrhnul typ udalosti
            if(!in_array($_POST['event']['type'], $allowed))
            {
                $ok = false;
                $_project['message']->addWarning($tr->tr('Nepovolený typ události.'));
                $_project['message']->saveMessages();
            }
            
            // jeslti nam nekdo nepodvrhnul prazdnou, nebo neexistujici sezonu
            $team_seasons = $logged_user->getActiveTeam()->getSeasons();
            if(empty($_POST['event']['season_id']) || !in_array($_POST['event']['season_id'], array_keys($team_seasons)))
            {
                $ok = false;
                $_project['message']->addWarning($tr->tr('Neexistující sezóna.'));
                $_project['message']->saveMessages();
            }

            if($ok)
            {
                // kontrola zda je poslany tym
                if(isset($_POST['event']['team']))
                {
                    // pokud posíláme soutezni zapas, nebo pratelsky zapas 
                    if($_POST['event']['type'] == Event::COMPETITION || $_POST['event']['type'] == Event::FRIENDLY)
                    {
                        // musíme mít tým který proti nám bude hrát, jinak je něco špatně
                        if((!is_array($_POST['event']['team']) || count($_POST['event']['team']) != 1))
                        {
                            // neco je spatne!
                            $ok = false;
                        }
                    }
                    else
                    {
                        // jindy se to posílat nemá
                        $ok = false;
                    }

                    if($ok)
                    {
                        // zkontrolujeme zda nejsou podvrzena data
                        $team_info = explode('-', $_POST['event']['team'][0]);
                        $team_id = $team_info[0];
                        $team_hash = $team_info[1];
                        
                        // nesedi hash
                        if($team_hash != getTeamHash($team_id))
                        {
                            // neco je spatne, kopneme ho do haje
                            $ok = false;
                        }
                        
                        // nacteme si info o tymu, ktery bude souperem
                        $oponent_team = TeamDAO::get($team_id);
                        
                        // zkontrolujeme zda pridava existujici team ze stejneho sportu
                        if(!($oponent_team instanceof Team) || empty($oponent_team->id) || $logged_user->getActiveTeam()->sport_id != $oponent_team->sport_id)
                        {
                            $ok = false;
                            $_project['message']->addWarning($tr->tr('Tento tým nemůže být Vašim soupeřem.'));
                            $_project['message']->saveMessages();
                        }
                    }
                }
                elseif($_POST['event']['type'] == Event::COMPETITION || $_POST['event']['type'] == Event::FRIENDLY)
                {
                    // musíme mít název týmu
                    if(empty($_POST['event']['team_name']))
                    {
                        $ok = false;
                    }
                    // pokud zadal email, posleme novemu tymu pozvanku
                    elseif(!empty($_POST['event']['invite_email']) && filter_var($_POST['event']['invite_email'], FILTER_VALIDATE_EMAIL))
                    {
                        SendMail::inviteTeam($_POST['event']['invite_email'], $logged_user, $logged_user->getActiveTeam());
                    }
                }
            }
        
            // zkontrolujeme zda pozval nejaky lidi a jestli opravdu patri do tymu
            $ok = checkAttendArray($_POST['event'], $players, $oponent_team, true);
    
            // všechny kontroly jsou OK, tak muzeme pridat event
            if($ok)
            {
                $type = 'event';
                // spravne nastaveni zacatku a konce
                $start = '';
                $end = '';
                
                // nastavime spravne datum zacatku a konce 
                checkDates($_POST['event'], $start, $end);
                
                // datum vytvoreni udalosti
                $now = date('Y-m-d H:i:s');
                
                $data = array(
                    'id'         => EventDAO::getLastId($logged_user->getActiveTeam()->id, $type),
                    'user_id'    => $logged_user->id,
                    'team_id'    => $logged_user->getActiveTeam()->id,
                    'oponent_id' => ($oponent_team instanceof Team && !empty($oponent_team->id) ? $oponent_team->id : 0),
                    'season_id'  => $_POST['event']['season_id'],
                    'oponent_season_id' => ($oponent_team instanceof Team && !empty($oponent_team->id) && $oponent_team->getActiveSeason() instanceof Season ? $oponent_team->getActiveSeason()->id : 0),
                    'confirmed_home' => $now,
                    'confirmed_away' => ($oponent_team instanceof Team && !empty($oponent_team->id) ? '0' : $now), // pokud mame neexistujici tym, tak muzeme nastavit ze je udalost potvrzena
                    'result_confirmed_home' => '0',
                    'result_confirmed_away' => '0',
                    'type'       => $_POST['event']['type'],
                    'created'    => $now,
                    'start'      => $start,
                    'end'        => $end,
                    'info'       => serialize(array(0 => $_POST['event']))
                );
                
                // ulozime si ID udalosti, protoze pak se bdue muze generovat opakovaci udalost a nevedeli bychom odkud kopirovat obrazek druheho tymu
                $first_id = $data['id'];
                
                // pokud se to povedlo vlozit
                if(EventDAO::insert($data))
                {
                    $hasImage = false;
                    // pokud se nám podařilo vložit událost, tak se pokusíme uploadovat logo
                    // upload loga týmu
                    if(empty($_POST['event']['team']) && !empty($_FILES['team_logo']))
                    {
                        $team->uploadImage(false, 'team_logo', false, '/event' . $data['id'] . '/');
                        $hasImage = true;
                    }
                    
                    // data pro notifikace
                    $notification_data = array();
                    
                    // info o udalosti do notifikace
                    $event = new Event($data);
                    
                    // musime vsem pozvanym poslat notifikaci
                    foreach($players as $player)
                    {
                        // nebudeme posilat notifikace prihlasenemu uzivateli 
                        if($player->id != $logged_user->id)
                        {
                            // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                            $notification_data[] = array(
                                'user_id'    => $player->id,
                                'object_id'  => $event->team_id . '_' . $event->id . '_event', 
                                'type_id'    => 'EVENT-INVITATION',
                                'info'       => serialize(
                                                    array(
                                                        'event_id'  => $event->id, 
                                                        'user_team' => $logged_user->getActiveTeam()->id
                                                    )
                                                ),
                                'object_info'=> '',
                                'timestamp'  => strtotime($now),
                                'read'       => '0'
                            );

                            if($logged_user->getActiveTeam()->userCanViewModule('attendance', $player, false))
                            {
                                SendMail::sendEventInvitation($player->email, $logged_user->getActiveTeam(), $event);
                            }
                        }
                    }

                    // pokud je souper existujici tym, tak mu posleme notifikace
                    if(!empty($event->oponent_id))
                    {
                        // nacteme soupere
                        $oponent = TeamDAO::get($event->oponent_id);

                        // pokud existuje a zaroven ji souper nema smazanou
                        if($oponent instanceof Team && $oponent->id > 0 && !$event->isDeleted($oponent))
                        {
                            // nacteme si adminy a jim posleme notifikace
                            $admins = $oponent->getAdmins();

                            $data = array();
                            $time = time();

                            foreach($admins as $admin_id)
                            {
                                if($admin_id == $logged_user->id)
                                {
                                    continue;
                                }
                                
                                // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                                $notification_data[] = array(
                                    'user_id'    => $admin_id,
                                    'object_id'  => $event->team_id . '_' . $event->id . '_event', 
                                    'type_id'    => 'EVENT-ADD-CONFIRM-REQUEST',
                                    'info'       => serialize(
                                                        array(
                                                            'user_team'     => $oponent->id,
                                                            'oponent_team'  => $logged_user->getActiveTeam()->id,
                                                            'event_id'      => $event->id
                                                        )
                                                    ),
                                    'object_info'=> '',
                                    'timestamp'  => $time,
                                    'read'       => '0'
                                );
                            }
                        }
                        else
                        {
                            // FIXME nejaky log, jinak je nam to jedno
                        }
                    }
                    
                    // uložíme do DB nové notifikace
                    Notifications::insertNotifications($notification_data);
                    
                    // zprava o pridani pro existujici tym
                    if(!empty($event->oponent_id))
                    {
                        $_project['message']->addDone($tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Událost byla úspěšně vytvořena a odeslána soupeři ke schválení.</p>'));
                    }
                    // neexistujici tym
                    else
                    {
                        $_project['message']->addDone($tr->tr('<h3 class="title">Hotovo!</h3><p class="text">Událost byla úspěšně vytvořena.</p>'));
                    }

                    $_project['message']->saveMessages();
                }
                // chyba při přidávání?
                else
                {
                    $_project['message']->addDone($tr->tr('<h3 class="title">Chyba!</h3><p class="error">Nastala chyba při přidávání události. Zkuste to později znovu.</p>'));
                    $_project['message']->saveMessages();
                }
                
                
                // má se událost opakovat?
                // musime mit odeslany checkbox, vyplnene datum a datum musi byt vetsi nez zacatek udalosti
                if(!empty($_POST['event']['repeat']) && !empty($_POST['event']['repeat_to']) && $_POST['event']['repeat_to'] > $start_date)
                {
                    $allowed = array(
                        'day',
                        'month',
                        'week',
                        'year'
                    );
                    
                    // aby to nechtel generovat treba na kazdou minutu
                    if(in_array($_POST['event']['repeat_interval'], $allowed))
                    {
                        $new_start = date('Y-m-d H:i:s', strtotime($data['start'] . ' +1'.$_POST['event']['repeat_interval']));
                        $new_end = date('Y-m-d H:i:s', strtotime($data['end'] . ' +1'.$_POST['event']['repeat_interval']));
                        
                        while($new_start <= ($_POST['event']['repeat_to'] . ' 23:59:59'))
                        {
                            $data['id'] = EventDAO::getLastId($data['team_id'], $type);
                            $data['start'] = $new_start;
                            $data['end'] = $new_end;
                            
                            if(EventDAO::insert($data))
                            {
                                // pokud se nám podařilo vložit událost, tak se pokusíme uploadovat logo
                                // upload loga týmu
                                if($hasImage)
                                {
                                    foreach($global_user_images as $img)
                                    {
                                        if(!file_exists(TEAM_DATADIR . $data['team_id'] . '/' . 'event' . $data['id'] . '/'))
                                        {
                                            mkdir(TEAM_DATADIR . $data['team_id'] . '/' . 'event' . $data['id'] . '/');
                                        }
                                        
                                        // pokusime se zkopirovat obrazek hostujiciho tymu
                                        if(!copy(TEAM_DATADIR . $data['team_id'] . '/' . 'event' . $first_id . '/' . $img['name'], TEAM_DATADIR . $data['team_id'] . '/' . 'event' . $data['id'] . '/' . $img['name']))
                                        {
                                            // FIXME - nepovedl se upload obrazku k jedne udalosti, takze nejakej fix nebo report
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // FIXME
                                // nejaka chybka, ale pokracuj dal
                            }
                        
                            $new_start = date('Y-m-d H:i:s', strtotime($new_start . ' +1'.$_POST['event']['repeat_interval']));
                            $new_end = date('Y-m-d H:i:s', strtotime($new_end . ' +1'.$_POST['event']['repeat_interval']));
                        }
                    }
                }
            }
        }
        
        if($ok)
        {
            Header('Location: ' . PATH_WEB_ROOT . 'events/');
            die;
        }
        // je tam nejaka chyba, musime mu vypsat hlasku
        else
        {
            // FIXME
        }
    } // konec vložení
}
?>