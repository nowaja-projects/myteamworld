<?php
$activeTeam = $logged_user->getActiveTeam();

if(isset($_POST['event']) && $activeTeam instanceof Team && $activeTeam->isAdmin($logged_user))
{
    // FIXME
    $date_from = $_POST['event']['date'] . ' ' .$_POST['event']['hour_from'] . ':00';
    $date_to = $_POST['event']['date'] . ' ' .$_POST['event']['hour_to'] . ':00';
    
    $data = array(
        'parent_id'     => '0',
        'source_id'     => '0',
        'user_id'       => $logged_user->id,
        'team_id'       => $activeTeam->id,
        'type'          => 'event',
        'created'       => date('Y-m-D H:i:s'),
        'date_begin'    => $date_from,
        'date_end'      => $date_to,
        'value'         => serialize($_POST['event'])
    );
    
    $event = new Event($data);
    
    $event->save();
}
?>