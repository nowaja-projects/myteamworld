<?php

if(isset($_GET['id']))
{
    // id udalosti
    $id = explode('-', $_GET['id']);
    
    // musime mit presne dve ID
    if(count($id) != 2)
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }
    
    // nacteme si událost
    $event = EventDAO::get($id[0], $id[1]);

    // udalost musi existovat
    if($event instanceof Event && !empty($event->id))
    {
        $event->info = unserialize($event->info);
        $event->result = unserialize($event->result);
        $title = 'Nastavit parametry události';
        $is_update = true;

        // domaci tym
        $homeTeam = TeamDAO::get($event->team_id);

        // nacteme si info o sportu
        $sport = SportDAO::get($homeTeam->sport_id);
    }
    else
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }

    // uzivatel musi mit prava to vubec editovat
    if(!$event->userCanEdit($logged_user))
    {
        // FIXME
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }

    // vysledek a statistiky muzeme zadavat az kdyz je udalost potvrzena
    if(!$event->isConfirmed())
    {
        // FIXME
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }

    // zaroven jeste nesmi byt potvrzeny vysledek
    if($event->isResultConfirmed() && !empty($event->oponent_id))
    {
        // FIXME
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;   
    }

    // soutěžní nebo přátelský zápas
    if($event->type == Event::COMPETITION || $event->type == Event::FRIENDLY)
    {
        // musime zjistit jestli jsme domaci nebo hoste
        if($event->team_id != $team->id && $event->oponent_id == $team->id && !empty($event->oponent_id))
        {
            $event_team = TeamDAO::get($event->team_id);
        }
        elseif($event->oponent_id != $team->id && $event->team_id == $team->id && !empty($event->oponent_id))
        {
            $event_team = TeamDAO::get($event->oponent_id);
        }
        // nemáme tým, tak je to trénink nebo týmová akce - toto by se nemelo stat nikdy, jinak je neco spatne // FIXME LOG
        elseif(empty($event->oponent_id))
        {
            $event_team = false;
        }
        else
        {
            Header('Location: ' . PATH_WEB_ROOT . 'events/');
            die;
        }
        
        // mame tym ktery je na MTW
        if(!empty($event->oponent_id))
        {
            // nacteme si data
            $event_data = array(
                'homeTeam' => TeamDAO::get($event->team_id),
                'awayTeam' => TeamDAO::get($event->oponent_id) 
            );
        }
        // neexistujici tym
        else
        {
            // nacteme si data
            $event_data = array(
                'homeTeam' => TeamDAO::get($event->team_id),
                'awayTeam' => new Team()
            );
        }

        // ukladani podrobnosti o zapase
        if(isset($_POST) && !empty($_POST))
        {
            // podle toho pak budeme ukladat info o udalosti
            $data = array(
                'id'         => $event->id,
                'team_id'    => $event->team_id,
                'team_stats' => '',
                'result'     => '',
                'rosters'    => ''
            );

            // pocet casti zapasu
            $periods = @intval($_POST['result']['result']['periods']);

            // musime mit poslany pocet casti zapasu
            if(empty($periods) || $periods < 1 || $periods > 10) // FIXME jaky je maximalni pocet casti pro dany sport
            {
                Header('Location: ' . PATH_WEB_ROOT . 'events/');
                die;
            }

            // vysledek zapasu
            if(isset($_POST['result']['result']))
            {
                // pokud neni zadan nejaky vysledek celkovy, tak jebat - nema vypinat javascript ktery to kontroluje
                if(!is_numeric($_POST['result']['result']['home']['final']) || !is_numeric($_POST['result']['result']['away']['final']))
                {
                    Header('Location: ' . PATH_WEB_ROOT . 'events/');
                    die;
                }

                if($periods > 1)
                {
                    // projdeme vsechny tretiny a zkontrolujeme zda maji zadany vysledek
                    for($i = 1; $i <= $periods; $i++)
                    {
                        $period = 'period' . $i;
                        // pokud neni zadan nejaky vysledek tretiny, tak jebat - nema vypinat javascript ktery to kontroluje
                        if(!isset($_POST['result']['result']['home'][$period]) || !isset($_POST['result']['result']['away'][$period]) || !is_numeric($_POST['result']['result']['home'][$period]) || !is_numeric($_POST['result']['result']['away'][$period]))
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }
                    }
                }

                // pokud vybral, ze bylo prodlouzeni
                if(!empty($_POST['result']['result']['isOvertime']))
                {
                    // musi mit vyplneny vysledek prodlouzeni
                    if(!is_numeric(@$_POST['result']['result']['home']['overtime']) || !is_numeric(@$_POST['result']['result']['away']['final']))
                    {
                        Header('Location: ' . PATH_WEB_ROOT . 'events/');
                        die;
                    }
                }

                // pro ulozeni do db
                $data['result'] = serialize($_POST['result']['result']);
            }

            // sestavy týmů
            if(isset($_POST['result']['players']))
            {
                $players = $_POST['result']['players'];
                // zkontrolujeme zda u vsech sedi hash
                foreach($_POST['result']['players'] as $key => $rows)
                {
                    // musi byt domaci nebo hoste
                    if($key != 'home' && $key != 'away')
                    {
                        Header('Location: ' . PATH_WEB_ROOT . 'events/');
                        die;
                    }
                    
                    $awayCount = 0;
                    // projdeme vsechny odeslane hrace
                    foreach($rows as $key1 => $row)
                    {
                        
                        // pokud uzivatel neexistuje, tak ho muzeme otrimovat
                        $row['player'] = trim(@$row['player']);
                        
                        if($row['player'] == '000' || empty($row['player']))
                        {
                            // odebereme aby se zbytecne neukladal
                            unset($players[$key][$key1]);
                            continue;
                        }

                        // pokud hrac existuje, tak zkontrolujeme, zda opravdu existuje
                        if(!empty($row['exists']))
                        {
                            // zjistime hash
                            $player = explode('-', $row['player']);

                            // musi byt ve tvaru player_id-player_hash
                            if(count($player) != 2)
                            {
                                Header('Location: ' . PATH_WEB_ROOT . 'events/');
                                die;
                            }

                            // kontrola hashe
                            if($player[1] != getRecipientHash($player[0]))
                            {
                                Header('Location: ' . PATH_WEB_ROOT . 'events/');
                                die;
                            }
                        }
                        /// hrac neni na MTW - kontrolujeme jen jesztli ma jmeno, a jestli neni moc dlouhe
                        else
                        {
                            // nesmi byt prazdne jmeno
                            if(empty($row['player']) || mb_strlen($row['player']) > 100)
                            {
                                Header('Location: ' . PATH_WEB_ROOT . 'events/');
                                die;
                            }

                            $players[$key][$awayCount] = $row;

                            if($key1 != $awayCount)
                            {
                                unset($players[$key][$key1]);
                            }

                            $awayCount++;
                        }
                    }
                }
                //printr($_POST['result']['player_stats']);   die;

                // FIXME - kontrola zda ma vybranou pozici ze spravneho sportu v obou pripadech
                // FIXME - kontrola zda je cislo ve spravnem rozsahu
                // FIXME - kontrola zda je opravdu clenem tymu

                $data['rosters'] = serialize($players);
            }

            // statistiky hracu
            if(isset($_POST['result']['player_stats']))
            {
                $player_stats = array();

                foreach($_POST['result']['player_stats'] as $key => $player_data)
                {
                    // existujici uzivatel
                    if(!empty($player_data['exists']))
                    {
                        $player = explode('-', $key);

                        // musi byt ve tvaru home/away-player_id-player_hash-position_id
                        if(count($player) != 4)
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }

                        // musi byt domaci nebo hoste
                        if($player[0] != 'home' && $player[0] != 'away')
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }

                        // kontrola hashe
                        if($player[2] != getRecipientHash($player[1]))
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }

                        // nacteme si info o jeho pozici
                        $position = SportDAO::getPosition($player[3]);

                        if(!$position)
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }
                    }
                    // hrac je jen fiktivni, zkontrolujeme jen pozici
                    else
                    {
                        $player = explode('-', $key);

                        // musi byt ve tvaru home/away-jmeno-position_id
                        if(count($player) != 3)
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }

                        // musi byt domaci nebo hoste
                        if($player[0] != 'home' && $player[0] != 'away')
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }

                        // nacteme si info o jeho pozici
                        $position = SportDAO::getPosition($player[2]);

                        if(!$position)
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }
                    }

                    // jake hodnoty mohou byt odeslany ve statistikach
                    $available_stats = unserialize($position->stats);

                    if(!is_array($available_stats))
                    {
                        $available_stats = array();
                    }

                    // tohle uz nas dale nezajima
                    $exists = intval(@$player_data['exists']);
                    unset($player_data['exists']);

                    // zkontrolujeme vsechny statistiky
                    foreach($player_data as $key1 => $val)
                    {
                        // musi poslana statistika existovat
                        if(!in_array($key1, array_keys($available_stats)))
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }

                        // statistiky by mely byt cislo
                        $player_data[$key1] = intval($val);
                    }

                    // pole pro ulozeni
                    $array = array(
                        'user_id'   => $player[1], //($exists ? $player[1] : $key),
                        'position_id' => $position->id,
                        'team'      => $player[0],
                        'team_id'   => $player[0] == 'home' ? $event->team_id : $event->oponent_id,
                        'event_id'  => $_GET['id']
                    );

                    // pripoji
                    $array += $player_data;

                    // po pozdejsi ulozeni
                    $player_stats[] = $array;
                }

                // FIXME tabulka podle sportu
                // promazeme stare statistiky k tomuto zapasu
                if(StatsDAO::deleteStatsForEvent($_GET['id'], $sport->stat_table))
                {
                    // jestlize byly odeslany nejaky data, ktery jsou v poradku, tak je ulozime
                    StatsDAO::insertStatArray($player_stats, $sport->stat_table);
                }
            }

            // tymove statistiky
            if(isset($_POST['result']['team_stats']))
            {
                // info o tymovych statistikach
                $available_stats = unserialize($sport->team_stats);

                // jestli se vubec nejaky tymovy statistiky ukladaji
                if(is_array($available_stats))
                {
                    // pak zkontrolujeme zda sedi data
                    foreach($_POST['result']['team_stats'] as $key => $data1)
                    {
                        // nejaky podvrzeny data
                        if(!in_array($key, array_keys($available_stats)))
                        {
                            Header('Location: ' . PATH_WEB_ROOT . 'events/');
                            die;
                        }
                    }
                }

                // vse je ok, muzeme updatovat udalost
                $data['team_stats'] = serialize($_POST['result']['team_stats']);
            }

            // ulozime do DB
            if(EventDAO::updateEventStats($data))
            {
                $now = date('Y-m-d H:i:s');
                $text = '<h3 class="title">Hotovo</h3><p class="text">Zadání výsledku a statistik k zápasu proběhlo úspěšně!</p>';
                $_project['message']->addDone($text);
                $_project['message']->saveMessages();

                $oponent_team_id = 0;

                // jestlize aktivni tym je domaci, tak posleme hostum notifikaci aby schvalili vysledek
                if($event->team_id == $logged_user->getActiveTeam()->id)
                {
                    // samozrejme pouze pokud souper je existujici tym
                    if(!empty($event->oponent_id))
                    {
                        $event->result_confirmed_home = $now;
                        $event->result_confirmed_away = 0;

                        $oponent_team_id = $event->oponent_id;
                    }
                    // jinak muzeme vse rovnou nastavit na schvalene
                    else
                    {
                        $event->result_confirmed_home = $now;
                        $event->result_confirmed_away = $now;
                    }
                }
                // jestlize jsme hoste
                elseif($event->oponent_id == $logged_user->getActiveTeam()->id)
                {
                    $oponent_team_id = $event->team_id;

                    $event->result_confirmed_home = 0;
                    $event->result_confirmed_away = $now;
                }

                $event->info = serialize($event->info);
                $event->result = serialize($event->result);

                $event->save();

                // pokud vime kdo je souper
                if(!empty($oponent_team_id))
                {
                    // nacteme soupere
                    $oponent = TeamDAO::get($oponent_team_id);

                    // pokud existuje a zaroven ji souper nema smazanou
                    if($oponent instanceof Team && $oponent->id > 0 && !$event->isDeleted($oponent))
                    {
                        // nacteme si adminy a jim posleme notifikace
                        $admins = $oponent->getAdmins();

                        $data = array();
                        $time = time();

                        foreach($admins as $admin_id)
                        {
                            if($admin_id == $logged_user->id)
                            {
                                continue;
                            }
                            
                            // loged user ukladame bez nactene zdi atd, proto zapis takovy jaky je
                            $data[] = array(
                                'user_id'    => $admin_id,
                                'object_id'  => $event->team_id . '_' . $event->id . '_event',
                                'type_id'    => 'EVENT-RESULT-CONFIRM-REQUEST',
                                'info'       => serialize(
                                                    array(
                                                        'user_team'     => $oponent->id,
                                                        'oponent_team'  => $logged_user->getActiveTeam()->id,
                                                        'event_id'      => $event->id
                                                    )
                                                ),
                                'object_info'=> '',
                                'timestamp'  => $time,
                                'read'       => '0'
                            );
                        }
                
                        // uložíme do DB nové notifikace
                        @Notifications::insertNotifications($data);
                    }
                    else
                    {
                        // FIXME nejaky log, jinak je nam to jedno
                    }
                }

                // kdyz neni souper muzeme ji zobrazit na nastence
                if(empty($event->oponent_id))
                {
                    $data = WallDAO::getLastTypeObject('event-result', $team->id, $event->team_id . '_' . $event->id . '_event', 0);

                    if(!$data)
                    {
                        $val = array();
                        $obj = array(
                            'object_parent_id'     => $event->team_id . '_' . $event->id . '_event',
                            'object_source_id'     => 0,
                            'object_user_id'       => $logged_user->id,
                            'object_team_id'       => $event->team_id,
                            'object_oponent_id'    => 0,
                            'object_type'          => 'event-result',
                            'object_date_created'  => date('Y-m-d H:i:s'),
                            'object_date_begin'    => date('Y-m-d H:i:s'),
                            'object_value'         => serialize($val)
                        );

                        WallDAO::insertTypeItem($obj);
                    }
                }

                // presmerujeme na detail udalosti
                Header('Location: ' . PATH_WEB_ROOT . 'event-detail/' . $_GET['id']);
                die;
            }
            else
            {
                // FIXME
            }
        }
    }
    // u ostatnich nezadavame vysledek, ani statistiky
    else
    {
        Header('Location: ' . PATH_WEB_ROOT . 'events/');
        die;
    }
}
else
{
    Header('Location: ' . PATH_WEB_ROOT . 'events/');
    die;
}

?>