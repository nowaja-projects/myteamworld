<?php
if(!$team->userCanViewModule('gallery', $logged_user))
{
	Header('Location:' . PATH_WEB_ROOT);
	die;
}

if(!empty($_GET['id']))
{
	$id = explode('-', $_GET['id']);

	if(count($id) != 2)
	{
		Header('Location: ' . PATH_WEB_ROOT);
		die;
	}

	$gallery = GalleryDAO::getGallery($id[0], $id[1]);

	if(!($gallery instanceof Gallery))
	{
		Header('Location: ' . PATH_WEB_ROOT);
		die;
	}
}
else
{
	Header('Location: ' . PATH_WEB_ROOT);
	die;
}
