<?php

// pokud je uzivatel prihlasen, tak na teto strance nema co delat
if(!empty($logged_user->id))
{
    Header('Location: ' . PATH_WEB_ROOT);
    die;
}

if(isset($_POST['password']))
{
    if(!empty($_POST['password']['email']))
    {
        $user = UserDAO::getByEmail($_POST['password']['email']);
        
        // jestli uzivatel existuje
        if($user instanceof User && !empty($user->id))
        {
            //zkusime vytvorit a ulozit nove heslo
            $new_password = $user->createNewPassword();
            if($new_password)
            {
                SendMail::sendNewPassword($user, $new_password);
                
                $_project['message']->addDone($tr->tr('Na váš e-mail jsme zaslali nové heslo.'));
                $_project['message']->saveMessages();
                
                Header('Location: ' . PATH_WEB_ROOT);
                die;
            }
            // nejaka chyba, tak ji vypis
            else
            {
                $_project['message']->addWarning($tr->tr('Nastala chyba při odesílání nového heslo. Zkuste to prosím později znovu.'));
                $_project['message']->saveMessages();
            }
            
        }
        else
        {
            $_project['message']->addWarning($tr->tr('Zadaný email není registrován na mtw.'));
            $_project['message']->saveMessages();
        }
        
        unset($user);
    }
    else
    {
        $_project['message']->addWarning($tr->tr('Zadejte prosím Vaši emailovou adresu.'));
        $_project['message']->saveMessages();
    }
}

?>